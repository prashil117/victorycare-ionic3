webpackJsonp([34],{

/***/ 942:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppMyHealthcarePageModule", function() { return AppMyHealthcarePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_my_healthcare__ = __webpack_require__(996);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppMyHealthcarePageModule = (function () {
    function AppMyHealthcarePageModule() {
    }
    AppMyHealthcarePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_my_healthcare__["a" /* AppMyHealthcarePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_my_healthcare__["a" /* AppMyHealthcarePage */]),
            ],
        })
    ], AppMyHealthcarePageModule);
    return AppMyHealthcarePageModule;
}());

//# sourceMappingURL=app-my-healthcare.module.js.map

/***/ }),

/***/ 996:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppMyHealthcarePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_component__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__environment_environment__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










/**
 * Generated class for the AppMyHealthcarePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppMyHealthcarePage = (function () {
    function AppMyHealthcarePage(navCtrl, navParams, menu, storage, alertCtrl, databaseService, loadingService, formBuilder, myApp, http, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.formBuilder = formBuilder;
        this.myApp = myApp;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.baseurl = __WEBPACK_IMPORTED_MODULE_9__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_9__environment_environment__["a" /* environment */].appsecret;
        this.eventsData = [];
        this.patientData = {};
        this.SearchData = [];
        this.healthcareProviders = [];
        this.result = [];
        this.dropdown = [];
        this.selected1 = [];
        ////////// HCP DropDown Code Starts /////////////
        this.SelectList = formBuilder.group({
            currentHCP: ['']
        });
    }
    AppMyHealthcarePage.prototype.appIntialize1 = function () {
        var _this = this;
        this.myApp.initializeApp();
        this.username = this.userLoginData.email;
        this.usertype = this.userLoginData.usertype;
        this.userid = this.userLoginData.id;
        this.ut = (this.usertype == 'healthcareprovider' ? false : true);
        console.log(this.userid);
        // alert(this.usertype);
        this.dropdown = this.getHCPData(this.userid, this.usertype);
        console.log(this.dropdown);
        var result = null;
        this.get('currentHCP').then(function (result) {
            console.log('Username1: ' + result);
            if (result != null) {
                _this.notselected = false;
                _this.selected1 = result;
                console.log('Username: ' + _this.selected1);
            }
            else {
                _this.notselected = true;
                console.log(_this.dropdown);
                _this.selected1 = _this.dropdown;
            }
        });
        this.gethcpdatafordoctor();
    };
    AppMyHealthcarePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppMyHealthcarePage');
        this.getHCPid();
    };
    AppMyHealthcarePage.prototype.logout = function () {
        this.databaseService.logout();
    };
    AppMyHealthcarePage.prototype.gethcpdatafordoctor = function () {
        var _this = this;
        this.loadingService.show();
        this.data.action = "gethcpdatafordoctor";
        this.data.id = this.userLoginData.id;
        this.data.appsecret = this.appsecret;
        this.http.post(this.baseurl + "getuser.php", this.data).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            console.log(result.data);
            if (result.status == 'success') {
                _this.loadingService.hide();
                if (result.data !== null) {
                    _this.data.values = result.data;
                    _this.SearchData = result.data;
                }
                else {
                    _this.loadingService.hide();
                    _this.data = [];
                    _this.data.keys = [];
                    _this.data.values = [];
                    _this.SearchData = [];
                }
            }
            else {
                _this.loadingService.hide();
                _this.toastCtrl.presentToast("There is a problem adding data, please try again");
                // this.navCtrl.setRoot("AppDashboardPage");
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
        }, function (err) {
            console.log(err);
            _this.loadingService.hide();
        });
    };
    AppMyHealthcarePage.prototype.setFilteredPatientsData = function () {
        var _this = this;
        this.SearchData = this.data.values.filter(function (Patient) {
            console.log(Patient);
            return Patient.firstname.toLowerCase().indexOf(_this.terms.toLowerCase()) > -1;
        });
    };
    AppMyHealthcarePage.prototype.editHealthcareProvider = function (email, editable) {
        var _this = this;
        this.loadingService.show();
        console.log(email);
        console.log(editable);
        this.data.action = "getuserbyid";
        this.data.userid = email;
        this.data.appsecret = this.appsecret;
        console.log(this.data.userid);
        this.http.post(this.baseurl + "getuser.php", this.data).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            if (result.status == 'exists') {
                _this.loadingService.hide();
                _this.data = result.data;
                _this.data.name = result.data.firstname + ' ' + result.data.lastname;
                _this.navCtrl.push("AppHealthcareproviderEditPage", { 'data': _this.data, 'action': 'edit', 'editable': editable });
            }
            else {
                _this.loadingService.hide();
                _this.toastCtrl.presentToast("Unable to fetch data");
                // self.errorMessage = "Unable to fetch data";
            }
        }, function (err) {
            _this.loadingService.hide();
            console.log(err);
        });
    };
    AppMyHealthcarePage.prototype.deleteHealthcareProvidersData = function (item) {
        var _this = this;
        var item1 = [];
        item1 = [item];
        console.log(item1[0]);
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Do you want to leave from this Healthcare Provider???',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function () {
                        var self = _this;
                        var value = [];
                        var keys = [];
                        var result = [];
                        var currentData = [{ id: self.userLoginData.id, email: self.userLoginData.email, name: self.userLoginData.firstname }];
                        console.log('Delete Ok clicked');
                        _this.data.action = "unlinkhcpfromdoctor";
                        _this.data.userid = _this.userLoginData.id;
                        _this.data.parentid = item1[0].parentid;
                        _this.data.appsecret = _this.appsecret;
                        console.log(_this.data);
                        _this.http.post(_this.baseurl + "removeuser.php", _this.data).subscribe(function (data) {
                            _this.loadingService.show();
                            var result = JSON.parse(data["_body"]);
                            console.log(result.status);
                            if (result.status == 'success') {
                                _this.loadingService.hide();
                                // self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
                                self.toastCtrl.presentToast("Healthcare Provider is unlinked successfully");
                                _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                                // self.logout();
                            }
                            else {
                                _this.loadingService.hide();
                                self.toastCtrl.presentToast("There is an error unlinking HCP!");
                                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                            }
                        }, function (err) {
                            _this.loadingService.hide();
                            console.log(err);
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    AppMyHealthcarePage.prototype.comparer = function (otherArray) {
        return function (current) {
            return otherArray.filter(function (other) {
                return other.email == current.email; // && other.name == current.name
                // return other == current
            }).length == 0;
        };
    };
    AppMyHealthcarePage.prototype.onAddHealthcareprovider = function (data, addexist, showSaveButton) {
        console.log(data);
        this.navCtrl.push("AppHealthcareproviderEditPage", { 'hcpdata': data, 'addexist': addexist, 'showSaveButton': showSaveButton, 'editable': false });
    };
    ////////// HCP DropDown Code Starts /////////////
    AppMyHealthcarePage.prototype.getHCPData = function (userid, usertype) {
        var _this = this;
        console.log(userid);
        console.log(usertype);
        var keys = [];
        var hcp1 = [];
        var self = this;
        var webServiceData = {};
        webServiceData.action = "gethcpdata";
        webServiceData.id = userid;
        webServiceData.appsecret = this.appsecret;
        webServiceData.usertype = usertype;
        self.data.id = this.userLoginData.id;
        self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
            _this.loadingService.show();
            var result = JSON.parse(data["_body"]);
            console.log(result);
            if (result.status == 'success') {
                _this.loadingService.hide();
                console.log(result.data);
                // result.data;
                hcp1 = hcp1.push(result.data);
                // hcp1 = result.data;
                console.log(hcp1);
                // return result1;
            }
            else {
                _this.loadingService.hide();
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
        }, function (err) {
            _this.loadingService.hide();
            console.log(err);
        });
        return hcp1;
    };
    AppMyHealthcarePage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        console.log('set string in storage: ' + result);
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        console.log(reason_1);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppMyHealthcarePage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        console.log('storageGET: ' + key + ': ' + result);
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_2 = _a.sent();
                        console.log(reason_2);
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppMyHealthcarePage.prototype.getHCPid = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('currentHCP').then(function (result) {
                            if (result != null) {
                                _this.hcpid = result;
                                _this.getuserLoginData();
                            }
                            else {
                                _this.getuserLoginData();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppMyHealthcarePage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('userLogin').then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                if (_this.userLoginData.usertype == "healthcareprovider") {
                                    _this.hcpid = _this.userLoginData.id;
                                }
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppMyHealthcarePage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("this.userdata", this.userLoginData);
                        return [4 /*yield*/, this.get('patientType').then(function (result) {
                                console.log("patienttypesdfdsfsdsdf", result);
                                if ((!result || result == null || result == undefined) && _this.userLoginData == "patient") {
                                    _this.doRefresh();
                                }
                                else {
                                    _this.patientType = result;
                                    _this.appIntialize1();
                                    console.log("sekeccdffvfdgdfgdfg", _this.patientType);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppMyHealthcarePage.prototype.remove = function (key) {
        this.storage.remove(key);
    };
    AppMyHealthcarePage.prototype.doRefresh = function (event) {
        var _this = this;
        if (event === void 0) { event = ''; }
        this.loadingService.show();
        setTimeout(function () {
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            _this.loadingService.hide();
        }, 2000);
    };
    AppMyHealthcarePage.prototype.selectEmployee = function ($event) {
        this.doRefresh();
        this.set('currentHCP', $event);
    };
    AppMyHealthcarePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-my-healthcare',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-my-healthcare/app-my-healthcare.html"*/'<!--\n  Generated template for the AppMyHealthcarePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>My Healthcare Provider List</ion-title>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <!-- <form [formGroup]="SelectList">\n      <ion-select *ngIf="ut && notselected" (ionChange)="selectEmployee($event)" right>\n        <ion-label>Select Healthcare Provider</ion-label>\n        <ion-option *ngFor="let key of dropdown[0]; let idx = index;" [selected]="key.id==selected1[0].id"\n          value="{{key.id}}">{{key.firstname}}</ion-option>\n      </ion-select>\n      <ion-select *ngIf="ut && !notselected" (ionChange)="selectEmployee($event)" right>\n        <ion-label>Select Healthcare Provider</ion-label>\n        <ion-option *ngFor="let key of dropdown[0]; let idx = index;" [selected]="key.id==selected1" value="{{key.id}}">\n          {{key.firstname}}</ion-option>\n      </ion-select>\n    </form> -->\n    <!-- <ion-buttons right (click)="logout()">\n      <button ion-button icon-only>\n        <ion-icon name="lock">Logout</ion-icon>\n      </button>\n    </ion-buttons> -->\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding class="fadeIn">\n  <ion-row>\n    <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n    </ion-col>\n    <ion-col col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2>\n    </ion-col>\n    <ion-col col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n      <ion-searchbar [(ngModel)]="terms" (ionInput)="setFilteredPatientsData()" placeholder="Type to search here">\n      </ion-searchbar>\n    </ion-col>\n  </ion-row>\n  <ion-grid padding *ngIf="data != null">\n    <ion-row>\n      <ion-col col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 style="padding:5px 15px 5px 15px;">\n        <ion-card text-center style="height: 177px;">\n          <ion-grid>\n            <ion-row>\n              <ion-col col-3>\n              </ion-col>\n              <ion-col (click)="onAddHealthcareprovider(SearchData, \'addexist\', true)">\n                <img style="height: 124px;width: 124px; margin:0px auto;" src="assets/images/others/add-patients.png" />\n              </ion-col>\n              <ion-col col-3>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-3>\n              </ion-col>\n              <ion-col>\n                Add New Healthcare Provider\n              </ion-col>\n              <ion-col col-3>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-card>\n      </ion-col>\n      <ion-col col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 *ngFor="let item of SearchData;let i = index;"\n        style="padding:5px 15px 5px 15px;">\n        <ion-card text-center style="height: 177px;">\n          <ion-grid>\n            <ion-row>\n              <ion-col col-3 style="margin-left:16px;">\n                <img *ngIf="item.image" src="{{item.image}}" />\n                <img *ngIf="!item.image" style="border-radius: 50% !important ;max-width: 150% !important;height: 90px !important;width: 90px !important;" src="assets/images/nopreview.jpeg" />\n              </ion-col>\n              <ion-col>\n                <ion-grid>\n                  <ion-row>\n                    <ion-col>\n                      <img (click)="editHealthcareProvider(item.id, false, false)" class="card-top-icon"\n                        style="margin-left:5px;" src="assets/images/others/info-icon.png" />\n                      <img (click)="deleteHealthcareProvidersData(item)" class="card-top-icon"\n                        src="assets/images/others/delete-icon.png" />\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col style="margin: 9px 0px 0px 72px;">\n                      <h2 style="font-size:16px; color:#fff;" text-center>{{item.firstname}}</h2>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n          <ion-row>\n            <ion-col style="top: -15px;">\n              <ion-label stacked>\n                Email:\n                <span style="color: #e0675c;margin-left: 7px;">*</span>\n              </ion-label>\n              <p style="font-size:12px; color:#fff;" text-center>{{item.email}}</p>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-my-healthcare/app-my-healthcare.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* MenuController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_6__app_app_component__["a" /* MyApp */], __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_8__services_toast_service__["a" /* ToastService */]])
    ], AppMyHealthcarePage);
    return AppMyHealthcarePage;
}());

//# sourceMappingURL=app-my-healthcare.js.map

/***/ })

});
//# sourceMappingURL=34.js.map