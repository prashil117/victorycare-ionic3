webpackJsonp([36],{

/***/ 940:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppMonthlyReportPageModule", function() { return AppMonthlyReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_monthly_report__ = __webpack_require__(994);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppMonthlyReportPageModule = (function () {
    function AppMonthlyReportPageModule() {
    }
    AppMonthlyReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_monthly_report__["a" /* AppMonthlyReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_monthly_report__["a" /* AppMonthlyReportPage */]),
            ],
        })
    ], AppMonthlyReportPageModule);
    return AppMonthlyReportPageModule;
}());

//# sourceMappingURL=app-monthly-report.module.js.map

/***/ }),

/***/ 994:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppMonthlyReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(569);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environment_environment__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var option = {
    legend: {
        display: false
    },
    scales: {
        xAxes: [{
                gridLines: {
                    display: false
                }
            }],
        yAxes: [{
                display: false,
                gridLines: {
                    display: false,
                    drawBorder: false,
                }
            }],
    }
};
var dougnhutOption = {
    responsive: true,
    aspectRatio: 1,
    maintainAspectRatio: true,
    legend: {
        display: true,
        position: 'bottom',
        labels: {
            usePointStyle: true,
        }
    }
};
var AppMonthlyReportPage = (function () {
    function AppMonthlyReportPage(events, toastService, loadingService, http, navCtrl, navParams, storage) {
        this.events = events;
        this.toastService = toastService;
        this.loadingService = loadingService;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.baseurl = __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].appsecret;
        this.TotalSleep = 0;
        this.TotalDistress = 0;
        this.TotalComfort = 0;
        this.TotalUncomfort = 0;
        this.startOfWeek = __WEBPACK_IMPORTED_MODULE_8_moment__(__WEBPACK_IMPORTED_MODULE_8_moment__().startOf('week').format('YYYY/MM/DD'));
        this.AsleepBarChartArray = [];
        this.AwakeBarChartArray = [];
        this.PainBarChartArray = [];
        this.UncomfortBarChartArray = [];
        this.AwakeArray = [];
        this.AsleepArray = [];
        this.PainArray = [];
        this.UncomfortArray = [];
        this.weeKnumberArray = [];
        this.weeklyHour = 720;
        this.selectedDate = new Date();
    }
    AppMonthlyReportPage.prototype.ionViewDidLoad = function () {
        this.getuserLoginData();
    };
    AppMonthlyReportPage.prototype.SleepDoughnut = function (shour) {
        var data = [this.weeklyHour, shour];
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartRef.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Total', 'Consume'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(83,155,192)',
                            'rgb(107,177,214)'
                        ],
                    }]
            },
            options: dougnhutOption
        });
    };
    AppMonthlyReportPage.prototype.DistressDoughnut = function (Dhour) {
        var data = [this.weeklyHour, Dhour];
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartRef1.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Total', 'Consume'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(226,96,96)',
                            'rgb(171,87,87)'
                        ],
                    }]
            },
            options: dougnhutOption
        });
    };
    AppMonthlyReportPage.prototype.UnomfortableDoughnut = function (Uhour) {
        var data = [this.weeklyHour, Uhour];
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartRef2.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Total', 'Consume'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(250,208,0)',
                            'rgb(201,169,13)'
                        ],
                    }]
            },
            options: dougnhutOption
        });
    };
    AppMonthlyReportPage.prototype.ComfortableDoughnut = function (Chour) {
        var data = [this.weeklyHour, Chour];
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartRef3.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Total', 'Consume'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(175,219,7)',
                            'rgb(151,187,12)'
                        ],
                    }]
            },
            options: dougnhutOption
        });
    };
    AppMonthlyReportPage.prototype.SleepChart = function () {
        this.barchart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.barchartRef.nativeElement, {
            type: 'bar',
            data: {
                labels: this.weeKnumberArray,
                datasets: [{
                        barPercentage: 0.5,
                        pointBorderColor: 'red',
                        data: this.AsleepBarChartArray,
                        backgroundColor: [
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)'
                        ],
                        borderColor: [
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                        ],
                        borderWidth: 1,
                    }, {
                        data: this.AsleepBarChartArray,
                        backgroundColor: [
                            'rgb(107,177,214)',
                        ],
                        borderColor: [
                            'rgb(107,177,214)',
                        ],
                        type: 'line',
                        fill: false
                    }
                ]
            },
            options: option
        });
    };
    AppMonthlyReportPage.prototype.DistressChart = function () {
        this.barchart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.barchartRef1.nativeElement, {
            type: 'bar',
            data: {
                labels: this.weeKnumberArray,
                datasets: [{
                        barPercentage: 0.5,
                        data: this.PainBarChartArray,
                        backgroundColor: [
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                        ],
                        borderColor: [
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                        ],
                        borderWidth: 1
                    }, {
                        data: this.PainBarChartArray,
                        backgroundColor: [
                            'rgb(171,87,87)',
                        ],
                        borderColor: [
                            'rgb(171,87,87)',
                        ],
                        type: 'line',
                        fill: false
                    }]
            },
            options: option
        });
    };
    AppMonthlyReportPage.prototype.ComfortableChart = function () {
        this.barchart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.barchartRef2.nativeElement, {
            type: 'bar',
            data: {
                labels: this.weeKnumberArray,
                datasets: [{
                        barPercentage: 0.5,
                        data: this.AwakeBarChartArray,
                        backgroundColor: [
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                        ],
                        borderColor: [
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                        ],
                        borderWidth: 1
                    },
                    {
                        data: this.AwakeBarChartArray,
                        backgroundColor: [
                            'rgb(201,169,13)',
                        ],
                        borderColor: [
                            'rgb(201,169,13)',
                        ],
                        type: 'line',
                        fill: false
                    }]
            },
            options: option
        });
    };
    AppMonthlyReportPage.prototype.UncomfortableChart = function () {
        this.barchart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.barchartRef3.nativeElement, {
            type: 'bar',
            data: {
                labels: this.weeKnumberArray,
                datasets: [{
                        barPercentage: 0.5,
                        data: this.UncomfortBarChartArray,
                        backgroundColor: [
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                        ],
                        borderColor: [
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                        ],
                        borderWidth: 1
                    },
                    {
                        data: this.UncomfortBarChartArray,
                        backgroundColor: [
                            'rgb(175,219,7)',
                        ],
                        borderColor: [
                            'rgb(175,219,7)',
                        ],
                        type: 'line',
                        fill: false
                    }
                ]
            },
            options: option
        });
    };
    AppMonthlyReportPage.prototype.appIntialize = function () {
        var _this = this;
        var date = '01';
        var month = __WEBPACK_IMPORTED_MODULE_8_moment__(this.selectedDate).format('MM-YYYY');
        console.log("month", month);
        var newDate = date + "-" + month;
        console.log("new dae", newDate);
        var currentWeek = parseInt(__WEBPACK_IMPORTED_MODULE_8_moment__(newDate, 'DD?MM/YYYY').format('w')) - 1;
        console.log("new dae", currentWeek);
        this.AsleepBarChartArray = [];
        this.AwakeBarChartArray = [];
        this.PainBarChartArray = [];
        this.UncomfortBarChartArray = [];
        this.AwakeArray = [];
        this.AsleepArray = [];
        this.PainArray = [];
        this.UncomfortArray = [];
        this.TotalSleep = 0;
        this.TotalDistress = 0;
        this.TotalComfort = 0;
        this.dateArray = [];
        this.weeKnumberArray = [];
        this.TotalUncomfort = 0;
        this.SleepHours = "0:0";
        this.DistressHours = "0:0";
        this.ComfortHours = "0:0";
        this.UncomfortHours = "0:0";
        this.loadingService.show();
        var data = {
            userid: this.userLoginData.id,
            appsecret: this.appsecret,
            startOfMonth: month,
            action: 'getmonthlygraphdata',
        };
        for (var i = 0; i < 4; i++) {
            var currentWeek = currentWeek + 1;
            this.weeKnumberArray.push("Week " + currentWeek);
        }
        this.http.post(this.baseurl + 'getgraphs.php', data).subscribe(function (data) {
            _this.loadingService.hide();
            var data1 = JSON.parse(data['_body']);
            var chartData = data1.graphSumData;
            if (data1.status == "success") {
                console.log("startofmonahr", data1);
                if (chartData && chartData.length > 0) {
                    _this.getDiffer(chartData);
                    var Aindex = 0;
                    var Pindex = 0;
                    var Uindex = 0;
                    var Sindex = 0;
                    for (var i_1 = 1; i_1 <= 5; i_1++) {
                        if (_this.UncomfortArray[Uindex] !== undefined) {
                            if (i_1 == parseInt(_this.UncomfortArray[Uindex].week)) {
                                var sec = (_this.UncomfortArray[Uindex].endtimeseconds - _this.UncomfortArray[Uindex].starttimeseconds) * 1000;
                                var tmpvalue = Math.round(__WEBPACK_IMPORTED_MODULE_8_moment__["duration"](sec).asHours());
                                _this.UncomfortBarChartArray.push(tmpvalue);
                                Uindex = Uindex + 1;
                            }
                            else {
                                _this.UncomfortBarChartArray.push(0);
                            }
                        }
                        else {
                            _this.UncomfortBarChartArray.push(0);
                        }
                        if (_this.AsleepArray[Sindex] !== undefined) {
                            if (i_1 == parseInt(_this.AsleepArray[Sindex].week)) {
                                var sec = (_this.AsleepArray[Sindex].endtimeseconds - _this.AsleepArray[Sindex].starttimeseconds) * 1000;
                                var tmpvalue = Math.round(__WEBPACK_IMPORTED_MODULE_8_moment__["duration"](sec).asHours());
                                _this.AsleepBarChartArray.push(tmpvalue);
                                Sindex = Sindex + 1;
                            }
                            else {
                                _this.AsleepBarChartArray.push(0);
                            }
                        }
                        else {
                            _this.AsleepBarChartArray.push(0);
                        }
                        if (_this.PainArray[Pindex] !== undefined) {
                            if (i_1 == parseInt(_this.PainArray[Pindex].week)) {
                                var sec = (_this.PainArray[Pindex].endtimeseconds - _this.PainArray[Pindex].starttimeseconds) * 1000;
                                var tmpvalue = Math.round(__WEBPACK_IMPORTED_MODULE_8_moment__["duration"](sec).asHours());
                                _this.PainBarChartArray.push(tmpvalue);
                                Pindex = Pindex + 1;
                            }
                            else {
                                _this.PainBarChartArray.push(0);
                            }
                        }
                        else {
                            _this.PainBarChartArray.push(0);
                        }
                        if (_this.AwakeArray[Aindex] !== undefined) {
                            if (i_1 == parseInt(_this.AwakeArray[Aindex].week)) {
                                var sec = (_this.AwakeArray[Aindex].endtimeseconds - _this.AwakeArray[Aindex].starttimeseconds) * 1000;
                                var tmpvalue = Math.round(__WEBPACK_IMPORTED_MODULE_8_moment__["duration"](sec).asHours());
                                _this.AwakeBarChartArray.push(tmpvalue);
                                Aindex = Aindex + 1;
                            }
                            else {
                                _this.AwakeBarChartArray.push(0);
                            }
                        }
                        else {
                            _this.AwakeBarChartArray.push(0);
                        }
                    }
                }
                else {
                    _this.AwakeBarChartArray = [0, 0, 0, 0];
                    _this.PainBarChartArray = [0, 0, 0, 0];
                    _this.AsleepBarChartArray = [0, 0, 0, 0];
                    _this.UncomfortBarChartArray = [0, 0, 0, 0];
                }
                _this.SleepChart();
                _this.DistressChart();
                _this.ComfortableChart();
                _this.UncomfortableChart();
                _this.DistressDoughnut(parseInt(_this.DistressHours.split(":")[0]));
                _this.SleepDoughnut(parseInt(_this.SleepHours.split(":")[0]));
                _this.ComfortableDoughnut(parseInt(_this.ComfortHours.split(":")[0]));
                _this.UnomfortableDoughnut(parseInt(_this.UncomfortHours.split(":")[0]));
            }
        }, function (err) {
            _this.loadingService.hide();
        });
    };
    AppMonthlyReportPage.prototype.getDiffer = function (chartData) {
        var DistressArray = [];
        var SleepArray = [];
        var UncomfortArray = [];
        var ComfortArray = [];
        for (var i in chartData) {
            if (chartData[i].name == "Awake") {
                this.AwakeArray.push(chartData[i]);
                this.TotalComfort += parseInt(chartData[i].count_name);
                var startTime = __WEBPACK_IMPORTED_MODULE_8_moment__(chartData[i].starttime).format("HH:mm");
                var endTime = __WEBPACK_IMPORTED_MODULE_8_moment__(chartData[i].endtime).format("HH:mm");
                var diff = this.diff(startTime, endTime);
                ComfortArray.push(diff);
            }
            if (chartData[i].name == "Asleep") {
                this.AsleepArray.push(chartData[i]);
                this.TotalSleep += parseInt(chartData[i].count_name);
                var startTime = __WEBPACK_IMPORTED_MODULE_8_moment__(chartData[i].starttime).format("HH:mm");
                var endTime = __WEBPACK_IMPORTED_MODULE_8_moment__(chartData[i].endtime).format("HH:mm");
                var diff = this.diff(startTime, endTime);
                SleepArray.push(diff);
            }
            if (chartData[i].name == "Pain") {
                this.PainArray.push(chartData[i]);
                this.TotalDistress += parseInt(chartData[i].count_name);
                var startTime = __WEBPACK_IMPORTED_MODULE_8_moment__(chartData[i].starttime).format("HH:mm");
                var endTime = __WEBPACK_IMPORTED_MODULE_8_moment__(chartData[i].endtime).format("HH:mm");
                var diff = this.diff(startTime, endTime);
                DistressArray.push(diff);
            }
            if (chartData[i].name == "Uncomfortable") {
                this.UncomfortArray.push(chartData[i]);
                this.TotalUncomfort += parseInt(chartData[i].count_name);
                var startTime = __WEBPACK_IMPORTED_MODULE_8_moment__(chartData[i].starttime).format("HH:mm");
                var endTime = __WEBPACK_IMPORTED_MODULE_8_moment__(chartData[i].endtime).format("HH:mm");
                var diff = this.diff(startTime, endTime);
                UncomfortArray.push(diff);
            }
        }
        ;
        this.DistressHours = this.calulateTotalTime(DistressArray);
        this.DistressDoughnut(parseInt(this.DistressHours.split(":")[0]));
        this.SleepHours = this.calulateTotalTime(SleepArray);
        this.SleepDoughnut(parseInt(this.SleepHours.split(":")[0]));
        this.ComfortHours = this.calulateTotalTime(ComfortArray);
        this.ComfortableDoughnut(parseInt(this.ComfortHours.split(":")[0]));
        this.UncomfortHours = this.calulateTotalTime(UncomfortArray);
        this.UnomfortableDoughnut(parseInt(this.UncomfortHours.split(":")[0]));
    };
    AppMonthlyReportPage.prototype.diff = function (start, end) {
        start = start.split(":");
        end = end.split(":");
        var startDate = new Date(0, 0, 0, start[0], start[1], 0);
        var endDate = new Date(0, 0, 0, end[0], end[1], 0);
        var diff = endDate.getTime() - startDate.getTime();
        var hours = Math.floor(diff / 1000 / 60 / 60);
        diff -= hours * 1000 * 60 * 60;
        var minutes = Math.floor(diff / 1000 / 60);
        // If using time pickers with 24 hours format, add the below line get exact hours
        if (hours < 0)
            hours = hours + 24;
        return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
    };
    AppMonthlyReportPage.prototype.hours = function (hh) {
        if (hh !== undefined)
            return hh.split(":")[0];
    };
    AppMonthlyReportPage.prototype.minutes = function (mm) {
        if (mm !== undefined)
            return mm.split(":")[1];
    };
    AppMonthlyReportPage.prototype.avg = function (hh) {
        if (hh !== undefined)
            return Math.ceil(hh.split(":")[0] / 4);
    };
    AppMonthlyReportPage.prototype.calulateTotalTime = function (timeArray) {
        var sum = timeArray.reduce(function (acc, time) { return acc.add(__WEBPACK_IMPORTED_MODULE_8_moment__["duration"](time)); }, __WEBPACK_IMPORTED_MODULE_8_moment__["duration"]());
        return [Math.floor(sum.asHours()), sum.minutes()].join(':');
    };
    AppMonthlyReportPage.prototype.getDates1 = function () {
        var _this = this;
        this.events.subscribe('app:sendUser', function (logged) {
            _this.selectedDate = logged;
            _this.appIntialize();
        });
        this.events.publish('app:getUser');
    };
    AppMonthlyReportPage.prototype.ionViewWillEnter = function () {
        this.getDates1();
    };
    AppMonthlyReportPage.prototype.ionViewWillLeave = function () {
        this.events.unsubscribe('app:sendUser');
    };
    AppMonthlyReportPage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppMonthlyReportPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        else {
                            return [2 /*return*/, null];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        reason_2 = _a.sent();
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppMonthlyReportPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("patientType").then(function (result) {
                            if (result == "independent") {
                                _this.dependent = false;
                            }
                            _this.appIntialize();
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppMonthlyReportPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("userLogin").then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineChart'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppMonthlyReportPage.prototype, "chartRef", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppMonthlyReportPage.prototype, "barchartRef", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineChart1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppMonthlyReportPage.prototype, "chartRef1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppMonthlyReportPage.prototype, "barchartRef1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineChart2'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppMonthlyReportPage.prototype, "chartRef2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart2'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppMonthlyReportPage.prototype, "barchartRef2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineChart3'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppMonthlyReportPage.prototype, "chartRef3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart3'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppMonthlyReportPage.prototype, "barchartRef3", void 0);
    AppMonthlyReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-monthly-report',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-monthly-report/app-monthly-report.html"*/'<ion-content padding>\n  <div style="overflow: hidden;height: 100%;">\n    <ion-row style="margin-top: -20px;">\n      <ion-col style="margin-left: -15px;height: 150px;display: inline-flex;\n      ">\n        <div class="rect-box">\n          <p class="copy-text">TOTAL</p>\n          <hr class="hr1">\n          <p class="count">{{TotalSleep}}</p>\n          <p class="registrations">REGISTRATIONS</p>\n        </div>\n        <div class="rect-box1">\n          <p class="copy-text2">SLEEP HOURS</p>\n          <br><br>\n          <hr class="hr2">\n          <p class="time">{{hours(SleepHours)}}<sub>hrs</sub> {{minutes(SleepHours)}}<sub>min</sub></p>\n        </div>\n        <div class="chart-container" style="height: 150px;width: 230px;margin-top:20px !important">\n          <canvas style="height: 120px;width: 120px;" #lineChart>{{ chart }}</canvas>\n        </div>\n        <div class="rect-box2">\n          <p class="count">{{avg(SleepHours)}}</p>\n          <hr class="hr3">\n          <p class="registrations">AVERAGE</p>\n        </div>\n        <div class="chart-container">\n          <canvas #barChart>{{ barchart }}</canvas>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col style="top:5px;margin-left: -15px;height: 150px;display: inline-flex;">\n        <div class="rect-box">\n          <p class="copy-text">TOTAL</p>\n          <hr class="hr12">\n          <p class="count">{{TotalDistress}}</p>\n          <p class="registrations">REGISTRATIONS</p>\n        </div>\n        <div class="rect-box1">\n          <p class="copy-text2">DISTRESS HOURS</p>\n          <br><br>\n          <hr class="hr2">\n          <p class="time">{{hours(DistressHours)}}<sub>hrs</sub> {{minutes(DistressHours)}}<sub>min</sub></p>\n        </div>\n        <div class="chart-container" style="height: 150px;width: 230px;margin-top:22px !important">\n          <canvas style="height: 120px;width: 120px;" #lineChart1>{{ chart }}</canvas>\n        </div>\n        <div class="rect-box2">\n          <p class="count">{{avg(DistressHours)}}</p>\n          <hr class="hr3">\n          <p class="registrations">AVERAGE</p>\n        </div>\n        <div class="chart-container">\n          <canvas #barChart1>{{ barchart }}</canvas>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col style="top:10px;margin-left: -15px;height: 150px;display: inline-flex;">\n        <div class="rect-box">\n          <p class="copy-text">TOTAL</p>\n          <hr class="hr13">\n          <p class="count">{{TotalUncomfort}}</p>\n          <p class="registrations">REGISTRATIONS</p>\n        </div>\n        <div class="rect-box1">\n          <p class="copy-text2">UNCOMFORTABLE HOURS</p>\n          <br><br>\n          <hr class="hr2">\n          <p class="time">{{hours(UncomfortHours)}}<sub>hrs</sub> {{minutes(UncomfortHours)}}<sub>min</sub></p>\n        </div>\n        <div class="chart-container" style="height: 150px;width: 230px;margin-top:24px !important">\n          <canvas style="height: 120px;width: 120px;" #lineChart2>{{ chart }}</canvas>\n        </div>\n        <div class="rect-box2">\n          <p class="count">{{TotalUncomfort}}</p>\n          <hr class="hr3">\n          <p class="registrations">AVERAGE</p>\n        </div>\n        <div class="chart-container">\n          <canvas #barChart3>{{ barchart }}</canvas>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col style="top:13px;margin-left: -15px;height: 150px;display: inline-flex;">\n        <div class="rect-box">\n          <p class="copy-text">TOTAL</p>\n          <hr class="hr14">\n          <p class="count">{{TotalComfort}}</p>\n          <p class="registrations">REGISTRATIONS</p>\n        </div>\n        <div class="rect-box1">\n          <p class="copy-text2">COMFORTABLE HOURS</p>\n          <br><br>\n          <hr class="hr2">\n          <p class="time">{{hours(ComfortHours)}}<sub>hrs</sub> {{minutes(ComfortHours)}}<sub>min</sub></p>\n        </div>\n        <div class="chart-container" style="height: 150px;width: 230px;margin-top:26px !important">\n          <canvas style="height: 120px;width: 120px;" #lineChart3>{{ chart }}</canvas>\n        </div>\n        <div class="rect-box2">\n          <p class="count">{{avg(ComfortHours)}}</p>\n          <hr class="hr3">\n          <p class="registrations">AVERAGE</p>\n        </div>\n        <div class="chart-container">\n          <canvas #barChart2>{{ barchart }}</canvas>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row style="margin-top:50px;">\n      <ion-col style="display:inline-flex;">\n        <div style="width: 17%;">\n          <p style="position: relative;\n          left: 30px;font-size:16px">Status of Well Being</p>\n        </div>\n        <div style="display:inline-flex;margin-left: 100px;">\n          <div class="sleep"></div>\n          <p class="p1">Sleep</p>\n          <div class="comfort"></div>\n          <p class="p1">Awake - Comfortable</p>\n          <div class="uncomfort"></div>\n          <p class="p1">Awake - Uncomfortable</p>\n          <div class="distress"></div>\n          <p class="p1">Awake - Distress</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-monthly-report/app-monthly-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Events */], __WEBPACK_IMPORTED_MODULE_7__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_6__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], AppMonthlyReportPage);
    return AppMonthlyReportPage;
}());

//# sourceMappingURL=app-monthly-report.js.map

/***/ })

});
//# sourceMappingURL=36.js.map