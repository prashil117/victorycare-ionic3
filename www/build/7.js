webpackJsonp([7],{

/***/ 1015:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_directive_email_validator__ = __webpack_require__(974);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_modal_app_modal__ = __webpack_require__(570);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__environment_environment__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_fcm__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_settings_terms_and_conditions_app_settings_terms_and_conditions__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_storage__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var AppRegisterPage = (function () {
    function AppRegisterPage(fcm, platform, storage, navCtrl, navParams, modalCtrl, formBuilder, alertCtrl, databaseService, toastCtrl, loadingService, http) {
        this.fcm = fcm;
        this.platform = platform;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.toastCtrl = toastCtrl;
        this.loadingService = loadingService;
        this.http = http;
        this.termscondition = false;
        this.data = {};
        this.baseurl = __WEBPACK_IMPORTED_MODULE_10__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_10__environment_environment__["a" /* environment */].appsecret;
        this.Registerdata = {};
        this.passwordType = 'password';
        this.passwordType1 = 'password';
        this.passwordIcon = 'eye-off';
        this.passwordIcon1 = 'eye-off';
        this.pushes = [];
        this.isEnabled = false;
        this.Registerform = formBuilder.group({
            // usertype: ['', Validators.required],
            // name: ['', Validators.required],
            firstname: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].pattern('[a-zA-Z ]*')])],
            lastname: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].pattern('[a-zA-Z ]*')])],
            email: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_3__app_directive_email_validator__["a" /* EmailValidator */].isValid])],
            password: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required],
            confirmPassword: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required],
            termscondition: [''],
        }, { validator: this.matchingPasswords('password', 'confirmPassword') });
        this.initPushNotification();
        console.log('isCheck', this.termscondition);
    }
    AppRegisterPage.prototype.initPushNotification = function () {
        var _this = this;
        this.platform.ready().then(function () {
            if (_this.platform.is('corodova') || _this.platform.is('ios')) {
                _this.fcm.getToken().then(function (token) {
                    _this.data.fcmtoken = token;
                    console.log("new token", token);
                    console.log(token);
                });
                _this.fcm.onTokenRefresh().subscribe(function (token) {
                    //register token
                    _this.data.fcmtoken = token;
                    console.log("tokenrefresh", token);
                });
            }
        });
    };
    AppRegisterPage.prototype.OpenTermsCondition = function () {
        console.log("hello");
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_12__app_settings_terms_and_conditions_app_settings_terms_and_conditions__["a" /* AppSettingsTermsAndConditionsPage */], null, {});
        modal.present();
    };
    AppRegisterPage.prototype.matchingPasswords = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirmPassword = group.controls[confirmPasswordKey];
            if (password.value !== confirmPassword.value) {
                return {
                    confirmPassword: true
                };
            }
        };
    };
    AppRegisterPage.prototype.hideShowPassword = function (item) {
        if (item == "pass1") {
            this.passwordType1 = this.passwordType1 === 'text' ? 'password' : 'text';
            this.passwordIcon1 = this.passwordIcon1
                === 'eye-off' ? 'eye' : 'eye-off';
        }
        else {
            this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
            this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
        }
    };
    AppRegisterPage.prototype.isInputValid = function () {
        this.usertypeMsg = "";
        this.nameMsg = "";
        this.firstnameMsg = "";
        this.lastnameMsg = "";
        this.emailMsg = "";
        this.passwordMsg = "";
        this.confirmPasswordMsg = "";
        if (this.data.usertype === undefined) {
            this.nameMsg = "Usertype cannot be empty";
            return false;
        }
        else if (this.data.firstname === undefined) {
            this.firstnameMsg = "Firstname cannot be empty";
            return false;
        }
        else if (this.data.firstname.length > 25 && this.data.firstname == "") {
            this.firstnameMsg = "Firstname cannot be greater than 25 characters";
            return false;
        }
        else if (this.data.lastname === undefined) {
            this.lastnameMsg = "Lastname cannot be empty";
            return false;
        }
        else if (this.data.lastname.length > 25 && this.data.lastname == "") {
            this.lastnameMsg = "Lastname cannot be greater than 25 characters";
            return false;
        }
        else if (!this.isEmailIDCorrect(this.data.email)) {
            this.emailMsg = "Please enter valid Email ID";
            return false;
        }
        else if (this.data.password === undefined) {
            this.passwordMsg = "Password cannot be empty";
            return false;
        }
        else if (this.data.confirmPassword === undefined) {
            this.confirmPasswordMsg = "Password cannot be empty";
        }
        else if (this.data.password !== this.data.confirmPassword) {
            this.confirmPasswordMsg = "Password does not match";
            return false;
        }
        else if (!this.termscondition) {
            this.errmsg = "please check the terms and condition";
            return false;
        }
        else
            return true;
    };
    AppRegisterPage.prototype.isEmailIDCorrect = function (email) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    };
    AppRegisterPage.prototype.backToLogin = function () {
        this.navCtrl.setRoot("AppLoginPage");
    };
    AppRegisterPage.prototype.terms = function () {
        if (this.termscondition) {
            this.errmsg = "";
        }
        else {
            this.errmsg = "please check the terms and condition";
        }
    };
    AppRegisterPage.prototype.createAccount = function () {
        var _this = this;
        var self = this;
        this.submitAttempt = true;
        if (!this.Registerform.valid) {
            console.log("Registration form Validation Fire!");
        }
        else {
            this.data.action = "insert";
            this.data.base_url = this.baseurl;
            this.data.appsecret = this.appsecret;
            this.data.firsttimelogin = 1;
            this.data.isterms = true;
            this.data.usertype = 'patient';
            // if (this.data.usertype == "patient") {
            //     this.data.firsttimelogin = 1;
            // }
            // else {
            //     this.data.firsttimelogin = 0;
            // }
            if (this.isInputValid()) {
                this.loadingService.show();
                // this.data.id = this.af.database.ref().push().key;
                console.log("register data:::::>>>>>>", this.data);
                this.http.post(this.baseurl + "register.php", this.data).subscribe(function (data) {
                    _this.loadingService.hide();
                    var result = JSON.parse(data["_body"]);
                    console.log(result.status1);
                    if (result.status1 == 'success') {
                        _this.loadingService.hide();
                        var modal = self.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__app_modal_app_modal__["a" /* AppModalPage */], {
                            'title': 'Confirm your email', 'description': 'Your account was successfully created but there\'s still few steps to do. Go to your mailbox and confirm your email and set up your profile!'
                        }, { cssClass: "my-register" });
                        modal.present();
                        // self.resetForm();
                        _this.navCtrl.setRoot('AppWelcomeScreenPage');
                    }
                    else if (result.status1 == 'exists') {
                        _this.loadingService.hide();
                        var modal = self.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__app_modal_app_modal__["a" /* AppModalPage */], { 'title': 'Email already exists', 'description': 'The email address you are trying to create is already exists, please try another email address.' }, { cssClass: "my-register" });
                        modal.present();
                        // self.resetForm();
                    }
                    else if (result.status1 == 'errorEmail') {
                        _this.loadingService.hide();
                        var modal = self.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__app_modal_app_modal__["a" /* AppModalPage */], { 'title': 'Email could not send', 'description': 'User registered but email couldnt be send, please contact administrator.', cssClass: "my-register" });
                        modal.present();
                    }
                    else {
                        _this.loadingService.hide();
                        // this.showToast("Something went wrong");
                        self.toastCtrl.presentToast("Something went wrong, Please check if email address you have enntered is same or not.");
                    }
                }, function (err) {
                    _this.loadingService.hide();
                    console.log(err);
                });
                // }, function(error){
                //     console.log(error);
                //     if(error!==undefined && error.message!==undefined){
                //         self.errorMessage = error.message;
                //     }
                //     self.resetForm();
                // });
            }
        }
    };
    // New Implementation
    AppRegisterPage.prototype.goBack = function () {
        this.navCtrl.setRoot("AppWelcomeScreenPage");
    };
    AppRegisterPage.prototype.resetForm = function () {
        this.data.name = "";
        this.data.email = "";
        this.data.password = "";
        this.data.confirmPassword = "";
    };
    AppRegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-register/app-register.html"*/'<ion-content padding class="fadeIn">\n    <ion-buttons left>\n        <button style="background-color:transparent;" (click)="goBack()" ion-button icon-only>\n            <ion-icon name="arrow-back"></ion-icon>\n        </button>\n    </ion-buttons>\n    <h1 class="app-header" style="margin-top: 15px;">Sign Up</h1>\n    <ion-grid style="text-align:center;position: relative;margin-top: -30px;">\n        <form [formGroup]="Registerform" style="margin-top: -25px;">\n            <ion-row wrap>\n                <ion-col col-12 col-sm-12 col-md-8 offset-md-2 offset-lg-3 col-lg-6 offset-xl-3 col-xl-6>\n\n                    <!-- <h1 class="app-header">Sign Up</h1> -->\n                    <div class="app-description margin-3t" style="margin-left: 80px;">\n                        Already have an account? <span class="link" (click)="backToLogin()">Log in here </span>\n                    </div>\n                    <div class="alert alert-danger fade in alert-dismissible" *ngIf="errorMessage">\n                        {{errorMessage}}\n                    </div>\n                    <!-- <ion-item style="margin-bottom:0;" margin transparent no-lines>\n                            <ion-label [whiteLabel] stacked>\n                                Customer Type\n                                <span  style="color: #e0675c;margin-left: 7px;">*</span>\n                            </ion-label>\n                            <ion-select style="color: white;" formControlName="usertype" placeholder="You are registering as.." [(ngModel)]="data.usertype">\n                                <ion-option value="patient">Patient</ion-option>\n                                <ion-option value="healthcareprovider">Health Care Provider</ion-option>\n                                <ion-option value="doctor">Doctor</ion-option>\n                                <ion-option value="nurse">Nurse</ion-option>\n                            </ion-select>\n                        </ion-item>\n                        <p *ngIf="!Registerform.controls.usertype.valid && (Registerform.controls.usertype.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please Select User Type</p> -->\n                    <!-- Firstname -->\n                    <ion-item style="margin-bottom:0; margin-top: 0px;" margin transparent no-lines>\n                        <ion-label [whiteLabel] stacked>First Name</ion-label>\n                        <ion-input placeholder="Type your firstname here" formControlName="firstname" type="text"\n                            [(ngModel)]="data.firstname"></ion-input>\n                    </ion-item>\n                    <ion-label style="float: left;margin: 0px 33px;"\n                        *ngIf="!Registerform.controls.firstname.valid && (Registerform.controls.firstname.dirty || submitAttempt)">\n                        <p style="margin-top:2px;margin-bottom:0;color:#e0675c">\n                            Firstname cannot be empty\n                        </p>\n                    </ion-label>\n\n                    <!-- Lastname  -->\n                    <ion-item style="margin-bottom:0;margin-top: 0px;" margin transparent no-lines>\n                        <ion-label [whiteLabel] stacked>Last Name</ion-label>\n                        <ion-input placeholder="Type your lastname here" formControlName="lastname" type="text"\n                            [(ngModel)]="data.lastname"></ion-input>\n                    </ion-item>\n                    <ion-label style="float: left;margin: 0px 33px;"\n                        *ngIf="!Registerform.controls.lastname.valid && (Registerform.controls.lastname.dirty || submitAttempt)">\n                        <p style="margin-top:2px;margin-bottom:0;color:#e0675c">\n                            Lastname cannot be empty\n                        </p>\n                    </ion-label>\n\n                    <ion-item style="margin-bottom:0;margin-top: 0px;" margin transparent no-lines>\n                        <ion-label [whiteLabel] stacked>Email Address</ion-label>\n                        <ion-input formControlName="email" type="text" placeholder="Type your email address here"\n                            [(ngModel)]="data.email"></ion-input>\n                    </ion-item>\n                    <ion-label style="float: left;margin: 0px 33px;"\n                        *ngIf="!Registerform.controls.email.valid && (Registerform.controls.email.dirty || submitAttempt)">\n                        <p style="margin-top:2px;margin-bottom:0;color:#e0675c">\n                            Please enter valid Email ID\n                        </p>\n                    </ion-label>\n                    <ion-item style="margin-bottom:0;margin-top: 0px;" margin transparent no-lines>\n                        <ion-label [whiteLabel] stacked>Password</ion-label>\n                        <ion-input formControlName="password" [type]="passwordType1"\n                            placeholder="Type your password here" [(ngModel)]="data.password"></ion-input>\n                        <ion-icon style="color:#81889f;margin-top: 50px;" [name]="passwordIcon1" item-right (click)="\n                                hideShowPassword(\'pass1\')"></ion-icon>\n                    </ion-item>\n\n                    <ion-label style="float: left;margin: 0px 33px;"\n                        *ngIf="!Registerform.controls.password.valid && (Registerform.controls.password.dirty || submitAttempt)">\n                        <p style="margin-top:2px;margin-bottom:0;color:#e0675c">\n                            Password cannot be empty\n                        </p>\n                    </ion-label>\n                    <ion-item style="margin-bottom:0;margin-top: 0px;" margin transparent no-lines>\n                        <ion-label [whiteLabel] stacked>Confirm Password</ion-label>\n                        <ion-input formControlName="confirmPassword" [type]="passwordType"\n                            placeholder="Re-type your password here" [(ngModel)]="data.confirmPassword"\n                            (keyup.enter)="createAccount()"></ion-input>\n                        <ion-icon style="color:#81889f;margin-top: 50px;" [name]="passwordIcon" item-right (click)="\n                        hideShowPassword(\'pass2\')">\n                        </ion-icon>\n                    </ion-item>\n                    <ion-label style="float: left;margin: 0px 33px;"\n                        *ngIf="!Registerform.controls.confirmPassword.valid && (Registerform.controls.confirmPassword.dirty || submitAttempt)">\n                        <p style="margin-top:2px;margin-bottom:0;color:#e0675c">\n                            Password cannot be empty\n                        </p>\n                    </ion-label>\n                    <ion-label style="float: left;margin: 0px 33px;" *ngIf="Registerform.hasError(\'confirmPassword\')">\n                        <p style="margin-top:2px;margin-bottom:0;color:#e0675c">\n                            Passwords are not <strong>matching</strong>.\n                        </p>\n                    </ion-label>\n                    <div class="app-description margin-4t">\n                        <!-- Remember me and stay Log In -->\n                        <ion-row>\n                            <ion-col col-4>\n                                <ion-checkbox  (ionChange)="terms()" [(ngModel)]="termscondition" formControlName="termscondition"></ion-checkbox>\n                            </ion-col>\n                            <ion-col col-8>\n                                <p style="top: -13px;position: relative;right: 63px;color:#69c2d1"  (click)="OpenTermsCondition()">Click here to read terms and conditions</p>\n                            </ion-col>\n                            <p style="margin-top:0px;margin-bottom:0;color:#e0675c;margin-left: 65px;" *ngIf="errmsg"> \n                                {{errmsg}}\n                              </p>\n                        </ion-row>\n                    </div>\n                    <div class="margin-3t ">\n                        <button (click)="createAccount()" class="dark-button" ion-button round>\n                            Save\n                        </button>\n\n                    </div>\n                    <!-- <div class="margin-3t">\n                        <p style="font-size: 16px;margin-left: 10px;margin-top: -16px;" stacked\n                            >\n                            Or Sign Up using social media account\n                        </p>\n                        <img class="button-margin" (click)="loginwithfackbook()" src="assets/app-images/facebook.png" />\n                        <img class="button-margin" (click)="loginwithgoogle()" src="assets/app-images/google.png" />\n                        <img class="button-margin" src="assets/app-images/microsoft.png" />\n                    </div> -->\n                </ion-col>\n            </ion-row>\n        </form>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-register/app-register.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_4__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_11__ionic_native_fcm__["a" /* FCM */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* Platform */], __WEBPACK_IMPORTED_MODULE_13__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */], __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_7__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_8__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_9__angular_http__["a" /* Http */]])
    ], AppRegisterPage);
    return AppRegisterPage;
}());

//# sourceMappingURL=app-register.js.map

/***/ }),

/***/ 961:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRegisterModule", function() { return AppRegisterModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_register__ = __webpack_require__(1015);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppRegisterModule = (function () {
    function AppRegisterModule() {
    }
    AppRegisterModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_register__["a" /* AppRegisterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_register__["a" /* AppRegisterPage */]),
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], AppRegisterModule);
    return AppRegisterModule;
}());

//# sourceMappingURL=app-register.module.js.map

/***/ }),

/***/ 974:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailValidator; });
var EmailValidator = (function () {
    function EmailValidator() {
    }
    EmailValidator.isValid = function (control) {
        var re = /^\s*(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$/.test(control.value);
        if (re) {
            return null;
        }
        return { "invalidEmail": true };
    };
    return EmailValidator;
}());

//# sourceMappingURL=email-validator.js.map

/***/ })

});
//# sourceMappingURL=7.js.map