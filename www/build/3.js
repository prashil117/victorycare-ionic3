webpackJsonp([3],{

/***/ 931:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppHealthcareproviderEditPageModule", function() { return AppHealthcareproviderEditPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_healthcareprovider_edit__ = __webpack_require__(985);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppHealthcareproviderEditPageModule = (function () {
    function AppHealthcareproviderEditPageModule() {
    }
    AppHealthcareproviderEditPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_healthcareprovider_edit__["a" /* AppHealthcareproviderEditPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_healthcareprovider_edit__["a" /* AppHealthcareproviderEditPage */]),
            ],
        })
    ], AppHealthcareproviderEditPageModule);
    return AppHealthcareproviderEditPageModule;
}());

//# sourceMappingURL=app-healthcareprovider-edit.module.js.map

/***/ }),

/***/ 974:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailValidator; });
var EmailValidator = (function () {
    function EmailValidator() {
    }
    EmailValidator.isValid = function (control) {
        var re = /^\s*(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$/.test(control.value);
        if (re) {
            return null;
        }
        return { "invalidEmail": true };
    };
    return EmailValidator;
}());

//# sourceMappingURL=email-validator.js.map

/***/ }),

/***/ 975:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NativeGeocoder; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_core__ = __webpack_require__(32);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @name Native Geocoder
 * @description
 * Cordova plugin for native forward and reverse geocoding
 *
 * @usage
 * ```typescript
 * import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
 *
 * constructor(private nativeGeocoder: NativeGeocoder) { }
 *
 * ...
 *
 * let options: NativeGeocoderOptions = {
 *     useLocale: true,
 *     maxResults: 5
 * };
 *
 * this.nativeGeocoder.reverseGeocode(52.5072095, 13.1452818, options)
 *   .then((result: NativeGeocoderReverseResult[]) => console.log(JSON.stringify(result[0])))
 *   .catch((error: any) => console.log(error));
 *
 * this.nativeGeocoder.forwardGeocode('Berlin', options)
 *   .then((coordinates: NativeGeocoderForwardResult[]) => console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude))
 *   .catch((error: any) => console.log(error));
 * ```
 * @interfaces
 * NativeGeocoderReverseResult
 * NativeGeocoderForwardResult
 * NativeGeocoderOptions
 */
var NativeGeocoder = (function (_super) {
    __extends(NativeGeocoder, _super);
    function NativeGeocoder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Reverse geocode a given latitude and longitude to find location address
     * @param latitude {number} The latitude
     * @param longitude {number} The longitude
     * @param options {NativeGeocoderOptions} The options
     * @return {Promise<NativeGeocoderReverseResult[]>}
     */
    /**
       * Reverse geocode a given latitude and longitude to find location address
       * @param latitude {number} The latitude
       * @param longitude {number} The longitude
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderReverseResult[]>}
       */
    NativeGeocoder.prototype.reverseGeocode = /**
       * Reverse geocode a given latitude and longitude to find location address
       * @param latitude {number} The latitude
       * @param longitude {number} The longitude
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderReverseResult[]>}
       */
    function (latitude, longitude, options) { return; };
    /**
     * Forward geocode a given address to find coordinates
     * @param addressString {string} The address to be geocoded
     * @param options {NativeGeocoderOptions} The options
     * @return {Promise<NativeGeocoderForwardResult[]>}
     */
    /**
       * Forward geocode a given address to find coordinates
       * @param addressString {string} The address to be geocoded
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderForwardResult[]>}
       */
    NativeGeocoder.prototype.forwardGeocode = /**
       * Forward geocode a given address to find coordinates
       * @param addressString {string} The address to be geocoded
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderForwardResult[]>}
       */
    function (addressString, options) { return; };
    NativeGeocoder.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])({
            callbackOrder: 'reverse'
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Number, Number, Object]),
        __metadata("design:returntype", Promise)
    ], NativeGeocoder.prototype, "reverseGeocode", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])({
            callbackOrder: 'reverse'
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Object]),
        __metadata("design:returntype", Promise)
    ], NativeGeocoder.prototype, "forwardGeocode", null);
    /**
     * @name Native Geocoder
     * @description
     * Cordova plugin for native forward and reverse geocoding
     *
     * @usage
     * ```typescript
     * import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
     *
     * constructor(private nativeGeocoder: NativeGeocoder) { }
     *
     * ...
     *
     * let options: NativeGeocoderOptions = {
     *     useLocale: true,
     *     maxResults: 5
     * };
     *
     * this.nativeGeocoder.reverseGeocode(52.5072095, 13.1452818, options)
     *   .then((result: NativeGeocoderReverseResult[]) => console.log(JSON.stringify(result[0])))
     *   .catch((error: any) => console.log(error));
     *
     * this.nativeGeocoder.forwardGeocode('Berlin', options)
     *   .then((coordinates: NativeGeocoderForwardResult[]) => console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude))
     *   .catch((error: any) => console.log(error));
     * ```
     * @interfaces
     * NativeGeocoderReverseResult
     * NativeGeocoderForwardResult
     * NativeGeocoderOptions
     */
    NativeGeocoder = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["f" /* Plugin */])({
            pluginName: 'NativeGeocoder',
            plugin: 'cordova-plugin-nativegeocoder',
            pluginRef: 'nativegeocoder',
            repo: 'https://github.com/sebastianbaar/cordova-plugin-nativegeocoder',
            platforms: ['iOS', 'Android']
        })
    ], NativeGeocoder);
    return NativeGeocoder;
}(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["e" /* IonicNativePlugin */]));

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 985:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppHealthcareproviderEditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_geocoder__ = __webpack_require__(975);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_directive_email_validator__ = __webpack_require__(974);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__environment_environment__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};













/**
 * Generated class for the AppHealthcareproviderEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppHealthcareproviderEditPage = (function () {
    function AppHealthcareproviderEditPage(navCtrl, navParams, storage, formBuilder, nativeGeocoder, modalCtrl, alertCtrl, databaseService, loadingService, camera, http, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.nativeGeocoder = nativeGeocoder;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.camera = camera;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.data = {};
        // diagnosiss:any = {};
        this.upload = {};
        this.logo = {};
        this.baseurl = __WEBPACK_IMPORTED_MODULE_12__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_12__environment_environment__["a" /* environment */].appsecret;
        this.action = "";
        this.image = "";
        this.data1 = {};
        this.readonly = {};
        this.date = new Date();
        this.enableDiv = false;
        this.showSaveButton = false;
        this.usertype = [];
        this.hcpdata = [];
        this.doctorsData = [];
        this.hcp = [];
        this.result = [];
        this.data2 = [];
        this.upload = "assets/images/upload1.svg";
        this.readonly = false;
        this.Profileform = formBuilder.group({
            selectType: [''],
            doctorsEmail: [''],
            storedDoctors: [''],
            name: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].maxLength(15), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].minLength(3), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].pattern('[a-zA-Z ]*'), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
            email: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_8__app_directive_email_validator__["a" /* EmailValidator */].isValid, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].pattern('^[a-z0-9_.+-]+@[a-z0-9-]+.[a-z0-9-.]+$')])],
            birthDate: [''],
            mobile: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].maxLength(15), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].minLength(5), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].pattern('^[0-9]+$'), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
            address: [''],
            usertype: [''],
            description: ['']
        });
    }
    AppHealthcareproviderEditPage.prototype.intializeApp1 = function () {
        this.enableDiv = (this.navParams.data.editable == false ? true : false);
        this.showSaveButton = (this.navParams.data.showSaveButton == true ? true : false);
        this.data.usertype = (this.data.usertype === undefined ? "healthcareprovider" : this.data.usertype);
        console.log(this.enableDiv);
        console.log(this.showSaveButton);
        if (!__WEBPACK_IMPORTED_MODULE_6_lodash__["isEmpty"](this.navParams.data)) {
            console.log(this.navParams.data);
            this.data = this.navParams.data.data;
            console.log(this.data);
            // this.data.address = (this.data.address === undefined ? "" : this.data.address);
            // this.data.description = (this.data.description === undefined ? "" : this.data.description);
            // this.action = 'edit';
            // console.log('navParams',navParams.data);
            this.hcpdata = this.navParams.data.hcpdata;
            this.action = this.navParams.data.action;
            this.addexist = this.navParams.data.addexist;
            this.data1.newDoctors = this.getDoctors(this.hcpdata);
        }
        this.data1.newDoctors = (this.data1.newDoctors == undefined ? [] : this.data1.newDoctors);
        // this.enableDiv = false;
        if (this.data == undefined) {
            this.data = [];
            this.data.description = (this.data.description === undefined ? "" : this.data.description);
            this.data.address = (this.data.address === undefined ? "" : this.data.address);
        }
    };
    AppHealthcareproviderEditPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppHealthcareproviderEditPage');
        this.getuserLoginData();
    };
    AppHealthcareproviderEditPage.prototype.checkState = function (email) {
        var _this = this;
        console.log(email);
        this.loadingService.show();
        var hcpWebServiceData = {};
        hcpWebServiceData.action = "getuserbyid";
        hcpWebServiceData.id = email;
        hcpWebServiceData.appsecret = this.appsecret;
        console.log(this.data);
        this.http.post(this.baseurl + "login.php", hcpWebServiceData).subscribe(function (snapshot) {
            console.log(snapshot);
            var result = JSON.parse(snapshot["_body"]);
            // var snapshot = snapshot.val();
            if (result.status == 'success') {
                _this.data = result.data;
                _this.data.name = result.data.firstname + ' ' + result.data.lastname;
                _this.enableDropdown = false;
                _this.enableDiv = true;
                _this.loadingService.hide();
            }
            else {
                // self.loadingService.hide();
                _this.toastCtrl.presentToast("Unable to fetch data");
                // self.errorMessage = "Unable to fetch data";
            }
        });
    };
    AppHealthcareproviderEditPage.prototype.getDoctors = function (doctorsData) {
        var _this = this;
        this.loadingService.show();
        console.log(doctorsData);
        var result1 = [];
        var hcpWebServiceData = {};
        var self = this;
        hcpWebServiceData.action = "getdropdownhcpdata";
        hcpWebServiceData.getdropdownhcpdata = doctorsData;
        hcpWebServiceData.appsecret = this.appsecret;
        // self.data.id = this._cookieService.get('userid');
        self.http.post(this.baseurl + "getdropdowndata.php", hcpWebServiceData).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            console.log(result);
            if (result.status == 'success') {
                _this.loadingService.hide();
                console.log(result.data);
                // result.data;
                result1 = result1.push(result.data); //[result.data];
                console.log(result1);
                // return result1;
            }
            else {
                _this.loadingService.hide();
                // this.toastCtrl.presentToast("Unable to fetch data");
            }
        }, function (err) {
            _this.loadingService.hide();
            console.log(err);
        });
        console.log(result1);
        return result1;
    };
    AppHealthcareproviderEditPage.prototype.comparer = function (otherArray) {
        return function (current) {
            return otherArray.filter(function (other) {
                // console.log(other);
                return other.email == current.email; //&& other.name == current.name
                // return other == current
            }).length == 0;
        };
    };
    AppHealthcareproviderEditPage.prototype.addHealthcareproviderDetails = function () {
        var _this = this;
        var self = this;
        this.submitAttempt = true;
        if (!this.Profileform.valid) {
            console.log("Profileform Validation Fire!");
        }
        else {
            console.log('this.data', this.data);
            this.Profileform.value.id = this.data.id;
            if (this.data.image != undefined) {
                this.data.image = this.data.image;
            }
            else {
                this.showImagePreview();
                this.data.image = "";
            }
            if (this.action === 'edit') {
            }
            else {
                self.data.action = "adddoctortohcp";
                self.data.hcpid = self.data.id;
                self.data.userid = this.userLoginData.id;
                console.log(self.data);
                self.data.usertype = this.userLoginData.usertype;
                self.data.appsecret = this.appsecret;
                self.http.post(this.baseurl + "insertrelationdata.php", self.data).subscribe(function (data) {
                    _this.loadingService.show();
                    var result = JSON.parse(data["_body"]);
                    console.log(result);
                    if (result.status == 'success') {
                        self.loadingService.hide();
                        self.toastCtrl.presentToast("User added successfully");
                        self.navCtrl.setRoot("AppDashboardPage");
                        // return result1;
                    }
                    else {
                        self.loadingService.hide();
                        self.toastCtrl.presentToast("There is a problem adding data, please try again");
                        self.navCtrl.setRoot("AppDashboardPage");
                        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                    }
                }, function (err) {
                    _this.loadingService.hide();
                    console.log(err);
                });
            }
        }
    };
    AppHealthcareproviderEditPage.prototype.random = function () {
        var rand = Math.floor(Math.random() * 2000000) + 1;
        return rand;
    };
    AppHealthcareproviderEditPage.prototype.getDate = function () {
        var date = new Date(this.date);
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        // return date.getDate();
    };
    AppHealthcareproviderEditPage.prototype.ImageUpload = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            // title: 'Image Upload',
            message: 'Select Image source',
            buttons: [
                {
                    text: 'Upload from Library',
                    handler: function () {
                        _this.gallery(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.gallery(_this.camera.PictureSourceType.CAMERA);
                    }
                }, {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    AppHealthcareproviderEditPage.prototype.gallery = function (sourceType) {
        var self = this;
        this.camera.getPicture({
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: sourceType,
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            correctOrientation: true,
            allowEdit: true
        }).then(function (imageData) {
            // imageData is a base64 encoded string
            self.showImagePreview();
            self.data.image = "data:image/jpeg;base64," + imageData;
            self.image = "data:image/jpeg;base64," + imageData;
        }, function (err) {
            console.log("Error " + err);
        });
    };
    AppHealthcareproviderEditPage.prototype.showImagePreview = function () {
        var self = this;
        self.setVisibility(self.previewImage, "block");
        self.setVisibility(self.imageUploader, "none");
        self.setVisibility(self.addPhotoCaption, "none");
        self.setVisibility(self.removePhotoIcon, "block");
    };
    AppHealthcareproviderEditPage.prototype.removeImage = function () {
        var self = this;
        this.data.image = undefined;
        self.setVisibility(self.previewImage, "none");
        self.setVisibility(self.imageUploader, "block");
        self.setVisibility(self.addPhotoCaption, "block");
        self.setVisibility(self.removePhotoIcon, "none");
    };
    AppHealthcareproviderEditPage.prototype.setVisibility = function (element, state) {
        if (element !== undefined)
            element.nativeElement.style.display = state;
    };
    AppHealthcareproviderEditPage.prototype.logout = function () {
        this.databaseService.logout();
    };
    AppHealthcareproviderEditPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('userLogin').then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppHealthcareproviderEditPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("this.userdata", this.userLoginData);
                        return [4 /*yield*/, this.get('patientType').then(function (result) {
                                console.log("patienttypesdfdsfsdsdf", result);
                                if ((!result || result == null || result == undefined) && _this.userLoginData == "patient") {
                                    _this.doRefresh();
                                }
                                else {
                                    _this.patientType = result;
                                    _this.intializeApp1();
                                    console.log("sekeccdffvfdgdfgdfg", _this.patientType);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppHealthcareproviderEditPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_1 = _a.sent();
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppHealthcareproviderEditPage.prototype.doRefresh = function () {
        var _this = this;
        setTimeout(function () {
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            // event.target.complete();
        }, 2000);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('imageUploader'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppHealthcareproviderEditPage.prototype, "imageUploader", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('addPhotoCaption'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppHealthcareproviderEditPage.prototype, "addPhotoCaption", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('removePhotoIcon'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppHealthcareproviderEditPage.prototype, "removePhotoIcon", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('previewImage'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppHealthcareproviderEditPage.prototype, "previewImage", void 0);
    AppHealthcareproviderEditPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-healthcareprovider-edit',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-healthcareprovider-edit/app-healthcareprovider-edit.html"*/'\n<ion-header>\n\n  <ion-navbar>\n      <ion-title *ngIf="!this.action" text-center>Add Healthcare Provider</ion-title>\n      <ion-title *ngIf="this.action" text-center>Edit Healthcare Provider Profile</ion-title>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n      </button>\n      <!-- <ion-buttons right (click)="logout()">\n          <button ion-button icon-only><ion-icon name="lock">Logout</ion-icon></button>\n      </ion-buttons> -->\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n  <ion-grid padding >\n      <ion-row>\n          <ion-col>\n              <div *ngIf="!this.action" class="app-description">\n                  Please fill all the info below\n              </div>\n              <div *ngIf="this.action" class="app-description">\n                  You can edit or fill all info below\n              </div>\n          </ion-col>\n      </ion-row>\n      <form [formGroup]="Profileform">\n          <ion-row wrap padding>\n              <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 padding>\n                  <ion-row>\n                      <ion-col col-2></ion-col>\n                      <ion-col col-8>\n                          <div no-padding transparent no-lines text-center class="item-width" >\n                              <div *ngIf="!data.image" padding style="background: #39415b; color:#444; border-radius: 10px">\n                                  <div #imageUploader><img style="width:30%;" [src]="upload"  (click)="!readonly && ImageUpload()"></div>\n                                  <div #addPhotoCaption style="color:#fff;">Add Healthcare Providers Photo</div>\n                              </div>\n                              <div #previewImage> <img style="border-radius:20px" [src]="data.image" *ngIf="data.image" ></div>\n                              <div #removePhotoIcon style="margin:auto;width: 10%;display: none">\n                                  <ion-icon name="close-circle"  (click)="!readonly && removeImage()"></ion-icon>\n                              </div>\n\n                          </div>\n                      </ion-col>\n                      <ion-col col-2></ion-col>\n                  </ion-row>\n                  <ion-item padding transparent class="item-width">\n                      <ion-label stacked >\n                          User Type\n                          <!-- <span style="color: #e0675c;margin-left: 7px;">*</span> -->\n                      </ion-label>\n                      <ion-input formControlName="usertype" style="color:#666 !important;" type="text" readonly [(ngModel)]="data.usertype" ></ion-input>\n                  </ion-item>\n                  <ion-item *ngIf="addexist" class="item-width" style="background:none;">\n                    <ion-label>Select Healthcare Provider</ion-label>\n                    <ion-select formControlName="storedDoctors" [(ngModel)]="data.storedDoctors" (ionChange)="checkState($event)">\n                      <ion-option *ngFor="let key of data1.newDoctors[0];" value="{{key.id}}">{{key.firstname}}</ion-option>\n                    </ion-select>\n                  </ion-item>\n                  <ion-item padding transparent class="item-width">\n                      <ion-label stacked >\n                          Name\n                          <span style="color: #e0675c;margin-left: 7px;">*</span>\n                      </ion-label>\n                      <ion-input formControlName="name" type="text" placeholder="Type your name here" [(ngModel)]="data.name" disabled="{{enableDiv}}"></ion-input>\n                  </ion-item>\n                  <p *ngIf="!Profileform.controls.name.valid && (Profileform.controls.name.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please Enter Name</p>\n                  <ion-item padding transparent class="item-width">\n                      <ion-label stacked >\n                          Establishment Date\n                          <!-- <span style="color: #e0675c;margin-left: 7px;">*</span> -->\n                      </ion-label>\n                      <ion-datetime  formControlName="birthDate" placeholder="DD/MM/YYYY" [(ngModel)]="data.birthdate" displayFormat="DD/MM/YYYY" style="margin:8px;padding:0px" disabled="{{enableDiv}}"></ion-datetime>\n                  </ion-item>\n                  <p *ngIf="!Profileform.controls.birthDate.valid && (Profileform.controls.birthDate.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please Select Birthdate</p>\n                </ion-col>\n                <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n                  <ion-item padding transparent class="item-width">\n                      <ion-label stacked >\n                          Email Address\n                          <span  style="color: #e0675c;margin-left: 7px;">*</span>\n                      </ion-label>\n                      <ion-input formControlName="email" placeholder="Select your email address here"  [(ngModel)]="data.email" type="text" disabled="{{enableDiv}}"></ion-input>\n                  </ion-item>\n                  <p *ngIf="!Profileform.controls.email.valid && (Profileform.controls.email.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please enter valid Email</p>\n                  <ion-item padding transparent class="item-width">\n                      <ion-label stacked >\n                          Mobile Number\n                          <span  style="color: #e0675c;margin-left: 7px;">*</span>\n                      </ion-label>\n                      <ion-input formControlName="mobile" placeholder="Enter Mobile Number"  [(ngModel)]="data.mobile" type="text" disabled="{{enableDiv}}"></ion-input>\n                  </ion-item>\n                  <p *ngIf="!Profileform.controls.mobile.valid && (Profileform.controls.mobile.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please Enter Mobile Number</p>\n\n                  <ion-item padding transparent class="item-width">\n                      <ion-label stacked>Full Address\n                        <span  style="color: #e0675c;margin-left: 7px;">*</span>\n                      </ion-label>\n                      <ion-input formControlName="address" placeholder="Street/ Building No/ Zip/ City/ Country" [(ngModel)]="data.street" type="text" disabled="{{enableDiv}}"></ion-input>\n                  </ion-item>\n                  <!-- <img class="location" src="assets/images/map.png" /> -->\n                  <ion-item padding transparent class="item-width">\n                      <ion-label stacked>Description\n                        <!-- <span  style="color: #e0675c;margin-left: 7px;">*</span> -->\n                      </ion-label>\n                      <ion-textarea formControlName="description"  placeholder="Type description here" type="text" [(ngModel)]="data.description" disabled="{{enableDiv}}"></ion-textarea>\n                  </ion-item>\n                  <p *ngIf="!Profileform.controls.description.valid && (Profileform.controls.description.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please Add Description</p>\n                  <div *ngIf="showSaveButton" class="block-insert" margin>\n                      <button (click)="addHealthcareproviderDetails()" class="dark-button" ion-button round>\n                          Save\n                      </button>\n                  </div>\n              </ion-col>\n          </ion-row>\n      </form>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-healthcareprovider-edit/app-healthcareprovider-edit.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_geocoder__["a" /* NativeGeocoder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_10__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_11__services_toast_service__["a" /* ToastService */]])
    ], AppHealthcareproviderEditPage);
    return AppHealthcareproviderEditPage;
}());

//# sourceMappingURL=app-healthcareprovider-edit.js.map

/***/ })

});
//# sourceMappingURL=3.js.map