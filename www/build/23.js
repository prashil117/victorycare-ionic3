webpackJsonp([23],{

/***/ 1010:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPatientsEventMasterModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_app_shared_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__environment_environment__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










/**
 * Generated class for the AppPatientsEventMasterModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppPatientsEventMasterModalPage = (function () {
    function AppPatientsEventMasterModalPage(navCtrl, navParams, alertCtrl, databaseService, loadingService, modalCtrl, viewCtrl, appSharedService, storage, http, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.appSharedService = appSharedService;
        this.storage = storage;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.colors = ['#E26060', '#8E24AA', '#6BB1D6', '#00897B', '#AFDB07', '#C0CA33', '#FAD000', '#FB8C00'];
        this.data = {};
        // colors:any = [];
        this.events = [];
        this.patientData = [];
        this.eventType = {};
        this.itemIndex = {};
        this.baseurl = __WEBPACK_IMPORTED_MODULE_9__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_9__environment_environment__["a" /* environment */].appsecret;
        this.date = new Date();
        this.eventList = [];
        this.patientAction = navParams.data.patientaction ? navParams.data.patientaction : '';
        this.eventdata = navParams.data.eventdata ? navParams.data.eventdata : '';
        console.log("this", this.patientAction);
        if (!__WEBPACK_IMPORTED_MODULE_4_lodash__["isEmpty"](navParams.data)) {
            if (navParams.data.type === 'edit') {
                this.storage.get('patientSession').then(function (val) {
                    if (val != undefined)
                        _this.patientSession = true;
                });
                this.storage.get('user').then(function (val) {
                    _this.patientData = val;
                });
                var self_1 = this;
                this.Pagetitle = 'Edit Current Status of Well Being';
                this.data = navParams.data.event;
                this.eventType = navParams.data.type;
                this.itemIndex = navParams.data.index;
                this.patientData = navParams.data.patientData;
                this.Eventmaster = "";
                if (this.data.color) {
                    this.data.color = this.data.color;
                }
                else {
                    this.data.color = '#E26060';
                }
            }
            else {
                this.Pagetitle = 'Add Current Status of Well Being';
                this.eventType = '';
                this.events = navParams.data.events;
                this.patientData = navParams.data.patientData;
                this.Eventmaster = navParams.data.Eventmaster;
                this.data.color = '#E26060';
            }
        }
        else {
            this.data.color = '#E26060';
        }
        // this.colors = this.appSharedService.colors;
    }
    // for choose color and set in dropdown
    AppPatientsEventMasterModalPage.prototype.prepareColorSelector = function () {
        var _this = this;
        setTimeout(function () {
            var buttonElements = document.querySelectorAll('div.alert-radio-group button');
            if (!buttonElements.length) {
                _this.prepareColorSelector();
            }
            else {
                for (var index = 0; index < buttonElements.length; index++) {
                    var buttonElement = buttonElements[index];
                    var optionLabelElement = buttonElement.querySelector('.alert-radio-label');
                    var color = optionLabelElement.innerHTML.trim();
                    if (_this.isHexColor(color)) {
                        buttonElement.classList.add('colorselect', 'color_' + color.slice(1, 7));
                        if (color == _this.data.color) {
                            buttonElement.classList.add('colorselected');
                        }
                    }
                }
            }
        }, 100);
    };
    // for choose color and set in dropdown
    AppPatientsEventMasterModalPage.prototype.isHexColor = function (color) {
        var hexColorRegEx = /^#(?:[0-9a-fA-F]{3}){1,2}$/;
        return hexColorRegEx.test(color);
    };
    // for choose color and set in dropdown
    AppPatientsEventMasterModalPage.prototype.selectColor = function (color) {
        var buttonElements = document.querySelectorAll('div.alert-radio-group button.colorselect');
        for (var index = 0; index < buttonElements.length; index++) {
            var buttonElement = buttonElements[index];
            buttonElement.classList.remove('colorselected');
            if (buttonElement.classList.contains('color_' + color.slice(1, 7))) {
                buttonElement.classList.add('colorselected');
            }
        }
    };
    // for choose color and set in dropdown
    AppPatientsEventMasterModalPage.prototype.setColor = function (color) {
        console.log('Selected Color is', color);
    };
    AppPatientsEventMasterModalPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    AppPatientsEventMasterModalPage.prototype.saveEvent = function () {
        // console.log(this.events);
        // console.log(this.data);
        var self = this;
        if (!__WEBPACK_IMPORTED_MODULE_4_lodash__["isUndefined"](this.data.name) && !__WEBPACK_IMPORTED_MODULE_4_lodash__["isUndefined"](this.data.color) && !__WEBPACK_IMPORTED_MODULE_4_lodash__["isUndefined"](this.data.description)) {
            console.log(self.data);
            if (this.userLoginData.usertype == "healthcareprovider") {
                this.data.userid = this.userLoginData.id;
            }
            else {
                this.data.userid = this.hcpid;
            }
            console.log('activity data -->', this.data);
            self.data.action = "addevent";
            self.data.doctorid = this.userLoginData.id;
            self.data.appsecret = this.appsecret;
            self.http.post(this.baseurl + "adddata.php", self.data).subscribe(function (data) {
                var result = JSON.parse(data["_body"]);
                console.log(result);
                if (result.status == 'success') {
                    self.loadingService.hide();
                    self.toastCtrl.presentToast("Event added successfully");
                    self.navCtrl.setRoot("AppEventMasterListPage");
                    // return result1;
                }
                else {
                    self.loadingService.hide();
                    self.toastCtrl.presentToast("There is a problem adding data, please try again");
                    self.navCtrl.setRoot("AppEventMasterListPage");
                    // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                }
            }, function (err) {
                console.log(err);
            });
        }
        else {
            this.presentAlert('Please enter Event name or choose color');
        }
    };
    AppPatientsEventMasterModalPage.prototype.getDate = function () {
        var date = new Date(this.date);
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        // return date.getDate();
    };
    AppPatientsEventMasterModalPage.prototype.updateEvent = function (patientData, index) {
        console.log(this.data);
        console.log(patientData);
        console.log(index);
        var self = this;
        if (!__WEBPACK_IMPORTED_MODULE_4_lodash__["isUndefined"](this.data.event) && !__WEBPACK_IMPORTED_MODULE_4_lodash__["isUndefined"](this.data.prestartdate) && !__WEBPACK_IMPORTED_MODULE_4_lodash__["isUndefined"](this.data.preenddate)) {
        }
        else {
            this.presentAlert('Please enter all required details');
        }
    };
    AppPatientsEventMasterModalPage.prototype.presentAlert = function (message) {
        var alert = this.alertCtrl.create({
            subTitle: message,
            buttons: ['Dismiss']
        });
        alert.present();
    };
    AppPatientsEventMasterModalPage.prototype.addTime = function () {
        console.log(this.data.date);
        // var newdate = this.data.date;
        this.data.date.push(this.data.date);
    };
    AppPatientsEventMasterModalPage.prototype.deleteTime = function (index) {
        this.data.date.splice(index, 1);
    };
    AppPatientsEventMasterModalPage.prototype.setInitialTime = function () {
        if (this.data.date === undefined) {
            this.data.date = [];
        }
        if (this.data.date.length === 0) {
            this.data.date.push({});
        }
    };
    AppPatientsEventMasterModalPage.prototype.ngAfterViewInit = function () {
        this.setInitialTime();
        this.getHCPid();
    };
    AppPatientsEventMasterModalPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_1 = _a.sent();
                        console.log(reason_1);
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppPatientsEventMasterModalPage.prototype.getHCPid = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('currentHCP').then(function (result) {
                            if (result != null) {
                                _this.hcpid = result;
                                _this.getuserLoginData();
                            }
                            else {
                                _this.getuserLoginData();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPatientsEventMasterModalPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('userLogin').then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.hcpid = _this.hcpid ? _this.hcpid : _this.userLoginData.id;
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPatientsEventMasterModalPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("this.userdata", this.userLoginData);
                        return [4 /*yield*/, this.get('patientType').then(function (result) {
                                console.log("patienttypesdfdsfsdsdf", result);
                                if ((!result || result == null || result == undefined) && _this.userLoginData == "patient") {
                                    _this.doRefresh('');
                                }
                                else {
                                    _this.patientType = result;
                                    console.log("sekeccdffvfdgdfgdfg", _this.patientType);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPatientsEventMasterModalPage.prototype.doRefresh = function (event) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            // event.target.complete();
        }, 2000);
    };
    AppPatientsEventMasterModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-patients-event-master-modal',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patients-event-master-modal/app-patients-event-master-modal.html"*/'\n<ion-header>\n    <ion-navbar>\n        <ion-title text-center>{{Pagetitle}}</ion-title>\n        <ion-buttons right>\n            <button ion-button icon-only (click)="closeModal()">\n                <ion-icon name="close"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n    <ion-grid padding>\n        <ion-row padding>\n            <ion-col style="margin-left: 200px;max-width: 600px;" col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12>\n                <ion-item style="margin-bottom: -23px;" padding transparent class="item-width">\n                    <ion-label stacked>Name of Current Status of Well Being</ion-label>\n                    <ion-input style="color:white" required type="text"\n                        placeholder="e.g. awake/sleep/dizzy/uncomfortable/in pain" [(ngModel)]="data.name"></ion-input>\n                </ion-item>\n                <ion-row class="colorselect color_{{data.color.slice(1,7)}}">\n                    <ion-item style="margin-bottom: -23px;" padding class="item-width" transparent>\n                        <ion-label class="primarylabel">Choose Color</ion-label>\n                        <ion-select (click)="prepareColorSelector()" (ionChange)="setColor(data.color)"\n                            [(ngModel)]="data.color">\n                            <ion-option (ionSelect)="selectColor(optioncolor)" *ngFor="let optioncolor of colors"\n                                [value]="optioncolor">{{optioncolor}}</ion-option>\n                        </ion-select>\n                    </ion-item>\n                </ion-row>\n                <ion-row>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>Description</ion-label>\n                        <ion-textarea placeholder="Type description here" style="color:#fff;" class="item-input"\n                            type="text" [(ngModel)]="data.description"></ion-textarea>\n                    </ion-item>\n                </ion-row>\n                <ion-item padding transparent class="item-width" style="width: 430px;margin-bottom: -30px;" no-lines>\n                    <ion-label style="color: white;">Add to Standard Wheel</ion-label>\n                    <ion-toggle style="color:#fff;" [(ngModel)]="data.addtowheel">\n                    </ion-toggle>\n                </ion-item>\n                <ion-item style="width: 430px;" padding transparent class="item-width" no-lines>\n                    <ion-label style="color: white;">Notification</ion-label>\n                    <ion-toggle style="color:#fff;" [(ngModel)]="data.notification">\n                    </ion-toggle>\n                </ion-item>\n                <div text-center class="block-insert" margin>\n                    <button (click)="saveEvent()" class="dark-button" ion-button round>Save</button>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patients-event-master-modal/app-patients-event-master-modal.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["F" /* ViewController */], __WEBPACK_IMPORTED_MODULE_5__services_app_shared_service__["a" /* AppSharedService */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_8__services_toast_service__["a" /* ToastService */]])
    ], AppPatientsEventMasterModalPage);
    return AppPatientsEventMasterModalPage;
}());

//# sourceMappingURL=app-patients-event-master-modal.js.map

/***/ }),

/***/ 956:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppPatientsEventMasterModalPageModule", function() { return AppPatientsEventMasterModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_patients_event_master_modal__ = __webpack_require__(1010);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppPatientsEventMasterModalPageModule = (function () {
    function AppPatientsEventMasterModalPageModule() {
    }
    AppPatientsEventMasterModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_patients_event_master_modal__["a" /* AppPatientsEventMasterModalPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_patients_event_master_modal__["a" /* AppPatientsEventMasterModalPage */]),
            ],
        })
    ], AppPatientsEventMasterModalPageModule);
    return AppPatientsEventMasterModalPageModule;
}());

//# sourceMappingURL=app-patients-event-master-modal.module.js.map

/***/ })

});
//# sourceMappingURL=23.js.map