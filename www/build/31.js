webpackJsonp([31],{

/***/ 1000:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppNotificationSettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environment_environment__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_open_native_settings__ = __webpack_require__(577);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var AppNotificationSettingsPage = (function () {
    function AppNotificationSettingsPage(openNativeSettings, toastServie, loadingService, http, navCtrl, navParams, storage) {
        this.openNativeSettings = openNativeSettings;
        this.toastServie = toastServie;
        this.loadingService = loadingService;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.baseurl = __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].appsecret;
        this.timeOption = [
            { key: '+0 minutes', value: 'on time' },
            { key: '+15 minutes', value: 'Default (15 minutes)' },
            { key: '+30 minutes', value: '30 minutes' },
            { key: '+45 minutes', value: '45 minutes' },
            { key: '+1 hour', value: '1 hour' }
        ];
    }
    AppNotificationSettingsPage.prototype.ionViewDidLoad = function () {
    };
    AppNotificationSettingsPage.prototype.appIntialize = function () {
        var _this = this;
        var data = {
            userid: this.userLoginData.id,
            appsecret: this.appsecret,
            action: 'getNotificationSettings',
        };
        this.http.post(this.baseurl + 'notificationsettings.php', data).subscribe(function (data) {
            var result = JSON.parse(data['_body']);
            console.log("result", result);
            _this.eventtime = result.data.event_time;
            _this.activitytime = result.data.activity_time;
            _this.medicinetime = result.data.medicine_time;
            _this.isNotification = result.data.isNotification ? result.data.isNotification : 0;
            _this.isStatistical = result.data.isStatistical ? result.data.isStatistical : 0;
            _this.isActivity = result.data.isActivity ? result.data.isActivity : 0;
            _this.isAccept = result.data.isAccept ? result.data.isAccept : 0;
        }), function (err) {
            console.log("error", err);
        };
    };
    AppNotificationSettingsPage.prototype.updateNotificationTime = function () {
        var _this = this;
        this.loadingService.show();
        var time = [
            { eventTime: this.eventtime },
            { activityTime: this.activitytime },
            { medicineTime: this.medicinetime },
        ];
        var data = {
            userid: this.userLoginData.id,
            appsecret: this.appsecret,
            activitytime: this.activitytime,
            medicinetime: this.medicinetime,
            eventtime: this.eventtime,
            isNotification: this.isNotification,
            isActivity: this.isActivity,
            isStatistical: this.isStatistical,
            isAccept: this.isAccept,
            action: 'timesettings',
        };
        this.openNativeSettings.open("application_details").then(function (val) {
            console.log('success', val);
        });
        this.http.post(this.baseurl + 'notificationsettings.php', data).subscribe(function (data) {
            _this.loadingService.hide();
            var result = JSON.parse(data['_body']);
            if (result.status == "success") {
                _this.toastServie.presentToast('Data saved successfully');
            }
        }), function (err) {
            _this.loadingService.hide();
            console.log("error : ", err);
        };
    };
    AppNotificationSettingsPage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        console.log("set string in storage: " + result);
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        console.log(reason_1);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppNotificationSettingsPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        else {
                            return [2 /*return*/, null];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        reason_2 = _a.sent();
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppNotificationSettingsPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("patientType").then(function (result) {
                            if (result == "independent") {
                                _this.dependent = false;
                            }
                            _this.appIntialize();
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppNotificationSettingsPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("userLogin").then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppNotificationSettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-notification-settings',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-notification-settings/app-notification-settings.html"*/'<!--\n  Generated template for the AppNotificationSettingsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Notifications</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <div style="margin: 0px auto;width:500px;">\n    <div>\n      <!-- <div style="position: relative;">\n        <p style="float: left;color: white;font-size: 14px;left: 16px;position: relative;top: 20px;">Apply Notifications\n          to your device?</p>\n        <div style="display: inline-flex;margin-bottom: -25px;margin-top: -25px;">\n          <ion-list radio-group [(ngModel)]="isNotification" style="display: -webkit-box;position: relative;top: 60px;right: 267px;">\n            <ion-item no-lines>\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="1">\n              </ion-radio>\n              <ion-label style="color:white;position: relative;left:45px;">Yes</ion-label>\n            </ion-item>\n            <ion-item no-lines style="right: 100px;">\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="0"></ion-radio>\n              <ion-label style="color:white;position: relative;left: 45px;">No</ion-label>\n            </ion-item>\n          </ion-list>\n        </div>\n      </div> -->\n      <br><br>\n      <div style="position: relative;top:25px">\n        <p style="float: left;color: white;font-size: 14px;left: 16px;position: relative;top: 20px;">Apply Notifications\n          to your device?</p>\n        \n          <ion-list radio-group [(ngModel)]="isNotification" style="display: -webkit-inline-box;margin-right: 280px;">\n            <ion-item no-lines>\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="1">\n              </ion-radio>\n              <ion-label style="color:white;position: relative;left:45px;">Yes</ion-label>\n            </ion-item>\n            <ion-item no-lines style="right: 100px;">\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="0"></ion-radio>\n              <ion-label style="color:white;position: relative;left: 45px;">No</ion-label>\n            </ion-item>\n          </ion-list>\n      </div>\n      <div style="margin-top: -25px;">\n        <p style="float: left;color: white;font-size: 14px;left: 16px;position: relative;top: 30px;">Do you want to\n          recieve a notification when the medicine are</p>\n        <p style="float: left;color: white;font-size: 16px;left: 16px;position: relative;top: 2px;">used to anonomously\n          for statistical analysis own data?</p>\n          <ion-list radio-group [(ngModel)]="isStatistical" style="display: -webkit-inline-box;margin-right: 280px;margin-top: -25px !important;margin-bottom: 30px !important;">\n            <ion-item no-lines>\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="1">\n              </ion-radio>\n              <ion-label style="color:white;position: relative;left:45px;">Yes</ion-label>\n            </ion-item>\n            <ion-item no-lines style="right: 100px;">\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="0"></ion-radio>\n              <ion-label style="color:white;position: relative;left: 45px;">No</ion-label>\n            </ion-item>\n          </ion-list>\n      </div>\n      <!-- <div style="position: relative;margin-top:-10px">\n        <p style="float: left;color: white;font-size: 14px;left: 16px;position: relative;top: -5px;">Some\n          notification\n          will be here about accept your data\n          Activity </p>\n        <div style="display: inline-flex;">\n          <ion-list radio-group [(ngModel)]="isAccept" style="display: -webkit-inline-box;position: relative;right: 140px;top:-35px">\n            <ion-item no-lines>\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="1">\n              </ion-radio>\n              <ion-label style="color:white;position: relative;left:45px;">Yes</ion-label>\n            </ion-item>\n            <ion-item no-lines style="right: 100px;">\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="0"></ion-radio>\n              <ion-label style="color:white;position: relative;left: 45px;">No</ion-label>\n            </ion-item>\n          </ion-list>\n        </div>\n      </div> -->\n    </div>\n    <div style="margin-top: -40px;margin-bottom: 40px;">\n      <ion-item style="margin-bottom: 20px;height: 0px;">\n        <ion-label style="color:white;">Medicine Notification Frequence</ion-label>\n        <ion-select style="color:white" [(ngModel)]="medicinetime">\n          <ion-option style="color:white" *ngFor="let time of timeOption" [value]="time.key">{{time.value}}\n          </ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item style="margin-bottom: 20px;height: 0px;">\n        <ion-label style="color:white;">Activity Notification Frequence</ion-label>\n        <ion-select style="color:white" [(ngModel)]="activitytime">\n          <ion-option style="color:white" *ngFor="let time of timeOption" [value]="time.key">{{time.value}}\n          </ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item style="margin-bottom: 20px;height: 0px;">\n        <ion-label style="color:white;">Well-Being Notification Frequence</ion-label>\n        <ion-select style="color:white" [(ngModel)]="eventtime">\n          <ion-option style="color:white" *ngFor="let time of timeOption" [value]="time.key">{{time.value}}\n          </ion-option>\n        </ion-select>\n      </ion-item>\n    </div>\n  </div>\n  <button class="btn" (click)="updateNotificationTime();" ion-button round>Save</button>\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-notification-settings/app-notification-settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__ionic_native_open_native_settings__["a" /* OpenNativeSettings */], __WEBPACK_IMPORTED_MODULE_6__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_5__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], AppNotificationSettingsPage);
    return AppNotificationSettingsPage;
}());

//# sourceMappingURL=app-notification-settings.js.map

/***/ }),

/***/ 946:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppNotificationSettingsPageModule", function() { return AppNotificationSettingsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_notification_settings__ = __webpack_require__(1000);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppNotificationSettingsPageModule = (function () {
    function AppNotificationSettingsPageModule() {
    }
    AppNotificationSettingsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_notification_settings__["a" /* AppNotificationSettingsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_notification_settings__["a" /* AppNotificationSettingsPage */]),
            ],
        })
    ], AppNotificationSettingsPageModule);
    return AppNotificationSettingsPageModule;
}());

//# sourceMappingURL=app-notification-settings.module.js.map

/***/ })

});
//# sourceMappingURL=31.js.map