webpackJsonp([25],{

/***/ 1008:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPatientSettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_app_shared_service__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AppPatientSettingsPage = (function () {
    function AppPatientSettingsPage(navCtrl, navParams, events, menu, databaseService, alertCtrl, loadingService, appSharedService) {
        this.navCtrl = navCtrl;
        this.events = events;
        this.menu = menu;
        this.databaseService = databaseService;
        this.alertCtrl = alertCtrl;
        this.loadingService = loadingService;
        this.appSharedService = appSharedService;
        this.data = {};
        this.patientData = {};
        this.tabs = [];
        this.eventData = [];
        if (!__WEBPACK_IMPORTED_MODULE_4_lodash__["isEmpty"](navParams.data)) {
            this.patientData = navParams.data.data;
            this.prepopulated = navParams.data.addprepopulated;
            console.log('this.patientData setting', this.patientData);
            console.log('this.prepopulated', this.prepopulated);
            if (this.prepopulated == 'yes') {
                this.eventData = [
                    { "color": '#AFDB07', 'name': 'Awake' },
                    { "color": '#6BB1D6', 'name': 'Sleeping' },
                    { "color": '#FAD000', 'name': 'Uncomfortable' },
                    { "color": '#E26060', 'name': 'Pain' },
                ];
                console.log('this.eventData', this.eventData);
            }
        }
        this.data.tabs = [
            { page: "AppPatientEventSettingsPage", title: "Add Wellbeing Status", params: this.patientData },
            { page: "AppPatientMedicineListPage", title: "Add Medicine", params: this.patientData },
            { page: "AppPatientActivityListPage", title: "Add Activity", params: this.patientData }
        ];
        //this.events.publish('user:created', this.patientData, Date.now());
    }
    AppPatientSettingsPage.prototype.ionViewDidEnter = function () {
        // this.navBar.backButtonClick = (e: UIEvent) => {
        //     // todo something
        //     this.navCtrl.setRoot("AppPatientsPage");
        // }
        this.menu.enable(false);
        // this.menu.swipeEnable(false);
    };
    AppPatientSettingsPage.prototype.ionViewWillLeave = function () {
        this.menu.enable(true);
    };
    AppPatientSettingsPage.prototype.logout = function () {
        this.databaseService.logout();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("tabs"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["C" /* Tabs */])
    ], AppPatientSettingsPage.prototype, "TimelineTabs", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Navbar */])
    ], AppPatientSettingsPage.prototype, "navBar", void 0);
    AppPatientSettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-patient-settings',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patient-settings/app-patient-settings.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title text-center>Onboarding Settings</ion-title>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-buttons right (click)="logout()">\n            <button ion-button icon-only>\n                <ion-icon name="lock">Logout</ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n<ion-content>\n\n    <ion-grid>\n        <ion-row>\n            <ion-col col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2></ion-col>\n            <ion-col col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 style="display: contents">\n\n                <ion-tabs class="margin-5t" #tabs tabsPlacement=\'top\' tabs-content>\n                    <ion-tab [rootParams]="patientData" [tabTitle]="item.title" [root]="item.page"\n                        *ngFor="let item of data.tabs;let i = index"></ion-tab>\n                </ion-tabs>\n            </ion-col>\n            <ion-col col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2></ion-col>\n\n        </ion-row>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patient-settings/app-patient-settings.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* MenuController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_5__services_app_shared_service__["a" /* AppSharedService */]])
    ], AppPatientSettingsPage);
    return AppPatientSettingsPage;
}());

//# sourceMappingURL=app-patient-settings.js.map

/***/ }),

/***/ 954:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppPatientSettingsModule", function() { return AppPatientSettingsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_patient_settings__ = __webpack_require__(1008);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppPatientSettingsModule = (function () {
    function AppPatientSettingsModule() {
    }
    AppPatientSettingsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_patient_settings__["a" /* AppPatientSettingsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_patient_settings__["a" /* AppPatientSettingsPage */])
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]],
        })
    ], AppPatientSettingsModule);
    return AppPatientSettingsModule;
}());

//# sourceMappingURL=app-patient-settings.module.js.map

/***/ })

});
//# sourceMappingURL=25.js.map