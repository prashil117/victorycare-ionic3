webpackJsonp([50],{

/***/ 923:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddcontectPageModule", function() { return AddcontectPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__addcontect__ = __webpack_require__(977);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddcontectPageModule = (function () {
    function AddcontectPageModule() {
    }
    AddcontectPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__addcontect__["a" /* AddcontectPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__addcontect__["a" /* AddcontectPage */]),
            ],
        })
    ], AddcontectPageModule);
    return AddcontectPageModule;
}());

//# sourceMappingURL=addcontect.module.js.map

/***/ }),

/***/ 977:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddcontectPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AddcontectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddcontectPage = (function () {
    function AddcontectPage(navCtrl, navParams, viewCtrl, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.data = [];
        this.contacts = [];
        this.Contectform = formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            relationship: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            mobile: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            notes: [''],
            address: ['']
        });
        if (!__WEBPACK_IMPORTED_MODULE_3_lodash__["isEmpty"](navParams.data)) {
            this.contacts = navParams.data.contacts;
            this.contactType = navParams.data.type;
        }
        else {
            this.contactType = "";
        }
        this.ContactTitle = navParams.data.type == "masterdata" ? 'Add Contact' : 'Add Next of Kin';
        this.patientData = navParams.data.patientData;
        this.contacts = navParams.data.contectdata;
    }
    AddcontectPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddcontectPage');
    };
    AddcontectPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    AddcontectPage.prototype.save = function () {
        this.submitAttempt = true;
    };
    AddcontectPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-addcontect',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/addcontect/addcontect.html"*/'<!--\n  Generated template for the AddcontectPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n        <ion-title text-center style="font-size:2.4rem;text-transform: none;font-weight: 500" >{{ContactTitle}}</ion-title>\n \n        <ion-buttons right>\n      <button ion-button icon-only (click)="closeModal()"><ion-icon name="close"></ion-icon></button>\n  </ion-buttons>\n    </ion-navbar>\n </ion-header>\n \n <ion-content>\n         <ion-grid padding>\n          <form [formGroup]="Contectform">\n             <ion-row wrap padding>\n                 <ion-col col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12>\n \n                     <ion-item padding transparent class="item-width">\n                             <ion-label stacked >\n                                     Name\n                                     <span style="color: #e0675c;margin-left: 7px;">*</span>\n                              </ion-label>\n                         <ion-input  formControlName="name" required type="text" placeholder="Type your name address here" [(ngModel)]="data.name"></ion-input>\n                     </ion-item>\n                     <p *ngIf="!Contectform.controls.name.valid && (Contectform.controls.name.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please Enter Name</p>\n                \n                     <ion-item padding transparent class="item-width">\n                         <ion-label stacked >\n                                 Relationship\n                                 <span style="color: #e0675c;margin-left: 7px;">*</span>\n                         </ion-label>\n                         <ion-input  formControlName="relationship" required type="text" placeholder="Type here" [(ngModel)]="data.relationship"></ion-input>\n                     </ion-item>\n                     <p *ngIf="!Contectform.controls.relationship.valid && (Contectform.controls.relationship.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please Enter Relationship</p>\n                \n                     <ion-item padding transparent class="item-width">\n                             <ion-label stacked >\n                                    Phone Number\n                                     <span style="color: #e0675c;margin-left: 7px;">*</span>\n                             </ion-label>\n                         <ion-input  formControlName="mobile"  required type="text" placeholder="0-000-000-000" [(ngModel)]="data.phone"></ion-input>\n                     </ion-item>\n                     <p *ngIf="!Contectform.controls.mobile.valid && (Contectform.controls.mobile.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please Enter Mobile Number</p>\n                \n                     <ion-item padding transparent class="item-width">\n                         <ion-label stacked>Full Address</ion-label>\n                         <ion-input  formControlName="address"  placeholder="Street/ Building No/ Zip/ City/ Country" [(ngModel)]="data.address" required type="text" ></ion-input>\n                     </ion-item>\n                     <img class="location" src="assets/images/map.png" />\n                     <ion-item padding transparent class="item-width">\n                         <ion-label stacked>Notes</ion-label>\n                         <ion-input  formControlName="notes"  placeholder="Write a few words about the contact person" [(ngModel)]="data.notes" required type="text" ></ion-input>\n                     </ion-item>\n \n                     <div class="block-insert" margin>\n                         <button (click)="save()" class="dark-button" ion-button round>Save</button>\n                         <!-- <button (click)="SaveContactData()" class="dark-button" ion-button round>Save</button> -->\n                     </div>\n                 </ion-col>\n             </ion-row>\n          </form>\n         </ion-grid>\n </ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/addcontect/addcontect.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["F" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]])
    ], AddcontectPage);
    return AddcontectPage;
}());

//# sourceMappingURL=addcontect.js.map

/***/ })

});
//# sourceMappingURL=50.js.map