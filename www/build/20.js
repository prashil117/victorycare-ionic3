webpackJsonp([20],{

/***/ 1013:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPrivacyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environment_environment__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var AppPrivacyPage = (function () {
    function AppPrivacyPage(storage, toastctrl, http, navCtrl, navParams) {
        this.storage = storage;
        this.toastctrl = toastctrl;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = {};
        this.appsecret = __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].appsecret;
        this.baseurl = __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].apiUrl;
    }
    AppPrivacyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppPrivacyPage');
        this.getuserLoginData();
    };
    AppPrivacyPage.prototype.getPrivacy = function () {
        var _this = this;
        var data1 = {
            userid: this.userLoginData.id,
            appsecret: this.appsecret,
            action: "getprivacy"
        };
        this.http.post(this.baseurl + 'privacy.php', data1).subscribe(function (res) {
            var result = JSON.parse(res['_body']);
            console.log("result", res);
            if (result.status == "success") {
                if (result.data) {
                    _this.data = result.data;
                    _this.oldata = result.data;
                }
            }
            else {
                _this.data.isNotification = 1;
                _this.data.isCopy = 1;
                _this.data.isReport = 1;
                _this.data.isStatistical = 1;
                _this.data.language = "English";
            }
        }, function (err) {
            console.log("error", err);
        });
    };
    AppPrivacyPage.prototype.addupdatePrivacy = function () {
        var _this = this;
        var data1 = {
            userid: this.userLoginData.id,
            appsecret: this.appsecret,
            action: !this.oldata ? "addprivacy" : 'updateprivacy',
            isnotification: this.data.isNotification,
            iscopy: this.data.isCopy,
            isstatistical: this.data.isStatistical,
            isreport: this.data.isReport,
            language: this.data.language
        };
        console.log("data", data1);
        this.http.post(this.baseurl + 'privacy.php', data1).subscribe(function (res) {
            var result = JSON.parse(res['_body']);
            _this.toastctrl.presentToast(result.status + "fully updated");
            _this.getPrivacy();
        });
    };
    AppPrivacyPage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        console.log(reason_1);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // to get a key/value pair
    AppPrivacyPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_2 = _a.sent();
                        console.log(reason_2);
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppPrivacyPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("userLogin").then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.getPrivacy();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPrivacyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-privacy',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-privacy/app-privacy.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Privacy</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <div style="width: 500px;text-align: left;margin: 20px auto;">\n    <!-- <ion-row>\n      <ion-item style="margin-left: -14px;height:50px">\n        <ion-label>Language</ion-label>\n        <ion-select  style="color: white;" readonly [(ngModel)]="data.language" >\n          <ion-option value="English">English</ion-option>\n          <ion-option value="Spanish">Spanish</ion-option>\n          <ion-option value="German">German</ion-option>\n          <ion-option value="French">French</ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-row> -->\n    <!-- <ion-row> \n      <ion-col>\n        <p style="color: white;font-size: 14px;"> Apply Notifications\n          to\n          your device?</p>\n        <div>\n          <ion-list radio-group [(ngModel)]="data.isNotification" name="notification"\n            style="display: -webkit-inline-box;position:relative;margin-top: -25px !important;">\n            <ion-item no-lines>\n              <ion-radio style="position: relative;right:130px;" class="radio radio-md" slot="start" value="1">\n              </ion-radio>\n              <ion-label style="color:white;position: relative;left:45px;">Yes</ion-label>\n            </ion-item>\n            <ion-item no-lines style="right: 100px;">\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="0"></ion-radio>\n              <ion-label style="color:white;position: relative;left: 45px;">No</ion-label>\n            </ion-item>\n          </ion-list>\n        </div>\n      </ion-col>\n    </ion-row> -->\n    <ion-row style="margin-top: -10px;">\n      <ion-col>\n        <p style="color: white;font-size: 14px;">Do you accept your data will be used for data analysis to provide you with personalized reporting?</p>\n        <div style="display: inline-flex;margin-bottom: -25px;margin-top: -25px;">\n          <ion-list radio-group [(ngModel)]="data.isReport" name="report" style="display: -webkit-inline-box;">\n            <ion-item no-lines>\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="1">\n              </ion-radio>\n              <ion-label style="color:white;position: relative;left:45px;">Yes</ion-label>\n            </ion-item>\n            <ion-item no-lines style="right: 100px;">\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="0"></ion-radio>\n              <ion-label style="color:white;position: relative;left: 45px;">No</ion-label>\n            </ion-item>\n          </ion-list>\n        </div>\n      </ion-col>\n    </ion-row>\n    <!-- <ion-row style="margin-top: -10px;">\n      <ion-col>\n        <p style="color: white;font-size: 14px;">Do you accept your\n          data\n          is used anonomously for statistical analysis improving\n          the\n          application- and offering you as an\n          end users with\n          comparible data analysis to your own data?</p>\n        <div style="display: inline-flex;margin-bottom: -25px;margin-top: -25px;">\n          <ion-list radio-group [(ngModel)]="data.isStatistical" name="statistical"\n            style="display: -webkit-inline-box;">\n            <ion-item no-lines>\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="1">\n              </ion-radio>\n              <ion-label style="color:white;position: relative;left:45px;">Yes</ion-label>\n            </ion-item>\n            <ion-item no-lines style="right: 100px;">\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="0"></ion-radio>\n              <ion-label style="color:white;position: relative;left: 45px;">No</ion-label>\n            </ion-item>\n          </ion-list>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row style="margin-top: -10px;">\n      <ion-col>\n        <p style="color: white;font-size: 14px;">Do you accept to\n          provide data providers a copy of your data as part\n          of a big dataset for analysis for different purposes,\n          for a fee, based\n          on the condition that you approve each instance its\n          applicable\n          and\n          decide which data who decide to share? Data shared under this agreement is for one purpose/one time only, and\n          cannot be used for other types\n          of analyses without your specific consent.</p>\n        <div style="display: inline-flex;margin-top: -25px;">\n          <ion-list radio-group [(ngModel)]="data.isCopy" name="copy" style="display: -webkit-inline-box;">\n            <ion-item no-lines>\n              <ion-radio style="right: 150px;position: relative;right:130px;" class="radio radio-md" slot="start"\n                value="1">\n              </ion-radio>\n              <ion-label style="color:white;position: relative;left:45px;">Yes</ion-label>\n            </ion-item>\n            <ion-item no-lines style="right: 100px;">\n              <ion-radio checked style="right: 150px;position: relative;right:130px;" class="radio radio-md"\n                slot="start" value="0"></ion-radio>\n              <ion-label style="color:white;position: relative;left: 45px;">No</ion-label>\n            </ion-item>\n          </ion-list>\n        </div>\n      </ion-col>\n    </ion-row> -->\n    <button (click)="addupdatePrivacy()" class="btn" ion-button round>Save</button>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-privacy/app-privacy.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */]])
    ], AppPrivacyPage);
    return AppPrivacyPage;
}());

//# sourceMappingURL=app-privacy.js.map

/***/ }),

/***/ 959:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppPrivacyPageModule", function() { return AppPrivacyPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_privacy__ = __webpack_require__(1013);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppPrivacyPageModule = (function () {
    function AppPrivacyPageModule() {
    }
    AppPrivacyPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_privacy__["a" /* AppPrivacyPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_privacy__["a" /* AppPrivacyPage */]),
            ],
        })
    ], AppPrivacyPageModule);
    return AppPrivacyPageModule;
}());

//# sourceMappingURL=app-privacy.module.js.map

/***/ })

});
//# sourceMappingURL=20.js.map