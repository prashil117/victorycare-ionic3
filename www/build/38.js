webpackJsonp([38],{

/***/ 937:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppLoggedGeneralTabModule", function() { return AppLoggedGeneralTabModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_logged_general_tab__ = __webpack_require__(991);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppLoggedGeneralTabModule = (function () {
    function AppLoggedGeneralTabModule() {
    }
    AppLoggedGeneralTabModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_logged_general_tab__["a" /* AppLoggedGeneralTabPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_logged_general_tab__["a" /* AppLoggedGeneralTabPage */])
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]],
        })
    ], AppLoggedGeneralTabModule);
    return AppLoggedGeneralTabModule;
}());

//# sourceMappingURL=app-logged-general-tab.module.js.map

/***/ }),

/***/ 991:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppLoggedGeneralTabPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_app_shared_service__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppLoggedGeneralTabPage = (function () {
    function AppLoggedGeneralTabPage(navCtrl, navParams, alertCtrl, databaseService, loadingService, modalCtrl, appSharedService) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.modalCtrl = modalCtrl;
        this.appSharedService = appSharedService;
        this.patientData = {};
        this.patientSettings = {};
        this.data = {};
        this.selectedDate = {};
        this.selectedTime = {};
        this.readOnly = true;
        this.eventStartDate = {};
        this.eventEndDate = {};
        var self = this;
        if (navParams.data !== undefined) {
            this.selectedDate = navParams.data.selectedDate;
            this.selectedTime = navParams.data.selectedTime;
        }
        if (this.appSharedService.patientData !== undefined) {
            this.patientData = this.appSharedService.patientData;
        }
        if (this.appSharedService.patientSettings !== undefined) {
            this.patientSettings = this.appSharedService.patientSettings;
        }
    }
    AppLoggedGeneralTabPage.prototype.updateLoggedGeneralDetails = function () {
        var self = this;
        console.log(this.data);
    };
    AppLoggedGeneralTabPage.prototype.getSlot = function (interval) {
        var selectedHour = this.selectedTime.split(':')[0];
        var selectedMinutes = this.selectedTime.split(':')[1];
        return selectedHour * (60 / interval) + (selectedMinutes % interval === 0 ? (selectedMinutes / interval) + 1 : (selectedMinutes / interval) - ((selectedMinutes / interval) % 1) + 1);
    };
    AppLoggedGeneralTabPage.prototype.getDate = function () {
        var date = new Date(this.selectedDate);
        return date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        //   return (new Date(this.selectedDate)).toLocaleDateString().split('/').join('-');
    };
    AppLoggedGeneralTabPage.prototype.makeEditable = function () {
        this.readOnly = false;
    };
    AppLoggedGeneralTabPage.prototype.setStartAndEndDate = function (interval) {
        var selectedTime = this.selectedTime.split(':');
        var date = new Date(this.selectedDate);
        date.setHours(selectedTime[0]);
        date.setMinutes(selectedTime[1]);
        var startDate = new Date(date);
        var endDate = new Date(date);
        startDate.setMinutes(startDate.getMinutes() - (selectedTime[1] % interval));
        endDate.setMinutes(startDate.getMinutes() + (interval - (selectedTime[1] % interval)));
        this.eventStartDate = startDate.toString();
        this.eventEndDate = endDate.toString();
    };
    AppLoggedGeneralTabPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-logged-general-tab',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-logged-general-tab/app-logged-general-tab.html"*/'\n<ion-content style="background: black;height:100vh;color:#fff">\n\n\n   <ion-grid padding>\n      <ion-row wrap>\n         <ion-col col-8 offset-2 col-sm-8  offset-sm-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2>\n            <ion-item no-lines style="background-color: #000;margin-bottom: 15px;padding:10px">\n               <ion-label floating>Event Start Date</ion-label>\n               <ion-input value="{{eventStartDate}}" [disabled]="true" required type="text" ></ion-input>\n            </ion-item>\n\n            <ion-item no-lines style="background-color: #000;margin-bottom: 15px;padding:10px">\n               <ion-label floating>Event End Date</ion-label>\n               <ion-input value="{{eventEndDate}}"  [disabled]="true" required type="text" ></ion-input>\n            </ion-item>\n\n            <ion-item no-lines style="background-color: #000;margin-bottom: 15px;padding:10px">\n               <ion-label floating>Event</ion-label>\n               <ion-select [disabled]="readOnly" [(ngModel)]="data.selectedEventKey">\n                  <ion-option value="{{i}}" *ngFor="let item of patientSettings.eventData;let i = index" >{{item.name}}</ion-option>\n               </ion-select>\n            </ion-item>\n\n            <ion-item no-lines style="background-color: #000;margin-bottom: 15px;padding:10px">\n               <ion-label floating>Event Notes</ion-label>\n               <ion-input [disabled]="readOnly" [(ngModel)]="data[\'eventNotes\']" required type="text" ></ion-input>\n            </ion-item>\n\n\n           <!-- <ion-item no-lines style="background-color: #000;margin-bottom: 15px;padding:10px">\n               <ion-label floating>Activity</ion-label>\n               <ion-select [disabled]="readOnly" [(ngModel)]="data.selectedActivityKey">\n                  <ion-option value="{{i}}" *ngFor="let item of patientSettings.activityData;let i = index" >{{item.name}}</ion-option>\n               </ion-select>\n            </ion-item>\n            -->\n            <div style="margin-top: 20px">\n               <button default-button block round ion-button *ngIf="!readOnly" (click)="updateLoggedGeneralDetails()" ><ion-icon name="add-circle"></ion-icon>Update</button>\n               <button default-button block round ion-button *ngIf="readOnly"  (click)="makeEditable()" ><ion-icon name="add-circle"></ion-icon>Edit</button>\n\n            </div>\n         </ion-col>\n      </ion-row>\n   </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-logged-general-tab/app-logged-general-tab.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */], __WEBPACK_IMPORTED_MODULE_4__services_app_shared_service__["a" /* AppSharedService */]])
    ], AppLoggedGeneralTabPage);
    return AppLoggedGeneralTabPage;
}());

//# sourceMappingURL=app-logged-general-tab.js.map

/***/ })

});
//# sourceMappingURL=38.js.map