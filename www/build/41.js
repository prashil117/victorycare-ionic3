webpackJsonp([41],{

/***/ 934:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppLibraryPageModule", function() { return AppLibraryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_library__ = __webpack_require__(988);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppLibraryPageModule = (function () {
    function AppLibraryPageModule() {
    }
    AppLibraryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_library__["a" /* AppLibraryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_library__["a" /* AppLibraryPage */]),
            ],
        })
    ], AppLibraryPageModule);
    return AppLibraryPageModule;
}());

//# sourceMappingURL=app-library.module.js.map

/***/ }),

/***/ 988:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppLibraryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_img_viewer__ = __webpack_require__(573);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__ = __webpack_require__(574);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_app_component__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__environment_environment__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_ngx__ = __webpack_require__(575);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};













var AppLibraryPage = (function () {
    function AppLibraryPage(navCtrl, navParams, storage, formBuilder, modalCtrl, alertCtrl, loadingCtrl, loadingService, camera, imageViewerCtrl, myApp, http, transfer, file, toastCtrl) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.loadingService = loadingService;
        this.camera = camera;
        this.imageViewerCtrl = imageViewerCtrl;
        this.myApp = myApp;
        this.http = http;
        this.transfer = transfer;
        this.file = file;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.dependent = true;
        this.baseurl = __WEBPACK_IMPORTED_MODULE_11__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_11__environment_environment__["a" /* environment */].appsecret;
        this.upload = "assets/images/upload1.svg";
    }
    AppLibraryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LibraryPage');
        this.getuserLoginData();
    };
    AppLibraryPage.prototype.ImageUpload = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            // title: 'Image Upload',
            message: "Select Image source",
            buttons: [
                {
                    text: "Upload from Library",
                    handler: function () {
                        _this.gallery(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    },
                },
                {
                    text: "Use Camera",
                    handler: function () {
                        _this.gallery(_this.camera.PictureSourceType.CAMERA);
                    },
                },
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: function () {
                    },
                },
            ],
        });
        alert.present();
    };
    AppLibraryPage.prototype.ConfirmImage = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: "Are you sure You want to upload picture ?",
            buttons: [
                {
                    text: "Yes",
                    handler: function () {
                        _this.uploadFile();
                    }
                },
                {
                    text: "No",
                    role: "cancel",
                    handler: function () {
                    },
                },
            ],
        });
        alert.present();
    };
    AppLibraryPage.prototype.presentImage = function (myImage) {
        var imageViewer = this.imageViewerCtrl.create(myImage);
        imageViewer.present();
        // setTimeout(() => imageViewer.dismiss(), 1000);
        // imageViewer.onDidDismiss(() => alert('Viewer dismissed'));
    };
    AppLibraryPage.prototype.gallery = function (sourceType) {
        var _this = this;
        var self = this;
        {
            var options = {
                quality: 50,
                destinationType: this.camera.DestinationType.FILE_URI,
                sourceType: sourceType,
                correctOrientation: true,
                // encodingType: this.camera.EncodingType.JPEG,
                mediaType: this.camera.MediaType.ALLMEDIA,
                allowEdit: true
            };
            this.camera.getPicture(options).then(function (imageData) {
                _this.image = imageData;
                _this.ConfirmImage();
            }, function (err) {
                console.log(err);
                // this.presentToast(err);
                _this.toastCtrl.presentToast(err);
            });
        }
    };
    AppLibraryPage.prototype.uploadFile = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Uploading..."
        });
        loader.present();
        var fileTransfer = this.transfer.create();
        var name = this.image.split("/").pop();
        var Params = {};
        Params.userid = this.userLoginData.id;
        Params.appsecret = this.appsecret;
        Params.action = "addlibrary";
        var options = {
            fileKey: 'file',
            fileName: name,
            chunkedMode: false,
            mimeType: 'multipart/form-data',
            headers: {},
            httpMethod: 'POST',
            params: Params
        };
        fileTransfer.upload(this.image, this.baseurl + 'library.php', options)
            .then(function (data) {
            console.log("data", data);
            // console.log(JSON.stringify(data) + " Uploaded Successfully");
            console.log("image", _this.image);
            loader.dismiss();
            _this.getlibrary();
        }, function (err) {
            console.log(err);
            loader.dismiss();
        });
    };
    AppLibraryPage.prototype.setVisibility = function (element, state) {
        if (element !== undefined) {
            element.nativeElement.style.display = state;
        }
    };
    AppLibraryPage.prototype.addlibrary = function () {
        var _this = this;
        var data1 = {};
        var self = this;
        data1.action = "addlibrary";
        data1.userid = this.userLoginData.id;
        data1.addedby = this.userLoginData.id;
        data1.appsecret = this.appsecret;
        data1.image = this.image;
        console.log("datasdfsfsd", data1);
        self.http.post(this.baseurl + "library.php", data1).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            console.log(result.status);
            if (result.status == "success") {
                alert("photo uploaded");
                _this.getlibrary();
            }
            else {
                // self.toastCtrl.presentToast("There is an error saving data!");
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
            }
        }, function (err) {
            _this.loadingService.hide();
            console.log(err);
        });
    };
    AppLibraryPage.prototype.getlibrary = function () {
        var _this = this;
        this.loadingService.show();
        var data1 = {};
        var self = this;
        data1.action = "getlibrary";
        data1.userid = this.userLoginData.id;
        data1.appsecret = this.appsecret;
        console.log("datasdfsfsd", data1);
        self.http.post(this.baseurl + "library.php", data1).subscribe(function (data) {
            _this.loadingService.hide();
            var result = JSON.parse(data["_body"]);
            if (result.data) {
                _this.images = result.data;
                console.log(result);
            }
            else {
                console.log("error");
            }
        }, function (err) {
            _this.loadingService.hide();
            console.log("error", err);
            console.log(err);
        });
    };
    AppLibraryPage.prototype.ConfirmDelete = function (id) {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: "Are you sure You want to delete picture ?",
            buttons: [
                {
                    text: "Yes",
                    handler: function () {
                        _this.deletelibrary(id);
                    }
                },
                {
                    text: "No",
                    role: "cancel",
                    handler: function () {
                    },
                },
            ],
        });
        alert.present();
    };
    AppLibraryPage.prototype.deletelibrary = function (id) {
        var _this = this;
        this.loadingService.show();
        var data1 = {};
        var self = this;
        data1.action = "deleteimage";
        data1.userid = this.userLoginData.id;
        data1.addedby = this.userLoginData.id;
        data1.appsecret = this.appsecret;
        data1.id = id;
        data1.image = this.image;
        console.log("datasdfsfsd", data1);
        self.http.post(this.baseurl + "library.php", data1).subscribe(function (data) {
            _this.loadingService.hide();
            var result = JSON.parse(data["_body"]);
            console.log(result.status);
            if (result.status == "success") {
                _this.toastCtrl.presentToast("picture Deleted!");
                _this.getlibrary();
            }
            else {
                // self.toastCtrl.presentToast("There is an error saving data!");
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
            }
        }, function (err) {
            _this.loadingService.hide();
            console.log(err);
        });
    };
    AppLibraryPage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        console.log("set string in storage: " + result);
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        console.log(reason_1);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // to get a key/value pair
    AppLibraryPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        console.log("storageGET: " + key + ": " + result);
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_2 = _a.sent();
                        console.log(reason_2);
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppLibraryPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("patientType").then(function (result) {
                            if (result == "independent") {
                                _this.dependent = false;
                            }
                            _this.getlibrary();
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppLibraryPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("userLogin").then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppLibraryPage.prototype.pressed = function () {
        console.log("pressed");
    };
    AppLibraryPage.prototype.active = function () {
        console.log("active");
    };
    AppLibraryPage.prototype.release = function () {
        console.log("released");
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("imageUploader"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppLibraryPage.prototype, "imageUploader", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("addPhotoCaption"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppLibraryPage.prototype, "addPhotoCaption", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("removePhotoIcon"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppLibraryPage.prototype, "removePhotoIcon", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("previewImage"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppLibraryPage.prototype, "previewImage", void 0);
    AppLibraryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-library',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-library/app-library.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Library</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-row>\n    <ion-col col-8>\n      <ion-row style="width:154%">\n        <!-- <div padding\n          style="margin-left: 35px;height: 150px;margin-right: 0px;width:200px;background: #39415b; color:#444; border-radius: 10px">\n          <div #imageUploader><img style="width:55%;" [src]="upload" (click)="ImageUpload()"></div>\n          <div #addPhotoCaption style="color:#fff;">Add Photo or vedio</div>\n        </div> -->\n        <!-- <div #previewImage> <img style="border-radius:20px" [src]="data.image"\n          *ngIf="data?.image"></div> -->\n        <div style="margin-bottom: 20px;margin-left: 30px;margin-right:-30px;" *ngFor="let item of images"\n          #previewImage>\n          <div class="div-back" (click)="ConfirmDelete(item.id)">\n            <ion-icon class="icon" name="trash">\n            </ion-icon>\n          </div>\n          <img style="height: 150px;width:200px;border-radius:20px" [src]="item.image" #myImage\n            (click)="presentImage(myImage)" />\n\n        </div>\n\n      </ion-row>\n    </ion-col>\n  </ion-row>\n</ion-content>\n\n<button (click)="ImageUpload()" style=" background-color: rgb(251 126 126);height: 80px;width: 80px;position: sticky;left: 920px;top:80%;z-index:\n  9999;" ion-fab>\n  <ion-icon style="font-size: 60px;" class="icAdd" name="add"></ion-icon>\n</button>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-library/app-library.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_ngx__["a" /* File */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_img_viewer__["a" /* ImageViewerController */],
            __WEBPACK_IMPORTED_MODULE_8__app_app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_9__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_ngx__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_10__services_toast_service__["a" /* ToastService */]])
    ], AppLibraryPage);
    return AppLibraryPage;
}());

//# sourceMappingURL=app-library.js.map

/***/ })

});
//# sourceMappingURL=41.js.map