webpackJsonp([43],{

/***/ 932:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppHealthcareproviderPageModule", function() { return AppHealthcareproviderPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_healthcareprovider__ = __webpack_require__(986);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppHealthcareproviderPageModule = (function () {
    function AppHealthcareproviderPageModule() {
    }
    AppHealthcareproviderPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_healthcareprovider__["a" /* AppHealthcareproviderPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_healthcareprovider__["a" /* AppHealthcareproviderPage */]),
            ],
        })
    ], AppHealthcareproviderPageModule);
    return AppHealthcareproviderPageModule;
}());

//# sourceMappingURL=app-healthcareprovider.module.js.map

/***/ }),

/***/ 986:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppHealthcareproviderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_cookie_services_cookies_service__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_cookie_services_cookies_service___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular2_cookie_services_cookies_service__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the AppHealthcareproviderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppHealthcareproviderPage = (function () {
    function AppHealthcareproviderPage(navCtrl, navParams, menu, storage, alertCtrl, databaseService, loadingService, _cookieService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this._cookieService = _cookieService;
        this.data = {};
        this.eventsData = [];
        this.patientData = {};
        this.SearchData = [];
    }
    AppHealthcareproviderPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppHealthcareproviderPage');
    };
    AppHealthcareproviderPage.prototype.logout = function () {
        this.databaseService.logout();
    };
    AppHealthcareproviderPage.prototype.ionViewWillEnter = function () {
    };
    AppHealthcareproviderPage.prototype.setFilteredPatientsData = function () {
        var _this = this;
        this.SearchData = this.data.values.filter(function (Patient) {
            console.log(Patient);
            return Patient.name.toLowerCase().indexOf(_this.terms.toLowerCase()) > -1;
        });
    };
    AppHealthcareproviderPage.prototype.deleteHealthcareProvidersData = function (item) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Do you want to delete this record?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function () {
                        console.log('Delete Ok clicked');
                        var self = _this;
                        setTimeout(function () { self.ionViewWillEnter(); }, 1000);
                    }
                }
            ]
        });
        alert.present();
    };
    AppHealthcareproviderPage.prototype.onAddHealthcareprovider = function () {
        this.navCtrl.push("AppHealthcareproviderEditPage");
    };
    AppHealthcareproviderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-healthcareprovider',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-healthcareprovider/app-healthcareprovider.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Healthcare Provider List</ion-title>\n    <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n    </button>\n    <!-- <ion-buttons right (click)="logout()">\n        <button ion-button icon-only><ion-icon name="lock">Logout</ion-icon></button>\n    </ion-buttons> -->\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding class="fadeIn">\n      <ion-row>\n          <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n          </ion-col>\n          <ion-col col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2>\n          </ion-col>\n          <ion-col col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n              <ion-searchbar [(ngModel)]="terms" (ionInput)="setFilteredPatientsData()" placeholder="Type to search here"></ion-searchbar>\n          </ion-col>\n      </ion-row>\n      <ion-grid padding *ngIf="data != null">\n          <ion-row>\n            <ion-col col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 style="padding:5px 15px 5px 15px;">\n                <ion-card text-center style="height: 177px;">\n                        <ion-grid>\n                            <ion-row>\n                                <ion-col col-3>\n                                </ion-col>\n                                <ion-col  (click)="onAddHealthcareprovider()">\n                                    <img style="height: 124px;width: 124px; margin:0px auto;" src="assets/images/others/add-patients.png" />\n                                </ion-col>\n                                <ion-col col-3>\n                                </ion-col>\n                            </ion-row>\n                            <ion-row>\n                                <ion-col col-3>\n                                </ion-col>\n                                <ion-col>\n                                    Add New Healthcare Provider\n                                </ion-col>\n                                <ion-col col-3>\n                                </ion-col>\n                            </ion-row>\n                        </ion-grid>\n                </ion-card>\n            </ion-col>\n            <ion-col col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 *ngFor="let item of SearchData;let i = index;" style="padding:5px 15px 5px 15px;">\n              <ion-card text-center style="height: 177px;">\n              <ion-grid>\n                <ion-row>\n                  <ion-col  col-3 style="margin-left:16px;">\n                    <img *ngIf="item.image" style="border-radius: 50%;height: 90px;width: 90px;" src="{{item.image}}" />\n                    <img *ngIf="!item.image" style="border-radius: 50%;height: 90px;width: 90px;" src="assets/images/nopreview.jpeg" />\n                  </ion-col>\n                  <ion-col>\n                      <ion-grid>\n                          <ion-row>\n                              <ion-col>\n                                  <img (click)="editHealthcareProvider(item.email)" class="card-top-icon" style="margin-left:5px;" src="assets/images/others/edit-icon.png" />\n                                  <img (click)="deleteHealthcareProvidersData(item)" class="card-top-icon" src="assets/images/others/delete-icon.png" />\n                              </ion-col>\n                          </ion-row>\n                          <ion-row>\n                              <ion-col>\n                                  <h2 style="font-size:16px; color:#fff;" text-center>{{item.name.substring(0,24)}}</h2>\n                              </ion-col>\n                          </ion-row>\n                      </ion-grid>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n              <ion-row>\n                  <ion-col>\n                    <ion-label stacked >\n                        Email:\n                        <span style="color: #e0675c;margin-left: 7px;">*</span>\n                    </ion-label>\n                      <p style="font-size:12px; color:#fff;" text-center>{{item.email}}</p>\n                  </ion-col>\n              </ion-row>\n              </ion-card>\n              </ion-col>\n          </ion-row>\n      </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-healthcareprovider/app-healthcareprovider.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* MenuController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_5_angular2_cookie_services_cookies_service__["CookieService"]])
    ], AppHealthcareproviderPage);
    return AppHealthcareproviderPage;
}());

//# sourceMappingURL=app-healthcareprovider.js.map

/***/ })

});
//# sourceMappingURL=43.js.map