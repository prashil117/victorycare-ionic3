webpackJsonp([17],{

/***/ 1017:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettingAboutVictoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AppSettingAboutVictoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppSettingAboutVictoryPage = (function () {
    function AppSettingAboutVictoryPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AppSettingAboutVictoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppSettingAboutVictoryPage');
    };
    AppSettingAboutVictoryPage.prototype.openwindow = function () {
        console.log("inside url");
        // window.open('', '_system');
        window.open("https://victory.care/", '_system', 'location=yes');
    };
    AppSettingAboutVictoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-setting-about-victory',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-setting-about-victory/app-setting-about-victory.html"*/'<!--\n  Generated template for the AppSettingAboutVictoryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n        <ion-title style="text-align: center;" center>About Victor(y) Care</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div style="padding-top: 40px;">\n        <img src="assets/imgs/logo-victory.png" />\n    </div>\n    <div style="padding-top: 40px;">\n        <label class="Victory-Care">Victor(y) Care</label>\n        <p class="We-help-communicate">Track your Health Communicate your Wellbeing</p>\n    </div>\n\n    <p class="App-version-number-1">App version number 0.2.0</p>\n    <div style="display: inline-flex; padding-top: 65px;">\n        <div col-4>\n            <label class="Stext-style-1">Support & Feedback:</label>\n            <a href="mailto:help.victory.care">\n                <p class="Support-Feedback ">\n                    hello@victory.care</p>\n            </a>\n        </div>\n        <div col-4>\n            <label class="text-style-1">Victor(y) Care enables individuals experiencing health problems to track and\n                communicate their health status visually.</label>\n            <div style="display: inline-flex;" (click)="openwindow()">\n                <p style="color: #f4f4f4; padding-right: 5px;">Visit us at </p>\n                <!-- <a href="https://www.victory.care"  target="_blank"> -->\n                <p class="Support-Feedback"> www.victory.care</p>\n                <!-- </a> -->\n            </div>\n\n        </div>\n        <div col-4>\n            <label class="text-style-1">Concept, Design and Development:</label>\n            <p style="color: #f4f4f4;">Victor(y) Care AS,Norway</p>\n        </div>\n    </div>\n\n\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-setting-about-victory/app-setting-about-victory.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */]])
    ], AppSettingAboutVictoryPage);
    return AppSettingAboutVictoryPage;
}());

//# sourceMappingURL=app-setting-about-victory.js.map

/***/ }),

/***/ 963:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppSettingAboutVictoryPageModule", function() { return AppSettingAboutVictoryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_setting_about_victory__ = __webpack_require__(1017);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppSettingAboutVictoryPageModule = (function () {
    function AppSettingAboutVictoryPageModule() {
    }
    AppSettingAboutVictoryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_setting_about_victory__["a" /* AppSettingAboutVictoryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_setting_about_victory__["a" /* AppSettingAboutVictoryPage */]),
            ],
        })
    ], AppSettingAboutVictoryPageModule);
    return AppSettingAboutVictoryPageModule;
}());

//# sourceMappingURL=app-setting-about-victory.module.js.map

/***/ })

});
//# sourceMappingURL=17.js.map