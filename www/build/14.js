webpackJsonp([14],{

/***/ 1019:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppTipPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppTipPage = (function () {
    function AppTipPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AppTipPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppTipPage');
    };
    AppTipPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-tip',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-tip/app-tip.html"*/'<!--\n  Generated template for the AppTipPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>app_tip</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-slides pager="true">\n\n        <ion-slide>\n            <div>\n              <img style="padding-top: 25px; width: 56%;" src="assets/imgs/wheelTip.png" />\n              <p style="margin-left: 130px; width: 70%;"> </p>\n                  <img style="padding-top: 25px; margin-left: 460px;" src="assets/imgs/next.png" />\n            </div>\n        </ion-slide>\n    \n        <ion-slide>\n          <div>\n            <img style="padding-top: 25px; width: 56%;" src="assets/imgs/healthcare.png" />\n            <p style="margin-left: 130px; width: 70%;"></p>\n              <img style="padding-top: 25px; margin-left: 460px;" src="assets/imgs/next.png" />\n          </div>\n        </ion-slide>\n    \n        <ion-slide>\n          <div>\n            <img style="padding-top: 25px; width: 56%;" src="assets/imgs/your_health.png" />\n            <p style="margin-left: 130px; width: 70%;"> </p>\n          </div>\n        </ion-slide>\n      </ion-slides>\n   \n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-tip/app-tip.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */]])
    ], AppTipPage);
    return AppTipPage;
}());

//# sourceMappingURL=app-tip.js.map

/***/ }),

/***/ 965:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppTipPageModule", function() { return AppTipPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_tip__ = __webpack_require__(1019);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppTipPageModule = (function () {
    function AppTipPageModule() {
    }
    AppTipPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_tip__["a" /* AppTipPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_tip__["a" /* AppTipPage */]),
            ],
        })
    ], AppTipPageModule);
    return AppTipPageModule;
}());

//# sourceMappingURL=app-tip.module.js.map

/***/ })

});
//# sourceMappingURL=14.js.map