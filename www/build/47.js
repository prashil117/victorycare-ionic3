webpackJsonp([47],{

/***/ 926:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppDashboardPageModule", function() { return AppDashboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_dashboard__ = __webpack_require__(980);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppDashboardPageModule = (function () {
    function AppDashboardPageModule() {
    }
    AppDashboardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_dashboard__["a" /* AppDashboardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_dashboard__["a" /* AppDashboardPage */]),
            ],
        })
    ], AppDashboardPageModule);
    return AppDashboardPageModule;
}());

//# sourceMappingURL=app-dashboard.module.js.map

/***/ }),

/***/ 980:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppDashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_component__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_chart_js__ = __webpack_require__(569);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__environment_environment__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_loading_service__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











/**
 * Generated class for the AppDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppDashboardPage = (function () {
    function AppDashboardPage(loadingService, navCtrl, navParams, databaseService, menu, storage, formBuilder, events, myApp, popoverController, http, toastCtrl) {
        this.loadingService = loadingService;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.databaseService = databaseService;
        this.menu = menu;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.events = events;
        this.myApp = myApp;
        this.popoverController = popoverController;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.dropdown = [];
        this.selected1 = "";
        this.username = "";
        this.usertype = "";
        this.ut = [];
        this.dependent = true;
        this.baseurl = __WEBPACK_IMPORTED_MODULE_9__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_9__environment_environment__["a" /* environment */].appsecret;
        this.storage.remove('patientSession');
        this.storage.remove('patientSessionData');
        this.storage.remove('user');
        this.Profileform = formBuilder.group({
            currentHCP: ['']
        });
        // this.get('currentHCP').then(result => {
        //   console.log('Username1:::::::: ' + result);
        //   console.log("result")
        //   if (result != null) {
        //     this.notselected = false;
        //     this.selected1 = result;
        //     console.log("resulrtttrtrtr")
        //   } else {
        //     this.notselected = true;
        //     console.log("dropdown    :::::::::", this.dropdown);
        //     this.selected1 = this.dropdown;
        //   }
        // });
    }
    AppDashboardPage.prototype.intializeApp1 = function () {
        if (this.userLoginData.usertype == "patient") {
            if (!this.patientType || this.patientType == undefined) {
                this.dependent = false;
            }
        }
        if (this.patientType == 'independent') {
            this.dependent = false;
            console.log("1234");
        }
        this.storage.get('patientSession').then(function (val) {
            console.log(val);
            console.log("12345");
        });
        this.myApp.initializeApp();
        this.username = this.userLoginData.email;
        this.usertype = this.userLoginData.usertype;
        this.userid = this.userLoginData.id;
        this.ut = (this.usertype == 'healthcareprovider' ? false : true);
        console.log(this.userid);
        this.dropdown = this.getHCPData(this.userid, this.usertype);
        console.log(this.dropdown);
    };
    AppDashboardPage.prototype.getHCPData = function (userid, usertype) {
        var _this = this;
        var hcp1 = [];
        var self = this;
        var webServiceData = {};
        webServiceData.action = "gethcpdata";
        webServiceData.id = userid;
        webServiceData.usertype = usertype;
        webServiceData.appsecret = this.appsecret;
        self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
            _this.loadingService.show();
            var result = JSON.parse(data["_body"]);
            console.log(result);
            if (result.status == 'success') {
                _this.loadingService.hide();
                // result.datansol
                console.log("result ::::::::=>", result);
                hcp1 = hcp1.push(result.data);
                _this.hcpDropdownArray = result.data;
                for (var i in _this.hcpDropdownArray) {
                    if (!_this.selected || _this.selected == undefined) {
                        _this.selected = _this.hcpDropdownArray[0].id;
                        _this.set('currentHCP', _this.selected);
                    }
                }
            }
            else {
                _this.loadingService.hide();
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
        }, function (err) {
            _this.loadingService.hide();
            console.log(err);
        });
        return hcp1;
    };
    // set a key/value
    AppDashboardPage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // to get a key/value pair
    AppDashboardPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_2 = _a.sent();
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // remove a single key value:
    AppDashboardPage.prototype.remove = function (key) {
        this.storage.remove(key);
    };
    AppDashboardPage.prototype.selectEmployee = function ($event) {
        this.set('currentHCP', $event);
        console.log("$seventtttttt", $event);
        this.doRefresh();
    };
    AppDashboardPage.prototype.getHCPid = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('currentHCP').then(function (result) {
                            if (result != null) {
                                _this.selected = result;
                                console.log("sekeccdffvfdgdfgdfg", _this.selected);
                                _this.getuserLoginData();
                            }
                            else {
                                _this.getuserLoginData();
                                console.log("else", _this.selected);
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppDashboardPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('userLogin').then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppDashboardPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("this.userdata", this.userLoginData);
                        return [4 /*yield*/, this.get('patientType').then(function (result) {
                                console.log("patienttypesdfdsfsdsdf", result);
                                if ((!result || result == null || result == undefined) && _this.userLoginData == "patient") {
                                    _this.doRefresh();
                                }
                                else {
                                    _this.patientType = result;
                                    _this.intializeApp1();
                                    console.log("sekeccdffvfdgdfgdfg", _this.patientType);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppDashboardPage.prototype.doRefresh = function () {
        var _this = this;
        setTimeout(function () {
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            // event.target.complete();
        }, 2000);
    };
    AppDashboardPage.prototype.timeout = function () {
        setTimeout(function () {
            console.log('refresh');
            // event.target.complete();
        }, 2000);
    };
    AppDashboardPage.prototype.ionViewDidLoad = function () {
        this.getHCPid();
        // this.getuserLoginData()
        // this.getpatientType()
        this.createBarChart();
        // this.storage.get('userLogin').then((val) => {
        //   console.log(val);
        // });
    };
    AppDashboardPage.prototype.createBarChart = function () {
        this.bars = new __WEBPACK_IMPORTED_MODULE_4_chart_js__["Chart"](this.barChart.nativeElement, {
            type: 'bar',
            data: {
                labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
                datasets: [{
                        label: 'Viewers in millions',
                        data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
                        backgroundColor: 'rgb(38, 194, 129)',
                        borderColor: 'rgb(255, 255, 255)',
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
        this.bars2 = new __WEBPACK_IMPORTED_MODULE_4_chart_js__["Chart"](this.barChart2.nativeElement, {
            type: 'line',
            data: {
                labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
                datasets: [{
                        label: 'Viewers in millions',
                        data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
                        backgroundColor: 'rgb(225, 56, 56)',
                        borderColor: 'rgb(255, 255, 255)',
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
        this.bars3 = new __WEBPACK_IMPORTED_MODULE_4_chart_js__["Chart"](this.barChart3.nativeElement, {
            type: 'pie',
            data: {
                labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
                datasets: [{
                        label: 'Viewers in millions',
                        data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
                        backgroundColor: ['rgb(225, 56, 56)', 'rgb(25, 1, 56)', 'rgb(78, 205, 196)', 'rgb(232, 198, 50)', 'rgb(255, 77, 20)', 'rgb(85, 98, 112)', 'rgb(224, 224, 224)', 'rgb(73, 88, 178)'],
                        borderColor: 'rgb(255, 255, 255)',
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
        this.bars4 = new __WEBPACK_IMPORTED_MODULE_4_chart_js__["Chart"](this.barChart4.nativeElement, {
            type: 'line',
            data: {
                labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
                datasets: [{
                        label: 'Viewers in millions',
                        data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
                        backgroundColor: 'rgba(0, 0, 0, 0)',
                        borderColor: 'rgb(255, 255, 255)',
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    };
    AppDashboardPage.prototype.logout = function () {
        this.storage.clear();
        this.databaseService.logout();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart'),
        __metadata("design:type", Object)
    ], AppDashboardPage.prototype, "barChart", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart2'),
        __metadata("design:type", Object)
    ], AppDashboardPage.prototype, "barChart2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart3'),
        __metadata("design:type", Object)
    ], AppDashboardPage.prototype, "barChart3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart4'),
        __metadata("design:type", Object)
    ], AppDashboardPage.prototype, "barChart4", void 0);
    AppDashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-dashboard',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-dashboard/app-dashboard.html"*/'<!--\n  Generated template for the AppDashboardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Dasboard | {{usertype}}</ion-title>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <form [formGroup]="Profileform">\n\n      <!-- <ion-item no-lines *ngIf="ut && notselected && dependent" class="item-width" style="background:none;">\n        <ion-select placeholder="Select Healthcare Provider" style="color: white;" \n          (ionChange)="selectEmployee($event)" right>\n          <ion-label>Select Healthcare Provider</ion-label>\n          <ion-option *ngFor="let key of dropdown[0]; let idx = index;" [selected]="key.id==selected"\n            value="{{key.id}}">{{key.firstname}}aaa</ion-option>\n        </ion-select>\n      </ion-item> -->\n      <ion-item no-lines class="item-width" style="background:none;" *ngIf="ut && dependent">\n        <ion-select placeholder="Select Healthcare Provider" style="color: white;" (ionChange)="selectEmployee($event)"\n          right>\n          <ion-label>Select Healthcare Provider</ion-label>\n          <ion-option *ngFor="let key of dropdown[0]; let idx = index;" [selected]="key.id==selected"\n            value="{{key.id}}">\n            {{key.firstname}}</ion-option>\n        </ion-select>\n      </ion-item>\n    </form>\n    <!-- <ion-select interface="popover" right>\n      <ion-label>Select</ion-label>\n      <ion-option value="logout">Logout</ion-option>\n      <ion-option value="changepassword">Change Password</ion-option>\n    </ion-select> -->\n    <!-- <ion-buttons right (click)="logout()">\n      <button ion-button icon-only>\n        <ion-icon name="lock">Logout</ion-icon>\n      </button>\n    </ion-buttons> -->\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 style="padding:5px 15px 5px 15px;">\n        <!-- <ion-list-header color="light">Vertical Bar Chart</ion-list-header> -->\n        <ion-card class="welcome-card">\n          <ion-card-header>\n            <ion-card-title>Sample Chart 1</ion-card-title>\n          </ion-card-header>\n          <ion-card-content>\n            <canvas #barChart></canvas>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n      <ion-col col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 style="padding:5px 15px 5px 15px;">\n        <!-- <ion-list-header color="light">Vertical Bar Chart</ion-list-header> -->\n        <ion-card class="welcome-card">\n          <ion-card-header>\n            <ion-card-title>Sample Chart 2</ion-card-title>\n          </ion-card-header>\n          <ion-card-content>\n            <canvas #barChart2></canvas>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n      <ion-col col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 style="padding:5px 15px 5px 15px;">\n        <!-- <ion-list-header color="light">Vertical Bar Chart</ion-list-header> -->\n        <ion-card class="welcome-card">\n          <ion-card-header>\n            <ion-card-title>Sample Chart 3</ion-card-title>\n          </ion-card-header>\n          <ion-card-content>\n            <canvas #barChart3></canvas>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n      <ion-col col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 style="padding:5px 15px 5px 15px;">\n        <!-- <ion-list-header color="light">Vertical Bar Chart</ion-list-header> -->\n        <ion-card class="welcome-card">\n          <ion-card-header>\n            <ion-card-title>Sample Chart 4</ion-card-title>\n          </ion-card-header>\n          <ion-card-content>\n            <canvas #barChart4></canvas>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <!-- <ion-grid>\n    <ion-row>\n\n      </ion-row>\n  </ion-grid> -->\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-dashboard/app-dashboard.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_10__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* MenuController */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Events */], __WEBPACK_IMPORTED_MODULE_3__app_app_component__["a" /* MyApp */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["A" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_8__services_toast_service__["a" /* ToastService */]])
    ], AppDashboardPage);
    return AppDashboardPage;
}());

//# sourceMappingURL=app-dashboard.js.map

/***/ })

});
//# sourceMappingURL=47.js.map