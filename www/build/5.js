webpackJsonp([5],{

/***/ 939:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppLoginModule", function() { return AppLoginModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_login__ = __webpack_require__(993);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppLoginModule = (function () {
    function AppLoginModule() {
    }
    AppLoginModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_login__["a" /* AppLoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_login__["a" /* AppLoginPage */]),
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], AppLoginModule);
    return AppLoginModule;
}());

//# sourceMappingURL=app-login.module.js.map

/***/ }),

/***/ 976:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppTimelinePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_component__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing__ = __webpack_require__(571);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_screenshot_ngx__ = __webpack_require__(572);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__environment_environment__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};













var AppTimelinePage = (function () {
    function AppTimelinePage(socialShareCtrl, navCtrl, alertCtrl, navParams, modalCtrl, databaseService, myApp, storage, formBuilder, loadingService, http, toastCtrl, screenshot) {
        this.socialShareCtrl = socialShareCtrl;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.databaseService = databaseService;
        this.myApp = myApp;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.loadingService = loadingService;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.screenshot = screenshot;
        this.baseurl = __WEBPACK_IMPORTED_MODULE_12__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_12__environment_environment__["a" /* environment */].appsecret;
        this.patientData = {};
        this.WheelActivityData = [];
        this.WheelMedicineData = [];
        this.WheelEventData = [];
        this.SettingsData = [];
        this.activityData = [];
        this.data = [];
        this.ActivityArrar = [];
        this.EventArrar = [];
        this.MedicineArrar = [];
        this.myColor = [];
        this.EventColor = [];
        this.MedicineColor = [];
        this.name = [];
        this.Eventname = [];
        this.column = "start";
        this.descending = false;
        this.medicineName = [];
        this.BorderColor = [];
        this.MedicineWheelData = [];
        this.ActivityWheelData = [];
        this.StatusWheelData = [];
        this.dropdown = [];
        this.selected1 = [];
        this.result = [];
        this.bordercolors = [
            "#7e1b1b",
            "#611881",
            "#3b2079",
            "#004d44",
            "#1d491f",
            "#787b1e",
            "#966c03",
            "#996100",
            "#804000",
            "#5e7a87",
        ];
        this.colors = [
            "#D32F2F",
            "#7B1FA2",
            "#512DA8",
            "#00796B",
            "#388E3C",
            "#AFB42B",
            "#FBC02D",
            "#FFA000",
            "#F57C00",
            "#455A64",
        ];
        this.WheelData = [];
        this.date = new Date();
        // Wheel declaretion start
        this.sleepColors = {
            "default": "#FFF",
            "sleep": "#6bb1d6",
            "sleepawake": "#afdb07",
            "sleepunawake": "#fad000",
            "sleepdistress": "#e26060",
            "sleepmultiple": "#FF69B4",
        };
        this.medicineColors = {
            "default": "#2b3249",
            "medicinelosec": "#95d1f0",
            "medicinezonegran": "#fadf59",
            "medicinemovicol": "#fd8686",
            "medicinekepra": "#cff14a",
            "medicinemultiple": "#FF69B4",
        };
        this.activityColors = {
            "default": "#FFF",
            "activitybpap": "#74a9c4",
            "activityoxygen": "#90b018",
            "activitytemperature": "#c8ac20",
            "activityurine": "#e69393",
            "activitymultiple": "#FF69B4",
        };
        this.numNodes = 96;
        this.defaultMinutes = 15;
        this.minuteTxt = [
            [15, 30, 45, 60],
            [30, 60],
        ];
        this.whichClockforTxt = 0;
        this.calMinute = this.numNodes / 12;
        this.arrClockSleep = new Array();
        this.arrClockMedicine = new Array();
        this.arrClockActivity = new Array();
        this.arrReturn = new Array();
        this.status = [];
        this.sleepDescription = [];
        this.wheelEventColor = [];
        this.startTime = [];
        this.endTime = [];
        this.medicineStart = [];
        this.wheelMedicineColor = [];
        this.medicineStatus = [];
        this.medicineDescription = [];
        this.wheelActivityColor = [];
        this.activityStartTime = [];
        this.activityDescription = [];
        this.activityEndTime = [];
        this.activityStatus = [];
        this.arrstartendtime = [];
        this.endmin = [];
        this.end = [];
        this.activityList = [];
        this.medicineList = [];
        this.wellbeaningList = [];
        this.arrDes = [];
        this.medicineDes = [];
        this.medicineColor = [];
        this.sleepTime = [];
        this.sleepDes = [];
        this.sleepColor = [];
        this.circle = [];
        this.startPoint = [];
        this.arrcolor = [];
        this.patientMasterEventData = {};
        this.patientMasterMedicineData = {};
        this.patientMasterActivityData = {};
        this.dependent = true;
        this.shareImageNi = "https://lh3.googleusercontent.com/8c4SpecjfcnCdfq2PXTwS7vvjPmrOa-lgbcczv6RBRF-6fgYv7-qCrNEW5owqQRPx15FesYOt3oqJrQJTmk0XZoR4WGEc3UGGbZgt7GvPKOd_pW4_Cbd9yqiHY9mxoHAkr5m-coUo47lsSy484J4kINWBrR3YaXClG_KRLvYDMH5vjipCJsTlh8-1Eg5z-IMON0yW9dwL0C42FPDTJ5pQIE2ilZ7J4CYGSo14XDw87zdZ7GrkrLtuESlupo2rzAX80zaZ4ER-S-o8fhHwvLYNrrYrfrJMCBjduqziJxvuBnB5fMSkn8BUI9JuIcqq9Hp5J2aEwIrb1xQUpW8bbC0o2Y8hVUv2yECJ8F4jieEg5EQaz--sPxjogAEgybsaGS7QJK6yphIm59ulIQRUmcwOEf5oqMHwXUrYfaW4v-hia_9h4vuNWoUOksNstaJa22PVRbZGIZLdFdfPYE9ekVyFvUxqsZ9Ft6pFAXdkD3UX7GaKujky6QFNJDqkCef8Tu0vXQFPycxmIdm4spMgKy6NIc92_p6AQo_7AYrv7pI_vtUL4-Sh6HtQ09o6g0GbprKCnforqQyHWsAOdVQ5M6zjOrh6dNAuxFTl4MHIV8=w353-h588-no";
        this.siteName = "https://victory.care";
        this.SelectList = formBuilder.group({
            currentHCP: [""],
        });
    }
    AppTimelinePage.prototype.openSelect = function () {
        this.selectRef.open();
    };
    AppTimelinePage.prototype.appIntialize1 = function () {
        var _this = this;
        this.loadingService.show();
        // setTimeout(() => {
        //   this.loadingService.hide
        //  }, 3000);
        var action = this.navParams.data.action;
        var patient = this.navParams.data.data;
        if (this.patientType == "independent") {
            this.dependent = false;
        }
        // if(action == true){
        if (this.userLoginData.usertype == "patient") {
            this.get("userLogin").then(function (res) {
                setTimeout(function () {
                    _this.loadingService.hide();
                }, 2000);
                _this.userlogindata = res;
                _this.userlogindata.userid = _this.userlogindata.id;
            });
            this.get("currentHCP").then(function (res) {
                _this.userlogindata.parentid = res;
                action = true;
                _this.navParams.data.data = _this.userlogindata;
                _this.patientData = _this.navParams.data.data;
                patient = _this.navParams.data.data;
                _this.storage.set("patientSession", action);
                _this.storage.set("patientSessionData", patient);
            });
        }
        else {
            this.storage.set("patientSession", action);
            this.storage.set("patientSessionData", patient);
        }
        this.selectedDate = new Date().toISOString();
        this.WheelData = [];
        if (!__WEBPACK_IMPORTED_MODULE_2_lodash__["isEmpty"](this.navParams.data.data)) {
            this.WheelData = [];
            this.patientData = this.navParams.data.data;
            // this.GetAllSettingsData();
        }
        this.loadWheel();
        // let modal = this.modalCtrl.create("AppSelectWheelTimePage");
        // modal.present();
        // modal.onDidDismiss(data => {
        // });
        ////////// HCP DropDown Code Starts /////////////
        this.myApp.initializeApp();
        this.username = this.userLoginData.email;
        this.usertype = this.userLoginData.usertype;
        this.userid = this.userLoginData.id;
        // this.ut = (this.usertype == 'healthcareprovider' ? false : true);
        if (this.userLoginData.usertype == "nurse" ||
            this.userLoginData.usertype == "doctor" ||
            this.userLoginData.usertype == "healthcareprovider") {
            this.ut = false;
        }
        else {
            this.ut = true;
        }
        // alert(this.usertype);
        this.dropdown = this.getHCPData(this.userid, this.usertype);
        var result = null;
        this.get("currentHCP").then(function (result) {
            if (result != null) {
                _this.notselected = false;
                _this.selected1 = result;
            }
            else {
                _this.notselected = true;
                _this.selected1 = _this.dropdown;
            }
        });
        this.LoadData();
    };
    AppTimelinePage.prototype.logout = function () {
        this.databaseService.logout();
    };
    AppTimelinePage.prototype.ionViewDidLoad = function () {
        this.getHCPid();
        this.sortField("start");
        this.order = -1;
    };
    AppTimelinePage.prototype.ionViewWillEnter = function () {
        this.sortField("start");
    };
    AppTimelinePage.prototype.ionViewDidEnter = function () {
        this.sortField("start");
    };
    AppTimelinePage.prototype.LoadData = function (selectedDate) {
        if (selectedDate === void 0) { selectedDate = ""; }
        this.WheelData = [];
        this.getWheelActivity(selectedDate);
        this.getWheelEventData(selectedDate);
        this.getWheelMedicineData(selectedDate);
        this.GetAllSettingsData();
    };
    AppTimelinePage.prototype.loadWheel = function () {
        var abc = this;
        setTimeout(function () {
            $(".pre-definedlist").show();
            abc.numNodes = (abc.defaultMinutes == 30) ? 48 : 96;
            abc.calMinute = abc.numNodes / 12;
            abc.updateClockHours();
            abc.GenerateCareGraphs();
        }, 1000);
    };
    AppTimelinePage.prototype.getWheelActivity = function (date) {
        var _this = this;
        if (date === void 0) { date = ""; }
        var self = this;
        // let Todaydate = this.getDate();
        // let Todaydate = new Date(this.selectedDate);
        // Todaydate = new Date();
        // console.log('current date', Todaydate);
        console.log(date);
        var Todaydate = (date != "") ? date : this.getDate();
        var webServiceData = {};
        webServiceData.action = "getactivitywheeldata";
        webServiceData.currentdate = Todaydate;
        webServiceData.userid = this.patientData.userid
            ? this.patientData.userid
            : this.userLoginData.id;
        webServiceData.appsecret = this.appsecret;
        if (this.patientType == "independent") {
            webServiceData.hcpid = 0;
        }
        else if (this.userLoginData.usertype == "nurse" ||
            this.userLoginData.usertype == "doctor" ||
            (this.userLoginData.usertype == "patient" &&
                this.patientType == "dependent")) {
            webServiceData.hcpid = this.hcpid;
        }
        else {
            webServiceData.hcpid = this.userLoginData.id;
        }
        var activity = self.http.post(this.baseurl + "wheeldata.php", webServiceData).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            if (result.status == "success") {
                _this.WheelActivityData = [];
                _this.WheelActivityData = result.data;
            }
            else {
                _this.WheelActivityData = [];
                // self.toastCtrl.presentToast("There is a No data Found");
            }
            _this.ActivityArrar = [];
            _this.ActivityArrar = self.WheelActivityData;
            // for(var c = 0; c < this.WheelActivityData.length; c++ ) {
            //   this.ActivityArrar.push(this.WheelActivityData[c].Activity);
            // }
            _this.myColor = [];
            _this.BorderColor = [];
            _this.name = [];
            _this.wheelActivityColor = [];
            _this.activityStartTime = [];
            _this.activityEndTime = [];
            _this.activityStatus = [];
            _this.activityDescription = [];
            for (var D = 0; D < _this.ActivityArrar.length; D++) {
                _this.myColor.push(_this.ActivityArrar[D].color);
                _this.name.push(_this.ActivityArrar[D].name);
                _this.BorderColor.push(_this.toObject(_this.myColor[D]));
                // if((<any>Object).keys(this.ActivityArrar[D]).length == 1) {
                //   this.myColor.push(this.ActivityArrar[D][0].color);
                //   this.name.push(this.ActivityArrar[D][0].name);
                //   this.BorderColor.push(this.toObject(this.myColor[D]));
                // }
            }
            for (var d = 0; d < _this.WheelActivityData.length; d++) {
                _this.WheelActivityData[d].color = _this.myColor[d];
                _this.WheelActivityData[d].name = _this.name[d];
                _this.WheelActivityData[d].bordercolor = _this.BorderColor[d];
                _this.WheelActivityData[d].source = "Activity";
                _this.activityStartTime.push(_this.WheelActivityData[d].start);
                _this.activityEndTime.push(_this.WheelActivityData[d].end);
                _this.wheelActivityColor.push(_this.WheelActivityData[d].color);
                _this.activityStatus.push(_this.WheelActivityData[d].name);
                _this.activityDescription.push(_this.WheelActivityData[d].description);
                _this.endmin = [];
                _this.end = [];
                for (var C = 0; C < _this.activityStartTime.length; C++) {
                    var End = _this.activityEndTime[C].split(":");
                    _this.activityEndHours = End[0];
                    _this.end.push(_this.activityEndHours);
                    _this.endmin.push(End[1]);
                }
            }
            _this.WheelData = _this.WheelData.concat(_this.WheelActivityData);
            _this.calltoupdateStatus("activity");
            // this.status = 'activityoxygen';
            activity.unsubscribe();
        });
    };
    AppTimelinePage.prototype.getWheelEventData = function (date) {
        var _this = this;
        var self = this;
        var Todaydate = this.getDate();
        var webServiceData = {};
        webServiceData.eventaction = "geteventwheeldata";
        webServiceData.currentdate = Todaydate;
        webServiceData.userid = this.patientData.userid
            ? this.patientData.userid
            : this.userLoginData.id;
        webServiceData.appsecret = this.appsecret;
        if (this.patientType == "independent") {
            webServiceData.hcpid = 0;
        }
        else if (this.userLoginData.usertype == "nurse" ||
            this.userLoginData.usertype == "doctor" ||
            (this.userLoginData.usertype == "patient" &&
                this.patientType == "dependent")) {
            webServiceData.hcpid = this.hcpid;
        }
        else {
            webServiceData.hcpid = this.userLoginData.id;
        }
        var event = self.http.post(this.baseurl + "wheeldata.php", webServiceData)
            .subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            if (result.status == "success") {
                _this.WheelEventData = [];
                _this.WheelEventData = result.data;
            }
            else {
                _this.WheelEventData = [];
                // self.toastCtrl.presentToast("There is a No data Found");
            }
            _this.EventArrar = [];
            _this.EventArrar = self.WheelEventData;
            _this.EventColor = [];
            _this.Eventname = [];
            _this.startTime = [];
            _this.endTime = [];
            _this.wheelEventColor = [];
            _this.status = [];
            _this.sleepDescription = [];
            for (var D = 0; D < _this.EventArrar.length; D++) {
                _this.EventColor.push(_this.EventArrar[D].eventcolor);
                _this.Eventname.push(_this.EventArrar[D].ename);
                _this.BorderColor.push(_this.toObject(_this.myColor[D]));
                // if((<any>Object).keys(this.EventArrar[D]).length == 1) {
                //  this.EventColor.push(this.EventArrar[D][0].color);
                //  this.Eventname.push(this.EventArrar[D][0].Eventname);
                // }
            }
            for (var d = 0; d < _this.WheelEventData.length; d++) {
                _this.WheelEventData[d].color = _this.EventColor[d];
                _this.WheelEventData[d].Eventname = _this.Eventname[d];
                _this.WheelEventData[d].source = "Event";
                _this.startTime.push(_this.WheelEventData[d].start);
                _this.endTime.push(_this.WheelEventData[d].end);
                _this.status.push(_this.Eventname[d]);
                _this.sleepDescription.push(_this.WheelEventData[d].description);
                _this.wheelEventColor.push(_this.WheelEventData[d].color);
            }
            _this.WheelData = _this.WheelData.concat(_this.WheelEventData);
            _this.calltoupdateStatus("sleep");
            // this.status = 'sleepawake';
            event.unsubscribe();
        });
    };
    AppTimelinePage.prototype.getWheelMedicineData = function (date) {
        var _this = this;
        var self = this;
        var Todaydate = this.getDate();
        var webServiceData = {};
        webServiceData.action = "getmedicinewheeldata";
        webServiceData.currentdate = Todaydate;
        webServiceData.userid = this.patientData.userid
            ? this.patientData.userid
            : this.userLoginData.id;
        webServiceData.appsecret = this.appsecret;
        if (this.patientType == "independent") {
            webServiceData.hcpid = 0;
        }
        else if (this.userLoginData.usertype == "nurse" ||
            this.userLoginData.usertype == "doctor" ||
            (this.userLoginData.usertype == "patient" &&
                this.patientType == "dependent")) {
            webServiceData.hcpid = this.hcpid;
        }
        else {
            webServiceData.hcpid = this.userLoginData.id;
        }
        var medicine = self.http.post(this.baseurl + "wheeldata.php", webServiceData).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            if (result.status == "success") {
                _this.WheelMedicineData = [];
                _this.WheelMedicineData = result.data;
            }
            else {
                _this.WheelMedicineData = [];
                // self.toastCtrl.presentToast("There is a No data Found");
            }
            // else{
            //   this.WheelData =  this.WheelData.concat(this.WheelMedicineData) ;
            //   this.toastCtrl.presentToast("There is a no data available.");
            // }
            _this.MedicineArrar = [];
            _this.MedicineArrar = self.WheelMedicineData;
            // for(var z = 0; z < this.WheelMedicineData.length; z++){
            //  this.MedicineArrar.push(this.WheelMedicineData[z].Medicine)
            // }
            _this.MedicineColor = [];
            _this.medicineName = [];
            _this.medicineStart = [];
            _this.wheelMedicineColor = [];
            _this.medicineStatus = [];
            _this.medicineDescription = [];
            for (var D = 0; D < _this.MedicineArrar.length; D++) {
                _this.MedicineColor.push(_this.MedicineArrar[D].color);
                _this.medicineName.push(_this.MedicineArrar[D].medicinename);
            }
            for (var d = 0; d < _this.WheelMedicineData.length; d++) {
                _this.WheelMedicineData[d].color = _this.MedicineColor[d];
                _this.WheelMedicineData[d].medicineName = _this.medicineName[d];
                _this.WheelMedicineData[d].source = "Medicine";
                _this.medicineStart.push(_this.WheelMedicineData[d].start);
                _this.wheelMedicineColor.push(_this.WheelMedicineData[d].color);
                _this.medicineStatus.push(_this.WheelMedicineData[d].medicineName);
                _this.medicineDescription.push(_this.WheelMedicineData[d].description);
            }
            _this.WheelData = _this.WheelData.concat(_this.WheelMedicineData);
            _this.calltoupdateStatus("medicine");
            // this.status = 'medicinezonegran';
            medicine.unsubscribe();
        }, function (err) {
            console.log(err);
        });
    };
    AppTimelinePage.prototype.notify = function (event) {
        var date = new Date(this.selectedDate);
        var selectedDate = event.year + "-" + event.month + "-" + event.day;
        if (selectedDate == undefined) {
            this.WheelData.selectedDate.getTime();
            date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        }
        this.updateClockHours();
        this.LoadData(selectedDate);
    };
    AppTimelinePage.prototype.getTime = function () {
        var time = new Date(this.date);
        return time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
    };
    AppTimelinePage.prototype.getFormatedTime = function () {
        var date = new Date(this.date);
        var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
        var am_pm = date.getHours() >= 12;
        var minutes = date.getMinutes() < 10
            ? "0" + date.getMinutes()
            : date.getMinutes();
        var time = hours + ":" + minutes + " ";
        return time;
    };
    AppTimelinePage.prototype.sortField = function (fieldname) {
        // this.column = fieldname;
        this.descending = !this.descending;
        this.order = this.descending ? 1 : -1;
    };
    AppTimelinePage.prototype.toObject = function (myColor) {
        var result = [];
        for (var i = 0; i < this.colors.length; i++) {
            result[this.colors[i]] = this.bordercolors[i];
        }
        return result[myColor];
    };
    AppTimelinePage.prototype.addActivity = function () {
        var _this = this;
        var modal = this.modalCtrl.create("AppAddActivityModelPage", {
            "patientData": this.patientData,
            "patientSettings": this.SettingsData,
            "date": this.getDate(),
            "activityData": this.notifyactivity
        });
        modal.present();
        modal.onDidDismiss(function (data) {
            _this.navParams.data.type = "";
            _this.WheelData = [];
            _this.LoadData();
            _this.updateClockHours();
            _this.GetAllSettingsData();
        });
        // this.WheelData = [];
    };
    AppTimelinePage.prototype.addMedicien = function () {
        var _this = this;
        var modal = this.modalCtrl.create("AppAddMedicineModelPage", {
            "patientData": this.patientData,
            "date": this.getDate(),
            "patientSettings": this.SettingsData,
            "medicineData": this.notifymedicine
        });
        modal.present();
        modal.onDidDismiss(function (data) {
            _this.navParams.data.type = "";
            _this.WheelData = [];
            _this.LoadData();
            _this.updateClockHours();
            _this.GetAllSettingsData();
        });
        // this.WheelData = [];
    };
    AppTimelinePage.prototype.addStatus = function () {
        var _this = this;
        var modal = this.modalCtrl.create("AppAddCurrentStatusModelPage", {
            "patientData": this.patientData,
            "date": this.getDate(),
            "patientSettings": this.SettingsData,
            "eventData": this.notifyevent
        });
        modal.present();
        modal.onDidDismiss(function (data) {
            _this.navParams.data.type = "";
            _this.WheelData = [];
            _this.LoadData();
            _this.updateClockHours();
            _this.GetAllSettingsData();
        });
    };
    AppTimelinePage.prototype.GetAllSettingsData = function () {
        var _this = this;
        var self = this;
        self.ActivityWheelData = [];
        self.MedicineWheelData = [];
        self.StatusWheelData = [];
        this.patientMasterActivityData.action = "getactivitymasterforwheel";
        this.patientMasterActivityData.appsecret = this.appsecret;
        if (this.patientType == "independent" ||
            this.userLoginData.usertype == "healthcareprovider") {
            this.patientMasterActivityData.userid = this.userLoginData.id;
        }
        else {
            this.patientMasterActivityData.userid = this.hcpid;
        }
        this.http.post(this.baseurl + "getdata.php", this.patientMasterActivityData)
            .subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            if (result.status == "success") {
                if (result.data !== null) {
                    self.ActivityWheelData.push(result.data);
                    self.activityList = result.data;
                    var notify = _this.navParams.data ? _this.navParams.data.type : '';
                    if (notify == "notification") {
                        _this.notifyactivity = _this.navParams.data ? _this.navParams.data.activitynot : '';
                        if (_this.notifyactivity) {
                            _this.addActivity();
                            _this.navParams.data.type = "";
                        }
                    }
                }
                else {
                    self.ActivityWheelData = [];
                    self.activityList = [];
                }
            }
        }, function (err) {
            console.log(err);
        });
        this.patientMasterMedicineData.action = "getmedicinemasterforwheel";
        this.patientMasterMedicineData.appsecret = this.appsecret;
        if (this.patientType == "independent" ||
            this.userLoginData.usertype == "healthcareprovider") {
            this.patientMasterMedicineData.userid = this.userLoginData.id;
        }
        else {
            this.patientMasterMedicineData.userid = this.hcpid;
        }
        this.http.post(this.baseurl + "getdata.php", this.patientMasterMedicineData)
            .subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            if (result.status == "success") {
                if (result.data !== null) {
                    self.MedicineWheelData.push(result.data);
                    self.medicineList = result.data;
                    var notify = _this.navParams.data ? _this.navParams.data.type : '';
                    if (notify == "notification") {
                        _this.notifymedicine = _this.navParams.data ? _this.navParams.data.medicinenot : '';
                        if (_this.notifymedicine) {
                            _this.addMedicien();
                            _this.navParams.data.type = "";
                        }
                    }
                }
                else {
                    self.MedicineWheelData = [];
                    self.medicineList = [];
                }
            }
        }, function (err) {
            console.log(err);
        });
        this.patientMasterEventData.action = "geteventmasterforwheel";
        this.patientMasterEventData.appsecret = this.appsecret;
        if (this.patientType == "independent" ||
            this.userLoginData.usertype == "healthcareprovider") {
            this.patientMasterEventData.userid = this.userLoginData.id;
        }
        else {
            this.patientMasterEventData.userid = this.hcpid;
        }
        this.http.post(this.baseurl + "getdata.php", this.patientMasterEventData)
            .subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            if (result.status == "success") {
                if (result.data !== null) {
                    self.StatusWheelData.push(result.data);
                    self.wellbeaningList = result.data;
                    var notify = _this.navParams.data ? _this.navParams.data.type : '';
                    if (notify == "notification") {
                        _this.notifyevent = _this.navParams.data ? _this.navParams.data.eventnot : '';
                        if (_this.notifyevent) {
                            _this.addStatus();
                            _this.navParams.data.type = "";
                        }
                    }
                }
                else {
                    self.StatusWheelData = [];
                    self.wellbeaningList = [];
                }
            }
        }, function (err) {
            console.log(err);
        });
        this.storage.get("user").then(function (patientData) {
            ////////// Event Data Start //////
            _this.patientMasterEventData.action = "getpatienteventdata";
            _this.patientMasterEventData.userid = patientData
                ? patientData.id
                : _this.userlogindata.id;
            _this.patientMasterEventData.appsecret = _this.appsecret;
            if (_this.userLoginData.usertype == "nurse" ||
                _this.userLoginData.usertype == "doctor" ||
                (_this.userLoginData.usertype == "patient" &&
                    _this.patientType == "dependent")) {
                _this.patientMasterEventData.hcpid = _this.hcpid;
            }
            else if (_this.patientType == "dependent") {
                _this.patientMasterEventData.hcpid = 0;
            }
            else {
                _this.patientMasterEventData.hcpid = _this.userLoginData.id;
            }
            _this.http.post(_this.baseurl + "getdata.php", _this.patientMasterEventData)
                .subscribe(function (data) {
                var result = JSON.parse(data["_body"]);
                if (result.status == "success") {
                    if (result.data !== null) {
                        _this.wellbeaningList = (result.data == null ? [] : result.data);
                        // this.MedicineData = result.data;
                        // this.SearchData = result.data;
                    }
                    else {
                        _this.data = [];
                        _this.data.keys = [];
                        _this.wellbeaningList = [];
                        // this.SearchData = [];
                    }
                }
            }, function (err) {
                console.log(err);
            });
            ////////// Event Data End //////
            ////////// Medicine Data Start //////
            _this.patientMasterMedicineData.action = "getpatientmedicinedata";
            _this.patientMasterMedicineData.userid = patientData
                ? patientData.id
                : _this.userlogindata.id;
            _this.patientMasterMedicineData.appsecret = _this.appsecret;
            if (_this.userLoginData.usertype == "nurse" ||
                _this.userLoginData.usertype == "doctor" ||
                (_this.userLoginData.usertype == "patient" &&
                    _this.patientType == "dependent")) {
                _this.patientMasterMedicineData.hcpid = _this.hcpid;
            }
            else if (_this.patientType == "dependent") {
                _this.patientMasterMedicineData.hcpid = 0;
            }
            else {
                _this.patientMasterMedicineData.hcpid = _this.userLoginData.id;
            }
            _this.http.post(_this.baseurl + "getdata.php", _this.patientMasterMedicineData).subscribe(function (data) {
                var result = JSON.parse(data["_body"]);
                if (result.status == "success") {
                    if (result.data !== null) {
                        _this.medicineList = (result.data == null ? [] : result.data);
                        // this.MedicineData = result.data;
                        // this.SearchData = result.data;
                    }
                    else {
                        _this.data = [];
                        _this.data.keys = [];
                        _this.medicineList = [];
                        // this.SearchData = [];
                        _this.toastCtrl.presentToast("There is a no medicine data available.");
                    }
                }
                else {
                    // this.loadingService.hide();
                    // this.navCtrl.setRoot("AppDashboardPage");
                    // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                }
            }, function (err) {
                console.log(err);
            });
            ////////// Medicine Data End //////
            ////////// Activity Data Start //////
            _this.patientMasterActivityData.action = "getpatientactivitydata";
            _this.patientMasterActivityData.userid = patientData
                ? patientData.id
                : _this.userlogindata.id;
            _this.patientMasterActivityData.appsecret = _this.appsecret;
            if (_this.userLoginData.usertype == "nurse" ||
                _this.userLoginData.usertype == "doctor" ||
                (_this.userLoginData.usertype == "patient" &&
                    _this.patientType == "dependent")) {
                _this.patientMasterActivityData.hcpid = _this.hcpid;
            }
            else if (_this.patientType == "dependent") {
                _this.patientMasterActivityData.hcpid = 0;
            }
            else {
                _this.patientMasterActivityData.hcpid = _this.userLoginData.id;
            }
            _this.http.post(_this.baseurl + "getdata.php", _this.patientMasterActivityData).subscribe(function (data) {
                var result = JSON.parse(data["_body"]);
                if (result.status == "success") {
                    if (result.data !== null) {
                        _this.activityList = (result.data == null ? [] : result.data);
                        // this.MedicineData = result.data;
                        // this.SearchData = result.data;
                    }
                    else {
                        _this.data = [];
                        _this.data.keys = [];
                        _this.activityList = [];
                        // this.SearchData = [];
                        _this.toastCtrl.presentToast("There is a no activity data available.");
                    }
                }
                else {
                    // this.loadingService.hide();
                    // this.navCtrl.setRoot("AppDashboardPage");
                    // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                }
            }, function (err) {
                console.log(err);
            });
            ////////// Activity Data End //////
        });
        this.SettingsData = {
            patientActivityMasterData: self.ActivityWheelData,
            patientMedicineMasterData: self.MedicineWheelData,
            patientEventMasterData: self.StatusWheelData,
        };
    };
    AppTimelinePage.prototype.getDate = function () {
        // console.log(this.selectedDate);
        var date = new Date(this.selectedDate);
        // return date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" +
            date.getDate();
    };
    AppTimelinePage.prototype.editWheelActivity = function (ActivityData) {
        var _this = this;
        var modal = this.modalCtrl.create("AppAddActivityModelPage", {
            "data": ActivityData,
            "patientSettings": this.SettingsData,
            "date": this.getDate(),
            "patient": this.patientData,
            "action": "edit",
        });
        modal.present();
        modal.onDidDismiss(function (data) {
            _this.WheelData = [];
            _this.LoadData();
            _this.updateClockHours();
        });
        // this.LoadData();
    };
    AppTimelinePage.prototype.EditEvent = function (EventData) {
        var _this = this;
        var modal = this.modalCtrl.create("AppAddCurrentStatusModelPage", {
            "data": EventData,
            "patientSettings": this.SettingsData,
            "date": this.getDate(),
            "patient": this.patientData,
            "action": "edit",
        });
        modal.present();
        modal.onDidDismiss(function (data) {
            _this.WheelData = [];
            _this.LoadData();
            _this.updateClockHours();
        });
    };
    AppTimelinePage.prototype.EditMedicine = function (MedicineData) {
        var _this = this;
        var modal = this.modalCtrl.create("AppAddMedicineModelPage", {
            "data": MedicineData,
            "patientSettings": this.SettingsData,
            "date": this.getDate(),
            "patient": this.patientData,
            "action": "edit",
        });
        modal.present();
        modal.onDidDismiss(function (data) {
            _this.WheelData = [];
            _this.LoadData();
            _this.updateClockHours();
        });
        // this.LoadData();
        //  this.WheelData = [];
    };
    AppTimelinePage.prototype.deleteWheelActivityData = function (item) {
        var _this = this;
        console.log(item);
        var alert = this.alertCtrl.create({
            title: "Confirm Delete",
            message: "Do you want to delete this record?",
            buttons: [{
                    text: "Cancel",
                    role: "cancel",
                    handler: function () {
                        // console.log('Cancel clicked');
                    },
                }, {
                    text: "Delete",
                    handler: function () {
                        var self = _this;
                        var Todaydate = self.getDate();
                        var webServiceData = {};
                        webServiceData.action = "deleteactivitywheeldata";
                        webServiceData.id = item.id;
                        webServiceData.appsecret = _this.appsecret;
                        self.http.post(_this.baseurl + "wheeldata.php", webServiceData)
                            .subscribe(function (data) {
                            var result = JSON.parse(data["_body"]);
                            console.log(result);
                            if (result.status == "success") {
                                self.toastCtrl.presentToast("Activity deleted successfully");
                                self.LoadData(Todaydate);
                                self.updateClockHours();
                            }
                            else {
                                // self.toastCtrl.presentToast("There is a problem deleting data");
                            }
                        }, function (err) {
                            console.log(err);
                        });
                    },
                }],
        });
        alert.present();
    };
    AppTimelinePage.prototype.deleteWheelEventData = function (item) {
        var _this = this;
        console.log(item.id);
        var alert = this.alertCtrl.create({
            title: "Confirm Delete",
            message: "Do you want to delete this record?",
            buttons: [{
                    text: "Cancel",
                    role: "cancel",
                    handler: function () {
                    },
                }, {
                    text: "Delete",
                    handler: function () {
                        var self = _this;
                        var Todaydate = self.getDate();
                        var webServiceData = {};
                        webServiceData.action = "deletestatuswheeldata";
                        webServiceData.id = item.id;
                        webServiceData.appsecret = _this.appsecret;
                        self.http.post(_this.baseurl + "wheeldata.php", webServiceData)
                            .subscribe(function (data) {
                            var result = JSON.parse(data["_body"]);
                            console.log(result);
                            if (result.status == "success") {
                                self.toastCtrl.presentToast("Event deleted successfully");
                                self.LoadData(Todaydate);
                                self.updateClockHours();
                            }
                            else {
                                // self.toastCtrl.presentToast("There is a problem deleting data");
                            }
                        }, function (err) {
                            console.log(err);
                        });
                    },
                }],
        });
        alert.present();
    };
    AppTimelinePage.prototype.deleteWheelMedicineData = function (item) {
        var _this = this;
        console.log(item.id);
        var alert = this.alertCtrl.create({
            title: "Confirm Delete",
            message: "Do you want to delete this record?",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: function () {
                    },
                },
                {
                    text: "Delete",
                    handler: function () {
                        var self = _this;
                        var Todaydate = self.getDate();
                        var webServiceData = {};
                        webServiceData.action = "deletemedicinewheeldata";
                        webServiceData.id = item.id;
                        webServiceData.appsecret = _this.appsecret;
                        self.http.post(_this.baseurl + "wheeldata.php", webServiceData)
                            .subscribe(function (data) {
                            var result = JSON.parse(data["_body"]);
                            console.log(result);
                            if (result.status == "success") {
                                self.toastCtrl.presentToast("Medicine deleted successfully");
                                self.LoadData(Todaydate);
                                self.updateClockHours();
                            }
                            else {
                                // self.toastCtrl.presentToast("There is a problem deleting data");
                            }
                        }, function (err) {
                            console.log(err);
                        });
                    },
                },
            ],
        });
        alert.present();
    };
    // Wheel Segment start
    AppTimelinePage.prototype.updateClockHours = function () {
        // var actualClockTxt = "";
        var clockStartPos = 6;
        var calMinutePos = 0;
        var abc = this;
        abc.whichClockforTxt = (abc.defaultMinutes == 15) ? 0 : 1;
        for (var i = 0; i < abc.numNodes; i++) {
            if ((abc.calMinute == 8 && i % 4 == 0) || (abc.calMinute == 4 && i % 2 == 0)) {
                calMinutePos = 0;
            }
            var actualClockTxt = (calMinutePos == 0)
                ? (clockStartPos == 0 ? "24/0" : clockStartPos)
                : (clockStartPos - 1 < 0)
                    ? "23." + abc.minuteTxt[abc.whichClockforTxt][calMinutePos - 1]
                    : (clockStartPos - 1) + "." +
                        abc.minuteTxt[abc.whichClockforTxt][calMinutePos - 1];
            calMinutePos++;
            abc.arrClockSleep[actualClockTxt] = abc.sleepColors["default"];
            abc.arrClockMedicine[actualClockTxt] = abc.medicineColors["default"];
            abc.arrClockActivity[actualClockTxt] = abc.activityColors["default"];
            abc.arrReturn[actualClockTxt] = new Array();
            abc.arrReturn[actualClockTxt]["sleep"] = abc
                .arrReturn[actualClockTxt]["medicine"] = abc
                .arrReturn[actualClockTxt]["activity"] = "";
            abc.arrReturn[actualClockTxt]["sleepText"] = abc
                .arrReturn[actualClockTxt]["medicineText"] = abc
                .arrReturn[actualClockTxt]["activityText"] = abc
                .arrReturn[actualClockTxt]["activityTextTime"] = abc
                .arrReturn[actualClockTxt]["activityTextDiscripation"] = abc
                .arrReturn[actualClockTxt]["sleepTextTime"] = abc
                .arrReturn[actualClockTxt]["sleepTextDiscripation"] = abc
                .arrReturn[actualClockTxt]["medicineTextTime"] = abc
                .arrReturn[actualClockTxt]["medicineTextDiscripation"] =
                abc.arrReturn[actualClockTxt]["activityColor"] = abc
                    .arrReturn[actualClockTxt]["medicineColor"] = abc
                    .arrReturn[actualClockTxt]["sleepColor"] = "";
            abc.arrReturn[actualClockTxt]["activityStartPoint"] = abc
                .arrReturn[actualClockTxt]["activityEndPoint"] = "";
            clockStartPos = (clockStartPos >= 24)
                ? 0
                : ((abc.calMinute == 8 && i % 4 == 0) ||
                    (abc.calMinute == 4 && i % 2 == 0))
                    ? (clockStartPos + 1)
                    : clockStartPos;
        }
    };
    AppTimelinePage.prototype.resetData = function () {
        $('input[name="sleep_starttime"]').val("");
        $('input[name="sleep_endtime"]').val("");
        $('select[name="sleepstatus"]').val("default");
        $('input[name="medicine_time"]').val("");
        $('select[name="medicinestatus"]').val("default");
        $('input[name="activity_starttime"]').val("");
        $('input[name="activity_endtime"]').val("");
        $('select[name="activitystatus"]').val("default");
    };
    AppTimelinePage.prototype.GenerateCareGraphs = function () {
        var abc = this;
        abc.drawActivity(250);
        abc.drawMedicine(220);
        abc.drawSleepCapsule(190);
        abc.drawClock(170);
    };
    AppTimelinePage.prototype.drawSleepCapsule = function (radiusVal) {
        var Sleepradius = radiusVal;
        var data = { title: "", color: "", discripation: "", time: "" };
        var numNodes = 96;
        var abc = this;
        var Sleepnodes = this.createNodes(numNodes, Sleepradius);
        var tip = d3.tip()
            .attr("class", "d3-tip")
            .offset([-10, 0])
            .html(function (d) {
            var title = d.title.split(",");
            var time = d.time.split(",");
            var discription = d.discripation.split(",");
            var color = d.color.split(",");
            var html = '<div class="activites_popup"> <div class="" style="text-align: left;"><strong>Wellbeing Status</strong></div>';
            for (var i = 0; i < title.length; i++) {
                html +=
                    '<div class="act_cont" style="padding-top:5px;"> <div class="activity" style="padding-top:10px; display:flex;"> <div class="point_dot" style="background: ' +
                        color[i] +
                        '; "></div> <span style="color: #81889f;display: flex;">' +
                        time[i] +
                        '</span><span style="padding-left:5px; color: #81889f;display: flex;">' +
                        title[i] +
                        '</span> </div> <div class="activity" style="padding-top:10px; display:flex;"><span padding-left:="" 3px;="" style="color: #81889f;display: flex;">' +
                        discription[i] + "</span></div> </div>";
            }
            html += "</div>";
            return html;
            // return '<div class=""><div><div class="" style="text-align: left; padding: 0px 10px 10px 1px; "><strong>Current status</strong></div><div class="" style="display: flex; "><div class="point_dot" style="background: '+d.color+'; "></div><span style="color: #81889f;display: flex;">'+d.time+'</span><span style="padding-left:5px; color: #81889f;display: flex;">'+d.title+'</span></div><div class="" style="text-align: left; padding: 15px 10px 8px 0px; "><strong>Discription</strong></div><div class="" style="display: flex; "><span padding-left: 3px; style="color: #81889f;display: flex;">'+d.discripation+'</span></div></div></div>';
        });
        var createSleepSvg = function (radius, callback) {
            var svg = d3.select("#sleepcanvas").append("svg:svg")
                .attr("width", (radius * 2) + 50)
                .attr("height", (radius * 2) + 50);
            svg.call(tip);
            callback(svg);
        };
        var createCapsule = function (svg, nodes, elementRadius, numNodes) {
            var anglePerCapsule = 360 / abc.numNodes;
            var rotateAngle = 0;
            var element = svg.selectAll("circle").data(nodes).enter().append("svg:rect")
                .attr("x", function (d, i) {
                return d.x - 5;
            })
                .attr("y", function (d, i) {
                return d.y - 8;
            })
                .attr("rx", elementRadius)
                .attr("ry", elementRadius)
                .attr("width", 20)
                .attr("height", 10.1)
                .attr("fill", function (d, i) {
                return abc.arrClockSleep[d.clockActualTxt];
            })
                .attr("stroke", function (d, i) {
                return d.defaultCapsuleStroke;
            })
                .attr("stroke-width", 0.2)
                .attr("transform", function (d, i) {
                rotateAngle = anglePerCapsule * i;
                return "rotate(" + rotateAngle + "," + d.x + "," + d.y + ")";
            })
                .on("click", function (d, i) {
                if (abc.arrReturn[d.clockActualTxt]["sleepText"] != "") {
                    data.title = abc.arrReturn[d.clockActualTxt]["sleep"];
                    data.color = abc.arrReturn[d.clockActualTxt]["sleepColor"];
                    data.time = abc.arrReturn[d.clockActualTxt]["sleepTextTime"];
                    data.discripation =
                        abc.arrReturn[d.clockActualTxt]["sleepTextDiscripation"];
                    tip.show(data, i);
                }
            })
                .on("focusout", function (d, i) {
                if (abc.arrReturn[d.clockActualTxt]["sleepText"] != "") {
                    data.title = abc.arrReturn[d.clockActualTxt]["sleep"];
                    data.color = abc.arrReturn[d.clockActualTxt]["sleepColor"];
                    data.time = abc.arrReturn[d.clockActualTxt]["sleepTextTime"];
                    data.discripation =
                        abc.arrReturn[d.clockActualTxt]["sleepTextDiscripation"];
                    tip.hide(data, i);
                }
            });
        };
        createSleepSvg(Sleepradius, function (svg) {
            createCapsule(svg, Sleepnodes, 6, abc.numNodes);
        });
    };
    AppTimelinePage.prototype.drawMedicine = function (radiusVal) {
        var Medicineradius = radiusVal;
        var data = { title: "", color: "", discripation: "", time: "" };
        var abc = this;
        var Medicinenodes = abc.createNodes(abc.numNodes, Medicineradius);
        var tip = d3.tip()
            .attr("class", "d3-tip")
            .offset([-10, 0])
            .html(function (d) {
            var title = d.title.split(",");
            var time = d.time.split(",");
            var discription = d.discripation.split(",");
            var color = d.color.split(",");
            var html = '<div class="activites_popup"> <div class="" style="text-align: left;"><strong>Medicines</strong></div>';
            for (var i = 0; i < title.length; i++) {
                html +=
                    '<div class="act_cont" style="padding-top:5px;"> <div class="activity" style="padding-top:10px; display:flex;"> <div class="point_dot" style="background: ' +
                        color[i] +
                        '; "></div> <span style="color: #81889f;display: flex;">' +
                        time[i] +
                        '</span><span style="padding-left:5px; color: #81889f;display: flex;">' +
                        title[i] +
                        '</span> </div> <div class="activity" style="padding-top:10px; display:flex;"><span padding-left:="" 3px;="" style="color: #81889f;display: flex;">' +
                        discription[i] + "</span></div> </div>";
            }
            html += "</div>";
            return html;
            // return '<div class=""><div><div class="" style="text-align: left; padding: 0px 10px 10px 1px; "><strong>Medicines</strong></div><div class="" style="display: flex; "><div class="point_dot" style="background: '+d.color+'; "></div><span style="color: #81889f;display: flex;">'+d.time+'</span><span style="padding-left:5px; color: #81889f;display: flex;">'+d.title+'</span></div><div class="" style="text-align: left; padding: 15px 10px 8px 0px; "><strong>Discription</strong></div><div class="" style="display: flex; "><span padding-left: 3px; style="color: #81889f;display: flex;">'+d.discripation+'</span></div></div></div>';
        });
        var createMedicineSvg = function (radius, callback) {
            //d3.selectAll('svg').remove();
            var svg = d3.select("#medicinecanvas").append("svg:svg")
                .attr("width", (radius * 2) + 50)
                .attr("height", (radius * 2) + 50);
            svg.call(tip);
            callback(svg);
        };
        var createMedicine = function (svg, nodes, elementRadius, numNodes) {
            var anglePerCapsule = 360 / abc.numNodes;
            var rotateAngle = 0;
            var element = svg.selectAll("circle").data(nodes).enter().append("svg:rect")
                .attr("x", function (d, i) {
                return d.x - 5;
            })
                .attr("y", function (d, i) {
                return d.y - 8;
            })
                .attr("rx", elementRadius)
                .attr("ry", elementRadius)
                .attr("width", 20)
                .attr("height", 10.1)
                .attr("fill", function (d, i) {
                return abc.arrClockMedicine[d.clockActualTxt];
            })
                .attr("stroke", "#2b3249")
                .attr("stroke-width", 0.2)
                .attr("transform", function (d, i) {
                rotateAngle = anglePerCapsule * i;
                return "rotate(" + rotateAngle + "," + d.x + "," + d.y + ")";
            })
                .on("click", function (d, i) {
                console.log("datsfsdfsda", abc);
                if (abc.arrReturn[d.clockActualTxt]["medicineText"] != "") {
                    data.title = abc.arrReturn[d.clockActualTxt]["medicine"];
                    data.color = abc.arrReturn[d.clockActualTxt]["medicineColor"];
                    data.time = abc.arrReturn[d.clockActualTxt]["medicineTextTime"];
                    data.discripation =
                        abc.arrReturn[d.clockActualTxt]["medicineTextDiscripation"];
                    tip.show(data, i);
                }
            })
                .on("focusout", function (d, i) {
                if (abc.arrReturn[d.clockActualTxt]["medicineText"] != "") {
                    data.title = abc.arrReturn[d.clockActualTxt]["medicine"];
                    data.color = abc.arrReturn[d.clockActualTxt]["medicineColor"];
                    data.time = abc.arrReturn[d.clockActualTxt]["medicineTextTime"];
                    data.discripation =
                        abc.arrReturn[d.clockActualTxt]["medicineTextDiscripation"];
                    tip.hide(data, i);
                }
            });
            element = svg.selectAll("circle").data(nodes).enter().append("svg:line")
                .attr("x1", function (d, i) {
                return d.x + 5;
            })
                .attr("y1", function (d, i) {
                return d.y - 8;
            })
                .attr("x2", function (d, i) {
                return d.x + 5;
            })
                .attr("y2", function (d, i) {
                return d.y + 2;
            })
                .attr("stroke", "#2b3249")
                .attr("stroke-width", 2)
                .attr("transform", function (d, i) {
                rotateAngle = anglePerCapsule * i;
                return "rotate(" + rotateAngle + "," + d.x + "," + d.y + ")";
            }).on("click", function (d, i) {
                console.log("abc", abc);
                if (abc.arrReturn[d.clockActualTxt]["medicineText"] != "") {
                    data.title = abc.arrReturn[d.clockActualTxt]["medicine"];
                    data.color = abc.arrReturn[d.clockActualTxt]["medicineColor"];
                    data.time = abc.arrReturn[d.clockActualTxt]["medicineTextTime"];
                    data.discripation =
                        abc.arrReturn[d.clockActualTxt]["medicineTextDiscripation"];
                    tip.show(data, i);
                }
            })
                .on("focusout", function (d, i) {
                if (abc.arrReturn[d.clockActualTxt]["medicineText"] != "") {
                    data.title = abc.arrReturn[d.clockActualTxt]["medicine"];
                    data.color = abc.arrReturn[d.clockActualTxt]["medicineColor"];
                    data.time = abc.arrReturn[d.clockActualTxt]["medicineTextTime"];
                    data.discripation =
                        abc.arrReturn[d.clockActualTxt]["medicineTextDiscripation"];
                    tip.hide(data, i);
                }
            });
        };
        createMedicineSvg(Medicineradius, function (svg) {
            createMedicine(svg, Medicinenodes, 6, 96);
        });
    };
    AppTimelinePage.prototype.drawActivity = function (radiusVal) {
        var abc = this;
        var data = { title: "", color: "", discripation: "", time: "" };
        var Activityradius = radiusVal;
        var Activitynodes = abc.createNodes(abc.numNodes, Activityradius);
        var tip = d3.tip()
            .attr("class", "d3-tip")
            .offset([-10, 0])
            .html(function (d) {
            var title = d.title.split(",");
            var time = d.time.split(",");
            var discription = d.discripation.split(",");
            var color = d.color.split(",");
            var html = '<div class="activites_popup"> <div class="" style="text-align: left;"><strong>Activities</strong></div>';
            for (var i = 0; i < title.length; i++) {
                html +=
                    '<div class="act_cont" style="padding-top:5px;"> <div class="activity" style="padding-top:10px; display:flex;"> <div class="point_dot" style="background: ' +
                        color[i] +
                        '; "></div> <span style="color: #81889f;display: flex;">' +
                        time[i] +
                        '</span><span style="padding-left:5px; color: #81889f;display: flex;">' +
                        title[i] +
                        '</span> </div> <div class="activity" style="padding-top:10px; display:flex;"><span padding-left:="" 3px;="" style="color: #81889f;display: flex;">' +
                        discription[i] + "</span></div> </div>";
            }
            html += "</div>";
            return html;
            // return '<div class=""><div><div class="" style="text-align: left; padding: 0px 10px 10px 1px; "><strong>Activities</strong></div><div class="" style="display: flex; "><div class="point_dot" style="background: '+d.color+'; "></div><span style="color: #81889f;display: flex;">'+d.time+'</span><span style="padding-left:5px; color: #81889f;display: flex;">'+d.title+'</span></div><div class="" style="text-align: left; padding: 15px 10px 8px 0px; "><strong>Discription</strong></div><div class="" style="display: flex; "><span padding-left: 3px; style="color: #81889f;display: flex;">'+d.discripation+'</span></div></div></div>';
        });
        var createActivitySvg = function (radius, callback) {
            var svg = d3.select("#activitycanvas1").append("svg:svg")
                .attr("width", (radius * 2) + 50)
                .attr("height", (radius * 2) + 50);
            svg.call(tip);
            callback(svg);
        };
        var createActivity = function (svg, nodes, elementRadius, numNodes) {
            var anglePerCapsule = 360 / numNodes;
            var rotateAngle = 0;
            $.each(nodes, function (d, i) {
                var nextElement = d + 1;
                if (typeof nodes[nextElement] === "undefined") {
                    nextElement = 0;
                }
                var element = svg.append("svg:line")
                    .attr("x1", i.x - 5)
                    .attr("y1", i.y - 8)
                    .attr("x2", nodes[nextElement].x - 5)
                    .attr("y2", nodes[nextElement].y - 8)
                    .attr("stroke", (abc.arrReturn[nodes[nextElement].clockActualTxt]["activity"] == "")
                    ? abc.activityColors["default"]
                    : (abc.arrReturn[nodes[nextElement].clockActualTxt]["activity"]
                        .split(",").length > 1)
                        ? abc.arrClockActivity[i.clockActualTxt]
                        : (abc.arrReturn[i.clockActualTxt]["activity"] != "")
                            ? abc.arrClockActivity[nodes[nextElement].clockActualTxt]
                            : abc.activityColors["default"])
                    .attr("stroke-width", 3)
                    .on("click", function (d, i) {
                    console.log("hello", abc);
                    if (abc
                        .arrReturn[nodes[nextElement].clockActualTxt]["activityText"] !=
                        "") {
                        data.title =
                            abc.arrReturn[nodes[nextElement].clockActualTxt]["activity"];
                        data.color =
                            abc
                                .arrReturn[nodes[nextElement].clockActualTxt]["activitycolor"];
                        data.time =
                            abc
                                .arrReturn[nodes[nextElement].clockActualTxt]["activityTextTime"];
                        data.discripation =
                            abc
                                .arrReturn[nodes[nextElement].clockActualTxt]["activityTextDiscripation"];
                        tip.show(data, i);
                    }
                })
                    .on("focusout", function (d, i) {
                    if (abc
                        .arrReturn[nodes[nextElement].clockActualTxt]["activityText"] !=
                        "") {
                        data.title =
                            abc.arrReturn[nodes[nextElement].clockActualTxt]["activity"];
                        data.color =
                            abc
                                .arrReturn[nodes[nextElement].clockActualTxt]["activitycolor"];
                        data.time =
                            abc
                                .arrReturn[nodes[nextElement].clockActualTxt]["activityTextTime"];
                        data.discripation =
                            abc
                                .arrReturn[nodes[nextElement].clockActualTxt]["activityTextDiscripation"];
                        tip.hide(data, i);
                    }
                });
                // console.log('abc.arrReturn[i.clockActualTxt]',abc.arrReturn[i.clockActualTxt]);
                if (abc.arrReturn[i.clockActualTxt]["activity"] != "" &&
                    abc.arrReturn[i.clockActualTxt]["activityText"] != "") {
                    if (abc.arrReturn[i.clockActualTxt]["activityStartPoint"] != "") {
                        var startPointStroke = (abc.arrReturn[i.clockActualTxt]["activityStartPoint"]);
                        element = svg.append("svg:circle")
                            .attr("r", 8)
                            .attr("cx", i.x - 5)
                            .attr("cy", i.y - 8)
                            .attr("fill", abc.arrReturn[i.clockActualTxt]["activityStartPoint"])
                            .attr("stroke", startPointStroke)
                            .attr("stroke-width", 3)
                            .on("click", function (i) {
                            if (abc
                                .arrReturn[nodes[nextElement].clockActualTxt]["activityText"] != "") {
                                data.title =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activity"];
                                data.color =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activitycolor"];
                                data.time =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activityTextTime"];
                                data.discripation =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activityTextDiscripation"];
                                tip.show(data, i);
                            }
                        })
                            .on("focusout", function (d, i) {
                            if (abc
                                .arrReturn[nodes[nextElement].clockActualTxt]["activityText"] != "") {
                                data.title =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activity"];
                                data.color =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activitycolor"];
                                data.time =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activityTextTime"];
                                data.discripation =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activityTextDiscripation"];
                                tip.hide(data, i);
                            }
                        });
                    }
                    if (abc.arrReturn[i.clockActualTxt]["activityEndPoint"] != "") {
                        var endPointStroke = (abc.arrReturn[i.clockActualTxt]["activityEndPoint"]);
                        element = svg.append("svg:circle")
                            .attr("r", 5)
                            .attr("cx", i.x - 5)
                            .attr("cy", i.y - 8)
                            .attr("fill", abc.arrReturn[i.clockActualTxt]["activityEndPoint"])
                            .attr("stroke", endPointStroke)
                            .attr("stroke-width", 2.5)
                            .on("click", function (d, i) {
                            if (abc
                                .arrReturn[nodes[nextElement].clockActualTxt]["activityText"] != "") {
                                data.title =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activity"];
                                data.color =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activitycolor"];
                                data.time =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activityTextTime"];
                                data.discripation =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activityTextDiscripation"];
                                tip.show(data, i);
                            }
                        })
                            .on("focusout", function (d, i) {
                            if (abc
                                .arrReturn[nodes[nextElement].clockActualTxt]["activityText"] != "") {
                                data.title =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activity"];
                                data.color =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activitycolor"];
                                data.time =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activityTextTime"];
                                data.discripation =
                                    abc
                                        .arrReturn[nodes[nextElement].clockActualTxt]["activityTextDiscripation"];
                                tip.hide(data, i);
                            }
                        });
                    }
                }
            });
        };
        createActivitySvg(Activityradius, function (svg) {
            createActivity(svg, Activitynodes, 6, abc.numNodes);
        });
    };
    AppTimelinePage.prototype.drawClock = function (radiusVal) {
        var radius = radiusVal;
        var abc = this;
        var nodes = abc.createNodes(this.numNodes, radius);
        var createSvg = function (radius, callback) {
            var svg = d3.select("#canvas").append("svg:svg")
                .attr("width", (radius * 2) + 50)
                .attr("height", (radius * 2) + 50);
            callback(svg);
        };
        var createElements = function (svg, nodes, elementRadius, numNodes) {
            var element = svg.selectAll("circle").data(nodes).enter().append("svg:circle")
                .attr("r", elementRadius)
                .attr("cx", function (d, i) {
                return d.x;
            })
                .attr("cy", function (d, i) {
                return d.y;
            })
                .attr("class", function (d, i) {
                if ((this.defaultMinutes == 30 && i % 2 == 0) ||
                    (this.defaultMinutes == 15 && i % 4 == 0)) {
                    return "fillnone";
                }
                else {
                    return "";
                }
            });
        };
        var CreateTextElements = function (svg, nodes, elementRadius, numNodes) {
            $.each(nodes, function (d, i) {
                if (i.clockTxt != "") {
                    svg.append("text")
                        .attr("x", i.xposition)
                        .attr("y", i.yposition)
                        .attr("fill", i.clockTxtColor)
                        .attr("font-size", i.textsize)
                        .text(i.clockTxt);
                }
            });
        };
        createSvg(radius, function (svg) {
            createElements(svg, nodes, 3, abc.numNodes);
            CreateTextElements(svg, nodes, 0, abc.numNodes);
        });
    };
    AppTimelinePage.prototype.createNodes = function (numNodes, radius) {
        var nodes = [], width = (radius * 2) + 50, height = (radius * 2) + 50, angle, x, y, i;
        var clockTxt = "";
        var FillColor = "";
        var actualClockTxt = "";
        var clockStartPos = 6;
        var calMinutePos = 0;
        var txtXPosition = 0;
        var txtYPosition = 0;
        var DefaultTextSize = "";
        this.whichClockforTxt = (this.defaultMinutes == 15) ? 0 : 1;
        for (i = 0; i < this.numNodes; i++) {
            angle = (i / (this.numNodes / 2)) * Math.PI; // Calculate the angle at which the element will be placed.
            // For a semicircle, we would use (i / numNodes) * Math.PI.
            x = (radius * Math.cos(angle)) + (width / 2); // Calculate the x position of the element.
            y = (radius * Math.sin(angle)) + (width / 2); // Calculate the y position of the element.
            if ((this.calMinute == 8 && i % 4 == 0) ||
                (this.calMinute == 4 && i % 2 == 0)) {
                calMinutePos = 0;
            }
            clockTxt = (calMinutePos == 0)
                ? (clockStartPos == 0 ? "24/0" : clockStartPos)
                : (clockStartPos - 1 < 0)
                    ? "23." + this.minuteTxt[this.whichClockforTxt][calMinutePos - 1]
                    : (clockStartPos - 1) + "." +
                        this.minuteTxt[this.whichClockforTxt][calMinutePos - 1];
            calMinutePos++;
            FillColor = (clockStartPos % 2 == 0) ? "white" : "#aaa";
            txtXPosition = (clockTxt < 5)
                ? x - 2
                : ((clockTxt == "24/0")
                    ? x - 10
                    : ((clockTxt > 9 && clockTxt <= 23) ? x - 6 : x - 2));
            txtYPosition = (clockTxt < 5)
                ? y + 2
                : ((clockTxt == "24/0")
                    ? y + 2
                    : ((clockTxt > 9 && clockTxt <= 23) ? y + 4 : y + 5));
            DefaultTextSize = (clockTxt == "24/0") ? "8px" : "10px";
            if (this.defaultMinutes == 30) {
                DefaultTextSize = "12px";
                txtXPosition = (clockTxt < 5)
                    ? x - 5
                    : ((clockTxt == "24/0") ? x - 13 : (clockTxt > 9 && clockTxt <= 23)
                        ? x - 8
                        : x - 2);
                txtYPosition = y + 2;
            }
            actualClockTxt = clockTxt;
            if ((this.defaultMinutes == 30 && i % 2 != 0) ||
                (this.defaultMinutes == 15 && i % 4 != 0)) {
                clockTxt = "";
            }
            nodes.push({
                "id": i,
                "x": x,
                "y": y,
                "clockTxt": clockTxt,
                "clockActualTxt": actualClockTxt,
                "clockTxtColor": FillColor,
                "textsize": DefaultTextSize,
                "xposition": txtXPosition,
                "yposition": txtYPosition,
                "defaultCapsuleStroke": "black",
            });
            clockStartPos = (clockStartPos >= 24)
                ? 0
                : ((this.calMinute == 8 && i % 4 == 0) ||
                    (this.calMinute == 4 && i % 2 == 0))
                    ? (clockStartPos + 1)
                    : clockStartPos;
        }
        return nodes;
    };
    AppTimelinePage.prototype.calltoupdateStatus = function (val) {
        var blcheck = true;
        var abc = this;
        if (val == "sleep") {
            if (blcheck) {
                $("#changedata-modal").delay(1000).fadeOut(450);
                setTimeout(function () {
                    d3.select("#sleepcanvas > svg").remove();
                    $("#changedata-modal").modal("hide").hide();
                    abc.updateCapsuleData(val);
                    abc.drawSleepCapsule(190);
                }, 1000);
            }
        }
        else if (val == "medicine") {
            if (blcheck) {
                $("#changedata-modal").delay(1000).fadeOut(450);
                setTimeout(function () {
                    d3.select("#medicinecanvas > svg").remove();
                    $("#changedata-modal").modal("hide").hide();
                    abc.updateCapsuleData("medicine");
                    abc.drawMedicine(220);
                }, 1000);
            }
        }
        else if (val == "activity") {
            if (blcheck) {
                $("#changedata-modal").delay(1000).fadeOut(450);
                setTimeout(function () {
                    d3.select("#activitycanvas1 > svg").remove();
                    $("#changedata-modal").modal("hide").hide();
                    abc.updateCapsuleData("activity");
                    abc.drawActivity(250);
                }, 1000);
            }
        }
        else {
            alert("Something went wrong. Please try again!");
            return false;
        }
    };
    AppTimelinePage.prototype.updateCapsuleData = function (whichCircle) {
        var abc = this;
        if (whichCircle == "medicine") {
            for (var A = 0; A < abc.medicineStart.length; A++) {
                var status = this.medicineStatus[A];
                var updatedColor = this.wheelMedicineColor[A];
                var Start = abc.medicineStart[A].split(":");
                // this.startHours = Start[0];
                // this.startMineuts = Start[1];
                // this.startMineuts = (Math.round(Start[1]/15) * 15) % 60;
                // if(this.startMineuts == 0){
                //   this.startHours = ((((Start[1]/105) | 0) + Start[0]) % 24) + 1;
                //   if(this.startHours == 24){
                //     this.startHours = 0;
                //   }
                // }
                // else{
                //   this.startHours = ((((Start[1]/105) + .5) | 0) + Start[0]) % 24;
                // }
                if (Start[1] >= 53) {
                    this.startMineuts = 45;
                    this.startHours = ((((Start[1] / 105) | 0) + Start[0]) % 24);
                }
                else {
                    this.startMineuts = (Math.round(Start[1] / 15) * 15) % 60;
                    this.startHours = ((((Start[1] / 105) | 0) + Start[0]) % 24);
                    if (this.startHours == 24) {
                        this.startHours = 0;
                    }
                }
                var medicineHour = abc.startHours;
                var medicineMinute = abc.startMineuts;
                var HourKey = (medicineHour == 0 && medicineMinute == 0)
                    ? "24/0"
                    : (medicineHour > 24)
                        ? medicineHour - 24
                        : medicineHour;
                var circleKey = (medicineMinute == 0)
                    ? (HourKey == "24/0") ? HourKey : parseFloat(HourKey)
                    : parseFloat(HourKey) + "." + medicineMinute;
                var arrExistingValues = [];
                this.medicineDes = [];
                this.medicineColor = [];
                if (status != "default") {
                    if (this.arrReturn[circleKey][whichCircle] != "") {
                        var listOfExistingValues = this.arrReturn[circleKey][whichCircle];
                        arrExistingValues = listOfExistingValues.split(",");
                        if (arrExistingValues.indexOf(status) != 0) {
                            arrExistingValues.push(status);
                        }
                        var listOfMidicineDescr = this.arrReturn[circleKey]["medicineTextDiscripation"];
                        this.medicineDes = listOfMidicineDescr.split(",");
                        if (this.medicineDes.indexOf(this.medicineDescription[A]) != 0) {
                            this.medicineDes.push(this.medicineDescription[A]);
                        }
                        var listOfMidicinecolor = this.arrReturn[circleKey]["medicineColor"];
                        this.medicineColor = listOfMidicinecolor.split(",");
                        if (this.medicineColor.indexOf(this.wheelMedicineColor[A]) != 0) {
                            this.medicineColor.push(this.wheelMedicineColor[A]);
                        }
                    }
                    else {
                        arrExistingValues.push(status);
                        this.medicineDes.push(this.medicineDescription[A]);
                        this.medicineColor.push(this.wheelMedicineColor[A]);
                    }
                }
                abc.arrReturn[circleKey]["medicineTextTime"] = "";
                abc.arrReturn[circleKey]["medicineTextDiscripation"] = "";
                abc.arrReturn[circleKey]["medicineColor"] = "";
                if (arrExistingValues.length == 1 || status == "default") {
                    this.arrClockMedicine[circleKey] = updatedColor;
                    abc.arrReturn[circleKey]["medicineTextTime"] = this.medicineStart[A];
                    // abc.arrReturn[circleKey]['medicineTextDiscripation'] = this.medicineDescription[A];
                }
                else {
                    this.arrClockMedicine[circleKey] =
                        this.medicineColors[whichCircle + "multiple"];
                    abc.arrReturn[circleKey]["medicineTextTime"] = this.medicineStart[A];
                }
                listOfExistingValues = arrExistingValues.join(",");
                var MedDes = this.medicineDes.join(",");
                var MedColor = this.medicineColor.join(",");
                this.arrReturn[circleKey][whichCircle] = listOfExistingValues;
                abc.arrReturn[circleKey]["medicineTextDiscripation"] = MedDes;
                abc.arrReturn[circleKey]["medicineColor"] = MedColor;
                var arrExistingLabels = [];
                $.each(arrExistingValues, function (key, item) {
                    var textLabel = status; //(item == "medicinelosec") ? "Losec" : (item == "medicinekepra") ? "Kepra" : (item == "medicinezonegran") ? "Zonegran" : (item == "medicinemovicol") ? "Movicol Junior" : "";
                    if (textLabel != "") {
                        arrExistingLabels.push(textLabel);
                    }
                });
                this.arrReturn[circleKey][whichCircle + "Text"] = arrExistingLabels
                    .join(" & ");
            }
        }
        else if (whichCircle == "sleep") {
            for (var D = 0; D < abc.startTime.length; D++) {
                var status = this.status[D];
                var updatedColor = this.wheelEventColor[D];
                var Start = abc.startTime[D].split(":");
                var End = abc.endTime[D].split(":");
                // this.startHours = Start[0];
                // this.startMineuts = Start[1];
                // this.endHours = End[0];
                // this.endMinites = End[1];
                if (Start[1] >= 53) {
                    this.startMineuts = 45;
                    this.startHours = ((((Start[1] / 105) | 0) + Start[0]) % 24);
                }
                else {
                    this.startMineuts = (Math.round(Start[1] / 15) * 15) % 60;
                    this.startHours = ((((Start[1] / 105) | 0) + Start[0]) % 24);
                    if (this.startHours == 24) {
                        this.startHours = 0;
                    }
                }
                if (End[1] >= 53) {
                    this.endMinites = 45;
                    this.endHours = ((((End[1] / 105) | 0) + End[0]) % 24);
                }
                else {
                    this.endMinites = (Math.round(End[1] / 15) * 15) % 60;
                    this.endHours = ((((End[1] / 105) | 0) + End[0]) % 24);
                    if (this.endHours == 24) {
                        this.endHours = 0;
                    }
                }
                for (var i = this.startHours; i <= this.endHours; i++) {
                    for (var m = 0; m < 60; m += parseFloat(this.defaultMinutes)) {
                        var updateCircleStatus = true;
                        if (i == this.startHours) {
                            updateCircleStatus = false;
                            if (m >= this.startMineuts) {
                                updateCircleStatus = true;
                            }
                        }
                        if (updateCircleStatus) {
                            HourKey = (i == 0 && m == 0) ? "24/0" : (i > 24) ? i - 24 : i;
                            var circleKey = (m == 0)
                                ? (HourKey == "24/0") ? HourKey : parseFloat(HourKey)
                                : parseFloat(HourKey) + "." + m;
                            arrExistingValues = [];
                            this.sleepTime = [];
                            this.sleepDes = [];
                            this.sleepColor = [];
                            if (status != "default") {
                                if (this.arrReturn[circleKey][whichCircle] != "") {
                                    var listOfExistingValues = this.arrReturn[circleKey][whichCircle];
                                    arrExistingValues = listOfExistingValues.split(",");
                                    if (arrExistingValues.indexOf(status) != 0) {
                                        arrExistingValues.push(status);
                                    }
                                    var listOfSleepTime = this.arrReturn[circleKey]["sleepTextTime"];
                                    this.sleepTime = listOfSleepTime.split(",");
                                    if (this.sleepTime.indexOf(this.startTime[D] + "-" + this.endTime[D]) != 0) {
                                        this.sleepTime.push(this.startTime[D] + "-" + this.endTime[D]);
                                    }
                                    var listOfSleepTimeDescr = this.arrReturn[circleKey]["sleepTextDiscripation"];
                                    this.sleepDes = listOfSleepTimeDescr.split(",");
                                    if (this.sleepDes.indexOf(this.sleepDescription[D]) != 0) {
                                        this.sleepDes.push(this.sleepDescription[D]);
                                    }
                                    var listOfSleepTimecolor = this.arrReturn[circleKey]["sleepColor"];
                                    this.sleepColor = listOfSleepTimecolor.split(",");
                                    if (this.sleepColor.indexOf(this.wheelEventColor[D]) != 0) {
                                        this.sleepColor.push(this.wheelEventColor[D]);
                                    }
                                }
                                else {
                                    arrExistingValues.push(status);
                                    this.sleepDes.push(this.sleepDescription[D]);
                                    this.sleepTime.push(this.startTime[D] + "-" + this.endTime[D]);
                                    this.sleepColor.push(this.wheelEventColor[D]);
                                }
                            }
                            abc.arrReturn[circleKey]["sleepTextTime"] = "";
                            abc.arrReturn[circleKey]["sleepTextDiscripation"] = "";
                            abc.arrReturn[circleKey]["sleepColor"] = "";
                            if (arrExistingValues.length == 1 || status == "default") {
                                this.arrClockSleep[circleKey] = updatedColor;
                                // abc.arrReturn[circleKey]['sleepTextTime'] = this.startTime[D]+'-'+this.endTime[D];
                                // abc.arrReturn[circleKey]['sleepTextDiscripation'] = this.sleepDescription[D];
                            }
                            else {
                                this.arrClockSleep[circleKey] = updatedColor; //(whichCircle == "sleep") ? this.sleepColors[whichCircle+"multiple"] : "#FFF";
                            }
                            listOfExistingValues = arrExistingValues.join(",");
                            var Sleepstartendtime = this.sleepTime.join(",");
                            var sleepDes = this.sleepDes.join(",");
                            var sleepColor = this.sleepColor.join(",");
                            this.arrReturn[circleKey][whichCircle] = listOfExistingValues;
                            abc.arrReturn[circleKey]["sleepTextTime"] = Sleepstartendtime;
                            abc.arrReturn[circleKey]["sleepTextDiscripation"] = sleepDes;
                            abc.arrReturn[circleKey]["sleepColor"] = sleepColor;
                            if (whichCircle == "sleep") {
                                var arrExistingLabels = [];
                                $.each(arrExistingValues, function (key, item) {
                                    var textLabel = status; //(item == "sleepdistress") ? "Distress" : (item == "sleepunawake") ? "Uncomfortable" : (item == "sleep") ? "A Sleep" : (item == "sleepawake") ? "Awake" : "";
                                    if (textLabel != "") {
                                        arrExistingLabels.push(textLabel);
                                    }
                                });
                                this.arrReturn[circleKey][whichCircle + "Text"] =
                                    arrExistingLabels.join(" & ");
                            }
                        }
                        if (i == abc.endHours && m == abc.endMinites) {
                            break;
                        }
                    }
                }
            }
        }
        else {
            this.circle = [];
            this.startPoint = [];
            for (var C = 0; C < abc.activityStartTime.length; C++) {
                var status = this.activityStatus[C];
                var updatedColor = this.wheelActivityColor[C];
                var Start = abc.activityStartTime[C].split(":");
                var End = abc.activityEndTime[C].split(":");
                // minuteValues="0,15,30,45"
                // this.activityStartHours = Start[0];
                // this.activityStartMineuts = Start[1];
                // this.activityEndHours = End[0];
                // this.activityEndMinites = End[1];
                if (Start[1] >= 53) {
                    this.activityStartMineuts = 45;
                    this.activityStartHours = ((((Start[1] / 105) | 0) + Start[0]) % 24);
                }
                else {
                    this.activityStartMineuts = (Math.round(Start[1] / 15) * 15) % 60;
                    this.activityStartHours = ((((Start[1] / 105) | 0) + Start[0]) % 24);
                    if (this.activityStartHours == 24) {
                        this.activityStartHours = 0;
                    }
                }
                if (End[1] >= 53) {
                    this.activityEndMinites = 45;
                    this.activityEndHours = ((((End[1] / 105) | 0) + End[0]) % 24);
                }
                else {
                    this.activityEndMinites = (Math.round(End[1] / 15) * 15) % 60;
                    this.activityEndHours = ((((End[1] / 105) | 0) + End[0]) % 24);
                    if (this.activityEndHours == 24) {
                        this.activityEndHours = 0;
                    }
                }
                // this.activityEndMinites = (Math.round(End[1]/15) * 15) % 60;
                //   if(this.activityEndMinites == 0){
                //     this.activityEndHours = ((((End[1]/105) | 0) + End[0]) % 24) + 1;
                //     if(this.activityEndHours == 24){
                //       this.activityEndHours = 0;
                //     }
                //   }
                //   else{
                //     this.activityEndHours = ((((End[1]/105) + .5) | 0) + End[0]) % 24;
                //   }
                var counter = 0;
                for (var i = this.activityStartHours; i <= this.activityEndHours; i++) {
                    for (var m = 0; m < 60; m += parseFloat(this.defaultMinutes)) {
                        var updateCircleStatus = true;
                        if (i == this.activityStartHours) {
                            updateCircleStatus = false;
                            if (m >= this.activityStartMineuts) {
                                updateCircleStatus = true;
                            }
                        }
                        if (updateCircleStatus) {
                            HourKey = (i == 0 && m == 0) ? "24/0" : (i > 24) ? i - 24 : i;
                            var circleKey = (m == 0)
                                ? (HourKey == "24/0") ? HourKey : parseFloat(HourKey)
                                : parseFloat(HourKey) + "." + m;
                            arrExistingValues = [];
                            this.arrDes = [];
                            this.arrstartendtime = [];
                            this.arrcolor = [];
                            if (status != "default") {
                                if (this.arrReturn[circleKey][whichCircle] != "") {
                                    var listOfExistingValues = this.arrReturn[circleKey][whichCircle];
                                    arrExistingValues = listOfExistingValues.split(",");
                                    if (arrExistingValues.indexOf(status) != 0) {
                                        arrExistingValues.push(status);
                                    }
                                    var listOfExistingValuesact = this.arrReturn[circleKey]["activityTextTime"];
                                    this.arrstartendtime = listOfExistingValuesact.split(",");
                                    if (this.arrstartendtime.indexOf(this.activityStartTime[C] + "-" + this.activityEndTime[C]) != 0) {
                                        this.arrstartendtime.push(this.activityStartTime[C] + "-" + this.activityEndTime[C]);
                                    }
                                    var listOfExistingDescr = this.arrReturn[circleKey]["activityTextDiscripation"];
                                    this.arrDes = listOfExistingDescr.split(",");
                                    if (this.arrDes.indexOf(this.activityDescription[C]) != 0) {
                                        this.arrDes.push(this.activityDescription[C]);
                                    }
                                    var listOfExistingColor = this.arrReturn[circleKey]["activitycolor"];
                                    this.arrcolor = listOfExistingColor.split(",");
                                    if (this.arrcolor.indexOf(this.activityStatus[C]) != 0) {
                                        this.arrcolor.push(this.wheelActivityColor[C]);
                                    }
                                }
                                else {
                                    arrExistingValues.push(status);
                                    this.arrstartendtime.push(this.activityStartTime[C] + "-" + this.activityEndTime[C]);
                                    this.arrDes.push(this.activityDescription[C]);
                                    this.arrcolor.push(this.wheelActivityColor[C]);
                                }
                            }
                            this.arrReturn[circleKey]["activityStartPoint"] = "";
                            this.arrReturn[circleKey]["activityEndPoint"] = "";
                            this.arrReturn[circleKey]["activityColor"] = "";
                            if (arrExistingValues.length == 1 || status == "default") {
                                this.arrClockActivity[circleKey] = updatedColor;
                            }
                            else {
                                this.arrClockActivity[circleKey] = updatedColor; //(whichCircle == "activity") ? this.activityColors[whichCircle+"multiple"] : "#FFF";
                            }
                            listOfExistingValues = arrExistingValues.join(",");
                            var startendtime = this.arrstartendtime.join(",");
                            var Des = this.arrDes.join(",");
                            var Activtitycolor = this.arrcolor.join(",");
                            this.arrReturn[circleKey][whichCircle] = listOfExistingValues;
                            this.arrReturn[circleKey]["activityTextTime"] = startendtime;
                            abc.arrReturn[circleKey]["activityTextDiscripation"] = Des;
                            abc.arrReturn[circleKey]["activitycolor"] = Activtitycolor;
                            if (whichCircle == "activity") {
                                if (status != "default" && counter == 0) {
                                    var start = this.arrReturn[circleKey]["activity"].split(",");
                                    if (start.length !== 1) {
                                        this.arrReturn[circleKey]["activityStartPoint"] = "#FF69B4";
                                    }
                                    else {
                                        this.arrReturn[circleKey]["activityStartPoint"] =
                                            updatedColor;
                                    }
                                    var color = {
                                        "color": this.arrReturn[circleKey]["activityStartPoint"],
                                        "time": circleKey,
                                        "type": "start",
                                    };
                                    this.circle.push(color);
                                    for (var d = 0; d < this.end.length; d++) {
                                        if (this.activityStartHours == this.end[d] &&
                                            this.activityStartMineuts == this.endmin[d]) {
                                            this.arrReturn[circleKey]["activityStartPoint"] =
                                                "#FF69B4";
                                            this.startPoint.push(circleKey);
                                        }
                                    }
                                }
                            }
                            var arrExistingLabels = [];
                            $.each(arrExistingValues, function (key, item) {
                                var textLabel = status;
                                if (textLabel != "") {
                                    arrExistingLabels.push(textLabel);
                                }
                            });
                            this.arrReturn[circleKey][whichCircle + "Text"] =
                                arrExistingLabels.join(" & ");
                            counter++;
                        }
                        if (i == abc.activityEndHours && m == abc.activityEndMinites) {
                            var end = this.arrReturn[circleKey]["activity"].split(",");
                            if (end.length !== 1) {
                                this.arrReturn[circleKey]["activityEndPoint"] = "#FF69B4";
                            }
                            else {
                                this.arrReturn[circleKey]["activityEndPoint"] = updatedColor;
                            }
                            var color = {
                                "color": this.arrReturn[circleKey]["activityEndPoint"],
                                "time": circleKey,
                                "type": "end",
                            };
                            this.circle.push(color);
                            break;
                        }
                    }
                }
            }
            for (var p = 0; p < this.circle.length; p++) {
                if (this.circle[p].type == "start") {
                    if ($.inArray(this.circle[p].time, this.startPoint) !== -1) {
                        this.arrReturn[this.circle[p].time]["activityStartPoint"] =
                            "#FF69B4";
                    }
                    else {
                        this.arrReturn[this.circle[p].time]["activityStartPoint"] =
                            this.circle[p].color;
                    }
                }
                else {
                    if ($.inArray(this.circle[p].time, this.startPoint) !== -1) {
                        this.arrReturn[this.circle[p].time]["activityEndPoint"] = "#FF69B4";
                    }
                    else {
                        this.arrReturn[this.circle[p].time]["activityEndPoint"] =
                            this.circle[p].color;
                    }
                }
            }
        }
        this.resetData();
    };
    ////////// HCP DropDown Code Starts /////////////
    AppTimelinePage.prototype.getHCPData = function (userid, usertype) {
        var keys = [];
        var hcp1 = [];
        var self = this;
        var webServiceData = {};
        webServiceData.action = "gethcpdata";
        webServiceData.id = userid;
        webServiceData.usertype = usertype;
        webServiceData.appsecret = this.appsecret;
        self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            if (result.status == "success") {
                // result.data;
                hcp1 = hcp1.push(result.data);
                // hcp1 = result.data;
                // return result1;
            }
            else {
            }
        }, function (err) {
            console.log(err);
        });
        return hcp1;
    };
    AppTimelinePage.prototype.getHCPid = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("currentHCP").then(function (result) {
                            if (result != null) {
                                _this.hcpid = result;
                                _this.getuserLoginData();
                            }
                            else {
                                _this.getuserLoginData();
                                _this.hcpid = 0;
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppTimelinePage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("userLogin").then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                if (_this.userLoginData.usertype == "healthcareprovider") {
                                    _this.hcpid = _this.userLoginData.id;
                                }
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppTimelinePage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("patientType").then(function (result) {
                            if ((!result || result == null || result == undefined) &&
                                _this.userLoginData == "patient") {
                                _this.doRefresh();
                            }
                            else {
                                _this.patientType = result;
                                _this.appIntialize1();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppTimelinePage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // to get a key/value pair
    AppTimelinePage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_2 = _a.sent();
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // remove a single key value:
    AppTimelinePage.prototype.remove = function (key) {
        this.storage.remove(key);
    };
    AppTimelinePage.prototype.doRefresh = function (event) {
        var _this = this;
        if (event === void 0) { event = ""; }
        this.loadingService.show();
        setTimeout(function () {
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            _this.WheelData = [];
            _this.loadingService.hide();
        }, 2000);
    };
    AppTimelinePage.prototype.selectEmployee = function ($event) {
        this.set("currentHCP", $event);
        this.doRefresh();
    };
    AppTimelinePage.prototype.shareWhatsapp = function () {
        var message = "Daily Timeline Details";
        this.socialShareCtrl.shareViaWhatsApp(message, this.shareImageNi, this.siteName).then(function (res) {
        }).catch(function (error) {
        });
    };
    AppTimelinePage.prototype.shareTwitter = function (img) {
        var message = "Daily Timeline Details";
        this.socialShareCtrl.shareViaTwitter(message, img, this.siteName).then(function (res) {
        }).catch(function (error) {
            console.log("failed : " + error);
        });
    };
    // shareFacebookHint() {
    //   let pasteData = "Testing";
    //   let message = "Daily Timeline Details";
    //   this.socialShareCtrl.shareViaFacebookWithPasteMessageHint(message, "", this.siteName, pasteData).then(res => {
    //     console.log("success facebook hint: " + res);
    //   }).catch(error => {
    //     console.log("failed facebook hint: " + error);
    //   })
    // }
    AppTimelinePage.prototype.shareInsta = function () {
        var nameAndSite = "Victory Care" + this.siteName;
        this.socialShareCtrl.shareViaInstagram(nameAndSite, this.shareImageNi).then(function (res) {
        }).catch(function (error) {
        });
    };
    AppTimelinePage.prototype.shareSms = function () {
        var nameAndSite = "Victory Care" + this.siteName;
        this.socialShareCtrl.shareViaSMS(nameAndSite, "").then(function (res) {
        }).catch(function (error) {
            console.log("failed SMS: " + error);
        });
    };
    AppTimelinePage.prototype.shareEmail = function (img) {
        var _this = this;
        var nameAndSite = this.siteName;
        var subject = "Daily Timeline Details";
        this.socialShareCtrl.canShareViaEmail().then(function (res) {
            console.log("success can email  : " + res);
            _this.socialShareCtrl.shareViaEmail(nameAndSite, subject, [], [], [], img).then(function (res) {
                console.log("success email : " + res);
            }).catch(function (error) {
                console.log("failed email: " + error);
            });
        }).catch(function (error) {
            console.log("failed can email: " + error);
        });
    };
    AppTimelinePage.prototype.shareFacebook = function () {
        var img = "https://lh3.googleusercontent.com/8c4SpecjfcnCdfq2PXTwS7vvjPmrOa-lgbcczv6RBRF-6fgYv7-qCrNEW5owqQRPx15FesYOt3oqJrQJTmk0XZoR4WGEc3UGGbZgt7GvPKOd_pW4_Cbd9yqiHY9mxoHAkr5m-coUo47lsSy484J4kINWBrR3YaXClG_KRLvYDMH5vjipCJsTlh8-1Eg5z-IMON0yW9dwL0C42FPDTJ5pQIE2ilZ7J4CYGSo14XDw87zdZ7GrkrLtuESlupo2rzAX80zaZ4ER-S-o8fhHwvLYNrrYrfrJMCBjduqziJxvuBnB5fMSkn8BUI9JuIcqq9Hp5J2aEwIrb1xQUpW8bbC0o2Y8hVUv2yECJ8F4jieEg5EQaz--sPxjogAEgybsaGS7QJK6yphIm59ulIQRUmcwOEf5oqMHwXUrYfaW4v-hia_9h4vuNWoUOksNstaJa22PVRbZGIZLdFdfPYE9ekVyFvUxqsZ9Ft6pFAXdkD3UX7GaKujky6QFNJDqkCef8Tu0vXQFPycxmIdm4spMgKy6NIc92_p6AQo_7AYrv7pI_vtUL4-Sh6HtQ09o6g0GbprKCnforqQyHWsAOdVQ5M6zjOrh6dNAuxFTl4MHIV8=w353-h588-no";
        var pasteData = "Pasting About Nithyam Tutorials";
        var message = "Daily Timeline Details";
        this.socialShareCtrl.shareViaFacebook(message, img, this.siteName).then(function (res) {
            console.log("success facebook hint: " + res);
        }).catch(function (error) {
            console.log("failed facebook hint: " + error);
        });
    };
    AppTimelinePage.prototype.shareFacebookHint = function (img) {
        var pasteData = "Pasting About Nithyam Tutorials";
        var message = "Nithyam Tutorials";
        this.socialShareCtrl.shareViaFacebookWithPasteMessageHint(message, "", this.siteName, pasteData).then(function (res) {
            console.log("success facebook hint: " + res);
        }).catch(function (error) {
            console.log("failed facebook hint: " + error);
        });
    };
    AppTimelinePage.prototype.takeScreenShot = function (socailmediaType) {
        var _this = this;
        console.log("inside take screenshot");
        var message = "Daily Timeline Settings";
        var subject = "Victory Care Data";
        // var img = "https://lh3.googleusercontent.com/8c4SpecjfcnCdfq2PXTwS7vvjPmrOa-lgbcczv6RBRF-6fgYv7-qCrNEW5owqQRPx15FesYOt3oqJrQJTmk0XZoR4WGEc3UGGbZgt7GvPKOd_pW4_Cbd9yqiHY9mxoHAkr5m-coUo47lsSy484J4kINWBrR3YaXClG_KRLvYDMH5vjipCJsTlh8-1Eg5z-IMON0yW9dwL0C42FPDTJ5pQIE2ilZ7J4CYGSo14XDw87zdZ7GrkrLtuESlupo2rzAX80zaZ4ER-S-o8fhHwvLYNrrYrfrJMCBjduqziJxvuBnB5fMSkn8BUI9JuIcqq9Hp5J2aEwIrb1xQUpW8bbC0o2Y8hVUv2yECJ8F4jieEg5EQaz--sPxjogAEgybsaGS7QJK6yphIm59ulIQRUmcwOEf5oqMHwXUrYfaW4v-hia_9h4vuNWoUOksNstaJa22PVRbZGIZLdFdfPYE9ekVyFvUxqsZ9Ft6pFAXdkD3UX7GaKujky6QFNJDqkCef8Tu0vXQFPycxmIdm4spMgKy6NIc92_p6AQo_7AYrv7pI_vtUL4-Sh6HtQ09o6g0GbprKCnforqQyHWsAOdVQ5M6zjOrh6dNAuxFTl4MHIV8=w353-h588-no"
        // this.socialShareCtrl.share(message, subject, img, this.siteName);
        this.screenshot.URI(80).then(function (response) {
            _this.socialShareCtrl.share(message, subject, response.URI, _this.siteName);
            // console.log("response from take: " + response);
            // if (socailmediaType == "facebook") {
            //   this.shareFacebookHint(response.URI)
            // }
            // else if (socailmediaType == "twitter") {
            //   this.shareTwitter(response.URI);
            // }
            // else if (socailmediaType == "facebookhint") {
            //   this.shareFacebook();
            // }
            // else if (socailmediaType == "email") {
            //   this.shareEmail(response.URI);
            // }
        }, function (error) {
            console.log("response from take error : " + error);
        });
    };
    AppTimelinePage.prototype.settings = function () {
        this.navCtrl.push("AppSettingsDailyWheelSettingsPage");
    };
    AppTimelinePage.prototype.closeSelect = function (e) {
        this.takeScreenShot(e);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Navbar */])
    ], AppTimelinePage.prototype, "navBar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('mySelect'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["B" /* Select */])
    ], AppTimelinePage.prototype, "selectRef", void 0);
    AppTimelinePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-app-timeline",template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-timeline/app-timeline.html"*/'<ion-header>\n  <ion-navbar>\n    <h1 style="float: left;color: white;">24-Hour Clock</h1>\n    <div class="datetime-text"\n      style="border: 1px solid #464e6a;border-radius: 10px;background-color:#464e6a;width: 250px;margin-left: 350px;margin-top: 20px;">\n      <div style="width: 42px;float: left;padding: 5px 2px 7px 0px;background-color: #74c2ce;"><img style="margin-top: 0px;\n        width: 25px;\n        height: 25px;" src="assets/images/menu-icons/calendar-icon.png">\n      </div>\n      <ion-datetime placeHolder="Select Date" [(ngModel)]="selectedDate" displayFormat="DD / MM / YYYY"\n        (ionChange)="notify($event)" style="margin-top: 5px;float: left;padding: 0 10px;font-size: 22px;\n        font-weight: 800; color: white;">\n      </ion-datetime>\n      <img style="height: 36px;width: 40px;margin-left: -11px;" src="assets/images/menu-icons/down-arrow.png">\n    </div>\n    <form [formGroup]="SelectList">\n      <ion-item no-lines *ngIf="ut && notselected && dependent" class="item-width" style="background:none;">\n        <ion-select (ionChange)="selectEmployee($event)" style="color: white;" right>\n          <ion-label>Select Healthcare Provider</ion-label>\n          <ion-option *ngFor="let key of dropdown[0]; let idx = index;" [selected]="key.id==selected1"\n            value="{{key.id}}">{{key.firstname}}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item no-lines class="item-width" style="background:none;" *ngIf="ut && !notselected && dependent">\n        <ion-select (ionChange)="selectEmployee($event)" style="color: white;" right>\n          <ion-label>Select Healthcare Provider</ion-label>\n          <ion-option *ngFor="let key of dropdown[0]; let idx = index;" [selected]="key.id==selected1"\n            value="{{key.id}}">{{key.firstname}}</ion-option>\n        </ion-select>\n      </ion-item>\n    </form>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <!-- <ion-row>\n    <ion-col col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n    </ion-col>\n  </ion-row> -->\n  <ion-row (click)="fab.close()" wrap padding>\n    <ion-col class="left-timeline" col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 padding>\n      <div class="container" style="padding-left: 16px;">\n        <ul>\n          <li *ngFor="let item of WheelData | sort: {property: column, order: order};">\n            <div *ngIf="item.source==\'Activity\'">\n              <div><span></span>\n                <ion-row>\n                  <ion-col style="text-align: start;" col-6>\n                    <div class="title">{{item.name}}</div>\n                  </ion-col>\n                  <ion-col col-6>\n                    <img (click)="editWheelActivity(item)" class="card-top-icon" style="margin-left:5px;"\n                      src="assets/images/others/edit-icon.png" />\n                    <img (click)="deleteWheelActivityData(item)" class="card-top-icon"\n                      src="assets/images/others/delete-icon.png" />\n                  </ion-col>\n                  <ion-col style="text-align: start; padding-top: 0px;" col-12>\n                    <div class="info">{{item.description}}</div>\n                  </ion-col>\n                </ion-row>\n              </div> <span [style.background]=item.color class="number"><span style="">\n                  <div class="innerpoint-timeline" [style.backgroundColor]=item.color\n                    [style.border-color]=item.bordercolor></div>{{item.start}}\n                </span> <span>\n                  <div class="innerpoint-timeline" [style.backgroundColor]=item.color\n                    [style.border-color]=item.bordercolor></div>{{item.end}}\n                </span></span>\n            </div>\n\n            <div *ngIf="item.source==\'Medicine\'">\n              <div><span></span>\n                <ion-row>\n                  <ion-col style="text-align: start;" col-6>\n                    <div class="title">{{item.medicineName}}</div>\n                  </ion-col>\n                  <ion-col col-6>\n                    <img (click)="EditMedicine(item)" class="card-top-icon" style="margin-left:5px;"\n                      src="assets/images/others/edit-icon.png" />\n                    <img (click)="deleteWheelMedicineData(item)" class="card-top-icon"\n                      src="assets/images/others/delete-icon.png" />\n                  </ion-col>\n                  <ion-col style="text-align: start; padding-top: 0px;" col-12>\n                    <div class="info">{{item.description}}</div>\n                  </ion-col>\n                </ion-row>\n              </div> <span [style.background]=item.color class="number"><span>\n                  <div class="ovel-icon" [style.backgroundColor]=item.color><img class="tabelt" style="float: left;"\n                      src="assets/imgs/icn-pill.png" /></div>{{item.start}}\n                </span> <span>\n                  <div class="ovel-icon" [style.backgroundColor]=item.color><img class="tabelt" style="float: left;"\n                      src="assets/imgs/icn-pill.png" /></div>{{item.end}}\n                </span></span>\n            </div>\n\n            <div *ngIf="item.source==\'Event\'">\n              <div><span></span>\n                <ion-row>\n                  <ion-col style="text-align: start;" col-6>\n                    <div class="title">{{item.Eventname}}</div>\n                  </ion-col>\n                  <ion-col col-6>\n                    <img (click)="EditEvent(item)" class="card-top-icon" style="margin-left:5px;"\n                      src="assets/images/others/edit-icon.png" />\n                    <img (click)="deleteWheelEventData(item)" class="card-top-icon"\n                      src="assets/images/others/delete-icon.png" />\n                  </ion-col>\n                  <ion-col style="text-align: start; padding-top: 0px;" col-12>\n                    <div class="info">{{item.description}}</div>\n                  </ion-col>\n                </ion-row>\n              </div> <span [style.background]=item.color class="number"><span>\n                  <div class="medicine" [style.backgroundColor]=item.color></div>{{item.start}}\n                </span> <span>\n                  <div class="medicine" [style.backgroundColor]=item.color></div>{{item.end}}\n                </span></span>\n            </div>\n          </li>\n        </ul>\n      </div>\n    </ion-col>\n    <ion-col col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8>\n    <div class="setting">\n      <img  (click)="settings()" src="assets/images/others/timelinesetting.png">\n    </div>\n      <!-- <img class="share" (click)="takeScreenShot()" src="assets/images/others/share.png">\n      <ion-item style="margin-bottom: -31px;opacity: 0;width: 0px;margin-top: -13px;height: 0px;" no-lines>\n        <ion-select [(ngModel)]="choices" (ionChange)="closeSelect($event)" #mySelect>\n          <ion-label>Share on Social Media</ion-label>\n          <ion-option value="facebook">Share on Facebook</ion-option>\n          <ion-option value="facebookhint">Share on Facebook12</ion-option>\n          <ion-option value="twitter">Share on Twitter</ion-option>\n          <ion-option value="linked">Share on Linked In</ion-option>\n          <ion-option value="email">Share via Email</ion-option>\n        </ion-select>\n      </ion-item> -->   \n      <div class="outside">\n        <div class="fade-in" id="activitycanvas">\n          <div class="fade-in" id="medicinecanvas">\n            <div class="fade-in" id="sleepcanvas">\n              <div class="fade-in" id="canvas">\n                <div class="fade-in" id="icons">\n                  <div class="Oval-9-Copy-8">\n                    <img style="padding-top: 10px;" src="assets/images/menu-icons/video-camera.png" />\n                  </div>\n                  <div class="Oval-9-Copy-7">\n                    <img style="padding-top: 10px;" src="assets/images/menu-icons/photo-camera.png" />\n                  </div>\n                  <div class="Oval-9-Copy-6">\n                    <img style="padding-top: 10px;" src="assets/images/menu-icons/stopwatch.png" />\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div id="activitycanvas1" class="fade-in"></div>\n        </div>\n      </div>\n\n      <div style="display: block;padding-left: 50px;" class="col-xs-8 col-sm-8 pre-definedlist">\n        <div class="row">\n          <ion-scroll direction="x-y" class="theroot">\n            <div class="col-xs-2 col-sm-2 text-left" style="text-align: justify;">\n              <span>Wellbeing status</span>\n\n              <div *ngFor="let item of wellbeaningList" class="sublist">\n                <div [style.background]=item.color class="circularclass sleep"></div>\n                <div style="width: 110px;">{{item.name}}</div>\n              </div>\n            </div>\n          </ion-scroll>\n          <ion-scroll direction="x-y" class="theroot">\n            <div style="padding-left: 15px;text-align: justify;" class="col-xs-3 col-sm-3 text-left">\n              <span (click)="takeScreenShot()">Medicines</span>\n              <div *ngFor="let item of medicineList" class="sublist">\n                <div [style.background]=item.color class="circularclass medicinelosec"></div>\n                <div style="width: 100px;">{{item.name}}</div>\n              </div>\n\n            </div>\n          </ion-scroll>\n          <ion-scroll direction="x-y" class="theroot">\n            <div style="padding-left: 15px;text-align: justify;" class="col-xs-4 col-sm-4 text-left">\n              <span >Activities</span>\n              <div *ngFor="let item of activityList" class="sublist">\n                <div [style.background]=item.color class="circularclass activitybpap"></div>\n                <div style="width: 66px;margin-top:-4px;">{{item.name}}</div>\n              </div>\n            </div>\n          </ion-scroll>\n\n        </div>\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n  <ion-fab right middle #fab>\n    <button class="boxshadow" style="background-color: #fb7e7e;" ion-fab>\n      <ion-icon class="icAdd" name="add"></ion-icon>\n    </button>\n    <ion-fab-list (click)="addActivity()" side="top">\n      <img (click)="fab.close()" class="addImg" src="assets/images/others/btn-add-activity.png">\n    </ion-fab-list>\n    <ion-fab-list (click)="addStatus()" class="btnBottom" side="button">\n      <img (click)="fab.close()" class="addImg" src="assets/images/others/add-wellbeing-status-.png">\n    </ion-fab-list>\n    <ion-fab-list (click)="addMedicien()" side="left">\n      <img (click)="fab.close()" class="addImg" src="assets/images/others/btn-add-medicine.png">\n    </ion-fab-list>\n\n  </ion-fab>\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-timeline/app-timeline.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_screenshot_ngx__["a" /* Screenshot */]],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4__services_database_service__["a" /* DatabaseService */],
            __WEBPACK_IMPORTED_MODULE_5__app_app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_9__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */],
            __WEBPACK_IMPORTED_MODULE_10__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_11__services_toast_service__["a" /* ToastService */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_screenshot_ngx__["a" /* Screenshot */]])
    ], AppTimelinePage);
    return AppTimelinePage;
}());

//# sourceMappingURL=app-timeline.js.map

/***/ }),

/***/ 993:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppLoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_app_shared_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_password_recovery_modal_app_password_recovery_modal__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_plus__ = __webpack_require__(576);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__environment_environment__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_fcm__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_timeline_app_timeline__ = __webpack_require__(976);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__ = __webpack_require__(181);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};













var AppLoginPage = (function () {
    function AppLoginPage(navCtrl, navParams, googlePlus, menu, platform, alertCtrl, databaseService, loadingService, splashScreen, appSharedService, modalCtrl, storage, http, fcm) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.googlePlus = googlePlus;
        this.menu = menu;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.splashScreen = splashScreen;
        this.appSharedService = appSharedService;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.http = http;
        this.fcm = fcm;
        this.password = "";
        this.email = "";
        this.rememberme = false;
        this.params = {};
        this.baseurl = __WEBPACK_IMPORTED_MODULE_9__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_9__environment_environment__["a" /* environment */].appsecret;
        this.data = {};
        this.data1 = {};
        this.Registerdata = {};
        this.errorMessage = "";
        this.userInfo = {};
        this.passwordType = "password";
        this.passwordIcon = "eye-off";
        if (window.indexedDB) {
            console.log("I'm in WKWebView!");
        }
        else {
            console.log("I'm in UIWebView11213");
        }
        this.data.logo = "assets/images/logo/victory.png";
        this.storage.get("remember").then(function (val) {
            if (val != undefined) {
                if (val.rememberme == true) {
                    _this.email = val.email;
                    _this.password = val.password;
                    _this.rememberme = val.rememberme;
                }
                else {
                    _this.storage.ready().then(function () {
                        // alert("session still set");
                        _this.storage.remove("userLogin");
                    });
                    _this.storage.clear();
                }
            }
            else {
                _this.storage.ready().then(function () {
                    // alert("session still set");
                    _this.storage.remove("userLogin");
                });
                _this.storage.clear();
            }
        });
        // if (_cookieService.get('rememberme')) {
        //     this.email = this._cookieService.get('username');
        //     this.password = this._cookieService.get('password');
        //     this.rememberme = this._cookieService.get('rememberme');
        //     // this.usertype = this._cookieService.get('usertype');
        //     // this.usertype = this._cookieService.get('usertype') ? this._cookieService.get('usertype') : '';
        // }
        this.initPushNotification();
    }
    AppLoginPage.prototype.hideShowPassword = function () {
        this.passwordType = this.passwordType === "text" ? "password" : "text";
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    };
    AppLoginPage.prototype.initPushNotification = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.splashScreen.hide();
            _this.fcm.subscribeToTopic('marketing');
            _this.fcm.getToken().then(function (token) {
                console.log("new token", token);
                console.log(token);
                _this.data.fcmtoken = token;
            });
            _this.fcm.onTokenRefresh().subscribe(function (token) {
                //register token
                _this.data.fcmtoken = token;
                console.log("tokenrefresh", token);
            });
            _this.storage.get("userLogin").then(function (val) {
                if (val != null || val != undefined) {
                    _this.fcm.onNotification().subscribe(function (data) {
                        if (data.wasTapped) {
                            console.log("get notification foreground", data);
                            var id = JSON.parse(data['gcm.notification.data']);
                            console.log("helllllo", id);
                            if (data.aps.alert.title == "Add event" || data.aps.alert.title == "Add activity" || data.aps.alert.title == "Add medicine") {
                                if (id.eventData) {
                                    _this.updatenotification(id);
                                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__app_timeline_app_timeline__["a" /* AppTimelinePage */], { 'eventnot': id.eventData, type: 'notification' });
                                }
                                else if (id.activityData) {
                                    _this.updatenotification(id);
                                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__app_timeline_app_timeline__["a" /* AppTimelinePage */], { 'activitynot': id.activityData, type: 'notification' });
                                }
                                else if (id.medicineData) {
                                    _this.updatenotification(id);
                                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__app_timeline_app_timeline__["a" /* AppTimelinePage */], { 'medicinenot': id.medicineData, type: 'notification' });
                                }
                            }
                        }
                        else {
                            _this.presentAlert(data);
                            console.log("get notification", data);
                        }
                        ;
                    });
                    _this.fcm.unsubscribeFromTopic('marketing');
                }
            });
        });
    };
    AppLoginPage.prototype.updatenotification = function (id) {
        var edata = {};
        if (id.eventData) {
            edata.eventDataid = id.eventData.eventDataid;
            edata.action = "eventnotify";
        }
        else if (id.activityData) {
            edata.activityDataid = id.activityData.activityDataid;
            edata.action = "activitynotify";
        }
        else if (id.medicineData) {
            edata.medicineDataid = id.medicineData.medicineDataid;
            edata.action = "medicinenotify";
        }
        edata.appsecret = this.appsecret;
        console.log("data", edata);
        this.http.post(this.baseurl + "updatedata.php", edata).subscribe(function (edata1) {
            var result = JSON.parse(edata1["_body"]);
            console.log("status ::::>", result.status);
        });
    };
    AppLoginPage.prototype.aa = function () {
        var edata = {};
        edata.eventDataid = 21;
        edata.action = "eventnotify";
        edata.appsecret = this.appsecret;
        this.http.post(this.baseurl + "updatedata.php", edata).subscribe(function (edata1) {
            var result = JSON.parse(edata1["_body"]);
            console.log("status ::::>", result.status);
        });
    };
    AppLoginPage.prototype.onLogin = function () {
        var _this = this;
        this.aa();
        var self = this;
        // console.log('this.rememberme',this.rememberme);
        this.loadingService.show();
        console.log(this.email);
        this.data.email = this.email;
        this.data.password = this.password;
        this.data.appsecret = this.appsecret;
        this.data.action = "getdetails";
        console.log("datalogin :::>>>", this.data);
        console.log(this.baseurl);
        this.http.post(this.baseurl + "login.php", this.data).subscribe(function (snapshot) {
            console.log("dataaaaa", snapshot);
            var result = JSON.parse(snapshot["_body"]);
            if (result.status == "success") {
                self.storage.set("userLogin", result.data);
                var userid = result.data.id;
                if (result.data.usertype == "patient") {
                    var psession = true;
                    self.storage.set("patientSession", psession);
                    _this.data1.action = "checkindependentuser";
                    _this.data1.userid = userid;
                    _this.data1.appsecret = _this.appsecret;
                    _this.http.post(_this.baseurl + "getuser.php", _this.data1).subscribe(function (data2) {
                        console.log("dataaaaa", data2);
                        var result1 = JSON.parse(data2["_body"]);
                        console.log("asdasdfa", data2);
                        _this.storage.set("patientType", result1.status);
                        if (result.data.usertype == "patient" &&
                            result.data.firsttimelogin == 1) {
                            _this.navCtrl.setRoot("AppPatientSettingsPage");
                        }
                        else {
                            self.navCtrl.setRoot("AppTimelinePage");
                        }
                    });
                }
                else {
                    self.navCtrl.setRoot("AppDashboardPage");
                }
                self.errorMessage = "";
                if (_this.rememberme == true) {
                    var data = {
                        email: _this.email,
                        password: _this.password,
                        rememberme: true,
                    };
                    _this.storage.set("remember", data);
                }
                else {
                    _this.storage.remove("remember");
                }
            }
            else if (result.status == "inactive") {
                self.resetForm();
                self.loadingService.hide();
                self.errorMessage =
                    "Unable to Log In: Please click on the verification link we have sent to your registered email address to verify your account.";
            }
            else if (result.status == "mismatch") {
                self.resetForm();
                self.loadingService.hide();
                self.errorMessage =
                    "Unable to Log In: credentials dont match, please try again.";
            }
            else if (result.status == "notexists") {
                self.resetForm();
                self.loadingService.hide();
                self.errorMessage = "Unable to Log In: User does not exist.";
            }
            else {
                self.loadingService.hide();
                self.errorMessage = "Unable to fetch webservice.";
                console.log("errorrrrrrerere userid", _this.data1.userid);
            }
        });
    };
    AppLoginPage.prototype.presentAlert = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("dataaaaaa", data);
                        return [4 /*yield*/, this.alertCtrl.create({
                                cssClass: 'my-custom-class',
                                title: data.aps.alert.title,
                                message: data.aps.alert.body,
                                buttons: [{
                                        text: 'OK',
                                        handler: function () {
                                            _this.openmodal(data);
                                        }
                                    }]
                            })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppLoginPage.prototype.openmodal = function (data) {
        console.log("hello");
        var id = JSON.parse(data['gcm.notification.data']);
        if (data.aps.alert.title == "Add event" || data.aps.alert.title == "Add activity" || data.aps.alert.title == "Add medicine") {
            if (id.eventData) {
                if (id.eventData.eventData.id) {
                    this.updatenotification(id);
                }
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__app_timeline_app_timeline__["a" /* AppTimelinePage */], { 'eventnot': id.eventData, type: 'notification' });
            }
            else if (id.activityData) {
                if (id.activityData.activityDataid) {
                    this.updatenotification(id);
                }
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__app_timeline_app_timeline__["a" /* AppTimelinePage */], { 'activitynot': id.activityData, type: 'notification' });
            }
            else if (id.medicineData) {
                if (id.medicineData.medicineDataid) {
                    this.updatenotification(id);
                }
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__app_timeline_app_timeline__["a" /* AppTimelinePage */], { 'medicinenot': id.medicineData, type: 'notification' });
            }
        }
    };
    AppLoginPage.prototype.resetForm = function () {
        this.email = "";
        this.password = "";
    };
    AppLoginPage.prototype.goBack = function () {
        this.navCtrl.setRoot("AppWelcomeScreenPage");
    };
    AppLoginPage.prototype.goToSignUpPage = function () {
        this.navCtrl.setRoot("AppRegisterPage");
    };
    AppLoginPage.prototype.recoverPassword = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__app_password_recovery_modal_app_password_recovery_modal__["a" /* AppPasswordRecoveryModalPage */], null, {
            cssClass: "my-modal",
        });
        modal.present();
    };
    AppLoginPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    AppLoginPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    AppLoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "app-login",template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-login/app-login.html"*/'<ion-content padding class="fadeIn">\n    <ion-buttons left>\n        <button style="background-color:transparent;" (click)="goBack()" ion-button icon-only>\n            <ion-icon name="arrow-back"></ion-icon>\n        </button>\n    </ion-buttons>\n    <h1 class="app-header" style="margin-top: 15px;margin-right: 50px;">Login</h1>\n    <ion-grid style="text-align:center;position: relative;">\n        <ion-row wrap>\n            <ion-col col-12 col-sm-12 col-md-8 offset-md-2 offset-lg-3 col-lg-6 offset-xl-3 col-xl-6>\n                <!-- <h1 class="app-header">Log in</h1> -->\n                <div class="app-description margin-3t">\n                    Don\'t have an account? <span class="link" (click)="goToSignUpPage()">Sign up </span>\n                </div>\n\n                <div class="alert alert-danger fade in alert-dismissible" *ngIf="errorMessage">\n                    {{errorMessage}}\n                </div>\n\n                <ion-item margin transparent no-lines>\n                    <ion-label [whiteLabel] stacked>Email Address</ion-label>\n                    <ion-input required type="text" placeholder="Type your email address here" [(ngModel)]="email">\n                    </ion-input>\n                </ion-item>\n\n                <ion-item margin transparent no-lines>\n                    <ion-label [whiteLabel] stacked>Password</ion-label>\n                    <ion-input required [type]="passwordType" placeholder="Type your password here"\n                        [(ngModel)]="password" (keyup.enter)="onLogin()" show-hide-input>\n                    </ion-input>\n                    <ion-icon style="color:#81889f;margin-top: 50px;" [name]="passwordIcon" item-right (click)="hideShowPassword()">\n                    </ion-icon>\n                </ion-item>\n\n                <div class="app-description margin-3t">\n                    Forgot your password? <span class="link" (click)="recoverPassword()">Remind me </span>\n                </div>\n\n                <div class="app-description margin-4t">\n                    <ion-checkbox [(ngModel)]="rememberme"></ion-checkbox>\n                    <!-- Remember me and stay Log In -->\n                    Remember me for next login\n                </div>\n                \n                <div class="margin-4t">\n                    <img (click)="onLogin()" class="button-margin" src="assets/app-images/btnLogIn.png" />\n                </div>\n                <!-- <div class="app-description margin-4t">\n                    Or Log in using social media account\n                </div>\n                <div class="margin-4t">\n                    <img class="button-margin" (click)="loginwithfackbook()" src="assets/app-images/facebook.png" />\n                    <img class="button-margin" (click)="loginwithgoogle()" src="assets/app-images/google.png" />\n                    <img class="button-margin" src="assets/app-images/microsoft.png" />\n                </div> -->\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-login/app-login.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_plus__["a" /* GooglePlus */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["z" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */],
            __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__services_app_shared_service__["a" /* AppSharedService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_fcm__["a" /* FCM */]])
    ], AppLoginPage);
    return AppLoginPage;
}());

//# sourceMappingURL=app-login.js.map

/***/ })

});
//# sourceMappingURL=5.js.map