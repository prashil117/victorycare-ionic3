webpackJsonp([4],{

/***/ 928:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppDoctorEditPageModule", function() { return AppDoctorEditPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_doctor_edit__ = __webpack_require__(982);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppDoctorEditPageModule = (function () {
    function AppDoctorEditPageModule() {
    }
    AppDoctorEditPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_doctor_edit__["a" /* AppDoctorEditPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_doctor_edit__["a" /* AppDoctorEditPage */]),
            ],
        })
    ], AppDoctorEditPageModule);
    return AppDoctorEditPageModule;
}());

//# sourceMappingURL=app-doctor-edit.module.js.map

/***/ }),

/***/ 974:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailValidator; });
var EmailValidator = (function () {
    function EmailValidator() {
    }
    EmailValidator.isValid = function (control) {
        var re = /^\s*(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$/.test(control.value);
        if (re) {
            return null;
        }
        return { "invalidEmail": true };
    };
    return EmailValidator;
}());

//# sourceMappingURL=email-validator.js.map

/***/ }),

/***/ 975:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NativeGeocoder; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_core__ = __webpack_require__(32);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @name Native Geocoder
 * @description
 * Cordova plugin for native forward and reverse geocoding
 *
 * @usage
 * ```typescript
 * import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
 *
 * constructor(private nativeGeocoder: NativeGeocoder) { }
 *
 * ...
 *
 * let options: NativeGeocoderOptions = {
 *     useLocale: true,
 *     maxResults: 5
 * };
 *
 * this.nativeGeocoder.reverseGeocode(52.5072095, 13.1452818, options)
 *   .then((result: NativeGeocoderReverseResult[]) => console.log(JSON.stringify(result[0])))
 *   .catch((error: any) => console.log(error));
 *
 * this.nativeGeocoder.forwardGeocode('Berlin', options)
 *   .then((coordinates: NativeGeocoderForwardResult[]) => console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude))
 *   .catch((error: any) => console.log(error));
 * ```
 * @interfaces
 * NativeGeocoderReverseResult
 * NativeGeocoderForwardResult
 * NativeGeocoderOptions
 */
var NativeGeocoder = (function (_super) {
    __extends(NativeGeocoder, _super);
    function NativeGeocoder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Reverse geocode a given latitude and longitude to find location address
     * @param latitude {number} The latitude
     * @param longitude {number} The longitude
     * @param options {NativeGeocoderOptions} The options
     * @return {Promise<NativeGeocoderReverseResult[]>}
     */
    /**
       * Reverse geocode a given latitude and longitude to find location address
       * @param latitude {number} The latitude
       * @param longitude {number} The longitude
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderReverseResult[]>}
       */
    NativeGeocoder.prototype.reverseGeocode = /**
       * Reverse geocode a given latitude and longitude to find location address
       * @param latitude {number} The latitude
       * @param longitude {number} The longitude
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderReverseResult[]>}
       */
    function (latitude, longitude, options) { return; };
    /**
     * Forward geocode a given address to find coordinates
     * @param addressString {string} The address to be geocoded
     * @param options {NativeGeocoderOptions} The options
     * @return {Promise<NativeGeocoderForwardResult[]>}
     */
    /**
       * Forward geocode a given address to find coordinates
       * @param addressString {string} The address to be geocoded
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderForwardResult[]>}
       */
    NativeGeocoder.prototype.forwardGeocode = /**
       * Forward geocode a given address to find coordinates
       * @param addressString {string} The address to be geocoded
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderForwardResult[]>}
       */
    function (addressString, options) { return; };
    NativeGeocoder.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])({
            callbackOrder: 'reverse'
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Number, Number, Object]),
        __metadata("design:returntype", Promise)
    ], NativeGeocoder.prototype, "reverseGeocode", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])({
            callbackOrder: 'reverse'
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Object]),
        __metadata("design:returntype", Promise)
    ], NativeGeocoder.prototype, "forwardGeocode", null);
    /**
     * @name Native Geocoder
     * @description
     * Cordova plugin for native forward and reverse geocoding
     *
     * @usage
     * ```typescript
     * import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
     *
     * constructor(private nativeGeocoder: NativeGeocoder) { }
     *
     * ...
     *
     * let options: NativeGeocoderOptions = {
     *     useLocale: true,
     *     maxResults: 5
     * };
     *
     * this.nativeGeocoder.reverseGeocode(52.5072095, 13.1452818, options)
     *   .then((result: NativeGeocoderReverseResult[]) => console.log(JSON.stringify(result[0])))
     *   .catch((error: any) => console.log(error));
     *
     * this.nativeGeocoder.forwardGeocode('Berlin', options)
     *   .then((coordinates: NativeGeocoderForwardResult[]) => console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude))
     *   .catch((error: any) => console.log(error));
     * ```
     * @interfaces
     * NativeGeocoderReverseResult
     * NativeGeocoderForwardResult
     * NativeGeocoderOptions
     */
    NativeGeocoder = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["f" /* Plugin */])({
            pluginName: 'NativeGeocoder',
            plugin: 'cordova-plugin-nativegeocoder',
            pluginRef: 'nativegeocoder',
            repo: 'https://github.com/sebastianbaar/cordova-plugin-nativegeocoder',
            platforms: ['iOS', 'Android']
        })
    ], NativeGeocoder);
    return NativeGeocoder;
}(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["e" /* IonicNativePlugin */]));

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 982:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppDoctorEditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_geocoder__ = __webpack_require__(975);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_directive_email_validator__ = __webpack_require__(974);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__environment_environment__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};













// import { Routes, RouterModule } from '@angular/router';
/**
 * Generated class for the AppDoctorEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppDoctorEditPage = (function () {
    function AppDoctorEditPage(navCtrl, navParams, storage, formBuilder, nativeGeocoder, modalCtrl, alertCtrl, databaseService, loadingService, camera, http, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.nativeGeocoder = nativeGeocoder;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.camera = camera;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.newDoctors = "";
        this.doctorData = [];
        this.baseurl = __WEBPACK_IMPORTED_MODULE_12__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_12__environment_environment__["a" /* environment */].appsecret;
        this.doctorsData = [];
        this.usertype = [];
        this.topics = [];
        this.SearchData = [];
        this.data = {};
        // diagnosiss:any = {};
        this.upload = {};
        this.logo = {};
        this.action = "";
        this.image = "";
        this.enableDiv = false;
        this.selectType = "";
        this.enableDropdown = "";
        this.data1 = {};
        this.data2 = {};
        this.readonly = {};
        this.result = [];
        this.date = new Date();
        this.hcp = [];
        console.log('edit doctor', navParams.data.data);
        this.upload = "assets/images/upload1.svg";
        this.readonly = false;
        this.Profileform = formBuilder.group({
            selectType: [''],
            // email: ['',  Validators.compose([Validators.required, EmailValidator.isValid, Validators.pattern('^[a-z0-9_.+-]+@[a-z0-9-]+.[a-z0-9-.]+$')])],
            storedDoctors: ['',],
            firstname: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].maxLength(15), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].minLength(1), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].pattern('[a-zA-Z ]*'), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
            lastname: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].maxLength(15), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].minLength(1), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].pattern('[a-zA-Z ]*'), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
            email: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_8__app_directive_email_validator__["a" /* EmailValidator */].isValid, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].pattern('^[a-z0-9_.+-]+@[a-z0-9-]+.[a-z0-9-.]+$')])],
            birthDate: [''],
            mobile: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].maxLength(15), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].minLength(4), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].pattern('^[0-9]+$'), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
            password: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required],
            confirmpassword: [''],
            sex: [''],
            // languagePreferred: ['', Validators.required],
            address: [''],
            usertype: [''],
            monStart: [''],
            monEnd: [''],
            tueStart: [''],
            tueEnd: [''],
            wedStart: [''],
            wedEnd: [''],
            thuStart: [''],
            thuEnd: [''],
            friStart: [''],
            friEnd: [''],
            satStart: [''],
            satEnd: [''],
            sunStart: [''],
            sunEnd: [''],
            department: [''],
            yearofpassing: [''],
            designation: [''],
            description: [''],
            doctors: this.formBuilder.array([
                this.initDoctor(),
            ])
        });
        // });
    }
    AppDoctorEditPage.prototype.appIntialize1 = function () {
        this.data.address = (this.data.address == undefined ? "" : this.data.address);
        this.data.usertype = (this.data.usertype == undefined ? "doctor" : this.data.usertype);
        this.data.description = (this.data.description == undefined ? "" : this.data.description);
        console.log(this.navParams.data);
        this.doctorsData = this.navParams.data.doctorData;
        this.myDoctor = this.navParams.data.myDoctor;
        this.editable = this.navParams.data.editable;
        this.savebutton = (this.navParams.data.savebutton && this.navParams.data.savebutton == true) ? true : false;
        if (this.userLoginData.usertype == "healthcareprovider" || this.userLoginData.usertype == "doctor") {
            this.savebutton = true;
        }
        // this.enableDiv = (navParams.data.editable == false ? true : false);
        this.enableDiv = (this.userLoginData.usertype) == 'healthcareprovider' ? false : true;
        this.enableDropdown = (this.myDoctor != undefined ? true : false);
        this.enableSelectDropdown = (this.userLoginData.usertype == 'healthcareprovider' && this.navParams.data.editable != false) ? true : false;
        this.data1.newDoctors = this.getDoctors(this.doctorsData);
        console.log(this.result);
        console.log(this.data1.newDoctors);
        console.log(this.enableSelectDropdown);
        console.log("mydoctor", this.myDoctor);
        if (!__WEBPACK_IMPORTED_MODULE_6_lodash__["isEmpty"](this.navParams.data.data)) {
            // this.action = 'edit';
            // console.log('navParams',navParams.data);
            this.data = this.navParams.data.data;
            console.log(this.data);
            this.action = this.navParams.data.action;
            if (this.data.doctors !== undefined) {
                for (var i = 0; i < this.data.doctors.length; i++) {
                    var control = this.Profileform.controls['doctors'];
                    control.push(this.initDoctor());
                }
            }
            else {
                this.data.doctors = [];
            }
            // this.selectType();
            // console.log(this.selectType($event == 'new'));
            // }
        }
    };
    AppDoctorEditPage.prototype.ionViewWillEnter = function () {
        // this.storageGet();
        // this.data.newDoctors = this.storageGet();
        // console.log(this.data.newDoctors);
    };
    AppDoctorEditPage.prototype.storageGet = function () {
        var x0 = this.storage.get('doctorsData');
        var var1 = [];
        Promise.all([x0]).then(function (arrayOfResults) {
            var1 = arrayOfResults[0];
        });
        return var1;
    };
    AppDoctorEditPage.prototype.getDoctors = function (doctorsData) {
        console.log(doctorsData);
        var keys = [];
        var result1 = [];
        var hcpWebServiceData = {};
        var self = this;
        hcpWebServiceData.action = "getdropdowndoctorsdata";
        hcpWebServiceData.appsecret = this.appsecret;
        hcpWebServiceData.getdropdowndoctorsdata = doctorsData;
        hcpWebServiceData.hcpid = this.hcpid;
        // self.data.id = this._cookieService.get('userid');
        self.http.post(this.baseurl + "getdropdowndata.php", hcpWebServiceData).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            console.log(result);
            if (result.status == 'success') {
                console.log(result.data);
                // result.data;
                result1 = result1.push(result.data); //[result.data];
                console.log(result1);
                // return result1;
            }
            else {
                // this.toastCtrl.presentToast("Unable to fetch data");
            }
        }, function (err) {
            console.log(err);
        });
        return result1;
    };
    AppDoctorEditPage.prototype.comparer = function (otherArray) {
        return function (current) {
            return otherArray.filter(function (other) {
                // console.log(other);
                return other.email == current.email; //&& other.name == current.name
                // return other == current
            }).length == 0;
        };
    };
    AppDoctorEditPage.prototype.checkState = function (email) {
        var _this = this;
        console.log(email);
        this.loadingService.show();
        var hcpWebServiceData = {};
        hcpWebServiceData.action = "getuserbyid";
        hcpWebServiceData.id = email;
        hcpWebServiceData.appsecret = this.appsecret;
        console.log(this.data);
        this.data1.selectType = 'exist';
        this.http.post(this.baseurl + "login.php", hcpWebServiceData).subscribe(function (snapshot) {
            console.log(snapshot);
            var result = JSON.parse(snapshot["_body"]);
            // var snapshot = snapshot.val();
            if (result.status == 'success') {
                _this.data = result.data;
                _this.data.name = result.data.firstname + ' ' + result.data.lastname;
                _this.enableDropdown = false;
                _this.data.storedDoctors = true;
                _this.enableDiv = true;
                _this.loadingService.hide();
            }
            else {
                // self.loadingService.hide();
                _this.toastCtrl.presentToast("Unable to fetch data");
                // self.errorMessage = "Unable to fetch data";
            }
        });
    };
    AppDoctorEditPage.prototype.addType = function ($event) {
        console.log($event);
        if ($event == 'new') {
            //this.doRefresh($event);
            this.loadingService.show();
            // this.enableDropdown = "display:none !important;";
            // this.enableDiv = "display:block !important;";
            this.enableDropdown = false;
            this.enableDiv = false;
            this.selectType = 'addnew';
            //this.data1.selectType = false;
            this.loadingService.hide();
        }
        else if ($event == 'exist') {
            // this.doRefresh($event);
            this.loadingService.show();
            // this.enableDropdown = "display:block !important;";
            // this.enableDiv = "display:none !important;";
            this.enableDropdown = true;
            this.enableDiv = (this.userLoginData.usertype != 'healthcareprovider') ? true : false;
            this.selectType = 'addexist';
            //this.data.storedDoctors = false;
            console.log("this.data.storedoctors", this.data.storedDoctors);
            this.loadingService.hide();
        }
    };
    AppDoctorEditPage.prototype.doRefresh = function (event) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            // event.target.complete();
        }, 2000);
    };
    AppDoctorEditPage.prototype.selector = function () {
        for (var i = 0; i < this.newDoctors.length; i++) {
            this.newDoctors[i].checked = true;
        }
        console.log(this.newDoctors);
    };
    AppDoctorEditPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppDoctorEditPage');
        this.getHCPid();
    };
    AppDoctorEditPage.prototype.initDoctor = function () {
        return this.formBuilder.group({
            email: [''],
            name: ['']
        });
    };
    AppDoctorEditPage.prototype.onblur = function () {
        if (this.data1.selectType == "new") {
            if (this.data.confirmpassword === undefined) {
                this.confirmPasswordMsg = "Password cannot be empty";
                return;
            }
            else if (this.data.password !== this.data.confirmpassword) {
                this.confirmPasswordMsg = "Password does not match";
                return;
            }
            else {
                this.confirmPasswordMsg = "";
            }
        }
    };
    AppDoctorEditPage.prototype.addDoctorDetails = function () {
        var _this = this;
        var self = this;
        this.submitAttempt = true;
        if ((!this.data.storedDoctors || this.data.storedDoctors == undefined) && this.selectType == "addexist") {
            console.log("this.data", this.data.storedDoctors);
            var alert_1 = this.alertCtrl.create({
                title: 'Please select doctor',
                // subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
                buttons: ['OK']
            });
            alert_1.present();
        }
        else if (!this.Profileform.valid && this.data1.selectType == "new") {
            console.log("Profileform Validation Fire!");
        }
        else {
            console.log('this.data', this.data);
            this.Profileform.value.id = this.data.id;
            if (this.data.image != undefined) {
                this.data.image = this.data.image;
            }
            else {
                this.showImagePreview();
                this.data.image = "";
            }
            if (this.action === 'edit') {
                self.data.action = "updateuser";
                console.log(self.data);
                self.data.appsecret = this.appsecret;
                self.http.post(this.baseurl + "updateuser.php", self.data).subscribe(function (data) {
                    var result = JSON.parse(data["_body"]);
                    console.log(result.status);
                    if (result.status == 'success') {
                        self.toastCtrl.presentToast("Doctors profile is updated successfully");
                        if (_this.userLoginData.usertype == "patient") {
                            self.navCtrl.setRoot("AppMyDoctorsPage", { 'data': self.data });
                        }
                        else {
                            self.navCtrl.setRoot("AppDoctorsPage", { 'data': self.data });
                        }
                    }
                    else {
                        self.toastCtrl.presentToast("There is an error saving data!");
                        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
                    }
                }, function (err) {
                    console.log(err);
                });
                // this.updateDoctorDetails().then(function(){
                //       self.navCtrl.setRoot("AppDoctorsPage",{'data': self.data});
                //   });
            }
            else {
                console.log(this.data1.selectType);
                // return false;
                if (this.data1.selectType == 'exist') {
                    this.loadingService.show();
                    self.data.action = "adddoctortohcp";
                    self.data.userid = this.data.id;
                    self.data.hcpid = this.userLoginData.id;
                    // console.log(this.data);return false;
                    self.data.appsecret = this.appsecret;
                    self.http.post(this.baseurl + "insertrelationdata.php", self.data).subscribe(function (data) {
                        _this.loadingService.show();
                        var result = JSON.parse(data["_body"]);
                        console.log(result);
                        if (result.status == 'success') {
                            self.loadingService.hide();
                            self.toastCtrl.presentToast("Doctor added successfully");
                            self.navCtrl.setRoot("AppDoctorsPage");
                            // return result1;
                        }
                        else {
                            self.loadingService.hide();
                            self.toastCtrl.presentToast("There is a problem adding data, please try again");
                            self.navCtrl.setRoot("AppMyDoctorsPage");
                            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                        }
                    }, function (err) {
                        _this.loadingService.hide();
                        console.log(err);
                    });
                }
                else {
                    console.log(this.userLoginData.email);
                    this.loadingService.show();
                    var hcp = [];
                    var doctors = [];
                    // console.log(this.userTypeToBeAdded);
                    // self.loadingService.show();
                    self.data.action = "insert";
                    self.data.doctorid = this.userLoginData.id;
                    self.data.usertype = 'doctor';
                    self.data.appsecret = this.appsecret;
                    self.data.firsttimelogin = 0;
                    self.data.base_url = this.baseurl;
                    self.http.post(this.baseurl + "register.php", self.data).subscribe(function (data) {
                        var result = JSON.parse(data["_body"]);
                        console.log(result);
                        if (result.status1 == 'success') {
                            self.loadingService.hide();
                            self.toastCtrl.presentToast("Doctor added successfully");
                            self.navCtrl.setRoot("AppDoctorsPage");
                            // return result1;
                        }
                        else if (result.status1 == 'exists') {
                            self.loadingService.hide();
                            self.toastCtrl.presentToast("Doctor exists please add from existing or contact administrator.");
                            // self.navCtrl.setRoot("AppDoctorsPage");
                        }
                        else {
                            self.loadingService.hide();
                            // self.toastCtrl.presentToast("There is a problem adding data, please try again");
                            self.navCtrl.setRoot("AppDoctorsPage");
                            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                        }
                    }, function (err) {
                        console.log(err);
                    });
                }
            }
        }
    };
    AppDoctorEditPage.prototype.ImageUpload = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            // title: 'Image Upload',
            message: 'Select Image source',
            buttons: [
                {
                    text: 'Upload from Library',
                    handler: function () {
                        _this.gallery(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.gallery(_this.camera.PictureSourceType.CAMERA);
                    }
                }, {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    AppDoctorEditPage.prototype.gallery = function (sourceType) {
        var self = this;
        this.camera.getPicture({
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: sourceType,
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            correctOrientation: true,
            allowEdit: true
        }).then(function (imageData) {
            // imageData is a base64 encoded string
            self.showImagePreview();
            self.data.image = "data:image/jpeg;base64," + imageData;
            self.image = "data:image/jpeg;base64," + imageData;
        }, function (err) {
            console.log("Error " + err);
        });
    };
    AppDoctorEditPage.prototype.showImagePreview = function () {
        var self = this;
        self.setVisibility(self.previewImage, "block");
        self.setVisibility(self.imageUploader, "none");
        self.setVisibility(self.addPhotoCaption, "none");
        self.setVisibility(self.removePhotoIcon, "block");
    };
    AppDoctorEditPage.prototype.removeImage = function () {
        var self = this;
        this.data.image = undefined;
        self.setVisibility(self.previewImage, "none");
        self.setVisibility(self.imageUploader, "block");
        self.setVisibility(self.addPhotoCaption, "block");
        self.setVisibility(self.removePhotoIcon, "none");
    };
    AppDoctorEditPage.prototype.setVisibility = function (element, state) {
        if (element !== undefined)
            element.nativeElement.style.display = state;
    };
    AppDoctorEditPage.prototype.ngAfterViewInit = function () {
        // this.setInitialDiagnosis();
        if (this.data.contacts === undefined) {
            this.data.contacts = [];
        }
        if (!__WEBPACK_IMPORTED_MODULE_6_lodash__["isEmpty"](this.action)) {
            this.showImagePreview();
            if (this.action === "view") {
                this.readonly = true;
            }
        }
    };
    AppDoctorEditPage.prototype.logout = function () {
        this.databaseService.logout();
    };
    // set a key/value
    AppDoctorEditPage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        console.log('set string in storage: ' + result);
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        console.log(reason_1);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // to get a key/value pair
    AppDoctorEditPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        console.log('storageGET: ' + key + ': ' + result);
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_2 = _a.sent();
                        console.log(reason_2);
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppDoctorEditPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('userLogin').then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                if (_this.userLoginData.usertype == "healthcareprovider") {
                                    _this.hcpid = _this.userLoginData.id;
                                }
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppDoctorEditPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("this.userdata", this.userLoginData);
                        return [4 /*yield*/, this.get('patientType').then(function (result) {
                                console.log("patienttypesdfdsfsdsdf", result);
                                if ((!result || result == null || result == undefined) && _this.userLoginData == "patient") {
                                    _this.doRefresh('');
                                }
                                else {
                                    _this.patientType = result;
                                    _this.appIntialize1();
                                    console.log("sekeccdffvfdgdfgdfg", _this.patientType);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // remove a single key value:
    AppDoctorEditPage.prototype.remove = function (key) {
        this.storage.remove(key);
    };
    AppDoctorEditPage.prototype.getHCPid = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('currentHCP').then(function (result) {
                            if (result != null) {
                                _this.hcpid = result;
                                _this.getuserLoginData();
                            }
                            else {
                                _this.getuserLoginData();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppDoctorEditPage.prototype.getDate = function () {
        var date = new Date(this.date);
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        // return date.getDate();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('imageUploader'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppDoctorEditPage.prototype, "imageUploader", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('addPhotoCaption'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppDoctorEditPage.prototype, "addPhotoCaption", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('removePhotoIcon'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppDoctorEditPage.prototype, "removePhotoIcon", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('previewImage'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppDoctorEditPage.prototype, "previewImage", void 0);
    AppDoctorEditPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-doctor-edit',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-doctor-edit/app-doctor-edit.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title *ngIf="!this.action" text-center>Add Doctor</ion-title>\n        <ion-title *ngIf="this.action" text-center>Edit Doctor Profile</ion-title>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-buttons right (click)="logout()">\n            <button ion-button icon-only>\n                <ion-icon name="lock">Logout</ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n<ion-content>`\n    <ion-grid padding>\n        <ion-row>\n            <ion-col>\n                <div *ngIf="!this.action" class="app-description">\n                    Please fill all the info below\n                </div>\n                <div *ngIf="this.action" class="app-description">\n                    You can edit or fill all info below\n                </div>\n            </ion-col>\n        </ion-row>\n        <form [formGroup]="Profileform">\n            <ion-row wrap padding>\n                <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 padding>\n                    <ion-row>\n                        <ion-col col-2></ion-col>\n                        <ion-col col-8>\n                            <div no-padding transparent no-lines text-center class="item-width">\n                                <div *ngIf="!data.image" padding\n                                    style="background: #39415b; color:#444; border-radius: 10px">\n                                    <div #imageUploader><img style="width:30%;" [src]="upload"\n                                            (click)="!readonly && ImageUpload()"></div>\n                                    <div #addPhotoCaption style="color:#fff;">Add Doctor Photo</div>\n                                </div>\n                                <div #previewImage> <img style="border-radius:20px" [src]="data.image"\n                                        *ngIf="data.image"></div>\n                                <div #removePhotoIcon style="margin:auto;width: 10%;display: none">\n                                    <ion-icon name="close-circle" (click)="!readonly && removeImage()"></ion-icon>\n                                </div>\n                            </div>\n                        </ion-col>\n                        <ion-col col-2></ion-col>\n                    </ion-row>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            User Type\n                            <!-- <span style="color: #e0675c;margin-left: 7px;">*</span> -->\n                        </ion-label>\n                        <ion-input formControlName="usertype" style="color:#666 !important;" type="text" readonly\n                            [(ngModel)]="data.usertype"></ion-input>\n                    </ion-item>\n                    <ion-item *ngIf="enableSelectDropdown" class="item-width" style="background:none;">\n                        <ion-label>Select Action</ion-label>\n                        <ion-select formControlName="selectType" [(ngModel)]="data1.selectType"\n                            (ionChange)="addType($event)">\n                            <ion-option value="new" selected>Add New</ion-option>\n                            <ion-option value="exist">Add Existing</ion-option>\n                        </ion-select>\n                    </ion-item>\n                    <ion-item *ngIf="enableDropdown" class="item-width" style="background:none;">\n                        <ion-label>Select Doctor</ion-label>\n                        <ion-select formControlName="storedDoctors" [(ngModel)]="data.storedDoctors"\n                            (ionChange)="checkState($event)">\n                            <ion-option *ngFor="let key of data1.newDoctors[0];" value="{{key.id}}">{{key.firstname}}\n                            </ion-option>\n                        </ion-select>\n                    </ion-item>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Email Address\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-input formControlName="email" placeholder="Select your email address here"\n                            [(ngModel)]="data.email" type="text" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.email.valid && (Profileform.controls.email.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please enter valid Email</p>\n                    <!-- <ion-item padding transparent class="item-width">\n                        <ion-label stacked >\n                            Name\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-input formControlName="name" type="text" placeholder="Type your name here" [(ngModel)]="data.name" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.name.valid && (Profileform.controls.name.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please Enter Name</p> -->\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Firstname\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-input formControlName="firstname" type="text" placeholder="Type your firstname here"\n                            [(ngModel)]="data.firstname" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.firstname.valid && (Profileform.controls.firstname.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please Enter Firstname</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Lastname\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-input formControlName="lastname" type="text" placeholder="Type your lastname here"\n                            [(ngModel)]="data.lastname" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.lastname.valid && (Profileform.controls.lastname.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please Enter Lastname</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Birth Date\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-datetime formControlName="birthDate" placeholder="DD/MM/YYYY" [(ngModel)]="data.birthdate"\n                            displayFormat="DD/MM/YYYY" style="margin:8px;padding:0px" disabled="{{enableDiv}}">\n                        </ion-datetime>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.birthDate.valid && (Profileform.controls.birthDate.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please Select Birthdate</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Sex\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-select formControlName="sex" placeholder="Select your gender here" [(ngModel)]="data.sex"\n                            disabled="{{enableDiv}}">\n                            <ion-option value="f">Female</ion-option>\n                            <ion-option value="m">Male</ion-option>\n                        </ion-select>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.sex.valid && (Profileform.controls.sex.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please Select sex</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Mobile Number\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-input formControlName="mobile" placeholder="Enter Mobile Number" [(ngModel)]="data.mobile"\n                            type="text" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.mobile.valid && (Profileform.controls.mobile.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please Enter Mobile Number</p>\n   \n                </ion-col>\n                <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n                    <ion-item *ngIf="selectType===\'addnew\'" padding transparent class="item-width">\n                        <ion-label stacked>Password</ion-label>\n                        <ion-input formControlName="password" placeholder="Password" [(ngModel)]="data.password"\n                            type="password" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <ion-item *ngIf="selectType===\'addnew\'" padding transparent class="item-width">\n                        <ion-label stacked>Confirm Password</ion-label>\n                        <ion-input (ionBlur)="onblur()" formControlName="confirmpassword" placeholder="Confirm Password"\n                            [(ngModel)]="data.confirmpassword" type="password" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <p *ngIf="confirmPasswordMsg" style="color: #e0675c;margin: 0px 17px;">\n                        {{confirmPasswordMsg}}</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>Full Address</ion-label>\n                        <ion-input formControlName="address" placeholder="Street/ Building No/ Zip/ City/ Country"\n                            [(ngModel)]="data.street" type="text" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n\n                    <!-- <img class="location" src="assets/images/map.png" /> -->\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Department\n                            <!-- <span  style="color: #e0675c;margin-left: 7px;">*</span> -->\n                        </ion-label>\n                        <ion-select formControlName="department" placeholder="Select your department here"\n                            [(ngModel)]="data.department" disabled="{{enableDiv}}">\n                            <ion-option value="Cancer">Cancer</ion-option>\n                            <ion-option value="Cardio">Cardio</ion-option>\n                            <ion-option value="Orthopedic">Orthopedic</ion-option>\n                            <ion-option value="Pediatric">Pediatric</ion-option>\n                            <ion-option value="ENT">ENT</ion-option>\n                        </ion-select>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.department.valid && (Profileform.controls.department.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please Select department</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Year of Passing\n                            <!-- <span  style="color: #e0675c;margin-left: 7px;">*</span> -->\n                        </ion-label>\n                        <ion-input formControlName="yearofpassing" placeholder="Type your year of passing here"\n                            [(ngModel)]="data.yearofpassing" type="text" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.yearofpassing.valid && (Profileform.controls.yearofpassing.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please enter year of passing</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Designation\n                            <!-- <span  style="color: #e0675c;margin-left: 7px;">*</span> -->\n                        </ion-label>\n                        <ion-select formControlName="designation" placeholder="Select your designation here"\n                            [(ngModel)]="data.designation" disabled="{{enableDiv}}">\n                            <ion-option value="Cancer">MBBS</ion-option>\n                            <ion-option value="Cardio">Cardiologist</ion-option>\n                            <ion-option value="Orthopedic">Orthopedic</ion-option>\n                            <ion-option value="Pediatric">Pediatric</ion-option>\n                            <ion-option value="ENT">ENT</ion-option>\n                        </ion-select>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.designation.valid && (Profileform.controls.designation.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please Select designation</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>Description</ion-label>\n                        <ion-textarea formControlName="description" placeholder="Type description here" type="text"\n                            [(ngModel)]="data.description" disabled="{{enableDiv}}"></ion-textarea>\n                    </ion-item>\n                    <!-- <p *ngIf="!Profileform.controls.description.valid && (Profileform.controls.description.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please Add Description</p> -->\n                    <div *ngIf="savebutton" class="block-insert" margin>\n                        <button (click)="addDoctorDetails()" class="dark-button" ion-button round>\n                            Save\n                        </button>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </form>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-doctor-edit/app-doctor-edit.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_geocoder__["a" /* NativeGeocoder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_10__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_11__services_toast_service__["a" /* ToastService */]])
    ], AppDoctorEditPage);
    return AppDoctorEditPage;
}());

//# sourceMappingURL=app-doctor-edit.js.map

/***/ })

});
//# sourceMappingURL=4.js.map