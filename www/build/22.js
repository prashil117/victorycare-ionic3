webpackJsonp([22],{

/***/ 1011:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPatientsEventPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_app_shared_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__environment_environment__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











/**
 * Generated class for the AppPatientsEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppPatientsEventPage = (function () {
    function AppPatientsEventPage(viewCtrl, navCtrl, navParams, formBuilder, appSharedService, alertCtrl, modalCtrl, loadingService, databaseService, storage, http, toastCtrl) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.appSharedService = appSharedService;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.loadingService = loadingService;
        this.databaseService = databaseService;
        this.storage = storage;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.patientData = {};
        this.medicineData = [];
        this.AddedMedicine = [];
        this.baseurl = __WEBPACK_IMPORTED_MODULE_10__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_10__environment_environment__["a" /* environment */].appsecret;
        this.doctorsList = [];
        this.eventList = [];
        this.date = new Date();
        this.medicineStep2form = formBuilder.group({
            event: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            prestartdate: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            preenddate: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            recurringDaily: [],
            addToStdWheel: [],
            enableDuringDay: [],
            tim: this.formBuilder.array([
                this.initTim(),
            ])
        });
        this.data.tim = [];
        if (this.navParams.data.eventdata1) {
            this.data.event = this.navParams.data.eventdata1.id;
        }
    }
    AppPatientsEventPage.prototype.initTim = function () {
        return this.formBuilder.group({
            // dose: [''],
            starttime: ['']
        });
    };
    AppPatientsEventPage.prototype.setInitialTim = function () {
        if (this.data.tim === undefined) {
            this.data.tim = [];
        }
        if (this.data.tim.length === 0) {
            this.data.tim.push({});
        }
    };
    AppPatientsEventPage.prototype.addTim = function () {
        var control = this.medicineStep2form.controls['tim'];
        control.push(this.initTim());
        this.data.tim.push({});
        console.log("timmm", this.data.tim);
    };
    AppPatientsEventPage.prototype.deleteTim = function (index) {
        this.data.tim.splice(index, 1);
    };
    AppPatientsEventPage.prototype.intializeApp1 = function () {
        var _this = this;
        var self = this;
        this.newEventName = this.navParams.data.EventName
            ? this.navParams.data.EventName
            : "";
        this.data.action = "geteventmasterdatafulllist";
        if (this.patientType == "independent" ||
            this.userLoginData.usertype == "healthcareprovider") {
            this.data.userid = this.userLoginData.id;
            this.data.appsecret = this.appsecret;
            this.http.post(this.baseurl + "getdata.php", this.data).subscribe(function (data) {
                _this.loadingService.show();
                var result = JSON.parse(data["_body"]);
                console.log(result.data);
                if (result.status == "success") {
                    _this.loadingService.hide();
                    if (result.data !== null) {
                        _this.eventList = result.data;
                        _this.data.tim = [];
                        // if (this.data.tim !== undefined) {
                        //   for (let i = 0; i < this.data.tim.length; i++) {
                        //     const control = <FormArray>this.medicineStep2form.controls['tim'];
                        //     control.push(this.initTim());
                        //   }
                        // }
                        // else {
                        //   this.data.tim = [];
                        // }
                        if (_this.newEventName) {
                            _this.data.event = _this.eventList.find(function (e) {
                                return e.name === _this.newEventName;
                            }).id;
                        }
                    }
                    else {
                        _this.loadingService.hide();
                        _this.data = [];
                        _this.eventList = [];
                    }
                }
                else {
                    _this.loadingService.hide();
                    _this.toastCtrl.presentToast("There is a problem adding data, please try again");
                }
            }, function (err) {
                _this.loadingService.hide();
                console.log(err);
            });
        }
        else {
            this.storage.get("currentHCP").then(function (hcpid) {
                _this.data.userid = hcpid;
                _this.data.appsecret = _this.appsecret;
                _this.http.post(_this.baseurl + "getdata.php", _this.data).subscribe(function (data) {
                    _this.loadingService.show();
                    var result = JSON.parse(data["_body"]);
                    console.log(result.data);
                    if (result.status == "success") {
                        _this.loadingService.hide();
                        if (result.data !== null) {
                            _this.eventList = result.data;
                            if (_this.newEventName) {
                                _this.data.event = _this.eventList.find(function (e) {
                                    return e.name === _this.newEventName;
                                }).id;
                            }
                        }
                        else {
                            _this.loadingService.hide();
                            _this.data = [];
                            _this.eventList = [];
                        }
                    }
                    else {
                        _this.loadingService.hide();
                        _this.toastCtrl.presentToast("There is a problem adding data, please try again");
                    }
                }, function (err) {
                    _this.loadingService.hide();
                    console.log(err);
                });
            });
        }
        this.presAddedBy = this.userLoginData.id;
        if (!__WEBPACK_IMPORTED_MODULE_5_lodash__["isEmpty"](this.navParams.data)) {
            // console.log('navParams-Medicinemaster',navParams.data);
            this.patientData = this.navParams.data.patientData;
            this.medicineData = this.navParams.data.MedicineData;
            this.AddedMedicine = this.navParams.data.AddedMedicine;
        }
        /*this.data.enablePrescriptionNotification = false;
        this.data.prescriptionaddress = '';
        this.data.areaexpertise = '';
        this.data.doctorid = '';*/
        this.data.enablePrescriptionNotification =
            this.data.enablePrescriptionNotification == undefined
                ? false
                : this.data.enablePrescriptionNotification;
        this.data.prescriptionaddress = this.data.prescriptionaddress == undefined
            ? ""
            : this.data.prescriptionaddress;
        this.data.areaexpertise = this.data.areaexpertise == undefined
            ? ""
            : this.data.areaexpertise;
        // this.data.doctorid = this.data.doctorid == undefined ? '' : this.data.doctorid;
        this.data.event = this.data.event == undefined ? "" : this.data.event;
    };
    AppPatientsEventPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad AppMedicinePrescriptionIssuedPage");
        this.getHCPid();
    };
    AppPatientsEventPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss(false);
    };
    AppPatientsEventPage.prototype.addPatientEventDetails = function (patientData, index) {
        var dose = Array.prototype.map.call(this.data.tim, function (s) { return s.dose; }).toString();
        var time = Array.prototype.map.call(this.data.tim, function (s) { return s.starttime; }).toString();
        // if (dose) {
        // }
        this.data.dose = dose;
        this.data.startTime = time;
        // this.data.hcpid = this.hcpid;
        console.log("dataaddose", this.data.dose);
        console.log("dataadtime", this.data.time);
        if (this.userLoginData.usertype == "patient") {
            var uid = this.userLoginData.id;
        }
        if (index == undefined)
            index = 0;
        console.log(index);
        this.submitAttempt = true;
        // if (!this.medicineStep2form.valid)
        // {
        //   console.log("Medicine Master Step 2 Validation Fire!");
        // }
        //
        // {
        var self = this;
        for (var key in self.AddedMedicine) {
            self.data[key] = self.AddedMedicine[key];
        }
        this.data.addedBy = this.presAddedBy;
        this.data.userid = this.userLoginData.id;
        if (this.data.addtowheel == true) {
            this.data.addtowheel == "1";
        }
        else {
            this.data.addtowheel == "0";
        }
        if (this.data.notification == true) {
            this.data.notification == "1";
        }
        else {
            this.data.notification == "0";
        }
        if (this.userLoginData.usertype == "healthcareprovider") {
            this.data.hcpid = this.userLoginData.id;
        }
        else if (this.patientType == "independent") {
            this.data.hcpid = 0;
        }
        else {
            this.data.hcpid = this.hcpid;
        }
        console.log(self.data);
        self.data.action = "addpatientevent";
        self.data.appsecret = this.appsecret;
        self.http.post(this.baseurl + "adddata.php", self.data).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            console.log(result);
            if (result.status == "success") {
                self.loadingService.hide();
                self.toastCtrl.presentToast("notification added successfully");
                self.viewCtrl.dismiss(true);
            }
            else {
                self.loadingService.hide();
                self.toastCtrl.presentToast("There is a problem adding data, please try again");
                self.viewCtrl.dismiss(false);
            }
        }, function (err) {
            console.log(err);
        });
    };
    AppPatientsEventPage.prototype.getDate = function () {
        var date = new Date(this.date);
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" +
            date.getDate();
        // return date.getDate();
    };
    AppPatientsEventPage.prototype.addTime = function () {
        this.data.date.push({});
    };
    AppPatientsEventPage.prototype.deleteTime = function (index) {
        this.data.date.splice(index, 1);
    };
    AppPatientsEventPage.prototype.setInitialTime = function () {
        if (this.data.date === undefined) {
            this.data.date = [];
        }
        if (this.data.date.length === 0) {
            this.data.date.push({});
        }
    };
    AppPatientsEventPage.prototype.getHCPid = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("currentHCP").then(function (result) {
                            if (result != null) {
                                _this.hcpid = result;
                                console.log("if", _this.hcpid);
                                _this.getuserLoginData();
                            }
                            else {
                                _this.hcpid = 0;
                                _this.getuserLoginData();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPatientsEventPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("userLogin").then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPatientsEventPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("this.userdata", this.userLoginData);
                        return [4 /*yield*/, this.get("patientType").then(function (result) {
                                console.log("patienttypesdfdsfsdsdf", result);
                                if ((!result || result == null || result == undefined) &&
                                    _this.userLoginData == "patient") {
                                    _this.doRefresh("");
                                }
                                else {
                                    _this.patientType = result;
                                    _this.intializeApp1();
                                    console.log("sekeccdffvfdgdfgdfg", _this.patientType);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPatientsEventPage.prototype.doRefresh = function (event) {
        var _this = this;
        console.log("Begin async operation");
        setTimeout(function () {
            console.log("Async operation has ended");
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            // event.target.complete();
        }, 2000);
    };
    AppPatientsEventPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        else {
                            return [2 /*return*/, null];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        reason_1 = _a.sent();
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppPatientsEventPage.prototype.ngAfterViewInit = function () {
        this.setInitialTime();
    };
    AppPatientsEventPage.prototype.oneventChange = function (e) {
        console.log("this.data.event", this.data.event);
        // if(e=="other")
        // {
        //   this.navCtrl.push('AppPatientsEventMasterModalPage',{ 'patientaction': 'add' });
        // }
    };
    AppPatientsEventPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-app-patients-event",template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patients-event/app-patients-event.html"*/'<!-- <ion-header>\n    <ion-navbar>\n        <ion-title text-center>Add Patients Event</ion-title>\n    </ion-navbar>\n</ion-header> -->\n<ion-content style="background: rgb(43 50 73);">\n    <ion-buttons right>\n        <button style="background-color: transparent;" ion-button icon-only (click)="closeModal()">\n            <ion-icon name="close"></ion-icon>\n        </button>\n    </ion-buttons>\n    <ion-grid padding>\n        <ion-title style="margin-top: -35px;" text-center>Add Wellbeing Status</ion-title>\n        <!-- <form [formGroup]="medicineStep2form"> -->\n        <ion-row [formGroup]="medicineStep2form" wrap padding>\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 padding>\n                <ion-item style="margin-bottom: -35px;width: 510px;background:none;margin-top: -40px;" padding\n                    class="item-width" interface="alert">\n                    <ion-label>Select Event</ion-label>\n                    <ion-select style="color: white;" formControlName="event" [(ngModel)]="data.event" (ionChange)="oneventChange($event)">\n                        <ion-option *ngFor="let key of eventList;" value="{{key.id}}">{{key.name}}</ion-option>\n                        <!-- <ion-option value="other">other</ion-option> -->\n                    </ion-select>\n                </ion-item>\n                <ion-row style="margin-top: -32px;width: 510px;">\n                    <ion-col col-6>\n                        <ion-item style="margin-bottom: -35px;" padding transparent class="item-width">\n                            <ion-label stacked>\n                                Start Date\n                                <span style="color: #e0675c;margin-left: 7px;">*</span>\n                            </ion-label>\n                            <ion-datetime formControlName="prestartdate" [disabled]="readonly" [(ngModel)]="data.prestartdate"\n                                displayFormat="DD/MM/YY" placeholder="DD/MM/YY" max="2020-12-31" style="margin:8px;padding:0px;color:#fff;">\n                            </ion-datetime>\n                        </ion-item>\n                    </ion-col>\n                    <ion-col col-6>\n                        <ion-item style="margin-bottom: -35px;" padding transparent class="item-width">\n                            <ion-label stacked>\n                                End Date\n                                <span style="color: #e0675c;margin-left: 7px;">*</span>\n                            </ion-label>\n                            <ion-datetime formControlName="preenddate" [disabled]="readonly" placeholder="DD/MM/YY" [(ngModel)]="data.preenddate" displayFormat="DD/MM/YY"\n                                max="2020-12-31" style="margin:8px;padding:0px;color:#fff;"></ion-datetime>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n                <!-- <ion-row style="width: 510px;">\n                    <ion-col col-5>\n                        <ion-item style="margin-bottom: -35px;width: 240px;" padding transparent class="item-width">\n                            <ion-label stacked>Start Time</ion-label>\n                            <ion-datetime placeholder="00:00" [(ngModel)]="data.startTime" displayFormat="HH:mm"\n                                style="margin:8px;padding:0px;color:#fff;"></ion-datetime>\n                        </ion-item>\n                    </ion-col>\n                    <ion-col col-5>\n                        <ion-item style="margin-bottom: -45px;margin-left:37px;width: 240px;" padding transparent\n                            class="item-width">\n                            <ion-label stacked>End Time</ion-label>\n                            <ion-datetime placeholder="00:00" [(ngModel)]="data.endTime" displayFormat="HH:mm"\n                                style="margin:8px;padding:0px; color:#fff;"></ion-datetime>\n                        </ion-item>\n                    </ion-col>\n                </ion-row> -->\n                <ion-row style="margin-top: 15px;">\n                    <ion-col col-12>\n                        <div style="color:#fff;padding-bottom: 0" padding>Time</div>\n                    </ion-col>\n                </ion-row>\n                <ion-row formArrayName="tim">\n                    <ng-container *ngFor="let item of data.tim;let i = index">\n                        <!-- <ion-col col-6>\n                            <ion-item style="width: 150px;" transparent class="item-width" [formGroupName]="i">\n                                <ion-label style="display: none;" stacked>Medicine taken</ion-label>\n                                <ion-input formControlName="dose" style="margin-top:0.5em;margin-bottom:0.5em"\n                                    placeholder="No of dose" [(ngModel)]="item.dose" type="text"></ion-input>\n                            </ion-item>\n                        </ion-col> -->\n                        <ion-col col-9>\n                            <ion-item style="margin-top: 10px;" transparent class="item-width" [formGroupName]="i">\n                                <ion-label style="display: none;" stacked>Time</ion-label>\n                                <ion-datetime style="padding: 0px 0px 0px 0px!important;color: white;" formControlName="starttime"\n                                    placeholder="add time" [(ngModel)]="item.starttime" displayFormat="HH:mm">\n                                </ion-datetime>\n                            </ion-item>\n                        </ion-col>\n                        <ion-col col-1>\n                            <ion-icon name="close" (click)="deleteTim(i)" margin></ion-icon>\n                        </ion-col>\n                    </ng-container>\n                </ion-row>\n                <div style="color: white;" padding (click)="addTim()">\n                    <ion-icon name="add"></ion-icon>\n                    Add Wellbeing time\n                </div>\n                <ion-row style="width: 350px;">\n                    <ion-item style="margin-bottom: -35px;width: 510px;" padding transparent class="item-width"\n                        no-lines>\n                        <ion-label style="color:#fff;">Recurring Daily</ion-label>\n                        <ion-toggle formControlName="recurringDaily" [(ngModel)]="data.recurringDaily"></ion-toggle>\n                    </ion-item>\n                    <ion-item style="width: 510px;" style="color:#fff;" padding transparent class="item-width" no-lines>\n                        <ion-label>Add to Standard Wheel</ion-label>\n                        <ion-toggle formControlName="addToStdWheel" style="color:#fff;" [(ngModel)]="data.addtostandardwheel"></ion-toggle>\n                    </ion-item>\n                    <div class="block-insert" style="width: 555px;margin-top: -15px;" right margin>\n                        <button style="float: right;width: 180px;left: 190px;" (click)="addPatientEventDetails(patientData, index)" class="dark-button" ion-button\n                            round>Save</button>\n                    </div>\n                </ion-row>\n            \n            </ion-col>\n            <!--  -->\n        </ion-row>\n        <!-- </form> -->\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patients-event/app-patients-event.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["F" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_6__services_app_shared_service__["a" /* AppSharedService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */],
            __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_9__services_toast_service__["a" /* ToastService */]])
    ], AppPatientsEventPage);
    return AppPatientsEventPage;
}());

//# sourceMappingURL=app-patients-event.js.map

/***/ }),

/***/ 957:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppPatientsEventPageModule", function() { return AppPatientsEventPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_patients_event__ = __webpack_require__(1011);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppPatientsEventPageModule = (function () {
    function AppPatientsEventPageModule() {
    }
    AppPatientsEventPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_patients_event__["a" /* AppPatientsEventPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_patients_event__["a" /* AppPatientsEventPage */]),
            ],
        })
    ], AppPatientsEventPageModule);
    return AppPatientsEventPageModule;
}());

//# sourceMappingURL=app-patients-event.module.js.map

/***/ })

});
//# sourceMappingURL=22.js.map