webpackJsonp([44],{

/***/ 930:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppEventMasterListPageModule", function() { return AppEventMasterListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_event_master_list__ = __webpack_require__(984);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



 //<--- here
var AppEventMasterListPageModule = (function () {
    function AppEventMasterListPageModule() {
    }
    AppEventMasterListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_event_master_list__["a" /* AppEventMasterListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_event_master_list__["a" /* AppEventMasterListPage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */]
            ],
        })
    ], AppEventMasterListPageModule);
    return AppEventMasterListPageModule;
}());

//# sourceMappingURL=app-event-master-list.module.js.map

/***/ }),

/***/ 984:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppEventMasterListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_patient_event_modal_app_patient_event_modal__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_app_component__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__environment_environment__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_event_viewall_notification_app_event_viewall_notification__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};













// import { IonInfiniteScroll } from '@ionic/angular';
/**
 * Generated class for the AppEventMasterListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppEventMasterListPage = (function () {
    function AppEventMasterListPage(loadingService, navCtrl, navParams, alertCtrl, modalCtrl, databaseService, storage, formBuilder, myApp, http, toastCtrl) {
        this.loadingService = loadingService;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.databaseService = databaseService;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.myApp = myApp;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.baseurl = __WEBPACK_IMPORTED_MODULE_11__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_11__environment_environment__["a" /* environment */].appsecret;
        this.EventData = [];
        this.SearchData = [];
        this.patientData = {};
        this.descending = false;
        this.onboard = false;
        this.column = "name";
        this.errmsg = false;
        this.patientSessionData = [];
        this.dropdown = [];
        this.selected1 = [];
        this.result = [];
        this.offset = 0;
        this.count = 0;
        this.arrEventData = [];
        this.Independent = false;
        this.IssamePage = false;
        this.masterDataButton = false;
        this.Isaction = false;
        this.dependent = true;
        this.getType = navParams ? navParams.data.get : "noView";
        if (!__WEBPACK_IMPORTED_MODULE_5_lodash__["isEmpty"](navParams.data)) {
            this.patientData = navParams.data.data;
        }
        ////////// HCP DropDown Code Starts /////////////
        this.SelectList = formBuilder.group({
            currentHCP: [""],
        });
        ////////// HCP DropDown Code ends /////////////
    }
    AppEventMasterListPage.prototype.appIntialize1 = function () {
        var _this = this;
        this.Isview();
        if (this.userLoginData.usertype == "patient") {
            this.patientPreSession = true;
        }
        if (this.patientType == "independent") {
            this.dependent = false;
        }
        this.storage.get("patientSessionData").then(function (data) {
            _this.patientSessionData = data;
        });
        this.storage.get("patientSession").then(function (val) {
            if (val != undefined || _this.userLoginData.usertype == "patient") {
                _this.patientSession = true;
            }
        });
        this.myApp.initializeApp();
        this.username = this.userLoginData.email;
        this.usertype = this.userLoginData.usertype;
        this.userid = this.userLoginData.id;
        this.ut = (this.usertype == "healthcareprovider" ? false : true);
        console.log(this.userid);
        // alert(this.usertype);
        this.dropdown = this.getHCPData(this.userid, this.usertype);
        var result = null;
        this.get("currentHCP").then(function (result) {
            if (result != null) {
                _this.notselected = false;
                _this.selected1 = result;
            }
            else {
                _this.notselected = true;
                _this.selected1 = _this.dropdown;
            }
        });
        this.loadMasterEventData();
    };
    AppEventMasterListPage.prototype.ionViewDidLoad = function () {
        this.getHCPid();
        this.order = -1;
    };
    AppEventMasterListPage.prototype.AddEvent = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__app_patient_event_modal_app_patient_event_modal__["a" /* AppPatientEventModalPage */], {
            "type": "add",
            "events": this.EventData,
            "patientData": this.patientData,
            "Eventmaster": "masterdata",
        });
        modal.present();
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.EventData = [];
                _this.SearchData = [];
                _this.arrEventData = [];
                if (_this.count > 10) {
                    _this.doRefresh();
                }
                else {
                    _this.loadMasterEventData();
                }
            }
        });
    };
    AppEventMasterListPage.prototype.AddPatientsEvent = function () {
        this.navCtrl.push("AppPatientsEventPage", { "EventData": this.EventData, "patientData": this.patientData });
    };
    AppEventMasterListPage.prototype.loadMasterEventData = function (infiniteScroll) {
        var _this = this;
        this.loadingService.show();
        this.storage.get("user").then(function (patientData) {
            _this.patientData = patientData;
        });
        this.storage.get("patientSession").then(function (val) {
            _this.storage.get("user").then(function (patientData) {
                if (val == true && val != undefined &&
                    _this.userLoginData.usertype == "patient" && _this.IssamePage) {
                    // this.patientData = patientData;
                    if (_this.userLoginData.usertype == "patient") {
                        _this.patientData = {
                            id: _this.userLoginData.id,
                            email: _this.userLoginData.email,
                            name: _this.userLoginData.firstname,
                        };
                    }
                    patientData = _this.patientData;
                    _this.data.action = "getpatienteventdata";
                    _this.data.userid = patientData.id;
                    _this.data.hcpid = _this.hcpid;
                    if (_this.patientType == "independent") {
                        _this.data.userid = _this.userLoginData.id;
                        _this.hcpid = 0;
                    }
                    else if (_this.userLoginData.usertype == "heathcareprovider") {
                        _this.data.hcpid = _this.userLoginData.id;
                    }
                    _this.data.offset = _this.offset;
                    _this.data.keyword = "";
                    _this.data.appsecret = _this.appsecret;
                    _this.http.post(_this.baseurl + "getdata.php", _this.data).subscribe(function (data) {
                        var result = JSON.parse(data["_body"]);
                        if (result.status == "success") {
                            _this.loadingService.hide();
                            if (result.data !== null) {
                                for (var i in result.data) {
                                    _this.arrEventData.push(result.data[i]);
                                }
                                _this.EventData = _this.arrEventData;
                                _this.SearchData = _this.arrEventData;
                                _this.count = result.count;
                                if (infiniteScroll) {
                                    infiniteScroll.complete();
                                }
                            }
                            else {
                                _this.loadingService.hide();
                                _this.data = [];
                                _this.toastCtrl.presentToast("There is a no data available.");
                                _this.data.keys = [];
                                _this.EventData = [];
                                _this.SearchData = [];
                            }
                        }
                        else {
                            _this.loadingService.hide();
                        }
                    }, function (err) {
                        _this.loadingService.hide();
                        console.log(err);
                    });
                }
                else {
                    _this.data.action = "geteventmasterdata";
                    _this.data.id = _this.userLoginData.id;
                    _this.data.offset = _this.offset;
                    if (_this.patientType == "independent" ||
                        _this.userLoginData.usertype == "healthcareprovider") {
                        _this.data.userid = _this.userLoginData.id;
                        console.log("this.userod", _this.data.userid);
                    }
                    else {
                        _this.data.userid = _this.hcpid;
                    }
                    _this.data.keyword = "";
                    _this.data.appsecret = _this.appsecret;
                    console.log(_this.data);
                    _this.http.post(_this.baseurl + "getdata.php", _this.data).subscribe(function (data) {
                        var result = JSON.parse(data["_body"]);
                        console.log("result::::", result);
                        if (result.status == "success") {
                            if (result.data0 == null) {
                                _this.errmsg = true;
                            }
                            else {
                                _this.errmsg = false;
                            }
                            _this.loadingService.hide();
                            for (var i in result.data0) {
                                for (var j in result.data1) {
                                    if (result.data0[i].id == result.data1[j].id) {
                                        result.data0[i].numofdata = result.data1[j]
                                            ? result.data1[j].numofdata
                                            : 0;
                                    }
                                    else {
                                    }
                                }
                                _this.arrEventData.push(result.data0[i]);
                            }
                            _this.EventData = _this.arrEventData;
                            _this.SearchData = _this.arrEventData;
                            if (result.data0) {
                                _this.count = result.data0[0].count;
                            }
                            else {
                                _this.count = 0;
                            }
                            if (infiniteScroll) {
                                infiniteScroll.complete();
                            }
                        }
                        else {
                            _this.loadingService.hide();
                            _this.data = [];
                            _this.data.keys = [];
                            _this.EventData = [];
                            _this.SearchData = [];
                            _this.errmsg = true;
                            _this.loadingService.hide();
                            _this.toastCtrl.presentToast("There is a problem getting data, please try again");
                        }
                    }, function (err) {
                        _this.errmsg = true;
                        _this.loadingService.hide();
                        console.log(err);
                    });
                }
            });
        });
    };
    AppEventMasterListPage.prototype.Isview = function () {
        if (this.userLoginData.usertype == "patient" &&
            this.patientType == "independent") {
            this.Independent = true;
            this.masterDataButton = true;
            this.Isaction = true;
        }
        else if (this.userLoginData.usertype == "doctor" ||
            this.userLoginData.usertype == "nurse" ||
            this.userLoginData.usertype == "healthcareprovider") {
            this.Independent = true;
            this.masterDataButton = false;
            this.Isaction = true;
        }
        else {
            this.Independent = false;
            this.Isaction = false;
        }
    };
    AppEventMasterListPage.prototype.ViewEventMaster = function () {
        this.patientSession = false;
        this.IssamePage = false;
        this.arrEventData = [];
        this.SearchData = [];
        this.onboard = true;
        this.EventData = [];
        this.loadMasterEventData("");
        //this.navCtrl.setRoot(AppMedicineMasterListPage);
    };
    AppEventMasterListPage.prototype.loadMore = function (infiniteScroll) {
        if (this.patientData || !this.patientData) {
            if (this.EventData.length >= this.count || this.count <= 10 || this.count == 0) {
                infiniteScroll.enable(false);
            }
            else {
                this.offset = this.offset + 10;
                this.loadMasterEventData(infiniteScroll);
            }
        }
    };
    AppEventMasterListPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.navBar.backButtonClick = function (e) {
            // todo something
            _this.navCtrl.setRoot("AppPatientsPage");
        };
    };
    AppEventMasterListPage.prototype.sortField = function (fieldname) {
        // this.column = fieldname;
        this.descending = !this.descending;
        this.order = this.descending ? 1 : -1;
    };
    AppEventMasterListPage.prototype.setFilteredNameData = function () {
        var _this = this;
        // if (this.terms == '') {
        //   this.doRefresh();
        // }
        if (!this.patientSession) {
            this.data.action = "geteventmasterdata";
        }
        else {
            this.data.action = "getpatienteventdata";
        }
        this.data.offset = 0;
        this.data.keyword = this.terms;
        this.data.appsecret = this.appsecret;
        this.http.post(this.baseurl + "getdata.php", this.data).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            if (result.status == "success") {
                _this.SearchData = result.data0;
            }
            else {
                _this.SearchData = [];
            }
        }, function (err) {
            _this.SearchData = [];
        });
    };
    AppEventMasterListPage.prototype.doRefresh = function () {
        var _this = this;
        this.loadingService.show();
        setTimeout(function () {
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            _this.loadingService.hide();
        }, 2000);
    };
    AppEventMasterListPage.prototype.setFilteredEventData = function () {
        var _this = this;
        this.SearchData = this.EventData.filter(function (Event) {
            return Event.event.toLowerCase().indexOf(_this.terms.toLowerCase()) > -1;
        });
    };
    AppEventMasterListPage.prototype.editEvent = function (event, index) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__app_patient_event_modal_app_patient_event_modal__["a" /* AppPatientEventModalPage */], {
            "event": event,
            "patientData": this.patientData,
            "type": "edit",
            "index": index,
            onboard: this.onboard,
        });
        //this.navCtrl.push("AppPatientEventModalPage", { 'event': event, 'patientData': this.patientData, 'type': 'edit', 'index': index, onboard: this.onboard });
        modal.present();
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.arrEventData = [];
                _this.SearchData = [];
                _this.EventData = [];
                if (_this.count > 10) {
                    _this.doRefresh();
                }
                else {
                    _this.loadMasterEventData();
                }
            }
        });
        console.log("event");
    };
    AppEventMasterListPage.prototype.deleteEvent = function (item, index, patientData) {
        var _this = this;
        // item = [item];
        var self = this;
        console.log(item);
        console.log(patientData);
        console.log(this.SearchData);
        var alert = this.alertCtrl.create({
            title: "Confirm Delete",
            message: "Do you want to delete this record?",
            buttons: [{
                    text: "Cancel",
                    role: "cancel",
                    handler: function () {
                        // console.log('Cancel clicked');
                    },
                }, {
                    text: "Delete",
                    handler: function () {
                        var self = _this;
                        _this.data.action = "deleteevent";
                        _this.data.id = item.id;
                        _this.data.appsecret = _this.appsecret;
                        _this.http.post(_this.baseurl + "deletedata.php", _this.data).subscribe(function (data) {
                            _this.loadingService.show();
                            var result = JSON.parse(data["_body"]);
                            console.log(result.data);
                            if (result.status == "success") {
                                _this.loadingService.hide();
                                self.toastCtrl.presentToast("Wellbeing is deleted successfully");
                                self.navCtrl.setRoot("AppEventMasterListPage");
                            }
                            else {
                                self.toastCtrl.presentToast("There is an error deleting data!");
                            }
                        }, function (err) {
                            _this.loadingService.hide();
                            console.log(err);
                        });
                    },
                }],
        });
        alert.present();
    };
    AppEventMasterListPage.prototype.deletePatientEvent = function (item, index, patientData) {
        var _this = this;
        var self = this;
        var alert = this.alertCtrl.create({
            title: "Confirm Delete",
            message: "Do you want to delete patients record?",
            buttons: [{
                    text: "Cancel",
                    role: "cancel",
                    handler: function () {
                        // console.log('Cancel clicked');
                    },
                }, {
                    text: "Delete",
                    handler: function () {
                        var self = _this;
                        _this.data.action = "deletepatientevent";
                        _this.data.id = item.id;
                        _this.data.appsecret = _this.appsecret;
                        _this.http.post(_this.baseurl + "deletedata.php", _this.data).subscribe(function (data) {
                            _this.loadingService.show();
                            var result = JSON.parse(data["_body"]);
                            console.log(result.data);
                            if (result.status == "success") {
                                self.toastCtrl.presentToast("Wellbeing is deleted successfully");
                                self.navCtrl.setRoot("AppEventMasterListPage");
                            }
                            else {
                                self.toastCtrl.presentToast("There is an error deleting data!");
                            }
                        }, function (err) {
                            console.log(err);
                        });
                    },
                }],
        });
        alert.present();
    };
    AppEventMasterListPage.prototype.getHCPid = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("currentHCP").then(function (result) {
                            if (result != null) {
                                _this.hcpid = result;
                                _this.getuserLoginData();
                            }
                            else {
                                _this.getuserLoginData();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppEventMasterListPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("userLogin").then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                if (_this.userLoginData.usertype == "healthcareprovider") {
                                    _this.hcpid = _this.userLoginData.id;
                                }
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppEventMasterListPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("this.userdata", this.userLoginData);
                        return [4 /*yield*/, this.get("patientType").then(function (result) {
                                console.log("patienttypesdfdsfsdsdf", result);
                                if ((!result || result == null || result == undefined) &&
                                    _this.userLoginData == "patient") {
                                    _this.doRefresh();
                                }
                                else {
                                    _this.patientType = result;
                                    // this.appIntialize1();
                                    _this.ViewEventMaster();
                                    console.log("sekeccdffvfdgdfgdfg", _this.patientType);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppEventMasterListPage.prototype.comparer = function (otherArray) {
        return function (current, key) {
            return otherArray.filter(function (other, k) {
                return other.id == current.id; // && other.name == current.name
                // return other == current
            }).length == 0;
        };
    };
    AppEventMasterListPage.prototype.logout = function () {
        this.databaseService.logout();
    };
    ////////// HCP DropDown Code Starts /////////////
    AppEventMasterListPage.prototype.getHCPData = function (userid, usertype) {
        console.log(userid);
        console.log(usertype);
        var keys = [];
        var hcp1 = [];
        var self = this;
        var webServiceData = {};
        webServiceData.action = "gethcpdata";
        webServiceData.id = userid;
        webServiceData.usertype = usertype;
        webServiceData.appsecret = this.appsecret;
        // self.data.id = this._cookieService.get('userid');
        self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            if (result.status == "success") {
                // result.data;
                hcp1 = hcp1.push(result.data);
                // hcp1 = result.data;
                // return result1;
            }
            else {
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
        }, function (err) {
            console.log(err);
        });
        return hcp1;
    };
    AppEventMasterListPage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        console.log(reason_1);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // to get a key/value pair
    AppEventMasterListPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_2 = _a.sent();
                        console.log(reason_2);
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // remove a single key value:
    AppEventMasterListPage.prototype.remove = function (key) {
        this.storage.remove(key);
    };
    AppEventMasterListPage.prototype.selectEmployee = function ($event) {
        this.set("currentHCP", $event);
        this.doRefresh();
    };
    AppEventMasterListPage.prototype.onEventNotify = function (item) {
        var _this = this;
        // this.navCtrl.push(AppEventNotificationPage, { data: item });
        var modal = this.modalCtrl.create("AppPatientsEventPage", { eventdata1: item });
        modal.present();
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.doRefresh();
            }
        });
    };
    AppEventMasterListPage.prototype.updateAddtoWheel = function (item, e) {
        var _this = this;
        this.loadingService.show();
        console.log("eventwheel", e.checked);
        console.log("eventitem", item);
        this.data.appsecret = this.appsecret;
        this.data.action = "updatestandardwheelevent";
        this.data.standardwheel = e.checked;
        this.data.id = item.id;
        this.http.post(this.baseurl + "updatedata.php", this.data).subscribe(function (data) {
            _this.loadingService.hide();
            _this.toastCtrl.presentToast("Wheel data updated");
        }, function (err) {
            console.log("err", err);
            _this.loadingService.hide();
        });
    };
    AppEventMasterListPage.prototype.onViewAllNotifications = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__app_event_viewall_notification_app_event_viewall_notification__["a" /* AppEventViewallNotificationPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Navbar */])
    ], AppEventMasterListPage.prototype, "navBar", void 0);
    AppEventMasterListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-app-event-master-list",template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-event-master-list/app-event-master-list.html"*/'<ion-header>\n	<ion-navbar>\n		<ion-title text-center>Wellbeing</ion-title>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-buttons right *ngIf="patientSessionData">\n			<!-- <button ion-button icon-only>\n				<ion-icon name="person">Patient | {{patientSessionData.firstname}}</ion-icon>\n			</button> -->\n		</ion-buttons>\n		<form [formGroup]="SelectList">\n			<ion-item no-lines *ngIf="ut && notselected && dependent" class="item-width" style="background:none;">\n				<ion-select (ionChange)="selectEmployee($event)" style="color: white;" right>\n					<ion-label>Select Healthcare Provider</ion-label>\n					<ion-option *ngFor="let key of dropdown[0]; let idx = index;" [selected]="key.id==selected1[0].id"\n						value="{{key.id}}">{{key.firstname}}</ion-option>\n				</ion-select>\n			</ion-item>\n			<ion-item no-lines class="item-width" style="background:none;" *ngIf="ut && !notselected && dependent">\n				<ion-select (ionChange)="selectEmployee($event)" style="color: white;" right>\n					<ion-label>Select Healthcare Provider</ion-label>\n					<ion-option *ngFor="let key of dropdown[0]; let idx = index;" [selected]="key.id==selected1"\n						value="{{key.id}}">{{key.firstname}}</ion-option>\n				</ion-select>\n			</ion-item>\n		</form>\n		<!-- <ion-buttons right (click)="logout()">\n			<button ion-button icon-only>\n				<ion-icon name="lock">Logout</ion-icon>\n			</button>\n		</ion-buttons> -->\n	</ion-navbar>\n</ion-header>\n<ion-content padding class="fadeIn">\n	<ion-refresher slot="fixed" (ionRefresh)="doRefresh($event)">\n		<ion-refresher-content></ion-refresher-content>\n	</ion-refresher>\n	<h1 class="app-header" style="text-align: center;"></h1>\n	<ion-grid style="text-align:center;position: relative;">\n		<ion-row wrap>\n			<ion-col col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12>\n				<ion-row>\n					<ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n					</ion-col>\n					<ion-col col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2>\n					</ion-col>\n					<ion-col *ngIf="!patientSession || patientSession " col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n\n						<ion-col style="left: 50px;" *ngIf="!patientSession" col-12 col-sm-12 col-md-4 col-lg-4\n							col-xl-4>\n							<button (click)="onViewAllNotifications()" style="width: 220px;" class="btn-back" ion-button\n								round>View\n								All\n								Notifications</button>\n						</ion-col>\n					</ion-col>\n					<!-- <ion-col *ngIf="patientSession" col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n	            		<ion-searchbar [(ngModel)]="terms" (ionInput)="setFilteredEventData()" placeholder="Type to search here"></ion-searchbar>\n	            	</ion-col> -->\n				</ion-row>\n				<ion-grid padding *ngIf="!patientSession">\n					<ion-row wrap>\n						<ion-col style="margin: 0px auto;" col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9 offset-1\n							offset-sm-1 offset-md-1 offset-lg-1 offset-xl-1>\n							<ion-row>\n								<ion-col col-4 style="display: inline-flex;">\n									<ion-searchbar [(ngModel)]="terms" (ionInput)="setFilteredNameData()"\n										placeholder="Type to search here"></ion-searchbar>\n								</ion-col>\n								<ion-col></ion-col>\n								<ion-col></ion-col>\n								<ion-col style="margin-left: 35px;">\n									<b>Delete</b>\n								</ion-col>\n								<ion-col>\n									<b>Edit</b>\n								</ion-col>\n								<ion-col style="left: 10px;">\n									<b>Add to Wheel</b>\n								</ion-col>\n								<ion-col>\n									<b>Notify</b>\n								</ion-col>\n							</ion-row>\n							<ion-row *ngIf="errmsg">\n								<ion-col>\n									<div class="app-description" style="text-align: center;">\n										No Data Found ..!!\n									</div>\n								</ion-col>\n							</ion-row>\n							<!-- <ion-item no-lines style="background-color: #fff" *ngFor="let item of medicines;let i = index" (click)="editMedicine(item, i)" style="margin-bottom: 15px;padding:10px"> -->\n							<ion-row *ngIf="SearchData">\n								<ion-item class="item-list" no-lines\n									*ngFor="let item of SearchData | sort: {property: column, order: order}; let idx = index;"\n									style="margin-bottom: 15px;">\n									<div style="display: inline;margin-top: 1.6rem;">\n										<div [ngStyle]="{\'background\':item.color}"\n											style="float:left;color:#000;margin-top: 22px;font-weight: bold;border-radius: 50%;width:25px;height: 25px;border: 2px solid black">\n										</div>\n										<div\n											style="margin-left: 1.3rem;float:left;margin-top: 22px;font-size: 1.7rem;color: white;">\n											{{item.name}}</div>\n										<div style="float:right;margin-top: 16px;" (click)="onEventNotify(item)">\n											<img *ngIf="item.numofdata>0"\n												src="assets/images/menu-icons/notification-btn.png">\n											<img *ngIf="item.numofdata==0 || item.numofdata==undefined"\n												src="assets/images/menu-icons/notification-inactive.png">\n										</div>\n										<ion-item\n											style="background-color: transparent;float:right;margin-top: 12px;width:100px">\n											<ion-toggle [checked]="item.addtowheel === \'1\' ? true : false"\n												(ionChange)="updateAddtoWheel(item,$event)"></ion-toggle>\n										</ion-item>\n										<div style="float:right;margin-top: 9px;margin-right: -4px;"\n											(click)=" editEvent(item,idx)">\n											<!-- <button expand="block" class="shadow-grey">\n											<ion-icon style="font-size: 20px;color:white" name="md-create"></ion-icon>\n										</button> -->\n											<img style="height: 54px;width: 60px;"\n												src="assets/images/menu-icons/edit.png">\n										</div>\n										<div style="float:right;margin-top: 16px;margin-right: 20px;"\n											(click)="deleteEvent(item, idx, patientData)">\n											<!-- <button expand="block" class="shadow-red">\n											<ion-icon style="font-size: 20px;color:white" name="trash"></ion-icon>\n										</button> -->\n											<img style="height: 40px;width: 40px;"\n												src="assets/images/menu-icons/delete.png">\n										</div>\n									</div>\n									<br>\n								</ion-item>\n							</ion-row>\n						</ion-col>\n					</ion-row>\n				</ion-grid>\n\n				<ion-grid *ngIf="SearchData && patientSession">\n					<ion-row>\n						<ion-col (click)="sortField(\'name\')">\n							<b>Event Name</b>\n							<ion-icon name="md-arrow-dropdown"></ion-icon>\n						</ion-col>\n						<ion-col *ngIf="!patientSession">\n							<b>Color Name</b>\n						</ion-col>\n\n						<ion-col *ngIf="patientSession">\n							<b>Start Date</b>\n							<ion-icon name="md-arrow-dropdown"></ion-icon>\n						</ion-col>\n						<ion-col *ngIf="patientSession">\n							<b>End Date</b>\n							<ion-icon name="md-arrow-dropdown"></ion-icon>\n						</ion-col>\n						<ion-col *ngIf="Isaction">\n							<b>Action</b>\n						</ion-col>\n					</ion-row>\n					<ion-row *ngIf="errmsg">\n						<ion-col>\n							<div class="app-description" style="text-align: center;">\n								No Data Found ..!!\n							</div>\n						</ion-col>\n					</ion-row>\n					<ion-row\n						*ngFor="let item of SearchData | sort: {property: column, order: order};  let idx = index;">\n						<ion-col style="color:#fff;" (click)="editEvent(item,idx)">\n							{{item.name}}\n						</ion-col>\n						<ion-col *ngIf="!patientSession">\n							<div [ngStyle]="{\'background\':item.color}"\n								style="margin-left:90px;color:#000;width:50px;height: 25px;border: 2px solid white">\n							</div>\n						</ion-col>\n\n						<ion-col *ngIf="patientSession" style="color:#fff;">\n							{{item.presstartdate}}\n						</ion-col>\n						<ion-col *ngIf="patientSession" style="color:#fff;">\n							{{item.presenddate}}\n						</ion-col>\n						<ion-col *ngIf="patientSession && Isaction">\n							<div (click)="deletePatientEvent(item, idx, patientData)">\n								<ion-icon name="close"></ion-icon>\n							</div>\n						</ion-col>\n						<ion-col *ngIf="!patientSession && Isaction">\n							<div (click)="deleteEvent(item, idx, patientData)">\n								<ion-icon name="close"></ion-icon>\n							</div>\n						</ion-col>\n					</ion-row>\n\n				</ion-grid>\n			</ion-col>\n\n			<!-- <div *ngIf="patientSession" style="display: inline-flex;float: right;">\n					<div *ngIf="masterDataButton" text-right class="block-insert" margin>\n						<button (click)="ViewEventMaster()" style="background-color: #FB7E7E;width: 220px;"\n							class="dark-button" ion-button round>\n							View Master Event</button>\n					</div>\n					<div *ngIf="Independent" text-right class="block-insert" margin>\n						<button (click)="AddPatientsEvent()" class="dark-button" ion-button round>Add Patients\n							Event</button>\n					</div>\n				</div> -->\n		</ion-row>\n	</ion-grid>\n	<ion-infinite-scroll (ionInfinite)="loadMore($event)" loadingSpinner="bubbles" loadingText="Loading Users...">\n		<ion-infinite-scroll-content></ion-infinite-scroll-content>\n	</ion-infinite-scroll>\n</ion-content>\n<button class="boxshadow " (click)="AddEvent()"\n	style="background-color: rgb(251 126 126);height: 80px;width: 80px;position: sticky;left: 920px;top:80%;z-index: 9999;"\n	ion-fab>\n	<ion-icon style="font-size: 60px;" class="icAdd" name="add"></ion-icon>\n</button>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-event-master-list/app-event-master-list.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_7__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_8__app_app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_9__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_10__services_toast_service__["a" /* ToastService */]])
    ], AppEventMasterListPage);
    return AppEventMasterListPage;
}());

//# sourceMappingURL=app-event-master-list.js.map

/***/ })

});
//# sourceMappingURL=44.js.map