webpackJsonp([28],{

/***/ 1004:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPatientDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AppPatientDetailsPage = (function () {
    function AppPatientDetailsPage(navCtrl, navParams, alertCtrl, databaseService, loadingService, camera) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.camera = camera;
        this.data = {};
        this.upload = {};
        this.logo = {};
        this.action = "";
        this.image = "";
        this.readonly = {};
        this.logo = "assets/images/logo/victory.png";
        this.upload = "assets/images/upload1.svg";
        this.readonly = false;
        console.log('details');
        if (!__WEBPACK_IMPORTED_MODULE_5_lodash__["isEmpty"](navParams.data)) {
            this.data = navParams.data.data;
            this.action = navParams.data.action;
        }
    }
    AppPatientDetailsPage.prototype.ngAfterViewInit = function () {
        if (!__WEBPACK_IMPORTED_MODULE_5_lodash__["isEmpty"](this.action)) {
            this.showImagePreview();
            if (this.action === "view") {
                this.readonly = true;
            }
        }
    };
    AppPatientDetailsPage.prototype.editPatientDetails = function () {
        this.action = 'edit';
        this.readonly = false;
    };
    AppPatientDetailsPage.prototype.ImageUpload = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            // title: 'Image Upload',
            message: 'Select Image source',
            buttons: [
                {
                    text: 'Upload from Library',
                    handler: function () {
                        _this.gallery(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.gallery(_this.camera.PictureSourceType.CAMERA);
                    }
                }, {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    AppPatientDetailsPage.prototype.gallery = function (sourceType) {
        var self = this;
        this.camera.getPicture({
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: sourceType,
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            correctOrientation: true,
            allowEdit: true
        }).then(function (imageData) {
            // imageData is a base64 encoded string
            self.showImagePreview();
            self.data.image = "data:image/jpeg;base64," + imageData;
            self.image = "data:image/jpeg;base64," + imageData;
        }, function (err) {
            console.log("Error " + err);
        });
    };
    AppPatientDetailsPage.prototype.showImagePreview = function () {
        var self = this;
        self.setVisibility(self.previewImage, "block");
        self.setVisibility(self.imageUploader, "none");
        self.setVisibility(self.addPhotoCaption, "none");
        self.setVisibility(self.removePhotoIcon, "block");
    };
    AppPatientDetailsPage.prototype.removeImage = function () {
        var self = this;
        this.data.image = undefined;
        self.setVisibility(self.previewImage, "none");
        self.setVisibility(self.imageUploader, "block");
        self.setVisibility(self.addPhotoCaption, "block");
        self.setVisibility(self.removePhotoIcon, "none");
    };
    AppPatientDetailsPage.prototype.setVisibility = function (element, state) {
        if (element !== undefined)
            element.nativeElement.style.display = state;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('imageUploader'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppPatientDetailsPage.prototype, "imageUploader", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('addPhotoCaption'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppPatientDetailsPage.prototype, "addPhotoCaption", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('removePhotoIcon'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppPatientDetailsPage.prototype, "removePhotoIcon", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('previewImage'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppPatientDetailsPage.prototype, "previewImage", void 0);
    AppPatientDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-patient-details',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patient-details/app-patient-details.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title *ngIf="action === \'\'" text-center>Add Patient</ion-title>\n        <ion-title *ngIf="action === \'view\'" text-center>View Patient</ion-title>\n        <ion-title *ngIf="action === \'edit\'" text-center>Edit Patient</ion-title>\n\n    </ion-navbar>\n</ion-header>\n<ion-content white-background>\n    <ion-grid padding >\n        <ion-row wrap padding>\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 style="padding:30px">\n                <ion-item no-padding transparent no-lines text-center class="item-width" >\n                    <div padding style="border:2px solid #4ECDC4;color:#444; border-radius: 5px">\n                        <div #imageUploader><img style="width:40%;" [src]="upload"  (click)="!readonly && ImageUpload() "></div>\n                        <div #previewImage> <img  [src]="data.image" *ngIf="data.image" />\n                        </div>\n                        <div #removePhotoIcon style="margin:auto;width: 10%;display: none">\n                            <ion-icon name="close-circle"  (click)="!readonly && removeImage()"></ion-icon>\n                        </div>\n                        <div #addPhotoCaption><b>Add Your Photo</b></div>\n                    </div>\n                </ion-item>\n                <ion-item no-padding transparent class="item-width">\n                    <ion-label floating>Name</ion-label>\n                    <ion-input [disabled]="readonly" required type="text" [(ngModel)]="data.name" ></ion-input>\n                </ion-item>\n                <ion-item no-padding transparent class="item-width">\n                    <ion-label floating>Birth Date</ion-label>\n                    <ion-datetime [disabled]="readonly" [(ngModel)]="data.birthDate" displayFormat="DD/MM/YYYY" style="margin:8px;padding:0px"></ion-datetime>\n                </ion-item>\n                <ion-item no-padding transparent class="item-width">\n                    <ion-label floating>Sex</ion-label>\n                    <ion-select [disabled]="readonly" [(ngModel)]="data.sex">\n                        <ion-option value="f">Female</ion-option>\n                        <ion-option value="m">Male</ion-option>\n                    </ion-select>\n                </ion-item>\n                <ion-item no-padding transparent class="item-width">\n                    <ion-label floating>Email Address</ion-label>\n                    <ion-input [disabled]="readonly" [(ngModel)]="data.email" required type="text" ></ion-input>\n                </ion-item>\n                <ion-item no-padding transparent class="item-width">\n                    <ion-label floating>Address</ion-label>\n                    <ion-input [disabled]="readonly" [(ngModel)]="data.address" required type="text" ></ion-input>\n                </ion-item>\n            </ion-col>\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n                <ion-item no-padding transparent class="item-width">\n                    <ion-label floating>Language Preferred</ion-label>\n                    <ion-input [disabled]="readonly" [(ngModel)]="data.languagePreferred" required type="text" ></ion-input>\n                </ion-item>\n                <ion-item no-padding transparent class="item-width">\n                    <ion-label floating>Diagnosis</ion-label>\n                    <ion-select [disabled]="readonly" [(ngModel)]="data.diagnosis">\n                        <ion-option value="Diabetes">Diabetes</ion-option>\n                        <ion-option value="Cancer">Cancer</ion-option>\n                    </ion-select>\n                </ion-item>\n                <h1 style="margin-bottom: 0;margin-top: 4rem">Contact Persons:</h1>\n                <ion-item no-padding transparent class="item-width">\n\n                    <ion-label floating>Physician</ion-label>\n                    <ion-input [disabled]="readonly" [(ngModel)]="data.physician" required type="text" ></ion-input>\n                </ion-item>\n                <ion-item no-padding transparent class="item-width">\n                    <ion-label floating>Doctor</ion-label>\n                    <ion-input [disabled]="readonly" [(ngModel)]="data.doctor" required type="text" ></ion-input>\n                </ion-item>\n                <ion-item no-padding transparent class="item-width">\n                    <ion-label floating>Specialist</ion-label>\n                    <ion-input [disabled]="readonly" [(ngModel)]="data.specialist" required type="text" ></ion-input>\n                </ion-item>\n                <ion-item no-padding transparent class="item-width">\n                    <ion-label floating>Relative</ion-label>\n                    <ion-input [disabled]="readonly" [(ngModel)]="data.relative" required type="text" ></ion-input>\n                </ion-item>\n                <ion-item no-padding transparent class="item-width">\n                    <ion-label floating>Emergency</ion-label>\n                    <ion-input [disabled]="readonly" [(ngModel)]="data.emergency" required type="text" ></ion-input>\n                </ion-item>\n                <div class="block-insert" *ngIf="action === \'\'" style="margin-top: 20px">\n                    <button default-button block round ion-button (click)="addPatientDetails()">Done</button>\n                </div>\n                <div class="block-insert" *ngIf="action === \'view\'" style="margin-top: 20px">\n                    <button default-button block round ion-button (click)="editPatientDetails()">Edit Details</button>\n                </div>\n                <div class="block-insert" *ngIf="action === \'edit\'" style="margin-top: 20px">\n                    <button default-button block round ion-button (click)="updatePatientDetails()">Update Details</button>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patient-details/app-patient-details.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */],
            __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */]])
    ], AppPatientDetailsPage);
    return AppPatientDetailsPage;
}());

//# sourceMappingURL=app-patient-details.js.map

/***/ }),

/***/ 950:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppPatientDetailsModule", function() { return AppPatientDetailsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_patient_details__ = __webpack_require__(1004);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppPatientDetailsModule = (function () {
    function AppPatientDetailsModule() {
    }
    AppPatientDetailsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_patient_details__["a" /* AppPatientDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_patient_details__["a" /* AppPatientDetailsPage */]),
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], AppPatientDetailsModule);
    return AppPatientDetailsModule;
}());

//# sourceMappingURL=app-patient-details.module.js.map

/***/ })

});
//# sourceMappingURL=28.js.map