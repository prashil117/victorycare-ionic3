webpackJsonp([9],{

/***/ 1027:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimelineSettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TimelineSettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TimelineSettingPage = (function () {
    function TimelineSettingPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = {};
        this.patientData = {};
        this.tabs = [];
        this.eventData = [];
        if (!__WEBPACK_IMPORTED_MODULE_2_lodash__["isEmpty"](navParams.data)) {
            this.patientData = navParams.data.data;
            this.prepopulated = navParams.data.addprepopulated;
            console.log("this.patientData setting", this.patientData);
            console.log("this.prepopulated", this.prepopulated);
            if (this.prepopulated == "yes") {
                this.eventData = [
                    { "color": "#AFDB07", "name": "Awake" },
                    { "color": "#6BB1D6", "name": "Sleeping" },
                    { "color": "#FAD000", "name": "Uncomfortable" },
                    { "color": "#E26060", "name": "Pain" },
                ];
                console.log("this.eventData", this.eventData);
            }
        }
        this.data.tabs = [
            {
                page: "AppPatientEventSettingsPage",
                title: "CURRENT STATUS",
                params: this.patientData,
            },
            {
                page: "AppPatientMedicineListPage",
                title: "MEDICINE",
                params: this.patientData,
            },
            {
                page: "AppPatientActivityListPage",
                title: "ACTIVITY",
                params: this.patientData,
            },
        ];
    }
    TimelineSettingPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad TimelineSettingPage");
    };
    TimelineSettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-timeline-setting",template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/timeline-setting/timeline-setting.html"*/'<!--\n  Generated template for the TimelineSettingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Timeline Setting</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n        <ion-col col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2>\n         \n        </ion-col>\n        <ion-col col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 style="display: contents">\n              <ion-tabs class="margin-5t" #tabs  tabsPlacement=\'top\' tabs-content >\n                        <ion-tab [rootParams]="patientData" [tabTitle]="item.title" [root]="item.page" *ngFor="let item of data.tabs;let i = index"></ion-tab>\n                    </ion-tabs>\n        </ion-col>\n        <ion-col col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2>\n          \n        </ion-col>\n\n    </ion-row>\n</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/timeline-setting/timeline-setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */]])
    ], TimelineSettingPage);
    return TimelineSettingPage;
}());

//# sourceMappingURL=timeline-setting.js.map

/***/ }),

/***/ 973:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimelineSettingPageModule", function() { return TimelineSettingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__timeline_setting__ = __webpack_require__(1027);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TimelineSettingPageModule = (function () {
    function TimelineSettingPageModule() {
    }
    TimelineSettingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__timeline_setting__["a" /* TimelineSettingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__timeline_setting__["a" /* TimelineSettingPage */]),
            ],
        })
    ], TimelineSettingPageModule);
    return TimelineSettingPageModule;
}());

//# sourceMappingURL=timeline-setting.module.js.map

/***/ })

});
//# sourceMappingURL=9.js.map