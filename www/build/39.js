webpackJsonp([39],{

/***/ 936:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppLoggedActivityTabModule", function() { return AppLoggedActivityTabModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_logged_activity_tab__ = __webpack_require__(990);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppLoggedActivityTabModule = (function () {
    function AppLoggedActivityTabModule() {
    }
    AppLoggedActivityTabModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_logged_activity_tab__["a" /* AppLoggedActivityTabPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_logged_activity_tab__["a" /* AppLoggedActivityTabPage */])
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]],
        })
    ], AppLoggedActivityTabModule);
    return AppLoggedActivityTabModule;
}());

//# sourceMappingURL=app-logged-activity-tab.module.js.map

/***/ }),

/***/ 990:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppLoggedActivityTabPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_app_shared_service__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AppLoggedActivityTabPage = (function () {
    function AppLoggedActivityTabPage(navCtrl, navParams, alertCtrl, databaseService, loadingService, modalCtrl, appSharedService) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.modalCtrl = modalCtrl;
        this.appSharedService = appSharedService;
        this.patientData = {};
        this.patientSettings = {};
        this.data = {};
        this.selectedDate = {};
        this.selectedTime = {};
        this.readOnly = true;
        var self = this;
        if (navParams.data !== undefined) {
            this.selectedDate = navParams.data.selectedDate;
            this.selectedTime = navParams.data.selectedTime;
        }
        if (this.appSharedService.patientData !== undefined) {
            this.patientData = this.appSharedService.patientData;
        }
    }
    AppLoggedActivityTabPage.prototype.setDummyValues = function () {
        var self = this;
        __WEBPACK_IMPORTED_MODULE_4_lodash__["each"](self.appSharedService.patientSettings.activityData, function (item, index) {
            if (self.data[index] === undefined) {
                self.data[index] = { 'what': '', 'amount': '' };
            }
        });
    };
    AppLoggedActivityTabPage.prototype.getSlot = function (interval) {
        var selectedHour = this.selectedTime.split(':')[0];
        var selectedMinutes = this.selectedTime.split(':')[1];
        return selectedHour * (60 / interval) + (selectedMinutes % interval === 0 ? (selectedMinutes / interval) + 1 : (selectedMinutes / interval) - ((selectedMinutes / interval) % 1) + 1);
    };
    AppLoggedActivityTabPage.prototype.getDate = function () {
        var date = new Date(this.selectedDate);
        return date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        // return (new Date(this.selectedDate)).toLocaleDateString().split('/').join('-');
    };
    AppLoggedActivityTabPage.prototype.makeEditable = function () {
        this.readOnly = false;
    };
    AppLoggedActivityTabPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-logged-activity-tab',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-logged-activity-tab/app-logged-activity-tab.html"*/'<ion-content style="background: black;height:100vh;color:#fff">\n\n   <ion-grid padding>\n      <ion-row wrap>\n         <ion-col col-8 offset-2 col-sm-8  offset-sm-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2>\n            <div no-lines *ngFor="let item of patientSettings.activityData;let i = index" style="background-color: #000;margin-bottom: 15px;padding:10px">\n               <h3 style="color:#fff">{{item.name}}</h3>\n\n               <ion-row wrap>\n               <ion-col width-50>\n                  <ion-item no-lines style="background-color: #000;padding:10px">\n                     <ion-label floating>What</ion-label> <ion-input  [disabled]="readOnly" [(ngModel)]="data[i].what"  required type="text" ></ion-input>\n\n                  </ion-item>\n               </ion-col>\n\n               <ion-col width-50>\n                  <ion-item no-lines style="background-color: #000;padding:10px">\n                     <ion-label floating>Amount</ion-label>  <ion-input [disabled]="readOnly" [(ngModel)]="data[i].amount"  required type="text" ></ion-input>\n                  </ion-item>\n               </ion-col>\n               </ion-row>\n\n            </div>\n\n            <div style="margin-top: 20px">\n               <button default-button block round ion-button *ngIf="!readOnly" (click)="updateLoggedActivities()" ><ion-icon name="add-circle"></ion-icon>Update</button>\n               <button default-button block round ion-button *ngIf="readOnly"  (click)="makeEditable()" ><ion-icon name="add-circle"></ion-icon>Edit</button>\n\n            </div>\n         </ion-col>\n      </ion-row>\n   </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-logged-activity-tab/app-logged-activity-tab.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */], __WEBPACK_IMPORTED_MODULE_5__services_app_shared_service__["a" /* AppSharedService */]])
    ], AppLoggedActivityTabPage);
    return AppLoggedActivityTabPage;
}());

//# sourceMappingURL=app-logged-activity-tab.js.map

/***/ })

});
//# sourceMappingURL=39.js.map