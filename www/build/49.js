webpackJsonp([49],{

/***/ 924:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Addproduct2PageModule", function() { return Addproduct2PageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__addproduct2__ = __webpack_require__(978);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var Addproduct2PageModule = (function () {
    function Addproduct2PageModule() {
    }
    Addproduct2PageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__addproduct2__["a" /* Addproduct2Page */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__addproduct2__["a" /* Addproduct2Page */]),
            ],
        })
    ], Addproduct2PageModule);
    return Addproduct2PageModule;
}());

//# sourceMappingURL=addproduct2.module.js.map

/***/ }),

/***/ 978:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Addproduct2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { HttpClient, HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the Addproduct2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Addproduct2Page = (function () {
    function Addproduct2Page(navCtrl, navParams, http, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.toast = toast;
        this.product = {};
    }
    Addproduct2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Addproduct2Page');
    };
    Addproduct2Page.prototype.insert = function () {
        var _this = this;
        console.log(this.product);
        // var headers = new HttpHeaders();
        // headers.append("Accept", 'application/json');
        // headers.append('Content-Type', 'application/json');
        // headers.append('Access-Control-Allow-Origin', '*');
        this.product.action = "insert";
        this.http.post("http://myswprojects.com/prodcust/webservice/product.php", this.product).subscribe(function (data) {
            console.log(data);
            var result = JSON.parse(data["_body"]);
            if (result.status == "success") {
                _this.showToast("Inserted successfully");
            }
            else {
                _this.showToast("Something went wrong");
            }
        }, function (err) {
            console.log(err);
        });
    };
    Addproduct2Page.prototype.showToast = function (message) {
        var toast = this.toast.create({
            message: message,
            duration: 2000
        });
        toast.present();
    };
    Addproduct2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-addproduct2',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/addproduct2/addproduct2.html"*/'<ion-header>\n    <ion-navbar>\n    <ion-title>Add New Product</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <ion-list>\n      <ion-item>\n        <ion-label stacked>Title</ion-label>\n        <ion-input type="text" [(ngModel)]="product.title"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label stacked>Description</ion-label>\n        <ion-input type="text" [(ngModel)]="product.desc"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label stacked>Price</ion-label>\n        <ion-input type="text" [(ngModel)]="product.price"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label stacked>Pic URL</ion-label>\n        <ion-input type="text" [(ngModel)]="product.pic"></ion-input>\n      </ion-item>\n      <button ion-button (click)="insert()">Insert</button>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/addproduct2/addproduct2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["D" /* ToastController */]])
    ], Addproduct2Page);
    return Addproduct2Page;
}());

//# sourceMappingURL=addproduct2.js.map

/***/ })

});
//# sourceMappingURL=49.js.map