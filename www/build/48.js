webpackJsonp([48],{

/***/ 925:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppAddPrescriptionMasterPageModule", function() { return AppAddPrescriptionMasterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_add_prescription_master__ = __webpack_require__(979);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppAddPrescriptionMasterPageModule = (function () {
    function AppAddPrescriptionMasterPageModule() {
    }
    AppAddPrescriptionMasterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_add_prescription_master__["a" /* AppAddPrescriptionMasterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_add_prescription_master__["a" /* AppAddPrescriptionMasterPage */]),
            ],
        })
    ], AppAddPrescriptionMasterPageModule);
    return AppAddPrescriptionMasterPageModule;
}());

//# sourceMappingURL=app-add-prescription-master.module.js.map

/***/ }),

/***/ 979:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppAddPrescriptionMasterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AppAddPrescriptionMasterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppAddPrescriptionMasterPage = (function () {
    function AppAddPrescriptionMasterPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AppAddPrescriptionMasterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppAddPrescriptionMasterPage');
    };
    AppAddPrescriptionMasterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-add-prescription-master',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-add-prescription-master/app-add-prescription-master.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title text-center>Add Prescription</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content>\n    <ion-grid padding >\n        <ion-row>\n            <ion-col>\n                <div class="app-description">\n                    Please fill all the info below\n                </div>\n            </ion-col>\n        </ion-row>\n        <form [formGroup]="medicineform">\n            <ion-row wrap padding>\n                <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 padding>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked >\n                            Medicine Name\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-input type="text" placeholder="Type name of medicine here"  formControlName="medicinename" [(ngModel)]="data.medicinename"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!medicineform.controls.medicinename.valid && (medicineform.controls.medicinename.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please enter Medicine Name</p>\n                    <ion-row class="colorselect color_{{data.color.slice(1,7)}}">\n                        <ion-item padding class="item-width" transparent>\n                            <ion-label class="primarylabel">Choose Color</ion-label>\n                            <ion-select (click)="prepareColorSelector()" (ionChange)="setColor(data.color)" formControlName="color" [(ngModel)]="data.color">\n                                <ion-option (ionSelect)="selectColor(optioncolor)" *ngFor="let optioncolor of colors" [value]="optioncolor">{{optioncolor}}</ion-option>\n                            </ion-select>\n                        </ion-item>\n                    </ion-row>\n                    <p *ngIf="!medicineform.controls.color.valid && (medicineform.controls.color.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please Choose colour</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label>Add to Standard Wheel</ion-label>\n                        <ion-toggle [(ngModel)]="data.enableStandardWheel" formControlName="enableStandardWheel"></ion-toggle>\n                    </ion-item>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked >Active Ingredients</ion-label>\n                        <ion-input type="text" placeholder="Type active ingredients here" formControlName="activeingredients" [(ngModel)]="data.activeingredients"></ion-input>\n                    </ion-item>\n\n                </ion-col>\n                <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 padding>\n                	<ion-row>\n                    <ion-col col-12>\n    		                <ion-item padding transparent class="item-width">\n    		                    <ion-label stacked>\n                                    Company\n                                    <span style="color: #e0675c;margin-left: 7px;">*</span>\n                                </ion-label>\n    		                    <ion-input  placeholder="Type company here" type="text" formControlName="dose" [(ngModel)]="data.dose"></ion-input>\n    		                </ion-item>\n                            <p *ngIf="!medicineform.controls.dose.valid && (medicineform.controls.dose.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please enter Dose</p>\n    	                </ion-col>\n    	            </ion-row>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Area of Use\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-input  placeholder="Type area of use here" type="text" formControlName="areaofuse" [(ngModel)]="data.areaofuse"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!medicineform.controls.areaofuse.valid && (medicineform.controls.areaofuse.dirty || submitAttempt)"  style="color: #e0675c;margin: 0px 17px;">Please enter Area of Use</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label>Notification for use during the day?</ion-label>\n                        <ion-toggle [(ngModel)]="data.enablenotification" formControlName="enablenotification"></ion-toggle>\n                    </ion-item>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>Notes</ion-label>\n                        <ion-textarea  placeholder="Type notes here" type="text" formControlName="notes" [(ngModel)]="data.notes"></ion-textarea>\n                    </ion-item>\n                    <ion-row>\n                        <div class="block-insert" margin right>\n                            <button (click)="addPatientMedicineDetails()" right class="dark-button" ion-button round>Save</button>\n                        </div>\n                    </ion-row>\n                    <ion-row>\n                        <div class="app-description margin-3t">\n                            Is this a prescripted medicine, <span class="link" (click)="addPatientMedicineDetails(\'prescription\')">click here to add more details </span>\n                        </div>\n                    </ion-row>\n                </ion-col>\n            </ion-row>\n        </form>\n    </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-add-prescription-master/app-add-prescription-master.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */]])
    ], AppAddPrescriptionMasterPage);
    return AppAddPrescriptionMasterPage;
}());

//# sourceMappingURL=app-add-prescription-master.js.map

/***/ })

});
//# sourceMappingURL=48.js.map