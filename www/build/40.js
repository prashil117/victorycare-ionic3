webpackJsonp([40],{

/***/ 935:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppLoggedActivityHomeModule", function() { return AppLoggedActivityHomeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_logged_activity_home__ = __webpack_require__(989);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppLoggedActivityHomeModule = (function () {
    function AppLoggedActivityHomeModule() {
    }
    AppLoggedActivityHomeModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_logged_activity_home__["a" /* AppLoggedActivityHome */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_logged_activity_home__["a" /* AppLoggedActivityHome */])
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]],
        })
    ], AppLoggedActivityHomeModule);
    return AppLoggedActivityHomeModule;
}());

//# sourceMappingURL=app-logged-activity-home.module.js.map

/***/ }),

/***/ 989:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppLoggedActivityHome; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppLoggedActivityHome = (function () {
    function AppLoggedActivityHome(navCtrl, navParams, alertCtrl, loadingService) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingService = loadingService;
        this.data = {};
        this.patientData = {};
        this.patientSettings = {};
        this.tabs = [];
        if (!__WEBPACK_IMPORTED_MODULE_4_lodash__["isEmpty"](navParams.data)) {
            this.patientData = navParams.data.patientData;
            this.patientSettings = navParams.data.patientSettings;
            var params = {
                'selectedDate': navParams.data.selectedDate,
                'selectedTime': navParams.data.selectedTime
            };
            this.data.tabs = [{ page: "AppLoggedGeneralTabPage", title: "General Details", params: params },
                { page: "AppLoggedActivityTabPage", title: "Activity Details", params: params },
                { page: "AppLoggedMedicinesTabPage", title: "Medicine Details", params: params }
            ];
        }
    }
    AppLoggedActivityHome.prototype.calculateAge = function (birthDate) {
        var birthday = new Date(birthDate);
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    };
    AppLoggedActivityHome.prototype.backToPatientHome = function () {
        this.navCtrl.setRoot("AppPatientHomePage", { 'data': this.patientData });
    };
    AppLoggedActivityHome = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-logged-activity-home',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-logged-activity-home/app-logged-activity-home.html"*/'<ion-header>\n    <ion-navbar >\n        <ion-buttons left>\n            <button ion-button icon-only (click)="backToPatientHome()">\n                <ion-icon name="arrow-back" ></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title text-center="" *ngIf="patientData.name">Patient Data - {{patientData.name}}</ion-title>\n       <!-- <ion-buttons right>\n            <button ion-button icon-only><ion-icon name="settings"></ion-icon></button>\n        </ion-buttons>-->\n    </ion-navbar>\n</ion-header>\n<ion-content login-background>\n\n    <ion-grid style="padding:0">\n        <ion-row wrap>\n            <ion-col col-9 col-sm-12 col-md-9 col-lg-9 col-xl-9 style="background: black;height:100vh;">\n\n                <ion-tabs #tabs tabsPlacement=\'top\' tabs-content >\n                    <ion-tab [rootParams]="item.params" [tabTitle]="item.title" [root]="item.page" *ngFor="let item of data.tabs;let i = index"></ion-tab>\n                </ion-tabs>\n\n            </ion-col>\n            <ion-col col-3 col-sm-12 col-md-3 col-lg-3 col-xl-3 style="height:100vh;">\n                <ion-item no-lines style="background: transparent">\n                    <img [src]="patientData.image" *ngIf="patientData.image" style="width: 100%">\n                </ion-item>\n\n                    <h1 text-center *ngIf="patientData.name" style="color:#fff;text-transform: uppercase;"><b>{{patientData.name}}</b></h1>\n\n                <h3 style="color:#fff;padding: 5px;margin-top:2rem" *ngIf="patientData.birthDate">Age : {{calculateAge(patientData.birthDate)}}</h3>\n\n                    <h3 style="color:#fff;padding: 5px" *ngIf="patientData.diagnosis">Condition : {{patientData.diagnosis}}</h3>\n\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-logged-activity-home/app-logged-activity-home.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */]])
    ], AppLoggedActivityHome);
    return AppLoggedActivityHome;
}());

//# sourceMappingURL=app-logged-activity-home.js.map

/***/ })

});
//# sourceMappingURL=40.js.map