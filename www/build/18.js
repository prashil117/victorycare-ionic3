webpackJsonp([18],{

/***/ 1016:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSelectWheelTimePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AppSelectWheelTimePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppSelectWheelTimePage = (function () {
    function AppSelectWheelTimePage(navCtrl, modalCtrl, viewCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.wheelTime = 15;
    }
    AppSelectWheelTimePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppSelectWheelTimePage');
    };
    AppSelectWheelTimePage.prototype.next = function () {
        this.viewCtrl.dismiss(this.wheelTime);
    };
    AppSelectWheelTimePage.prototype.close = function () {
        this.viewCtrl.dismiss(this.wheelTime);
    };
    AppSelectWheelTimePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-select-wheel-time',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-select-wheel-time/app-select-wheel-time.html"*/'<!--\n  Generated template for the AppSelectWheelTimePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <!-- <ion-navbar>\n    <ion-title>app-select-wheel-time</ion-title>\n  </ion-navbar> -->\n\n</ion-header>\n\n\n<ion-content style="background-color:#2b3249 !important;" padding>\n  <div >\n    <img (click)="close()" src="assets/images/others/icn-close.png" srcset="assets/images/others/icn-close.png" class="icn_close" data-dismiss="modal">\n    <br style="clear: both;" />\n    <h2 class="modaltitle">Wheel Chart Settings</h2>\n    <p class="modaldata">The Daily Wheel is showing your selective status in a objective way. The more granular with registrating your Current Well Being Status while your are ill, the easier it is for both you and your care personnel to understand how Well you Feel.</p>\n    <p class="modaldata">For Chronical users we recomment every 15 to 30 minutes, and for other long term ill we recomment from every 30 minutes to 2 hours, while for more short term illness we recomment every 2 -4 hours.</p>\n    <p class="modaldata">But hey, remember you can always change this, but the more detailed you can be, the better insight you get about your Current Status of your Well Being and health.</p>\n    <select [(ngModel)]="wheelTime" name="initialminute" class="modalselect">\n      <option value="15">15 Min</option>\n      <option value="30">30 Min</option>\n    </select>\n    <br />\n    <input (click)="next()" type="button" id="nextstep" class="modalbutton" value="Next" />\n    <br style="clear: both;" />\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-select-wheel-time/app-select-wheel-time.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["F" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */]])
    ], AppSelectWheelTimePage);
    return AppSelectWheelTimePage;
}());

//# sourceMappingURL=app-select-wheel-time.js.map

/***/ }),

/***/ 962:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppSelectWheelTimePageModule", function() { return AppSelectWheelTimePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_select_wheel_time__ = __webpack_require__(1016);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppSelectWheelTimePageModule = (function () {
    function AppSelectWheelTimePageModule() {
    }
    AppSelectWheelTimePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_select_wheel_time__["a" /* AppSelectWheelTimePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_select_wheel_time__["a" /* AppSelectWheelTimePage */]),
            ],
        })
    ], AppSelectWheelTimePageModule);
    return AppSelectWheelTimePageModule;
}());

//# sourceMappingURL=app-select-wheel-time.module.js.map

/***/ })

});
//# sourceMappingURL=18.js.map