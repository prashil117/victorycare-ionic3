webpackJsonp([16],{

/***/ 1018:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppSettingsPage = (function () {
    function AppSettingsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AppSettingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppSettingsPage');
    };
    AppSettingsPage.prototype.getLocation = function (value) {
        var target = value == "profile" ? "AppUserEditPage" :
            value == "notification" ? "AppNotificationSettingsPage" :
                value == "privacy" ? "AppPrivacyPage" :
                    value == "how" ? "AppHowDoesWorkPage" :
                        value == "wheel" ? "AppSettingsDailyWheelSettingsPage" :
                            value == "security" ? "AppSecurityAndAccessSettingsPage" :
                                value == "about" ? "AppSettingAboutVictoryPage" : "AppSettingsTermsAndConditionsPage";
        this.navCtrl.push(target);
    };
    AppSettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-settings',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-settings/app-settings.html"*/'<!--\n  Generated template for the AppSettingsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Settings</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <ion-item (click)="getLocation(\'profile\')" class="item-list" no-lines style="margin-bottom: 15px;">\n    <img style="margin-top: 10px;" src="assets/images/settingsicon/profile-icn.png" />\n    <span class="span-text"> Profile Settings</span>\n  </ion-item>\n  <ion-item (click)="getLocation(\'privacy\')" class="item-list" no-lines style="margin-bottom: 15px;">\n    <img style="margin-top: 10px;" src="assets/images/settingsicon/privacy-icn.png" />\n    <span class="span-text"> Privacy</span>\n  </ion-item>\n  <ion-item (click)="getLocation(\'notification\')" class="item-list" no-lines style="margin-bottom: 15px;">\n    <img style="margin-top: 10px;" src="assets/images/settingsicon/notification.png" />\n    <span class="span-text"> Notifications</span>\n  </ion-item>\n  <ion-item (click)="getLocation(\'how\')" class="item-list" no-lines style="margin-bottom: 15px;">\n    <img style="margin-top: 10px;" src="assets/images/settingsicon/que.png" />\n    <span class="span-text"> How does it work?</span>\n  </ion-item>\n  <ion-item (click)="getLocation(\'wheel\')" class="item-list" no-lines style="margin-bottom: 15px;">\n    <img style="margin-top: 10px;" src="assets/images/settingsicon/wheel.png" />\n    <span class="span-text"> 24-Hour Clock Settings”</span>\n  </ion-item>\n  <!-- <ion-item (click)="getLocation(\'security\')" class="item-list" no-lines style="margin-bottom: 15px;">\n    <img style="margin-top: 10px;" src="assets/images/settingsicon/key.png" />\n    <span class="span-text"> Security and Access settings</span>\n  </ion-item> -->\n  <ion-item (click)="getLocation(\'about\')" class="item-list" no-lines style="margin-bottom: 15px;">\n    <img style="margin-top: 10px;" src="assets/images/settingsicon/info.png" />\n    <span class="span-text"> About Victor(y) Care</span>\n  </ion-item>\n  <ion-item (click)="getLocation(\'terms\')" class="item-list" no-lines style="margin-bottom: 15px;">\n    <img style="margin-top: 10px;" src="assets/images/settingsicon/terms.png" />\n    <span class="span-text"> Terms & conditons and Privacy Policy</span>\n  </ion-item>\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-settings/app-settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */]])
    ], AppSettingsPage);
    return AppSettingsPage;
}());

//# sourceMappingURL=app-settings.js.map

/***/ }),

/***/ 964:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppSettingsPageModule", function() { return AppSettingsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_settings__ = __webpack_require__(1018);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppSettingsPageModule = (function () {
    function AppSettingsPageModule() {
    }
    AppSettingsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettingsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettingsPage */]),
            ],
        })
    ], AppSettingsPageModule);
    return AppSettingsPageModule;
}());

//# sourceMappingURL=app-settings.module.js.map

/***/ })

});
//# sourceMappingURL=16.js.map