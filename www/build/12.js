webpackJsonp([12],{

/***/ 1026:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppWelcomeScreenPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_app_shared_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AppWelcomeScreenPage = (function () {
    function AppWelcomeScreenPage(storage, navCtrl, navParams, alertCtrl, databaseService, loadingService, appSharedService, menu) {
        var _this = this;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.appSharedService = appSharedService;
        this.menu = menu;
        // if (this.storage.get('remember')) {
        //     console.log("userfsdf")
        //     this.navCtrl.setRoot("ApptimelinePage");
        // }
        // else{
        //     console.log("userfsdfsdfsfsdf")
        // }
        this.storage.get('userLogin').then(function (val) {
            if (val) {
                _this.navCtrl.setRoot("AppTimelinePage");
            }
        });
    }
    AppWelcomeScreenPage.prototype.signUp = function () {
        this.navCtrl.setRoot("AppRegisterPage");
    };
    AppWelcomeScreenPage.prototype.signIn = function () {
        this.navCtrl.setRoot("AppLoginPage");
    };
    /*ionViewDidLoad() {
        this.menu.swipeEnable(false);
    }*/
    AppWelcomeScreenPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    AppWelcomeScreenPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    AppWelcomeScreenPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-welcome-screen',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-welcome-screen/app-welcome-screen.html"*/'<ion-content class="fadeIn">\n    <ion-grid class="screen-center">\n        <ion-row wrap>\n            <ion-col col-12 col-sm-12 col-md-8 offset-md-2 offset-lg-3 col-lg-6 offset-xl-3 col-xl-6>\n\n                    <img src="assets/imgs/logo-victory.png" />\n                <h1 class="app-header">Welcome to Victor(y) Care</h1>\n                <div class="app-description margin-3t">\n\n                    We give chronical ill and at-home care patients at tool to gain insight into their subjective health status\n                    and helping them communicate their health status clearly and precise to care professionals and their relatives.\n                </div>\n                <div class="margin-4t">\n                    <img (click)="signUp()" class="button-margin" src="assets/app-images/btnSignUp.png" />\n                    <img (click)="signIn()" class="button-margin" src="assets/app-images/btnLogIn.png"/>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-welcome-screen/app-welcome-screen.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__services_app_shared_service__["a" /* AppSharedService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* MenuController */]])
    ], AppWelcomeScreenPage);
    return AppWelcomeScreenPage;
}());

//# sourceMappingURL=app-welcome-screen.js.map

/***/ }),

/***/ 972:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppWelcomeScreenModule", function() { return AppWelcomeScreenModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_welcome_screen__ = __webpack_require__(1026);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppWelcomeScreenModule = (function () {
    function AppWelcomeScreenModule() {
    }
    AppWelcomeScreenModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_welcome_screen__["a" /* AppWelcomeScreenPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_welcome_screen__["a" /* AppWelcomeScreenPage */]),
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], AppWelcomeScreenModule);
    return AppWelcomeScreenModule;
}());

//# sourceMappingURL=app-welcome-screen.module.js.map

/***/ })

});
//# sourceMappingURL=12.js.map