webpackJsonp([13],{

/***/ 1020:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppWeeklyDashboard2ReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(569);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environment_environment__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var option = {
    legend: {
        display: false
    },
    scales: {
        xAxes: [{
                gridLines: {
                    display: false
                },
                categoryPercentage: 1.5,
            }],
        yAxes: [{
                display: false,
                gridLines: {
                    display: false,
                    drawBorder: false,
                }
            }],
    },
};
var dougnhutOption = {
    responsive: true,
    aspectRatio: 1,
    maintainAspectRatio: true,
    legend: {
        display: true,
        position: 'bottom',
        labels: {
            usePointStyle: true,
        },
        centertext: 1
    }
};
var dougnhutOption1 = {
    responsive: true,
    aspectRatio: 1,
    maintainAspectRatio: true,
    legend: {
        display: false,
        position: 'bottom',
        labels: {
            usePointStyle: true,
        }
    }
};
var AppWeeklyDashboard2ReportPage = (function () {
    function AppWeeklyDashboard2ReportPage(toastService, loadingService, http, navCtrl, navParams, storage) {
        this.toastService = toastService;
        this.loadingService = loadingService;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.baseurl = __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].appsecret;
        this.startOfWeek = __WEBPACK_IMPORTED_MODULE_8_moment__(__WEBPACK_IMPORTED_MODULE_8_moment__().startOf('week').format('YYYY/MM/DD'));
        this.AsleepBarChartArray = [];
        this.AwakeBarChartArray = [];
        this.PainBarChartArray = [];
        this.UncomfortBarChartArray = [];
        this.AwakeArray = [];
        this.AsleepArray = [];
        this.PainArray = [];
        this.UncomfortArray = [];
        this.weeklyHour = 168;
        this.data = {};
        this.tabs = [];
        this.data.tabs = [
            { page: "AppWeeklyDashboard2ReportPage", title: "Today" },
            { page: "AppDashboardPage", title: "Week" }
        ];
    }
    AppWeeklyDashboard2ReportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppWeeklyDashboard2ReportPage');
        this.SleepDoughnut(25);
        this.DistressDoughnut(30);
        this.UnomfortableDoughnut(50);
        this.ComfortableDoughnut(10);
        this.SleepDoughnut1(5);
        this.DistressDoughnut1(20);
        this.UnomfortableDoughnut1(40);
        this.ComfortableDoughnut1(50);
        this.SleepChart();
        this.DistressChart();
        this.ComfortableChart();
        this.UncomfortableChart();
    };
    AppWeeklyDashboard2ReportPage.prototype.SleepDoughnut = function (shour) {
        var data = [this.weeklyHour - shour, shour];
        console.log("dta", data);
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartRef.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Sleep'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(83,155,192)',
                            'rgb(107,177,214)'
                        ],
                    }]
            },
            options: dougnhutOption
        });
        this.chart1 = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.doughnut1.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Sleep'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(83,155,192)',
                            'rgb(107,177,214)'
                        ],
                    }]
            },
            options: dougnhutOption1
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.SleepDoughnut1 = function (shour) {
        var data = [this.weeklyHour - shour, shour];
        console.log("dta", data);
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartRef4.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Sleep'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(83,155,192)',
                            'rgb(107,177,214)'
                        ],
                    }]
            },
            options: dougnhutOption
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.DistressDoughnut = function (Dhour) {
        var data = [this.weeklyHour - Dhour, Dhour];
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartRef1.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Distress'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(226,96,96)',
                            'rgb(171,87,87)'
                        ],
                    }]
            },
            options: dougnhutOption
        });
        this.chart1 = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.doughnut2.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Distress'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(226,96,96)',
                            'rgb(171,87,87)'
                        ],
                    }]
            },
            options: dougnhutOption1
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.DistressDoughnut1 = function (Dhour) {
        var data = [this.weeklyHour - Dhour, Dhour];
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartRef5.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Distress'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(226,96,96)',
                            'rgb(171,87,87)'
                        ],
                    }]
            },
            options: dougnhutOption
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.UnomfortableDoughnut = function (Uhour) {
        var data = [this.weeklyHour - Uhour, Uhour];
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartRef2.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Awake'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(250,208,0)',
                            'rgb(201,169,13)'
                        ],
                    }]
            },
            options: dougnhutOption
        });
        this.chart1 = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.doughnut3.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Awake'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(250,208,0)',
                            'rgb(201,169,13)'
                        ],
                    }]
            },
            options: dougnhutOption1
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.UnomfortableDoughnut1 = function (Uhour) {
        var data = [this.weeklyHour - Uhour, Uhour];
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartRef6.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Awake'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(250,208,0)',
                            'rgb(201,169,13)'
                        ],
                    }]
            },
            options: dougnhutOption
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.ComfortableDoughnut = function (Chour) {
        var data = [this.weeklyHour - Chour, Chour];
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartRef3.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Daily'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(175,219,7)',
                            'rgb(151,187,12)'
                        ],
                    }]
            },
            options: dougnhutOption
        });
        this.chart1 = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.doughnut4.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Daily'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(175,219,7)',
                            'rgb(151,187,12)'
                        ],
                    }]
            },
            options: dougnhutOption1
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.ComfortableDoughnut1 = function (Chour) {
        var data = [this.weeklyHour - Chour, Chour];
        this.chart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.chartRef7.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Daily'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(175,219,7)',
                            'rgb(151,187,12)'
                        ],
                    }]
            },
            options: dougnhutOption
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.SleepChart = function () {
        this.AsleepBarChartArray = [9, 8, 7, 5, 8, 9, 6];
        this.barchart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.barchartRef.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
                datasets: [{
                        // barThickness: 16,
                        // offsetGridLines: 0,
                        barPercentage: 0.5,
                        pointBorderColor: 'red',
                        data: this.AsleepBarChartArray,
                        backgroundColor: [
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                        ],
                        borderColor: [
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                        ],
                        borderWidth: 1,
                    }, {
                        data: this.AsleepBarChartArray,
                        backgroundColor: [
                            'rgb(107,177,214)',
                        ],
                        borderColor: [
                            'rgb(107,177,214)',
                        ],
                        type: 'line',
                        fill: false
                    }
                ]
            },
            options: option
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.DistressChart = function () {
        this.PainBarChartArray = [5, 6, 4, 8, 1, 3, 9];
        this.barchart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.barchartRef1.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
                datasets: [{
                        barPercentage: 0.5,
                        data: this.PainBarChartArray,
                        backgroundColor: [
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                        ],
                        borderColor: [
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                        ],
                        borderWidth: 1
                    }, {
                        data: this.PainBarChartArray,
                        backgroundColor: [
                            'rgb(171,87,87)',
                        ],
                        borderColor: [
                            'rgb(171,87,87)',
                        ],
                        type: 'line',
                        fill: false
                    }]
            },
            options: option
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.ComfortableChart = function () {
        this.AwakeBarChartArray = [10, 8, 7, 5, 8, 9, 6];
        this.barchart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.barchartRef2.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
                datasets: [{
                        barPercentage: 0.5,
                        data: this.AwakeBarChartArray,
                        backgroundColor: [
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                        ],
                        borderColor: [
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                        ],
                        borderWidth: 1
                    },
                    {
                        data: this.AwakeBarChartArray,
                        backgroundColor: [
                            'rgb(201,169,13)',
                        ],
                        borderColor: [
                            'rgb(201,169,13)',
                        ],
                        type: 'line',
                        fill: false
                    }]
            },
            options: option
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.UncomfortableChart = function () {
        this.UncomfortBarChartArray = [2, 3, 4, 8, 5, 4, 2];
        this.barchart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.barchartRef3.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
                datasets: [{
                        barPercentage: 0.5,
                        data: this.UncomfortBarChartArray,
                        backgroundColor: [
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                        ],
                        borderColor: [
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                        ],
                        borderWidth: 1
                    },
                    {
                        data: this.UncomfortBarChartArray,
                        backgroundColor: [
                            'rgb(175,219,7)',
                        ],
                        borderColor: [
                            'rgb(175,219,7)',
                        ],
                        type: 'line',
                        fill: false
                    }
                ]
            },
            options: option
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("tabs"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["C" /* Tabs */])
    ], AppWeeklyDashboard2ReportPage.prototype, "TimelineTabs", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineChart'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineChart1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineChart2'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart2'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineChart3'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart3'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineChart4'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef4", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart4'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef4", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineChart5'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef5", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart5'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef5", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineChart6'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef6", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart6'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef6", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineChart7'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef7", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barChart7'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef7", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('doughnut1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "doughnut1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('doughnut2'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "doughnut2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('doughnut3'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "doughnut3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('doughnut4'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppWeeklyDashboard2ReportPage.prototype, "doughnut4", void 0);
    AppWeeklyDashboard2ReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-weekly-dashboard2-report',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-weekly-dashboard2-report/app-weekly-dashboard2-report.html"*/'<ion-header class="header1">\n\n  <ion-navbar>\n    <!-- <ion-title>Common Dashboard</ion-title> -->\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <ion-segment class="segment">\n    <ion-segment-button value="Today" class="button1-grey">\n      Today\n    </ion-segment-button>\n    <ion-segment-button value="Weekly" class="button1">\n      Weekly\n    </ion-segment-button>\n  </ion-segment>\n  <!-- <ion-tabs tabsPlacement=\'top\' tabs-content>\n    <ion-tab [tabTitle]="item.title" [root]="item.page" *ngFor="let item of data.tabs;let i = index"></ion-tab>\n  </ion-tabs> -->\n  <ion-title style="float:left; font-size:22px; margin:10px 0px 0px 100px;">Patients List</ion-title>\n  <!-- <ion-col col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4> -->\n  <ion-searchbar style="width:300px; margin-left:50px; float:right;margin-top:0px;" [(ngModel)]="terms" placeholder="Type to search here">\n  </ion-searchbar>\n  <!-- </ion-col> -->\n</ion-content>\n\n<ion-content padding style="margin-top:80px;">\n  <ion-row >\n    <ion-col style="display:inline-flex;">\n      <!-- <div style="width: 17%;">\n          <p style="position: relative;\n          left: 30px;font-size:16px">Status of Well Being</p>\n        </div> -->\n      <div style="font-size:16px; width:100px;">\n        <span><b>Sort By</b></span>\n        <ion-icon name="md-arrow-dropdown"></ion-icon>\n      </div>\n      <div style="display:inline-flex;margin-left: 250px;">\n        <div class="sleep"></div>\n        <p class="p1">sleep</p>\n        <div class="comfort"></div>\n        <p class="p1">Awake - comfortable</p>\n        <div class="uncomfort"></div>\n        <p class="p1">Awake - uncomfortable</p>\n        <div class="distress"></div>\n        <p class="p1">Awake - distress</p>\n      </div>\n    </ion-col>\n  </ion-row>\n  <ion-row style="background-color:#22293e !important; border:none !important; border-radius:30px;">\n    <!-- <ion-item class="item-list" no-lines style="margin-bottom: 15px; background-color:#22293e !important; border:none !important;"> -->\n    <ion-col col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 style="padding:5px 15px 5px 15px;">\n      <!-- <ion-card text-center style="height: 200px;"> -->\n      <ion-grid>\n        <ion-row>\n          <ion-col col-2>\n            <img style="border-radius: 50%;" src="assets/images/victor.png" />\n          </ion-col>\n          <ion-col col-2>\n            <h2 text-center\n              style="font-family:\'HelveticaNeue-Medium\', sans-serif; font-size:18px; font-weight:500; color:#fff; line-height: 1.22; text-align:left !important; margin-left:10px;">\n              Victor Elliot</h2>\n            <ion-label\n              style="margin:10px; color:#aab2ce !important; letter-spacing: 0.22px; font-size:10px; text-align:left !important;"\n              stacked>\n              Diagnosis Here\n            </ion-label>\n            <ion-label\n              style="margin:10px; color:#fff !important; letter-spacing: 0.31px; font-size:14px; line-height: 1.14;"\n              stacked>\n              <div style="width:18px; height:18px;background-color:#ef6d49; border-radius:50%;float:left;"></div>\n              <div style="margin-left:10px; float:left;">Status</div>\n            </ion-label>\n            <ion-label style="text-align:left; margin:5px 0 !important;">\n              <button class="button-patient" style="width:125px !important; height:28px; margin:0px !important;"\n                ion-button margin round outline medium>View More</button>\n            </ion-label>\n          </ion-col>\n          <ion-col col-2>\n            <div class="chart-container" style="height: 100px;width: 100px;">\n              <canvas #lineChart>{{ chart }}</canvas>\n            </div>\n          </ion-col>\n          <ion-col col-2>\n            <div class="chart-container" style="height: 100px;width: 100px;">\n              <canvas #lineChart1>{{ chart }}</canvas>\n            </div>\n          </ion-col>\n          <ion-col col-2>\n            <div class="chart-container" style="height: 100px;width: 100px;">\n              <canvas #lineChart2>{{ chart }}</canvas>\n            </div>\n          </ion-col>\n          <ion-col col-2>\n            <div class="chart-container" style="height: 100px;width: 100px;">\n              <canvas #lineChart3>{{ chart }}</canvas>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <!-- </ion-card> -->\n    </ion-col>\n    <!-- </ion-item> -->\n    <!-- <ion-item class="item-list" no-lines style="margin-bottom: 15px; background-color:#22293e !important; border:none !important;">\n        <div style="display: inline;margin-top: 1.6rem;">\n          <div style="margin-left: 1.3rem;float:left;font-size: 1.7rem;color: white;">\n            Name\n          </div>\n          <div style="float:right;">\n            <img src="assets/images/menu-icons/notification-inactive.png" />\n          </div>\n          <ion-item style="background-color: transparent;float:right;width:100px;">\n            <ion-toggle [checked]="true"></ion-toggle>\n          </ion-item>\n          <div style="float:right;margin-right: 13px;">\n            <img style="height: 40px;width: 40px;" src="assets/images/menu-icons/delete.png" />\n          </div>\n        </div>\n        <br>\n      </ion-item> -->\n  </ion-row>\n  <br>\n  <ion-row style="background-color:#22293e !important; border:none !important; border-radius:30px;">\n    <ion-col col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 style="padding:5px 15px 5px 15px;">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-2>\n            <img style="border-radius: 50%;" src="assets/images/man.png" />\n          </ion-col>\n          <ion-col col-2>\n            <h2 text-center\n              style="font-family:\'HelveticaNeue-Medium\', sans-serif; font-size:18px; font-weight:500; color:#fff; line-height: 1.22; text-align:left !important; margin-left:10px;">\n              Jacob Hoggman</h2>\n            <ion-label\n              style="margin:10px; color:#aab2ce !important; letter-spacing: 0.22px; font-size:10px; text-align:left !important;"\n              stacked>\n              Diagnosis Here\n            </ion-label>\n            <ion-label\n              style="margin:10px; color:#fff !important; letter-spacing: 0.31px; font-size:14px; line-height: 1.14;"\n              stacked>\n              <div style="width:18px; height:18px;background-color:#afdb07; border-radius:50%;float:left;"></div>\n              <div style="margin-left:10px; float:left;">Status</div>\n            </ion-label>\n          </ion-col>\n          <ion-col col-2>\n            <div class="chart-container" style="height: 100px;width: 100px;">\n              <canvas #lineChart4>{{ chart }}</canvas>\n            </div>\n          </ion-col>\n          <ion-col col-2>\n            <div class="chart-container" style="height: 100px;width: 100px;">\n              <canvas #lineChart5>{{ chart }}</canvas>\n            </div>\n          </ion-col>\n          <ion-col col-2>\n            <div class="chart-container" style="height: 100px;width: 100px;">\n              <canvas #lineChart6>{{ chart }}</canvas>\n            </div>\n          </ion-col>\n          <ion-col col-2>\n            <div class="chart-container" style="height: 100px;width: 100px;">\n              <canvas #lineChart7>{{ chart }}</canvas>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-col>\n    <!-- </ion-row>\n    <br>\n    <ion-row style="background-color:#22293e !important; border:none !important; border-radius:30px; height:300px;"> -->\n    <ion-col col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 style="padding: 6px 20px 0px 0px;">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-3>\n            <div class="chart-container" id="barChart0">\n              <canvas #barChart>{{ barchart }}</canvas>\n            </div>\n            <div class="rect-box">\n              <p class="copy-text">SLEEP HOURS</p>\n              <hr class="hr1">\n              <div style="display:flex;">\n                <div class="chart-container" style="height: 40px;width: 40px;margin: 10px 0px 0px -5px!important">\n                  <canvas #doughnut1>{{ chart1 }}</canvas>\n                </div>\n                <p class="time">72<sub>hrs</sub> 33<sub>min</sub></p>\n              </div>\n            </div>\n          </ion-col>\n          <ion-col col-3>\n            <div class="chart-container" id="barChart1">\n              <canvas #barChart1>{{ barchart }}</canvas>\n            </div>\n            <div class="rect-box">\n              <p class="copy-text">UNCOMFORTABLE HOURS</p>\n              <hr class="hr1">\n              <div style="display:flex;">\n                <div class="chart-container" style="height: 40px;width: 40px;margin: 10px 0px 0px -5px!important">\n                  <canvas #doughnut2>{{ chart1 }}</canvas>\n                </div>\n                <p class="time">72<sub>hrs</sub> 33<sub>min</sub></p>\n              </div>\n            </div>\n          </ion-col>\n          <ion-col col-3>\n            <div class="chart-container" id="barChart2">\n              <canvas #barChart2>{{ barchart }}</canvas>\n            </div>\n            <div class="rect-box">\n              <p class="copy-text">COMFORTABLE HOURS</p>\n              <hr class="hr1">\n              <div style="display:flex;">\n                <div class="chart-container" style="height: 40px;width: 40px;margin: 10px 0px 0px -5px!important">\n                  <canvas #doughnut3>{{ chart1 }}</canvas>\n                </div>\n                <p class="time">72<sub>hrs</sub> 33<sub>min</sub></p>\n              </div>\n            </div>\n          </ion-col>\n          <ion-col col-3>\n            <div class="chart-container" id="barChart3">\n              <canvas #barChart3>{{ barchart }}</canvas>\n            </div>\n            <div class="rect-box">\n              <p class="copy-text">ANXIOUS HOUR</p>\n              <hr class="hr1">\n              <div style="display:flex;">\n                <div class="chart-container" style="height: 40px;width: 40px;margin: 10px 0px 0px -5px!important">\n                  <canvas #doughnut4>{{ chart1 }}</canvas>\n                </div>\n                <p class="time">72<sub>hrs</sub> 33<sub>min</sub></p>\n              </div>\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-label style="text-align:center; margin:5px 0 !important;">\n          <button class="button-patient" style="width:200px !important; height:28px;" ion-button\n            margin round outline medium>View Less</button>\n        </ion-label>\n      </ion-grid>\n    </ion-col>\n  </ion-row>\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-weekly-dashboard2-report/app-weekly-dashboard2-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_6__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], AppWeeklyDashboard2ReportPage);
    return AppWeeklyDashboard2ReportPage;
}());

//# sourceMappingURL=app-weekly-dashboard2-report.js.map

/***/ }),

/***/ 966:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppWeeklyDashboard2ReportPageModule", function() { return AppWeeklyDashboard2ReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_weekly_dashboard2_report__ = __webpack_require__(1020);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppWeeklyDashboard2ReportPageModule = (function () {
    function AppWeeklyDashboard2ReportPageModule() {
    }
    AppWeeklyDashboard2ReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_weekly_dashboard2_report__["a" /* AppWeeklyDashboard2ReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_weekly_dashboard2_report__["a" /* AppWeeklyDashboard2ReportPage */]),
            ],
        })
    ], AppWeeklyDashboard2ReportPageModule);
    return AppWeeklyDashboard2ReportPageModule;
}());

//# sourceMappingURL=app-weekly-dashboard2-report.module.js.map

/***/ })

});
//# sourceMappingURL=13.js.map