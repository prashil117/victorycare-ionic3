webpackJsonp([8],{

/***/ 1023:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InfiniteScrollPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker_ngx__ = __webpack_require__(1024);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the InfiniteScrollPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InfiniteScrollPage = (function () {
    // @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
    // items: any = [];
    // const users =
    // [{
    //   "name": "Aline Grover",
    //   "created": "November 28, 2012"
    // }, {
    //   "name": "Nevada Anders",
    //   "created": "January 18, 2014"
    // }, {
    //   "name": "Nicholas Morissette",
    //   "created": "November 11, 2014"
    // }, {
    //   "name": "Rusty Umland",
    //   "created": "January 8, 2019"
    // }, {
    //   "name": "Amada Cerulli",
    //   "created": "July 22, 2009"
    // }, {
    //   "name": "Harriette Garcia",
    //   "created": "July 29, 2018"
    // }, {
    //   "name": "Erminia Marotz",
    //   "created": "September 29, 2016"
    // }, {
    //   "name": "Shanelle Parodi",
    //   "created": "May 26, 2018"
    // }, {
    //   "name": "Roger Leite",
    //   "created": "August 6, 2015"
    // }, {
    //   "name": "Latina Faulcon",
    //   "created": "February 5, 2014"
    // }, {
    //   "name": "Jerrie Hoekstra",
    //   "created": "June 2, 2016"
    // }, {
    //   "name": "Domonique Byam",
    //   "created": "December 30, 2010"
    // }, {
    //   "name": "Monnie Bonar",
    //   "created": "December 20, 2018"
    // }, {
    //   "name": "Chu Kahle",
    //   "created": "November 17, 2017"
    // }, {
    //   "name": "Allan Passman",
    //   "created": "November 12, 2015"
    // }, {
    //   "name": "Conrad Caliendo",
    //   "created": "February 10, 2016"
    // }, {
    //   "name": "Elma Chenier",
    //   "created": "August 13, 2011"
    // }, {
    //   "name": "Wendi Hirano",
    //   "created": "July 27, 2018"
    // }, {
    //   "name": "Loren Wordlaw",
    //   "created": "December 20, 2014"
    // }, {
    //   "name": "Hubert Frum",
    //   "created": "January 21, 2013"
    // }, {
    //   "name": "Rueben Basil",
    //   "created": "December 2, 2013"
    // }, {
    //   "name": "Krystyna Hardiman",
    //   "created": "February 11, 2016"
    // }, {
    //   "name": "Micki Murch",
    //   "created": "December 17, 2009"
    // }, {
    //   "name": "Allene Knight",
    //   "created": "November 3, 2010"
    // }, {
    //   "name": "Davis Lunsford",
    //   "created": "October 17, 2011"
    // }, {
    //   "name": "Elin Conte",
    //   "created": "June 23, 2015"
    // }, {
    //   "name": "Yasuko Hites",
    //   "created": "August 25, 2017"
    // }, {
    //   "name": "Gerri Pinon",
    //   "created": "May 21, 2014"
    // }, {
    //   "name": "Caryl Hawker",
    //   "created": "April 13, 2018"
    // }, {
    //   "name": "Savannah Hoard",
    //   "created": "October 31, 2009"
    // }, {
    //   "name": "Carolyn Knutsen",
    //   "created": "July 16, 2015"
    // }, {
    //   "name": "Shantay Mckissack",
    //   "created": "July 9, 2010"
    // }, {
    //   "name": "Vertie Pinales",
    //   "created": "November 20, 2010"
    // }, {
    //   "name": "Gidget Stuck",
    //   "created": "August 17, 2017"
    // }, {
    //   "name": "Drew Crownover",
    //   "created": "August 30, 2017"
    // }, {
    //   "name": "Vashti Krajewski",
    //   "created": "January 25, 2018"
    // }, {
    //   "name": "Candice Dike",
    //   "created": "November 19, 2018"
    // }, {
    //   "name": "Dorthey Buhler",
    //   "created": "October 22, 2012"
    // }, {
    //   "name": "Hailey Deluna",
    //   "created": "September 13, 2012"
    // }, {
    //   "name": "Richard Aaron",
    //   "created": "April 27, 2016"
    // }];
    function InfiniteScrollPage(navCtrl, navParams, imagePicker) {
        //   for (let i = 0; i < 10; i++) {
        //     this.items.push( this.items.length );
        //     console.log(this.items.length)
        //   }
        // }
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.imagePicker = imagePicker;
        // doInfinite(infiniteScroll) {
        //   console.log('Begin async operation');
        //   setTimeout(() => {
        //     for (let i = 0; i < 30; i++) {
        //       this.items.push( this.items.length );
        //     }
        //     console.log('Async operation has ended');
        //     infiniteScroll.complete();
        //   }, 500);
    }
    InfiniteScrollPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-infinite-scroll',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/infinite-scroll/infinite-scroll.html"*/'<!--\n  Generated template for the InfiniteScrollPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>infinite-scroll</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <!-- <ion-list>\n    <ion-item *ngFor="let i of items">{{i}}</ion-item>\n  </ion-list>\n \n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content loadingSpinner="bubbles"\n    loadingText="Loading more data..."></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content> --> \n<!-- <ion-content>\n  <ion-button (click)="toggleInfiniteScroll()" expand="block">\n    Toggle Infinite Scroll\n  </ion-button>\n\n  <ion-list></ion-list>\n\n  <ion-infinite-scroll threshold="100px" (ionInfinite)="loadData($event)">\n    <ion-infinite-scroll-content\n      loadingSpinner="bubbles"\n      loadingText="Loading more data...">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content> -->\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/infinite-scroll/infinite-scroll.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker_ngx__["a" /* ImagePicker */]])
    ], InfiniteScrollPage);
    return InfiniteScrollPage;
}());

this.imagePicker.getPictures(Option).then(function (results) {
    for (var i = 0; i < results.length; i++) {
        console.log('Image URI: ' + results[i]);
    }
}, function (err) { });
//# sourceMappingURL=infinite-scroll.js.map

/***/ }),

/***/ 1024:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export OutputType */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImagePicker; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_core__ = __webpack_require__(32);



var OutputType;
(function (OutputType) {
    OutputType[OutputType["FILE_URL"] = 0] = "FILE_URL";
    OutputType[OutputType["DATA_URL"] = 1] = "DATA_URL";
})(OutputType || (OutputType = {}));
var ImagePicker = /** @class */ (function (_super) {
    Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["c" /* __extends */])(ImagePicker, _super);
    function ImagePicker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ImagePicker.prototype.getPictures = function (options) { return Object(__WEBPACK_IMPORTED_MODULE_2__ionic_native_core__["cordova"])(this, "getPictures", { "callbackOrder": "reverse" }, arguments); };
    ImagePicker.prototype.hasReadPermission = function () { return Object(__WEBPACK_IMPORTED_MODULE_2__ionic_native_core__["cordova"])(this, "hasReadPermission", { "platforms": ["Android"] }, arguments); };
    ImagePicker.prototype.requestReadPermission = function () { return Object(__WEBPACK_IMPORTED_MODULE_2__ionic_native_core__["cordova"])(this, "requestReadPermission", { "platforms": ["Android"] }, arguments); };
    ImagePicker.pluginName = "ImagePicker";
    ImagePicker.plugin = "cordova-plugin-telerik-imagepicker";
    ImagePicker.pluginRef = "window.imagePicker";
    ImagePicker.repo = "https://github.com/Telerik-Verified-Plugins/ImagePicker";
    ImagePicker.install = "ionic cordova plugin add cordova-plugin-telerik-imagepicker --variable PHOTO_LIBRARY_USAGE_DESCRIPTION=\"your usage message\"";
    ImagePicker.installVariables = ["PHOTO_LIBRARY_USAGE_DESCRIPTION"];
    ImagePicker.platforms = ["Android", "iOS"];
    ImagePicker = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["b" /* __decorate */])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])()
    ], ImagePicker);
    return ImagePicker;
}(__WEBPACK_IMPORTED_MODULE_2__ionic_native_core__["e" /* IonicNativePlugin */]));

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvQGlvbmljLW5hdGl2ZS9wbHVnaW5zL2ltYWdlLXBpY2tlci9uZ3gvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyw4QkFBc0MsTUFBTSxvQkFBb0IsQ0FBQztBQW9EeEUsTUFBTSxDQUFOLElBQVksVUFHWDtBQUhELFdBQVksVUFBVTtJQUNwQixtREFBWSxDQUFBO0lBQ1osbURBQVEsQ0FBQTtBQUNWLENBQUMsRUFIVyxVQUFVLEtBQVYsVUFBVSxRQUdyQjs7SUF3Q2dDLCtCQUFpQjs7OztJQVVoRCxpQ0FBVyxhQUFDLE9BQTJCO0lBV3ZDLHVDQUFpQjtJQVdqQiwyQ0FBcUI7Ozs7Ozs7O0lBaENWLFdBQVc7UUFEdkIsVUFBVSxFQUFFO09BQ0EsV0FBVztzQkFoR3hCO0VBZ0dpQyxpQkFBaUI7U0FBckMsV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvcmRvdmEsIElvbmljTmF0aXZlUGx1Z2luLCBQbHVnaW4gfSBmcm9tICdAaW9uaWMtbmF0aXZlL2NvcmUnO1xuXG5leHBvcnQgaW50ZXJmYWNlIEltYWdlUGlja2VyT3B0aW9ucyB7XG4gIC8qKlxuICAgKiBtYXggaW1hZ2VzIHRvIGJlIHNlbGVjdGVkLCBkZWZhdWx0cyB0byAxNS4gSWYgdGhpcyBpcyBzZXQgdG8gMSwgdXBvbiBzZWxlY3Rpb24gb2YgYSBzaW5nbGUgaW1hZ2UsIHRoZSBwbHVnaW4gd2lsbCByZXR1cm4gaXQuIChBbmRyb2lkIG9ubHkpXG4gICAqL1xuICBtYXhpbXVtSW1hZ2VzQ291bnQ/OiBudW1iZXI7XG5cbiAgLyoqXG4gICAqIE1heCB3aWR0aCB0byBhbGxvdyBpbWFnZXMgdG8gYmVcbiAgICovXG4gIHdpZHRoPzogbnVtYmVyO1xuXG4gIC8qKlxuICAgKiBNYXggaGVpZ2h0IHRvIGFsbG93IGltYWdlcyB0byBiZVxuICAgKi9cbiAgaGVpZ2h0PzogbnVtYmVyO1xuXG4gIC8qKlxuICAgKiBRdWFsaXR5IG9mIGltYWdlcywgZGVmYXVsdHMgdG8gMTAwXG4gICAqL1xuICBxdWFsaXR5PzogbnVtYmVyO1xuXG4gIC8qKlxuICAgKiBWaWRlb3MgYWxsb3dlZD9cbiAgICovXG4gIGFsbG93X3ZpZGVvPzogYm9vbGVhbjtcblxuICAvKipcbiAgICogdGhlIGRlZmF1bHQgaXMgdGhlIG1lc3NhZ2Ugb2YgdGhlIG9sZCBwbHVnaW4gaW1wbFxuICAgKi9cbiAgdGl0bGU/OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIHRoZSBvbGQgcGx1Z2luIGltcGwgZGlkbid0IGhhdmUgaXQsIHNvIHBhc3NpbmcgbnVsbCBieSBkZWZhdWx0XG4gICAqL1xuICBtZXNzYWdlPzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBDaG9vc2UgdGhlIGZvcm1hdCBvZiB0aGUgcmV0dXJuIHZhbHVlLlxuICAgKiBEZWZpbmVkIGluIEltYWdlUGlja2VyLk91dHB1dFR5cGUuIERlZmF1bHQgaXMgRklMRV9VUkkuXG4gICAqICAgICAgRklMRV9VUkkgOiAwLCAgIFJldHVybiBpbWFnZSBmaWxlIFVSSSxcbiAgICogICAgICBEQVRBX1VSTCA6IDEsICAgUmV0dXJuIGltYWdlIGFzIGJhc2U2NC1lbmNvZGVkIHN0cmluZ1xuICAgKi9cbiAgb3V0cHV0VHlwZT86IG51bWJlcjtcblxuICAvKipcbiAgICogRGlzYWJsZSB0aGUgaU9TIHBvcG92ZXIgYXMgc2VlbiBvbiBpUGFkXG4gICAqL1xuICBkaXNhYmxlX3BvcG92ZXI/OiBib29sZWFuO1xufVxuXG5leHBvcnQgZW51bSBPdXRwdXRUeXBlIHtcbiAgRklMRV9VUkwgPSAwLFxuICBEQVRBX1VSTCxcbn1cblxuLyoqXG4gKiBAbmFtZSBJbWFnZSBQaWNrZXJcbiAqIEBkZXNjcmlwdGlvblxuICogQ29yZG92YSBQbHVnaW4gRm9yIE11bHRpcGxlIEltYWdlIFNlbGVjdGlvblxuICpcbiAqIFJlcXVpcmVzIENvcmRvdmEgcGx1Z2luOiBgY29yZG92YS1wbHVnaW4taW1hZ2UtcGlja2VyYC5cbiAqIEZvciBtb3JlIGluZm8sIHBsZWFzZSBzZWUgdGhlIGh0dHBzOi8vZ2l0aHViLmNvbS9UZWxlcmlrLVZlcmlmaWVkLVBsdWdpbnMvSW1hZ2VQaWNrZXJcbiAqXG4gKiBAdXNhZ2VcbiAqIGBgYHR5cGVzY3JpcHRcbiAqIGltcG9ydCB7IEltYWdlUGlja2VyIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9pbWFnZS1waWNrZXIvbmd4JztcbiAqXG4gKlxuICogY29uc3RydWN0b3IocHJpdmF0ZSBpbWFnZVBpY2tlcjogSW1hZ2VQaWNrZXIpIHsgfVxuICpcbiAqIC4uLlxuICpcbiAqIHRoaXMuaW1hZ2VQaWNrZXIuZ2V0UGljdHVyZXMob3B0aW9ucykudGhlbigocmVzdWx0cykgPT4ge1xuICogICBmb3IgKHZhciBpID0gMDsgaSA8IHJlc3VsdHMubGVuZ3RoOyBpKyspIHtcbiAqICAgICAgIGNvbnNvbGUubG9nKCdJbWFnZSBVUkk6ICcgKyByZXN1bHRzW2ldKTtcbiAqICAgfVxuICogfSwgKGVycikgPT4geyB9KTtcbiAqXG4gKiBgYGBcbiAqIEBpbnRlcmZhY2VzXG4gKiBJbWFnZVBpY2tlck9wdGlvbnNcbiAqL1xuQFBsdWdpbih7XG4gIHBsdWdpbk5hbWU6ICdJbWFnZVBpY2tlcicsXG4gIHBsdWdpbjogJ2NvcmRvdmEtcGx1Z2luLXRlbGVyaWstaW1hZ2VwaWNrZXInLFxuICBwbHVnaW5SZWY6ICd3aW5kb3cuaW1hZ2VQaWNrZXInLFxuICByZXBvOiAnaHR0cHM6Ly9naXRodWIuY29tL1RlbGVyaWstVmVyaWZpZWQtUGx1Z2lucy9JbWFnZVBpY2tlcicsXG4gIGluc3RhbGw6XG4gICAgJ2lvbmljIGNvcmRvdmEgcGx1Z2luIGFkZCBjb3Jkb3ZhLXBsdWdpbi10ZWxlcmlrLWltYWdlcGlja2VyIC0tdmFyaWFibGUgUEhPVE9fTElCUkFSWV9VU0FHRV9ERVNDUklQVElPTj1cInlvdXIgdXNhZ2UgbWVzc2FnZVwiJyxcbiAgaW5zdGFsbFZhcmlhYmxlczogWydQSE9UT19MSUJSQVJZX1VTQUdFX0RFU0NSSVBUSU9OJ10sXG4gIHBsYXRmb3JtczogWydBbmRyb2lkJywgJ2lPUyddLFxufSlcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBJbWFnZVBpY2tlciBleHRlbmRzIElvbmljTmF0aXZlUGx1Z2luIHtcbiAgLyoqXG4gICAqIFBpY2sgcGljdHVyZXMgZnJvbSB0aGUgbGlicmFyeS5cbiAgICogQHBhcmFtIHtJbWFnZVBpY2tlck9wdGlvbnN9IG9wdGlvbnNcbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn0gUmV0dXJucyBhIFByb21pc2UgdGhhdCByZXNvbHZlcyB0aGUgaW1hZ2UgZmlsZSBVUklcbiAgICogb3RoZXJ3aXNlIHJlamVjdHMgd2l0aCBhbiBlcnJvci5cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBjYWxsYmFja09yZGVyOiAncmV2ZXJzZScsXG4gIH0pXG4gIGdldFBpY3R1cmVzKG9wdGlvbnM6IEltYWdlUGlja2VyT3B0aW9ucyk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIHdlIGhhdmUgcGVybWlzc2lvbiB0byByZWFkIGltYWdlc1xuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxib29sZWFuPn0gUmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aXRoIGEgYm9vbGVhbiB0aGF0IGluZGljYXRlcyB3aGV0aGVyIHdlIGhhdmUgcGVybWlzc2lvblxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHBsYXRmb3JtczogWydBbmRyb2lkJ10sXG4gIH0pXG4gIGhhc1JlYWRQZXJtaXNzaW9uKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXF1ZXN0IHBlcm1pc3Npb24gdG8gcmVhZCBpbWFnZXNcbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBwbGF0Zm9ybXM6IFsnQW5kcm9pZCddLFxuICB9KVxuICByZXF1ZXN0UmVhZFBlcm1pc3Npb24oKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cbn1cbiJdfQ==

/***/ }),

/***/ 970:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfiniteScrollPageModule", function() { return InfiniteScrollPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__infinite_scroll__ = __webpack_require__(1023);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InfiniteScrollPageModule = (function () {
    function InfiniteScrollPageModule() {
    }
    InfiniteScrollPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__infinite_scroll__["a" /* InfiniteScrollPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__infinite_scroll__["a" /* InfiniteScrollPage */]),
            ],
        })
    ], InfiniteScrollPageModule);
    return InfiniteScrollPageModule;
}());

//# sourceMappingURL=infinite-scroll.module.js.map

/***/ })

});
//# sourceMappingURL=8.js.map