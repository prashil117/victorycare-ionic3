webpackJsonp([33],{

/***/ 943:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppMyNursesPageModule", function() { return AppMyNursesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_my_nurses__ = __webpack_require__(997);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppMyNursesPageModule = (function () {
    function AppMyNursesPageModule() {
    }
    AppMyNursesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_my_nurses__["a" /* AppMyNursesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_my_nurses__["a" /* AppMyNursesPage */]),
            ],
        })
    ], AppMyNursesPageModule);
    return AppMyNursesPageModule;
}());

//# sourceMappingURL=app-my-nurses.module.js.map

/***/ }),

/***/ 997:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppMyNursesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_component__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__environment_environment__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_modal_app_modal__ = __webpack_require__(570);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











/**
 * Generated class for the AppMyNursesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppMyNursesPage = (function () {
    function AppMyNursesPage(navCtrl, navParams, modalCtrl, menu, storage, alertCtrl, databaseService, loadingService, formBuilder, myApp, http, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.menu = menu;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.formBuilder = formBuilder;
        this.myApp = myApp;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.data = {};
        this.baseurl = __WEBPACK_IMPORTED_MODULE_9__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_9__environment_environment__["a" /* environment */].appsecret;
        this.eventsData = [];
        this.patientData = {};
        this.SearchData = [];
        this.nurses = [];
        this.result = [];
        this.noDelete = false;
        this.dropdown = [];
        this.selected1 = [];
        ////////// HCP DropDown Code ends /////////////
        this.SelectList = formBuilder.group({
            currentHCP: ['']
        });
    }
    AppMyNursesPage.prototype.appIntialize1 = function () {
        var _this = this;
        this.noDelete = this.userLoginData.usertype == 'patient' ? false : true;
        ////////// HCP DropDown Code Starts /////////////
        this.myApp.initializeApp();
        this.username = this.userLoginData.email;
        this.usertype = this.userLoginData.usertype;
        this.userid = this.userLoginData.id;
        this.ut = (this.usertype == 'healthcareprovider' ? false : true);
        console.log(this.userid);
        // alert(this.usertype);
        this.dropdown = this.getHCPData(this.userid, this.usertype);
        console.log(this.dropdown);
        var result = null;
        this.get('currentHCP').then(function (result) {
            console.log('Username1: ' + result);
            if (result != null) {
                _this.notselected = false;
                _this.selected1 = result;
                console.log('Username: ' + _this.selected1);
            }
            else {
                _this.notselected = true;
                console.log(_this.dropdown);
                _this.selected1 = _this.dropdown;
            }
        });
        this.getpatientsfromhcpdata();
    };
    AppMyNursesPage.prototype.ionViewDidLoad = function () {
        this.getHCPid();
        console.log('ionViewDidLoad AppMyNursesPage');
    };
    AppMyNursesPage.prototype.logout = function () {
        this.databaseService.logout();
    };
    AppMyNursesPage.prototype.getpatientsfromhcpdata = function () {
        var _this = this;
        this.loadingService.show();
        var self = this;
        this.usertype = this.userLoginData ? this.userLoginData.usertype : '';
        var webServiceData = {};
        if (this.userLoginData.usertype == "healthcareprovider" || this.userLoginData.usertype == "patient") {
            webServiceData.action = "getpatientsfromhcpdata";
        }
        else if (this.userLoginData.usertype == "nurse" || this.userLoginData.usertype == "doctor") {
            webServiceData.action = "getpatientsfromhcpanddoctornurselogin";
        }
        console.log('show data', webServiceData.action);
        self.storage.get('currentHCP').then(function (val) {
            if (val != null || val != undefined) {
                webServiceData.parentid = val;
                webServiceData.usertype = "nurse";
                webServiceData.userid = _this.usertype == 'patient' ? _this.data.id : _this.userLoginData.id;
                webServiceData.loggedinusertype = _this.usertype;
                webServiceData.appsecret = _this.appsecret;
                console.log(webServiceData);
                self.http.post(_this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
                    var result = JSON.parse(data["_body"]);
                    console.log(result);
                    if (result.status == 'success') {
                        _this.loadingService.hide();
                        console.log(result.data);
                        self.SearchData = result.data;
                        self.data.values = result.data;
                    }
                    else {
                        _this.loadingService.hide();
                        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                    }
                }, function (err) {
                    _this.loadingService.hide();
                    console.log(err);
                });
            }
            else {
                _this.loadingService.hide();
                var modal = self.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__app_modal_app_modal__["a" /* AppModalPage */], { 'title': 'Please selct HCP', 'description': 'Healthcare provider is not selected, please select healthcare provider from dropdown' }, { cssClass: "my-modal" });
                modal.present();
                // this.toastCtrl.presentToast("Healthcare provider is not selected, please select healthcare provider from dropdown");
            }
        });
    };
    AppMyNursesPage.prototype.onSelectedPatient = function (item) {
        console.log(item);
        this.navCtrl.push("AppPatientsPage", { 'data': item });
    };
    AppMyNursesPage.prototype.setFilteredPatientsData = function () {
        var _this = this;
        this.SearchData = this.data.values.filter(function (Patient) {
            console.log(Patient);
            return Patient.firstname.toLowerCase().indexOf(_this.terms.toLowerCase()) > -1;
        });
    };
    AppMyNursesPage.prototype.editnurse = function (data, editable) {
        console.log(data);
        editable = (this.userLoginData.usertype == "healthcareprovider") ? true : false;
        console.log(editable);
        this.navCtrl.push("AppNurseEditPage", { 'data': data, 'action': 'edit', 'editable': editable });
    };
    AppMyNursesPage.prototype.deletenursesData = function (item) {
        var _this = this;
        var item1 = [];
        item1 = [item];
        // var hcp:any = [];
        // let self = this;
        // hcp = this.nurses;
        // var onlyInA = hcp.filter(self.comparer(item1));
        // var onlyInB = item1.filter(self.comparer(hcp));
        // self.result = onlyInB.concat(onlyInA);
        // console.log(self.result);
        // return false;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Do you want to de-link from this Nurse?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function () {
                        console.log('Delete Ok clicked');
                        var self = _this;
                        _this.data.action = "unlinkhcpfromdoctor";
                        _this.data.appsecret = _this.appsecret;
                        if (_this.userLoginData.usertype == 'patient') {
                            _this.data.parentid = item.parentid;
                            _this.data.userid = _this.userLoginData.id;
                        }
                        else {
                            _this.data.parentid = _this.userLoginData.id;
                            _this.data.userid = item.userid;
                        }
                        console.log(_this.data);
                        _this.http.post(_this.baseurl + "removeuser.php", _this.data).subscribe(function (data) {
                            var result = JSON.parse(data["_body"]);
                            console.log(result.status);
                            if (result.status == 'success') {
                                // self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
                                self.toastCtrl.presentToast("Nurse is unlinked successfully");
                                _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                                // self.logout();
                            }
                            else {
                                self.toastCtrl.presentToast("There is an error unlinking nurse!");
                                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                            }
                        }, function (err) {
                            console.log(err);
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    AppMyNursesPage.prototype.comparer = function (otherArray) {
        console.log(otherArray);
        return function (current) {
            console.log(current);
            return otherArray.filter(function (other) {
                console.log(other);
                return other.email == current.email && other.name == current.name;
                // return other == current
            }).length == 0;
        };
    };
    AppMyNursesPage.prototype.onAddNurse = function (data, myNurse) {
        this.navCtrl.push("AppNurseEditPage", { 'nurseData': data, 'myNurse': myNurse, 'savebutton': true });
    };
    ////////// HCP DropDown Code Starts /////////////
    AppMyNursesPage.prototype.getHCPData = function (userid, usertype) {
        console.log(userid);
        console.log(usertype);
        var keys = [];
        var hcp1 = [];
        var self = this;
        var webServiceData = {};
        webServiceData.action = "gethcpdata";
        webServiceData.id = userid;
        webServiceData.usertype = usertype;
        webServiceData.appsecret = this.appsecret;
        self.data.id = this.userLoginData.id;
        self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            console.log(result);
            if (result.status == 'success') {
                console.log(result.data);
                // result.data;
                hcp1 = hcp1.push(result.data);
                // hcp1 = result.data;
                console.log(hcp1);
                // return result1;
            }
            else {
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
        }, function (err) {
            console.log(err);
        });
        return hcp1;
    };
    AppMyNursesPage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        console.log('set string in storage: ' + result);
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        console.log(reason_1);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // to get a key/value pair
    AppMyNursesPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        console.log('storageGET: ' + key + ': ' + result);
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_2 = _a.sent();
                        console.log(reason_2);
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // remove a single key value:
    AppMyNursesPage.prototype.remove = function (key) {
        this.storage.remove(key);
    };
    AppMyNursesPage.prototype.doRefresh = function (event) {
        var _this = this;
        if (event === void 0) { event = ''; }
        this.loadingService.show();
        setTimeout(function () {
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            _this.loadingService.hide();
        }, 2000);
    };
    AppMyNursesPage.prototype.selectEmployee = function ($event) {
        this.doRefresh();
        this.set('currentHCP', $event);
    };
    AppMyNursesPage.prototype.getHCPid = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('currentHCP').then(function (result) {
                            if (result != null) {
                                _this.hcpid = result;
                                _this.getuserLoginData();
                            }
                            else {
                                _this.getuserLoginData();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppMyNursesPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('userLogin').then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                if (_this.userLoginData.usertype == "healthcareprovider") {
                                    _this.hcpid = _this.userLoginData.id;
                                }
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppMyNursesPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("this.userdata", this.userLoginData);
                        return [4 /*yield*/, this.get('patientType').then(function (result) {
                                console.log("patienttypesdfdsfsdsdf", result);
                                if ((!result || result == null || result == undefined) && _this.userLoginData == "patient") {
                                    _this.doRefresh();
                                }
                                else {
                                    _this.patientType = result;
                                    _this.appIntialize1();
                                    console.log("sekeccdffvfdgdfgdfg", _this.patientType);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppMyNursesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-my-nurses',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-my-nurses/app-my-nurses.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>My Nurse List</ion-title>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <form [formGroup]="SelectList">\n      <ion-select style="color: white;" *ngIf="ut && notselected" (ionChange)="selectEmployee($event)" right>\n        <ion-label>Select Healthcare Provider</ion-label>\n        <ion-option *ngFor="let key of dropdown[0]; let idx = index;" [selected]="key.id==selected1[0].id"\n          value="{{key.id}}">{{key.firstname}}</ion-option>\n      </ion-select>\n      <ion-select style="color: white;" *ngIf="ut && !notselected" (ionChange)="selectEmployee($event)" right>\n        <ion-label>Select Healthcare Provider</ion-label>\n        <ion-option *ngFor="let key of dropdown[0]; let idx = index;" [selected]="key.id==selected1" value="{{key.id}}">\n          {{key.firstname}}</ion-option>\n      </ion-select>\n    </form>\n    <!-- <ion-buttons right (click)="logout()">\n      <button ion-button icon-only>\n        <ion-icon name="lock">Logout</ion-icon>\n      </button>\n    </ion-buttons> -->\n  </ion-navbar>\n\n</ion-header>\n\n<br>\n<ion-content padding class="fadeIn">\n  <ion-row>\n    <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n    </ion-col>\n    <ion-col col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2>\n    </ion-col>\n    <ion-col col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n      <ion-searchbar [(ngModel)]="terms" (ionInput)="setFilteredPatientsData()" placeholder="Type to search here">\n      </ion-searchbar>\n    </ion-col>\n  </ion-row>\n  <ion-grid padding *ngIf="data != null">\n    <ion-row>\n      <ion-col *ngIf="noDelete" col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 style="padding:5px 15px 5px 15px;">\n        <ion-card text-center style="height: 200px;">\n          <ion-grid>\n            <ion-row>\n              <ion-col col-3>\n              </ion-col>\n              <ion-col (click)="onAddNurse(SearchData, \'myNurse\')">\n                <img style="height: 140px;width: 150px; margin:0px auto;" src="assets/images/others/add-patients.png" />\n              </ion-col>\n              <ion-col col-3>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                Add New Nurse\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-card>\n      </ion-col>\n      <ion-col col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 *ngFor="let item of SearchData;let i = index;"\n        style="padding:5px 15px 5px 15px;">\n        <ion-card text-center style="height: 200px;">\n          <ion-grid>\n            <ion-row>\n              <ion-col col-4>\n                <img *ngIf="item.image" style="border-radius: 50%;height: 64px;width: 64px;" src="{{item.image}}" />\n                <img *ngIf="!item.image" style="border-radius: 50%;height: 64px;width: 64px;"\n                  src="assets/images/nopreview.jpeg" />\n\n              </ion-col>\n              <ion-col col-8>\n                <img (click)="editnurse(item, false)" class="card-top-icon" style="margin-left:5px;"\n                  src="assets/images/others/info-icon.png" />\n                <img (click)="deletenursesData(item)" class="card-top-icon"\n                  src="assets/images/others/delete-icon.png" />\n                <h2 style="font-size:16px; color:#fff;" text-center>{{item.firstname.substring(0,24)}}</h2>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-label stacked>\n                  Email:\n                  <span style="color: #e0675c;">*</span>\n                </ion-label>\n                <p style="font-size:12px; color:#fff;" text-center>{{item.email}}</p>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngIf="noDelete">\n              <ion-col>\n                <button class="button-patient" ion-button margin round outline medium style=""\n                  (click)="onSelectedPatient(item, \'\', \'nurses\', false)">View Patients</button>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf="SearchData?.length == 0">\n      <ion-col>\n        <div class="app-description" style="text-align: center;">\n          No Data Found ..!!\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-my-nurses/app-my-nurses.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* MenuController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_6__app_app_component__["a" /* MyApp */], __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_8__services_toast_service__["a" /* ToastService */]])
    ], AppMyNursesPage);
    return AppMyNursesPage;
}());

//# sourceMappingURL=app-my-nurses.js.map

/***/ })

});
//# sourceMappingURL=33.js.map