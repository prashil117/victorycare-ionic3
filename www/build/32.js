webpackJsonp([32],{

/***/ 945:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppMyPatientsPageModule", function() { return AppMyPatientsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_my_patients__ = __webpack_require__(999);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppMyPatientsPageModule = (function () {
    function AppMyPatientsPageModule() {
    }
    AppMyPatientsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_my_patients__["a" /* AppMyPatientsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_my_patients__["a" /* AppMyPatientsPage */]),
            ],
        })
    ], AppMyPatientsPageModule);
    return AppMyPatientsPageModule;
}());

//# sourceMappingURL=app-my-patients.module.js.map

/***/ }),

/***/ 999:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppMyPatientsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_component__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__environment_environment__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










/**
 * Generated class for the AppMyPatientsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppMyPatientsPage = (function () {
    function AppMyPatientsPage(navCtrl, navParams, menu, storage, events, alertCtrl, databaseService, loadingService, formBuilder, myApp, http, toastCtrl) {
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.storage = storage;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.formBuilder = formBuilder;
        this.myApp = myApp;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.baseurl = __WEBPACK_IMPORTED_MODULE_9__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_9__environment_environment__["a" /* environment */].appsecret;
        this.data = {};
        this.eventsData = [];
        this.patientData = {};
        this.SearchData = [];
        this.doctorEmail = [];
        this.editOn = false;
        this.result = [];
        this.editable = false;
        this.actionLogo = "";
        this.dropdown = [];
        this.selected1 = [];
        console.log('app patients');
        this.SelectList = formBuilder.group({
            currentHCP: ['']
        });
        this.doctorEmail = navParams.data.data;
        ////////// HCP DropDown Code ends /////////////
    }
    AppMyPatientsPage.prototype.intializeApp1 = function () {
        var _this = this;
        this.editable = (this.userLoginData.usertype == 'healthcareprovider' ? true : false);
        this.actionLogo = (this.userLoginData.usertype == 'healthcareprovider' ? "edit-icon.png" : "info-icon.png");
        console.log(this.actionLogo);
        ////////// HCP DropDown Code Starts /////////////
        this.myApp.initializeApp();
        this.username = this.userLoginData.email;
        this.usertype = this.userLoginData.usertype;
        this.userid = this.userLoginData.id;
        this.ut = (this.usertype == 'healthcareprovider' ? false : true);
        console.log(this.userid);
        // alert(this.usertype);
        this.dropdown = this.getHCPData(this.userid, this.usertype);
        console.log(this.dropdown);
        var result = null;
        this.get('currentHCP').then(function (result) {
            console.log('Username1: ' + result);
            if (result != null) {
                _this.notselected = false;
                _this.selected1 = result;
                console.log('Username: ' + _this.selected1);
            }
            else {
                _this.notselected = true;
                console.log(_this.dropdown);
                _this.selected1 = _this.dropdown;
            }
        });
        this.ionViewWillEnter();
    };
    AppMyPatientsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppMyPatientsPage');
        this.getuserLoginData();
    };
    AppMyPatientsPage.prototype.logout = function () {
        this.databaseService.logout();
    };
    AppMyPatientsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.loadingService.show();
        var self = this;
        this.usertype = this.userLoginData ? this.userLoginData.usertype : '';
        var webServiceData = {};
        this.editOn = true;
        if (this.usertype == 'healthcareprovider') {
            webServiceData.action = "getpatientsfromhcplogin";
            webServiceData.usertype = "patient";
            webServiceData.parentid = this.userLoginData.id;
            webServiceData.appsecret = this.appsecret;
            console.log(webServiceData);
            self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
                var result = JSON.parse(data["_body"]);
                console.log(result);
                if (result.status == 'success') {
                    _this.loadingService.hide();
                    console.log(result.data);
                    self.SearchData = result.data;
                    // self.SearchData.firstname = (result.data.firstname == null ? 'noname' : result.data.firstname);
                    self.data.values = result.data;
                }
                else {
                    _this.loadingService.hide();
                    _this.toastCtrl.presentToast("No data found");
                    // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                }
            }, function (err) {
                _this.loadingService.hide();
                console.log(err);
            });
        }
        else {
            webServiceData.action = "getpatientfromdoctornurse";
            self.storage.get('currentHCP').then(function (val) {
                webServiceData.parentid = val;
                webServiceData.usertype = "patient";
                webServiceData.userid = _this.userLoginData.id;
                webServiceData.appsecret = _this.appsecret;
                console.log(webServiceData);
                self.http.post(_this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
                    var result = JSON.parse(data["_body"]);
                    console.log(result);
                    if (result.status == 'success') {
                        _this.loadingService.hide();
                        console.log(result.data);
                        self.SearchData = result.data;
                        self.data.values = result.data;
                    }
                    else {
                        _this.loadingService.hide();
                        _this.toastCtrl.presentToast("No data found");
                        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                    }
                }, function (err) {
                    _this.loadingService.hide();
                    console.log(err);
                });
            });
        }
    };
    AppMyPatientsPage.prototype.comparer = function (otherArray) {
        return function (current) {
            return otherArray.filter(function (other) {
                // console.log(other);
                return other.email != current.email; //&& other.name == current.name
                // return other == current
            }).length == 0;
        };
    };
    AppMyPatientsPage.prototype.comparer2 = function (otherArray) {
        return function (current) {
            return otherArray.filter(function (other) {
                // console.log(other);
                return other.email == current.email; //&& other.name == current.name
                // return other == current
            }).length == 0;
        };
    };
    AppMyPatientsPage.prototype.ngAfterViewInit = function () {
    };
    AppMyPatientsPage.prototype.onSelectedPatient = function (item, action) {
        if (action === void 0) { action = ""; }
        this.storage.set('user', item);
        console.log(item);
        // this.events.publish('user:created', item);
        this.navCtrl.setRoot("AppTimelinePage", { 'data': item, 'action': action });
    };
    AppMyPatientsPage.prototype.onAddPatient = function (data, email, userData) {
        console.log(data);
        console.log(email);
        console.log(userData);
        this.navCtrl.push("AppPatientProfilePage", { 'patientData': data, 'email': email, 'userData': userData });
    };
    AppMyPatientsPage.prototype.viewPatient = function (patient, editable) {
        console.log(editable);
        this.navCtrl.push("AppMyPatientProfilePage", { 'data': patient, 'action': 'edit', 'editable': editable });
    };
    AppMyPatientsPage.prototype.RedirectToContacts = function (item) {
        this.navCtrl.push("AppPatientsContactsPage", { 'data': item });
    };
    AppMyPatientsPage.prototype.RedirectToMedicineMasterList = function (item) {
        this.navCtrl.push("AppMedicineMasterListPage", { 'data': item });
    };
    AppMyPatientsPage.prototype.RedirectToActivityMasterList = function (item) {
        this.navCtrl.push("AppActivityMasterListPage", { 'data': item });
    };
    AppMyPatientsPage.prototype.RedirectToEventMasterList = function (item) {
        this.navCtrl.push("AppEventMasterListPage", { 'data': item });
    };
    AppMyPatientsPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    AppMyPatientsPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    AppMyPatientsPage.prototype.setFilteredPatientsData = function () {
        var _this = this;
        this.SearchData = this.data.values.filter(function (Patient) {
            return Patient.firstname.toLowerCase().indexOf(_this.terms.toLowerCase()) > -1;
        });
    };
    AppMyPatientsPage.prototype.deletePatientData = function (item) {
        var _this = this;
        var item1 = [];
        item1 = [item];
        console.log(item);
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Do you want to de-link this Patient?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function () {
                        var self = _this;
                        var value = [];
                        var keys = [];
                        var result = [];
                        var currentData = [{ id: self.userLoginData.id, email: self.userLoginData.email, name: self.userLoginData.firstname }];
                        console.log('Delete Ok clicked');
                        _this.data.action = "unlinkhcpfromdoctor";
                        _this.data.parentid = _this.userLoginData.id;
                        _this.data.userid = item.userid;
                        _this.data.appsecret = _this.appsecret;
                        console.log(_this.data);
                        _this.http.post(_this.baseurl + "removeuser.php", _this.data).subscribe(function (data) {
                            var result = JSON.parse(data["_body"]);
                            console.log(result.status);
                            if (result.status == 'success') {
                                // self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
                                self.toastCtrl.presentToast("Patient is unlinked successfully");
                                _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                                // self.logout();
                            }
                            else {
                                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                            }
                        }, function (err) {
                            console.log(err);
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    ////////// HCP DropDown Code Starts /////////////
    AppMyPatientsPage.prototype.getHCPData = function (userid, usertype) {
        var hcp1 = [];
        var self = this;
        var webServiceData = {};
        webServiceData.action = "gethcpdata";
        webServiceData.id = userid;
        webServiceData.usertype = usertype;
        webServiceData.appsecret = this.appsecret;
        // self.data.id = this._cookieService.get('userid');
        self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            ;
            if (result.status == 'success') {
                // result.data;
                hcp1 = hcp1.push(result.data);
                // hcp1 = result.data;
                console.log(hcp1);
                // return result1;
            }
            else {
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
        }, function (err) {
            console.log(err);
        });
        return hcp1;
    };
    AppMyPatientsPage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        console.log('set string in storage: ' + result);
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        console.log(reason_1);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // to get a key/value pair
    AppMyPatientsPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        console.log('storageGET: ' + key + ': ' + result);
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_2 = _a.sent();
                        console.log(reason_2);
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppMyPatientsPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('userLogin').then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppMyPatientsPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("this.userdata", this.userLoginData);
                        return [4 /*yield*/, this.get('patientType').then(function (result) {
                                console.log("patienttypesdfdsfsdsdf", result);
                                if ((!result || result == null || result == undefined) && _this.userLoginData == "patient") {
                                    _this.doRefresh();
                                }
                                else {
                                    _this.patientType = result;
                                    _this.intializeApp1();
                                    console.log("sekeccdffvfdgdfgdfg", _this.patientType);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // remove a single key value:
    AppMyPatientsPage.prototype.remove = function (key) {
        this.storage.remove(key);
    };
    AppMyPatientsPage.prototype.doRefresh = function (event) {
        var _this = this;
        if (event === void 0) { event = ''; }
        this.loadingService.show();
        setTimeout(function () {
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            _this.loadingService.hide();
        }, 2000);
    };
    AppMyPatientsPage.prototype.selectEmployee = function ($event) {
        this.doRefresh();
        this.set('currentHCP', $event);
    };
    AppMyPatientsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-my-patients',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-my-patients/app-my-patients.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title text-center>My Patients</ion-title>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <form [formGroup]="SelectList">\n            <ion-select style="color: white;" *ngIf="ut && notselected" (ionChange)="selectEmployee($event)" right>\n                <ion-label>Select Healthcare Provider</ion-label>\n                <ion-option *ngFor="let key of dropdown[0]; let idx = index;" [selected]="key.id==selected1[0].id"\n                    value="{{key.id}}">{{key.firstname}}</ion-option>\n            </ion-select>\n            <ion-select style="color: white;" *ngIf="ut && !notselected" (ionChange)="selectEmployee($event)" right>\n                <ion-label>Select Healthcare Provider</ion-label>\n                <ion-option *ngFor="let key of dropdown[0]; let idx = index;" [selected]="key.id==selected1"\n                    value="{{key.id}}">{{key.firstname}}</ion-option>\n            </ion-select>\n        </form>\n        <!-- <ion-buttons right (click)="logout()">\n            <button ion-button icon-only>\n                <ion-icon name="lock">Logout</ion-icon>\n            </button>\n        </ion-buttons> -->\n    </ion-navbar>\n</ion-header>\n<ion-content padding class="fadeIn">\n    <ion-row>\n        <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n        </ion-col>\n        <ion-col col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2>\n        </ion-col>\n        <ion-col col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n            <ion-searchbar [(ngModel)]="terms" (ionInput)="setFilteredPatientsData()" placeholder="Type to search here">\n            </ion-searchbar>\n        </ion-col>\n    </ion-row>\n    <ion-grid padding *ngIf="data != null">\n        <ion-row>\n            <ion-col col-12 col-sm-6 col-md-4 col-lg-4 col-xl-6 style="padding:5px 15px 5px 15px;">\n                <ion-card text-center style="height: 200px;">\n                    <ion-grid>\n                        <ion-row>\n                            <ion-col col-2>\n                            </ion-col>\n                            <ion-col (click)="onAddPatient(SearchData, doctorEmail, data.values)">\n                                <img style="height: 130px;width: 150px;" src="assets/images/others/add-patients.png" />\n                            </ion-col>\n                            <ion-col col-2>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col col-2>\n                            </ion-col>\n                            <ion-col>\n                                Add New Patient\n                            </ion-col>\n                            <ion-col col-2>\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </ion-card>\n            </ion-col>\n            <ion-col col-12 col-sm-6 col-md-4 col-lg-4 col-xl-6 *ngFor="let item of SearchData;let i = index;"\n                style="padding:5px 15px 5px 15px;">\n                <ion-card text-center style="height: 200px;">\n                    <ion-grid>\n                        <ion-row>\n                            <ion-col col-4>\n                                <img *ngIf="item.image" style="border-radius: 50%;height: 64px;width: 64px;"\n                                    src="{{item.image}}" />\n                                <img *ngIf="!item.image" style="border-radius: 50%;height: 64px;width: 64px;"\n                                    src="assets/images/nopreview.jpeg" />\n                            </ion-col>\n                            <ion-col col-8>\n                                <img *ngIf="editOn" (click)="viewPatient(item, editable)" class="card-top-icon"\n                                    style="margin-left:5px;" src="assets/images/others/{{actionLogo}}" />\n                                <img (click)="deletePatientData(item)" class="card-top-icon"\n                                    src="assets/images/others/delete-icon.png" />\n                                <h2 style="font-size:16px; color:#fff;" text-center>{{item.firstname}}</h2>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col>\n                                <ion-label style="margin:0" stacked>\n                                    Email:\n                                    <span style="color: #e0675c;">*</span>\n                                </ion-label>\n                                <p style="font-size:12px; color:#fff;" text-center>{{item.email}}</p>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col>\n                                <button class="button-patient" ion-button margin round outline medium style=""\n                                    (click)="onSelectedPatient(item, true)">View</button>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row *ngIf="!SearchData.length> 0">\n                            <ion-col>\n                                <div class="app-description" style="text-align: center;">\n                                    No Data Found ..!!\n                                </div>\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </ion-card>\n            </ion-col>\n        </ion-row>\n\n    </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-my-patients/app-my-patients.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* MenuController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_6__app_app_component__["a" /* MyApp */], __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_8__services_toast_service__["a" /* ToastService */]])
    ], AppMyPatientsPage);
    return AppMyPatientsPage;
}());

//# sourceMappingURL=app-my-patients.js.map

/***/ })

});
//# sourceMappingURL=32.js.map