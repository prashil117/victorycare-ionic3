webpackJsonp([46],{

/***/ 927:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppDashboard2PageModule", function() { return AppDashboard2PageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_dashboard2__ = __webpack_require__(981);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppDashboard2PageModule = (function () {
    function AppDashboard2PageModule() {
    }
    AppDashboard2PageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_dashboard2__["a" /* AppDashboard2Page */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_dashboard2__["a" /* AppDashboard2Page */]),
            ],
        })
    ], AppDashboard2PageModule);
    return AppDashboard2PageModule;
}());

//# sourceMappingURL=app-dashboard2.module.js.map

/***/ }),

/***/ 981:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppDashboard2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AppDashboard2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppDashboard2Page = (function () {
    function AppDashboard2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data1 = {};
        this.patientData = {};
        this.tabs1 = [];
        this.eventData = [];
        this.tabs1 = [
            { page: "AppWeeklyDashboard2ReportPage", title: "Today" },
            { page: "AppWeeklyDashboard2ReportPage", title: "Week" }
        ];
    }
    AppDashboard2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppDashboard2Page');
        this.navCtrl.popToRoot();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("tabs"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["C" /* Tabs */])
    ], AppDashboard2Page.prototype, "TimelineTabs", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["x" /* Navbar */])
    ], AppDashboard2Page.prototype, "navBar", void 0);
    AppDashboard2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-dashboard2',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-dashboard2/app-dashboard2.html"*/'<ion-header class="header1">\n\n  <ion-navbar>\n    <!-- <ion-title>Common Dashboard</ion-title> -->\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-tabs tabsPlacement=\'top\' tabs-content>\n    <ion-tab  class="width: 400px;" [tabTitle]="item.title" [root]="item.page" *ngFor="let item of tabs1 ;let i = index"></ion-tab>\n  </ion-tabs>\n  <ion-title style="float:left; font-size:22px; margin:-14px 0px 0px 450px;">Patients List</ion-title>\n  <!-- <ion-col col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4> -->\n  <ion-searchbar style="width:300px; margin-left:50px; float:right;margin-top:-23px;" [(ngModel)]="terms" placeholder="Type to search here">\n  </ion-searchbar>\n  <!-- </ion-col> -->\n</ion-content>\n'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-dashboard2/app-dashboard2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */]])
    ], AppDashboard2Page);
    return AppDashboard2Page;
}());

//# sourceMappingURL=app-dashboard2.js.map

/***/ })

});
//# sourceMappingURL=46.js.map