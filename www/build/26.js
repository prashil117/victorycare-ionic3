webpackJsonp([26],{

/***/ 1006:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPatientMedicineListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_medicine_master_list_modal_app_medicine_master_list_modal__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__environment_environment__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_toast_service__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









// import {AppAddContactModalPage} from "../app-add-contact-modal/app-add-contact-modal";
var AppPatientMedicineListPage = (function () {
    function AppPatientMedicineListPage(storage, toastCtrl, http, navCtrl, navParams, app, alertCtrl, databaseService, loadingService, modalCtrl) {
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.navCtrl = navCtrl;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.modalCtrl = modalCtrl;
        this.data = {};
        this.medicines = [];
        this.patientData = {};
        this.baseurl = __WEBPACK_IMPORTED_MODULE_7__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_7__environment_environment__["a" /* environment */].appsecret;
        this.isloadmore = false;
        this.offset = 0;
        this.tmparr = [];
    }
    AppPatientMedicineListPage.prototype.addMedicineListItem = function () {
        var _this = this;
        // console.log('patientDatalist',this.patientData);
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__app_medicine_master_list_modal_app_medicine_master_list_modal__["a" /* AppMedicineMasterListModalPage */], {
            "type": "add",
            "MedicineData": this.medicines,
            "patientData": this.patientData,
            "onboard": true,
        }, { cssClass: "on-addmedicine" });
        // let modal = this.modalCtrl.create(AppMedicineMasterListModalPage, { 'MedicineData': this.medicines, 'patientData': this.patientData });
        modal.present();
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.MedicineData = [];
                _this.tmparr = [];
                _this.loadMedicineListSettings();
            }
        });
    };
    AppPatientMedicineListPage.prototype.doRefresh = function () {
        var _this = this;
        this.loadingService.show();
        setTimeout(function () {
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            _this.loadingService.hide();
        }, 100);
    };
    AppPatientMedicineListPage.prototype.loadMedicineListSettings = function (infiniteScroll) {
        var _this = this;
        this.data.action = "getmedicinemasterdata";
        this.data.userid = this.userLoginData.id;
        this.data.offset = 0;
        this.data.keyword = "";
        this.data.appsecret = this.appsecret;
        console.log("this.data", this.data);
        this.http.post(this.baseurl + "getdata.php", this.data).subscribe(function (data) {
            _this.loadingService.show();
            var result = JSON.parse(data["_body"]);
            console.log("result", result);
            if (infiniteScroll) {
                infiniteScroll.complete();
            }
            if (result.status == "notmatchingkey") {
                _this.doRefresh();
            }
            if (result.status == "success") {
                _this.loadingService.hide();
                if (result.data0 !== null) {
                    for (var i in result.data0) {
                        _this.tmparr.push(result.data0[i]);
                    }
                    _this.MedicineData = _this.tmparr;
                    if (result.data0) {
                        _this.count = result.data0[0].count;
                    }
                    else {
                        _this.count = 0;
                    }
                    console.log("MedicenData", _this.MedicineData);
                }
                else {
                    _this.loadingService.hide();
                    _this.data = [];
                    _this.data.keys = [];
                    _this.MedicineData = [];
                }
            }
            else {
                _this.loadingService.hide();
                // this.loadingService.hide();
                // this.toastCtrl.presentToast("There is a no data available.");
                // this.navCtrl.setRoot("AppDashboardPage");
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
        }, function (err) {
            console.log(err);
            _this.loadingService.hide();
        });
    };
    AppPatientMedicineListPage.prototype.loadMore = function (infiniteScroll) {
        if (this.MedicineData.length >= this.count || this.count == 0) {
            infiniteScroll.enable(false);
        }
        else {
            this.offset = this.offset + 10;
            this.loadMedicineListSettings(infiniteScroll);
        }
    };
    AppPatientMedicineListPage.prototype.MedicineUpdate = function (item, index) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__app_medicine_master_list_modal_app_medicine_master_list_modal__["a" /* AppMedicineMasterListModalPage */], {
            "MedicineData": item,
            "patientData": this.patientData,
            "index": index,
            "type": "edit",
            "onboard": true,
        }, { cssClass: "on-addmedicine " });
        modal.present();
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.MedicineData = [];
                _this.tmparr = [];
                _this.loadMedicineListSettings();
            }
        });
    };
    AppPatientMedicineListPage.prototype.tabNextButton = function () {
        this.navCtrl.parent.select(2);
    };
    AppPatientMedicineListPage.prototype.deleteMedicineData = function (item, index) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Confirm Delete",
            message: "Do you want to delete this record?",
            buttons: [{
                    text: "Cancel",
                    role: "cancel",
                    handler: function () {
                    },
                }, {
                    text: "Delete",
                    handler: function () {
                        var self = _this;
                        _this.data.action = "deletemedicine";
                        _this.data.id = item.id;
                        _this.data.appsecret = _this.appsecret;
                        _this.http.post(_this.baseurl + "deletedata.php", _this.data).subscribe(function (data) {
                            var result = JSON.parse(data["_body"]);
                            if (result.status == "success") {
                                self.toastCtrl.presentToast("Medicine is deleted successfully");
                                _this.MedicineData = [];
                                _this.tmparr = [];
                                _this.loadMedicineListSettings();
                            }
                            else {
                                self.toastCtrl.presentToast("There is an error deleting data!");
                            }
                        }, function (err) {
                            console.log(err);
                        });
                    },
                }],
        });
        alert.present();
    };
    AppPatientMedicineListPage.prototype.GotoHome = function () {
        var _this = this;
        this.loadingService.show();
        this.data.action = "setloginfirsttime";
        this.data.userid = this.userLoginData.id;
        this.data.appsecret = this.appsecret;
        console.log("data", this.data);
        this.http.post(this.baseurl + "login.php", this.data).subscribe(function (data) {
            _this.loadingService.show();
            var result = JSON.parse(data["_body"]);
            if (result.status == "success") {
                _this.loadingService.hide();
                _this.app.getRootNav().setRoot("AppTimelinePage");
                console.log("result", result);
            }
            else {
                _this.loadingService.hide();
                // this.showToast("Something went wrong");
                // this.toastCtrl.presentToast("Something went wrong, Please try again.");
            }
        }, function (err) {
            _this.loadingService.hide();
            console.log(err);
        });
    };
    AppPatientMedicineListPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_1 = _a.sent();
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppPatientMedicineListPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("userLogin").then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.loadMedicineListSettings();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPatientMedicineListPage.prototype.ionViewDidLoad = function () {
        this.getuserLoginData();
    };
    AppPatientMedicineListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "app-patient-medicine-list",template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patient-medicine-list/app-patient-medicine-list.html"*/'<ion-content>\n\n   <!-- <h2 text-center padding>Medicines</h2> -->\n\n   <ion-grid padding>\n      <ion-row wrap>\n         <ion-col col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 offset-2 offset-sm-2 offset-md-2 offset-lg-2 offset-xl-2>\n            <!-- <ion-item no-lines style="background-color: #fff" *ngFor="let item of medicines;let i = index" (click)="editMedicine(item, i)" style="margin-bottom: 15px;padding:10px"> -->\n            <ion-item class="item-list" no-lines *ngFor="let item of MedicineData;let i = index">\n               <div style="display: inline;margin-top: 1.6rem;">\n                  <div [ngStyle]="{\'background\':item.color}"\n                     style="float:left;color:#000;font-weight: bold;border-radius: 50%;width:25px;height: 25px;border: 2px solid black;margin-top: 22px;">\n                  </div>\n                  <div style="margin-left: 1.3rem;float:left;font-size: 1.7rem;margin-top: 25px;">{{item.name}}</div>\n                  <div style="float:right;margin-left: 7px;margin-top: 10px;" (click)="MedicineUpdate(item, item.key)">\n                     <img style="height: 54px;width: 60px;" src="assets/images/menu-icons/edit.png">\n                  </div>\n                  <div style="float:right;margin-top: 16px;" (click)="deleteMedicineData(item, item.key)">\n                     <img style="height: 40px;width: 40px;" src="assets/images/menu-icons/delete.png">\n                  </div>\n               </div>\n               <br>\n            </ion-item>\n            <div style="margin-top: 70px;" text-center class="margin-17t" *ngIf="medicines.length === 0">\n               Enter the medicines you use regularly here.\n               You can use the built-in notification feature to help you remember when to take your medicine as\n               prescribed by your doctor.<br> Feel free to add more now or later in the settings menu.\n            </div>\n            <br>\n            <div text-center class="margin-4t">\n               <img (click)="addMedicineListItem()" class="button-margin" src="assets/app-images/addBtn.png" />\n            </div>\n            <!-- <br>\n            <div text-right class="block-insert" margin>\n               <button class="dark-button" ion-button round>Next</button>\n            </div> -->\n         </ion-col>\n\n      </ion-row>\n   </ion-grid>\n   <ion-infinite-scroll (ionInfinite)="loadMore($event)" loadingSpinner="bubbles" loadingText="Loading Medicine..">\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n   </ion-infinite-scroll>\n</ion-content>\n<div style="z-index: 9999;top: 82%;position: fixed;right: 0px;" text-right class="block-insert" margin>\n   <button class="dark-button" (click)="tabNextButton()" ion-button round>Next</button>\n   <ion-label (click)="GotoHome()" style="color: #81889f !important;font-size: 14px;padding-top: 10px;">\n      Or you can fill it later <span style="color: #69c2d1 !important;">SKIP</span>\n   </ion-label>\n</div>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patient-medicine-list/app-patient-medicine-list.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_8__services_toast_service__["a" /* ToastService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */],
            __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */]])
    ], AppPatientMedicineListPage);
    return AppPatientMedicineListPage;
}());

//# sourceMappingURL=app-patient-medicine-list.js.map

/***/ }),

/***/ 952:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppPatientMedicineListModule", function() { return AppPatientMedicineListModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_patient_medicine_list__ = __webpack_require__(1006);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppPatientMedicineListModule = (function () {
    function AppPatientMedicineListModule() {
    }
    AppPatientMedicineListModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_patient_medicine_list__["a" /* AppPatientMedicineListPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_patient_medicine_list__["a" /* AppPatientMedicineListPage */])
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]],
        })
    ], AppPatientMedicineListModule);
    return AppPatientMedicineListModule;
}());

//# sourceMappingURL=app-patient-medicine-list.module.js.map

/***/ })

});
//# sourceMappingURL=26.js.map