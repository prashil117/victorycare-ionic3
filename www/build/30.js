webpackJsonp([30],{

/***/ 1002:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppNursesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__environment_environment__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the AppNursesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppNursesPage = (function () {
    function AppNursesPage(navCtrl, navParams, menu, storage, alertCtrl, databaseService, loadingService, toastCtrl, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.data = {};
        this.baseurl = __WEBPACK_IMPORTED_MODULE_7__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_7__environment_environment__["a" /* environment */].appsecret;
        this.eventsData = [];
        this.patientData = {};
        this.SearchData1 = [];
        this.usertype = "";
        this.result = [];
    }
    AppNursesPage.prototype.getUsersbyStoredEmail = function () {
        var varNur = [];
        var test = this.storage.get('currentHCP').then(function (val) {
            varNur.push(val);
        });
        console.log(varNur);
        return varNur;
    };
    AppNursesPage.prototype.ionViewDidLoad = function () {
        // this.title = this._cookieService.get('username');
        console.log('ionViewDidLoad AppNursesPage');
        this.getuserLoginData();
    };
    AppNursesPage.prototype.logout = function () {
        this.databaseService.logout();
    };
    AppNursesPage.prototype.onSelectedPatient = function (item, hcpLoggedIn, userTypeToBeAdded) {
        console.log(item);
        this.navCtrl.push("AppPatientsPage", { 'data': item, 'hcpLoggedIn': hcpLoggedIn, 'userTypeToBeAdded': userTypeToBeAdded });
    };
    AppNursesPage.prototype.getusersdetails = function () {
        var _this = this;
        var self = this;
        this.usertype = this.userLoginData.usertype ? this.userLoginData.usertype : '';
        var webServiceData = {};
        if (this.userLoginData.usertype == 'healthcareprovider') {
            webServiceData.action = "getpatientsfromhcplogin";
            webServiceData.usertype = "nurse";
            webServiceData.parentid = this.userLoginData.id;
            webServiceData.appsecret = this.appsecret;
            console.log(webServiceData);
            self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
                _this.loadingService.show();
                var result = JSON.parse(data["_body"]);
                console.log(result);
                if (result.status == 'success') {
                    _this.loadingService.hide();
                    console.log(result.data);
                    self.SearchData1 = result.data;
                    self.data.values = result.data;
                }
                else {
                    _this.loadingService.hide();
                    // this.toastCtrl.presentToast("No data found");
                    // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                }
            }, function (err) {
                _this.loadingService.hide();
                console.log(err);
            });
        }
        else {
            webServiceData.action = "getpatientsfromhcpdata";
            self.storage.get('currentHCP').then(function (val) {
                webServiceData.parentid = val;
                webServiceData.usertype = "nurse";
                webServiceData.userid = _this.userLoginData.id;
                webServiceData.appsecret = _this.appsecret;
                console.log(webServiceData);
                self.http.post(_this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
                    _this.loadingService.show();
                    var result = JSON.parse(data["_body"]);
                    console.log(result);
                    if (result.status == 'success') {
                        _this.loadingService.hide();
                        console.log(result.data);
                        self.SearchData1 = result.data;
                        self.data.values = result.data;
                    }
                    else {
                        _this.loadingService.hide();
                        // this.toastCtrl.presentToast("No data found");
                        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                    }
                }, function (err) {
                    _this.loadingService.hide();
                    console.log(err);
                });
            });
        }
    };
    AppNursesPage.prototype.setFilteredPatientsData = function () {
        var _this = this;
        if (this.data.values != undefined) {
            this.SearchData1 = this.data.values.filter(function (Patient) {
                return Patient.firstname.toLowerCase().indexOf(_this.terms.toLowerCase()) > -1;
            });
        }
        else {
            this.toastCtrl.presentToast("There is no data found...");
        }
    };
    AppNursesPage.prototype.onSelectedDoctor = function (item) {
        // this.storage.set('user', item);
        console.log(item);
        var self = this;
        if (item !== null) {
            self.eventsData = item;
            this.navCtrl.setRoot("AppTimelinePage", { 'data': item });
        }
        else {
            self.eventsData = item;
            this.navCtrl.setRoot("AppTimelinePage", { 'data': item });
        }
    };
    AppNursesPage.prototype.editNurse = function (data, editable) {
        console.log(data);
        editable = (this.userLoginData.usertype == "healthcareprovider") ? true : false;
        console.log(editable);
        this.navCtrl.push("AppNurseEditPage", { 'data': data, 'action': 'edit', 'editable': editable, 'savebutton': editable });
    };
    AppNursesPage.prototype.deleteNurseData = function (item) {
        var _this = this;
        console.log(item);
        var item1 = [];
        item1 = [item];
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Do you want to de-link this record?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function () {
                        var self = _this;
                        console.log('Delete Ok clicked');
                        _this.data.action = "unlinkhcpfromdoctor";
                        _this.data.parentid = _this.userLoginData.id;
                        _this.data.userid = item.userid;
                        _this.data.appsecret = _this.appsecret;
                        console.log(_this.data);
                        _this.http.post(_this.baseurl + "removeuser.php", _this.data).subscribe(function (data) {
                            _this.loadingService.show();
                            var result = JSON.parse(data["_body"]);
                            console.log(result.status);
                            if (result.status == 'success') {
                                _this.loadingService.hide();
                                // self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
                                self.toastCtrl.presentToast("Nurse is unlinked successfully");
                                _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                                // self.logout();
                            }
                            else {
                                _this.loadingService.hide();
                                self.toastCtrl.presentToast("There is an error unlinking Nurse!");
                                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                            }
                        }, function (err) {
                            _this.loadingService.hide();
                            console.log(err);
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    AppNursesPage.prototype.comparer = function (otherArray) {
        return function (current) {
            return otherArray.filter(function (other) {
                return other.email == current.email; // && other.name == current.name
                // return other == current
            }).length == 0;
        };
    };
    AppNursesPage.prototype.onAddNurse = function (data, editable) {
        editable = (this.userLoginData.usertype == 'healthcareprovider') ? true : false;
        this.navCtrl.push("AppNurseEditPage", { 'nurseData': data, 'action': 'edit', 'editable': editable, 'savebutton': editable });
    };
    // set a key/value
    AppNursesPage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        console.log('set string in storage: ' + result);
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        console.log(reason_1);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // to get a key/value pair
    AppNursesPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        console.log('storageGET: ' + key + ': ' + result);
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_2 = _a.sent();
                        console.log(reason_2);
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppNursesPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('userLogin').then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppNursesPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("this.userdata", this.userLoginData);
                        return [4 /*yield*/, this.get('patientType').then(function (result) {
                                console.log("patienttypesdfdsfsdsdf", result);
                                if ((!result || result == null || result == undefined) && _this.userLoginData == "patient") {
                                    _this.doRefresh('');
                                }
                                else {
                                    _this.patientType = result;
                                    _this.getusersdetails();
                                    console.log("sekeccdffvfdgdfgdfg", _this.patientType);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppNursesPage.prototype.doRefresh = function (event) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            // event.target.complete();
        }, 2000);
    };
    // remove a single key value:
    AppNursesPage.prototype.remove = function (key) {
        this.storage.remove(key);
    };
    AppNursesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-nurses',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-nurses/app-nurses.html"*/'\n<ion-header>\n\n    <ion-navbar>\n        <ion-title>Nurses List</ion-title>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <!-- <ion-buttons right (click)="logout()">\n            <button ion-button icon-only>\n                <ion-icon name="lock">Logout</ion-icon>\n            </button>\n        </ion-buttons> -->\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding class="fadeIn">\n    <ion-row>\n        <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n        </ion-col>\n        <ion-col col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2>\n        </ion-col>\n        <ion-col col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n            <ion-searchbar [(ngModel)]="terms" (ionInput)="setFilteredPatientsData()" placeholder="Type to search here">\n            </ion-searchbar>\n        </ion-col>\n    </ion-row>\n    <ion-grid padding *ngIf="data != null">\n        <ion-row>\n            <ion-col col-12 col-sm-6 col-md-4 col-lg-4 col-xl-6 style="padding:5px 15px 5px 15px;">\n                <ion-card text-center style="height: 200px;">\n                    <ion-grid>\n                        <ion-row>\n                            <ion-col col-2>\n                            </ion-col>\n                            <ion-col (click)="onAddNurse(SearchData1, true)">\n                                <img style="height: 140px;width: 150px; margin:0px auto;"\n                                    src="assets/images/others/add-patients.png" />\n                            </ion-col>\n                            <ion-col col-2>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col col-2>\n                            </ion-col>\n                            <ion-col>\n                                Add New Nurse\n                            </ion-col>\n                            <ion-col col-2>\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </ion-card>\n            </ion-col>\n\n            <ion-col col-12 col-sm-6 col-md-4 col-lg-4 col-xl-6 *ngFor="let item of SearchData1;let i = index;"\n                style="padding:5px 15px 5px 15px;">\n                <ion-card text-center style="height: 200px;">\n                    <ion-grid>\n                        <ion-row>\n                            <ion-col col-4>\n                                <img *ngIf="item.image" style="border-radius: 50%;height: 64px;width: 64px;"\n                                    src="{{item.image}}" />\n                                <img *ngIf="!item.image" style="border-radius: 50%;height: 64px;width:64px;"\n                                    src="assets/images/nopreview.jpeg" />\n                            </ion-col>\n                            <ion-col col-8>\n                                <img (click)="editNurse(item, false)" class="card-top-icon" style="margin-left:5px;"\n                                    src="assets/images/others/edit-icon.png" />\n                                <img (click)="deleteNurseData(item)" class="card-top-icon"\n                                    src="assets/images/others/delete-icon.png" />\n                                <h2 style="font-size:16px; color:#fff;" text-center>{{item.firstname}}</h2>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col>\n                                <ion-label stacked>\n                                    Email:\n                                    <span style="color: #e0675c;">*</span>\n                                </ion-label>\n                                <p style="font-size:12px; color:#fff;" text-center>{{item.email}}</p>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col>\n                                <button class="button-patient" ion-button margin round outline medium style=""\n                                    (click)="onSelectedPatient(item, hcpLoggedIn, \'nurses\')">View Patients</button>\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </ion-card>\n            </ion-col>\n            <ion-grid padding *ngIf="SearchData1.length == 0 || SearchData1.length == undefined">\n                <ion-row>\n                    <ion-col col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 style="padding:5px 15px 5px 15px;">\n                        <ion-title>No data found...</ion-title>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n        </ion-row>\n    </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-nurses/app-nurses.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* MenuController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_5__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_6__angular_http__["a" /* Http */]])
    ], AppNursesPage);
    return AppNursesPage;
}());

//# sourceMappingURL=app-nurses.js.map

/***/ }),

/***/ 948:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppNursesPageModule", function() { return AppNursesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_nurses__ = __webpack_require__(1002);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppNursesPageModule = (function () {
    function AppNursesPageModule() {
    }
    AppNursesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_nurses__["a" /* AppNursesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_nurses__["a" /* AppNursesPage */]),
            ],
        })
    ], AppNursesPageModule);
    return AppNursesPageModule;
}());

//# sourceMappingURL=app-nurses.module.js.map

/***/ })

});
//# sourceMappingURL=30.js.map