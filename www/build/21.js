webpackJsonp([21],{

/***/ 1012:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPatientsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__environment_environment__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var AppPatientsPage = (function () {
    // currentData:any = [];
    function AppPatientsPage(navCtrl, navParams, menu, storage, events, alertCtrl, databaseService, loadingService, toastCtrl, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.storage = storage;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.data = {};
        this.baseurl = __WEBPACK_IMPORTED_MODULE_7__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_7__environment_environment__["a" /* environment */].appsecret;
        this.SearchData1 = [];
        this.eventsData = [];
        this.patientData = {};
        this.SearchData = [];
        this.doctorEmail = [];
        this.noDelete = false;
        this.result = [];
        this.patients = [];
    }
    AppPatientsPage.prototype.intializeApp1 = function () {
        console.log('app patients');
        this.doctorEmail = this.navParams.data ? this.navParams.data.data : this.navParams.data.doctornurse;
        this.doctorid = this.navParams.data ? this.navParams.data.doctorid : '';
        this.noDelete = (this.userLoginData.usertype == 'healthcareprovider') ? true : this.navParams.data.noDelete;
        this.userTypeToBeAdded = this.navParams.data.userTypeToBeAdded;
        console.log(this.doctorEmail);
        console.log(this.userTypeToBeAdded);
        this.getusersdetails(this.userTypeToBeAdded, this.doctorEmail.userid);
    };
    AppPatientsPage.prototype.getusersdetails = function (userTypeToBeAdded, userid) {
        var _this = this;
        this.loadingService.show();
        var self = this;
        this.usertype = this.userLoginData.usertype ? this.userLoginData.usertype : '';
        var webServiceData = {};
        if (this.usertype == 'healthcareprovider') {
            webServiceData.action = "getpatientfromdoctornurse";
            webServiceData.usertype = 'patient';
            webServiceData.userid = userid;
            webServiceData.parentid = this.userLoginData.id;
            webServiceData.appsecret = this.appsecret;
            console.log(webServiceData);
            self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
                var result = JSON.parse(data["_body"]);
                console.log(result);
                if (result.status == 'success') {
                    _this.loadingService.hide();
                    console.log(result.data);
                    self.SearchData1 = result.data;
                    console.log("this.searchdataatasfsd", self.SearchData1);
                    self.data.values = result.data;
                }
                else {
                    _this.loadingService.hide();
                    // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                }
            }, function (err) {
                _this.loadingService.hide();
                console.log(err);
            });
        }
        else {
            webServiceData.action = "getpatientfromdoctornurse";
            self.storage.get('currentHCP').then(function (val) {
                webServiceData.parentid = val;
                // webServiceData.usertype = "nurse";
                // webServiceData.userid = this._cookieService.get('userid');
                webServiceData.usertype = "patient";
                webServiceData.userid = userid;
                webServiceData.appsecret = _this.appsecret;
                console.log(webServiceData);
                self.http.post(_this.baseurl + "getuser.php", webServiceData).subscribe(function (data) {
                    var result = JSON.parse(data["_body"]);
                    console.log(result);
                    if (result.status == 'success') {
                        _this.loadingService.hide();
                        console.log(result.data);
                        self.SearchData1 = result.data;
                        console.log("this.searchdataatasfsd", self.SearchData1);
                        self.data.values = result.data;
                    }
                    else {
                        _this.loadingService.hide();
                        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                    }
                }, function (err) {
                    _this.loadingService.hide();
                    console.log(err);
                });
            });
        }
    };
    AppPatientsPage.prototype.ngAfterViewInit = function () {
        this.getuserLoginData();
    };
    AppPatientsPage.prototype.onSelectedPatient = function (item) {
        this.storage.set('user', item);
        // this.events.publish('user:created', item);
        var self = this;
    };
    AppPatientsPage.prototype.onAddPatient = function (data, doctorsdata, addExisting, userTypeToBeAdded) {
        console.log(data);
        // this.navCtrl.push("AppPatientProfilePage",{'patientData':data});
        // this.navCtrl.push("AppPatientProfilePage");
        this.navCtrl.setRoot("AppPatientProfilePage", { 'patientData': data, 'doctorsdata': doctorsdata, 'addExisting': addExisting, 'userTypeToBeAdded': userTypeToBeAdded });
        console.log("adddpaitentexisitng", addExisting);
    };
    AppPatientsPage.prototype.viewPatient = function (patient) {
        this.navCtrl.push("AppMyPatientProfilePage", { 'data': patient, 'action': 'edit' });
    };
    AppPatientsPage.prototype.logout = function () {
        this.databaseService.logout();
    };
    AppPatientsPage.prototype.RedirectToContacts = function (item) {
        this.navCtrl.push("AppPatientsContactsPage", { 'data': item });
    };
    AppPatientsPage.prototype.RedirectToMedicineMasterList = function (item) {
        this.navCtrl.push("AppMedicineMasterListPage", { 'data': item });
    };
    AppPatientsPage.prototype.RedirectToActivityMasterList = function (item) {
        this.navCtrl.push("AppActivityMasterListPage", { 'data': item });
    };
    AppPatientsPage.prototype.RedirectToEventMasterList = function (item) {
        this.navCtrl.push("AppEventMasterListPage", { 'data': item });
    };
    AppPatientsPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    AppPatientsPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    AppPatientsPage.prototype.setFilteredPatientsData = function () {
        var _this = this;
        this.SearchData1 = this.data.values.filter(function (Patient) {
            return Patient.firstname.toLowerCase().indexOf(_this.terms.toLowerCase()) > -1;
        });
    };
    AppPatientsPage.prototype.deletePatientData = function (item, userid) {
        var _this = this;
        console.log(item);
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Do you want to delete this record?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: function () {
                        console.log('Delete Ok clicked');
                        _this.data.action = "unlinkhcpfromdoctor";
                        _this.data.parentid = userid;
                        _this.data.userid = item.userid;
                        _this.data.appsecret = _this.appsecret;
                        console.log(_this.data);
                        _this.http.post(_this.baseurl + "removeuser.php", _this.data).subscribe(function (data) {
                            var result = JSON.parse(data["_body"]);
                            console.log(result.status);
                            if (result.status == 'success') {
                                // self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
                                _this.toastCtrl.presentToast("Patient is unlinked successfully");
                                // this.navCtrl.setRoot(this.navCtrl.getActive().component);
                                _this.navCtrl.setRoot("AppDoctorsPage");
                                // self.logout();
                            }
                            else {
                                _this.toastCtrl.presentToast("There is an error unlinking doctor!");
                                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                            }
                        }, function (err) {
                            console.log(err);
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    AppPatientsPage.prototype.comparer2 = function (otherArray) {
        return function (current) {
            return otherArray.filter(function (other) {
                // console.log(other);
                return other.email == current.email; //&& other.name == current.name
                // return other == current
            }).length == 0;
        };
    };
    AppPatientsPage.prototype.comparer = function (otherArray) {
        return function (current) {
            // console.log(current);
            return otherArray.filter(function (other) {
                // console.log(other);
                return other.email != current.email;
                // return other == current
            }).length == 0;
        };
    };
    AppPatientsPage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        console.log('storageGET: ' + key + ': ' + result);
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_1 = _a.sent();
                        console.log(reason_1);
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppPatientsPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('userLogin').then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPatientsPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("this.userdata", this.userLoginData);
                        return [4 /*yield*/, this.get('patientType').then(function (result) {
                                console.log("patienttypesdfdsfsdsdf", result);
                                if ((!result || result == null || result == undefined) && _this.userLoginData == "patient") {
                                    _this.doRefresh('');
                                }
                                else {
                                    _this.patientType = result;
                                    _this.intializeApp1();
                                    console.log("sekeccdffvfdgdfgdfg", _this.patientType);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPatientsPage.prototype.doRefresh = function (event) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            // event.target.complete();
        }, 2000);
    };
    AppPatientsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-patients',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patients/app-patients.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title text-center>Select Patient</ion-title>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <!-- <ion-buttons right (click)="logout()">\n            <button ion-button icon-only>\n                <ion-icon name="lock">Logout</ion-icon>\n            </button>\n        </ion-buttons> -->\n    </ion-navbar>\n</ion-header>\n<ion-content padding class="fadeIn">\n    <ion-row>\n        <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n        </ion-col>\n        <ion-col col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2>\n        </ion-col>\n        <ion-col col-12 col-sm-12 col-md-4 col-lg-6 col-xl-4>\n            <ion-searchbar [(ngModel)]="terms" (ionInput)="setFilteredPatientsData()" placeholder="Type to search here">\n            </ion-searchbar>\n        </ion-col>\n    </ion-row>\n    <ion-grid padding *ngIf="data != null">\n        <ion-row>\n            <ion-col *ngIf="(superAdminLoggedIn === false) || (noDelete === true)" col-12 col-sm-6 col-md-4 col-lg-4\n                col-xl-6 style="padding:5px 15px 5px 15px;">\n                <ion-card text-center style="height: 200px;">\n                    <ion-grid>\n                        <ion-row>\n                            <ion-col col-3>\n                            </ion-col>\n                            <ion-col (click)="onAddPatient(SearchData1, doctorEmail, addExisting, userTypeToBeAdded)">\n                                <img style="height: 140px;width: 150px;" src="assets/images/others/add-patients.png" />\n                            </ion-col>\n                            <ion-col col-3>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col col-2>\n                            </ion-col>\n                            <ion-col>\n                                Add New Patient\n                            </ion-col>\n                            <ion-col col-2>\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </ion-card>\n            </ion-col>\n            <ion-col col-12 col-sm-6 col-md-4 col-lg-4 col-xl-6 *ngFor="let item of SearchData1;let i = index;"\n                style="padding:5px 15px 5px 15px;">\n                <ion-card text-center style="height: 200px;">\n                    <ion-grid>\n                        <ion-row>\n                            <ion-col col-4>\n                                <img *ngIf="item.image" style="border-radius: 50%;height: 64px;width: 64px;"\n                                    src="{{item.image}}" />\n                                <img *ngIf="!item.image" style="border-radius: 50%;height: 64px;width: 64px;"\n                                    src="assets/images/nopreview.jpeg" />\n                            </ion-col>\n                            <ion-col col-8 *ngIf="noDelete">\n                                <img (click)="viewPatient(item)" class="card-top-icon" style="margin-left:5px;"\n                                    src="assets/images/others/edit-icon.png" />\n                                <img (click)="deletePatientData(item, doctorEmail.userid)" class="card-top-icon"\n                                    src="assets/images/others/delete-icon.png" />\n                                <h2 text-center>{{item.firstname.substring(0,24)}}\n                                </h2>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col>\n                                <ion-label style="margin:10px" stacked>\n                                    Email:\n                                    <span style="color: #e0675c;">*</span>\n                                </ion-label>\n                                <p style="font-size:12px; color:#fff;" text-center>{{item.email}}</p>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col>\n                                <button class="button-patient" ion-button margin round outline medium style=""\n                                    (click)="onSelectedPatient(item)">View</button>\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </ion-card>\n            </ion-col>\n        </ion-row>\n        <ion-row *ngIf="SearchData1?.length == 0">\n            <ion-col>\n                <div class="app-description" style="text-align: center;">\n                    No Data Found ..!!\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n\n<!--Fab Button-->\n<!-- <ion-fab #fab bottom right>\n    <button button-ion-fab ion-fab (click)="onAddPatient()">\n        <ion-icon name="add"></ion-icon>\n    </button>\n</ion-fab> -->'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patients/app-patients.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* MenuController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_5__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_6__angular_http__["a" /* Http */]])
    ], AppPatientsPage);
    return AppPatientsPage;
}());

//# sourceMappingURL=app-patients.js.map

/***/ }),

/***/ 958:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppPatientsModule", function() { return AppPatientsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_patients__ = __webpack_require__(1012);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppPatientsModule = (function () {
    function AppPatientsModule() {
    }
    AppPatientsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_patients__["a" /* AppPatientsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_patients__["a" /* AppPatientsPage */]),
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], AppPatientsModule);
    return AppPatientsModule;
}());

//# sourceMappingURL=app-patients.module.js.map

/***/ })

});
//# sourceMappingURL=21.js.map