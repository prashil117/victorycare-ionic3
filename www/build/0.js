webpackJsonp([0],{

/***/ 1007:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPatientProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_database_service__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loading_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_geocoder__ = __webpack_require__(975);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_directive_email_validator__ = __webpack_require__(974);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_add_contact_modal_app_add_contact_modal__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_storage__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_toast_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__environment_environment__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};














var AppPatientProfilePage = (function () {
    function AppPatientProfilePage(navCtrl, navParams, storage, formBuilder, nativeGeocoder, modalCtrl, alertCtrl, databaseService, loadingService, camera, http, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.nativeGeocoder = nativeGeocoder;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.databaseService = databaseService;
        this.loadingService = loadingService;
        this.camera = camera;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.newDoctors = "";
        this.data = {};
        this.diagnosiss = {};
        this.upload = {};
        this.baseurl = __WEBPACK_IMPORTED_MODULE_13__environment_environment__["a" /* environment */].apiUrl;
        this.appsecret = __WEBPACK_IMPORTED_MODULE_13__environment_environment__["a" /* environment */].appsecret;
        this.logo = {};
        this.action = "";
        this.image = "";
        this.data1 = {};
        this.data2 = {};
        this.data3 = "";
        this.data4 = {};
        this.selectType = "";
        this.readonly = {};
        this.result = [];
        this.enableDropdown = "";
        this.enableDiv = "";
        this.userData = [];
        this.date = new Date();
        this.patientsData = [];
        this.SearchData = [];
        console.log('patient profile');
        this.upload = "assets/images/upload1.svg";
        this.readonly = false;
        // this.usertype = this._cookieService.get('usertype');
        this.Profileform = this.formBuilder.group({
            usertype: [''],
            selectType: [''],
            storedDoctors: [''],
            // name: ['', Validators.required],
            firstname: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
            lastname: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].maxLength(15), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].minLength(3), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].pattern('[a-zA-Z ]*'), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
            email: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_8__app_directive_email_validator__["a" /* EmailValidator */].isValid])],
            birthDate: [''],
            mobile: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
            sex: [''],
            toppings: [''],
            languagePreferred: [''],
            password: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required],
            confirmPassword: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required],
            address: [''],
            contacts: this.formBuilder.array([
                this.initContact(),
            ]),
            diagnosis: this.formBuilder.array([
                this.initDiagnosis(),
            ])
        }, { validator: this.matchingPasswords('password', 'confirmPassword') });
    }
    AppPatientProfilePage.prototype.intializeApp1 = function () {
        this.ut = (this.userLoginData.usertype == 'healthcareprovider') ? true : false;
        if (this.ut == true) {
            this.Profileform = this.formBuilder.group({
                usertype: [''],
                selectType: [''],
                storedDoctors: [''],
                // name: ['', Validators.required],
                firstname: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
                lastname: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].maxLength(15), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].minLength(3), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].pattern('[a-zA-Z ]*'), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
                email: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_8__app_directive_email_validator__["a" /* EmailValidator */].isValid])],
                birthDate: [''],
                mobile: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
                sex: [''],
                toppings: [''],
                languagePreferred: [''],
                password: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required],
                confirmPassword: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required],
                address: [''],
                contacts: this.formBuilder.array([
                    this.initContact(),
                ]),
                diagnosis: this.formBuilder.array([
                    this.initDiagnosis(),
                ])
            }, { validator: this.matchingPasswords('password', 'confirmPassword') });
        }
        else {
            this.Profileform = this.formBuilder.group({
                usertype: [''],
                selectType: [''],
                storedDoctors: [''],
                // name: ['', Validators.required],
                firstname: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
                lastname: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].maxLength(15), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].minLength(3), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].pattern('[a-zA-Z ]*'), __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
                email: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_8__app_directive_email_validator__["a" /* EmailValidator */].isValid])],
                birthDate: [''],
                mobile: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["Validators"].required])],
                sex: [''],
                toppings: [''],
                languagePreferred: [''],
                address: [''],
                password: [''],
                confirmPassword: [''],
                contacts: this.formBuilder.array([
                    this.initContact(),
                ]),
                diagnosis: this.formBuilder.array([
                    this.initDiagnosis(),
                ])
            });
        }
        this.data.usertype = (this.data.usertype == undefined ? "patient" : this.data.usertype);
        this.data.address = (this.data.address == undefined ? "" : this.data.address);
        this.data.birthDate = (this.data.birthDate == undefined ? "" : this.data.birthDate);
        this.data.mobile = (this.data.mobile == undefined ? "" : this.data.mobile);
        this.data.sex = (this.data.sex == undefined ? "" : this.data.sex);
        this.data.image = (this.data.image == undefined ? "" : this.data.image);
        this.data.languagePreferred = (this.data.languagePreferred == undefined ? "" : this.data.languagePreferred);
        this.enableDiv = false;
        console.log("asdfghjklqwertyuiopzxcvbnm,", this.navParams.data);
        this.data3 = this.navParams.data.doctorsdata;
        this.patientsData = this.navParams.data.patientData;
        this.userData = this.navParams.data.userData;
        this.addExisting = this.navParams.data.addExisting;
        this.userTypeToBeAdded = this.navParams.data.userTypeToBeAdded;
        if (this.userTypeToBeAdded == undefined) {
            this.userTypeToBeAdded = this.userLoginData.usertype;
        }
        this.hcpLoggedIn = (this.userTypeToBeAdded == 'healthcareprovider' ? true : false);
        console.log(this.navParams.data);
        if (!__WEBPACK_IMPORTED_MODULE_6_lodash__["isEmpty"](this.navParams.data.data)) {
            this.data = this.navParams.data.data;
            if (!__WEBPACK_IMPORTED_MODULE_6_lodash__["isEmpty"](this.data)) {
                // this.data = navParams.data.data;
                this.action = this.navParams.data.action;
                if (this.action == 'edit') {
                    console.log('contacts', this.data.diagnosis);
                    this.data.address = (this.data.address == undefined ? "" : this.data.address);
                    console.log(this.data);
                    if (this.data.diagnosis !== undefined) {
                        for (var i = 0; i < this.data.diagnosis.length; i++) {
                            var control = this.Profileform.controls['diagnosis'];
                            control.push(this.initDiagnosis());
                        }
                    }
                    else {
                        this.data.diagnosis = [];
                    }
                    if (this.data.contacts !== undefined) {
                        for (var i = 0; i < this.data.contacts.length; i++) {
                            var control = this.Profileform.controls['contacts'];
                            control.push(this.initContact());
                        }
                    }
                    else {
                        this.data.contacts = [];
                    }
                }
            }
        }
        this.getDoctors(this.patientsData);
    };
    AppPatientProfilePage.prototype.matchingPasswords = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirmPassword = group.controls[confirmPasswordKey];
            if (password.value !== confirmPassword.value) {
                return {
                    confirmPassword: true
                };
            }
        };
    };
    AppPatientProfilePage.prototype.getHCPid = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('currentHCP').then(function (result) {
                            if (result != null) {
                                _this.hcpid = result;
                                _this.getuserLoginData();
                            }
                            else {
                                _this.getuserLoginData();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPatientProfilePage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('userLogin').then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                if (_this.userLoginData.usertype == "healthcareprovider") {
                                    _this.hcpid = _this.userLoginData.id;
                                    console.log("this.hcpid else", _this.hcpid);
                                }
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPatientProfilePage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('patientType').then(function (result) {
                            console.log("patienttypesdfdsfsdsdf", result);
                            if ((!result || result == null || result == undefined) && _this.userLoginData == "patient") {
                                _this.doRefresh('');
                            }
                            else {
                                _this.patientType = result;
                                _this.intializeApp1();
                                // console.log("sekeccdffvfdgdfgdfg", this.patientType)
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppPatientProfilePage.prototype.doRefresh = function (event) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            // event.target.complete();
        }, 2000);
    };
    AppPatientProfilePage.prototype.getDoctors = function (patientsData) {
        var _this = this;
        this.loadingService.show();
        // console.log("patintDoctors", patientsData);
        // console.log("doctonursedata", this.navParams.data);
        // console.log(patientsData);
        var keys = [];
        var result1 = [];
        var self = this;
        self.data.action = "getdropdownpatientdata";
        self.data.id = this.userLoginData.id;
        self.data.parentid = this.hcpid ? this.hcpid : this.userLoginData.id;
        self.data.getdropdownpatientdata = patientsData;
        self.data.appsecret = this.appsecret;
        // console.log(patientsData);
        self.http.post(this.baseurl + "getdropdowndata.php", self.data).subscribe(function (data) {
            var result = JSON.parse(data["_body"]);
            console.log(result);
            if (result.status == 'success') {
                _this.loadingService.hide();
                console.log("dssfsfsdfdfsdaabcdefghjijklmno", result.data);
                // result.data;
                result1 = result1.push(result.data); //[result.data];
                _this.data4.newDoctors = result.data;
                console.log(result1);
                // return result1;
            }
            else {
                _this.loadingService.hide();
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
        }, function (err) {
            _this.loadingService.hide();
            console.log(err);
        });
        // console.log(result1);
        // this.get('allPatientsData').then((val) => {
        //   console.log(val);
        //   result.push(val);
        // });
        // this.remove('allPatientsData');
        return result1;
    };
    AppPatientProfilePage.prototype.comparer = function (otherArray) {
        return function (current) {
            // console.log(current);
            return otherArray.filter(function (other) {
                // console.log(other);
                return other.email == current.email;
                // return other == current
            }).length == 0;
        };
    };
    AppPatientProfilePage.prototype.checkState = function (email) {
        var _this = this;
        console.log(email);
        this.loadingService.show();
        this.data.action = "getuserbyid";
        this.data.id = email;
        this.data.appsecret = this.appsecret;
        console.log(this.data);
        this.http.post(this.baseurl + "login.php", this.data).subscribe(function (snapshot) {
            var result = JSON.parse(snapshot["_body"]);
            // var snapshot = snapshot.val();
            if (result.status == 'success') {
                _this.data = result.data;
                // this.data.name = result.data.firstname+' '+result.data.lastname;
                _this.enableDropdown = false;
                _this.enableDiv = true;
                _this.loadingService.hide();
            }
            else {
                _this.loadingService.hide();
                _this.toastCtrl.presentToast("Unable to fetch data");
                // self.errorMessage = "Unable to fetch data";
            }
        });
    };
    AppPatientProfilePage.prototype.addType = function ($event) {
        this.loadingService.show();
        console.log($event);
        if ($event == 'new') {
            this.doRefresh($event);
            // this.enableDropdown = "display:none !important;";
            // this.enableDiv = "display:block !important;";
            this.enableDropdown = false;
            this.enableDiv = false;
            this.selectType = 'addnew';
            this.data.selectType = false;
            this.loadingService.hide();
        }
        else if ($event == 'exist') {
            // this.doRefresh($event);
            this.enableDropdown = true;
            this.enableDiv = true;
            this.selectType = 'addexist';
            this.data.storedDoctors = false;
            this.loadingService.hide();
        }
    };
    AppPatientProfilePage.prototype.setInitialDiagnosis = function () {
        if (this.data.diagnosis === undefined) {
            this.data.diagnosis = [];
        }
        if (this.data.diagnosis.length === 0) {
            this.data.diagnosis.push({});
        }
    };
    AppPatientProfilePage.prototype.addDiagnosis = function () {
        console.log('diagnosis', this.data.diagnosis);
        var control = this.Profileform.controls['diagnosis'];
        control.push(this.initDiagnosis());
        this.data.diagnosis.push({ name: '' });
    };
    AppPatientProfilePage.prototype.logout = function () {
        this.databaseService.logout();
    };
    AppPatientProfilePage.prototype.initDiagnosis = function () {
        return this.formBuilder.group({
            name: ['']
        });
    };
    AppPatientProfilePage.prototype.initContact = function () {
        return this.formBuilder.group({
            name: [''],
            phone: ['']
        });
    };
    AppPatientProfilePage.prototype.deleteDiagnosis = function (index) {
        this.data.diagnosis.splice(index, 1);
    };
    AppPatientProfilePage.prototype.addContact = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__app_add_contact_modal_app_add_contact_modal__["a" /* AppAddContactModalPage */], { 'contacts': this.data.contacts });
        modal.present();
        modal.onDidDismiss(function (data) {
            _this.getLetLong();
        });
        // this.data.contacts.push({});
    };
    AppPatientProfilePage.prototype.deleteContact = function (index) {
        this.data.contacts.splice(index, 1);
    };
    AppPatientProfilePage.prototype.ngAfterViewInit = function () {
        this.setInitialDiagnosis();
        if (this.data.contacts === undefined) {
            this.data.contacts = [];
        }
        if (!__WEBPACK_IMPORTED_MODULE_6_lodash__["isEmpty"](this.action)) {
            this.showImagePreview();
            if (this.action === "view") {
                this.readonly = true;
            }
        }
        this.getHCPid();
    };
    AppPatientProfilePage.prototype.getLetLong = function () {
        var options = {
            useLocale: true,
            maxResults: 5
        };
        var control = this.Profileform.controls['contacts'];
        control.push(this.initContact());
        var self = this;
        console.log('contacts', self.data.contacts);
        var _loop_1 = function (i) {
            self.nativeGeocoder.forwardGeocode(self.data.contacts[i].address, options)
                .then(function (coordinates) {
                self.data.contacts[i].lat = coordinates[0].latitude;
                self.data.contacts[i].long = coordinates[0].longitude;
            }).catch(function (error) { return console.log(error); });
        };
        for (var i = 0; i < self.data.contacts.length; i++) {
            _loop_1(i);
        }
    };
    AppPatientProfilePage.prototype.addPatientDetails = function (thirdPartyData) {
        var _this = this;
        console.log(this.data);
        this.data.base_url = this.baseurl;
        var self = this;
        this.submitAttempt = true;
        if (!this.Profileform.valid) {
            console.log("Profileform Validation Fire!");
        }
        else {
            // console.log('this.data',this.data);
            this.Profileform.value.id = this.data.id;
            if (this.data.image != undefined) {
                this.data.image = this.data.image;
            }
            else {
                this.showImagePreview();
                this.data.image = "";
            }
            if (this.action === 'edit') {
            }
            else {
                var hcp = [];
                var doctors = [];
                console.log(this.userTypeToBeAdded);
                // self.loadingService.show();
                // self.data.action = (this._cookieService.get('usertype') == 'healthcareprovider') ? "insert" : ""
                if (this.userLoginData.usertype == 'healthcareprovider' && !this.navParams.data.doctorsdata) {
                    self.data.action = "insert";
                    self.data.url = "register.php";
                }
                else {
                    self.data.action = "addpatienttodoctor";
                    self.data.url = "insertrelationdata.php";
                }
                self.data.doctorid = this.navParams.data.doctorsdata ? this.navParams.data.doctorsdata.userid : this.userLoginData.id;
                self.data.usertype = 'patient';
                self.data.appsecret = this.appsecret;
                self.data.firsttimelogin = 0;
                self.data.base_url = this.baseurl;
                console.log("data", this.data);
                self.http.post(this.baseurl + self.data.url, self.data).subscribe(function (data) {
                    _this.loadingService.show();
                    var result = JSON.parse(data["_body"]);
                    console.log(result);
                    if (result.status == 'success') {
                        self.loadingService.hide();
                        self.toastCtrl.presentToast("Patient added successfully");
                        if (_this.userLoginData.usertype == "healthcareprovider" && !_this.navParams.data.doctorsdata) {
                            self.navCtrl.setRoot("AppMyPatientsPage");
                        }
                        else if (_this.navParams.data.doctorsdata.usertype == "nurse") {
                            self.navCtrl.setRoot("AppMyNursesPage");
                        }
                        else if (_this.navParams.data.doctorsdata.usertype == "doctor") {
                            self.navCtrl.setRoot("AppMyDoctorsPage");
                        }
                    }
                    else if (result.status == 'errorEmail') {
                        self.loadingService.hide();
                        self.navCtrl.setRoot("AppMyPatientsPage");
                        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                    }
                    else {
                        self.loadingService.hide();
                        self.navCtrl.setRoot("AppMyPatientsPage");
                    }
                }, function (err) {
                    _this.loadingService.hide();
                    console.log(err);
                });
            }
        }
    };
    AppPatientProfilePage.prototype.ImageUpload = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            // title: 'Image Upload',
            message: 'Select Image source',
            buttons: [
                {
                    text: 'Upload from Library',
                    handler: function () {
                        _this.gallery(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.gallery(_this.camera.PictureSourceType.CAMERA);
                    }
                }, {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    AppPatientProfilePage.prototype.gallery = function (sourceType) {
        var self = this;
        this.camera.getPicture({
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: sourceType,
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            correctOrientation: true,
            allowEdit: true
        }).then(function (imageData) {
            // imageData is a base64 encoded string
            self.showImagePreview();
            self.data.image = "data:image/jpeg;base64," + imageData;
            self.image = "data:image/jpeg;base64," + imageData;
        }, function (err) {
            console.log("Error " + err);
        });
    };
    AppPatientProfilePage.prototype.showImagePreview = function () {
        var self = this;
        self.setVisibility(self.previewImage, "block");
        self.setVisibility(self.imageUploader, "none");
        self.setVisibility(self.addPhotoCaption, "none");
        self.setVisibility(self.removePhotoIcon, "block");
    };
    AppPatientProfilePage.prototype.removeImage = function () {
        var self = this;
        this.data.image = undefined;
        self.setVisibility(self.previewImage, "none");
        self.setVisibility(self.imageUploader, "block");
        self.setVisibility(self.addPhotoCaption, "block");
        self.setVisibility(self.removePhotoIcon, "none");
    };
    AppPatientProfilePage.prototype.setVisibility = function (element, state) {
        if (element !== undefined)
            element.nativeElement.style.display = state;
    };
    AppPatientProfilePage.prototype.getDate = function () {
        var date = new Date(this.date);
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        // return date.getDate();
    };
    AppPatientProfilePage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        console.log('set string in storage: ' + result);
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        console.log(reason_1);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // to get a key/value pair
    AppPatientProfilePage.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        console.log('storageGET: ' + key + ': ' + result);
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        return [2 /*return*/, null];
                    case 2:
                        reason_2 = _a.sent();
                        console.log(reason_2);
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // remove a single key value:
    AppPatientProfilePage.prototype.remove = function (key) {
        this.storage.remove(key);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('imageUploader'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppPatientProfilePage.prototype, "imageUploader", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('addPhotoCaption'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppPatientProfilePage.prototype, "addPhotoCaption", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('removePhotoIcon'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppPatientProfilePage.prototype, "removePhotoIcon", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('previewImage'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AppPatientProfilePage.prototype, "previewImage", void 0);
    AppPatientProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-patient-profile',template:/*ion-inline-start:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patient-profile/app-patient-profile.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title *ngIf="!this.action" text-center>Patient Profile</ion-title>\n        <ion-title *ngIf="this.action" text-center>Edit Patient Profile</ion-title>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <!-- <ion-buttons right (click)="logout()">\n            <button ion-button icon-only>\n                <ion-icon name="lock">Logout</ion-icon>\n            </button>\n        </ion-buttons> -->\n    </ion-navbar>\n</ion-header>\n<ion-content>\n    <ion-refresher slot="fixed" (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n    <ion-grid padding>\n        <ion-row>\n            <ion-col>\n                <div *ngIf="!this.action" class="app-description">\n                    Please fill all the info below\n                </div>\n                <div *ngIf="this.action" class="app-description">\n                    You can edit or fill all info below\n                </div>\n            </ion-col>\n        </ion-row>\n        <form [formGroup]="Profileform">\n            <ion-row wrap padding>\n                <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 padding>\n                    <ion-row>\n                        <ion-col col-2></ion-col>\n                        <ion-col col-8>\n                            <div no-padding transparent no-lines text-center class="item-width">\n                                <div *ngIf="!data?.image" padding\n                                    style="background: #39415b; color:#444; border-radius: 10px">\n                                    <div #imageUploader><img style="width:30%;" [src]="upload"\n                                            (click)="!readonly && ImageUpload()"></div>\n                                    <div #addPhotoCaption style="color:#fff;">Add Photo</div>\n                                </div>\n                                <div #previewImage> <img style="border-radius:20px" [src]="data.image"\n                                        *ngIf="data?.image"></div>\n                                <div *ngIf="data.image" #removePhotoIcon style="margin:auto;width: 10%">\n                                    <ion-icon name="close-circle" (click)="!readonly && removeImage()"></ion-icon>\n                                </div>\n\n                            </div>\n                        </ion-col>\n                        <ion-col col-2></ion-col>\n                    </ion-row>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            User Type\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-input formControlName="usertype" style="color:#666 !important;" type="text" readonly\n                            [(ngModel)]="data.usertype"></ion-input>\n                    </ion-item>\n                    <!-- <ion-item class="item-width" style="background:none;">\n                      <ion-label>Select Action</ion-label>\n                      <ion-select formControlName="selectType" [(ngModel)]="data1.selectType" (ionChange)="addType($event)">\n                        <ion-option *ngIf="!addExisting" value="new" selected>Add New</ion-option>\n                        <ion-option value="exist">Add Existing</ion-option>\n                      </ion-select>\n                    </ion-item> -->\n                    <ion-item *ngIf="!hcpLoggedIn" class="item-width" style="background:none;">\n                        <ion-label>Select Patient</ion-label>\n                        <ion-select formControlName="storedDoctors" [(ngModel)]="data.storedDoctors"\n                            (ionChange)="checkState($event)">\n                            <ion-option *ngFor="let key of data4.newDoctors;" value="{{key.id}}">\n                                {{key.firstname}}\n\n                            </ion-option>\n                        </ion-select>\n                    </ion-item>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Email Address\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-input formControlName="email" placeholder="Select your email address here"\n                            [(ngModel)]="data.email" type="text" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.email.valid && (Profileform.controls.email.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please enter valid Email</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Firstname\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-input formControlName="firstname" type="text" placeholder="Type your firstname here"\n                            [(ngModel)]="data.firstname" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.firstname.valid && (Profileform.controls.firstname.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please Enter Firstname</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Lastname\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-input formControlName="lastname" type="text" placeholder="Type your lastname here"\n                            [(ngModel)]="data.lastname" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.lastname.valid && (Profileform.controls.lastname.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please Enter Lastname</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Birth Date\n                            <!-- <span style="color: #e0675c;margin-left: 7px;">*</span> -->\n                        </ion-label>\n                        <ion-datetime formControlName="birthDate" placeholder="DD/MM/YYYY" [(ngModel)]="data.birthdate"\n                            displayFormat="DD/MM/YYYY" style="margin:8px;padding:0px" disabled="{{enableDiv}}">\n                        </ion-datetime>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.birthDate.valid && (Profileform.controls.birthDate.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please Select birthDate</p>\n                </ion-col>\n                <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n                    <ion-item *ngIf="ut" style="margin-bottom:0;" margin transparent no-lines>\n                        <ion-label [whiteLabel] stacked>Password</ion-label>\n                        <ion-input formControlName="password" type="password" placeholder="Type your password here"\n                            [(ngModel)]="data.password"></ion-input>\n                    </ion-item>\n                    <ion-label style="float: left;margin: 0px 33px;"\n                        *ngIf="!Profileform.controls.password.valid && (Profileform.controls.password.dirty && ut || submitAttempt)">\n                        <p style="margin-top:2px;margin-bottom:0;color:#e0675c">\n                            Password cannot be empty\n                        </p>\n                    </ion-label>\n                    <ion-item *ngIf="ut" style="margin-bottom:0;" margin transparent no-lines>\n                        <ion-label [whiteLabel] stacked>Confirm Password</ion-label>\n                        <ion-input formControlName="confirmPassword" type="password"\n                            placeholder="Re-type your password here" [(ngModel)]="data.confirmPassword"></ion-input>\n                    </ion-item>\n                    <ion-label style="float: left;margin: 0px 33px;"\n                        *ngIf="!Profileform.controls.confirmPassword.valid && (Profileform.controls.confirmPassword.dirty || submitAttempt && ut)">\n                        <p style="margin-top:2px;margin-bottom:0;color:#e0675c">\n                            Password cannot be empty\n                        </p>\n                    </ion-label>\n                    <ion-label style="float: left;margin: 0px 33px;"\n                        *ngIf="Profileform.hasError(\'confirmPassword\') && ut">\n                        <p style="margin-top:2px;margin-bottom:0;color:#e0675c">\n                            Passwords are not <strong>matching</strong>.\n                        </p>\n                    </ion-label>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Sex\n                            <!-- <span  style="color: #e0675c;margin-left: 7px;">*</span> -->\n                        </ion-label>\n                        <ion-select formControlName="sex" placeholder="Select your gender here" [(ngModel)]="data.sex"\n                            disabled="{{enableDiv}}">\n                            <ion-option value="f">Female</ion-option>\n                            <ion-option value="m">Male</ion-option>\n                        </ion-select>\n                    </ion-item>\n                    <!-- <p *ngIf="!Profileform.controls.sex.valid && (Profileform.controls.sex.dirty || submitAttempt)" style="color: #e0675c;margin: 0px 17px;">Please Select sex</p> -->\n\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Mobile Number\n                            <span style="color: #e0675c;margin-left: 7px;">*</span>\n                        </ion-label>\n                        <ion-input formControlName="mobile" placeholder="Enter Mobile Number" [(ngModel)]="data.mobile"\n                            type="text" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.mobile.valid && (Profileform.controls.mobile.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please Enter Mobile Number</p>\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>Full Address</ion-label>\n                        <ion-input formControlName="address" placeholder="Street/ Building No/ Zip/ City/ Country"\n                            [(ngModel)]="data.street" type="text" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <!-- <img class="location" src="assets/images/map.png" /> -->\n                    <ion-item padding transparent class="item-width">\n                        <ion-label stacked>\n                            Language Preferred\n                            <!-- <span  style="color: #e0675c;margin-left: 7px;">*</span> -->\n                        </ion-label>\n                        <ion-input formControlName="languagePreferred" placeholder="Type your preferred Language here"\n                            [(ngModel)]="data.languagepreferred" type="text" disabled="{{enableDiv}}"></ion-input>\n                    </ion-item>\n                    <p *ngIf="!Profileform.controls.languagePreferred.valid && (Profileform.controls.languagePreferred.dirty || submitAttempt)"\n                        style="color: #e0675c;margin: 0px 17px;">Please enter Language Preferred</p>\n                    <!-- <ion-row>\n                        <ion-col col-12>\n                            <div style="color:#fff;padding-bottom: 0" padding>Diagnosis</div>\n                        </ion-col>\n                    </ion-row>\n                    <ion-row formArrayName="diagnosis">\n                        <ng-container *ngFor="let item of data.diagnosis;let i = index">\n                           <ion-col col-11>\n                               <ion-item  transparent class="item-width" [formGroupName]="i">\n                                   <ion-label style="display: none;" stacked>Diagnosis</ion-label>\n                                   <ion-input formControlName="name" style="margin-top:0.5em;margin-bottom:0.5em"  placeholder="Type diagnosis here or add documents" [(ngModel)]="item.name" type="text" disabled="{{enableDiv}}"></ion-input>\n                               </ion-item>\n                           </ion-col>\n                           <ion-col col-1>\n                               <ion-icon name="close" (click)="deleteDiagnosis(i)" *ngIf="i!==0" margin></ion-icon>\n                           </ion-col>\n                        </ng-container>\n                    </ion-row>\n                    <div padding (click)="addDiagnosis()">\n                        <ion-icon name="add"></ion-icon>\n                        Add additional diagnosis\n                    </div> -->\n\n                    <!-- <ion-row formArrayName="diagnosis">\n                        <ng-container *ngFor="let item of Profileform.controls.diagnosis.controls; let i=index">\n                           <ion-col col-11>\n                               <ion-item  transparent class="item-width" [formGroupName]="i">\n                                   <ion-label style="display: none;" stacked>Diagnosis</ion-label>\n                                   <ion-input [(ngModel)]="item.name" formControlName="name" style="margin-top:0.5em;margin-bottom:0.5em"  placeholder="Type diagnosis here or add documents" type="text" ></ion-input>\n                               </ion-item>\n                           </ion-col>\n                           <ion-col col-1>\n                               <ion-icon name="close" (click)="removeAddress(i)" *ngIf="i!==0" margin></ion-icon>\n                           </ion-col>\n                        </ng-container>\n                    </ion-row> -->\n\n                    <!-- <div padding (click)="addAddress()">\n                        <ion-icon name="add"></ion-icon>\n                        Add additional diagnosis\n                    </div> -->\n\n                    <!-- <div style="color:#fff;padding-bottom: 0" padding>Next of kin</div> -->\n\n                    <ion-row formArrayName="contacts">\n                        <ng-container *ngFor="let item of data.contacts;let i = index">\n                            <ion-col col-6>\n                                <ion-item transparent class="item-width" [formGroupName]="i">\n                                    <ion-label style="display:none" stacked>Contacts</ion-label>\n                                    <ion-input formControlName="name" style="margin-top:0.5em;margin-bottom:0.5em"\n                                        placeholder="Contact Name" [(ngModel)]="item.name" required type="text"\n                                        disabled="{{enableDiv}}"></ion-input>\n                                </ion-item>\n                            </ion-col>\n                            <ion-col col-5>\n                                <ion-item transparent class="item-width" [formGroupName]="i">\n                                    <ion-label style="display:none" stacked>Contacts</ion-label>\n                                    <ion-input formControlName="phone" style="margin-top:0.5em;margin-bottom:0.5em"\n                                        placeholder="Phone number" [(ngModel)]="item.phone" required type="text"\n                                        disabled="{{enableDiv}}"></ion-input>\n                                </ion-item>\n                            </ion-col>\n                            <ion-col col-1>\n                                <ion-icon name="close" (click)="deleteContact(i)" margin></ion-icon>\n                            </ion-col>\n                        </ng-container>\n                    </ion-row>\n\n                    <div padding (click)="addContact()">\n                        <ion-icon name="add"></ion-icon>\n                        Add Next of Kin\n                    </div>\n                    <div class="block-insert" margin>\n                        <button (click)="addPatientDetails(data4.newDoctors)" class="dark-button" ion-button round>\n                            Save\n                        </button>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </form>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/prashilparmar/Documents/IONIC/BitBucket/victoryGIT/victorycare-ionic3/src/pages/app-patient-profile/app-patient-profile.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* NavParams */], __WEBPACK_IMPORTED_MODULE_10__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_geocoder__["a" /* NativeGeocoder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_database_service__["a" /* DatabaseService */], __WEBPACK_IMPORTED_MODULE_3__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_11__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_12__services_toast_service__["a" /* ToastService */]])
    ], AppPatientProfilePage);
    return AppPatientProfilePage;
}());

//# sourceMappingURL=app-patient-profile.js.map

/***/ }),

/***/ 953:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppPatientProfileModule", function() { return AppPatientProfileModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_patient_profile__ = __webpack_require__(1007);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppPatientProfileModule = (function () {
    function AppPatientProfileModule() {
    }
    AppPatientProfileModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_patient_profile__["a" /* AppPatientProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_patient_profile__["a" /* AppPatientProfilePage */]),
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], AppPatientProfileModule);
    return AppPatientProfileModule;
}());

//# sourceMappingURL=app-patient-profile.module.js.map

/***/ }),

/***/ 974:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailValidator; });
var EmailValidator = (function () {
    function EmailValidator() {
    }
    EmailValidator.isValid = function (control) {
        var re = /^\s*(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$/.test(control.value);
        if (re) {
            return null;
        }
        return { "invalidEmail": true };
    };
    return EmailValidator;
}());

//# sourceMappingURL=email-validator.js.map

/***/ }),

/***/ 975:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NativeGeocoder; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_core__ = __webpack_require__(32);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @name Native Geocoder
 * @description
 * Cordova plugin for native forward and reverse geocoding
 *
 * @usage
 * ```typescript
 * import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
 *
 * constructor(private nativeGeocoder: NativeGeocoder) { }
 *
 * ...
 *
 * let options: NativeGeocoderOptions = {
 *     useLocale: true,
 *     maxResults: 5
 * };
 *
 * this.nativeGeocoder.reverseGeocode(52.5072095, 13.1452818, options)
 *   .then((result: NativeGeocoderReverseResult[]) => console.log(JSON.stringify(result[0])))
 *   .catch((error: any) => console.log(error));
 *
 * this.nativeGeocoder.forwardGeocode('Berlin', options)
 *   .then((coordinates: NativeGeocoderForwardResult[]) => console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude))
 *   .catch((error: any) => console.log(error));
 * ```
 * @interfaces
 * NativeGeocoderReverseResult
 * NativeGeocoderForwardResult
 * NativeGeocoderOptions
 */
var NativeGeocoder = (function (_super) {
    __extends(NativeGeocoder, _super);
    function NativeGeocoder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Reverse geocode a given latitude and longitude to find location address
     * @param latitude {number} The latitude
     * @param longitude {number} The longitude
     * @param options {NativeGeocoderOptions} The options
     * @return {Promise<NativeGeocoderReverseResult[]>}
     */
    /**
       * Reverse geocode a given latitude and longitude to find location address
       * @param latitude {number} The latitude
       * @param longitude {number} The longitude
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderReverseResult[]>}
       */
    NativeGeocoder.prototype.reverseGeocode = /**
       * Reverse geocode a given latitude and longitude to find location address
       * @param latitude {number} The latitude
       * @param longitude {number} The longitude
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderReverseResult[]>}
       */
    function (latitude, longitude, options) { return; };
    /**
     * Forward geocode a given address to find coordinates
     * @param addressString {string} The address to be geocoded
     * @param options {NativeGeocoderOptions} The options
     * @return {Promise<NativeGeocoderForwardResult[]>}
     */
    /**
       * Forward geocode a given address to find coordinates
       * @param addressString {string} The address to be geocoded
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderForwardResult[]>}
       */
    NativeGeocoder.prototype.forwardGeocode = /**
       * Forward geocode a given address to find coordinates
       * @param addressString {string} The address to be geocoded
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderForwardResult[]>}
       */
    function (addressString, options) { return; };
    NativeGeocoder.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])({
            callbackOrder: 'reverse'
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Number, Number, Object]),
        __metadata("design:returntype", Promise)
    ], NativeGeocoder.prototype, "reverseGeocode", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])({
            callbackOrder: 'reverse'
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Object]),
        __metadata("design:returntype", Promise)
    ], NativeGeocoder.prototype, "forwardGeocode", null);
    /**
     * @name Native Geocoder
     * @description
     * Cordova plugin for native forward and reverse geocoding
     *
     * @usage
     * ```typescript
     * import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
     *
     * constructor(private nativeGeocoder: NativeGeocoder) { }
     *
     * ...
     *
     * let options: NativeGeocoderOptions = {
     *     useLocale: true,
     *     maxResults: 5
     * };
     *
     * this.nativeGeocoder.reverseGeocode(52.5072095, 13.1452818, options)
     *   .then((result: NativeGeocoderReverseResult[]) => console.log(JSON.stringify(result[0])))
     *   .catch((error: any) => console.log(error));
     *
     * this.nativeGeocoder.forwardGeocode('Berlin', options)
     *   .then((coordinates: NativeGeocoderForwardResult[]) => console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude))
     *   .catch((error: any) => console.log(error));
     * ```
     * @interfaces
     * NativeGeocoderReverseResult
     * NativeGeocoderForwardResult
     * NativeGeocoderOptions
     */
    NativeGeocoder = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["f" /* Plugin */])({
            pluginName: 'NativeGeocoder',
            plugin: 'cordova-plugin-nativegeocoder',
            pluginRef: 'nativegeocoder',
            repo: 'https://github.com/sebastianbaar/cordova-plugin-nativegeocoder',
            platforms: ['iOS', 'Android']
        })
    ], NativeGeocoder);
    return NativeGeocoder;
}(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["e" /* IonicNativePlugin */]));

//# sourceMappingURL=index.js.map

/***/ })

});
//# sourceMappingURL=0.js.map