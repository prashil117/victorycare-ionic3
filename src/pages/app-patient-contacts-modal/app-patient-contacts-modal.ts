import { Component, ViewChild, AfterViewInit, ElementRef } from "@angular/core";
import {
  AlertController,
  IonicPage,
  ModalController,
  NavController,
  Navbar,
  NavParams,
  Platform,
  ViewController,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import * as _ from "lodash";
import {
  Validators,
  FormBuilder,
  FormGroup,
  FormControl,
} from "@angular/forms";
import { ToastService } from "../../services/toast-service";
import { Http } from "@angular/http";
import { Storage } from "@ionic/storage";
import { environment } from "../../environment/environment";
import { timeInterval } from "rxjs/operators";
import { Geolocation } from "@ionic-native/geolocation";

declare var google;

@IonicPage()
@Component({
  selector: "page-app-patient-contacts-modal",
  templateUrl: "app-patient-contacts-modal.html",
  providers: [DatabaseService],
})
export class AppPatientContactsModalPage {
  @ViewChild("map")
  mapElement: ElementRef;
  map: any;
  address: string;
  latitude: number;
  longitude: number;
  data: any = {};
  contactsdata: any = [];
  patientData: any = [];
  ContactsType: any = {};
  itemIndex: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  PageTitle: any;
  Contectform: FormGroup;
  userLoginData: any;
  patientType: any;
  type: any;

  @ViewChild(Navbar)
  navBar: Navbar;
  submitAttempt: boolean;
  constructor(
    private geo: Geolocation,
    public http: Http,
    public storage: Storage,
    private toastCtrl: ToastService,
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public navParams: NavParams,
    private platform: Platform,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    private databaseService: DatabaseService,
  ) {
    this.type = navParams.data.type;
    console.log("this.type", this.type);
    this.Contectform = formBuilder.group({
      name: ["", Validators.required],
      relationship: ["", Validators.required],
      mobile: ["", Validators.required],
      notes: [""],
      address: [""],
    });
    this.geo.getCurrentPosition().then((pos) => {
      console.log("eroror", pos);
      this.latitude = pos.coords.latitude;
      this.longitude = pos.coords.longitude;
      // this.loadMap(this.latitude, this.longitude);
    }).catch((err) => console.log(err));

    if (!_.isEmpty(navParams.data)) {
      if (navParams.data.type === "edit") {
        this.patientData = navParams.data.data;
        this.ContactsType = navParams.data.type;
        this.itemIndex = navParams.data.index;
        this.data = navParams.data.contactsData;
        this.PageTitle = "Edit Contact";
        console.log("data", this.navParams.data);
      } else {
        this.patientData = navParams.data.data;
        this.ContactsType = "";
        this.data = navParams.data.contactsData;
        console.log("add", this.data);
        this.PageTitle = "Add Contact";
      }
    }
  }

  loadMap(lat, lng) {
    let latLng = new google.maps.LatLng(lat, lng);

    let mapOption = {
      center: latLng,
      zoom: 14,
      mapTypeId: "roadmap",
      disableDefaultUI: true,
    };

    let element = document.getElementById("map");

    this.map = new google.maps.Map(this.mapElement, mapOption);
    console.log("this.map", this.map);
    let marker = new google.maps.Marker({
      position: latLng,
      title: "Biratnagar, Janpath-15",
      icon: "http://maps.google.com/mapfiles/ms/icons/green-dot.png",
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AppPatientContactsModalPage");
    this.getuserLoginData();
  }

  closeModal() {
    this.viewCtrl.dismiss(false);
  }

  savePatientContact() {
    let self = this;
    this.submitAttempt = true;
    if (!this.Contectform.valid) {
      console.log("Profileform Validation Fire!");
    } else {
      if (
        !_.isUndefined(this.data.name) && !_.isUndefined(this.data.phone) &&
        !_.isUndefined(this.data.notes)
      ) {
        if (this.patientType == "independent") {
          this.data.userid = this.userLoginData.id;
          this.data.addedby = this.userLoginData.id;
        }
        this.data.action = "addcontact";
        this.data.appsecret = this.appsecret;
        this.http.post(this.baseurl + "adddata.php", this.data).subscribe(
          (data) => {
            console.log("!!!!")
            var result = JSON.parse(data["_body"]);
            if (result.status == "success") {
              if (this.navParams.data.msg == "no") {
                this.viewCtrl.dismiss(false);
                // this.toastCtrl.presentToast("Contact added successfully");
              } else {
                console.log(")))))))))2356")
                this.viewCtrl.dismiss(false);
                this.toastCtrl.presentToast("Contact added successfully");
              }
            } else {
              console.log(")))))))))456")
              this.viewCtrl.dismiss(true);
              this.toastCtrl.presentToast("Contact not added successfully");
            }
          },
        ), (err) => {
          console.log(")))))))))123")
          this.viewCtrl.dismiss(true);
          this.toastCtrl.presentToast("something went wrong");
        };
      } else {
        console.log(")))))))))")
        this.presentAlert("Please enter all required details");
      }
    }
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ["Dismiss"],
    });
    alert.present();
  }

  updateContactsData() {
    // console.log('itemIndex',this.patientData);
    // console.log('itemIndex',this.data);
    let self = this;
    this.submitAttempt = true;
    if (!this.Contectform.valid) {
      console.log("Profileform Validation Fire!");
    } else {
      if (
        !_.isUndefined(this.data.name) && !_.isUndefined(this.data.phone) &&
        !_.isUndefined(this.data.notes)
      ) {
        //this.getLetLong();
        // self.databaseService.updatePatient(self.patientData.data);
        this.data.action = "updatecontact";
        this.data.appsecret = this.appsecret;
        this.http.post(this.baseurl + "updatedata.php", this.data).subscribe(
          (data) => {
            var result = JSON.parse(data["_body"]);
            if (result.status = "success") {
              this.viewCtrl.dismiss(true);
              this.toastCtrl.presentToast("Contact updated successfully");
            } else {
              this.viewCtrl.dismiss(false);
              this.toastCtrl.presentToast("Contact not updated successfully");
            }
          },
        ), (err) => {
          this.viewCtrl.dismiss(false);
          this.toastCtrl.presentToast("something went wrong");
        };
      } else {
        this.presentAlert("Please enter all required details");
      }
    }
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }
  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get("patientType").then((result) => {
      console.log("patienttypesdfdsfsdsdf", result);
      if (
        (!result || result == null || result == undefined) &&
        this.userLoginData == "patient"
      ) {
        this.doRefresh("");
      } else {
        this.patientType = result;
        console.log("sekeccdffvfdgdfgdfg", this.patientType);
      }
    });
  }
  doRefresh(event) {
    console.log("Begin async operation");

    setTimeout(() => {
      console.log("Async operation has ended");
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }
}
