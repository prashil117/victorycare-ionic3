import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppPatientContactsModalPage } from './app-patient-contacts-modal';

@NgModule({
  declarations: [
    AppPatientContactsModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AppPatientContactsModalPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppPatientContactsModalPageModule {}
