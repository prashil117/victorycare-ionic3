import { Component, AfterViewInit } from "@angular/core";
import {
  AlertController,
  IonicPage,
  ModalController,
  NavController,
  NavParams,
  Platform,
  ViewController,
} from "ionic-angular";

import { LoadingService } from "../../services/loading-service";
import { Observable } from "rxjs/Observable";
import * as _ from "lodash";
import { AppSharedService } from "../../services/app-shared-service";
import {
  Validators,
  FormBuilder,
  FormGroup,
  FormControl,
} from "@angular/forms";

@IonicPage()
@Component({
  selector: "app-add-contact-modal",
  templateUrl: "app-add-contact-modal.html",
  providers: [],
})
export class AppAddContactModalPage {
  data: any = {};
  // contacts: any = [];
  contacts = [];
  contactType: any;
  ContactTitle: any;
  Contectform: FormGroup;
  submitAttempt: boolean;

  constructor(
    public navCtrl: NavController,
    navParams: NavParams,
    public formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private loadingService: LoadingService,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public appSharedService: AppSharedService,
  ) {
    this.Contectform = formBuilder.group({
      name: ["", Validators.required],
      relationship: ["", Validators.required],
      mobile: ["", Validators.required],
      address: [""],
      notes: [""],
    });

    if (!_.isEmpty(navParams.data)) {
      this.contacts = navParams.data.contacts;
      console.log("this.contact", this.contacts);
      this.contactType = navParams.data.type;
    } else {
      this.contactType = "";
    }
    this.ContactTitle = navParams.data.type == "masterdata"
      ? "Add Contact"
      : "Add Next of Kin";
  }

  closeModal() {
    this.viewCtrl.dismiss(false);
  }

  /*SaveContactData(){
        console.log('this.data',this.data);
        console.log('this.contacts',this.contacts);
    }*/

  public onSubmit() {
    let self = this;
    this.submitAttempt = true;
    if (!this.Contectform.valid) {
      console.log("Profileform Validation Fire!");
    } else {
      console.log("abcd", this.contacts);
      self.contacts.push(this.data);
      self.closeModal();
    }
  }
}
