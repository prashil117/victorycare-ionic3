import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {AppAddContactModalPage} from "./app-add-contact-modal";



@NgModule({
  declarations: [
      AppAddContactModalPage
  ],
  imports: [
    IonicPageModule.forChild(AppAddContactModalPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppAddContactModalModule {}
