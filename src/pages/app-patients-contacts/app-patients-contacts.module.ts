import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AppPatientsContactsPage } from './app-patients-contacts';
import { PipesModule } from '../../pipes/pipes.module';//<--- here

@NgModule({
  declarations: [
    AppPatientsContactsPage,
  ],
  imports: [
    IonicPageModule.forChild(AppPatientsContactsPage),
    NgxDatatableModule,
    PipesModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppPatientsContactsPageModule {}
