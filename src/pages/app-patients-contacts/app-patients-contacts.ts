import { Component, ViewChild, AfterViewInit, ElementRef } from "@angular/core";
import {
  AlertController,
  IonicPage,
  ModalController,
  MenuController,
  NavController,
  Navbar,
  NavParams,
  Platform,
  ViewController,
  Events,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from "rxjs/Observable";
import { Storage } from "@ionic/storage";
import * as _ from "lodash";
import { AppPatientContactsModalPage } from "../app-patient-contacts-modal/app-patient-contacts-modal";
import { AppAddContactModalPage } from "../app-add-contact-modal/app-add-contact-modal";
import { AppPatientHomePage } from "../app-patient-home/app-patient-home";
import { Http } from "@angular/http";
import { environment } from "../../environment/environment";
import { ToastService } from "../../services/toast-service";

/**
 * Generated class for the AppPatientsContactsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-app-patients-contacts",
  templateUrl: "app-patients-contacts.html",
  providers: [DatabaseService],
})
export class AppPatientsContactsPage implements AfterViewInit {
  data: any = [];
  patientData: any = {};
  pages: Array<{ title: string; component: any; icon: any }>;
  contactsData: any = [];
  SearchData: any = [];
  descending: boolean = false;
  order: number;
  terms: string;
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  patientSessionData: any;
  column: string = "name";
  hcpid: any;
  userLoginData: any;
  patientType: any;
  @ViewChild(Navbar)
  navBar: Navbar;
  @ViewChild("map")
  mapElement: ElementRef;
  // private map:GoogleMap;
  // private location:LatLng;

  constructor(
    public loadingService: LoadingService,
    private storage: Storage,
    private toastCtrl: ToastService,
    public http: Http,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public events: Events,
    public menu: MenuController,
    public modalCtrl: ModalController,
    private databaseService: DatabaseService,
  ) {
    this.storage.get("patientSession").then((val) => {
    });
    if (!_.isEmpty(navParams.data.data)) {
      this.patientData = navParams.data.data;
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AppPatientsContactsPage");
    this.getHCPid();
  }

  ngAfterViewInit(): void {
    console.log("ngAfterViewInit");
  }

  updatefilter(event) {
    const val = event.target.value.toLowerCase();
  }

  async getHCPid() {
    await this.get("currentHCP").then((result) => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
        console.log("if", this.hcpid);
      } else {
        this.getuserLoginData();
      }
    });
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      } else {
        return null;
      }
    } catch (reason) {
      return null;
    }
  }

  AddContact() {
    var data: any = {};
    if (this.patientData != null) {
      data.userid = this.patientData.id
        ? this.patientData.id
        : this.userLoginData.id;
      data.addedby = this.hcpid;
    }
    let modal = this.modalCtrl.create(
      AppPatientContactsModalPage,
      { "contactsData": data, type: "add" },
    );

    modal.present();
    modal.onDidDismiss((data) => {
      console.log("data",data)
      // if (data) {
        this.loadPatientContacts();
      // }
    });
  }

  // getLetLong() {
  //   let options: NativeGeocoderOptions = {
  //     useLocale: true,
  //     maxResults: 5
  //   };
  //   let self = this;
  //   for (let i = 0; i < self.patientData.data.contacts.length; i++) {
  //     // console.log('self.patientData.data.contacts[i].address',self.patientData.data.contacts[i].address);
  //     self.nativeGeocoder.forwardGeocode(self.patientData.data.contacts[i].address, options)
  //       .then((coordinates: NativeGeocoderForwardResult[]) => {
  //         self.patientData.data.contacts[i].lat = coordinates[0].latitude;
  //         self.patientData.data.contacts[i].long = coordinates[0].longitude;
  //         // console.log('patientData1',self.patientData.data);
  //       }).catch((error: any) => console.log(error));
  //   }
  //   console.log('patientData', self.patientData.data);
  //   self.databaseService.updatePatient(self.patientData.data);
  //   this.loadPatientContacts();
  // }

  loadPatientContacts() {
    this.loadingService.show();
    console.log("vararrsdfr", this.hcpid);
    let self = this;
    if (this.data.contacts == undefined) {
      self.contactsData = [];
      self.SearchData = [];
    }
    var data1: any = {};
    data1.action = "getnextofkin";
    data1.appsecret = this.appsecret;
    if (this.patientType == "independent") {
      data1.userid = this.userLoginData.id;
      data1.addedby = this.userLoginData.id;
      console.log("q1");
    } else if (
      this.userLoginData.usertype == "nurse" ||
      this.userLoginData.usertype == "patient" ||
      this.userLoginData.usertype == "doctor"
    ) {
      data1.userid = this.patientData.userid
        ? this.patientData.userid
        : this.userLoginData.id;
      data1.addedby = this.hcpid;
      console.log("q2");
    } else if (this.userLoginData.usertype == "healthcareprovider") {
      console.log("this.patientuser", this.patientData);
      data1.userid = this.patientData.userid;
      data1.addedby = this.userLoginData.id;
      console.log("q3");
    }
    this.http.post(this.baseurl + "getdata.php", data1).subscribe((result) => {
      console.log(result);
      let result1 = JSON.parse(result["_body"]);
      this.data.contacts = result1.data;
      if (this.data.contacts !== undefined) {
        this.loadingService.hide();
        self.contactsData = result1.data;
        self.SearchData = result1.data;
        //self.SearchData = this.patientData.data.contacts;
      } else {
        this.loadingService.hide();
        self.contactsData = [];
        self.SearchData = [];

        // this.toastCtrl.presentToast("something went wrong !");
      }
    });
    (err) => {
      this.loadingService.hide();
      // this.toastCtrl.presentToast("something went wrong !");
    };
  }

  EditContactsPopup(item, index) {
    let modal = this.modalCtrl.create(
      AppPatientContactsModalPage,
      {
        "contactsData": item,
        "data": this.patientData,
        "type": "edit",
        "index": index,
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.loadPatientContacts();
      }
    });
  }

  ionViewDidEnter() {
    this.navBar.backButtonClick = (e: UIEvent) => {
      // todo something
      this.navCtrl.setRoot("AppPatientsPage");
    };
  }

  sortField(fieldname) {
    this.column = fieldname;
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }

  setFilteredContactsData() {
    this.SearchData = this.contactsData.filter((Contacts) => {
      return Contacts.name.toLowerCase().indexOf(this.terms.toLowerCase()) > -1;
    });
  }

  deleteContectData(item, index) {
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete this record?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
        {
          text: "Delete",
          handler: () => {
            let self = this;
            var data1: any = {};
            data1.id = item.id;
            data1.action = "deletecontact";
            data1.appsecret = this.appsecret;
            this.http.post(this.baseurl + "deletedata.php", data1).subscribe(
              (res) => {
                var result = JSON.parse(res["_body"]);
                if (result.status = "success") {
                  this.toastCtrl.presentToast("Contact deleted succesfully !");
                  this.loadPatientContacts();
                } else {
                  this.toastCtrl.presentToast("Contact not deleted!");
                }
              },
            ), (err) => {
              this.toastCtrl.presentToast("something went wrong !");
            };
          },
        },
      ],
    });
    alert.present();
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
        }
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get("patientType").then((result) => {
      console.log("patienttypesdfdsfsdsdf", result);
      if (
        (!result || result == null || result == undefined) &&
        this.userLoginData == "patient"
      ) {
        this.doRefresh();
      } else {
        this.patientType = result;
        this.loadPatientContacts();
        console.log("sekeccdffvfdgdfgdfg", this.patientType);
      }
    });
  }

  doRefresh() {
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }
}
