import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppMedicineMasterListPage } from './app-medicine-master-list';
import { PipesModule } from '../../pipes/pipes.module';//<--- here

@NgModule({
  declarations: [
    AppMedicineMasterListPage,
  ],
  imports: [
    IonicPageModule.forChild(AppMedicineMasterListPage),
    PipesModule
  ],
})
export class AppMedicineMasterListPageModule {}
