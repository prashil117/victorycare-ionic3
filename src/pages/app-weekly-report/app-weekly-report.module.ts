import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppWeeklyReportPage } from './app-weekly-report';

@NgModule({
  declarations: [
    AppWeeklyReportPage,
  ],
  imports: [
    IonicPageModule.forChild(AppWeeklyReportPage),
  ],
})
export class AppWeeklyReportPageModule {}
