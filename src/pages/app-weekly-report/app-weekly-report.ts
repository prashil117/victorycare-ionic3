import { Component, ElementRef, ViewChild } from '@angular/core';
import { Events, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { Storage } from "@ionic/storage";
import { environment } from '../../environment/environment';
import { Http } from '@angular/http';
import { LoadingService } from '../../services/loading-service';
import { ToastService } from '../../services/toast-service';
import * as moment from 'moment';
import { DatabaseService } from '../../services/database-service'
const option = {
  legend: {
    display: false,
  },
  scales: {
    xAxes: [{
      gridLines: {
        display: false
      }
    }],
    yAxes: [{
      display: false,
      gridLines: {
        display: false,
        drawBorder: false,
      }
    }],
  }
}
const dougnhutOption: any = {
  responsive: true,
  aspectRatio: 1,
  maintainAspectRatio: true,
  legend: {
    display: true,
    fullWidth:true,
    position: 'bottom',
    labels: {
      usePointStyle: true,
  
    }
  }
}

@IonicPage()
@Component({
  selector: 'page-app-weekly-report',
  templateUrl: 'app-weekly-report.html',
  providers: [DatabaseService]
})
export class AppWeeklyReportPage {

  @ViewChild('lineChart') private chartRef: ElementRef;
  @ViewChild('barChart') private barchartRef: ElementRef;
  @ViewChild('lineChart1') private chartRef1: ElementRef;
  @ViewChild('barChart1') private barchartRef1: ElementRef;
  @ViewChild('lineChart2') private chartRef2: ElementRef;
  @ViewChild('barChart2') private barchartRef2: ElementRef;
  @ViewChild('lineChart3') private chartRef3: ElementRef;
  @ViewChild('barChart3') private barchartRef3: ElementRef;
  chart: any;
  barchart: any;
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  userLoginData: any;
  dependent: any;
  TotalSleep: any;
  TotalDistress: any;
  TotalComfort: any;
  dateArray: any;
  TotalUncomfort: any;
  SleepHours: any;
  DistressHours: any;
  ComfortHours: any;
  startOfWeek = moment(moment().startOf('week').format('YYYY/MM/DD'));
  UncomfortHours: any;
  AsleepBarChartArray: any = [];
  AwakeBarChartArray: any = [];
  PainBarChartArray: any = [];
  UncomfortBarChartArray: any = [];
  AwakeArray: any = [];
  AsleepArray: any = [];
  PainArray: any = [];
  UncomfortArray: any = [];
  weeklyHour = 168;
  selectedDate: any = new Date();

  constructor(public events: Events, public dateService: DatabaseService, private toastService: ToastService, private loadingService: LoadingService, public http: Http, public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {

  }

  ionViewDidLoad() {
    this.getuserLoginData()
  }
  getDates1() {
    this.events.subscribe('app:sendUser', logged => {
      this.selectedDate = logged;
      console.log("logged", logged)
      this.appIntialize(logged);
    });
    this.events.publish('app:getUser');
  }
  ionViewWillEnter() {
    this.getDates1();
  }

  ionViewWillLeave() {
    this.events.unsubscribe('app:sendUser');
  }

  SleepDoughnut(shour) {
    var data = [this.weeklyHour, shour]
    this.chart = new Chart(this.chartRef.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Total', 'Consume'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(83,155,192)',
            'rgb(107,177,214)'
          ],
        }]
      },
      options: dougnhutOption
    });
  }
  DistressDoughnut(Dhour) {
    var data = [this.weeklyHour, Dhour]
    this.chart = new Chart(this.chartRef1.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Total', 'Consume'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(226,96,96)',
            'rgb(171,87,87)'

          ],
        }]
      },
      options: dougnhutOption
    });
  }
  UnomfortableDoughnut(Uhour) {
    var data = [this.weeklyHour, Uhour]
    this.chart = new Chart(this.chartRef2.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Total', 'Consume'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(250,208,0)',
            'rgb(201,169,13)'
          ],
        }]
      },
      options: dougnhutOption
    });
  }
  ComfortableDoughnut(Chour) {
    var data = [this.weeklyHour , Chour]
    this.chart = new Chart(this.chartRef3.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Total', 'Consume'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(175,219,7)',
            'rgb(151,187,12)'


          ],
        }]
      },
      options: dougnhutOption
    });
  }

  SleepChart() {
    this.barchart = new Chart(this.barchartRef.nativeElement, {
      type: 'bar',
      data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
        datasets: [{

          barPercentage: 0.5,
          pointBorderColor: 'red',

          data: this.AsleepBarChartArray,
          backgroundColor: [
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
          ],
          borderColor: [
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
          ],
          borderWidth: 1,

        }, {
          data: this.AsleepBarChartArray,
          backgroundColor: [
            'rgb(107,177,214)',
          ],
          borderColor: [
            'rgb(107,177,214)',
          ],
          type: 'line',
          fill: false
        }

        ]
      },
      options: option
    });
  }

  DistressChart() {
    this.barchart = new Chart(this.barchartRef1.nativeElement, {
      type: 'bar',
      data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
        datasets: [{
          barPercentage: 0.5,
          data: this.PainBarChartArray,
          backgroundColor: [
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
          ],
          borderColor: [
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
          ],
          borderWidth: 1
        }, {
          data: this.PainBarChartArray,
          backgroundColor: [
            'rgb(171,87,87)',
          ],
          borderColor: [
            'rgb(171,87,87)',
          ],
          type: 'line',
          fill: false
        }]
      },
      options: option
    });
  }

  ComfortableChart() {
    this.barchart = new Chart(this.barchartRef2.nativeElement, {
      type: 'bar',
      data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
        datasets: [{
          barPercentage: 0.5,
          data: this.AwakeBarChartArray,
          backgroundColor: [
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
          ],
          borderColor: [
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
          ],
          borderWidth: 1
        },
        {
          data: this.AwakeBarChartArray,
          backgroundColor: [
            'rgb(201,169,13)',
          ],
          borderColor: [
            'rgb(201,169,13)',
          ],
          type: 'line',
          fill: false
        }]
      },
      options: option
    });
  }

  UncomfortableChart() {
    this.barchart = new Chart(this.barchartRef3.nativeElement, {
      type: 'bar',
      data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
        datasets: [{
          barPercentage: 0.5,
          data: this.UncomfortBarChartArray,
          backgroundColor: [
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',

          ],
          borderColor: [
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
          ],
          borderWidth: 1
        },
        {
          data: this.UncomfortBarChartArray,
          backgroundColor: [
            'rgb(175,219,7)',
          ],
          borderColor: [
            'rgb(175,219,7)',
          ],
          type: 'line',
          fill: false
        }
        ]
      },
      options: option
    });
  }

  appIntialize(date?) {
    var startOfWeek = moment(this.selectedDate).startOf('week').format('YYYY/MM/DD');
    var startOfWeek1 = moment(this.selectedDate).startOf('week');
    var startOfWeek2 = moment(this.selectedDate).startOf('week');
    var startOfWeek3 = moment(this.selectedDate).startOf('week');
    var startOfWeek4 = moment(this.selectedDate).startOf('week');
    var endOfWeek = moment(this.selectedDate).endOf('week').format('YYYY/MM/DD')
    this.AsleepBarChartArray = [];
    this.AwakeBarChartArray = [];
    this.PainBarChartArray = [];
    this.UncomfortBarChartArray = [];
    this.AwakeArray = [];
    this.AsleepArray = [];
    this.PainArray = [];
    this.UncomfortArray = [];
    this.TotalSleep = 0;
    this.TotalDistress = 0;
    this.TotalComfort = 0;
    this.dateArray = [];
    this.TotalUncomfort = 0;
    this.SleepHours = "0:0";
    this.DistressHours = "0:0";
    this.ComfortHours = "0:0";
    this.UncomfortHours = "0:0";
    this.getDates(startOfWeek, endOfWeek)
    this.loadingService.show();
    var data = {
      userid: this.userLoginData.id,
      appsecret: this.appsecret,
      startOfWeek: startOfWeek,
      endOfWeek: endOfWeek,
      action: 'getweeklygraphdata',
    }
    console.log("sdf", data)
    this.http.post(this.baseurl + 'getgraphs.php', data).subscribe(data => {
      this.loadingService.hide();
      var data1 = JSON.parse(data['_body'])
      var chartData = data1.graphSumData;
      if (data1.status == "success") {
        if (data1.graphData !== null || data1.graphData !== undefined) {
          data1 = data1.graphData;
          if (data1.asleep && data1.asleep.length > 0) {
            this.TotalSleep = data1.asleep.length;
            var sleepArray = [];
            for (var i in data1.asleep) {
              var startTime = moment(data1.asleep[i].starttime).format("HH:mm")
              var endTime = moment(data1.asleep[i].endtime).format("HH:mm")
              var diff = this.diff(startTime, endTime)

              sleepArray.push(diff)
            }
            this.SleepHours = this.calulateTotalTime(sleepArray);
            this.SleepDoughnut(parseInt(this.SleepHours.split(":")[0]))
          }
          else {
            this.TotalSleep = 0;
            this.SleepHours = '0:0';
            this.SleepDoughnut(0);

          }
          if (data1.pain && data1.pain.length > 0) {
            this.TotalDistress = data1.pain.length;
            var DistressArray = [];
            for (var i in data1.pain) {
              var startTime = moment(data1.pain[i].starttime).format("HH:mm")
              var endTime = moment(data1.pain[i].endtime).format("HH:mm")
              var diff = this.diff(startTime, endTime)
              DistressArray.push(diff)
            }
            this.DistressHours = this.calulateTotalTime(DistressArray);
            this.DistressDoughnut(parseInt(this.DistressHours.split(":")[0]))
          }
          else {
            this.TotalDistress = 0
            this.DistressHours = "0:0"
            this.DistressDoughnut(0);
          }
          if (data1.awake && data1.awake.length > 0) {
            this.TotalComfort = data1.awake.length;
            var ComfortArray = [];
            var chartArray = [];
            for (var i in data1.awake) {
              var startTime = moment(data1.awake[i].starttime).format("HH:mm");
              var endTime = moment(data1.awake[i].endtime).format("HH:mm");
              var diff = this.diff(startTime, endTime);
              chartArray.push(moment.duration(diff).asSeconds())
              ComfortArray.push(diff);
            }
            this.ComfortHours = this.calulateTotalTime(ComfortArray);
            this.ComfortableDoughnut(parseInt(this.ComfortHours.split(":")[0]))
          }
          else {
            this.TotalComfort = 0;
            this.ComfortHours = "0:0";
            this.ComfortableDoughnut(0)
          }
          if (data1.uncomfortable && data1.uncomfortable.length > 0) {
            this.TotalUncomfort = data1.uncomfortable.length;
            var UncomfortArray = [];
            for (var i in data1.uncomfortable) {
              var startTime = moment(data1.uncomfortable[i].starttime).format("HH:mm");
              var endTime = moment(data1.uncomfortable[i].endtime).format("HH:mm");
              var diff = this.diff(startTime, endTime);
              UncomfortArray.push(diff);
            }
            this.UncomfortHours = this.calulateTotalTime(UncomfortArray);
            this.UnomfortableDoughnut(parseInt(this.UncomfortHours.split(":")[0]))
          }
          else {
            this.TotalUncomfort = 0;
            this.UncomfortHours = "0:0"
            this.UnomfortableDoughnut(parseInt(this.UncomfortHours.split(":")[0]))
          }
        }
        if (chartData && chartData.length > 0) {
          this.getDiffer(chartData);
          // var startOfWeek = moment().startOf('week').format('YYYY/MM/DD');
          var abc = [];
          var Aindex = 0
          var Pindex = 0
          var Uindex = 0
          var Sindex = 0
          var index = 0
          for (let i = 0; i < 7; i++) {

            if (this.UncomfortArray[Uindex] !== undefined) {
              if (moment(this.UncomfortArray[Uindex].createddate).format('YYYY/MM/DD') === moment(startOfWeek1).format('YYYY/MM/DD')) {
                var sec = (this.UncomfortArray[Uindex].endtimeseconds - this.UncomfortArray[Uindex].starttimeseconds) * 1000;
                var tmpvalue = Math.round(moment.duration(sec).asHours())
                this.UncomfortBarChartArray.push(tmpvalue)
                Uindex = Uindex + 1;
                startOfWeek1 = moment(startOfWeek1).add(1, 'days');
              }
              else {

                this.UncomfortBarChartArray.push(0)
                startOfWeek1 = moment(startOfWeek1).add(1, 'days');
              }
            }
            else {
              this.UncomfortBarChartArray.push(0)
            }
            if (this.AsleepArray[Sindex] !== undefined) {
              if (moment(this.AsleepArray[Sindex].createddate).format('YYYY/MM/DD') === moment(startOfWeek2).format('YYYY/MM/DD')) {
                var sec = (this.AsleepArray[Sindex].endtimeseconds - this.AsleepArray[Sindex].starttimeseconds) * 1000;
                var tmpvalue = Math.round(moment.duration(sec).asHours())
                this.AsleepBarChartArray.push(tmpvalue)
                Sindex = Sindex + 1;
                startOfWeek2 = moment(startOfWeek2).add(1, 'days');
              }
              else {
                this.AsleepBarChartArray.push(0)
                startOfWeek2 = moment(startOfWeek2).add(1, 'days');
              }

            }
            else {
              this.AsleepBarChartArray.push(0)
            }
            if (this.PainArray[Pindex] !== undefined) {
              if (moment(this.PainArray[Pindex].createddate).format('YYYY/MM/DD') === moment(startOfWeek3).format('YYYY/MM/DD')) {
                var sec = (this.PainArray[Pindex].endtimeseconds - this.PainArray[Pindex].starttimeseconds) * 1000;
                var tmpvalue = Math.round(moment.duration(sec).asHours())
                this.PainBarChartArray.push(tmpvalue)
                Pindex = Pindex + 1;
                startOfWeek3 = moment(startOfWeek3).add(1, 'days');
              }
              else {
                console.log("index", index)
                this.PainBarChartArray.push(0)
                console.log("sdfsdf")
                startOfWeek3 = moment(startOfWeek3).add(1, 'days');
              }
            }
            else {
              this.PainBarChartArray.push(0)
            }
            if (this.AwakeArray[Aindex] !== undefined) {
              if (moment(this.AwakeArray[Aindex].createddate).format('YYYY/MM/DD') === moment(startOfWeek4).format('YYYY/MM/DD')) {
                var sec = (this.AwakeArray[Aindex].endtimeseconds - this.AwakeArray[Aindex].starttimeseconds) * 1000;
                var tmpvalue = Math.round(moment.duration(sec).asHours())
                this.AwakeBarChartArray.push(tmpvalue)
                Aindex = Aindex + 1;
                console.log("index", Aindex)
                startOfWeek4 = moment(startOfWeek4).add(1, 'days');
              }
              else {
                console.log("index", Aindex)
                this.AwakeBarChartArray.push(0)
                console.log("sdfsdf")
                startOfWeek4 = moment(startOfWeek4).add(1, 'days');
              }
            }
            else {
              this.AwakeBarChartArray.push(0)
            }
          }

        }
        else {
          this.AwakeBarChartArray = [0, 0, 0, 0, 0, 0, 0];
          this.PainBarChartArray = [0, 0, 0, 0, 0, 0, 0];
          this.AsleepBarChartArray = [0, 0, 0, 0, 0, 0, 0];
          this.UncomfortBarChartArray = [0, 0, 0, 0, 0, 0, 0];
        }
        this.SleepChart();
        this.DistressChart();
        this.ComfortableChart();
        this.UncomfortableChart();
        this.DistressDoughnut(parseInt(this.DistressHours.split(":")[0]))
        this.SleepDoughnut(parseInt(this.SleepHours.split(":")[0]))
        this.ComfortableDoughnut(parseInt(this.ComfortHours.split(":")[0]))
        this.UnomfortableDoughnut(parseInt(this.UncomfortHours.split(":")[0]))
      }
    }, err => {
      this.loadingService.hide();
    });
  }
  hours(hh) {
    if (hh !== undefined)
      return hh.split(":")[0]; 
  }
  minutes(mm) {
    if (mm !== undefined)
      return mm.split(":")[1];
  }

  avg(hh) {
    if (hh !== undefined)
      return Math.ceil(hh.split(":")[0] / 7);

  }

  getDates(startDate, stopdate) {
    var dateArray = [];
    var currentDate = moment(startDate);
    var stopDate = moment(stopdate);
    while (currentDate <= stopDate) {
      dateArray.push(moment(currentDate).format('YYYY/MM/DD'))
      currentDate = moment(currentDate).add(1, 'days');
    }
    this.dateArray = dateArray;
  }

  diff(start, end) {
    start = start.split(":");
    end = end.split(":");
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);

    // If using time pickers with 24 hours format, add the below line get exact hours
    if (hours < 0)
      hours = hours + 24;

    return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
  }

  calulateTotalTime(timeArray) {
    const sum = timeArray.reduce((acc, time) => acc.add(moment.duration(time)), moment.duration());
    return [Math.floor(sum.asHours()), sum.minutes()].join(':')
  }


  getDiffer(chartData) {
    for (var i in chartData) {
      if (chartData[i].name == "Awake") {
        this.AwakeArray.push(chartData[i])
      }
      if (chartData[i].name == "Asleep") {
        this.AsleepArray.push(chartData[i])
      }
      if (chartData[i].name == "Pain") {
        this.PainArray.push(chartData[i])
      }
      if (chartData[i].name == "Uncomfortable") {
        this.UncomfortArray.push(chartData[i])
      }
    }
  }

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      // console.log("set string in storage: " + result);
      return true;
    } catch (reason) {
      // console.log(reason);
      return false;
    }
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      } else {
        return null;
      }
    } catch (reason) {
      return null;
    }
  }

  async getpatientType() {
    await this.get("patientType").then((result) => {
      if (result == "independent") {
        this.dependent = false;
      }
      this.appIntialize();
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }

}
