import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppMonthlyReportPage } from './app-monthly-report';

@NgModule({
  declarations: [
    AppMonthlyReportPage,
  ],
  imports: [
    IonicPageModule.forChild(AppMonthlyReportPage),
  ],
})
export class AppMonthlyReportPageModule {}
