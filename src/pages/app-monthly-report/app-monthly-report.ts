import { Component, ElementRef, ViewChild } from '@angular/core';
import { Events, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { Storage } from "@ionic/storage";
import { environment } from '../../environment/environment';
import { Http } from '@angular/http';
import { LoadingService } from '../../services/loading-service';
import { ToastService } from '../../services/toast-service';
import * as moment from 'moment';
const option = {
  legend: {
    display: false
  },
  scales: {
    xAxes: [{
      gridLines: {
        display: false
      }
    }],
    yAxes: [{
      display: false,
      gridLines: {
        display: false,
        drawBorder: false,
      }
    }],
  }
}
const dougnhutOption: any = {
  responsive: true,
  aspectRatio: 1,
  maintainAspectRatio: true,
  legend: {
    display: true,
    position: 'bottom',
    labels: {
      usePointStyle: true,

    }
  }
}
@IonicPage()
@Component({
  selector: 'page-app-monthly-report',
  templateUrl: 'app-monthly-report.html',
})
export class AppMonthlyReportPage {

  @ViewChild('lineChart') private chartRef: ElementRef;
  @ViewChild('barChart') private barchartRef: ElementRef;
  @ViewChild('lineChart1') private chartRef1: ElementRef;
  @ViewChild('barChart1') private barchartRef1: ElementRef;
  @ViewChild('lineChart2') private chartRef2: ElementRef;
  @ViewChild('barChart2') private barchartRef2: ElementRef;
  @ViewChild('lineChart3') private chartRef3: ElementRef;
  @ViewChild('barChart3') private barchartRef3: ElementRef;
  chart: any;
  barchart: any;
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  userLoginData: any;
  dependent: any;
  TotalSleep: number = 0;
  TotalDistress: number = 0;
  TotalComfort: number = 0;
  dateArray: any;
  TotalUncomfort: number = 0;
  SleepHours: any;
  DistressHours: any;
  ComfortHours: any;
  startOfWeek = moment(moment().startOf('week').format('YYYY/MM/DD'));
  UncomfortHours: any;
  AsleepBarChartArray: any = [];
  AwakeBarChartArray: any = [];
  PainBarChartArray: any = [];
  UncomfortBarChartArray: any = [];
  AwakeArray: any = [];
  AsleepArray: any = [];
  PainArray: any = [];
  UncomfortArray: any = [];
  weeKnumberArray: any = [];
  weeklyHour = 720;
  selectedDate: any = new Date();


  constructor(public events: Events, private toastService: ToastService, private loadingService: LoadingService, public http: Http, public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {

  }

  ionViewDidLoad() {
    this.getuserLoginData()
  }

  SleepDoughnut(shour) {
    var data = [this.weeklyHour, shour]
    this.chart = new Chart(this.chartRef.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Total', 'Consume'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(83,155,192)',
            'rgb(107,177,214)'

          ],
        }]
      },
      options: dougnhutOption
    });
  }
  DistressDoughnut(Dhour) {
    var data = [this.weeklyHour, Dhour]
    this.chart = new Chart(this.chartRef1.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Total', 'Consume'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(226,96,96)',
            'rgb(171,87,87)'

          ],
        }]
      },
      options: dougnhutOption
    });
  }
  UnomfortableDoughnut(Uhour) {
    var data = [this.weeklyHour, Uhour]
    this.chart = new Chart(this.chartRef2.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Total', 'Consume'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(250,208,0)',
            'rgb(201,169,13)'
          ],
        }]
      },
      options: dougnhutOption
    });
  }
  ComfortableDoughnut(Chour) {
    var data = [this.weeklyHour, Chour]
    this.chart = new Chart(this.chartRef3.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Total', 'Consume'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(175,219,7)',
            'rgb(151,187,12)'
          ],
        }]
      },
      options: dougnhutOption
    });
  }

  SleepChart() {
    this.barchart = new Chart(this.barchartRef.nativeElement, {
      type: 'bar',
      data: {
        labels: this.weeKnumberArray,
        datasets: [{

          barPercentage: 0.5,
          pointBorderColor: 'red',

          data: this.AsleepBarChartArray,
          backgroundColor: [
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)'
          ],
          borderColor: [
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
          ],
          borderWidth: 1,

        }, {
          data: this.AsleepBarChartArray,
          backgroundColor: [
            'rgb(107,177,214)',
          ],
          borderColor: [
            'rgb(107,177,214)',
          ],
          type: 'line',
          fill: false
        }

        ]
      },
      options: option
    });
  }

  DistressChart() {
    this.barchart = new Chart(this.barchartRef1.nativeElement, {
      type: 'bar',
      data: {
        labels: this.weeKnumberArray,
        datasets: [{
          barPercentage: 0.5,
          data: this.PainBarChartArray,
          backgroundColor: [
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
          ],
          borderColor: [
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
          ],
          borderWidth: 1
        }, {
          data: this.PainBarChartArray,
          backgroundColor: [
            'rgb(171,87,87)',
          ],
          borderColor: [
            'rgb(171,87,87)',
          ],
          type: 'line',
          fill: false
        }]
      },
      options: option
    });
  }

  ComfortableChart() {
    this.barchart = new Chart(this.barchartRef2.nativeElement, {
      type: 'bar',
      data: {
        labels: this.weeKnumberArray,
        datasets: [{
          barPercentage: 0.5,
          data: this.AwakeBarChartArray,
          backgroundColor: [
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
          ],
          borderColor: [
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
          ],
          borderWidth: 1
        },
        {
          data: this.AwakeBarChartArray,
          backgroundColor: [
            'rgb(201,169,13)',
          ],
          borderColor: [
            'rgb(201,169,13)',
          ],
          type: 'line',
          fill: false
        }]
      },
      options: option
    });
  }

  UncomfortableChart() {
    this.barchart = new Chart(this.barchartRef3.nativeElement, {
      type: 'bar',
      data: {
        labels: this.weeKnumberArray,
        datasets: [{
          barPercentage: 0.5,
          data: this.UncomfortBarChartArray,
          backgroundColor: [
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',

          ],
          borderColor: [
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
          ],
          borderWidth: 1
        },
        {
          data: this.UncomfortBarChartArray,
          backgroundColor: [
            'rgb(175,219,7)',
          ],
          borderColor: [
            'rgb(175,219,7)',
          ],
          type: 'line',
          fill: false
        }
        ]
      },
      options: option
    });
  }

  appIntialize() {
    var date = '01'
    var month = moment(this.selectedDate).format('MM-YYYY');
    console.log("month",month)
    var newDate = date + "-" + month;
    console.log("new dae",newDate)    
    var currentWeek = parseInt(moment(newDate, 'DD?MM/YYYY').format('w')) - 1;  
    console.log("new dae",currentWeek)    
    this.AsleepBarChartArray = [];
    this.AwakeBarChartArray = [];
    this.PainBarChartArray = [];
    this.UncomfortBarChartArray = [];
    this.AwakeArray = [];
    this.AsleepArray = [];
    this.PainArray = [];
    this.UncomfortArray = [];
    this.TotalSleep = 0;
    this.TotalDistress = 0;
    this.TotalComfort = 0;
    this.dateArray = [];
    this.weeKnumberArray=[];
    this.TotalUncomfort = 0;
    this.SleepHours = "0:0";
    this.DistressHours = "0:0";
    this.ComfortHours = "0:0";
    this.UncomfortHours = "0:0";
    this.loadingService.show();
    var data = {
      userid: this.userLoginData.id,
      appsecret: this.appsecret,
      startOfMonth: month,
      action: 'getmonthlygraphdata',
    }
    for (var i = 0; i < 4; i++) {
      var currentWeek = currentWeek + 1;
      this.weeKnumberArray.push("Week " + currentWeek);
    }
    this.http.post(this.baseurl + 'getgraphs.php', data).subscribe(data => {
      this.loadingService.hide();
      var data1 = JSON.parse(data['_body'])
      var chartData = data1.graphSumData;
      if (data1.status == "success") {
        console.log("startofmonahr", data1)
        if (chartData && chartData.length > 0) {
          this.getDiffer(chartData);
          var Aindex = 0
          var Pindex = 0
          var Uindex = 0
          var Sindex = 0
          for (let i = 1; i <= 5; i++) {
            if (this.UncomfortArray[Uindex] !== undefined) {
              if (i == parseInt(this.UncomfortArray[Uindex].week)) {
                var sec = (this.UncomfortArray[Uindex].endtimeseconds - this.UncomfortArray[Uindex].starttimeseconds) * 1000;
                var tmpvalue = Math.round(moment.duration(sec).asHours())
                this.UncomfortBarChartArray.push(tmpvalue)
                Uindex = Uindex + 1;
              }
              else {
                this.UncomfortBarChartArray.push(0)
              }
            }
            else {
              this.UncomfortBarChartArray.push(0)
            }
            if (this.AsleepArray[Sindex] !== undefined) {
              if (i == parseInt(this.AsleepArray[Sindex].week)) {
                var sec = (this.AsleepArray[Sindex].endtimeseconds - this.AsleepArray[Sindex].starttimeseconds) * 1000;
                var tmpvalue = Math.round(moment.duration(sec).asHours())
                this.AsleepBarChartArray.push(tmpvalue)
                Sindex = Sindex + 1;
              }
              else {
                this.AsleepBarChartArray.push(0)
              }

            }
            else {
              this.AsleepBarChartArray.push(0)
            }
            if (this.PainArray[Pindex] !== undefined) {
              if (i == parseInt(this.PainArray[Pindex].week)) {
                var sec = (this.PainArray[Pindex].endtimeseconds - this.PainArray[Pindex].starttimeseconds) * 1000;
                var tmpvalue = Math.round(moment.duration(sec).asHours())
                this.PainBarChartArray.push(tmpvalue)
                Pindex = Pindex + 1;
              }
              else {
                this.PainBarChartArray.push(0)
              }
            }
            else {
              this.PainBarChartArray.push(0)
            }
            if (this.AwakeArray[Aindex] !== undefined) {
              if (i == parseInt(this.AwakeArray[Aindex].week)) {
                var sec = (this.AwakeArray[Aindex].endtimeseconds - this.AwakeArray[Aindex].starttimeseconds) * 1000;
                var tmpvalue = Math.round(moment.duration(sec).asHours())
                this.AwakeBarChartArray.push(tmpvalue)
                Aindex = Aindex + 1;
              }
              else {
                this.AwakeBarChartArray.push(0)
              }
            }
            else {
              this.AwakeBarChartArray.push(0)
            }
          }

        }
        else {
          this.AwakeBarChartArray = [0, 0, 0, 0];
          this.PainBarChartArray = [0, 0, 0, 0];
          this.AsleepBarChartArray = [0, 0, 0, 0];
          this.UncomfortBarChartArray = [0, 0, 0, 0];
        }
        this.SleepChart();
        this.DistressChart();
        this.ComfortableChart();
        this.UncomfortableChart();
        this.DistressDoughnut(parseInt(this.DistressHours.split(":")[0]))
        this.SleepDoughnut(parseInt(this.SleepHours.split(":")[0]))
        this.ComfortableDoughnut(parseInt(this.ComfortHours.split(":")[0]))
        this.UnomfortableDoughnut(parseInt(this.UncomfortHours.split(":")[0]))
      }
    }, err => {
      this.loadingService.hide();
    });
  }


  getDiffer(chartData) {
    var DistressArray = [];
    var SleepArray = [];
    var UncomfortArray = [];
    var ComfortArray = [];
    for (var i in chartData) {

      if (chartData[i].name == "Awake") {
        this.AwakeArray.push(chartData[i])
        this.TotalComfort += parseInt(chartData[i].count_name);
        var startTime = moment(chartData[i].starttime).format("HH:mm")
        var endTime = moment(chartData[i].endtime).format("HH:mm")
        var diff = this.diff(startTime, endTime)
        ComfortArray.push(diff)
      }
      if (chartData[i].name == "Asleep") {
        this.AsleepArray.push(chartData[i])
        this.TotalSleep += parseInt(chartData[i].count_name);
        var startTime = moment(chartData[i].starttime).format("HH:mm")
        var endTime = moment(chartData[i].endtime).format("HH:mm")
        var diff = this.diff(startTime, endTime)
        SleepArray.push(diff)
      }
      if (chartData[i].name == "Pain") {
        this.PainArray.push(chartData[i])
        this.TotalDistress += parseInt(chartData[i].count_name);
        var startTime = moment(chartData[i].starttime).format("HH:mm")
        var endTime = moment(chartData[i].endtime).format("HH:mm")
        var diff = this.diff(startTime, endTime)
        DistressArray.push(diff)
      }
      if (chartData[i].name == "Uncomfortable") {
        this.UncomfortArray.push(chartData[i])
        this.TotalUncomfort += parseInt(chartData[i].count_name);
        var startTime = moment(chartData[i].starttime).format("HH:mm")
        var endTime = moment(chartData[i].endtime).format("HH:mm")
        var diff = this.diff(startTime, endTime)
        UncomfortArray.push(diff)

      }
    };
    this.DistressHours = this.calulateTotalTime(DistressArray);
    this.DistressDoughnut(parseInt(this.DistressHours.split(":")[0]))
    this.SleepHours = this.calulateTotalTime(SleepArray);
    this.SleepDoughnut(parseInt(this.SleepHours.split(":")[0]))
    this.ComfortHours = this.calulateTotalTime(ComfortArray);
    this.ComfortableDoughnut(parseInt(this.ComfortHours.split(":")[0]))
    this.UncomfortHours = this.calulateTotalTime(UncomfortArray);
    this.UnomfortableDoughnut(parseInt(this.UncomfortHours.split(":")[0]))
  }

  diff(start, end) {
    start = start.split(":");
    end = end.split(":");
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);

    // If using time pickers with 24 hours format, add the below line get exact hours
    if (hours < 0)
      hours = hours + 24;

    return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
  }

  hours(hh) {
    if (hh !== undefined)
      return hh.split(":")[0];
  }

  minutes(mm) {
    if (mm !== undefined)
      return mm.split(":")[1];
  }

  avg(hh) {
    if (hh !== undefined)
      return Math.ceil(hh.split(":")[0] / 4);

  }
  calulateTotalTime(timeArray) {
    const sum = timeArray.reduce((acc, time) => acc.add(moment.duration(time)), moment.duration());
    return [Math.floor(sum.asHours()), sum.minutes()].join(':')
  }

  getDates1() {
    this.events.subscribe('app:sendUser', logged => {
      this.selectedDate = logged;
      this.appIntialize();
    });
    this.events.publish('app:getUser');
  }

  ionViewWillEnter() {
    this.getDates1();
  }

  ionViewWillLeave() {
    this.events.unsubscribe('app:sendUser');
  }

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      return true;
    } catch (reason) {
      return false;
    }
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      } else {
        return null;
      }
    } catch (reason) {
      return null;
    }
  }

  async getpatientType() {
    await this.get("patientType").then((result) => {
      if (result == "independent") {
        this.dependent = false;
      }
      this.appIntialize();
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }

}
