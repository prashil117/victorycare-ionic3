import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {AppPatientProfilePage} from "./app-patient-profile";



@NgModule({
  declarations: [
      AppPatientProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(AppPatientProfilePage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppPatientProfileModule {}
