import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { AlertController, IonicPage, ModalController, NavController, NavParams } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import * as _ from "lodash";
import { Validators, FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { EmailValidator } from '../../app/directive/email-validator';
import { AppAddContactModalPage } from "../app-add-contact-modal/app-add-contact-modal";
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";

@IonicPage()
@Component({
  selector: 'app-patient-profile',
  templateUrl: 'app-patient-profile.html',
  providers: [Camera, DatabaseService]
})
export class AppPatientProfilePage implements AfterViewInit {
  newDoctors: any = "";
  Profileform: FormGroup;
  submitAttempt: boolean;
  data: any = {};
  diagnosiss: any = {};
  upload: any = {};   
  userLoginData: any;
  patientType: any;
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  logo: any = {};
  action: any = "";
  image: any = "";
  data1: any = {};
  data2: any = {};
  data3: any = "";
  data4: any = {};
  userTypeToBeAdded: string;
  selectType: any = "";
  addExisting: string;
  readonly: any = {};
  result: any = [];
  enableDropdown: any = "";
  enableDiv: any = "";
  hcpLoggedIn: boolean;
  userData: any = [];
  date: any = new Date();
  patientsData: any = [];
  SearchData: any = [];
  userdataid: string;
  passwordMsg: string;
  confirmPasswordMsg: string;
  ut: boolean;
  hcpid: any;

  @ViewChild('imageUploader') imageUploader: ElementRef;
  @ViewChild('addPhotoCaption') addPhotoCaption: ElementRef;
  @ViewChild('removePhotoIcon') removePhotoIcon: ElementRef;
  @ViewChild('previewImage') previewImage: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public formBuilder: FormBuilder, private nativeGeocoder: NativeGeocoder, public modalCtrl: ModalController, private alertCtrl: AlertController, private databaseService: DatabaseService,  private loadingService: LoadingService, private camera: Camera, public http: Http, private toastCtrl: ToastService) {
    console.log('patient profile');
    this.upload = "assets/images/upload1.svg";
    this.readonly = false;
    // this.usertype = this._cookieService.get('usertype');
    this.Profileform = this.formBuilder.group({
      usertype: [''],
      selectType: [''],
      storedDoctors: [''],
      // name: ['', Validators.required],
      firstname: ['', Validators.compose([Validators.required])],
      lastname: ['', Validators.compose([Validators.maxLength(15), Validators.minLength(3), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      birthDate: [''],
      mobile: ['', Validators.compose([Validators.required])],
      sex: [''],
      toppings: [''],
      languagePreferred: [''],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      address: [''],
      contacts: this.formBuilder.array([
        this.initContact(),
      ]),
      diagnosis: this.formBuilder.array([
        this.initDiagnosis(),
      ])
    }, { validator: this.matchingPasswords('password', 'confirmPassword') }
    );
  }

  intializeApp1() {
    this.ut = (this.userLoginData.usertype == 'healthcareprovider') ? true : false;
    if (this.ut == true) {
      this.Profileform = this.formBuilder.group({
        usertype: [''],
        selectType: [''],
        storedDoctors: [''],
        // name: ['', Validators.required],
        firstname: ['', Validators.compose([Validators.required])],
        lastname: ['', Validators.compose([Validators.maxLength(15), Validators.minLength(3), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
        email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
        birthDate: [''],
        mobile: ['', Validators.compose([Validators.required])],
        sex: [''],
        toppings: [''],
        languagePreferred: [''],
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required],
        address: [''],
        contacts: this.formBuilder.array([
          this.initContact(),
        ]),
        diagnosis: this.formBuilder.array([
          this.initDiagnosis(),
        ])
      }, { validator: this.matchingPasswords('password', 'confirmPassword') }
      );
    } else {
      this.Profileform = this.formBuilder.group({
        usertype: [''],
        selectType: [''],
        storedDoctors: [''],
        // name: ['', Validators.required],
        firstname: ['', Validators.compose([Validators.required])],
        lastname: ['', Validators.compose([Validators.maxLength(15), Validators.minLength(3), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
        email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
        birthDate: [''],
        mobile: ['', Validators.compose([Validators.required])],
        sex: [''],
        toppings: [''],
        languagePreferred: [''],
        address: [''],
        password: [''],
        confirmPassword: [''],
        contacts: this.formBuilder.array([
          this.initContact(),
        ]),
        diagnosis: this.formBuilder.array([
          this.initDiagnosis(),
        ])
      });
    }
    this.data.usertype = (this.data.usertype == undefined ? "patient" : this.data.usertype);
    this.data.address = (this.data.address == undefined ? "" : this.data.address);
    this.data.birthDate = (this.data.birthDate == undefined ? "" : this.data.birthDate);
    this.data.mobile = (this.data.mobile == undefined ? "" : this.data.mobile);
    this.data.sex = (this.data.sex == undefined ? "" : this.data.sex);
    this.data.image = (this.data.image == undefined ? "" : this.data.image);
    this.data.languagePreferred = (this.data.languagePreferred == undefined ? "" : this.data.languagePreferred);
    this.enableDiv = false;
    console.log("asdfghjklqwertyuiopzxcvbnm,", this.navParams.data);
    this.data3 = this.navParams.data.doctorsdata;
    this.patientsData = this.navParams.data.patientData;
    this.userData = this.navParams.data.userData;
    this.addExisting = this.navParams.data.addExisting;
    this.userTypeToBeAdded = this.navParams.data.userTypeToBeAdded;
    if (this.userTypeToBeAdded == undefined) {
      this.userTypeToBeAdded = this.userLoginData.usertype;
    }
    this.hcpLoggedIn = (this.userTypeToBeAdded == 'healthcareprovider' ? true : false);

    console.log(this.navParams.data);
    if (!_.isEmpty(this.navParams.data.data)) {
      this.data = this.navParams.data.data;
      if (!_.isEmpty(this.data)) {
        // this.data = navParams.data.data;
        this.action = this.navParams.data.action;
        if (this.action == 'edit') {

          console.log('contacts', this.data.diagnosis);
          this.data.address = (this.data.address == undefined ? "" : this.data.address);
          console.log(this.data);
          if (this.data.diagnosis !== undefined) {
            for (let i = 0; i < this.data.diagnosis.length; i++) {
              const control = <FormArray>this.Profileform.controls['diagnosis'];
              control.push(this.initDiagnosis());
            }
          }
          else {
            this.data.diagnosis = [];
          }
          if (this.data.contacts !== undefined) {
            for (let i = 0; i < this.data.contacts.length; i++) {
              const control = <FormArray>this.Profileform.controls['contacts'];
              control.push(this.initContact());
            }
          }
          else {
            this.data.contacts = [];
          }
        }
      }
    }
    this.getDoctors(this.patientsData);
  }
  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          confirmPassword: true
        };
      }
    }
  }

  async getHCPid() {

    await this.get('currentHCP').then(result => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      } else {
        this.getuserLoginData();
      }
    });
  }
  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
          console.log("this.hcpid else", this.hcpid);
        }
        this.getpatientType();
      }
    });
  }
  async getpatientType() {

    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh('');
      }
      else {
        this.patientType = result;
        this.intializeApp1()
        // console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');
    setTimeout(() => {
      console.log('Async operation has ended');
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }

  getDoctors(patientsData) {
    this.loadingService.show();
    // console.log("patintDoctors", patientsData);
    // console.log("doctonursedata", this.navParams.data);
    // console.log(patientsData);
    var keys: any = [];
    var result1: any = [];
    let self = this;
    self.data.action = "getdropdownpatientdata";
    self.data.id = this.userLoginData.id;
    self.data.parentid = this.hcpid ? this.hcpid : this.userLoginData.id;
    self.data.getdropdownpatientdata = patientsData;
    self.data.appsecret = this.appsecret;
    // console.log(patientsData);
    self.http.post(this.baseurl + "getdropdowndata.php", self.data).subscribe(data => {

      let result = JSON.parse(data["_body"]);
      console.log(result);
      if (result.status == 'success') {
        this.loadingService.hide();
        console.log("dssfsfsdfdfsdaabcdefghjijklmno", result.data);
        // result.data;
        result1 = result1.push(result.data); //[result.data];
        this.data4.newDoctors = result.data;
        console.log(result1);
        // return result1;
      } else {
        this.loadingService.hide();
        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
      }
    }, err => {
      this.loadingService.hide();
      console.log(err);
    });
    // console.log(result1);
    // this.get('allPatientsData').then((val) => {
    //   console.log(val);
    //   result.push(val);
    // });
    // this.remove('allPatientsData');
    return result1;
  }
  comparer(otherArray) {
    return function (current) {
      // console.log(current);
      return otherArray.filter(function (other) {
        // console.log(other);
        return other.email == current.email
        // return other == current
      }).length == 0;
    }
  }
  checkState(email) {
    console.log(email);
    this.loadingService.show();
    this.data.action = "getuserbyid";
    this.data.id = email;
    this.data.appsecret = this.appsecret;
    console.log(this.data);
    this.http.post(this.baseurl + "login.php", this.data).subscribe(snapshot => {
      let result = JSON.parse(snapshot["_body"]);
      // var snapshot = snapshot.val();
      if (result.status == 'success') {
        this.data = result.data;
        // this.data.name = result.data.firstname+' '+result.data.lastname;
        this.enableDropdown = false;
        this.enableDiv = true;
        this.loadingService.hide();
      } else {
        this.loadingService.hide();
        this.toastCtrl.presentToast("Unable to fetch data");
        // self.errorMessage = "Unable to fetch data";
      }
    });
  }
  addType($event) {
    this.loadingService.show();
    console.log($event);
    if ($event == 'new') {
      this.doRefresh($event);

      // this.enableDropdown = "display:none !important;";
      // this.enableDiv = "display:block !important;";
      this.enableDropdown = false;
      this.enableDiv = false;
      this.selectType = 'addnew';
      this.data.selectType = false;
      this.loadingService.hide();
    } else if ($event == 'exist') {
      // this.doRefresh($event);
      this.enableDropdown = true;
      this.enableDiv = true;
      this.selectType = 'addexist';
      this.data.storedDoctors = false;
      this.loadingService.hide();
    }
  }
  setInitialDiagnosis() {
    if (this.data.diagnosis === undefined) {
      this.data.diagnosis = [];
    }

    if (this.data.diagnosis.length === 0) {
      this.data.diagnosis.push({});
    }
  }

  addDiagnosis() {
    console.log('diagnosis', this.data.diagnosis);
    const control = <FormArray>this.Profileform.controls['diagnosis'];
    control.push(this.initDiagnosis());
    this.data.diagnosis.push({ name: '' });
  }

  logout() {
    this.databaseService.logout();
  }
  initDiagnosis() {
    return this.formBuilder.group({
      name: ['']
    });
  }

  initContact() {
    return this.formBuilder.group({
      name: [''],
      phone: ['']
    });
  }

  deleteDiagnosis(index) {
    this.data.diagnosis.splice(index, 1);
  }

  addContact() {
    let modal = this.modalCtrl.create(AppAddContactModalPage, { 'contacts': this.data.contacts });
    modal.present();
    modal.onDidDismiss(data => {
      this.getLetLong();
    });
    // this.data.contacts.push({});
  }

  deleteContact(index) {
    this.data.contacts.splice(index, 1);
  }


  ngAfterViewInit(): void {
    this.setInitialDiagnosis();
    if (this.data.contacts === undefined) {
      this.data.contacts = [];
    }
    if (!_.isEmpty(this.action)) {
      this.showImagePreview();

      if (this.action === "view") {
        this.readonly = true;
      }
    }
    this.getHCPid();

  }

  getLetLong() {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    const control = <FormArray>this.Profileform.controls['contacts'];
    control.push(this.initContact());
    let self = this;
    console.log('contacts', self.data.contacts);
    for (let i = 0; i < self.data.contacts.length; i++) {
      self.nativeGeocoder.forwardGeocode(self.data.contacts[i].address, options)
        .then((coordinates: NativeGeocoderForwardResult[]) => {
          self.data.contacts[i].lat = coordinates[0].latitude;
          self.data.contacts[i].long = coordinates[0].longitude;
        }).catch((error: any) => console.log(error));
    }
  }

  addPatientDetails(thirdPartyData) {
    console.log(this.data);
    this.data.base_url = this.baseurl;
    let self = this;
    this.submitAttempt = true;

    if (!this.Profileform.valid) {
      console.log("Profileform Validation Fire!");
    }
    else {
      // console.log('this.data',this.data);
      this.Profileform.value.id = this.data.id;
      if (this.data.image != undefined) {
        this.data.image = this.data.image;
      }
      else {
        this.showImagePreview();
        this.data.image = "";
      }
      if (this.action === 'edit') {
      } else {
        var hcp: any = [];
        var doctors: any = [];
        console.log(this.userTypeToBeAdded);
        // self.loadingService.show();
        // self.data.action = (this._cookieService.get('usertype') == 'healthcareprovider') ? "insert" : ""
        if (this.userLoginData.usertype == 'healthcareprovider' && !this.navParams.data.doctorsdata) {
          self.data.action = "insert";
          self.data.url = "register.php";
        } else {
          self.data.action = "addpatienttodoctor";
          self.data.url = "insertrelationdata.php";
        }

        self.data.doctorid = this.navParams.data.doctorsdata ? this.navParams.data.doctorsdata.userid : this.userLoginData.id;
        self.data.usertype = 'patient';
        self.data.appsecret = this.appsecret;
        self.data.firsttimelogin = 0;
        self.data.base_url = this.baseurl;
        console.log("data", this.data);
        self.http.post(this.baseurl + self.data.url, self.data).subscribe(data => {
          this.loadingService.show();
          let result = JSON.parse(data["_body"]);
          console.log(result);
          if (result.status == 'success') {
            self.loadingService.hide();
            self.toastCtrl.presentToast("Patient added successfully");
            if (this.userLoginData.usertype == "healthcareprovider" && !this.navParams.data.doctorsdata) {
              self.navCtrl.setRoot("AppMyPatientsPage");
            }
            else if (this.navParams.data.doctorsdata.usertype == "nurse") {
              self.navCtrl.setRoot("AppMyNursesPage");
            }
            else if (this.navParams.data.doctorsdata.usertype == "doctor") {
              self.navCtrl.setRoot("AppMyDoctorsPage");
            }
          } else if (result.status == 'errorEmail') {
            self.loadingService.hide();
            self.navCtrl.setRoot("AppMyPatientsPage");
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
          } else {
            self.loadingService.hide();
            self.navCtrl.setRoot("AppMyPatientsPage");
          }
        }, err => {
          this.loadingService.hide();
          console.log(err);
        });
      }
    }
  }


  ImageUpload() {
    let alert = this.alertCtrl.create({
      // title: 'Image Upload',
      message: 'Select Image source',
      buttons: [
        {
          text: 'Upload from Library',
          handler: () => {
            this.gallery(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.gallery(this.camera.PictureSourceType.CAMERA);
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }


  gallery(sourceType: PictureSourceType) {
    let self = this;
    this.camera.getPicture({
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      correctOrientation: true,
      allowEdit: true
    }).then((imageData) => {
      // imageData is a base64 encoded string
      self.showImagePreview();
      self.data.image = "data:image/jpeg;base64," + imageData;
      self.image = "data:image/jpeg;base64," + imageData;

    }, (err) => {
      console.log("Error " + err);
    });
  }

  showImagePreview() {
    let self = this;
    self.setVisibility(self.previewImage, "block");
    self.setVisibility(self.imageUploader, "none");
    self.setVisibility(self.addPhotoCaption, "none");
    self.setVisibility(self.removePhotoIcon, "block");
  }

  removeImage() {
    let self = this;
    this.data.image = undefined;
    self.setVisibility(self.previewImage, "none");
    self.setVisibility(self.imageUploader, "block");
    self.setVisibility(self.addPhotoCaption, "block");
    self.setVisibility(self.removePhotoIcon, "none");

  }

  setVisibility(element: ElementRef, state) {
    if (element !== undefined)
      element.nativeElement.style.display = state;
  }

  getDate() {
    let date = new Date(this.date);
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    // return date.getDate();
  }

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log('set string in storage: ' + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log('storageGET: ' + key + ': ' + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }
  // remove a single key value:
  remove(key: string) {
    this.storage.remove(key);
  }

}
