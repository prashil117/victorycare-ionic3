import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Camera, PictureSourceType } from "@ionic-native/camera";
import * as _ from "lodash";


@IonicPage()
@Component({
    selector: 'app-patient-details',
    templateUrl: 'app-patient-details.html',
    providers: [Camera, DatabaseService]
})
export class AppPatientDetailsPage implements AfterViewInit {

    data: any = {};
    upload: any = {};
    logo: any = {};
    action: any = "";
    image: any = "";
    readonly: any = {};
    @ViewChild('imageUploader') imageUploader: ElementRef;
    @ViewChild('addPhotoCaption') addPhotoCaption: ElementRef;
    @ViewChild('removePhotoIcon') removePhotoIcon: ElementRef;
    @ViewChild('previewImage') previewImage: ElementRef;

    constructor(public navCtrl: NavController,
        navParams: NavParams,
        private alertCtrl: AlertController,
        private databaseService: DatabaseService,
        private loadingService: LoadingService,
        private camera: Camera) {
        this.logo = "assets/images/logo/victory.png";
        this.upload = "assets/images/upload1.svg";
        this.readonly = false;
        console.log('details')
        if (!_.isEmpty(navParams.data)) {
            this.data = navParams.data.data;
            this.action = navParams.data.action;
        }
    }
    ngAfterViewInit(): void {
        if (!_.isEmpty(this.action)) {
            this.showImagePreview();

            if (this.action === "view") {
                this.readonly = true;
            }
        }

    }


    editPatientDetails() {
        this.action = 'edit';
        this.readonly = false;
    }
    ImageUpload() {
        let alert = this.alertCtrl.create({
            // title: 'Image Upload',
            message: 'Select Image source',
            buttons: [
                {
                    text: 'Upload from Library',
                    handler: () => {
                        this.gallery(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: () => {
                        this.gallery(this.camera.PictureSourceType.CAMERA);
                    }
                }, {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                    }
                }
            ]
        });
        alert.present();
    }


    gallery(sourceType: PictureSourceType) {
        let self = this;
        this.camera.getPicture({
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: sourceType,
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            correctOrientation: true,
            allowEdit: true
        }).then((imageData) => {
            // imageData is a base64 encoded string
            self.showImagePreview();
            self.data.image = "data:image/jpeg;base64," + imageData;
            self.image = "data:image/jpeg;base64," + imageData;

        }, (err) => {
            console.log("Error " + err);
        });
    }

    showImagePreview() {
        let self = this;
        self.setVisibility(self.previewImage, "block");
        self.setVisibility(self.imageUploader, "none");
        self.setVisibility(self.addPhotoCaption, "none");
        self.setVisibility(self.removePhotoIcon, "block");
    }

    removeImage() {
        let self = this;
        this.data.image = undefined;
        self.setVisibility(self.previewImage, "none");
        self.setVisibility(self.imageUploader, "block");
        self.setVisibility(self.addPhotoCaption, "block");
        self.setVisibility(self.removePhotoIcon, "none");

    }

    setVisibility(element: ElementRef, state) {
        if (element !== undefined)
            element.nativeElement.style.display = state;
    }

}