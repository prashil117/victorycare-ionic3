import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppPatientDetailsPage} from "./app-patient-details";


@NgModule({
  declarations: [
      AppPatientDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(AppPatientDetailsPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppPatientDetailsModule {}
