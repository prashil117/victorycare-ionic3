import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppNursesPage } from './app-nurses';

@NgModule({
  declarations: [
    AppNursesPage,
  ],
  imports: [
    IonicPageModule.forChild(AppNursesPage),
  ],
})
export class AppNursesPageModule {}
