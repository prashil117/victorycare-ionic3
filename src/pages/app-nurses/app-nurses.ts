import { Component, AfterViewInit } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { ToastService } from "../../services/toast-service";
import { Http } from '@angular/http';
import { environment } from "../../environment/environment";

/**
 * Generated class for the AppNursesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-nurses',
  templateUrl: 'app-nurses.html',
  providers: [DatabaseService]
})
export class AppNursesPage {
  data: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  eventsData: any = [];
  patientData: any = {};
  SearchData1: any = [];
  terms: string;
  usertype: any = "";
  hcpLoggedIn: boolean;
  result: any = [];
  userLoginData: any;
  patientType: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, public storage: Storage, private alertCtrl: AlertController, private databaseService: DatabaseService, private loadingService: LoadingService, private toastCtrl: ToastService, public http: Http) {


  }
  getUsersbyStoredEmail() {
    var varNur = [];
    var test = this.storage.get('currentHCP').then((val) => {
      varNur.push(val);
    });
    console.log(varNur);
    return varNur;
  }

  ionViewDidLoad() {
    // this.title = this._cookieService.get('username');
    console.log('ionViewDidLoad AppNursesPage');
    this.getuserLoginData();
  }

  logout() {
    this.databaseService.logout();
  }
  onSelectedPatient(item, hcpLoggedIn, userTypeToBeAdded) {
    console.log(item)
    this.navCtrl.push("AppPatientsPage", { 'data': item, 'hcpLoggedIn': hcpLoggedIn, 'userTypeToBeAdded': userTypeToBeAdded });
  }
  getusersdetails() {
    let self = this;
    this.usertype = this.userLoginData.usertype ? this.userLoginData.usertype : '';
    var webServiceData: any = {};

    if (this.userLoginData.usertype == 'healthcareprovider') {
      webServiceData.action = "getpatientsfromhcplogin";
      webServiceData.usertype = "nurse";
      webServiceData.parentid = this.userLoginData.id;
      webServiceData.appsecret = this.appsecret;
      console.log(webServiceData);
      self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
        this.loadingService.show();
        let result = JSON.parse(data["_body"]);
        console.log(result);
        if (result.status == 'success') {
          this.loadingService.hide();
          console.log(result.data);
          self.SearchData1 = result.data;
          self.data.values = result.data;
        } else {
          this.loadingService.hide();
          // this.toastCtrl.presentToast("No data found");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      }, err => {
        this.loadingService.hide();
        console.log(err);
      });
    }
    else {
      webServiceData.action = "getpatientsfromhcpdata";
      self.storage.get('currentHCP').then((val) => {
        webServiceData.parentid = val;
        webServiceData.usertype = "nurse";
        webServiceData.userid = this.userLoginData.id;
        webServiceData.appsecret = this.appsecret;
        console.log(webServiceData);
        self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
          this.loadingService.show();
          let result = JSON.parse(data["_body"]);
          console.log(result);
          if (result.status == 'success') {
            this.loadingService.hide();
            console.log(result.data);
            self.SearchData1 = result.data;
            self.data.values = result.data;
          } else {
            this.loadingService.hide();
            // this.toastCtrl.presentToast("No data found");
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
          }
        }, err => {
          this.loadingService.hide();
          console.log(err);
        });
      });
    }
  }

  setFilteredPatientsData() {
    if (this.data.values != undefined) {
      this.SearchData1 = this.data.values.filter((Patient) => {
        return Patient.firstname.toLowerCase().indexOf(this.terms.toLowerCase()) > -1;
      });
    } else {
      this.toastCtrl.presentToast("There is no data found...");
    }
  }
  onSelectedDoctor(item) {
    // this.storage.set('user', item);
    console.log(item);
    let self = this;
    if (item !== null) {
      self.eventsData = item;
      this.navCtrl.setRoot("AppTimelinePage", { 'data': item });
    }
    else {
      self.eventsData = item;
      this.navCtrl.setRoot("AppTimelinePage", { 'data': item });
    }
  }

  editNurse(data, editable) {
    console.log(data);
    editable = (this.userLoginData.usertype == "healthcareprovider") ? true : false;
    console.log(editable);
    this.navCtrl.push("AppNurseEditPage", { 'data': data, 'action': 'edit', 'editable': editable, 'savebutton': editable });
  }

  deleteNurseData(item) {
    console.log(item);
    var item1: any = [];
    item1 = [item];
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Do you want to de-link this record?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            let self = this;

            console.log('Delete Ok clicked');
            this.data.action = "unlinkhcpfromdoctor";
            this.data.parentid = this.userLoginData.id;
            this.data.userid = item.userid;
            this.data.appsecret = this.appsecret;
            console.log(this.data);
            this.http.post(this.baseurl + "removeuser.php", this.data).subscribe(data => {
              this.loadingService.show();
              let result = JSON.parse(data["_body"]);
              console.log(result.status);
              if (result.status == 'success') {
                this.loadingService.hide();  
                // self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
                self.toastCtrl.presentToast("Nurse is unlinked successfully");
                this.navCtrl.setRoot(this.navCtrl.getActive().component);
                // self.logout();
              } else {
                this.loadingService.hide();
                self.toastCtrl.presentToast("There is an error unlinking Nurse!");
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
              }
            }, err => {
              this.loadingService.hide();
              console.log(err);
            });
          }
        }
      ]
    });
    alert.present();
  }

  comparer(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        return other.email == current.email // && other.name == current.name
        // return other == current
      }).length == 0;
    }
  }

  onAddNurse(data, editable) {
    editable = (this.userLoginData.usertype == 'healthcareprovider') ? true : false;
    this.navCtrl.push("AppNurseEditPage", { 'nurseData': data, 'action': 'edit', 'editable': editable, 'savebutton': editable });
  }
  // set a key/value
  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log('set string in storage: ' + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log('storageGET: ' + key + ': ' + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }
  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh('');
      }
      else {
        this.patientType = result;
        this.getusersdetails();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }
  // remove a single key value:
  remove(key: string) {
    this.storage.remove(key);
  }

}
