import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoadingService } from '../../services/loading-service';
import { MyApp } from '../../app/app.component';
/**
 * Generated class for the AppLogoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-logout',
  templateUrl: 'app-logout.html',
})
export class AppLogoutPage {

  constructor(public loadingservice: LoadingService, public navCtrl: NavController, public storage: Storage, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppLogoutPage');
  }
  closeModal() {
    this.viewCtrl.dismiss(false);
  }
  onLogout() {
    this.viewCtrl.dismiss(true);
  }
}
