import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppLogoutPage } from './app-logout';

@NgModule({
  declarations: [
    AppLogoutPage,
  ],
  imports: [
    IonicPageModule.forChild(AppLogoutPage),
  ],
})
export class AppLogoutPageModule {}
