import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup, FormControl} from '@angular/forms';
import * as _ from "lodash";

/**
 * Generated class for the AddcontectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addcontect',
  templateUrl: 'addcontect.html'
})
export class AddcontectPage {
  data:any = [];
  contacts: any = [];
  contactType:any;
  ContactTitle:any;
  Contectform: FormGroup;
  submitAttempt: boolean;
  patientData: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public formBuilder: FormBuilder) {
        this.Contectform = formBuilder.group({
          name: ['', Validators.required],
          relationship: ['', Validators.required],
          mobile: ['', Validators.required],
          notes: [''],
          address: ['']     
      
        });
        if(!_.isEmpty(navParams.data)){
          this.contacts = navParams.data.contacts;
          this.contactType = navParams.data.type;
      }
      else{
          this.contactType = "";   
      }
      this.ContactTitle = navParams.data.type == "masterdata" ? 'Add Contact' : 'Add Next of Kin';
      this.patientData = navParams.data.patientData;
      this.contacts = navParams.data.contectdata;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddcontectPage');
  }

    closeModal(){
      this.viewCtrl.dismiss();
  }

  save(){
    this.submitAttempt = true;
  }

}
