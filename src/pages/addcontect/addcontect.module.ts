import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddcontectPage } from './addcontect';

@NgModule({
  declarations: [
    AddcontectPage,
  ],
  imports: [
    IonicPageModule.forChild(AddcontectPage),
  ],
})
export class AddcontectPageModule {}
