import { Component, AfterViewInit } from '@angular/core';
import { AlertController, IonicPage, ModalController, NavController, NavParams, MenuController } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { Validators, FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { MyApp } from "../../app/app.component";
import { Http } from '@angular/http';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
import { AppModalPage } from "../app-modal/app-modal";

/**
 * Generated class for the AppMyNursesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-my-nurses',
  templateUrl: 'app-my-nurses.html',
  providers: [DatabaseService]
})
export class AppMyNursesPage {
  SelectList: FormGroup;
  currentHCP: any[];
  data: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  eventsData: any = [];
  patientData: any = {};
  SearchData: any = [];
  terms: string;
  nurses: any = [];
  result: any = [];
  noDelete: boolean = false;
  userLoginData: any;
  patientType: any;
  usertype: string;
  username: string;
  userid: string;
  ut: boolean;
  hcpid: any;
  dropdown: any = [];
  notselected: boolean;
  selected1: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, private menu: MenuController, public storage: Storage,  private alertCtrl: AlertController, private databaseService: DatabaseService, private loadingService: LoadingService, public formBuilder: FormBuilder, public myApp: MyApp, public http: Http, private toastCtrl: ToastService) {
    ////////// HCP DropDown Code ends /////////////
    this.SelectList = formBuilder.group({
      currentHCP: ['']
    });
  }

  appIntialize1() {
    this.noDelete = this.userLoginData.usertype == 'patient' ? false : true;

    ////////// HCP DropDown Code Starts /////////////

    this.myApp.initializeApp();
    this.username = this.userLoginData.email;
    this.usertype = this.userLoginData.usertype;
    this.userid = this.userLoginData.id;
    this.ut = (this.usertype == 'healthcareprovider' ? false : true);

    console.log(this.userid);
    // alert(this.usertype);
    this.dropdown = this.getHCPData(this.userid, this.usertype);
    console.log(this.dropdown);
    var result = null;
    this.get('currentHCP').then(result => {
      console.log('Username1: ' + result);
      if (result != null) {
        this.notselected = false;
        this.selected1 = result;
        console.log('Username: ' + this.selected1);
      } else {
        this.notselected = true;
        console.log(this.dropdown);
        this.selected1 = this.dropdown;
      }
    });
    this.getpatientsfromhcpdata()
  }

  ionViewDidLoad() {
    this.getHCPid();
    console.log('ionViewDidLoad AppMyNursesPage');
  }
  logout() {
    this.databaseService.logout();
  }
  getpatientsfromhcpdata() {
    this.loadingService.show();
    let self = this;
    this.usertype = this.userLoginData ? this.userLoginData.usertype : '';
    var webServiceData: any = {};
    if (this.userLoginData.usertype == "healthcareprovider" || this.userLoginData.usertype == "patient") {
      webServiceData.action = "getpatientsfromhcpdata";
    }
    else if (this.userLoginData.usertype == "nurse" || this.userLoginData.usertype == "doctor") {
      webServiceData.action = "getpatientsfromhcpanddoctornurselogin";
    }
    console.log('show data', webServiceData.action);
    self.storage.get('currentHCP').then((val) => {
      if (val != null || val != undefined) {
        webServiceData.parentid = val;
        webServiceData.usertype = "nurse";
        webServiceData.userid = this.usertype == 'patient' ? this.data.id : this.userLoginData.id;
        webServiceData.loggedinusertype = this.usertype;
        webServiceData.appsecret = this.appsecret
        console.log(webServiceData);
        self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
          let result = JSON.parse(data["_body"]);
          console.log(result);
          if (result.status == 'success') {
            this.loadingService.hide();
            console.log(result.data);
            self.SearchData = result.data;
            self.data.values = result.data;
          } else {
            this.loadingService.hide();

            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
          }
        }, err => {
          this.loadingService.hide();
          console.log(err);
        });
      }
      else {
        this.loadingService.hide();
        let modal = self.modalCtrl.create(AppModalPage, { 'title': 'Please selct HCP', 'description': 'Healthcare provider is not selected, please select healthcare provider from dropdown' }, { cssClass: "my-modal" });
        modal.present();
        // this.toastCtrl.presentToast("Healthcare provider is not selected, please select healthcare provider from dropdown");
      }
    });
  }

  onSelectedPatient(item) {
    console.log(item)
    this.navCtrl.push("AppPatientsPage", { 'data': item });
  }
  setFilteredPatientsData() {
    this.SearchData = this.data.values.filter((Patient) => {
      console.log(Patient);
      return Patient.firstname.toLowerCase().indexOf(this.terms.toLowerCase()) > -1;
    });
  }

  editnurse(data, editable) {
    console.log(data);
    editable = (this.userLoginData.usertype == "healthcareprovider") ? true : false;
    console.log(editable);
    this.navCtrl.push("AppNurseEditPage", { 'data': data, 'action': 'edit', 'editable': editable });
  }
  deletenursesData(item) {
    var item1: any = [];
    item1 = [item];

    // var hcp:any = [];
    // let self = this;
    // hcp = this.nurses;
    // var onlyInA = hcp.filter(self.comparer(item1));
    // var onlyInB = item1.filter(self.comparer(hcp));
    // self.result = onlyInB.concat(onlyInA);
    // console.log(self.result);
    // return false;

    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Do you want to de-link from this Nurse?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            console.log('Delete Ok clicked');
            let self = this;

            this.data.action = "unlinkhcpfromdoctor";
            this.data.appsecret = this.appsecret
            if (this.userLoginData.usertype == 'patient') {
              this.data.parentid = item.parentid;
              this.data.userid = this.userLoginData.id;
            } else {
              this.data.parentid = this.userLoginData.id;
              this.data.userid = item.userid;
            }
            console.log(this.data);
            this.http.post(this.baseurl + "removeuser.php", this.data).subscribe(data => {
              let result = JSON.parse(data["_body"]);
              console.log(result.status);
              if (result.status == 'success') {
                // self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
                self.toastCtrl.presentToast("Nurse is unlinked successfully");
                this.navCtrl.setRoot(this.navCtrl.getActive().component);
                // self.logout();
              } else {
                self.toastCtrl.presentToast("There is an error unlinking nurse!");
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
              }
            }, err => {
              console.log(err);
            });
          }
        }
      ]
    });
    alert.present();
  }
  comparer(otherArray) {
    console.log(otherArray);
    return function (current) {
      console.log(current);
      return otherArray.filter(function (other) {
        console.log(other);
        return other.email == current.email && other.name == current.name
        // return other == current
      }).length == 0;
    }
  }
  onAddNurse(data, myNurse) {
    this.navCtrl.push("AppNurseEditPage", { 'nurseData': data, 'myNurse': myNurse, 'savebutton': true });
  }
  ////////// HCP DropDown Code Starts /////////////
  getHCPData(userid, usertype) {
    console.log(userid);
    console.log(usertype);
    var keys: any = [];
    var hcp1: any = [];
    let self = this;
    var webServiceData: any = {};
    webServiceData.action = "gethcpdata";
    webServiceData.id = userid;
    webServiceData.usertype = usertype;
    webServiceData.appsecret = this.appsecret
    self.data.id = this.userLoginData.id;
    self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
      let result = JSON.parse(data["_body"]);
      console.log(result);
      if (result.status == 'success') {
        console.log(result.data);
        // result.data;
        hcp1 = hcp1.push(result.data);
        // hcp1 = result.data;
        console.log(hcp1);
        // return result1;
      } else {
        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
      }
    }, err => {
      console.log(err);
    });
    return hcp1;
    
  }
  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log('set string in storage: ' + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log('storageGET: ' + key + ': ' + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }
  // remove a single key value:
  remove(key: string) {
    this.storage.remove(key);
  }
  doRefresh(event = '') {
    this.loadingService.show();
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.loadingService.hide();
    }, 2000);
  }
  selectEmployee($event) {
    this.doRefresh();
    this.set('currentHCP', $event);
  }

  async getHCPid() {
    await this.get('currentHCP').then(result => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      } else {
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
        }
        this.getpatientType();
      }
    });
  }


  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh();
      }
      else {
        this.patientType = result;
        this.appIntialize1();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }
  ////////// HCP DropDown Code ends /////////////
}
