import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppMyNursesPage } from './app-my-nurses'

@NgModule({
  declarations: [
    AppMyNursesPage,
  ],
  imports: [
    IonicPageModule.forChild(AppMyNursesPage),
  ],
})
export class AppMyNursesPageModule { }
