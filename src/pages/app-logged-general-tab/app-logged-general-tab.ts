import {Component, AfterViewInit} from '@angular/core';
import {
    AlertController, IonicPage, ModalController, NavController, NavParams, Platform,
    ViewController
} from 'ionic-angular';
import {DatabaseService} from "../../services/database-service";
import {LoadingService} from "../../services/loading-service";
import {Observable} from 'rxjs/Observable';
import * as _ from "lodash";
import {AppSharedService} from "../../services/app-shared-service";


@IonicPage()
@Component({
    selector: 'app-logged-general-tab',
    templateUrl: 'app-logged-general-tab.html',
    providers: [DatabaseService]
})
export class AppLoggedGeneralTabPage {


    patientData: any = {};
    patientSettings: any = {};
    data: any = {};
    selectedDate: any = {};
    selectedTime: any = {};
    readOnly:any = true;
    eventStartDate:any = {};
    eventEndDate:any = {};

    constructor(public navCtrl: NavController, navParams: NavParams, private alertCtrl: AlertController, private databaseService: DatabaseService, private loadingService: LoadingService,public modalCtrl: ModalController, public appSharedService: AppSharedService) {

        let self = this;
        if(navParams.data !== undefined){
            this.selectedDate = navParams.data.selectedDate;
            this.selectedTime = navParams.data.selectedTime;
        }
        if(this.appSharedService.patientData !== undefined){
            this.patientData = this.appSharedService.patientData;
        }
        if(this.appSharedService.patientSettings !== undefined){
            this.patientSettings = this.appSharedService.patientSettings;
        }

    }

    updateLoggedGeneralDetails (){
        let self = this;
        console.log(this.data);
    }

    getSlot(interval){
        let selectedHour = this.selectedTime.split(':')[0];
        let selectedMinutes = this.selectedTime.split(':')[1];
        return selectedHour*(60/interval) + (selectedMinutes % interval === 0 ? (selectedMinutes/interval)+1 : (selectedMinutes / interval) - ((selectedMinutes/interval)%1)+1);
    }

    getDate(){
        var date = new Date(this.selectedDate);
        return date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
     //   return (new Date(this.selectedDate)).toLocaleDateString().split('/').join('-');
    }

    makeEditable(){
        this.readOnly = false;
    }

    setStartAndEndDate(interval){

      let selectedTime = this.selectedTime.split(':');
      let date = new Date(this.selectedDate);
      date.setHours(selectedTime[0]);
      date.setMinutes(selectedTime[1]);
      let startDate = new Date(date);
      let endDate = new Date(date);

        startDate.setMinutes(startDate.getMinutes()-(selectedTime[1] % interval));
        endDate.setMinutes(startDate.getMinutes() + (interval - (selectedTime[1] % interval)));

        this.eventStartDate = startDate.toString();
        this.eventEndDate = endDate.toString();

    }

}



