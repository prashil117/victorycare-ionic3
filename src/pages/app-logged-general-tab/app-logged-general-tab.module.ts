import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppLoggedGeneralTabPage} from "./app-logged-general-tab";


@NgModule({
  declarations: [
      AppLoggedGeneralTabPage
  ],
  imports: [
    IonicPageModule.forChild(AppLoggedGeneralTabPage)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})

export class AppLoggedGeneralTabModule {}
