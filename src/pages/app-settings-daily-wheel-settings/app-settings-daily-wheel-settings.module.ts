import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppSettingsDailyWheelSettingsPage } from './app-settings-daily-wheel-settings';

@NgModule({
  declarations: [
    AppSettingsDailyWheelSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(AppSettingsDailyWheelSettingsPage),
  ],
})
export class AppSettingsDailyWheelSettingsPageModule {}
