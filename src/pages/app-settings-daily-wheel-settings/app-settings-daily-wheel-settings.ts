import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AppSettingsDailyWheelSettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-settings-daily-wheel-settings',
  templateUrl: 'app-settings-daily-wheel-settings.html',
})
export class AppSettingsDailyWheelSettingsPage {
  language = "English";
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppSettingsDailyWheelSettingsPage');
  }

  editValues(value) {
    var param = value == "wellbeing" ? "DailyWheelSettingsWellbeingPage" :
      value == "medicine" ? "DailyWheelSettingsMedicinePage" :
        value == "activity" ? "DailyWheelSettingsActivityPage" : "SocialAccountsPage";

    this.navCtrl.push(param);
  }

}
