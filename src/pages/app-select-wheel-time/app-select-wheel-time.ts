import { Component } from '@angular/core';
import { IonicPage,ModalController,ViewController, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AppSelectWheelTimePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-select-wheel-time',
  templateUrl: 'app-select-wheel-time.html',
})
export class AppSelectWheelTimePage {
  wheelTime : any;
  constructor(public navCtrl: NavController, public modalCtrl: ModalController,public viewCtrl: ViewController,public navParams: NavParams) {
    this.wheelTime = 15;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppSelectWheelTimePage');
  }
  next(){
    this.viewCtrl.dismiss(this.wheelTime);
  }
  close(){
    this.viewCtrl.dismiss(this.wheelTime);
  }
}
