import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppSelectWheelTimePage } from './app-select-wheel-time';

@NgModule({
  declarations: [
    AppSelectWheelTimePage,
  ],
  imports: [
    IonicPageModule.forChild(AppSelectWheelTimePage),
  ],
})
export class AppSelectWheelTimePageModule {}
