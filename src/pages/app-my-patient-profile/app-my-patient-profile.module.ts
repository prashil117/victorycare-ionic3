import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppMyPatientProfilePage } from './app-my-patient-profile';

@NgModule({
  declarations: [
    AppMyPatientProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(AppMyPatientProfilePage),
  ],
})
export class AppMyPatientProfilePageModule {}
