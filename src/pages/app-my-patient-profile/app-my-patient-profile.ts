import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { AlertController, IonicPage, ModalController, NavController, NavParams } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import * as _ from "lodash";
import { Validators, FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { EmailValidator } from '../../app/directive/email-validator';
import { AppAddContactModalPage } from "../app-add-contact-modal/app-add-contact-modal";
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";

/**
 * Generated class for the AppMyPatientProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-my-patient-profile',
  templateUrl: 'app-my-patient-profile.html',
  providers: [Camera, DatabaseService]
})
export class AppMyPatientProfilePage implements AfterViewInit {
  newDoctors: any = "";
  Profileform: FormGroup;
  submitAttempt: boolean;
  data: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  diagnosiss: any = {};
  upload: any = {};
  logo: any = {};
  action: any = "";
  image: any = "";
  data1: any = {};
  data2: any = {};
  data3: any = "";
  data4: any = {};
  userTypeToBeAdded: string;
  selectType: any = "";
  addExisting: string;
  readonly: any = {};
  result: any = [];
  enableDropdown: any = "";
  enableDiv: any = false;
  hcpLoggedIn: boolean;
  userData: any = [];
  SearchData: any = [];
  patientsData: any = [];
  userLoginData: any;
  patientType: any;
  hcpid: any;
  usertype: string;
  patientData1: any;
  @ViewChild('imageUploader') imageUploader: ElementRef;
  @ViewChild('addPhotoCaption') addPhotoCaption: ElementRef;
  @ViewChild('removePhotoIcon') removePhotoIcon: ElementRef;
  @ViewChild('previewImage') previewImage: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams,  public storage: Storage, public formBuilder: FormBuilder, private nativeGeocoder: NativeGeocoder, public modalCtrl: ModalController, private alertCtrl: AlertController, private databaseService: DatabaseService, private loadingService: LoadingService, private camera: Camera, public http: Http, private toastCtrl: ToastService) {
    console.log('patient profile');
    this.upload = "assets/images/upload1.svg";
    this.readonly = false;
    this.Profileform = formBuilder.group({
      usertype: [''],
      selectType: [''],
      storedDoctors: [''],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      birthDate: [''],
      mobile: [''],
      sex: [''],
      toppings: [''],
      languagePreferred: [''],
      address: [''],
      // diagnosis: [],
      // contacts: [],
      contacts: this.formBuilder.array([
        this.initContact(),
      ]),
      // phone: [],
      diagnosis: this.formBuilder.array([
        this.initDiagnosis(),
      ])
    });
  }

  appIntialize1() {
    this.data.usertype = (this.data.usertype == undefined ? "patient" : this.data.usertype);
    this.data.address = (this.data.address == undefined ? "" : this.data.address);
    this.data.birthDate = (this.data.birthDate == undefined ? "" : this.data.birthDate);
    this.data.mobile = (this.data.mobile == undefined ? "" : this.data.mobile);
    this.data.sex = (this.data.sex == undefined ? "" : this.data.sex);
    this.data.image = (this.data.image == undefined ? "" : this.data.image);
    this.data.languagePreferred = (this.data.languagePreferred == undefined ? "" : this.data.languagePreferred);
    // this.enableDiv = false;
    console.log(this.navParams.data);
    this.enableDiv = ((this.navParams.data.editable == true) ? false : true);
    this.data3 = this.navParams.data.doctorsdata;
    this.patientsData = this.navParams.data.patientData;
    this.userData = this.navParams.data.userData;
    this.patientData1 = this.navParams.data.data;
    this.addExisting = this.navParams.data.addExisting;
    this.userTypeToBeAdded = this.navParams.data.userTypeToBeAdded;
    console.log(this.navParams.data.patientData);
    console.log("htis.dataafaaaaaa", this.data)
    if (this.userTypeToBeAdded == undefined) {
      this.userTypeToBeAdded = this.userLoginData.usertype;
    }
    this.hcpLoggedIn = ((this.userTypeToBeAdded == 'healthcareprovider' && this.navParams.data.editable != true) ? true : false);
    console.log(this.hcpLoggedIn);
    console.log(this.enableDiv);
    console.log(this.userTypeToBeAdded);
    console.log(this.navParams.data.editable);
    console.log(this.data4.newDoctors);
    console.log(this.navParams.data);
    if (!_.isEmpty(this.navParams.data.data)) {
      this.data = this.navParams.data.data;
      this.data.image=this.navParams.data.data.image
      console.log("this.data",this.data.image)
      if (!_.isEmpty(this.data)) {
        this.action = this.navParams.data.action;
        if (this.action == 'edit') {
          this.storage.get('currentHCP').then(hcpid => {
            console.log('contacts', this.data.diagnosis);
            this.data.address = (this.data.address == undefined ? "" : this.data.address);
            console.log(this.data);
            var contactdata: any = {};
            contactdata.action = "getnextofkin";
            contactdata.userid = this.navParams.data.data.userid;
            console.log("hhhhhhccccccppppp", this.hcpid)
            if (this.userLoginData.usertype == "doctor" || this.userLoginData.usertype == "nurse") {
              contactdata.addedby = hcpid;
            }
            else {
              contactdata.addedby = this.userLoginData.id;
            }
            contactdata.appsecret = this.appsecret
            console.log("contactdataadadadasdadasd", contactdata)
            this.http.post(this.baseurl + 'getdata.php', contactdata).subscribe(result => {
              this.loadingService.show();
              console.log(result);
              let result1 = JSON.parse(result['_body']);
              console.log("result", result1);
              this.data.contacts = result1.data;
              if (this.data.contacts !== undefined) {
                this.loadingService.hide();
                console.log("result", result)
                for (let i = 0; i < this.data.contacts.length; i++) {
                  const control = <FormArray>this.Profileform.controls['contacts'];
                  control.push(this.initContact());
                }
              }
              else {
                this.loadingService.hide();
                this.data.contacts = [];
              }
            }), err => {
              this.loadingService.hide();
              this.toastCtrl.presentToast("something went wrong !");
            }
          });
        }
      }
    }
  }

 

  comparer(otherArray) {
    return function (current) {
      // console.log(current);
      return otherArray.filter(function (other) {
        // console.log(other);
        return other.email == current.email
        // return other == current
      }).length == 0;
    }
  }

  checkState(email) {
    console.log(email);
    this.loadingService.show();
  }

  addType($event) {
    console.log($event);
    if ($event == 'new') {
      this.doRefresh($event);
      this.loadingService.show();
      // this.enableDropdown = "display:none !important;";
      // this.enableDiv = "display:block !important;";
      this.enableDropdown = false;
      this.enableDiv = false;
      this.selectType = 'addnew';
      this.data.selectType = false;
      this.loadingService.hide();
    } else if ($event == 'exist') {
      // this.doRefresh($event);
      this.loadingService.show();
      // this.enableDropdown = "display:block !important;";
      // this.enableDiv = "display:none !important;";
      this.enableDropdown = true;
      this.enableDiv = true;
      this.selectType = 'addexist';
      this.data.storedDoctors = false;

      this.loadingService.hide();
    }
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }

  setInitialDiagnosis() {
    if (this.data.diagnosis === undefined) {
      this.data.diagnosis = [];
    }

    if (this.data.diagnosis.length === 0) {
      this.data.diagnosis.push({});
    }
  }

  addDiagnosis() {
    console.log('diagnosis', this.data.diagnosis);
    const control = <FormArray>this.Profileform.controls['diagnosis'];
    control.push(this.initDiagnosis());
    this.data.diagnosis.push({ name: '' });

  }

  logout() {
    this.databaseService.logout();
  }

  initDiagnosis() {
    return this.formBuilder.group({
      name: ['']
    });
  }

  initContact() {
    return this.formBuilder.group({
      name: [''],
      phone: ['']
    });
  }

  deleteDiagnosis(index) {

    this.data.diagnosis.splice(index, 1);
  }

  addContact() {
    let modal = this.modalCtrl.create(AppAddContactModalPage, { 'contacts': this.data.contacts });
    modal.present();
    modal.onDidDismiss(data => {
      this.getLetLong();
    });
    // this.data.contacts.push({});
  }

  deleteContact(index) {
    this.data.contacts.splice(index, 1);
  }

  ngAfterViewInit(): void {
    this.setInitialDiagnosis();
    if (this.data.contacts === undefined) {
      this.data.contacts = [];
    }

    if (!_.isEmpty(this.action)) {
      this.showImagePreview();

      if (this.action === "view") {
        this.readonly = true;
      }
    }

  }

  getLetLong() {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    const control = <FormArray>this.Profileform.controls['contacts'];
    control.push(this.initContact());
    let self = this;
    console.log('contacts', self.data.contacts);
    for (let i = 0; i < self.data.contacts.length; i++) {
      self.nativeGeocoder.forwardGeocode(self.data.contacts[i].address, options)
        .then((coordinates: NativeGeocoderForwardResult[]) => {
          self.data.contacts[i].lat = coordinates[0].latitude;
          self.data.contacts[i].long = coordinates[0].longitude;
        }).catch((error: any) => console.log(error));
    }
  }

  addPatientDetails() {
    let self = this;
    this.submitAttempt = true;

    if (!this.Profileform.valid) {
      console.log("Profileform Validation Fire!");
    }
    else {
      // console.log('this.data',this.data);
      this.Profileform.value.id = this.data.id;
      if (this.data.image != undefined) {
        this.data.image = this.data.image;
      }
      else {
        this.showImagePreview();
        this.data.image = "";
      }
      if (this.action === 'edit') {
        console.log(self.data);
        self.data.action = "updateuser";
        self.data.appsecret = this.appsecret
        self.http.post(this.baseurl + "updateuser.php", self.data).subscribe(data => {
          this.loadingService.show();
          let result = JSON.parse(data["_body"]);
          console.log(result.status);
          if (result.status == 'success') {
            this.loadingService.hide();
            self.toastCtrl.presentToast("Patients profile is updated successfully");
            self.navCtrl.setRoot("AppMyPatientsPage", { 'data': self.data });
          } else {
            this.loadingService.hide();
            self.toastCtrl.presentToast("There is an error saving data!");
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
          }
        }, err => {
          this.loadingService.hide();
          console.log(err);
        });
        // this.updatePatientDetails().then(function(){
        //     self.navCtrl.setRoot("AppMyPatientsPage",{'data': self.data});
        // });
        var data1: any = {};

        data1.action = "addnextofkin"
        data1.data = self.data.contacts
        data1.appsecret = this.appsecret
        if (this.userLoginData.usertype != 'patient') {
          data1.userid = this.patientData1.userid;
          data1.addedby = this.userLoginData.id;
        }
        else {
          data1.userid = this.userLoginData.id;
          data1.addedby = this.userLoginData.id;
        }
        console.log("datasdfsfsd", data1)
        self.http.post(this.baseurl + "adddata.php", data1).subscribe(data => {
          this.loadingService.show();
          let result = JSON.parse(data["_body"]);
          console.log(result.status);
          if (result.status == 'success') {
            this.loadingService.hide();
            self.toastCtrl.presentToast("Patients profile is updated successfully");
            self.navCtrl.setRoot("AppMyPatientsPage", { 'data': self.data });
          } else {
            this.loadingService.hide();
            self.toastCtrl.presentToast("There is an error saving data!");
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
          }
        }, err => {
          this.loadingService.hide();
          console.log(err);
        });

      } else {
        this.data.appsecret = this.appsecret;
        self.http.post(this.baseurl + "updateuser.php", self.data).subscribe(data => {
          this.loadingService.show();
          let result = JSON.parse(data["_body"]);
          console.log(result.status);
          if (result.status == 'success') {
            this.loadingService.hide();
            self.toastCtrl.presentToast("Patients profile is updated successfully");
            self.navCtrl.setRoot("AppMyPatientsPage", { 'data': self.data });
          } else {
            this.loadingService.hide();
            self.toastCtrl.presentToast("There is an error saving data!");
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
          }
        }, err => {
          this.loadingService.hide();
          console.log(err);
        });
      }
    }
  }

  ImageUpload() {
    let alert = this.alertCtrl.create({
      // title: 'Image Upload',
      message: 'Select Image source',
      buttons: [
        {
          text: 'Upload from Library',
          handler: () => {
            this.gallery(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.gallery(this.camera.PictureSourceType.CAMERA);
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  gallery(sourceType: PictureSourceType) {
    let self = this;
    this.camera.getPicture({
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      correctOrientation: true,
      allowEdit: true
    }).then((imageData) => {
      // imageData is a base64 encoded string
      self.showImagePreview();
      self.data.image = "data:image/jpeg;base64," + imageData;
      self.image = "data:image/jpeg;base64," + imageData;

    }, (err) => {
      console.log("Error " + err);
    });
  }

  showImagePreview() {
    let self = this;
    self.setVisibility(self.previewImage, "block");
    self.setVisibility(self.imageUploader, "none");
    self.setVisibility(self.addPhotoCaption, "none");
    self.setVisibility(self.removePhotoIcon, "block");
  }

  removeImage() {
    let self = this;
    this.data.image = undefined;
    self.setVisibility(self.previewImage, "none");
    self.setVisibility(self.imageUploader, "block");
    self.setVisibility(self.addPhotoCaption, "block");
    self.setVisibility(self.removePhotoIcon, "none");

  }

  setVisibility(element: ElementRef, state) {
    if (element !== undefined)
      element.nativeElement.style.display = state;
  }

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log('set string in storage: ' + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log('storageGET: ' + key + ': ' + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }
  // remove a single key value:
  remove(key: string) {
    this.storage.remove(key);
  }

  ionViewDidLoad() {
    this.getHCPid()
    console.log('ionViewDidLoad AppMyPatientProfilePage');
  }

  async getHCPid() {
    await this.get('currentHCP').then(result => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData()
      } else {
        this.getuserLoginData()
      }
    });
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
        }
        this.getpatientType();
      }
    });
  }


  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh('');
      }
      else {
        this.patientType = result;
        this.appIntialize1();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }


}
