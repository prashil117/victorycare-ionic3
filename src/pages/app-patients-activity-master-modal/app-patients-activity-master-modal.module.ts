import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppPatientsActivityMasterModalPage } from './app-patients-activity-master-modal';

@NgModule({
  declarations: [
    AppPatientsActivityMasterModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AppPatientsActivityMasterModalPage),
  ],
})
export class AppPatientsActivityMasterModalPageModule {}
