import { Component, AfterViewInit } from '@angular/core';
import {
    AlertController, IonicPage, ModalController, NavController, NavParams, Platform, ViewController
} from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from 'rxjs/Observable';
import * as _ from "lodash";
import { AppSharedService } from "../../services/app-shared-service";
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";

/**
 * Generated class for the AppPatientsActivityMasterModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-app-patients-activity-master-modal',
    templateUrl: 'app-patients-activity-master-modal.html',
    providers: [DatabaseService]
})
export class AppPatientsActivityMasterModalPage implements AfterViewInit {

    // colors: Array<string> = ['#d435a2', '#a834bf', '#6011cf', '#0d0e81', '#0237f1', '#0d8bcd', '#16aca4', '#3c887e', '#157145', '#57a773', '#88aa3d', '#b7990d', '#fcbf55', '#ff8668', '#ff5c6a', '#c2454c', '#c2183f', '#d8226b', '#8f2d56', '#482971', '#000000', '#561f37', '#433835', '#797979', '#819595'];
    colors: Array<string> = ['#E26060', '#8E24AA', '#6BB1D6', '#00897B', '#AFDB07', '#C0CA33', '#FAD000', '#FB8C00'];
    data: any = {};
    Pagetitle: string;
    // colors:any = [];
    events: any = [];
    patientData: any = [];
    eventType: any = {};
    baseurl = environment.apiUrl;
    appsecret = environment.appsecret;
    itemIndex: any = {};
    Eventmaster: any;
    date: any = new Date();
    eventList: any = [];
    patientSession: boolean;
    patientAction: any
    newActivityData: any;
    userLoginData: any;
    patientType: any;
    hcpid: any;

    constructor(public navCtrl: NavController, navParams: NavParams,  private alertCtrl: AlertController, private databaseService: DatabaseService,  private loadingService: LoadingService, public modalCtrl: ModalController, public viewCtrl: ViewController, public appSharedService: AppSharedService, public storage: Storage, public http: Http, private toastCtrl: ToastService) {
        this.patientAction = navParams.data.patientAction ? navParams.data.patientAction : '';
        this.newActivityData = navParams.data.activitydata ? navParams.data.activitydata : '';
        if (!_.isEmpty(navParams.data)) {
            if (navParams.data.type === 'edit') {
                this.storage.get('patientSession').then((val) => {
                    if (val != undefined)
                        this.patientSession = true;
                });
                this.storage.get('user').then((val) => {
                    this.patientData = val;
                });

                this.Pagetitle = 'Edit Activity';
                this.data = navParams.data.event;
                this.eventType = navParams.data.type;
                this.itemIndex = navParams.data.index;
                this.patientData = navParams.data.patientData;
                this.Eventmaster = "";
                if (this.data.color) {
                    this.data.color = this.data.color;
                }
                else {
                    this.data.color = '#E26060';
                }

            } else {
                this.Pagetitle = 'Add Activity';
                this.eventType = '';
                this.events = navParams.data.events;
                this.patientData = navParams.data.patientData;
                this.Eventmaster = navParams.data.Eventmaster;
                this.data.color = '#E26060';
            }

        }
        else {
            this.data.color = '#E26060';
        }
        // this.colors = this.appSharedService.colors;
    }

    // for choose color and set in dropdown
    prepareColorSelector() {
        setTimeout(() => {
            let buttonElements = document.querySelectorAll('div.alert-radio-group button');
            if (!buttonElements.length) {
                this.prepareColorSelector();
            } else {
                for (let index = 0; index < buttonElements.length; index++) {
                    let buttonElement = buttonElements[index];
                    let optionLabelElement = buttonElement.querySelector('.alert-radio-label');
                    let color = optionLabelElement.innerHTML.trim();

                    if (this.isHexColor(color)) {
                        buttonElement.classList.add('colorselect', 'color_' + color.slice(1, 7));
                        if (color == this.data.color) {
                            buttonElement.classList.add('colorselected');
                        }
                    }
                }
            }
        }, 100);
    }

    // for choose color and set in dropdown
    isHexColor(color) {
        let hexColorRegEx = /^#(?:[0-9a-fA-F]{3}){1,2}$/;
        return hexColorRegEx.test(color);
    }

    // for choose color and set in dropdown
    selectColor(color) {
        let buttonElements = document.querySelectorAll('div.alert-radio-group button.colorselect');
        for (let index = 0; index < buttonElements.length; index++) {
            let buttonElement = buttonElements[index];
            buttonElement.classList.remove('colorselected');
            if (buttonElement.classList.contains('color_' + color.slice(1, 7))) {
                buttonElement.classList.add('colorselected');
            }
        }
    }

    // for choose color and set in dropdown
    setColor(color) {
        console.log('Selected Color is', color);

    }

    closeModal() {
        this.viewCtrl.dismiss();
    }

    saveActivity() {
        let self = this;
        if (!_.isUndefined(this.data.name) && !_.isUndefined(this.data.color) && !_.isUndefined(this.data.description)) {
            console.log(self.data);
            if (this.userLoginData.usertype == "healthcareprovider") {
                this.data.userid = this.userLoginData.id;
            }
            else {
                this.data.userid = this.hcpid;
            }
            console.log('activity data -->', this.data);
            self.data.action = "addactivity";
            self.data.doctorid = this.userLoginData.id;
            self.data.appsecret = this.appsecret;
            self.http.post(this.baseurl + "adddata.php", self.data).subscribe(data => {
                let result = JSON.parse(data["_body"]);
                console.log(result);
                if (result.status == 'success') {
                    self.loadingService.hide();
                    self.toastCtrl.presentToast("Activity added successfully");
                    self.navCtrl.setRoot("AppActivityMasterListPage");

                    // return result1;
                } else {
                    self.loadingService.hide();
                    self.toastCtrl.presentToast("There is a problem adding data, please try again");
                    self.navCtrl.setRoot("AppActivityMasterListPage");
                    // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                }
            }, err => {
                console.log(err);
            });
        } else {
            this.presentAlert('Please enter Event name or choose color');
        }

    }

    getDate() {
        let date = new Date(this.date);
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        // return date.getDate();
    }



    presentAlert(message) {
        let alert = this.alertCtrl.create({
            subTitle: message,
            buttons: ['Dismiss']
        });
        alert.present();
    }

    addTime() {
        console.log(this.data.date);
        // var newdate = this.data.date;
        this.data.date.push(this.data.date);
    }

    deleteTime(index) {
        this.data.date.splice(index, 1);
    }

    setInitialTime() {
        if (this.data.date === undefined) {
            this.data.date = [];
        }

        if (this.data.date.length === 0) {
            this.data.date.push({});
        }
    }

    ngAfterViewInit(): void {
        this.setInitialTime();
        this.getHCPid();
        console.log("daadsdsadsdsdfdsf");
    }

    async get(key: string): Promise<any> {
        try {
            const result = await this.storage.get(key);
            if (result != null) {
                return result;
            }
            return null;
        } catch (reason) {
            console.log(reason);
            return null;
        }
    }
    async getHCPid() {
        await this.get('currentHCP').then(result => {
            if (result != null) {
                this.hcpid = result;
                console.log("if", this.hcpid)
                this.getuserLoginData();
            } else {
                this.getuserLoginData();
            }
        });
    }

    async getuserLoginData() {
        await this.get('userLogin').then(result => {
            if (result != null) {
                this.userLoginData = result;
                this.hcpid = this.hcpid ? this.hcpid : this.userLoginData.id
                this.getpatientType();
            }
        });
    }
    async getpatientType() {
        console.log("this.userdata", this.userLoginData);
        await this.get('patientType').then(result => {
            console.log("patienttypesdfdsfsdsdf", result)
            if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
                this.doRefresh('');
            }
            else {
                this.patientType = result;
                console.log("sekeccdffvfdgdfgdfg", this.patientType)
            }
        });
    }

    doRefresh(event) {
        console.log('Begin async operation');
        setTimeout(() => {
            console.log('Async operation has ended');
            this.navCtrl.setRoot(this.navCtrl.getActive().component);
            // event.target.complete();
        }, 2000);
    }
}
