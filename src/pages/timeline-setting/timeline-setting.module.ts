import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimelineSettingPage } from './timeline-setting';

@NgModule({
  declarations: [
    TimelineSettingPage,
  ],
  imports: [
    IonicPageModule.forChild(TimelineSettingPage),
  ],
})
export class TimelineSettingPageModule {}
