import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import * as _ from "lodash";

/**
 * Generated class for the TimelineSettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-timeline-setting",
  templateUrl: "timeline-setting.html",
})
export class TimelineSettingPage {
  data: any = {};
  patientData: any = {};
  tabs: any = [];
  prepopulated: any;
  eventData: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    if (!_.isEmpty(navParams.data)) {
      this.patientData = navParams.data.data;
      this.prepopulated = navParams.data.addprepopulated;
      console.log("this.patientData setting", this.patientData);
      console.log("this.prepopulated", this.prepopulated);
      if (this.prepopulated == "yes") {
        this.eventData = [
          { "color": "#AFDB07", "name": "Awake" },
          { "color": "#6BB1D6", "name": "Sleeping" },
          { "color": "#FAD000", "name": "Uncomfortable" },
          { "color": "#E26060", "name": "Pain" },
        ];
        console.log("this.eventData", this.eventData);
      }
    }

    this.data.tabs = [
      {
        page: "AppPatientEventSettingsPage",
        title: "CURRENT STATUS",
        params: this.patientData,
      },
      {
        page: "AppPatientMedicineListPage",
        title: "MEDICINE",
        params: this.patientData,
      },
      {
        page: "AppPatientActivityListPage",
        title: "ACTIVITY",
        params: this.patientData,
      },
    ];
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad TimelineSettingPage");
  }
}
