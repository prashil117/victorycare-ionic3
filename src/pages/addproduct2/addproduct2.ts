import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
// import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http } from '@angular/http';

/**
 * Generated class for the Addproduct2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addproduct2',
  templateUrl: 'addproduct2.html',
})
export class Addproduct2Page {
  product: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams, public http:Http, public toast: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Addproduct2Page');
  }

  insert(){
    console.log(this.product);
    // var headers = new HttpHeaders();
    // headers.append("Accept", 'application/json');
    // headers.append('Content-Type', 'application/json');
    // headers.append('Access-Control-Allow-Origin', '*');

    this.product.action = "insert";
    this.http.post("http://myswprojects.com/prodcust/webservice/product.php", this.product).subscribe(data=>{
      console.log(data);
      let result = JSON.parse(data["_body"]);
      if(result.status == "success"){
        this.showToast("Inserted successfully");
      }else{
        this.showToast("Something went wrong");
      }
    }, err=>{
    console.log(err);
    })
  }

  showToast(message){
    let toast = this.toast.create({
      message:message,
      duration: 2000
    });
    toast.present();
  }

}
