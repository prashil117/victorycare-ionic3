import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Addproduct2Page } from './addproduct2';

@NgModule({
  declarations: [
    Addproduct2Page,
  ],
  imports: [
    IonicPageModule.forChild(Addproduct2Page),
  ],
})
export class Addproduct2PageModule {}
