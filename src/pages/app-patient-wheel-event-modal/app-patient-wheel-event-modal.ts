import {Component, AfterViewInit} from '@angular/core';
import {
    AlertController, IonicPage, ModalController, NavController, NavParams, Platform,
    ViewController
} from 'ionic-angular';
import {DatabaseService} from "../../services/database-service";
import {LoadingService} from "../../services/loading-service";
import {Observable} from 'rxjs/Observable';
import * as _ from "lodash";
import * as moment from 'moment';
import {AppSharedService} from "../../services/app-shared-service";



@IonicPage()
@Component({
    selector: 'app-patient-wheel-event-modal',
    templateUrl: 'app-patient-wheel-event-modal.html',
    providers: [DatabaseService]
})
export class AppPatientWheelEventModalPage {

    data: any = {};
    colors:any = [];
    events :any = [];
    patientData:any = [];
    patientSettings:any = [];
    selectedEvent: any = {};
    selectedActivities:any = [];
    clonedActivities : any = [];
    selectedMedicines:any = [];
    selectedEventKey: any = {};
    interval: any = 15;
    date: any = new Date();
    selectedTime: any = "";
    selectedSlot: any = "";
    public currentUser : any = {};

    constructor(public navCtrl: NavController, navParams: NavParams,  private alertCtrl: AlertController, private databaseService: DatabaseService, 
         private loadingService: LoadingService,public modalCtrl: ModalController, public viewCtrl: ViewController, public appSharedService: AppSharedService) {

        let self = this;
        if(!_.isEmpty(navParams.data)){
            this.patientSettings = navParams.data.patientSettings;
            this.patientData = navParams.data.patientData;
            this.date = navParams.data.selectedDate;
            this.selectedTime = navParams.data.selectedTime;
            this.selectedSlot = navParams.data.selectedSlot;
            this.currentUser = this.appSharedService.currentUser;

            if(this.selectedSlot!==""){
               let time = this.selectedSlot.split(':');
               let hours  = time[0] !==undefined && time[0]>10? time[0] : "0"+time[0];
               let minutes = time[1];
               this.selectedTime = hours + ":"+ minutes;
            }


        }
    }

    


    

    transformSelectedActivities(){
        let self = this;
        let activities= (<any>Object).keys(self.selectedActivities).map(function (key) {
            self.selectedActivities[key].activityId = key;
            return self.selectedActivities[key];
        });

        self.selectedActivities = activities;
    }

    closeModal(action){
        this.viewCtrl.dismiss({'action':action});
    }

    presentAlert(message) {
        let alert = this.alertCtrl.create({
            subTitle: message,
            buttons: ['Dismiss']
        });
        alert.present();
    }

    selectEvent(event, item, index){
        let target = event.currentTarget;
        console.log("target",target);
        if(target.classList.contains('selected')){
            target.classList.remove('selected');
            this.selectedEvent = undefined;
            this.selectedEventKey = -1;
        }else{
            event.currentTarget.classList.add('selected');
            this.selectedEvent = item;
            this.selectedEventKey = index;
        }

    }

    selectActivity(item, index){
        let self = this;

        let activity =_.find(this.selectedActivities,{'id':index});
        if(activity!==undefined){
            this.selectedActivities.splice(_.indexOf(self.selectedActivities,activity),1);
            item.selected = false;
        }else{
            this.selectedActivities.push({
                id:index,
                comments: item.comments === undefined ? "" : item.comments
            });
            item.selected = true;
        }
    }

    selectMedicine(item, index){
        let self = this;

        let medicine =_.find(this.selectedMedicines,{'id':index});
        if(medicine){
            this.selectedMedicines.splice(_.indexOf(self.selectedMedicines,medicine),1);
            item.selected = false;
        }else{
            this.selectedMedicines.push({
                id:index,
                comments: item.comments === undefined ? "" : item.comments
            });
            item.selected = true;
        }
    }

    
    getSlot(interval){
        this.data.time = this.selectedSlot.split('-')[0].trim();
        let selectedHour = this.data.time.split(':')[0];
        let selectedMinutes = this.data.time.split(':')[1];
        return selectedHour*(60/interval) + (selectedMinutes % interval === 0 ? (selectedMinutes/interval)+1 : (selectedMinutes / interval) - ((selectedMinutes/interval)%1)+1);
    }

    getDate(){
        let date = new Date(this.date);
        return date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
    }

    getActivityName(id){
        if(this.patientSettings.activityData[id] !==undefined){
          return  this.patientSettings.activityData[id].name;
        }else{
            return "";
        }
    }

    getMedicineName(id){
        if(this.patientSettings.medicineData[id] !==undefined){
            return  this.patientSettings.medicineData[id].salt;
        }else{
            return "";
        }
    }



    findSlotsForSelectedTimeRange(startTime,endTime){
        let slots = [];
        let startSlot = this.findSlot(startTime,this.interval);
        let endSlot = this.findSlot(endTime,this.interval);
        for(let i = startSlot;i<=endSlot;i++){
            slots.push(i);
        }
        return slots;
    }

    findSlot(date,interval){
        let selectedHour = date.split(':')[0];
        let selectedMinutes = date.split(':')[1];
        return selectedHour*(60/interval) + (selectedMinutes % interval === 0 ? (selectedMinutes/interval)+1 : (selectedMinutes / interval) - ((selectedMinutes/interval)%1)+1);
    }

    resetModal(){
        let self = this;
        self.selectedActivities = [];
        self.clonedActivities = [];
        self.selectedMedicines = [];
        self.selectedEventKey = "";
        self.selectedEvent = "";
        _.each(self.patientSettings.activityData, function(item){
            item.selected =  false;
        });

        _.each(self.patientSettings.medicineData, function(item){
            item.selected =  false;
        });
    }
}



