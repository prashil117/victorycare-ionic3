import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppPatientWheelEventModalPage} from "./app-patient-wheel-event-modal";


@NgModule({
  declarations: [
      AppPatientWheelEventModalPage
  ],
  imports: [
    IonicPageModule.forChild(AppPatientWheelEventModalPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppPatientWheelEventModalModule {}
