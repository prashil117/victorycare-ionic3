import { Component } from '@angular/core';
import { IonicPage, ViewController, AlertController, NavController, NavParams } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as _ from "lodash";
import { Action } from 'rxjs/scheduler/Action';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
/**
 * Generated class for the AppAddActivityModelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-add-activity-model',
  templateUrl: 'app-add-activity-model.html',
  providers: [DatabaseService]
})
export class AppAddActivityModelPage {

  activityform: FormGroup;
  submitAttempt: boolean = false;
  activityData: any = [];
  baseurl = environment.apiUrl;
  activityWheelData: any = [];
  mintime:any;
  maxtime:any;
  appsecret = environment.appsecret;
  patientData: any = {};
  SearchData: any = [];
  patientSettings: any = [];
  patientActivityMasterData: any = [];
  ActivityData: any = [];
  data: any = {};
  action: any = "";
  key: any;
  hcpid: any;
  enableCurrentTime: boolean = false;
  disabled: boolean = false;
  date: any = new Date();
  selectedActivity: any = [];
  originalActivity: any = [];
  selectedEvent: any = {};
  selectedEventKey: any = {};
  TodayDate: any;
  time: any;
  userLoginData: any;
  patientType: any;
  previousKey: any = undefined;
  shadesEl: any;
  classList: any = [];

  constructor(public storage: Storage, public navCtrl: NavController,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    private databaseService: DatabaseService,
    public navParams: NavParams, public http: Http, private toastCtrl: ToastService) {
    this.action = navParams.data.action;
    //console.log('Activity',this.ActivityData);
    this.activityform = formBuilder.group({
      start: ['', Validators.required],
      end: ['', Validators.required],
      current: [''],
      description: ['']

    });

    if (this.action == 'edit') {
      this.data = navParams.data.data;
      //this.patientActivityMasterData = this.patientSettings['patientActivityMasterData'];
      this.key = navParams.data.data.key;
      this.patientData = navParams.data.patient;
      this.patientSettings = navParams.data.patientSettings;
      this.patientActivityMasterData = this.patientSettings['patientActivityMasterData'];
      this.data.enableCurrentTime = false;
      // this.selectedEvent = navParams.data.data.Activity[0];
      // this.selectedEventKey = navParams.data.data.Activity[0].selectedKey;
      this.TodayDate = navParams.data.date;
      this.originalActivity = navParams.data.data;
      this.selectedActivity.id = this.originalActivity.activitymasterid;
      this.data.activitydataid = this.originalActivity.activitymasterid;
      this.selectedActivity.color = this.originalActivity.color;
      this.selectedActivity.name = this.originalActivity.name;
      console.log(navParams);
      //this.notify();
      console.log(this.TodayDate);
      console.log("navparams", navParams.data);
      console.log("data 12345", navParams.data);
    }
    else {
      if (!_.isEmpty(navParams.data)) {
        console.log("data 12345", navParams.data);

        this.patientData = navParams.data.patientData;
        this.TodayDate = navParams.data.date;
        this.selectedActivity.id = this.navParams.data.activityData ? this.navParams.data.activityData.activityid : '';
        this.data.activitydataid = this.navParams.data.activityData ? this.navParams.data.activityData.activityid : '';
        this.data.end = this.navParams.data.activityData ? this.navParams.data.activityData.endtime : '';
        this.data.start = this.navParams.data.activityData ? this.navParams.data.activityData.starttime : '';
        this.patientSettings = navParams.data.patientSettings;
        this.patientActivityMasterData = this.patientSettings['patientActivityMasterData'];
        // console.log('Activity',this.patientActivityMasterData);
        this.ActivityData = this.patientActivityMasterData;

        // this.getActivity();
        console.log(this.patientActivityMasterData);

      }
    }

    // this.getDate();
    // var time = this.getTime();
    this.data.enableCurrentTime = false;
    // this.data.start = this.getFormatedTime();
  }

  notify() {
    this.disabled = this.data.enableCurrentTime;
    if (this.data.enableCurrentTime == true) {
      this.data.start = this.getFormatedTime();
      this.data.end = this.getFormatedTime();
    } else {
      this.disabled = false;
    }
  }

  setMaxTime()
  {
    console.log("end",this.data.end)
    this.mintime=this.data.end
  }
  setMinTime()
  {
    console.log("end",this.data.start)
    this.maxtime=this.data.start
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AppAddActivityModelPage');
    this.getHCPid();

  }

  closeModal() {
    this.viewCtrl.dismiss();
    // this.loadWheelActivity();
  }

  getDate() {
    let date = new Date(this.date);
    return date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
  }

  getFormatedTime() {
    var date = new Date(this.date);
    var hours = date.getHours() > 24 ? date.getHours() - 12 : date.getHours();
    var am_pm = date.getHours() >= 24;
    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    let time = hours + ":" + minutes;
    return time;
  }



  selectActivity(event, item, index, previousKey) {
    let target = event.currentTarget;
    console.log(target);
    this.selectedActivity = [];
    if (target.classList.contains('selected')) {
      target.classList.remove('selected');
      //  this.selectedEvent = undefined;
      this.selectedEventKey = -1;
    } else {
      // console.log(this.selectedActivity);
      let shadesEl = document.querySelector('.selected');
      if (shadesEl != undefined) {
        shadesEl.classList.remove('selected');
      }


      event.currentTarget.classList.add('selected');
      previousKey = this.selectedActivity;
      // this.selectedActivity.push({id:item.id,color:item.color,name:item.name,selectedKey:index });
      this.selectedActivity.id = item.id;
      this.selectedActivity.color = item.color;
      this.selectedActivity.name = item.name;
      console.log('selected color', this.selectedActivity);
      // this.selectedActivity.selectedKey = item.index;
      this.selectedEventKey = index;
    }
    // console.log('selected color',this.selectedActivity);
  }



  loadWheelActivity() {
    let self = this;
    var webServiceData: any = {};
    webServiceData.action = "getactivitywheeldata";
    webServiceData.currentdate = this.TodayDate;
    webServiceData.userid = this.patientData.userid;
    webServiceData.appsecret = this.appsecret;
    if (this.patientType == "independent") {
      webServiceData.hcpid = 0;
    }
    else {
      webServiceData.hcpid = this.hcpid;
    }
    var activity = self.http.post(this.baseurl + "wheeldata.php", webServiceData).subscribe(data => {
      let result = JSON.parse(data["_body"]);
      if (result.status == 'success') {
        this.activityWheelData = [];
        this.activityWheelData = result.data;
      } else {
        this.activityWheelData = [];
      }
    });
  }

  addActivity(currentDate) {
    console.log(currentDate);
    this.submitAttempt = true;

    if (!this.activityform.valid) {

    }
    else {
      // if(this.selectedActivity.length == 0){
      if (this.selectedActivity.id == undefined || this.selectedActivity.id == "" || this.selectedActivity.id == null) {

        const alert = this.alertCtrl.create({
          title: 'Please select Activity',
          // subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
          buttons: ['OK']
        });
        alert.present();
      } else {
        let self = this;
        self.data.Activity = self.selectedActivity;
        self.activityWheelData.push(self.data);

        var webServiceData: any = {};
        webServiceData.action = "addactivitywheeldata";
        webServiceData.userid = this.patientData.userid;
        webServiceData.appsecret = this.appsecret;
        webServiceData.activityid = self.data.Activity['id'];
        webServiceData.activityname = this.navParams.data.activityData ? this.navParams.data.activityData.activityname : self.data.Activity['name'];
        webServiceData.activitycolor = self.data.Activity['color'];
        webServiceData.enableCurrentTime = self.data.enableCurrentTime;
        webServiceData.start = self.data.start;
        webServiceData.end = self.data.end;
        webServiceData.description = self.data.description;
        webServiceData.currentDate = currentDate;
        if (this.patientType == "independent") {
          webServiceData.hcpid = 0;
        }
        else if (this.userLoginData.usertype == "nurse" || this.userLoginData.usertype == "doctor" || (this.userLoginData.usertype == "patient" && this.patientType == "dependent")) {
          webServiceData.hcpid = this.hcpid;
        }
        else {
          webServiceData.hcpid = this.userLoginData.id;
        }


        console.log(webServiceData);
        var activity = self.http.post(this.baseurl + "wheeldata.php", webServiceData).subscribe(data => {
          let result = JSON.parse(data["_body"]);
          if (result.status == 'success') {
            self.toastCtrl.presentToast("Activity data added successfully");
            self.closeModal();
          } else {
            self.toastCtrl.presentToast("There is a problem adding data, please try again");
            self.closeModal();
          }
        });
      }
    }
  }

  async getHCPid() {
    await this.get('currentHCP').then(result => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData()
        console.log("if", this.hcpid)
      }
      else {
        this.hcpid = 0;
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }
  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh();
      }
      else {
        this.patientType = result;
        this.loadWheelActivity();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }

  doRefresh() {
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
    }, 2000);
  }


  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log('storageGET: ' + key + ': ' + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }

  updateActivity(currentDate) {
    console.log(currentDate);
    if (this.selectedActivity.id == undefined) {

      const alert = this.alertCtrl.create({
        title: 'Plese select Activity',
        // subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
        buttons: ['OK']
      });
      alert.present();
    }
    else {
      let self = this;
      self.data.Activity = self.selectedActivity;
      self.activityWheelData = self.data;
      console.log(self.activityWheelData);
      console.log(self.data);
      console.log(this.patientData);

      var webServiceData: any = {};
      webServiceData.action = "updateactivitywheeldata";
      webServiceData.id = self.activityWheelData.id;
      webServiceData.userid = self.patientData.userid;
      webServiceData.activityid = self.activityWheelData.Activity.id;
      webServiceData.activityname = self.activityWheelData.Activity.name;
      webServiceData.start = self.activityWheelData.start;
      webServiceData.end = self.activityWheelData.end;
      webServiceData.description = self.activityWheelData.description;
      webServiceData.enableCurrentTime = self.activityWheelData.enableCurrentTime;
      webServiceData.currentDate = currentDate;
      webServiceData.appsecret = this.appsecret
      console.log(webServiceData);

      self.http.post(this.baseurl + "wheeldata.php", webServiceData).subscribe(data => {
        let result = JSON.parse(data["_body"]);
        console.log(result);
        if (result.status == 'success') {
          self.toastCtrl.presentToast("Activity data updated successfully");
          self.closeModal();
        } else {
          self.toastCtrl.presentToast("There is a problem updating data");
        }
      }, err => {
        console.log(err);
      });
    }
  }
}
