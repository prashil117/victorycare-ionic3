import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppAddActivityModelPage } from './app-add-activity-model';


@NgModule({
  declarations: [
    AppAddActivityModelPage,
  ],
  imports: [
    IonicPageModule.forChild(AppAddActivityModelPage),
  ],
})
export class AppAddActivityModelPageModule {}
