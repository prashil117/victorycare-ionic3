import { Component, ViewChild, ElementRef } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController,
  ViewController,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";   
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl,
} from "@angular/forms";
import { Camera, PictureSourceType } from "@ionic-native/camera";
import * as _ from "lodash";
import { Storage } from "@ionic/storage";
import { AppSharedService } from "../../services/app-shared-service";
import { Http } from "@angular/http";
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";

/**
 * Generated class for the AppAddMedicineMasterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-app-add-medicine-master",
  templateUrl: "app-add-medicine-master.html",
  providers: [Camera, DatabaseService],
})
export class AppAddMedicineMasterPage {
  @ViewChild("imageUploader")
  imageUploader: ElementRef;
  @ViewChild("addPhotoCaption")
  addPhotoCaption: ElementRef;
  @ViewChild("removePhotoIcon")
  removePhotoIcon: ElementRef;
  @ViewChild("previewImage")
  previewImage: ElementRef;
  //  colors: Array<string> = ['#d435a2', '#a834bf', '#6011cf', '#0d0e81', '#0237f1', '#0d8bcd', '#16aca4', '#3c887e', '#157145', '#57a773', '#88aa3d', '#b7990d', '#fcbf55', '#ff8668', '#ff5c6a', '#c2454c', '#c2183f', '#d8226b', '#8f2d56', '#482971', '#000000', '#561f37', '#433835', '#797979', '#819595'];
  colors: Array<string> = [
    "#BA68C8",
    "#64B5F6",
    "#4DD0E1",
    "#4DB6AC",
    "#81C784",
    "#DCE775",
    "#FFF176",
    "#FFD54F",
    "#FFB74D",
    "#90A4AE",
  ];
  medicineform: FormGroup;
  submitAttempt: boolean;
  data: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  patientData: any = {};
  userLoginData: any;
  patientType: any;
  medicineData: any = [];
  // colors:any = [];
  readonly: any = {};
  upload: any = {};
  id: any;
  key: number;
  hcpid: any;
  action: any = "";
  image: any = "";
  newMediceneData: any;
  patientAction: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public appSharedService: AppSharedService,
    private alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    private camera: Camera,
    private loadingService: LoadingService,
    public storage: Storage,
    private databaseService: DatabaseService,
    public http: Http,
    private toastCtrl: ToastService,
  ) {
    this.upload = "assets/images/upload1.svg";
    this.readonly = false;
    console.log("navParams.datalist", navParams.data);
    this.patientAction = navParams.data.patientaction
      ? navParams.data.patientaction
      : "";
    this.newMediceneData = navParams.data.medicinedata
      ? navParams.data.medicinedata
      : "";
    if (!_.isEmpty(navParams.data)) {
      this.patientData = navParams.data.patientData;
      this.medicineData = navParams.data.MedicineData;
      console.log(this.medicineData);
    }
    // this.colors = this.appSharedService.colors;
    this.data.color = "#BA68C8";
    this.medicineform = formBuilder.group({
      medicinename: ["", Validators.required],
      color: ["", Validators.required],
      activeingredients: [""],
      company: ["", Validators.required],
      dose: ["", Validators.required],
      timeday: ["", Validators.required],
      areaofuse: ["", Validators.required],
      notes: [""],

      enablenotification: [],
      enableStandardWheel: [],
    });
    /*this.data.enableStandardWheel = false;
    this.data.enablenotification = false;
    this.data.activeingredients = '';
    this.data.notes = '';*/
    this.data.enableStandardWheel = this.data.enableStandardWheel == undefined
      ? false
      : this.data.enableStandardWheel;
    this.data.enablenotification = this.data.enablenotification == undefined
      ? false
      : this.data.enablenotification;
    this.data.activeingredients = this.data.activeingredients == undefined
      ? ""
      : this.data.activeingredients;
    this.data.notes = this.data.notes == undefined ? "" : this.data.notes;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AppAddMedicineMasterPage");
    this.getHCPid();
  }

  // for choose color and set in dropdown
  prepareColorSelector() {
    setTimeout(() => {
      let buttonElements = document.querySelectorAll(
        "div.alert-radio-group button",
      );
      if (!buttonElements.length) {
        this.prepareColorSelector();
      } else {
        for (let index = 0; index < buttonElements.length; index++) {
          let buttonElement = buttonElements[index];
          let optionLabelElement = buttonElement.querySelector(
            ".alert-radio-label",
          );
          let color = optionLabelElement.innerHTML.trim();

          if (this.isHexColor(color)) {
            buttonElement.classList.add(
              "colorselect",
              "color_" + color.slice(1, 7),
            );
            if (color == this.data.color) {
              buttonElement.classList.add("colorselected");
            }
          }
        }
      }
    }, 100);
  }

  // for choose color and set in dropdown
  isHexColor(color) {
    let hexColorRegEx = /^#(?:[0-9a-fA-F]{3}){1,2}$/;
    return hexColorRegEx.test(color);
  }

  // for choose color and set in dropdown
  selectColor(color) {
    let buttonElements = document.querySelectorAll(
      "div.alert-radio-group button.colorselect",
    );
    for (let index = 0; index < buttonElements.length; index++) {
      let buttonElement = buttonElements[index];
      buttonElement.classList.remove("colorselected");
      if (buttonElement.classList.contains("color_" + color.slice(1, 7))) {
        buttonElement.classList.add("colorselected");
      }
    }
  }

  // for choose color and set in dropdown
  setColor(color) {
  }

  ImageUpload() {
    let alert = this.alertCtrl.create({
      // title: 'Image Upload',
      message: "Select Image source",
      buttons: [
        {
          text: "Upload from Library",
          handler: () => {
            this.gallery(this.camera.PictureSourceType.PHOTOLIBRARY);
          },
        },
        {
          text: "Use Camera",
          handler: () => {
            this.gallery(this.camera.PictureSourceType.CAMERA);
          },
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
          },
        },
      ],
    });
    alert.present();
  }

  gallery(sourceType: PictureSourceType) {
    let self = this;
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      quality: 50,
      correctOrientation: true,
      allowEdit: true
    }).then((imageData) => {
      // imageData is a base64 encoded string
      self.showImagePreview();
      self.data.image = "data:image/jpeg;base64," + imageData;
      self.image = "data:image/jpeg;base64," + imageData;
    }, (err) => {
      console.log("Error " + err);
    });
  }

  showImagePreview() {
    let self = this;
    self.setVisibility(self.previewImage, "block");
    self.setVisibility(self.imageUploader, "none");
    self.setVisibility(self.addPhotoCaption, "none");
    self.setVisibility(self.removePhotoIcon, "block");
  }

  removeImage() {
    let self = this;
    this.data.image = undefined;
    self.setVisibility(self.previewImage, "none");
    self.setVisibility(self.imageUploader, "block");
    self.setVisibility(self.addPhotoCaption, "block");
    self.setVisibility(self.removePhotoIcon, "none");
  }

  setVisibility(element: ElementRef, state) {
    if (element !== undefined) {
      element.nativeElement.style.display = state;
    }
  }

  closeModal() {
    this.viewCtrl.dismiss(false);
  }
  addPatientMedicineDetails(flag) {
    this.submitAttempt = true;

    if (!this.medicineform.valid) {
      this.toastCtrl.presentToast("Please fill all the required fields!");
    } else {
      let self = this;
      if (this.data.image != undefined) {
        console.log("in");
        this.data.image = this.data.image;
      } else {
        console.log("out");
        this.showImagePreview();
        this.data.image = "";
      }
      if (this.data.addtowheel == true) {
        this.data.addtowheel = "1";
      } else {
        this.data.addtowheel = "0";
      }
      if (this.data.notification == true) {
        this.data.notification = "1";
      } else {
        this.data.notification = "0";
      }
      console.log("self.data", self.data);
      if (flag == "prescription") {
        self.navCtrl.push(
          "AppMedicinePrescriptionIssuedPage",
          {
            "MedicineData": self.medicineData,
            "AddedMedicine": self.data,
            "patientData": self.patientData,
          },
        );
      } else {
        if (
          this.userLoginData.usertype == "healthcareprovider" ||
          this.patientType == "independent"
        ) {
          this.data.userid = this.userLoginData.id;
        } else {
          this.data.userid = this.hcpid;
        }
        console.log("medicine data -->", this.data);

        //self.medicineData.push(self.data);
        self.data.action = "addmedicine";
        self.data.appsecret = this.appsecret;
        self.data.doctorid = this.userLoginData.id;
        self.data.usertype = "doctor";
        self.http.post(this.baseurl + "adddata.php", self.data).subscribe(
          (data) => {
            this.loadingService.show();
            let result = JSON.parse(data["_body"]);
            console.log(result);
            if (result.status == "success") {
              self.loadingService.hide();
              self.toastCtrl.presentToast("Medicine added successfully");
              this.viewCtrl.dismiss(true);
              // return result1;
            } else {
              this.viewCtrl.dismiss(true);
              self.loadingService.hide();
              // self.toastCtrl.presentToast("There is a problem adding data, please try again");
              if (this.userLoginData.usertype == "patient") {
                //self.navCtrl.push("AppMedicinePrescriptionIssuedPage", { 'NewAddedMedicineData': result.data })
                this.viewCtrl.dismiss(true);
              } else {
                this.loadingService.hide();
                this.viewCtrl.dismiss(true);
              }
              // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
          },
          (err) => {
            this.loadingService.hide();
            console.log(err);
          },
        );
      }
    }
  }

  async getHCPid() {
    await this.get("currentHCP").then((result) => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      } else {
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
        }
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get("patientType").then((result) => {
      console.log("patienttypesdfdsfsdsdf", result);
      if (
        (!result || result == null || result == undefined) &&
        this.userLoginData == "patient"
      ) {
        this.doRefresh();
      } else {
        this.patientType = result;
        console.log("sekeccdffvfdgdfgdfg", this.patientType);
      }
    });
  }

  doRefresh() {
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      } else {
        return null;
      }
    } catch (reason) {
      return null;
    }
  }

  deleteMedicineData() {
    // item = [item];
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Are you sure you want to delete picture ?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
        },
      }, {
        text: "Delete",
        handler: () => {
          this.removeImage();
        }
      }],
    });
    alert.present();
  }

}
