import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppAddMedicineMasterPage } from './app-add-medicine-master';

@NgModule({
  declarations: [
    AppAddMedicineMasterPage,
  ],
  imports: [
    IonicPageModule.forChild(AppAddMedicineMasterPage),
  ],
})
export class AppAddMedicineMasterPageModule {}
