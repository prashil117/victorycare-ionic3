import {Component, AfterViewInit, ElementRef, ViewChild} from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams, Events} from 'ionic-angular';
import {DatabaseService} from "../../services/database-service";
import {LoadingService} from "../../services/loading-service";
import {Observable} from 'rxjs/Observable';
import * as _ from "lodash";
import {AppPatientWheelEventModalPage} from "../app-patient-wheel-event-modal/app-patient-wheel-event-modal";
import {AppSharedService} from "../../services/app-shared-service";
import {unescapeHtml} from "@angular/platform-browser/src/browser/transfer_state";
// import { AppPatientsContactsPage } from "../app-patients-contacts/app-patients-contacts";
/*import { AppMedicineMasterListPage } from '../app-medicine-master-list/app-medicine-master-list';
import { AppEventMasterListPage } from "../app-event-master-list/app-event-master-list";
import { AppActivityMasterListPage } from "../app-activity-master-list/app-activity-master-list";
import { AppPatientsContactsPage } from "../app-patients-contacts/app-patients-contacts";*/

@IonicPage()
@Component({
    selector: 'app-patient-home',
    templateUrl: 'app-patient-home.html',
    providers: [DatabaseService]
})
export class AppPatientHomePage implements AfterViewInit {
    ngAfterViewInit(): void {
        this.renderChartWithData(this.interval);
    }

    pages: Array<{title: string, component: any, icon:any}>;
    data: any = {};
    patientData: any = {};
    patientSettings: any = {};
    tableData: any = [];
    eventData: any = [];
    selectedDate: any = {};
    hasData: any = false;
    selectedRowIndex: any = -1;
    selectedTime: any = "";
    chartData: any = [];
    listView:any = false;
    @ViewChild('tableView') tableView;
    @ViewChild('chartView') chartView;
    @ViewChild('chartContainer') chartContainer;
    interval : any = 15;
    eventLegends : any = [];
    medicineLegends : any = [];
    activityLegends : any = [];
    selectedItemInfo : any = {};
    selectedSlot : any = "";
    chartDepth : any = undefined;

    constructor(public navCtrl: NavController, private navParams: NavParams, public events: Events,  private alertCtrl: AlertController, private databaseService: DatabaseService,  private loadingService: LoadingService, public modalCtrl: ModalController, public appSharedService: AppSharedService) {
        let self = this;
        if (!_.isEmpty(navParams.data)) {
            this.patientData = navParams.data.data;
            console.log('navParamsHome',navParams.data);
            self.appSharedService.patientData = self.patientData;
            self.selectedDate = new Date().toISOString();

                console.log('pationt home')
        }
        // console.log('patientData',this.patientData);
    }

    ionViewDidLoad() {
        this.events.publish('user:created', this.patientData, Date.now());
    }

    backToPatientHome() {
        this.navCtrl.setRoot("AppPatientsPage", {'data': this.patientData});
    }

    navigateToPatientSettings() {
        //this.navCtrl.push("AppPatientSettingsPage",{'data':this.patientData});
        this.navCtrl.push("AppPatientSettingsPage", {'data': this.patientData});
    }

    calculateAge(birthDate) {
        var birthday = new Date(birthDate);
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    openAddEventModal() {
        let self = this;
        let modal = this.modalCtrl.create(AppPatientWheelEventModalPage, {
            'patientSettings': this.patientSettings,
            'patientData': this.patientData,
            'selectedDate': this.selectedDate,
            'selectedTime': this.selectedTime,
            'selectedSlot': self.selectedSlot
        });

        modal.onDidDismiss((data) => {
            if(data !==null && data.action && data.action === 'save'){
                self.dateChanged();
            }
        });

        modal.present();
    }

    getTimeIntervals() {
        var times = [];
        var quarterHours = ["00", "15", "30", "45"];

        for (var i = 0; i < 24; i++) {
            for (var j = 0; j < 4; j++) {
                var time = i + ":" + quarterHours[j];
                if (i < 10) {
                    time = "0" + time;
                }
                times.push(time);
            }
        }
        return times;
    }

    setTableData() {

        this.tableData = [];
        let times = this.getTimeIntervals();

        for (let i = 0; i < times.length; i++) {
            let item: any = {};
            if (i === times.length - 1) {
                item.interval = times[i] + " - " + times[0];
            } else {
                item.interval = times[i] + " - " + times[i + 1];
            }

            if (this.eventData[i + 1] !== undefined) {
                let slotData = this.eventData[i + 1].slotGeneralData;
                item.hasData = true;
                let eventDetails = this.patientSettings.eventData[slotData.selectedEventKey];
                if (eventDetails !== undefined) {
                    item.eventName = eventDetails.name;
                    item.eventColor = eventDetails.color;
                }

            } else {

                item.hasData = false;
                item.eventName = '-';
                item.eventColor = "white";
            }

            if (this.eventData[i + 1] !== undefined) {
                let slotData = this.eventData[i + 1].slotGeneralData;
                let activityDetails = this.patientSettings.activityData[slotData.selectedActivityKey];
                if (activityDetails !== undefined) {
                    item.activityName = activityDetails.name;
                    item.activityColor = activityDetails.color;
                }

            } else {

                item.activityName = '-';
                item.activityColor = "white";
            }

            this.tableData.push(item);
        }
    }

    dateChanged() {
        this.selectedRowIndex = -1;
        let self = this;
        this.loadingService.show();
    }

    getDate(date) {
        return date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
    }

    onRowClick(item, selectedSlot) {
        this.selectedRowIndex = selectedSlot - 1;
        this.hasData = item.hasData;
        this.selectedTime = item.interval.split('-')[0];
    }

    openLoggedActivityHome() {

        this.navCtrl.setRoot("AppLoggedActivityHome", {
            'patientData': this.patientData,
            'patientSettings': this.patientSettings,
            'selectedDate': this.selectedDate,
            'selectedTime': this.selectedTime
        });
    }

    renderChart() {

        let d3 = window["d3"];
        d3.select("#vis svg").remove();
        let json = this.chartData;
        let self = this;
        var width = 600,
            height = width,
            radius = width / 2,
            x = d3.scale.linear().range([0, 2* Math.PI]),
            y = d3.scale.pow().exponent(0.4).domain([0, 1]).range([0, radius]),
            padding = 7,
            duration = 500;
        var div = d3.select("#vis");

        div.select("img").remove();

        var vis = div.append("svg")
            .attr("width", width + padding * 2)
            .attr("height", height + padding * 2)
            .append("g")
            .attr("transform", "translate(" + [radius + padding, radius + padding] + ")");

        var partition = d3.layout.partition()
            .sort(null)
            .value(function (d) {
               // return 5.8 - d.depth;
                return d.size;
            });

        var arc = d3.svg.arc()
            .startAngle(function (d) {
                return Math.max(0, Math.min(2 * Math.PI, x(d.x)));
            })
            .endAngle(function (d) {
                return Math.max(0, Math.min(2 * Math.PI, x(d.x + d.dx)));
            })
            .innerRadius(function (d) {
                return  Math.max(0, d.y ? y(d.y) : d.y);
            })
            .outerRadius(function (d) {
                return Math.max(0, y(d.y + d.dy));
            });

        var nodes = partition.nodes({children: json});
        var path = vis.selectAll("path").data(nodes);
        var text = vis.selectAll("text").data(nodes);


        path.enter().append("path")
            .attr("id", function (d, i) {
                return "path-" + i;
            })
            .attr("d", arc)
            .attr("fill-rule", "evenodd")
            .style("fill", self.colour.bind(self))
            .on("click", function(d){
                self.click(d, path,duration, padding, x, y, text, radius,arc);
            });

        var textEnter = text.enter().append("text")
            .style("fill-opacity", 1)
            .style("fill", function (d) {
                return self.brightness(d3.rgb(self.colour(d))) < 125 ? "#eee" : "#000";
            })
            .attr("text-anchor", function (d) {
                return x(d.x + d.dx / 2) > Math.PI ? "end" : "start";
            })
            .attr("dy", ".2em")
            .attr("transform", function (d) {
                var multiline = (d.name || "").split(" ").length > 1,
                    angle = x(d.x + d.dx / 2) * 180 / Math.PI - 90,
                    rotate = angle + (multiline ? -.5 : 0);
                return "rotate(" + rotate + ")translate(" + (y(d.y) + padding) + ")rotate(" + (angle > 90 ? -180 : 0) + ")";
            })
            .on("click", function(d){
                self.click(d, path,duration, padding, x, y, text, radius,arc);
            });
        textEnter.append("tspan")
            .attr("x", 0)
            .text(function (d) {
                //return d.depth ? d.name.split(" ")[0] : "";
                return d.slotName === undefined ? d.name : d.slotName;
            });
        textEnter.append("tspan")
            .attr("x", 0)
            .attr("dy", "1em")
            .text(function (d) {
              //  return d.depth ? d.name.split(" ")[1] || "" : "";
                return d.comments ===undefined || d.comments === "" ? "" : d.comments.length > 20 ? d.comments.substr(0,20) + "..." : d.comments;
            });
        text.style("visibility", function(e) {
            if(e.depth > 1){
                return "hidden";
            }
            return null;
        });

    }

    renderNewChart(){

        let self = this;

        let rootItem : any = {};
        rootItem.name="";
        rootItem.colour = "#fff";
        rootItem.children = [];
        rootItem.children = this.chartData;
        let root = rootItem;
        let d3 = window["d3"];

        d3.select("#vis svg").remove();

        const width = 600,
            height = 600,
            maxRadius = (Math.min(width, height) / 2) - 5;

        const formatNumber = d3.format(',d');

        const x = d3.scaleLinear()
            .range([0, 2 * Math.PI])
            .clamp(true);

        const y = d3.scaleSqrt()
            .range([maxRadius*.1, maxRadius]);

        const partition = d3.partition();

        const arc = d3.arc()
            .startAngle(d => x(d.x0))
            .endAngle(d => x(d.x1))
            .innerRadius(d => Math.max(0, y(d.y0)))
            .outerRadius(d => Math.max(0, y(d.y1)));

        const middleArcLine = d => {
            const halfPi = Math.PI/2;
            const angles = [x(d.x0) - halfPi, x(d.x1) - halfPi];
            const r = Math.max(0, (y(d.y0) + y(d.y1)) / 2);

            const middleAngle = (angles[1] + angles[0]) / 2;
            const invertDirection = middleAngle > 0 && middleAngle < Math.PI; // On lower quadrants write text ccw
            if (invertDirection) { angles.reverse(); }

            const path = d3.path();
            path.arc(0, 0, r, angles[0], angles[1], invertDirection);
            return path.toString();
        };

        const textFits = d => {
            const CHAR_SPACE = 6;

            const deltaAngle = x(d.x1) - x(d.x0);
            const r = Math.max(0, (y(d.y0) + y(d.y1)) / 2);
            const perimeter = r * deltaAngle;

            return arcLabel(d.data.slotName,d.data.comments).length * CHAR_SPACE < perimeter;
        };

        const svg = d3.select('#vis').append('svg')
            .style('width', '600')
            .style('height', '600')
            .attr('viewBox', `${-width / 2} ${-height / 2} ${width} ${height}`)
            .on('click', () => focusOn({})); // Reset zoom on canvas click


        root = d3.hierarchy(root);
        root.sum(function(d) { return !d.children || d.children.length === 0 ? d.size :0; });

        const slice = svg.selectAll('g.slice')
            .data(partition(root).descendants());

        slice.exit().remove();

        const newSlice = slice.enter()
            .append('g').attr('class', 'slice')
            .on('click', d => {
                d3.event.stopPropagation();
                focusOn(d);
            });

        newSlice.append('title')
            .text(d => {d.data.name + '\n' + formatNumber(d.value)});

        newSlice.append('path')
            .attr('class', 'main-arc')
            .style('fill', function(d){return d.data.colour;})
            .attr('d', arc);

        newSlice.append('path')
            .attr('class', 'hidden-arc')
            .attr('id', (_, i) => `hiddenArc${i}`)
            .attr('d', middleArcLine);

        const text = newSlice.append('text')
            .attr('display', d => textFits(d) ? null : 'none');


        text.append('textPath')
            .attr('startOffset','50%')
            .attr('xlink:href', (_, i) => `#hiddenArc${i}` )
            .text(d => arcLabel(d.data.slotName,d.data.comments));

        function arcLabel(title, subtitle){
           let labelTitle = title === undefined ? "" : title;
           let subLabelTitle = subtitle === undefined ? "" : subtitle;

           if(labelTitle !=="" && subLabelTitle !==""){
               return labelTitle +" - "+ subLabelTitle;
           }

           return labelTitle +" "+ subLabelTitle;
        }

        function focusOn(d) {

            if(_.isEmpty(d)){
                return;
            }

            if(d.depth === 1 || d.depth === 0 || d.depth === 2){
                const transition = svg.transition()
                    .duration(750)
                    .tween('scale', () => {
                        const xd = d3.interpolate(x.domain(), [d.x0, d.x1]),
                            yd = d3.interpolate(y.domain(), [d.y0, 1]);
                        return t => { x.domain(xd(t)); y.domain(yd(t)); };
                    });

                transition.selectAll('path.main-arc')
                    .attrTween('d', d => () => arc(d));

                transition.selectAll('path.hidden-arc')
                    .attrTween('d', d => () => middleArcLine(d));


                transition.selectAll('text')
                    .attrTween('display', d => () => textFits(d) ? null : 'none');

                /*transition.selectAll('text textPath')
                    .attrTween('text', d=> {
                        const CHAR_SPACE = 6;

                        const deltaAngle = x(d.x1) - x(d.x0);
                        const r = Math.max(0, (y(d.y0) + y(d.y1)) / 2);
                        const perimeter = r * deltaAngle;
                        let label = arcLabel(d.data.slotName,d.data.comments);
                        let doesFill = label.length * CHAR_SPACE < perimeter;
                        if(doesFill){
                            return label;
                        }else{
                            let offset = label.length * CHAR_SPACE - perimeter;
                            return label.substr(0,label.length - offset - 3)+"...";
                        }
                    });*/


                moveStackToFront(d);
            }else{
                // This piece of code is used to render table info
                self.selectedSlot = self.getIntervalFromSlot(d.data.slotId-1,self.interval);
                self.selectedItemInfo = {
                    'type': d.data.slotType,
                    'name':d.data.slotName,
                    'isEmpty' : d.data.hasData,
                    'comment' : d.data.comments === "" || d.data.comments === undefined ? "-" : d.data.comments,
                    'time' : d.data.startTime !==undefined && d.data.endTime !==undefined ? d.data.startTime + " - " + d.data.endTime : undefined
                };
            }

            if((d.depth > 2 || self.chartDepth === d.depth) && !_.isEmpty(d.parent)){
                self.hasData = d.data.hasData;
                self.selectedTime = d.parent.data.name;

                let modal = self.modalCtrl.create(AppPatientWheelEventModalPage, {
                    'patientSettings': self.patientSettings,
                    'patientData': self.patientData,
                    'selectedDate': self.selectedDate,
                    'selectedTime': self.selectedTime,
                    'selectedSlot': self.getIntervalFromSlot(d.data.slotId-1,self.interval)
                });

                modal.onDidDismiss((data) => {
                    if(data !==null && data.action && data.action === 'save'){
                        self.dateChanged();
                    }
                });
                modal.present();
            }

            self.chartDepth = d.depth;

            // Reset to top-level if no data point specified

            function moveStackToFront(elD) {
                svg.selectAll('.slice').filter(d => d === elD)
                    .each(function(d) {
                        this.parentNode.appendChild(this);
                        if (d.parent) { moveStackToFront(d.parent); }
                    })
            }
        }
    }

    brightness(rgb) {
        return rgb.r * .299 + rgb.g * .587 + rgb.b * .114;
    }

    colour(d) {
        let d3 = window["d3"];
        let self = this;
        return d.colour || "#fff";
    }

    click(d, path, duration, padding, x, y, text, radius, arc) {

        let d3 = window["d3"];
        let self = this;


        //If you select an Hour
        if(d.depth === 1 || d.depth === 0 || d.depth === 2){

            path.transition()
                .duration(duration)
                .attrTween("d", self.arcTween(d, x, y, radius, arc));

            // Somewhat of a hack as we rely on arcTween updating the scales.
            text.style("visibility", function(e) {
                if(d.depth===0){
                    if(e.depth > 1){
                        return "hidden";
                    }
                    return null;
                }else{
                    return d3.select(this).style("visibility", self.isParentOf(d, e) ? null : "hidden");
                }
            })
                .transition()
                .duration(duration)
                .attrTween("text-anchor", function(d) {
                    return function() {
                        return x(d.x + d.dx / 2) > Math.PI ? "end" : "start";
                    };
                })
                .attrTween("transform", function(d) {
                    console.log('transform');
                    var multiline = (d.name || "").split(" ").length > 1;
                    return function() {
                        var angle = x(d.x + d.dx / 2) * 180 / Math.PI - 90,
                            rotate = angle + (multiline ? -.5 : 0);
                        let resultAngle = angle > 90 ? -180 : 0;
                        let extraPadding = 0;

                        if(angle < 0){
                            angle = angle + 360;
                        }

                        if(d.depth > 1){
                            extraPadding = 15;
                            if(angle > 0 && angle < 90){
                                resultAngle = -100;
                            }else if(angle > 90 && angle < 180){
                                resultAngle = -80;
                            }else if(angle > 180 && angle < 270){
                                resultAngle = 90;
                            }else if(angle > 270 && angle < 360){
                                resultAngle = 100;
                            }
                        }
                        return "rotate(" + rotate + ")translate(" + (y(d.y) + padding + extraPadding) + ")rotate(" + (resultAngle) + ")";
                    };
                })
                .style("fill-opacity", function(e) { return self.isParentOf(d, e) ? 1 : 1e-6; })
                .each("end", function(e) {
                    if(d.depth===0){
                        if(e.depth > 1){
                            return "hidden";
                        }
                        return null;
                    }else{
                        return d3.select(this).style("visibility", self.isParentOf(d, e) ? null : "hidden");
                    }
                });

        }else{
            // This piece of code is used to render table info
            this.selectedSlot = self.getIntervalFromSlot(d.slotId-1,self.interval);
            this.selectedItemInfo = {
                'type': d.slotType,
                'name':d.slotName,
                'isEmpty' : d.hasData,
                'comment' : d.comments === "" || d.comments === undefined ? "-" : d.comments,
                'time' : d.startTime !==undefined && d.endTime !==undefined ? d.startTime + " - " + d.endTime : undefined
            };
        }


        if(d.depth > 2 || this.chartDepth === d.depth){
            this.hasData = d.hasData;
            this.selectedTime = d.parent.name;

            let modal = this.modalCtrl.create(AppPatientWheelEventModalPage, {
                'patientSettings': this.patientSettings,
                'patientData': this.patientData,
                'selectedDate': this.selectedDate,
                'selectedTime': this.selectedTime,
                'selectedSlot': self.getIntervalFromSlot(d.slotId-1,self.interval)
            });

            modal.onDidDismiss((data) => {
               if(data !==null && data.action && data.action === 'save'){
                   self.dateChanged();
               }
            });
            modal.present();
        }

        this.chartDepth = d.depth;
    }

    arcTween(d, x, y, radius, arc) {
        let d3 = window["d3"];
        let self = this;
        var my = self.maxY(d),
            xd = d3.interpolate(x.domain(), [d.x, d.x + d.dx]),
            yd = d3.interpolate(y.domain(), [d.y, my]),
            yr = d3.interpolate(y.range(), [d.y ? 20 : 0, radius]);
        return function(d) {
            return function(t) { x.domain(xd(t)); y.domain(yd(t)).range(yr(t)); return arc(d); };
        };
    }

    maxY(d) {
        let self = this;
        return d.children ? Math.max.apply(Math, d.children.map(self.maxY.bind(this))) : d.y + d.dy;
    }

    isParentOf(p, c) {

        let self = this;
        if (p === c) return true;
        if (p.children) {
            return p.children.some(function(d) {
                return self.isParentOf(d, c);
            });
        }
        return false;
    }

    renderChartWithData(interval) {
        this.chartData = [];
        this.eventLegends = [];
        this.medicineLegends = [];
        this.activityLegends = [];
        let size = 2400;
        let self = this;
        let slot = 1;
        for (let i = 0; i < 24; i++) {

            let item = {
                "name": i<10?"0"+i+":00":i + ":00",
                "slotName": i<10?"0"+i+":00":i + ":00",
                "colour": "#fff",
                "id": i,
                "size":size/24,
                "children": []
            };

            for (let j = interval; j <= 60; j=j+interval) {

                let event = {
                    "name": "",
                    "colour": "#fff",
                    "slotName":self.getIntervalFromSlot(slot - 1,self.interval) + " - Event",
                    "slotId": slot,
                    "slotType":"event",
                    "size":item.size / (60/self.interval),
                    "hasData":false,
                    "children":[{
                        "name": "",
                        "colour": "#fff",
                        "slotName":"",
                        "slotId":slot,
                        "comments":"Medicines",
                        "slotType":"medicine",
                        "size":item.size / (60/self.interval)
                    }]
                };

                if (this.eventData[slot] !== undefined) {
                    let slotGeneralData = this.eventData[slot].slotGeneralData;
                    let slotMedicineData = this.eventData[slot].slotMedicineData;

                    if(slotGeneralData!==undefined){
                        let eventDetails = this.patientSettings.eventData[slotGeneralData.selectedEventKey];
                        if (eventDetails !== undefined) {

                            self.pushLegends(eventDetails.name,eventDetails.color, self.eventLegends);
                            event.hasData = true;
                            event.colour = eventDetails.color;
                            event.slotName = self.getIntervalFromSlot(slot - 1,self.interval) + " - "+eventDetails.name;
                            event.slotType = "event";
                        }
                    }

                    if(slotMedicineData!==undefined && slotMedicineData.length>0){
                        event.children = [];
                        _.each(slotMedicineData,function(medicine){
                            let medicineSettings = self.patientSettings.medicineData[medicine.id];
                            if(medicineSettings!==undefined){

                                self.pushLegends(medicineSettings.salt,medicineSettings.color, self.medicineLegends);

                                let medicineSlot = {
                                    "name": "",
                                    "colour": medicineSettings.color,
                                    "hasData":true,
                                    "slotId":slot,
                                    "slotType":"medicine",
                                    "slotName":medicineSettings.salt,
                                    "comments":medicine.comments,
                                    "size":event.size/slotMedicineData.length,
                                    "children":[]
                                };
                                event.children.push(medicineSlot);

                            }else{
                                event.children.push(
                                    {
                                        "name":"",
                                        "colour":"#fff",
                                        "slotName":"",
                                        "slotId":slot,
                                        "comments":"Medicines",
                                        "slotType":"medicine",
                                        "size":event.size
                                    });
                            }
                        });
                    }
                }

                let activityItems = [];

                let medicines = event.children;
                _.each(medicines,function(medicine){
                    activityItems = [];
                    let eventSlot = self.eventData[slot];
                    let activityData = undefined;
                    if(eventSlot !==undefined && eventSlot.slotActivityData !==undefined){
                        activityData = self.getActivityMap(eventSlot.slotActivityData);

                        let prevActivityData = [];
                        let trackedItems = [];
                        let prevEventSlot = self.eventData[slot-1];
                        if(prevEventSlot!==undefined && prevEventSlot.slotActivityData !==undefined){
                            prevActivityData = self.getActivityMap(prevEventSlot.slotActivityData);
                        }

                       if(prevActivityData.length === 0){
                           trackedItems = activityData;
                       }else{

                       _.each(prevActivityData, function(prevItem){
                           let item = _.find(activityData,{'id':prevItem.id});
                           if(item !==undefined){
                               trackedItems.push(item);
                           }else{
                               trackedItems.push(
                                    {
                                        "hasData": false,
                                        "colour":"#000",
                                        "slotName": "",
                                        "comments":""
                                    }
                                );
                           }});

                           for(let i = prevActivityData.length-1;i>=0;i--){

                           }

                       _.each(trackedItems,function(currentActivityItem){
                           let item = _.find(trackedItems,{'id':currentActivityItem.id});
                           if(item !==undefined){
                               trackedItems.push(item);
                           }});

                       }

                        _.each(activityData,function (item,index) {
                            item.size = medicine.size;
                            if(index===0){
                                activityItems.push(self.getActivityWithData(slot,item,null));
                            }else if (activityData[index - 1] !== undefined) {
                                activityItems.push(self.getActivityWithData(slot,item, activityData[index - 1].activityId));
                            }
                        });

                    }else{
                        activityItems = [];
                    }

                    let root = self.childrenForParent(activityItems, null)[0];
                    if(root !==undefined){
                        activityItems = [self.transformActivity(slot,activityItems, root)];
                    }

                    medicine.children = activityItems;
                });

                item.children.push(event);
                slot++;
            }

            this.chartData.push(item);
        }
       // this.renderChart();
        this.renderNewChart();

    }

    getActivityWithData(slot,activity,parentId){
        let self = this;
        let activitySetting = self.patientSettings.activityData[activity.id];
        let activityColor = "#fff";
        if(activitySetting!==undefined){
            activityColor = activitySetting.color;
        }

        self.pushLegends(activitySetting.name,activitySetting.color, self.activityLegends);

        return {
            "name": "",
            "colour": activityColor,
            "hasData":true,
            "activityId":activity.activityId,
            "startTime":activity.startTime,
            "endTime":activity.endTime,
            "parentId":parentId,
            "size":activity.size,
            "slotType":"activity",
            "slotId":slot,
            "slotName":activitySetting.name,
            "comments":activity.comments,
            "children":[]
        };
    }


    getActivityMap(slotActivityData){
        return (<any>Object).keys(slotActivityData).map(function (key) {
            slotActivityData[key].activityId = key;
            return slotActivityData[key];
        });
    }

    onListViewToggleChange(){

        if(this.listView){
            this.tableView.nativeElement.style.display = "block";
            this.chartView.nativeElement.style.display = "none";
            this.setTableData();
        }else{
            this.tableView.nativeElement.style.display = "none";
            this.chartView.nativeElement.style.display = "block";
            this.renderChartWithData(this.interval);
        }
    }

    getIntervalFromSlot(slot,interval){
       return parseInt(""+((slot*interval)/60)) + ":"+((slot*interval)%60 ===0? (slot*interval)%60+"0":(slot*interval)%60) ;
    }

    childrenForParent(activityItems, parentId) {
        var children = [];
        for (var i = 0; i < activityItems.length; i++) {
            if (activityItems[i].parentId === parentId) {
                children.push(activityItems[i]);
            }
        }
        return children;
    }

    transformActivity(slot, activityItems, parent) {
        let self = this;
        let immediateChildren = self.childrenForParent(activityItems, parent.activityId);
        let children = [];
        for (let i = 0; i < immediateChildren.length; i++) {
            children.push(self.transformActivity(slot, activityItems, immediateChildren[i]));
        }

        return {
            "name": "",
            "colour": parent.colour,
            "hasData":parent.hasData,
            "activityId":parent.activityId,
            "parentId":parent.parentId,
            "size":parent.size,
            "slotType":"activity",
            "startTime":parent.startTime,
            "endTime":parent.endTime,
            "slotId":slot,
            "slotName":parent.slotName,
            "comments":parent.comments,
            children: children
        }
    }

    pushLegends(name,color,legendItems){
        let self = this;
        let legendItem = {'name':name,'color':color};
        if(!self.checkIfLegendExists(legendItem,legendItems)){
            legendItems.push(legendItem);
        }
    }

    checkIfLegendExists(item, legendItems){
        return _.find(legendItems, item) !== undefined;
    }
}
