import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppPatientHomePage} from "./app-patient-home";


@NgModule({
  declarations: [
    AppPatientHomePage,
  ],
  imports: [
    IonicPageModule.forChild(AppPatientHomePage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppPatientHomeModule {}
