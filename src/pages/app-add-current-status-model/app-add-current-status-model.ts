import { Component, ɵConsole } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, ViewController } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as _ from "lodash";
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
import { id } from '@swimlane/ngx-datatable/release/utils';
import * as moment from 'moment';
/**
 * Generated class for the AppAddCurrentStatusModelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-add-current-status-model',
  templateUrl: 'app-add-current-status-model.html',
  providers: [DatabaseService]
})
export class AppAddCurrentStatusModelPage {

  statusform: FormGroup;
  submitAttempt: boolean;
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  // activityData: any=[];
  CurrentStatusWheelData: any = [];
  patientData: any = {};
  SearchData: any = [];
  patientSettings: any = [];
  patientEventMasterData: any = [];
  
  
  data: any = {};
  mintime:any;
  maxtime:any;
  date: any = new Date();
  selectedCurrentStatus: any = [];
  action: any;
  userLoginData: any;
  patientType: any;
  disabled: boolean = false;
  key: any;
  WheelEventData: any = [];
  selectedEvent: any = {};
  selectedEventKey: any = {};
  TodayDate: any;
  ActivityData: any = [];
  patientActivityMasterData: any = [];
  selectedOriginalStatus: any;
  lastKey: any;
  previousKey: any = undefined;
  shadesEl: any;
  hcpid: any;
  classList: any = [];

  constructor(public storage: Storage, public navCtrl: NavController,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public navParams: NavParams,
    private databaseService: DatabaseService, public http: Http, private toastCtrl: ToastService) {
    this.action = navParams.data.action;
    console.log("this.navparms datadata", navParams.data);
    if (this.action == "edit") {
      console.log("actionnnnnnnnnnnnnn", this.action)
      this.data = navParams.data.data;
      
      this.patientSettings = navParams.data.patientSettings;
      this.patientEventMasterData = this.patientSettings['patientEventMasterData'];
      console.log("this.patientEventMasterData", this.patientEventMasterData);
      console.log("this.patientSettings", this.patientSettings);
      this.key = navParams.data.data.key;
      this.patientData = navParams.data.patient;
      this.data.enableCurrentTime = false;
      // this.selectedEvent = navParams.data.data.CurrentStatus[0];
      // this.selectedEventKey = navParams.data.data.CurrentStatus[0].selectedKey;
      this.TodayDate = navParams.data.date;
      this.mintime=navParams.data.end
      this.maxtime=navParams.data.start
      this.selectedOriginalStatus = navParams.data.data;
      this.selectedCurrentStatus.id = this.selectedOriginalStatus.eventmasterid;
      this.data.eventdataid = this.selectedOriginalStatus.eventmasterid;
      this.selectedCurrentStatus.color = this.selectedOriginalStatus.color;
      this.selectedCurrentStatus.Eventname = this.selectedOriginalStatus.Eventname;
    }
    else {
      if (!_.isEmpty(navParams.data)) {
        this.patientData = navParams.data.patientData;
        this.patientSettings = navParams.data.patientSettings;
        this.TodayDate = navParams.data.date;
        this.data.end = this.navParams.data.eventData ? this.navParams.data.eventData.endtime : '';
        this.data.start = this.navParams.data.eventData ? this.navParams.data.eventData.starttime : '';
        this.data.eventdataid = this.navParams.data.eventData ? this.navParams.data.eventData.eventid : '';
        this.selectedCurrentStatus.id = this.navParams.data.eventData ? this.navParams.data.eventData.eventid : '';
        this.patientEventMasterData = this.patientSettings['patientEventMasterData'];
        this.ActivityData = this.patientActivityMasterData;
      }
    }
    this.statusform = formBuilder.group({
      start: ['', Validators.required],
      end: ['', Validators.required],
      current: [''],
      description: ['']

    });
    this.data.enableCurrentTime = false;
  }

  ionViewDidLoad() {
    console.log("on load", this.selectedOriginalStatus)
    this.getHCPid();

  }

  setMaxTime()
  {
    console.log("end",this.data.end)
    this.mintime=this.data.end
  }
  setMinTime()
  {
    console.log("end",this.data.start)
    this.maxtime=this.data.start
  }
  closeModal() {
    this.viewCtrl.dismiss();
    this.loadWheelEvents();
  }

  notify() {
    // console.log(this.getTime())
    this.disabled = this.data.enableCurrentTime;
    if (this.data.enableCurrentTime == true) {
      this.data.start = this.getTime();
      this.data.end = this.getTime();
    } else {
      this.disabled = true;
    }
  }

  getTime() {
    let date = new Date(this.date);
    var hours = date.getHours() > 24 ? date.getHours() - 12 : date.getHours();
    var am_pm = date.getHours() >= 24;
    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    let time = hours + ":" + minutes;
    return time;
  }

  loadWheelEvents() {
    let self = this;
    let Todaydate = this.getDate();
    var webServiceData: any = {};
    webServiceData.eventaction = "geteventwheeldata";
    webServiceData.currentdate = this.TodayDate;
    webServiceData.appsecret = this.appsecret;
    webServiceData.userid = this.patientData.userid;
    console.log("webservice data", webServiceData);
    console.log("patient data", this.patientData);
    if (this.patientType == "independent") {
      webServiceData.hcpid = 0;
    }
    else {
      webServiceData.hcpid = this.hcpid;
    }
    var activity = self.http.post(this.baseurl + "wheeldata.php", webServiceData).subscribe(data => {
      let result = JSON.parse(data["_body"]);
      if (result.status == 'success') {
        this.CurrentStatusWheelData = [];
        this.CurrentStatusWheelData = result.data;
        console.log("resultdsfsdfsfsd", result.data);
      } else {
        this.CurrentStatusWheelData = [];
        // this.toastCtrl.presentToast("There is a no data");
      }
    });
  }
  getDate() {
    let date = new Date(this.date);
    return date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
  }



  selectEvent(event, item, index, previousKey) {
    console.log('item', item);
    let target = event.currentTarget;
    this.selectedCurrentStatus = [];
    if (target.classList.contains('selected')) {
      target.classList.remove('selected');
      this.selectedEvent = undefined;
      this.selectedEventKey = -1;
      // this.toastCtrl.presentToast('select on item')
    }
    else {
      this.selectedCurrentStatus.push({
        color: item.color,
        Eventname: item.name,
        selectedKey: index
      });
      console.log(target.classList);
      target.classList.contains('id.selected')
      console.log('id', target)
      let shadesEl = document.querySelector('.selected');
      if (shadesEl != undefined) {
        shadesEl.classList.remove('selected');
      }

      event.currentTarget.classList.add('selected');
      previousKey = this.selectedCurrentStatus;
      // this.selectedEventKey = this.lastKey;
      // this.lastKey = index;
      this.selectedEventKey = -1;
      this.selectedCurrentStatus.id = item.id;
      this.selectedCurrentStatus.color = item.color;
      this.selectedCurrentStatus.Eventname = item.name;
      this.selectedEventKey = index;

      console.log('selected color of previouskey', previousKey);

      console.log('kay of data', this.lastKey);
      console.log('selected key of event key', this.selectedEventKey);
    }
    console.log('selected color', this.selectedCurrentStatus);
  }

  addStatus(currentDate) {

    this.submitAttempt = true;

    if (!this.statusform.valid) {

    }
    else {
      if (this.selectedCurrentStatus.id == undefined || this.selectedCurrentStatus.id == null || this.selectedCurrentStatus.id == "") {

        const alert = this.alertCtrl.create({
          title: 'Please Select Current Status',
          buttons: ['OK']
        });
        alert.present();
      }
      else {
        let self = this;
        self.data.CurrentStatus = self.selectedCurrentStatus;
        self.CurrentStatusWheelData.push(self.data);
        var webServiceData: any = {};
        webServiceData.action = "addstatuswheeldata";
        webServiceData.appsecret = this.appsecret;
        webServiceData.userid = this.patientData.userid;
        webServiceData.eventid = self.data.CurrentStatus['id'];
        webServiceData.eventname = this.navParams.data.eventData ? this.navParams.data.eventData.eventname : self.data.CurrentStatus['Eventname'];
        webServiceData.eventcolor = self.data.CurrentStatus['color'];
        webServiceData.enableCurrentTime = self.data.enableCurrentTime;
        webServiceData.start = self.data.start;
        webServiceData.end = self.data.end;
        webServiceData.description = self.data.description;
        webServiceData.currentDate = currentDate;
        if (this.patientType == "independent") {
          webServiceData.hcpid = 0;
        }
        else if (this.userLoginData.usertype == "nurse" || this.userLoginData.usertype == "doctor" || this.userLoginData.usertype == "patient" && this.patientType == "dependent") {
          webServiceData.hcpid = this.hcpid;
        }
        else {
          webServiceData.hcpid = this.userLoginData.id;
        }
        console.log("datawheel", webServiceData);

        var activity = self.http.post(this.baseurl + "wheeldata.php", webServiceData).subscribe(data => {
          let result = JSON.parse(data["_body"]);
          if (result.status == 'success') {
            self.toastCtrl.presentToast("Event data added successfully");
            self.closeModal();
          } else {
            self.toastCtrl.presentToast("There is a problem adding data, please try again");
            self.closeModal();
          }
        });
      }
    }
  }


  updateEvent(currentDate) {
    if (this.selectedCurrentStatus.id == undefined || this.selectedCurrentStatus.id == null || this.selectedCurrentStatus.id == "" ) {
      const alert = this.alertCtrl.create({
        title: 'Please Select Current Status',
        buttons: ['OK']
      });
      alert.present();
    }
    else {
      let self = this;
      self.data.CurrentStatus = self.selectedCurrentStatus
      self.WheelEventData = self.data;

      var webServiceData: any = {};
      webServiceData.appsecret = this.appsecret;
      webServiceData.action = "updatestatuswheeldata";
      webServiceData.id = self.WheelEventData.id;
      webServiceData.userid = self.patientData.userid;
      webServiceData.eventid = self.WheelEventData.CurrentStatus.id;
      webServiceData.eventname = self.WheelEventData.CurrentStatus.Eventname;
      webServiceData.start = self.WheelEventData.start;
      webServiceData.end = self.WheelEventData.end;
      webServiceData.description = self.WheelEventData.description;
      webServiceData.enableCurrentTime = self.WheelEventData.enableCurrentTime;
      webServiceData.currentDate = currentDate;
      console.log("data", webServiceData)
      self.http.post(this.baseurl + "wheeldata.php", webServiceData).subscribe(data => {
        let result = JSON.parse(data["_body"]);
        console.log(result);
        if (result.status == 'success') {
          self.toastCtrl.presentToast("Status data updated successfully");
          self.closeModal();
        } else {
          self.toastCtrl.presentToast("There is a problem updating data");
        }
      }, err => {
        console.log(err);
      });
    }
  }

  async getHCPid() {
    await this.get('currentHCP').then(result => {
      if (result != null) {
        this.hcpid = result;
        console.log("if", this.hcpid)
        this.getuserLoginData()
      }
      else {
        this.hcpid = 0;
        this.getuserLoginData()
      }
    });
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }
  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh();
      }
      else {
        this.patientType = result;
        this.loadWheelEvents();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }

  doRefresh() {
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log('storageGET: ' + key + ': ' + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }
}
