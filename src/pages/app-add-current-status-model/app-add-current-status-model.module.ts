import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppAddCurrentStatusModelPage } from './app-add-current-status-model';

@NgModule({
  declarations: [
    AppAddCurrentStatusModelPage,
  ],
  imports: [
    IonicPageModule.forChild(AppAddCurrentStatusModelPage),
  ],
})
export class AppAddCurrentStatusModelPageModule {}
