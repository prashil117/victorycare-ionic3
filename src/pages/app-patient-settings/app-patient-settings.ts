import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { AlertController, IonicPage, NavController, MenuController, NavParams, Navbar, Tabs, Events, Menu } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from 'rxjs/Observable';
import * as _ from "lodash";
import { AppSharedService } from "../../services/app-shared-service";


@IonicPage()
@Component({
    selector: 'app-patient-settings',
    templateUrl: 'app-patient-settings.html',
    providers: [DatabaseService]
})
export class AppPatientSettingsPage {

    @ViewChild("tabs") TimelineTabs: Tabs;
    @ViewChild(Navbar) navBar: Navbar;
    data: any = {};
    patientData: any = {};
    tabs: any = [];
    prepopulated: any;
    eventData: any = [];

    constructor(public navCtrl: NavController, navParams: NavParams, public events: Events, private menu: MenuController,  private databaseService: DatabaseService, private alertCtrl: AlertController,  private loadingService: LoadingService, public appSharedService: AppSharedService) {

        if (!_.isEmpty(navParams.data)) {
            this.patientData = navParams.data.data;
            this.prepopulated = navParams.data.addprepopulated;
            console.log('this.patientData setting', this.patientData);
            console.log('this.prepopulated', this.prepopulated);
            if (this.prepopulated == 'yes') {
                this.eventData = [
                    { "color": '#AFDB07', 'name': 'Awake' },
                    { "color": '#6BB1D6', 'name': 'Sleeping' },
                    { "color": '#FAD000', 'name': 'Uncomfortable' },
                    { "color": '#E26060', 'name': 'Pain' },
                ];
                console.log('this.eventData', this.eventData); 
            }
        }   
 
        this.data.tabs = [
        { page: "AppPatientEventSettingsPage", title: "Add Wellbeing Status", params: this.patientData },
            { page: "AppPatientMedicineListPage", title: "Add Medicine", params: this.patientData },
            { page: "AppPatientActivityListPage", title: "Add Activity", params: this.patientData }];
        //this.events.publish('user:created', this.patientData, Date.now());
    }

    ionViewDidEnter() {
        // this.navBar.backButtonClick = (e: UIEvent) => {
        //     // todo something
        //     this.navCtrl.setRoot("AppPatientsPage");
        // }
        this.menu.enable(false);
        // this.menu.swipeEnable(false);
    }

    ionViewWillLeave() {
        this.menu.enable(true);
    }
    logout() {
        this.databaseService.logout();
    }

    /*ionViewWillLeave() {
        this.menu.swipeEnable(true);
    }*/
}
