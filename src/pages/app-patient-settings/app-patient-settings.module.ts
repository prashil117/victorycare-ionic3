import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { AppPatientSettingsPage } from "./app-patient-settings";
import { AppLogoutPage } from '../app-logout/app-logout';


@NgModule({
  declarations: [
    AppPatientSettingsPage
  ],
  imports: [
    IonicPageModule.forChild(AppPatientSettingsPage)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})

export class AppPatientSettingsModule { }
