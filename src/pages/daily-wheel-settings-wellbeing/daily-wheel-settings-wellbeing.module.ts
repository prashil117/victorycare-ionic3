import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyWheelSettingsWellbeingPage } from './daily-wheel-settings-wellbeing';

@NgModule({
  declarations: [
    DailyWheelSettingsWellbeingPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyWheelSettingsWellbeingPage),
  ],
})
export class DailyWheelSettingsWellbeingPageModule {}
