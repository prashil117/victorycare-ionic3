import { Component, ViewChild } from "@angular/core";
import {
  AlertController,
  IonicPage,
  NavController,
  NavParams,
  Navbar,
  ModalController,
  Item,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from "rxjs/Observable";
import { map, catchError } from "rxjs/operators";
import { Storage } from "@ionic/storage";
import * as _ from "lodash";
import { AppPatientEventModalPage } from "../app-patient-event-modal/app-patient-event-modal";
import {
  Validators,
  FormBuilder,
  FormArray,
  FormGroup,
  FormControl,
} from "@angular/forms";
import { MyApp } from "../../app/app.component";
import { Http } from "@angular/http";
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
import { AppEventViewallNotificationPage } from "../app-event-viewall-notification/app-event-viewall-notification";

/**
 * Generated class for the DailyWheelSettingsWellbeingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-daily-wheel-settings-wellbeing',
  templateUrl: 'daily-wheel-settings-wellbeing.html',
  providers: [DatabaseService]
})
export class DailyWheelSettingsWellbeingPage {

  SelectList: FormGroup;
  currentHCP: any[];
  data: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  EventData: any = [];
  SearchData: any = [];
  patientData: any = {};
  descending: boolean = false;
  order: number;
  onboard: boolean = false;
  terms: string;
  column: string = "name";
  @ViewChild(Navbar)
  navBar: Navbar;
  patientSession: boolean;
  errmsg: boolean = false;
  patientSessionData: any = [];
  patientPreSession: boolean;
  usertype: string;
  username: string;
  userid: string;
  ut: boolean;
  dropdown: any = [];
  notselected: boolean;
  selected1: any = [];
  result: any = [];
  offset: number = 0;
  userLoginData: any;
  patientType: any;
  count: number = 0;
  arrEventData = [];
  Independent: boolean = false;
  IssamePage: boolean = false;
  masterDataButton: boolean = false;
  Isaction: boolean = false;
  hcpkey: any;
  hcpid: any;
  dependent: boolean = true;
  getType: any;

  constructor(
    private loadingService: LoadingService,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private databaseService: DatabaseService,
    public storage: Storage,
    public formBuilder: FormBuilder,
    public myApp: MyApp,
    public http: Http,
    private toastCtrl: ToastService,
  ) {
    this.getType = navParams ? navParams.data.get : "noView";
    if (!_.isEmpty(navParams.data)) {
      this.patientData = navParams.data.data;
    }
    ////////// HCP DropDown Code Starts /////////////
    this.SelectList = formBuilder.group({
      currentHCP: [""],
    });
    ////////// HCP DropDown Code ends /////////////
  }

  appIntialize1() {
    this.Isview();
    if (this.userLoginData.usertype == "patient") {
      this.patientPreSession = true;
    }
    if (this.patientType == "independent") {
      this.dependent = false;
    }
    this.storage.get("patientSessionData").then((data) => {
      this.patientSessionData = data;
    });
    this.storage.get("patientSession").then((val) => {
      if (val != undefined || this.userLoginData.usertype == "patient") {
        this.patientSession = true;
      }
    });

    this.myApp.initializeApp();
    this.username = this.userLoginData.email;
    this.usertype = this.userLoginData.usertype;
    this.userid = this.userLoginData.id;
    this.ut = (this.usertype == "healthcareprovider" ? false : true);

    console.log(this.userid);
    // alert(this.usertype);
    this.dropdown = this.getHCPData(this.userid, this.usertype);

    var result = null;
    this.get("currentHCP").then((result) => {
      if (result != null) {
        this.notselected = false;
        this.selected1 = result;
      } else {
        this.notselected = true;

        this.selected1 = this.dropdown;
      }
    });
    this.loadMasterEventData();
  }

  ionViewDidLoad() {
    this.getHCPid();
    this.order = -1;
  }

  AddEvent() {
    let modal = this.modalCtrl.create(
      AppPatientEventModalPage,
      {
        "type": "add",
        "events": this.EventData,
        "patientData": this.patientData,
        "Eventmaster": "masterdata",
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.EventData = [];
        this.SearchData = [];
        this.arrEventData = [];
        this.loadMasterEventData();
      }
    });
  }
  AddPatientsEvent() {
    this.navCtrl.push(
      "AppPatientsEventPage",
      { "EventData": this.EventData, "patientData": this.patientData },
    );
  }
  loadMasterEventData(infiniteScroll?) {
    this.loadingService.show();
    this.storage.get("user").then((patientData) => {
      this.patientData = patientData;
    });
    this.storage.get("patientSession").then((val) => {
      this.storage.get("user").then((patientData) => {
        if (
          val == true && val != undefined &&
          this.userLoginData.usertype == "patient" && this.IssamePage
        ) {
          // this.patientData = patientData;
          if (this.userLoginData.usertype == "patient") {
            this.patientData = {
              id: this.userLoginData.id,
              email: this.userLoginData.email,
              name: this.userLoginData.firstname,
            };
          }
          patientData = this.patientData;
          this.data.action = "getpatienteventdata";
          this.data.userid = patientData.id;
          this.data.hcpid = this.hcpid;
          if (this.patientType == "independent") {
            this.data.userid = this.userLoginData.id;
            this.hcpid = 0;
          } else if (this.userLoginData.usertype == "heathcareprovider") {
            this.data.hcpid = this.userLoginData.id;
          }
          this.data.offset = this.offset;
          this.data.keyword = "";
          this.data.appsecret = this.appsecret;
          this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
            (data) => {
              let result = JSON.parse(data["_body"]);

              if (result.status == "success") {
                this.loadingService.hide();
                if (result.data !== null) {
                  for (var i in result.data) {
                    this.arrEventData.push(result.data[i]);
                  }
                  this.EventData = this.arrEventData;
                  this.SearchData = this.arrEventData;
                  this.count = result.count;
                  if (infiniteScroll) {
                    infiniteScroll.complete();
                  }
                } else {
                  this.loadingService.hide();
                  this.data = [];
                  this.toastCtrl.presentToast("There is a no data available.");
                  this.data.keys = [];
                  this.EventData = [];
                  this.SearchData = [];
                }
              } else {
                this.loadingService.hide();
              }
            },
            (err) => {
              this.loadingService.hide();
              console.log(err);
            },
          );
        } else {
          this.data.action = "geteventmasterdata";
          this.data.id = this.userLoginData.id;
          this.data.offset = this.offset;
          if (
            this.patientType == "independent" ||
            this.userLoginData.usertype == "healthcareprovider"
          ) {
            this.data.userid = this.userLoginData.id;
            console.log("this.userod", this.data.userid);
          } else {
            this.data.userid = this.hcpid;
          }
          this.data.keyword = "";
          this.data.appsecret = this.appsecret;
          console.log(this.data);
          this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
            (data) => {
              let result = JSON.parse(data["_body"]);
              console.log("result::::", result);
              if (result.status == "success") {
                if (result.data0 == null) {
                  this.errmsg = true;
                } else {
                  this.errmsg = false;
                }
                this.loadingService.hide();
                for (var i in result.data0) {
                  for (var j in result.data1) {
                    if (result.data0[i].id == result.data1[j].id) {
                      result.data0[i].numofdata = result.data1[j]
                        ? result.data1[j].numofdata
                        : 0;
                    } else {
                    }
                  }
                  this.arrEventData.push(result.data0[i]);
                }
                this.EventData = this.arrEventData;
                this.SearchData = this.arrEventData;
                if (result.data0) {
                  this.count = result.data0[0].count;
                } else {
                  this.count = 0;
                }
                if (infiniteScroll) {
                  infiniteScroll.complete();
                }
              } else {
                this.loadingService.hide();
                this.data = [];
                this.data.keys = [];
                this.EventData = [];
                this.SearchData = [];
                this.errmsg = true;
                this.loadingService.hide();
                this.toastCtrl.presentToast(
                  "There is a problem getting data, please try again",
                );
              }
            },
            (err) => {
              this.errmsg = true;
              this.loadingService.hide();
              console.log(err);
            },
          );
        }
      });
    });
  }

  Isview() {
    if (
      this.userLoginData.usertype == "patient" &&
      this.patientType == "independent"
    ) {
      this.Independent = true;
      this.masterDataButton = true;
      this.Isaction = true;
    } else if (
      this.userLoginData.usertype == "doctor" ||
      this.userLoginData.usertype == "nurse" ||
      this.userLoginData.usertype == "healthcareprovider"
    ) {
      this.Independent = true;
      this.masterDataButton = false;
      this.Isaction = true;
    } else {
      this.Independent = false;
      this.Isaction = false;
    }
  }

  ViewEventMaster() {
    this.patientSession = false;
    this.IssamePage = false;
    this.arrEventData = [];
    this.SearchData = [];
    this.onboard = true;
    this.EventData = [];
    this.loadMasterEventData("");
    //this.navCtrl.setRoot(AppMedicineMasterListPage);
  }
  loadMore(infiniteScroll) {
    if (this.patientData || !this.patientData) {
      if (this.EventData.length >= this.count || this.count <= 10 || this.count == 0) {
        infiniteScroll.enable(false);
      } else {
        this.offset = this.offset + 10;
        this.loadMasterEventData(infiniteScroll);
      }
    }
  }
  // ionViewDidEnter() {
  //   this.navBar.backButtonClick = (e: UIEvent) => {
  //     // todo something
  //     this.navCtrl.setRoot("AppPatientsPage");
  //   };
  // }

  sortField(fieldname) {
    // this.column = fieldname;
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }

  setFilteredNameData() {
    // if (this.terms == '') {
    //   this.doRefresh();
    // }
    if (!this.patientSession) {
      this.data.action = "geteventmasterdata";
    } else {
      this.data.action = "getpatienteventdata";
    }
    this.data.offset = 0;
    this.data.keyword = this.terms;
    this.data.appsecret = this.appsecret;
    this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);
        if (result.status == "success") {
          this.SearchData = result.data0;
        } else {
          this.SearchData = [];
        }
      },
      (err) => {
        this.SearchData = [];
      },
    );
  }
  doRefresh() {
    this.loadingService.show();
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.loadingService.hide();
    }, 2000);
  }

  setFilteredEventData() {
    this.SearchData = this.EventData.filter((Event) => {
      return Event.event.toLowerCase().indexOf(this.terms.toLowerCase()) > -1;
    });
  }

  editEvent(event, index) {
    let modal = this.modalCtrl.create(
      AppPatientEventModalPage,
      {
        "event": event,
        "patientData": this.patientData,
        "type": "edit",
        "index": index,
        onboard: this.onboard,
      },
    );
    //this.navCtrl.push("AppPatientEventModalPage", { 'event': event, 'patientData': this.patientData, 'type': 'edit', 'index': index, onboard: this.onboard });
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.arrEventData = [];
        this.SearchData = [];
        this.EventData = [];
        this.loadMasterEventData();
      }
    });
    console.log("event");
  }

  deleteEvent(item, index, patientData) {
    // item = [item];
    let self = this;
    console.log(item);
    console.log(patientData);
    console.log(this.SearchData);

    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete this record?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
          // console.log('Cancel clicked');
        },
      }, {
        text: "Delete",
        handler: () => {
          let self = this;
          this.data.action = "deleteevent";
          this.data.id = item.id;
          this.data.appsecret = this.appsecret;
          this.http.post(this.baseurl + "deletedata.php", this.data).subscribe(
            (data) => {
              this.loadingService.show();
              let result = JSON.parse(data["_body"]);
              console.log(result.data);
              if (result.status == "success") {
                this.loadingService.hide();
                self.toastCtrl.presentToast("Wellbeing is deleted successfully");
                this.arrEventData = [];
                this.SearchData = [];
                this.EventData = [];
                this.loadMasterEventData();
              } else {
                self.toastCtrl.presentToast("There is an error deleting data!");
              }
            },
            (err) => {
              this.loadingService.hide();
              console.log(err);
            },
          );
        },
      }],
    });
    alert.present();
  }

  deletePatientEvent(item, index, patientData) {
    let self = this;

    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete patients record?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
          // console.log('Cancel clicked');
        },
      }, {
        text: "Delete",
        handler: () => {
          let self = this;
          this.data.action = "deletepatientevent";
          this.data.id = item.id;
          this.data.appsecret = this.appsecret;
          this.http.post(this.baseurl + "deletedata.php", this.data).subscribe(
            (data) => {
              this.loadingService.show();
              let result = JSON.parse(data["_body"]);
              console.log(result.data);
              if (result.status == "success") {
                self.toastCtrl.presentToast("Wellbeing is deleted successfully");
                this.arrEventData = [];
                this.SearchData = [];
                this.EventData = [];
                this.loadMasterEventData();
              } else {
                self.toastCtrl.presentToast("There is an error deleting data!");
              }
            },
            (err) => {
              console.log(err);
            },
          );
        },
      }],
    });
    alert.present();
  }

  async getHCPid() {
    await this.get("currentHCP").then((result) => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      } else {
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
        }
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get("patientType").then((result) => {
      console.log("patienttypesdfdsfsdsdf", result);
      if (
        (!result || result == null || result == undefined) &&
        this.userLoginData == "patient"
      ) {
        this.doRefresh();
      } else {
        this.patientType = result;
        // this.appIntialize1();
        this.ViewEventMaster();
        console.log("sekeccdffvfdgdfgdfg", this.patientType);
      }
    });
  }

  comparer(otherArray) {
    return function (current, key) {
      return otherArray.filter(function (other, k) {
        return other.id == current.id; // && other.name == current.name
        // return other == current
      }).length == 0;
    };
  }
  logout() {
    this.databaseService.logout();
  }
  ////////// HCP DropDown Code Starts /////////////
  getHCPData(userid, usertype) {
    console.log(userid);
    console.log(usertype);
    var keys: any = [];
    var hcp1: any = [];
    let self = this;
    var webServiceData: any = {};
    webServiceData.action = "gethcpdata";
    webServiceData.id = userid;
    webServiceData.usertype = usertype;
    webServiceData.appsecret = this.appsecret;
    // self.data.id = this._cookieService.get('userid');
    self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);

        if (result.status == "success") {
          // result.data;
          hcp1 = hcp1.push(result.data);
          // hcp1 = result.data;

          // return result1;
        } else {
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      },
      (err) => {
        console.log(err);
      },
    );
    return hcp1;
  }
  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);

      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);

      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }
  // remove a single key value:
  remove(key: string) {
    this.storage.remove(key);
  }

  selectEmployee($event) {
    this.set("currentHCP", $event);
    this.doRefresh();
  }

  onEventNotify(item) {
    // this.navCtrl.push(AppEventNotificationPage, { data: item });
    let modal = this.modalCtrl.create(
      "AppPatientsEventPage",
      { eventdata1: item },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.doRefresh();
      }
    });
  }

  updateAddtoWheel(item, e) {
    this.loadingService.show();
    console.log("eventwheel", e.checked);
    console.log("eventitem", item);
    this.data.appsecret = this.appsecret;
    this.data.action = "updatestandardwheelevent";
    this.data.standardwheel = e.checked;
    this.data.id = item.id;
    this.http.post(this.baseurl + "updatedata.php", this.data).subscribe(
      (data) => {
        this.loadingService.hide();
        this.toastCtrl.presentToast("Wheel data updated");
      },
      (err) => {
        console.log("err", err);
        this.loadingService.hide();
      },
    );
  }

  onViewAllNotifications() {
    this.navCtrl.push(AppEventViewallNotificationPage);
  }

}
