import { Component, ViewChild } from "@angular/core";
import {
  AlertController,
  IonicPage,
  NavController,
  NavParams,
  Navbar,
  ModalController,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from "rxjs/Observable";
import { map, catchError } from "rxjs/operators";
import { Storage } from "@ionic/storage";
import {
  Validators,
  FormBuilder,
  FormArray,
  FormGroup,
  FormControl,
} from "@angular/forms";
import * as _ from "lodash";
import { AppPatientActivityModalPage } from "../app-patient-activity-modal/app-patient-activity-modal";
import { MyApp } from "../../app/app.component";
import { Http } from "@angular/http";
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
import { AppActivityViewallNotificationPage } from "../app-activity-viewall-notification/app-activity-viewall-notification";
import { AppPatientsActivityPage } from "../app-patients-activity/app-patients-activity";



@IonicPage()
@Component({
  selector: "page-app-activity-master-list",
  templateUrl: "app-activity-master-list.html",
  providers: [DatabaseService],
})
export class AppActivityMasterListPage {
  SelectList: FormGroup;
  currentHCP: any[];
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  data: any = {};
  offset: number = 0;
  arrActivitydata = [];
  count: number = 0;
  ActivityData: any = [];
  onboard: boolean = false;
  SearchData: any = [];
  patientData: any = {};
  descending: boolean = false;
  order: number;
  terms: string;
  column: string = "name";
  patientSession: boolean;
  patientPreSession: boolean;
  patientSessionData: any = [];
  usertype: string;
  username: string;
  userid: string;
  ut: boolean;
  dropdown: any = [];
  notselected: boolean;
  selected1: any = [];
  result: any = [];
  items: any = [];
  data1: any = [];
  newarr: any = [];
  Independent: boolean = false;
  IssamePage: boolean = false;
  masterDataButton: boolean = false;
  Isaction: boolean = false;
  hcpkey: any;
  hcpid: any;
  userLoginData: any;
  patientType: any;
  errmsg: boolean = false;
  dependent: boolean = true;

  @ViewChild(Navbar)
  navBar: Navbar;

  //  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    private loadingService: LoadingService,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private databaseService: DatabaseService,
    public storage: Storage,
    public formBuilder: FormBuilder,
    public myApp: MyApp,
    public http: Http,
    private toastCtrl: ToastService,
  ) {
    if (!_.isEmpty(navParams.data)) {
      this.patientData = navParams.data.data;
      // console.log(this.patientData);
    }
    ////////// HCP DropDown Code Starts /////////////
    this.SelectList = formBuilder.group({
      currentHCP: [""],
    });
    ////////// HCP DropDown Code ends /////////////
  }

  // }

  appIntialize1() {
    if (this.patientType == "independent") {
      this.dependent = false;
    }
    if (this.userLoginData.usertype == "patient") {
      this.patientPreSession = true;
    }
    this.storage.get("patientSessionData").then((data) => {
      this.patientSessionData = data;
    });
    this.storage.get("patientSession").then((val) => {
      if (val != undefined || this.userLoginData.usertype == "patient") {
        this.patientSession = true;
      }
    });

    this.myApp.initializeApp();
    this.username = this.userLoginData.firstname;
    this.usertype = this.userLoginData.usertype;
    this.userid = this.userLoginData.id;
    this.ut = (this.usertype == "healthcareprovider" ? false : true);

    // alert(this.usertype);
    this.dropdown = this.getHCPData(this.userid, this.usertype);

    var result = null;
    this.get("currentHCP").then((result) => {
      if (result != null) {
        this.notselected = false;
        this.selected1 = result;
      } else {
        this.notselected = true;

        this.selected1 = this.dropdown;
      }
    });
    this.loadMasterActivityData();
  }

  Isview() {
    if (
      this.userLoginData.usertype == "patient" &&
      this.patientType == "independent"
    ) {
      this.Independent = true;
      this.masterDataButton = true;
      this.Isaction = true;
    } else if (
      this.userLoginData.usertype == "doctor" ||
      this.userLoginData.usertype == "nurse" ||
      this.userLoginData.usertype == "healthcareprovider"
    ) {
      this.Independent = true;
      this.masterDataButton = false;
      this.Isaction = true;
    } else {
      this.Independent = false;
      this.Isaction = false;
    }
  }

  ViewActivityMaster() {
    this.patientSession = false;
    this.onboard = true;
    this.IssamePage = false;
    this.arrActivitydata = [];
    this.SearchData = [];
    this.ActivityData = [];
    this.loadMasterActivityData();
  }

  ionViewDidLoad() {
    this.order = -1;
    this.getHCPid();
  }
  AddActivity() {
    let modal = this.modalCtrl.create(
      AppPatientActivityModalPage,
      {
        "type": "add",
        "activities": this.ActivityData,
        "patientData": this.patientData,
        "Activitymaster": "masterdata",
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.arrActivitydata = [];
        this.SearchData = [];
        this.ActivityData = [];
        if (this.count > 10) {
          this.doRefresh("");
        } else {
          this.loadMasterActivityData();
        }
      }
    });
  }

  AddPatientsActivity(patientData) {
    // console.log(patientid);return false;
    this.navCtrl.push(
      "AppPatientsActivityPage",
      { "ActivityData": this.ActivityData, "patientData": patientData },
    );
  }

  loadMasterActivityData(infiniteScroll?) {
    this.loadingService.show();
    this.storage.get("patientSession").then((val) => {
      this.storage.get("user").then((patientData) => {
        if (
          val == true && val != undefined &&
          (this.userLoginData.usertype == "patient" && this.IssamePage)
        ) {
          if (this.userLoginData.usertype == "patient") {
            this.patientData = {
              id: this.userLoginData.id,
              email: this.userLoginData.email,
              name: this.userLoginData.firstname,
            };
            patientData = this.patientData;
          }
          this.data.action = "getpatientactivitydata";
          this.data.appsecret = this.appsecret;
          this.data.userid = patientData.id;
          this.data.hcpid = this.hcpid;
          if (this.patientType == "independent") {
            this.data.userid = this.userLoginData.id;
            this.data.hcpid = 0;
          }
          if (
            this.userLoginData.usertype == "nurse" ||
            this.userLoginData.usertype == "doctor"
          ) {
            this.data.hcpid = this.hcpid;
          }
          if (this.userLoginData.usertype == "healthcareprovider") {
            this.data.hcpid = this.userLoginData.id;
          }
          this.data.offset = this.offset;
          this.data.keyword = "";
          // console.log(this.data.id);
          this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
            (data) => {
              let result = JSON.parse(data["_body"]);
              if (infiniteScroll) {
                infiniteScroll.complete();
              }
              if (result.status == "success") {
                this.loadingService.hide();
                if (result.data !== null) {
                  this.count = result.count;
                  this.loadingService.hide();
                  console.log("this.count", result);
                  for (var i in result.data) {
                    this.arrActivitydata.push(result.data[i]);
                  }
                  this.ActivityData = this.arrActivitydata;
                  this.SearchData = this.arrActivitydata;
                } else {
                  this.loadingService.hide();
                  console.log("this.counteeeeeeeeee", result);
                  this.data = [];
                  this.data.keys = [];
                  this.ActivityData = [];
                  this.SearchData = [];
                  // this.toastCtrl.presentToast("There is a no data available.");
                }
              } else {
                this.loadingService.hide();
              }
            },
            (err) => {
              console.log("this.counteeeeeeeeee");
              this.loadingService.hide();
              console.log(err);
            },
          );
        } else {
          this.data.action = "getactivitymasterdata";
          this.data.appsecret = this.appsecret;
          this.data.offset = this.offset;
          this.data.keyword = "";
          if (
            this.patientType == "independent" ||
            this.userLoginData.usertype == "healthcareprovider"
          ) {
            this.data.userid = this.userLoginData.id;
            console.log("this.userod", this.data.userid);
          } else {
            this.data.userid = this.hcpid;
          }
          console.log("data123456", this.data);
          this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
            (data) => {
              let result = JSON.parse(data["_body"]);
              console.log("result::::", result);
              if (result.status == "success") {
                if (result.data0 == null) {
                  this.errmsg = true;
                } else {
                  this.errmsg = false;
                }
                this.loadingService.hide();
                console.log("this.count", this.count);
                for (var i in result.data0) {
                  for (var j in result.data1) {
                    if (result.data0[i].id == result.data1[j].id) {
                      result.data0[i].numofdata = result.data1[j]
                        ? result.data1[j].numofdata
                        : 0;
                    } else {
                    }
                  }
                  this.arrActivitydata.push(result.data0[i]);
                }
                this.ActivityData = this.arrActivitydata;
                this.SearchData = this.arrActivitydata;
                if (result.data0) {
                  this.count = result.data0[0].count;
                } else {
                  this.count = 0;
                }
                console.log("resultsearch::::", this.SearchData);
                if (infiniteScroll) {
                  infiniteScroll.complete();
                }
              } else {
                this.loadingService.hide();
                this.data = [];
                this.data.keys = [];
                this.ActivityData = [];
                this.SearchData = [];
                this.errmsg = true;
                this.loadingService.hide();
                this.errmsg = true;
                this.toastCtrl.presentToast(
                  "There is a problem adding data, please try again",
                );
              }
            },
            (err) => {
              this.errmsg = true;
              this.loadingService.hide();
              console.log(err);
            },
          );
        }
      });
    });
  }
  loadMore(infiniteScroll) {
    if (this.patientData || !this.patientData) {
      if (this.ActivityData.length >= this.count || this.count<=10 || this.count==0) {
        infiniteScroll.enable(false);
      } else {
        this.offset = this.offset + 10;
        this.loadMasterActivityData(infiniteScroll);
      }
    }
  }

  ionViewDidEnter() {
    this.navBar.backButtonClick = (e: UIEvent) => {
      // todo something
      this.navCtrl.setRoot("AppPatientsPage");
    };
  }

  sortField(fieldname) {
    // this.column = fieldname;
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }

  doRefresh(event) {
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.loadingService.hide();
    }, 2000);
  }

  setFilteredNameData() {
    // if (this.terms == "") {
    //   this.doRefresh("");
    // }
    if (!this.patientSession) {
      this.data.action = "getactivitymasterdata";
      console.log("dataa");
    } else {
      this.data.action = "getpatientactivitydata";
      console.log("dataappppatient");
    }
    this.data.offset = 0;
    this.data.keyword = this.terms;
    this.data.appsecret = this.appsecret;
    console.log("ddddddsdsdsdsd", this.data.keyword);
    this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);
        if (result.status == "success") {
          console.log("dddddd", result);
          this.SearchData = result.data0;
        } else {
          this.SearchData = [];
        }
      },
      (err) => {
        this.SearchData = [];
      },
    );
  }

  setFilteredActivityData() {
    this.SearchData = this.ActivityData.filter((Activity) => {
      return Activity.name.toLowerCase().indexOf(this.terms.toLowerCase()) > -1;
    });
  }

  editActivity(activity, index) {
    let modal = this.modalCtrl.create(
      "AppPatientActivityModalPage",
      {
        "activity": activity,
        "patientData": this.patientData,
        "type": "edit",
        "index": index,
        onboard: this.onboard,
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.arrActivitydata = [];
        this.SearchData = [];
        this.ActivityData = [];
        if (this.count > 10) {
          this.doRefresh("");
        } else {
          this.loadMasterActivityData();
        }
      }
    });
  }

  deleteActivity(item, index, patientData) {
    // item = [item];
    let self = this;
    item.userid = this.userLoginData.id;
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete this record?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
          // console.log('Cancel clicked');
        },
      }, {
        text: "Delete",
        handler: () => {
          let self = this;
          this.data.action = "deleteactivity";
          this.data.appsecret = this.appsecret;
          this.data.id = item.id;
          this.http.post(this.baseurl + "deletedata.php", this.data).subscribe(
            (data) => {
              let result = JSON.parse(data["_body"]);
              if (result.status == "success") {
                self.toastCtrl.presentToast("Activity is deleted successfully");
                self.navCtrl.setRoot("AppActivityMasterListPage");
              } else {
                self.toastCtrl.presentToast("There is an error deleting data!");
              }
            },
            (err) => {
              console.log(err);
            },
          );
        },
      }],
    });
    alert.present();
  }

  deletePatientsActivity(item, index, patientData) {
    let self = this;
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete Patients record?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
          // console.log('Cancel clicked');
        },
      }, {
        text: "Delete",
        handler: () => {
          let self = this;
          this.data.action = "deletepatientactivity";
          this.data.appsecret = this.appsecret;
          this.data.id = item.id;
          this.http.post(this.baseurl + "deletedata.php", this.data).subscribe(
            (data) => {
              let result = JSON.parse(data["_body"]);

              if (result.status == "success") {
                self.toastCtrl.presentToast("Activity is deleted successfully");
                self.navCtrl.setRoot("AppActivityMasterListPage");
              } else {
                self.toastCtrl.presentToast("There is an error deleting data!");
              }
            },
            (err) => {
              console.log(err);
            },
          );
        },
      }],
    });
    alert.present();
  }

  comparer(otherArray) {
    return function (current, key) {
      return otherArray.filter(function (other, k) {
        return other.id == current.id; // && other.name == current.name
        // return other == current
      }).length == 0;
    };
  }

  logout() {
    this.databaseService.logout();
  }
  ////////// HCP DropDown Code Starts /////////////
  getHCPData(userid, usertype) {
    var keys: any = [];
    var hcp1: any = [];
    let self = this;
    var webServiceData: any = {};
    webServiceData.action = "gethcpdata";
    webServiceData.appsecret = this.appsecret;
    webServiceData.id = userid;
    webServiceData.usertype = usertype;
    self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);

        if (result.status == "success") {
          // result.data;
          hcp1 = hcp1.push(result.data);
          // hcp1 = result.data;

          // return result1;
        } else {
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      },
      (err) => {
        console.log(err);
      },
    );
    return hcp1;
  }

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);

      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }

  async getHCPid() {
    await this.get("currentHCP").then((result) => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      } else {
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
        }
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get("patientType").then((result) => {
      console.log("patienttypesdfdsfsdsdf", result);
      if (
        (!result || result == null || result == undefined) &&
        this.userLoginData == "patient"
      ) {
        this.doRefresh("");
      } else {
        this.patientType = result;
        // this.appIntialize1();
        this.ViewActivityMaster();
        this.Isview();
        console.log("sekeccdffvfdgdfgdfg", this.patientType);
      }
    });
  }

  remove(key: string) {
    this.storage.remove(key);
  }

  selectEmployee($event) {
    this.set("currentHCP", $event);
    this.doRefresh("");
  }

  onActivityNotification(item) {
    let modal = this.modalCtrl.create(
      AppPatientsActivityPage,
      { activitydata1: item },
    );
    modal.onDidDismiss((data) => {
      if (data) {
        this.doRefresh("");
      }
    });
    modal.present();
  }

  updateAddtoWheel(item, e) {
    this.loadingService.show();
    console.log("eventwheel", e.checked);
    console.log("eventitem", item);
    this.data.appsecret = this.appsecret;
    this.data.action = "updatestandardwheelactivity";
    this.data.standardwheel = e.checked;
    this.data.id = item.id;
    this.http.post(this.baseurl + "updatedata.php", this.data).subscribe(
      ((data) => {
        this.loadingService.hide();
        this.toastCtrl.presentToast("Wheel data updated");
      }),
      (err) => {
        console.log("err", err);
        this.loadingService.hide();
      },
    );
  }
  onViewAllNotifications() {
    this.navCtrl.push(AppActivityViewallNotificationPage);
  }
}
