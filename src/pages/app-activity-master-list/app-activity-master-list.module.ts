import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppActivityMasterListPage } from './app-activity-master-list';
import { PipesModule } from '../../pipes/pipes.module';//<--- here

@NgModule({
  declarations: [
    AppActivityMasterListPage,
  ],
  imports: [
    IonicPageModule.forChild(AppActivityMasterListPage),
    PipesModule
  ],
})
export class AppActivityMasterListPageModule {}
