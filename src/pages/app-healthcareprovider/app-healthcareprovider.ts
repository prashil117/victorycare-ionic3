import { Component, AfterViewInit } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import {DatabaseService} from "../../services/database-service";
import {LoadingService} from "../../services/loading-service";
import {Observable} from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { CookieService } from 'angular2-cookie/services/cookies.service';

/**
 * Generated class for the AppHealthcareproviderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-healthcareprovider',
  templateUrl: 'app-healthcareprovider.html',
  providers: [DatabaseService]
})
export class AppHealthcareproviderPage {
  data: any = {};
  eventsData: any = [];
  patientData: any = {};
  SearchData: any = [];
  terms: string;
  usertype: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, public storage:Storage, private alertCtrl: AlertController, private databaseService: DatabaseService, private loadingService: LoadingService, private _cookieService:CookieService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppHealthcareproviderPage');
  }
  logout(){
      this.databaseService.logout();
  }
  ionViewWillEnter(){
  }
  setFilteredPatientsData(){
      this.SearchData = this.data.values.filter((Patient) => {
        console.log(Patient);
          return Patient.name.toLowerCase().indexOf(this.terms.toLowerCase()) > -1;
      });
  }

  deleteHealthcareProvidersData(item) {
      let alert = this.alertCtrl.create({
          title: 'Confirm Delete',
          message: 'Do you want to delete this record?',
          buttons: [
              {
                  text: 'Cancel',
                  role: 'cancel',
                  handler: () => {
                      console.log('Cancel clicked');
                  }
              },
              {
                  text: 'Delete',
                  handler: () => {
                      console.log('Delete Ok clicked');
                      let self = this;
                      setTimeout(function(){ self.ionViewWillEnter(); }, 1000);
                  }
              }
          ]
      });
      alert.present();
  }
  onAddHealthcareprovider() {
      this.navCtrl.push("AppHealthcareproviderEditPage");
  }

}
