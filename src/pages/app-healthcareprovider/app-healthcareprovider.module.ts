import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppHealthcareproviderPage } from './app-healthcareprovider';

@NgModule({
  declarations: [
    AppHealthcareproviderPage,
  ],
  imports: [
    IonicPageModule.forChild(AppHealthcareproviderPage),
  ],
})
export class AppHealthcareproviderPageModule {}
