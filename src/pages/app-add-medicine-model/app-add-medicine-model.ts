import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, ViewController } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as _ from "lodash";
import { Action } from 'rxjs/scheduler/Action';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";


@IonicPage()
@Component({
  selector: 'page-app-add-medicine-model',
  templateUrl: 'app-add-medicine-model.html',
  providers: [DatabaseService]
})
export class AppAddMedicineModelPage {

  statusform: FormGroup;
  medicineform: FormGroup;
  submitAttempt: boolean;
  MedicineWheelData: any = [];
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  patientData: any = {};
  userLoginData: any;
  patientType: any;
  SearchData: any = [];
  patientSettings: any = [];
  patientMedicineMasterData: any = [];
  data: any = {};
  date: any = new Date();
  hcpid: any;
  selectedMedicine: any = [];
  originalMedicine: any = [];
  action: any;
  key: any;
  WheelMedicineData: any = [];
  disabled: boolean = false;
  CurrentStatusWheelData: any = [];
  selectedEvent: any = {};
  selectedEventKey: any = {};
  TodayDate: any;
  previousKey: any = undefined;
  shadesEl: any;
  classList: any = [];

  constructor(public storage: Storage, public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    private databaseService: DatabaseService, public http: Http, private toastCtrl: ToastService) {
    this.action = navParams.data.action;
    if (!_.isEmpty(navParams.data)) {
      if (this.action == 'edit') {
        console.log(navParams.data.data);
        this.data = navParams.data.data;
        this.patientSettings = navParams.data.patientSettings;
        this.patientMedicineMasterData = this.patientSettings['patientMedicineMasterData'];
        this.key = navParams.data.data.key;
        this.patientData = navParams.data.patient;
        this.data.enableCurrentTime = false;
        // this.selectedEvent = navParams.data.data.Medicine[0];
        this.TodayDate = navParams.data.date;
        console.log(this.TodayDate);
        // this.selectedEventKey = navParams.data.data.Medicine[0].selectedKey;
        this.selectedEventKey = this.data.key;
        this.originalMedicine = navParams.data.data;
        this.selectedMedicine.id = this.originalMedicine.medicinemasterid;
        this.data.medicinedataid = this.originalMedicine.medicinemasterid;
        this.selectedMedicine.color = this.originalMedicine.color;
        this.selectedMedicine.medicinename = this.originalMedicine.medicineName;

      } else {
        this.TodayDate = navParams.data.date;
        this.patientData = navParams.data.patientData;
        this.data.medicinedataid = this.navParams.data.medicineData ? this.navParams.data.medicineData.medicineid : '';
        this.selectedMedicine.id = this.navParams.data.medicineData ? this.navParams.data.medicineData.medicineid : '';
        this.patientSettings = navParams.data.patientSettings;
        this.data.start = this.navParams.data.medicineData ? this.navParams.data.medicineData.starttime : ''
        this.patientMedicineMasterData = this.patientSettings['patientMedicineMasterData'];
        console.log(this.TodayDate);
      }

    }

    this.medicineform = formBuilder.group({
      start: ['', Validators.required],
      end: [''],
      current: [''],
      description: ['']

    });
    this.data.enableCurrentTime = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppAddMedicineModelPage');
    this.getHCPid();
  }

  closeModal() {
    this.viewCtrl.dismiss();
    this.MedicineWheelData = [];
    this.loadWheelMedcines()
  }

  notify() {
    this.disabled = this.data.enableCurrentTime;
    if (this.data.enableCurrentTime == true) {
      this.data.start = this.getTime();
      this.data.end = this.getTime();
    } else {
      this.disabled = false;
    }
  }

  getTime() {
    let date = new Date(this.date);
    var hours = date.getHours() > 24 ? date.getHours() - 12 : date.getHours();
    var am_pm = date.getHours() >= 24;
    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    let time = hours + ":" + minutes;
    return time;
  }
  getDate() {
    let date = new Date(this.date);
    return date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
  }



  // selectMedicine(item, index){
  //   let self = this;
  //   let medicine =_.find(this.selectedMedicine,{'color':item.color,'medicinename':item.medicinename});
  //   if(medicine!==undefined)
  //   {
  //     this.selectedMedicine.splice(_.indexOf(self.selectedMedicine,medicine),1);
  //     item.selected = true;
  //   }
  //   else
  //   {
  //     this.selectedMedicine.push({
  //       color:item.color,
  //       medicinename:item.medicinename,

  //     });
  //     item.selected = true;
  //   }
  //   console.log(this.selectedMedicine);
  // }

  selectMedicine(event, item, index, previousKey) {
    let target = event.currentTarget;
    this.selectedMedicine = [];
    if (target.classList.contains('selected')) {
      target.classList.remove('selected');
      this.selectedEvent = undefined;
      this.selectedEventKey = -1;
    } else {
      let shadesEl = document.querySelector('.selected');
      if (shadesEl != undefined) {
        shadesEl.classList.remove('selected');
      }

      event.currentTarget.classList.add('selected');
      previousKey = this.selectedMedicine;
      // this.selectedMedicine.push({
      //   color:item.color,
      //   medicinename:item.medicinename,
      //   selectedKey: index
      // });
      this.selectedEventKey = -1;
      console.log(item);
      this.selectedMedicine.id = item.id;
      this.selectedMedicine.color = item.color;
      this.selectedMedicine.medicinename = item.name;
      this.selectedEventKey = index;
    }

  }

  addMedicen(currentDate) {
    console.log(this.TodayDate);
    this.submitAttempt = true;

    if (!this.medicineform.valid) {
      this.toastCtrl.presentToast("Please fill up all the required data");
    }
    else {
      if (!this.selectedMedicine.id || !this.selectedMedicine.id == undefined) {

        const alert = this.alertCtrl.create({
          title: 'Plese Select Medicine',
          buttons: ['OK']
        });
        alert.present();
      }
      else {
        let self = this;
        this.data.end = this.data.start;
        self.data.Medicine = self.selectedMedicine;
        self.WheelMedicineData.push(self.data);

        var webServiceData: any = {};
        webServiceData.action = "addmedicinewheeldata";
        webServiceData.appsecret = this.appsecret;
        webServiceData.userid = this.patientData.userid;
        webServiceData.medicineid = self.data.Medicine['id'];
        webServiceData.medicinename = this.navParams.data.medicineData ? this.navParams.data.medicineData.starttime : self.data.Medicine['medicinename'];
        webServiceData.medicinecolor = self.data.Medicine['color'];
        webServiceData.enableCurrentTime = self.data.enableCurrentTime;
        webServiceData.start = self.data.start;
        webServiceData.end = self.data.end;
        webServiceData.description = self.data.description;
        webServiceData.currentDate = this.TodayDate;
        if (this.patientType == "independent") {
          webServiceData.hcpid = 0;
        }
        else if (this.userLoginData.usertype == "nurse" || this.userLoginData.usertype == "doctor" || this.userLoginData.usertype == "patient" && this.patientType == "dependent") {
          webServiceData.hcpid = this.hcpid;
        }
        else {
          webServiceData.hcpid = this.userLoginData.id;
        }
        console.log(webServiceData);
        var activity = self.http.post(this.baseurl + "wheeldata.php", webServiceData).subscribe(data => {
          let result = JSON.parse(data["_body"]);
          if (result.status == 'success') {
            self.toastCtrl.presentToast("Medicine data added successfully");
            self.closeModal();
          } else {
            self.toastCtrl.presentToast("There is a problem adding data, please try again");
            self.closeModal();
          }
        });
      }
    }

  }
  loadWheelMedcines() {
    let self = this;
    let Todaydate = this.getDate();
    var webServiceData: any = {};
    webServiceData.action = "getmedicinewheeldata";
    webServiceData.currentdate = this.TodayDate;
    webServiceData.appsecret = this.appsecret;
    webServiceData.userid = this.patientData.userid;
    if (this.patientType == "independent") {
      webServiceData.hcpid = 0;
    }
    else {
      webServiceData.hcpid = this.hcpid;
    }
    var activity = self.http.post(this.baseurl + "wheeldata.php", webServiceData).subscribe(data => {
      let result = JSON.parse(data["_body"]);
      if (result.status == 'success') {
        this.CurrentStatusWheelData = [];
        this.CurrentStatusWheelData = result.data;
        console.log("resultdsfsdfsfsd", result.data);
      } else {
        this.CurrentStatusWheelData = [];
        // this.toastCtrl.presentToast("There is a no data");
      }
    });
  }
  updateMedicine(currentDate) {

    if (!this.selectedMedicine.id) {
      const alert = this.alertCtrl.create({
        title: 'Plese Select Medicine',
        buttons: ['OK']
      });
      alert.present();
    }
    else {
      let self = this;
      this.data.end = this.data.start;
      self.data.Medicine = self.selectedMedicine;
      self.MedicineWheelData = self.data;
      //console.log("this.data", this.data)
      var webServiceData: any = {};
      webServiceData.action = "updatemedicinewheeldata";
      webServiceData.appsecret = this.appsecret;
      webServiceData.id = self.MedicineWheelData.id;
      webServiceData.userid = self.patientData.userid;
      webServiceData.medicineid = self.MedicineWheelData.Medicine.id;
      webServiceData.medicinename = self.MedicineWheelData.Medicine.medicinename;
      webServiceData.start = self.MedicineWheelData.start;
      webServiceData.end = self.MedicineWheelData.end;
      webServiceData.description = self.MedicineWheelData.description;
      webServiceData.enableCurrentTime = self.MedicineWheelData.enableCurrentTime;
      webServiceData.currentDate = this.TodayDate;
      console.log("this.data", webServiceData);
      self.http.post(this.baseurl + "wheeldata.php", webServiceData).subscribe(data => {
        let result = JSON.parse(data["_body"]);
        console.log(result);
        if (result.status == 'success') {
          self.toastCtrl.presentToast("Medicine data updated successfully");
          self.closeModal();
        } else {
          self.toastCtrl.presentToast("There is a problem updating data");
        }
      }, err => {
        console.log(err);
      });
    }
  }

  async getHCPid() {
    await this.get('currentHCP').then(result => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      }
      else {
        this.hcpid = 0;
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh();
      }
      else {
        this.patientType = result;
        this.loadWheelMedcines();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }

  doRefresh() {
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log('storageGET: ' + key + ': ' + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }

}
