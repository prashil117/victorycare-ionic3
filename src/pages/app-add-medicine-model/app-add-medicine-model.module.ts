import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppAddMedicineModelPage } from './app-add-medicine-model';

@NgModule({
  declarations: [
    AppAddMedicineModelPage,
  ],
  imports: [
    IonicPageModule.forChild(AppAddMedicineModelPage),
  ],
})
export class AppAddMedicineModelPageModule {}
