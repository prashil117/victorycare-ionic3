"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppReportPage = void 0;
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var database_service_1 = require("../../services/database-service");
/**
 * Generated class for the AppReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AppReportPage = /** @class */ (function () {
    function AppReportPage(navCtrl, navParams, events, DateChange) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.DateChange = DateChange;
        this.data = {};
        this.patientData = {};
        this.tabs = [];
        this.eventData = [];
        this.WheelData = [];
        this.date = new Date();
        // this.data = {}
        this.tabs = [];
        this.tabs = [
            { page: "AppWeeklyReportPage", title: "weekly", data1: this.selectedDate },
            { page: "AppMonthlyReportPage", title: "Monthly", data1: this.selectedDate },
            { page: "AppQuaterlyReportPage", title: "Quaterly", data1: this.selectedDate },
            { page: "AppYearlyReportPage", title: "Yearly", data1: this.selectedDate }
        ];
    }
    AppReportPage.prototype.ionViewDidLoad = function () {
        this.selectedDate = new Date().toISOString();
        console.log(this.navParams.data);
    };
    AppReportPage.prototype.ionViewDidLeave = function () {
        console.log("fdsf");
        setTimeout(function () {
        }, 2000);
    };
    AppReportPage.prototype.notify = function (event) {
        var _this = this;
        this.events.subscribe('app:getUser', function () {
            console.log('Publish the event!');
            _this.events.publish('app:sendUser', _this.selectedDate);
        });
        this.events.publish('app:sendUser', this.selectedDate);
    };
    AppReportPage.prototype.getTime = function () {
        var time = new Date(this.date);
        return time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
    };
    __decorate([
        core_1.ViewChild("tabs")
    ], AppReportPage.prototype, "TimelineTabs");
    __decorate([
        core_1.ViewChild(ionic_angular_1.Navbar)
    ], AppReportPage.prototype, "navBar");
    AppReportPage = __decorate([
        ionic_angular_1.IonicPage(),
        core_1.Component({
            selector: 'page-app-report',
            templateUrl: 'app-report.html',
            providers: [database_service_1.DatabaseService]
        })
    ], AppReportPage);
    return AppReportPage;
}());
exports.AppReportPage = AppReportPage;
