import { Component, ViewChild } from '@angular/core';
import { Events, IonicPage, Navbar, NavController, NavParams, Tabs } from 'ionic-angular';
import { delay } from 'rxjs/operator/delay';
import { DatabaseService } from '../../services/database-service';

/**
 * Generated class for the AppReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-report',
  templateUrl: 'app-report.html',
  providers: [DatabaseService]
})
export class AppReportPage {
  @ViewChild("tabs") TimelineTabs: Tabs;
  @ViewChild(Navbar) navBar: Navbar;
  data: any = {};
  patientData: any = {};
  tabs: any = [];
  prepopulated: any;
  eventData: any = [];
  selectedDate: any;
  WheelData: any = [];
  date: any = new Date();
  fooId: {
    firstName: "lawlesscreation",
    email: "user@email.com",
    score: number // can also set it later in the constructor
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public DateChange: DatabaseService,) {
    // this.data = {}
    this.tabs = [];
    this.tabs = [
      { page: "AppWeeklyReportPage", title: "weekly", data1: this.selectedDate },
      { page: "AppMonthlyReportPage", title: "Monthly", data1: this.selectedDate },
      { page: "AppQuaterlyReportPage", title: "Quaterly", data1: this.selectedDate },
      { page: "AppYearlyReportPage", title: "Yearly", data1: this.selectedDate }
    ];

  }

  ionViewDidLoad() {
    this.selectedDate = new Date().toISOString();
    console.log(this.navParams.data);
  }

  ionViewDidLeave()
  {
    console.log("fdsf");
    setTimeout(()=>{

    },2000)
  }
  notify(event) {
    this.events.subscribe('app:getUser', () => {
      console.log('Publish the event!');
      this.events.publish('app:sendUser', this.selectedDate);
    });
    this.events.publish('app:sendUser', this.selectedDate);

  }
  getTime() {
    let time = new Date(this.date);
    return time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
  }
}
