import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppDoctorsPage } from './app-doctors';

@NgModule({
  declarations: [
    AppDoctorsPage,
  ],
  imports: [
    IonicPageModule.forChild(AppDoctorsPage),
  ],
})
export class AppDoctorsPageModule {}
