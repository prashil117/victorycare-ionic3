import { Component, AfterViewInit } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { ToastService } from "../../services/toast-service";
import { Http } from '@angular/http';
import { environment } from "../../environment/environment";
/**
 * Generated class for the AppDoctorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-doctors',
  templateUrl: 'app-doctors.html',
  providers: [DatabaseService]
})
export class AppDoctorsPage implements AfterViewInit {

  data: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  eventsData: any = [];
  patientData: any = {};
  SearchData: any = [];
  terms: string;
  usertype: string;
  tempdata: any = [];
  title: any;
  userLoginData: any;
  patientType: any;

  useremail: any = "";
  hcpLoggedIn: boolean;
  redirect: string;
  result: any = [];
  // newdata: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, public storage: Storage, private alertCtrl: AlertController, private databaseService: DatabaseService, private loadingService: LoadingService, private toastCtrl: ToastService, public http: Http) {
    var test = [];
    console.log(this.getUsersbyStoredEmail());
    this.get('currentHCP').then(result => {
      console.log('Username1: ' + result);
    });
  }
  getUsersbyStoredEmail() {
    var var1 = [];
    var test = this.storage.get('currentHCP').then((val) => {
      var1.push(val);
    });
    console.log(var1);
    return var1;
  }

  ionViewDidLoad() {
    //this.title = this.userLoginData.email;
    console.log('ionViewDidLoad AppDoctorsPage');
    this.getuserLoginData()
  }
  logout() {
    this.databaseService.logout();
  }
  ionViewWillEnter() {
    // console.log(temp);
  }
  onSelectedPatient(item, hcpLoggedIn, userTypeToBeAdded) {
    console.log(item);
    this.navCtrl.push("AppPatientsPage", { 'data': item, 'hcpLoggedIn': hcpLoggedIn, 'userTypeToBeAdded': userTypeToBeAdded });
  }

  getusersdetails() {
    this.loadingService.show();
    let self = this;
    this.usertype = this.userLoginData ? this.userLoginData.usertype : '';
    var webServiceData: any = {};
    if (this.usertype == 'healthcareprovider') {
      
      webServiceData.action = "getpatientsfromhcplogin";
      webServiceData.usertype = "doctor";
      webServiceData.parentid = this.userLoginData.id;
      webServiceData.appsecret = this.appsecret;
      console.log(webServiceData);
      self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
        let result = JSON.parse(data["_body"]);
        console.log(result);
        if (result.status == 'success') {
          this.loadingService.hide();
          console.log(result.data);
          self.SearchData = result.data;
          self.data.values = result.data;
        } else {
          this.loadingService.hide();
          this.toastCtrl.presentToast("No data found");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      }, err => {
        this.loadingService.hide();
        console.log(err);
      });
    } else {
      webServiceData.action = "getpatientsfromhcpdata";
      self.storage.get('currentHCP').then((val) => {
        webServiceData.parentid = val;
        webServiceData.usertype = "doctor";
        webServiceData.userid = this.userLoginData.id;
        webServiceData.appsecret = this.appsecret;
        console.log(webServiceData);
        self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
          let result = JSON.parse(data["_body"]);
          console.log(result);
          if (result.status == 'success') {
            this.loadingService.hide();
            console.log(result.data);
            self.SearchData = result.data;
            self.data.values = result.data;
          } else {
            this.loadingService.hide();
            this.toastCtrl.presentToast("No data found");
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
          }
        }, err => {
          this.loadingService.hide();
          console.log(err);
        });
      });
    }
  }

  setFilteredPatientsData() {
    if (this.data.values != undefined) {
      this.SearchData = this.data.values.filter((Patient) => {
        console.log(Patient);
        return Patient.firstname.toLowerCase().indexOf(this.terms.toLowerCase()) > -1;
      });
    } else {
      this.toastCtrl.presentToast("There is no data found...");
    }
  }

  onSelectedDoctor(item) {
    // this.storage.set('user', item);
    console.log(item);
    let self = this;
    if (item !== null) {
      self.eventsData = item;
      this.navCtrl.setRoot("AppTimelinePage", { 'data': item });
    }
    else {
      self.eventsData = item;
      this.navCtrl.setRoot("AppTimelinePage", { 'data': item });
    }
  }

  editDoctor(data, editable) {
    console.log(data);
    editable = (this.userLoginData.usertype == "healthcareprovider") ? true : false;
    console.log(editable);
    this.navCtrl.push("AppDoctorEditPage", { 'data': data, 'action': 'edit', 'editable': editable, 'savebutton': editable });
  }

  deleteDoctorData(item) {
    console.log(item);  
    var item1: any = [];
    item1 = [item];
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Do you want to de-link this record?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            console.log('Delete Ok clicked');
            let self = this;

            console.log('Delete Ok clicked');
            this.data.action = "unlinkhcpfromdoctor";
            this.data.parentid = this.userLoginData.id;
            this.data.userid = item.userid;
            this.data.appsecret = this.appsecret;
            console.log(this.data);
            this.http.post(this.baseurl + "removeuser.php", this.data).subscribe(data => {
              this.loadingService.show();
              let result = JSON.parse(data["_body"]);
              console.log(result.status);
              if (result.status == 'success') {
                this.loadingService.hide();
                // self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
                self.toastCtrl.presentToast("Doctor is unlinked successfully");
                this.navCtrl.setRoot(this.navCtrl.getActive().component);
                // self.logout();
              } else {
                this.loadingService.hide();
                self.toastCtrl.presentToast("There is an error unlinking doctor!");
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
              }
            }, err => {
              this.loadingService.hide();
              console.log(err);
            });
          }
        }
      ]
    });
    alert.present();
  }

  comparer(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        return other.email == current.email // && other.name == current.name
        // return other == current
      }).length == 0;
    }
  }

  onAddDoctor(data, editable) {
    console.log(data);
    editable = (this.userLoginData.usertype == 'healthcareprovider') ? true : false;
    this.navCtrl.push("AppDoctorEditPage", { 'doctorData': data, 'editable': editable, 'savebutton': editable });
  }

  ngAfterViewInit(): void {

  }

  // set a key/value
  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log('set string in storage: ' + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh();
      }
      else {
        this.patientType = result;
        this.getusersdetails();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }

  doRefresh() {
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }

  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log('storageGET: ' + key + ': ' + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }
  // remove a single key value:
  remove(key: string) {
    this.storage.remove(key);
  }

}
