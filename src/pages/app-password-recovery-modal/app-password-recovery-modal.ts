import { Component, AfterViewInit } from '@angular/core';
import {
    AlertController, IonicPage, ModalController, NavController, NavParams, Platform,
    ViewController
} from 'ionic-angular';
import { LoadingService } from "../../services/loading-service";
import { Observable } from 'rxjs/Observable';
import * as _ from "lodash";
import { AppSharedService } from "../../services/app-shared-service";
import { Http } from '@angular/http';
import { environment } from "../../environment/environment";


@IonicPage()
@Component({
    selector: 'app-password-recovery-modal',
    templateUrl: 'app-password-recovery-modal.html',
    providers: []
})
export class AppPasswordRecoveryModalPage {

    email: string;
    userMsg: string;
    data: any = {};
    baseurl = environment.apiUrl;
    appsecret = environment.appsecret;


    constructor(public navCtrl: NavController, navParams: NavParams, private alertCtrl: AlertController, private loadingService: LoadingService, public modalCtrl: ModalController, public viewCtrl: ViewController, public appSharedService: AppSharedService, public http: Http) {
    }

    closeModal() {
        this.viewCtrl.dismiss();
    }

    public onSubmit() {
        let self = this;
        this.loadingService.show();
        console.log(this.email);

        this.data.email = this.email;
        this.data.action = "passwordreset";
        this.data.appsecret = this.appsecret;
        this.http.post(this.baseurl + "resetpassword.php", this.data).subscribe(snapshot => {
            this.loadingService.show();
            console.log(snapshot);
            let result = JSON.parse(snapshot["_body"]);
            if (result.status == 'success') {
                self.loadingService.hide();
                self.userMsg = "Please click on the link that has just been sent to your email account to change your password.";
            }
            else {
                this.loadingService.hide();
                self.userMsg = "Something went wrong. Please try again.";
            }
        },(err)=>{
            // this.loadingService.hide();
            console.log("error",err)
        });
    }
}
