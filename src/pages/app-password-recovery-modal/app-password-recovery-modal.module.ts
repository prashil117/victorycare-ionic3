import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppPasswordRecoveryModalPage} from "./app-password-recovery-modal";


@NgModule({
  declarations: [
      AppPasswordRecoveryModalPage
  ],
  imports: [
    IonicPageModule.forChild(AppPasswordRecoveryModalPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppPasswordRecoveryModalModule {}
