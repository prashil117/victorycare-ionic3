import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppPatientsPage} from "./app-patients";


@NgModule({
  declarations: [
    AppPatientsPage,
  ],
  imports: [
    IonicPageModule.forChild(AppPatientsPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppPatientsModule {}
