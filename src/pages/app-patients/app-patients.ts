import { Component, AfterViewInit } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, Events, MenuController } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { ToastService } from "../../services/toast-service";
import { Http } from '@angular/http';
import { environment } from "../../environment/environment";

@IonicPage()
@Component({
  selector: 'app-patients',
  templateUrl: 'app-patients.html',
  providers: [DatabaseService]
})
export class AppPatientsPage implements AfterViewInit {

  data: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  SearchData1: any = [];
  eventsData: any = [];
  patientData: any = {};
  SearchData: any = [];
  userLoginData: any;
  patientType: any;
  terms: string;
  doctorid: any;
  doctorEmail: any = [];
  superAdminLoggedIn: boolean;
  addExisting: boolean;
  userTypeToBeAdded: string;
  noDelete: boolean = false;
  usertype: string;
  result: any = [];
  patients: any = [];
  // currentData:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, public storage: Storage,public events: Events, private alertCtrl: AlertController, private databaseService: DatabaseService, private loadingService: LoadingService, private toastCtrl: ToastService, public http: Http) {

  }

  intializeApp1() {
    console.log('app patients');
    this.doctorEmail = this.navParams.data ? this.navParams.data.data : this.navParams.data.doctornurse;
    this.doctorid = this.navParams.data ? this.navParams.data.doctorid : '';
    this.noDelete = (this.userLoginData.usertype == 'healthcareprovider') ? true : this.navParams.data.noDelete;
    this.userTypeToBeAdded = this.navParams.data.userTypeToBeAdded;
    console.log(this.doctorEmail);
    console.log(this.userTypeToBeAdded);
    this.getusersdetails(this.userTypeToBeAdded, this.doctorEmail.userid);
  }

  getusersdetails(userTypeToBeAdded, userid) {
    this.loadingService.show();
    let self = this;
    this.usertype = this.userLoginData.usertype ? this.userLoginData.usertype : '';
    var webServiceData: any = {};
    if (this.usertype == 'healthcareprovider') {
      webServiceData.action = "getpatientfromdoctornurse";
      webServiceData.usertype = 'patient';
      webServiceData.userid = userid;
      webServiceData.parentid = this.userLoginData.id;
      webServiceData.appsecret = this.appsecret;
      console.log(webServiceData);
      self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
        let result = JSON.parse(data["_body"]);
        console.log(result);
        if (result.status == 'success') {
          this.loadingService.hide();
          console.log(result.data);
          self.SearchData1 = result.data;
          console.log("this.searchdataatasfsd", self.SearchData1);
          self.data.values = result.data;
        } else {
          this.loadingService.hide();
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      }, err => {
        this.loadingService.hide();
        console.log(err);
      });
    }
    else {
      webServiceData.action = "getpatientfromdoctornurse";
      self.storage.get('currentHCP').then((val) => {
        webServiceData.parentid = val;
        // webServiceData.usertype = "nurse";
        // webServiceData.userid = this._cookieService.get('userid');
        webServiceData.usertype = "patient";
        webServiceData.userid = userid;
        webServiceData.appsecret = this.appsecret;
        console.log(webServiceData);
        self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
          let result = JSON.parse(data["_body"]);
          console.log(result);
          if (result.status == 'success') {
            this.loadingService.hide();
            console.log(result.data);
            self.SearchData1 = result.data;
            console.log("this.searchdataatasfsd", self.SearchData1);
            self.data.values = result.data;
          } else {
            this.loadingService.hide();
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
          }
        }, err => {
          this.loadingService.hide();
          console.log(err);
        });
      });
    }
  }

  ngAfterViewInit(): void {
    this.getuserLoginData();
  }

  onSelectedPatient(item) {
    this.storage.set('user', item);
    // this.events.publish('user:created', item);
    let self = this;

  }

  onAddPatient(data, doctorsdata, addExisting, userTypeToBeAdded) {
    console.log(data);
    // this.navCtrl.push("AppPatientProfilePage",{'patientData':data});
    // this.navCtrl.push("AppPatientProfilePage");
    this.navCtrl.setRoot("AppPatientProfilePage", { 'patientData': data, 'doctorsdata': doctorsdata, 'addExisting': addExisting, 'userTypeToBeAdded': userTypeToBeAdded });
    console.log("adddpaitentexisitng", addExisting);
  }

  viewPatient(patient) {
    this.navCtrl.push("AppMyPatientProfilePage", { 'data': patient, 'action': 'edit' });
  }

  logout() {
    this.databaseService.logout();
  }

  RedirectToContacts(item) {
    this.navCtrl.push("AppPatientsContactsPage", { 'data': item });
  }

  RedirectToMedicineMasterList(item) {
    this.navCtrl.push("AppMedicineMasterListPage", { 'data': item });
  }

  RedirectToActivityMasterList(item) {
    this.navCtrl.push("AppActivityMasterListPage", { 'data': item });
  }

  RedirectToEventMasterList(item) {
    this.navCtrl.push("AppEventMasterListPage", { 'data': item });
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  setFilteredPatientsData() {
    this.SearchData1 = this.data.values.filter((Patient) => {
      return Patient.firstname.toLowerCase().indexOf(this.terms.toLowerCase()) > -1;
    });
  }

  deletePatientData(item, userid) {
    console.log(item);
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Do you want to delete this record?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            console.log('Delete Ok clicked');
            this.data.action = "unlinkhcpfromdoctor";
            this.data.parentid = userid;
            this.data.userid = item.userid;
            this.data.appsecret = this.appsecret;
            console.log(this.data);
            this.http.post(this.baseurl + "removeuser.php", this.data).subscribe(data => {
              let result = JSON.parse(data["_body"]);
              console.log(result.status);
              if (result.status == 'success') {
                // self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
                this.toastCtrl.presentToast("Patient is unlinked successfully");
                // this.navCtrl.setRoot(this.navCtrl.getActive().component);
                this.navCtrl.setRoot("AppDoctorsPage");
                // self.logout();
              } else {
                this.toastCtrl.presentToast("There is an error unlinking doctor!");
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
              }
            }, err => {
              console.log(err);
            });
          }
        }
      ]
    });
    alert.present();
  }

  comparer2(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        // console.log(other);
        return other.email == current.email //&& other.name == current.name
        // return other == current
      }).length == 0;
    }
  }

  comparer(otherArray) {
    return function (current) {
      // console.log(current);
      return otherArray.filter(function (other) {
        // console.log(other);
        return other.email != current.email
        // return other == current
      }).length == 0;
    }
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log('storageGET: ' + key + ': ' + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh('');
      }
      else {
        this.patientType = result;
        this.intializeApp1()
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');
    setTimeout(() => {
      console.log('Async operation has ended');
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }
}
