import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppActivityViewallNotificationPage } from './app-activity-viewall-notification';

@NgModule({
  declarations: [
    AppActivityViewallNotificationPage,
  ],
  imports: [
    IonicPageModule.forChild(AppActivityViewallNotificationPage),
  ],
})
export class AppActivityViewallNotificationPageModule {}
