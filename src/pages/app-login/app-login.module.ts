import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppLoginPage} from "./app-login";


@NgModule({
  declarations: [
    AppLoginPage,
  ],
  imports: [
    IonicPageModule.forChild(AppLoginPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppLoginModule {}
