import { Component, Input } from "@angular/core";
import {
  AlertController,
  IonicPage,
  ModalController,
  MenuController,
  NavController, NavParams, Platform, ModalOptions,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { AppSharedService } from "../../services/app-shared-service";
import { AppPasswordRecoveryModalPage } from "../app-password-recovery-modal/app-password-recovery-modal";
import { GooglePlus } from "@ionic-native/google-plus";
import { Storage } from "@ionic/storage";
import { Http } from "@angular/http";
import { environment } from "../../environment/environment";
import { FCM } from '@ionic-native/fcm';
import { AppTimelinePage } from "../app-timeline/app-timeline";
import { SplashScreen } from "@ionic-native/splash-screen";

@IonicPage()
@Component({
  selector: "app-login",
  templateUrl: "app-login.html",
  providers: [DatabaseService],
})
export class AppLoginPage {
  password: string = "";
  email: string = "";
  rememberme: any = false;
  page: any;
  params: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  data: any = {};
  data1: any = {};
  Registerdata: any = {};
  errorMessage: any = "";
  userInfo: any = {};
  usertype: string;
  passwordType: string = "password";
  passwordIcon: string = "eye-off";

  constructor(
    public navCtrl: NavController,
    navParams: NavParams,
    private googlePlus: GooglePlus,
    private menu: MenuController,
    private platform: Platform,
    private alertCtrl: AlertController,
    private databaseService: DatabaseService,
    private loadingService: LoadingService,
    private splashScreen: SplashScreen,
    public appSharedService: AppSharedService,
    public modalCtrl: ModalController,
    public storage: Storage,
    public http: Http,
    public fcm: FCM,
  ) {
    if (window.indexedDB) {
      console.log("I'm in WKWebView!");
    } else {
      console.log("I'm in UIWebView11213");
    }
    this.data.logo = "assets/images/logo/victory.png";
    this.storage.get("remember").then((val) => {
      if (val != undefined) {
        if (val.rememberme == true) {
          this.email = val.email;
          this.password = val.password;
          this.rememberme = val.rememberme;
        } else {
          this.storage.ready().then(() => {
            // alert("session still set");
            this.storage.remove("userLogin");
          });
          this.storage.clear();
        }
      } else {
        this.storage.ready().then(() => {
          // alert("session still set");
          this.storage.remove("userLogin");
        });
        this.storage.clear();
      }
    });
    // if (_cookieService.get('rememberme')) {
    //     this.email = this._cookieService.get('username');
    //     this.password = this._cookieService.get('password');
    //     this.rememberme = this._cookieService.get('rememberme');
    //     // this.usertype = this._cookieService.get('usertype');
    //     // this.usertype = this._cookieService.get('usertype') ? this._cookieService.get('usertype') : '';
    // }
    this.initPushNotification();
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === "text" ? "password" : "text";
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
  initPushNotification() {
    this.platform.ready().then(() => {
      this.splashScreen.hide();
      this.fcm.subscribeToTopic('marketing');
      this.fcm.getToken().then(token => {
        console.log("new token", token);
        console.log(token);
        this.data.fcmtoken = token;
      })
      this.fcm.onTokenRefresh().subscribe(token => {
        //register token
        this.data.fcmtoken = token;
        console.log("tokenrefresh", token);
      })
      this.storage.get("userLogin").then((val) => {
        if (val != null || val != undefined) {
          this.fcm.onNotification().subscribe(data => {
            if (data.wasTapped) {
              console.log("get notification foreground", data)
              var id = JSON.parse(data['gcm.notification.data']);
              console.log("helllllo", id);
              if (data.aps.alert.title == "Add event" || data.aps.alert.title == "Add activity" || data.aps.alert.title == "Add medicine") {
                if (id.eventData) {
                  this.updatenotification(id)
                  this.navCtrl.setRoot(AppTimelinePage, { 'eventnot': id.eventData, type: 'notification' });
                }
                else if (id.activityData) {
                  this.updatenotification(id)
                  this.navCtrl.setRoot(AppTimelinePage, { 'activitynot': id.activityData, type: 'notification' });
                }
                else if (id.medicineData) {
                  this.updatenotification(id)
                  this.navCtrl.setRoot(AppTimelinePage, { 'medicinenot': id.medicineData, type: 'notification' });
                }
              }
            } else {
              this.presentAlert(data)
              console.log("get notification", data)
            };
          })



          this.fcm.unsubscribeFromTopic('marketing');
        }
      });
    })
  }

  updatenotification(id) {
    var edata: any = {};
    if (id.eventData) {
      edata.eventDataid = id.eventData.eventDataid;
      edata.action = "eventnotify";
    }
    else if (id.activityData) {
      edata.activityDataid = id.activityData.activityDataid;
      edata.action = "activitynotify";
    }
    else if (id.medicineData) {
      edata.medicineDataid = id.medicineData.medicineDataid;
      edata.action = "medicinenotify";
    }
    edata.appsecret = this.appsecret;
    console.log("data", edata);
    this.http.post(this.baseurl + "updatedata.php", edata).subscribe(edata1 => {
      let result = JSON.parse(edata1["_body"]);
      console.log("status ::::>", result.status);
    })

  }
  aa() {
    var edata: any = {};
    edata.eventDataid = 21
    edata.action = "eventnotify";

    edata.appsecret = this.appsecret;
    this.http.post(this.baseurl + "updatedata.php", edata).subscribe(edata1 => {
      let result = JSON.parse(edata1["_body"]);
      console.log("status ::::>", result.status);
    })

  }
  onLogin() {
    this.aa();
    var self = this;
    // console.log('this.rememberme',this.rememberme);
    this.loadingService.show();
    console.log(this.email);
    this.data.email = this.email;
    this.data.password = this.password;
    this.data.appsecret = this.appsecret;
    this.data.action = "getdetails";
    console.log("datalogin :::>>>", this.data);
    console.log(this.baseurl);

    this.http.post(this.baseurl + "login.php", this.data).subscribe(
      (snapshot) => {
        console.log("dataaaaa", snapshot);
        let result = JSON.parse(snapshot["_body"]);
        if (result.status == "success") {
          self.storage.set("userLogin", result.data);
          var userid = result.data.id;
          if (result.data.usertype == "patient") {
            var psession = true;
            self.storage.set("patientSession", psession);
            this.data1.action = "checkindependentuser";
            this.data1.userid = userid;
            this.data1.appsecret = this.appsecret;

            this.http.post(this.baseurl + "getuser.php", this.data1).subscribe(
              (data2) => {
                console.log("dataaaaa", data2);
                let result1 = JSON.parse(data2["_body"]);
                console.log("asdasdfa", data2);
                this.storage.set("patientType", result1.status);
                if (
                  result.data.usertype == "patient" &&
                  result.data.firsttimelogin == 1
                ) {
                  this.navCtrl.setRoot("AppPatientSettingsPage");
                } else {
                  self.navCtrl.setRoot("AppTimelinePage");
                }
              },
            );
          } else {
            self.navCtrl.setRoot("AppDashboardPage");
          }
          self.errorMessage = "";

          if (this.rememberme == true) {
            var data = {
              email: this.email,
              password: this.password,
              rememberme: true,
            };
            this.storage.set("remember", data);
          } else {
            this.storage.remove("remember");
          }
        } else if (result.status == "inactive") {
          self.resetForm();
          self.loadingService.hide();
          self.errorMessage =
            "Unable to Log In: Please click on the verification link we have sent to your registered email address to verify your account.";
        } else if (result.status == "mismatch") {
          self.resetForm();
          self.loadingService.hide();
          self.errorMessage =
            "Unable to Log In: credentials dont match, please try again.";
        } else if (result.status == "notexists") {
          self.resetForm();
          self.loadingService.hide();
          self.errorMessage = "Unable to Log In: User does not exist.";
        } else {
          self.loadingService.hide();
          self.errorMessage = "Unable to fetch webservice.";
          console.log("errorrrrrrerere userid", this.data1.userid);
        }
      },
    );
  }

  async presentAlert(data) {
    console.log("dataaaaaa", data)
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      title: data.aps.alert.title,
      message: data.aps.alert.body,
      buttons: [{
        text: 'OK',
        handler: () => {
          this.openmodal(data);
        }
      }]
    });

    await alert.present();
  }

  openmodal(data) {
    console.log("hello");

    var id = JSON.parse(data['gcm.notification.data'])
    if (data.aps.alert.title == "Add event" || data.aps.alert.title == "Add activity" || data.aps.alert.title == "Add medicine") {
      if (id.eventData) {
        if (id.eventData.eventData.id) {
          this.updatenotification(id)
        }
        this.navCtrl.setRoot(AppTimelinePage, { 'eventnot': id.eventData, type: 'notification' });
      }
      else if (id.activityData) {
        if (id.activityData.activityDataid) {
          this.updatenotification(id)
        }
        this.navCtrl.setRoot(AppTimelinePage, { 'activitynot': id.activityData, type: 'notification' });
      }
      else if (id.medicineData) {
        if (id.medicineData.medicineDataid) {
          this.updatenotification(id)
        }
        this.navCtrl.setRoot(AppTimelinePage, { 'medicinenot': id.medicineData, type: 'notification' });
      }
    }
  }

  public resetForm() {
    this.email = "";
    this.password = "";
  }

  public goBack() {
    this.navCtrl.setRoot("AppWelcomeScreenPage");
  }

  public goToSignUpPage() {
    this.navCtrl.setRoot("AppRegisterPage");
  }

  public recoverPassword() {
    let modal = this.modalCtrl.create(AppPasswordRecoveryModalPage, null, {
      cssClass: "my-modal",
    });
    modal.present();
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

}
