import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppMyDoctorsPage } from './app-my-doctors';

@NgModule({
  declarations: [
    AppMyDoctorsPage,
  ],
  imports: [
    IonicPageModule.forChild(AppMyDoctorsPage),
  ],
})
export class AppMyDoctorsPageModule {}
