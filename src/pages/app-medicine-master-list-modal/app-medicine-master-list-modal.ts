import { AfterViewInit, Component, ViewChild, ElementRef } from "@angular/core";
import {
  AlertController,
  IonicPage,
  NavController,
  ModalController,
  NavParams,
  ViewController,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import * as _ from "lodash";
import { Camera, PictureSourceType } from "@ionic-native/camera";
import { AppSharedService } from "../../services/app-shared-service";
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl,
  FormArray,
} from "@angular/forms";
import { Storage } from "@ionic/storage";
import { Http } from "@angular/http";
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";

/**
 * Generated class for the AppMedicineMasterListModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.


 */

@IonicPage()
@Component({
  selector: "page-app-medicine-master-list-modal",
  templateUrl: "app-medicine-master-list-modal.html",
  providers: [DatabaseService],
})
export class AppMedicineMasterListModalPage implements AfterViewInit {
  @ViewChild("imageUploader")
  imageUploader: ElementRef;
  @ViewChild("addPhotoCaption")
  addPhotoCaption: ElementRef;
  @ViewChild("removePhotoIcon")
  removePhotoIcon: ElementRef;
  @ViewChild("previewImage")
  previewImage: ElementRef;

  // colors: Array<string> = ['#d435a2', '#a834bf', '#6011cf', '#0d0e81', '#0237f1', '#0d8bcd', '#16aca4', '#3c887e', '#157145', '#57a773', '#88aa3d', '#b7990d', '#fcbf55', '#ff8668', '#ff5c6a', '#c2454c', '#c2183f', '#d8226b', '#8f2d56', '#482971', '#000000', '#561f37', '#433835', '#797979', '#819595'];
  colors: Array<string> = [
    "#BA68C8",
    "#64B5F6",
    "#4DD0E1",
    "#4DB6AC",
    "#81C784",
    "#DCE775",
    "#FFF176",
    "#FFD54F",
    "#FFB74D",
    "#90A4AE",
    "#FFFFFF",
  ];
  Editmedicineform: FormGroup;
  submitAttempt: boolean;
  data: any = {};
  data2: any = {};
  Pagetitle: string;
  today: any;
  MedicineData: any = [];
  patientData: any = [];
  tim: any = [];
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  // colors:any = [];
  itemIndex: any = {};
  readonly: any = {};
  upload: any = {};
  MedicineType: any = {};
  ActiveStatus: any;
  patientSession: boolean;
  DetailJson: any = [];
  doctorsList: any = [];
  medicineList: any = [];
  disable: boolean = false;
  doctorType: boolean = false;
  doctorsFullName: string;
  date: any = new Date();
  doctorDetail: any;
  presAddedBy: string;
  doctorid: string;
  SearchData: any;
  image: any = "";
  action: any = "";
  userLoginData: any;
  patientType: any;
  newMedicineData: any;
  newMedicineName: string;
  type: string;
  sel: boolean = true;
  tog: boolean = false;
  onboard: any;
  doctordrop: boolean = false;
  // addexist: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private camera: Camera,
    private alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public appSharedService: AppSharedService,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    private databaseService: DatabaseService,
    public storage: Storage,
    public http: Http,
    private toastCtrl: ToastService,
  ) {
    this.Editmedicineform = formBuilder.group({
      medicinename: ["", Validators.required],
      color: ["", Validators.required],
      activeingredients: [""],
      timeday: ["", Validators.required],
      company: ["", Validators.required],
      areaofuse: ["", Validators.required],
      notes: [""],
      enablenotification: [],
      enableStandardWheel: [],
      doctor: ["", Validators.required],
      medicine: ["", Validators.required],
      dose: ["", Validators.required],
      prestartdate: ["", Validators.required],
      preenddate: ["", Validators.required],
      enablePrescriptionNotification: [],
      areaexpertise: [],
      tog: [],
      selecttime: [],
      prescriptionaddress: [],
      tim: this.formBuilder.array([
        this.initDiagnosis(),
      ]),
    });
    console.log(this.navParams.data);
    if (this.navParams.data.newMedicineData) {
      this.data.color = this.navParams.data.newMedicineData.color;
    } else {
      this.data.color = "#BA68C8";
    }

    if (this.navParams.data.MedicineData) {
      if (this.navParams.data.MedicineData.selecttime) {
        console.log("data")
        this.sel = false;
        this.tog = true;
      }
      else {
        this.sel = true;
        this.tog = false;
      }
    }
  }


  intializeApp1() {
    if (this.userLoginData.usertype == "patient" && this.patientType == "independent"
    ) {
      this.doctordrop = true;
      this.disable = false;
    } else if (
      this.userLoginData.usertype == "doctor" ||
      this.userLoginData.usertype == "nurse" ||
      this.userLoginData.usertype == "healthcareprovider"
    ) {
      this.disable = false;
    } else {
      this.disable = true;
    }
    this.presAddedBy = this.userLoginData.id;
    this.newMedicineData = this.navParams.data.newMedicineData
      ? this.navParams.data.newMedicineData
      : "";
    this.onboard = this.navParams.data.onboard
      ? this.navParams.data.onboard
      : false;

    console.log("this.newMedicendata", this.newMedicineData);
    this.newMedicineName = this.navParams.data.NewAddedMedicineName
      ? this.navParams.data.NewAddedMedicineName
      : "";
    this.type = this.navParams.data.type ? this.navParams.data.type : "";
    this.doctorType = (this.userLoginData.usertype == "doctor" ? true : false);
    if (this.userLoginData.usertype == "doctor") {
      this.doctorsFullName = this.userLoginData.firstname;
      // this.disable = true;
      this.doctorType = true;
      this.doctorid = this.userLoginData.id;
      this.doctorsList = [
        {
          id: this.userLoginData.id,
          email: this.userLoginData.email,
          name: this.userLoginData.firstname,
        },
      ];
      this.data.doctor = this.userLoginData.id + "|" +
        this.userLoginData.firstname + "|" + this.userLoginData.email;
    }
    this.upload = "assets/images/upload1.svg";
    this.readonly = false;
    // this.colors = this.appSharedService.colors;
    this.storage.get("user").then((val) => {
      console.log(val);
      this.patientData = val;
      var allPatientMedicineDetails = [];
    });
    this.storage.get("patientSession").then((val) => {
      console.log("in13e");
      if (val != undefined || this.userLoginData.usertype == "patient") {
        if (this.patientType == "dependent") {
          this.patientSession = true;
        }
        if (this.patientType == "independent") {
          if (this.navParams.data.onboard == true) {
            console.log("this.inside1");
            this.patientSession = false;
          } else if (!this.navParams.data.onboard) {
            this.patientSession = true;
          } else {
            this.patientSession = true;
          }
        } else {
          console.log("in3");
          this.patientSession = true;
        }
      }
    });
    if (this.navParams.data.type === "edit") {
      this.Pagetitle = "Edit Medicine List";
      this.patientData = this.navParams.data.patientData;
      this.itemIndex = this.navParams.data.index;
      this.data2.index = this.itemIndex;
      console.log("this.index :::>", this.data2.index);
      this.data = this.navParams.data.MedicineData
        ? this.navParams.data.MedicineData
        : this.navParams.data.newMedicineData;
      if (this.data.time) {
        var search = this.data.time;
        var n = search.search(",")
      }
      if (n != -1) {
        // var dosearr = this.data.dose ? this.data.dose.split(',') : this.data.dose;
        var timearr: any = this.data.time ? this.data.time.split(',') : this.data.dose;
        // var tmp = {}
        console.log("timearr", timearr)
        for (var i in timearr) {
          console.log("abcd", timearr[i])
          this.tim.push({ time: timearr[i] });
          console.log("abcdsdfsdf", this.tim)
        }

      }
      else {
        var timearr = this.data.time;
        this.tim.push({ time: timearr });
        console.log("this.tim", this.tim)
      }




      console.log("abcdefghijkl", this.tim);
      if (this.tim.length > 0) {
        for (let i = 0; i < this.tim.length; i++) {
          const control = <FormArray>this.Editmedicineform.controls['tim'];
          control.push(this.initDiagnosis());
          console.log("this.tim4", this.tim)
        }
      }
      else {
        console.log("this.tim5", this.tim)
        this.tim = [];
      }
      if (this.data.addtowheel == "1") {
        this.data.enableStandardWheel = true;
      } else {
        this.data.enableStandardWheel = false;
      }
      if (this.data.notification == "1") {
        this.data.enablenotification = true;
      } else {
        this.data.enablenotification = false;
      }
      console.log("data::::", this.navParams.data.MedicineData);
      this.MedicineType = this.navParams.data.type;
      // this.addexist = navParams.data.addexist;
      //  console.log(navParams.data);

      var currentDate = new Date();
      // console.log('currentDate',this.data);

      var minDate = new Date(this.data.prestartdate);
      var maxDate = new Date(this.data.preenddate);

      if (currentDate >= minDate && currentDate <= maxDate) {
        this.ActiveStatus = "active";
      } else {
        this.ActiveStatus = "inactive";
      }
      if (this.data.color) {
        this.data.color = this.data.color;
      } else {
        this.data.color = "#BA68C8";
      }
      this.showImagePreview();
    } else {
      // this.data =  navParams.data.MedicineData;
      if (this.patientType == "independent") {
        this.patientSession = false;
        console.log("patient sesiion fase");
      }
      console.log("patient sesiion fase", this.patientSession);
      this.Pagetitle = "Add Medicine";
      this.patientData = this.navParams.data.patientData;
      this.MedicineType = "";
      this.MedicineData = this.navParams.data.MedicineData;
      this.data.color = "#BA68C8";
    }
    this.storage.get("patientSession").then((val) => {
      if (val == undefined) {
        this.data.enableStandardWheel =
          this.data.enableStandardWheel == undefined
            ? false
            : this.data.enableStandardWheel;
        this.data.enablenotification = this.data.enablenotification == undefined
          ? false
          : this.data.enablenotification;
        this.data.enablePrescriptionNotification =
          this.data.enablePrescriptionNotification == undefined
            ? false
            : this.data.enablePrescriptionNotification;
        this.data.prescriptionaddress = this.data.prescaddress == undefined
          ? ""
          : this.data.prescaddress;
        this.data.areaexpertise = this.data.areaexpertise == undefined
          ? ""
          : this.data.areaexpertise;
        this.data.doctorid = this.data.doctorid == undefined
          ? ""
          : this.data.doctorid;
        this.data.activeingredients = this.data.activeingredients == undefined
          ? ""
          : this.data.activeingredients;
        this.data.notes = this.data.notes == undefined ? "" : this.data.notes;
        this.data.medicinename = this.data.medicinename == undefined
          ? ""
          : this.data.medicinename;
      } else {
        if (this.userLoginData.usertype == "doctor") {
          this.doctorType = true;
        } else {
          this.doctorType = false;
        }
        this.data2.enablePrescriptionNotification =
          this.data.enablePrescriptionNotification
            ? this.data.enablePrescriptionNotification
            : this.newMedicineData.enablePrescriptionNotification;
        this.data2.id = this.data.id ? this.data.id : this.newMedicineData.id;
        this.data2.areaexpertise = this.data.areaexpertise
          ? this.data.areaexpertise
          : this.newMedicineData.areaexpertise;
        this.data2.doctor = this.data.doctorid
          ? this.data.doctorid
          : this.newMedicineData.doctorid;
        this.data2.medicine = this.data.medicinemasterid
          ? this.data.medicinemasterid
          : this.newMedicineData.medicinemasterid;
        this.data2.prestartdate = this.data.prestartdate
          ? this.data.prestartdate
          : this.newMedicineData.prestartdate;
        this.data2.preenddate = this.data.preenddate
          ? this.data.preenddate
          : this.newMedicineData.preenddate;
        this.data2.prescriptionaddress = this.data.prescaddress
          ? this.data.prescaddress
          : this.newMedicineData.prescaddress;
        this.data2.dose = this.data.dose
          ? this.data.dose
          : this.newMedicineData.dose;
        console.log("sdsdsdsdsdsd", this.data2);

        console.log("doctor", this.data.doctor);
      }
      if (this.userLoginData.usertype == "patient") {
        console.log("this.newMedicendata3444444", this.newMedicineData);
        // if (this._cookieService.get('usertype') == 'doctor') {
        //   this.disable = true;
        // }
        // else {
        //   this.disable = false;
        // }
        this.data2.enablePrescriptionNotification =
          this.data.enablePrescriptionNotification
            ? this.data.enablePrescriptionNotification
            : this.newMedicineData.enablePrescriptionNotification;
        this.data2.id = this.data.id ? this.data.id : this.newMedicineData.id;
        this.data2.areaexpertise = this.data.areaexpertise
          ? this.data.areaexpertise
          : this.newMedicineData.areaexpertise;
        this.data2.doctor = this.data.doctorid
          ? this.data.doctorid
          : this.newMedicineData.doctorid;
        this.data2.medicine = this.data.medicinemasterid
          ? this.data.medicinemasterid
          : this.newMedicineData.medicinemasterid;
        this.data2.prestartdate = this.data.prestartdate
          ? this.data.prestartdate
          : this.newMedicineData.prestartdate;
        this.data2.preenddate = this.data.preenddate
          ? this.data.preenddate
          : this.newMedicineData.preenddate;
        this.data2.prescriptionaddress = this.data.prescaddress
          ? this.data.prescaddress
          : this.newMedicineData.prescaddress;
        this.data2.dose = this.data.dose
          ? this.data.dose
          : this.newMedicineData.dose;
        console.log("this.newMedicendatasdsdsds", this.data2);
      }
    });
    // console.log(this.data.doctor);
    let self = this;

    this.data.action = "getdoctorslist";
    this.data.usertype = "doctor";
    this.data.appsecret = this.appsecret;

    this.data.usertype = this.userLoginData.usertype;
    if (
      this.data.usertype == "healthcareprovider" ||
      this.data.usertype == "nurse"
    ) {
      this.http.post(this.baseurl + "getuser.php", this.data).subscribe(
        (data) => {
          let result = JSON.parse(data["_body"]);
          console.log(result.data);
          if (result.status == "success") {
            if (result.data !== null) {
              this.data.values = result.data;
              this.SearchData = result.data;
            } else {
              this.data = [];
              this.data.keys = [];
              this.data.values = [];
              this.SearchData = [];
            }
          } else {
            // this.toastCtrl.presentToast("There is a problem adding data, please try again");
          }
        },
        (err) => {
          console.log(err);
        },
      );
    }

    if (
      this.userLoginData.usertype == "healthcareprovider" ||
      this.userLoginData.usertype == "nurse" ||
      this.userLoginData.usertype == "patient" &&
      this.patientType == "dependent"
    ) {
      console.log("this.newMedicendatadfdfdfdf", this.newMedicineData);
      this.doctorid = this.userLoginData.id;
      var result1: any = [];
      var hcpWebServiceData: any = {};
      hcpWebServiceData.action = "getdropdowndoctorsdata";
      hcpWebServiceData.appsecret = this.appsecret;
      self.http.post(this.baseurl + "getdropdowndata.php", hcpWebServiceData)
        .subscribe((data) => {
          let result = JSON.parse(data["_body"]);
          if (result.status == "success") {
            console.log(result.data);
            console.log(this.doctorid);
            this.doctorsList = result.data;
            console.log("the", this.doctorsList);
          } else {
            this.toastCtrl.presentToast("Doctors dropdown is empty");
          }
        }, (err) => {
          console.log(err);
        });
    }
    // });

    this.data.action = "getmedicinemasterdatafulllist";
    if (
      this.patientType == "independent" ||
      this.userLoginData.usertype == "healthcareprovider"
    ) {
      this.data.action = "getmedicinemasterdatafulllist";
      this.data.userid = this.userLoginData.id;
      this.data.appsecret = this.appsecret;
      this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
        (data) => {
          let result = JSON.parse(data["_body"]);
          console.log(result.data);
          if (result.status == "success") {
            if (result.data !== null) {
              this.medicineList = result.data;
              if (this.newMedicineName) {
                this.data2.medicine = this.medicineList.find((e) =>
                  e.name === this.newMedicineName
                ).id;
              }
            } else {
              this.data = [];
              this.medicineList = [];
            }
          } else {
            // this.loadingService.hide();
            // this.toastCtrl.presentToast("There is a problem adding data, please try again");
            // this.navCtrl.setRoot("AppDashboardPage");
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
          }
        },
        (err) => {
          console.log(err);
        },
      );
    } else {
      this.storage.get("currentHCP").then((result) => {
        this.data.userid = result;
        this.data.appsecret = this.appsecret;
        this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
          (data) => {
            let result = JSON.parse(data["_body"]);
            console.log(result.data);
            if (result.status == "success") {
              if (result.data !== null) {
                this.medicineList = result.data;
                if (this.newMedicineName) {
                  this.data2.medicine = this.medicineList.find((e) =>
                    e.name === this.newMedicineName
                  ).id;
                }
              } else {
                this.data = [];
                this.medicineList = [];
              }
            } else {
              // this.loadingService.hide();
              // this.toastCtrl.presentToast("There is a problem adding data, please try again");
              // this.navCtrl.setRoot("AppDashboardPage");
              // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
          },
          (err) => {
            console.log(err);
          },
        );
      });
    }
  }

  prepareColorSelector() {
    setTimeout(() => {
      let buttonElements = document.querySelectorAll(
        "div.alert-radio-group button",
      );
      if (!buttonElements.length) {
        this.prepareColorSelector();
      } else {
        for (let index = 0; index < buttonElements.length; index++) {
          let buttonElement = buttonElements[index];
          let optionLabelElement = buttonElement.querySelector(
            ".alert-radio-label",
          );
          let color = optionLabelElement.innerHTML.trim();

          if (this.isHexColor(color)) {
            buttonElement.classList.add(
              "colorselect",
              "color_" + color.slice(1, 7),
            );
            if (color == this.data.color) {
              buttonElement.classList.add("colorselected");
            }
          }
        }
      }
    }, 100);
  }

  // for choose color and set in dropdown
  isHexColor(color) {
    let hexColorRegEx = /^#(?:[0-9a-fA-F]{3}){1,2}$/;
    return hexColorRegEx.test(color);
  }

  // for choose color and set in dropdown
  selectColor(color) {
    let buttonElements = document.querySelectorAll(
      "div.alert-radio-group button.colorselect",
    );
    for (let index = 0; index < buttonElements.length; index++) {
      let buttonElement = buttonElements[index];
      buttonElement.classList.remove("colorselected");
      if (buttonElement.classList.contains("color_" + color.slice(1, 7))) {
        buttonElement.classList.add("colorselected");
      }
    }
  }

  // for choose color and set in dropdown
  setColor(color) {
    console.log("Selected Color is", color);
  }

  getDates(startDate = "", endDate = "") {
    var dates = [],
      currentDate = startDate,
      addDays = function (days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
      };
    while (currentDate <= endDate) {
      dates.push(currentDate);
      currentDate = addDays.call(currentDate, 1);
    }
    return dates;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AppMedicineMasterListModalPage");
    this.getuserLoginData();
  }

  ngAfterViewInit(): void {
    if (!_.isEmpty(this.MedicineType)) {
      this.showImagePreview();
    }
  }

  closeModal() {
    this.viewCtrl.dismiss(false);
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ["Dismiss"],
    });
    alert.present();
  }

  updateMedicinePrescriptionData(patientData, index) {
    console.log(this.data2);
    this.submitAttempt = true;
    let self = this;
    if (this.data.image != undefined) {
      console.log("in");
      this.data.image = this.data.image;
    } else {
      console.log("out");
      this.showImagePreview();
      this.data.image = "";
    }
    this.data2.addedBy = this.presAddedBy;
    this.data2.createDate = this.getDates();
    this.data2.isDeleted = 0;
    this.data2.medicineid = this.data2.id
      ? this.data2.id
      : this.newMedicineData.id;
    this.data2.enablePrescriptionNotification =
      this.data2.enablePrescriptionNotification;
    this.data2.enablenotification = this.data.notification;
    this.data2.enableStandardWheel = this.data.addtowheel;
    var dose = Array.prototype.map.call(this.tim, s => s.dose).toString();
    var time = Array.prototype.map.call(this.tim, s => s.time).toString();
    console.log("time", time)
    this.data2.dose = dose;
    this.data2.time = time;
    console.log("data", self.data2);
    self.data2.action = "updatepatientsmedicine";
    self.data2.appsecret = this.appsecret;
    console.log(self.data2);
    self.http.post(this.baseurl + "updatedata.php", self.data2).subscribe(
      (data) => {
        console.log(data);
        let result = JSON.parse(data["_body"]);
        console.log(result.status);
        if (result.status == "success") {
          self.toastCtrl.presentToast("Medicine is updated successfully");
          this.viewCtrl.dismiss(true);
        } else {
          this.viewCtrl.dismiss(false);
          self.toastCtrl.presentToast("There is an error saving data!");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
        }
      },
      (err) => {
        console.log(err);
      },
    );
  }

  getDate() {
    let date = new Date(this.date);
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" +
      date.getDate();
    // return date.getDate();
  }

  updateMedicineData() {
    this.submitAttempt = true;
    let self = this;

    if (this.data.image != undefined) {
      console.log("in");
      this.data.image = this.data.image;
    } else {
      console.log("out");
      this.showImagePreview();
      this.data.image = "";
    }
    if (this.data.enableStandardWheel == true) {
      this.data.addtowheel = "1";
    } else {
      this.data.addtowheel = "0";
    }
    if (this.data.enablenotification == true) {
      this.data.notification = "1";
    } else {
      this.data.enablenotification = "0";
    }
    if (!this.tog) {
      this.data.selecttime = null;
    }
    console.log("ABCDEFGHIJKLMNOPQRSTUV");
    self.data.doses = self.data.dose;
    self.MedicineData.push(self.data);
    console.log("self.datanew", self.data);
    self.data.action = "updatemedicine";
    console.log(self.data);
    self.data.appsecret = this.appsecret;
    self.http.post(this.baseurl + "updatedata.php", self.data).subscribe(
      (data) => {
        console.log(data);
        let result = JSON.parse(data["_body"]);
        console.log(result.status);
        if (result.status == "success") {
          self.toastCtrl.presentToast("Medicine is updated successfully");
          if (this.patientType == "independent") {
            this.viewCtrl.dismiss(true);
          } else {
            self.navCtrl.setRoot("AppMedicineMasterListPage");
          }
        } else {
          self.toastCtrl.presentToast("There is an error saving data!");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
        }
      },
      (err) => {
        console.log(err);
      },
    );
  }

  saveMedicineData() {
    this.submitAttempt = true;
    let self = this;
    if (!this.data.name || !this.data.dose || !this.data.timesperday) {
      return
    }
    if (this.data.image != undefined) {
      console.log("in");
      this.data.image = this.data.image;
    } else {
      console.log("out");
      this.showImagePreview();
      this.data.image = "";
    }

    if (this.data.enableStandardWheel == true) {
      this.data.enableStandardWheel = "1";
    } else {
      this.data.enableStandardWheel = "0";
    }
    if (this.data.enablenotification == true) {
      this.data.enablenotification = "1";
    } else {
      this.data.enablenotification = "0";
    }
    if (!this.tog) {
      this.data.selecttime = null;
    }
    self.data.userid = this.userLoginData.id;
    console.log("self.datanew", self.data);
    self.data.action = "addmedicine";
    console.log(self.data);
    self.data.appsecret = this.appsecret;
    self.http.post(this.baseurl + "adddata.php", self.data).subscribe(
      (data) => {
        console.log(data);
        let result = JSON.parse(data["_body"]);
        console.log(result.status);
        if (result.status == "success") {
          self.toastCtrl.presentToast("Medicine is updated successfully");
          this.viewCtrl.dismiss(true);
        } else {
          self.toastCtrl.presentToast("There is an error saving data!");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
        }
      },
      (err) => {
        console.log(err);
      },
    );
  }

  ImageUpload() {
    let alert = this.alertCtrl.create({
      // title: 'Image Upload',
      message: "Select Image source",
      buttons: [
        {
          text: "Upload from Library",
          handler: () => {
            this.gallery(this.camera.PictureSourceType.PHOTOLIBRARY);
          },
        },
        {
          text: "Use Camera",
          handler: () => {
            this.gallery(this.camera.PictureSourceType.CAMERA);
          },
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
          },
        },
      ],
    });
    alert.present();
  }

  gallery(sourceType: PictureSourceType) {
    let self = this;
    this.camera.getPicture({
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      correctOrientation: true,
      allowEdit: true
    }).then((imageData) => {
      // imageData is a base64 encoded string
      self.showImagePreview();
      self.data.image = "data:image/jpeg;base64," + imageData;
      self.image = "data:image/jpeg;base64," + imageData;
    }, (err) => {
      console.log("Error " + err);
    });
  }

  showImagePreview() {
    let self = this;
    self.setVisibility(self.previewImage, "block");
    self.setVisibility(self.imageUploader, "none");
    self.setVisibility(self.addPhotoCaption, "none");
    self.setVisibility(self.removePhotoIcon, "block");
  }

  removeImage() {
    let self = this;
    this.data.image = undefined;
    self.setVisibility(self.previewImage, "none");
    self.setVisibility(self.imageUploader, "block");
    self.setVisibility(self.addPhotoCaption, "block");
    self.setVisibility(self.removePhotoIcon, "none");
  }

  setVisibility(element: ElementRef, state) {
    if (element !== undefined) {
      element.nativeElement.style.display = state;
    }
  }

  onChangeAddnewMedicine(e) {
    console.log("event", e);
    // if (e === "other") {
    //   this.navCtrl.push("AppAddMedicineMasterPage", { 'patientaction': 'edit', 'medicinedata': this.data });
    //   this.data2.medicine = this.medicineList.find(e => e.id === this.data.medicinemasterid).id;
    // }
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      return null;
    }
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get("patientType").then((result) => {
      console.log("patienttypesdfdsfsdsdf", result);
      if (
        (!result || result == null || result == undefined) &&
        this.userLoginData == "patient"
      ) {
        this.doRefresh();
      } else {
        this.patientType = result;
        this.intializeApp1();
        console.log("sekeccdffvfdgdfgdfg", this.patientType);
      }
    });
  }


  initDiagnosis() {
    return this.formBuilder.group({
      dose: [''],
      time: ['']
    });
  }

  setInitialDiagnosis() {
    if (this.tim === undefined) {
      this.tim = [];
    }

    if (this.tim.length === 0) {
      this.tim.push({});
    }
  }

  addDiagnosis() {
    const control = <FormArray>this.Editmedicineform.controls['tim'];
    control.push(this.initDiagnosis());
    this.tim.push({});
    console.log("abvc", this.tim)
  }

  onChange(e) {
    if (e.value) {
      this.sel = false;
    } else {
      this.data.selecttime = null;
      this.sel = true;
    }
  }

  deleteDiagnosis(index) {
    this.tim.splice(index, 1);
    console.log("this.tim", this.tim)
  }

  doRefresh() {
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }

  deleteMedicineData() {
    // item = [item];
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Are you sure you want to delete picture ?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
        },
      }, {
        text: "Delete",
        handler: () => {
          this.removeImage();
        }
      }],
    });
    alert.present();
  }

}
