import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppMedicineMasterListModalPage } from './app-medicine-master-list-modal';

@NgModule({
  declarations: [
    AppMedicineMasterListModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AppMedicineMasterListModalPage),
  ],
})
export class AppMedicineMasterListModalPageModule {}
