import { AfterViewInit, Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, ViewController } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as _ from "lodash";
import { AppSharedService } from "../../services/app-shared-service";
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";

/**
 * Generated class for the AppPatientsActivityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-patients-activity',
  templateUrl: 'app-patients-activity.html',
  providers: [DatabaseService]
})
export class AppPatientsActivityPage implements AfterViewInit {

  activityFormGroup: FormGroup;
  submitAttempt: boolean;
  data: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  patientData: any = {};
  medicineData: any = [];
  AddedMedicine: any = [];
  doctorsList: any = [];
  activityList: any = [];
  presAddedBy: string;
  selectActivityName: string;
  selectActivityid: any;
  userLoginData: any;
  patientType: any;
  date: any = new Date();
  index: any;
  hcpkey: any;
  hcpid: any;
  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public appSharedService: AppSharedService, private alertCtrl: AlertController, public modalCtrl: ModalController, private loadingService: LoadingService,  public storage: Storage, public http: Http, private toastCtrl: ToastService) {
    this.activityFormGroup = formBuilder.group({
      activity: ['', Validators.required],
      prestartdate: ['', Validators.required],
      preenddate: ['', Validators.required],
      recurringDaily: [],
      addToStdWheel: [],
      tim: this.formBuilder.array([
        this.initTim(),
      ])
    });
    this.data.tim=[];
    if (this.navParams.data.activitydata1) {
      this.data.activity = this.navParams.data.activitydata1.id;
    }
  }

  initTim() {
    return this.formBuilder.group({
      // dose: [''],
      starttime: ['']
    });
  }

  // setInitialTim() {
  //   if (this.data.tim === undefined) {
  //     this.data.tim = [];
  //   }

  //   if (this.data.tim.length === 0) {
  //     this.data.tim.push({});
  //   }
  // }

  addTim() {
    const control = <FormArray>this.activityFormGroup.controls['tim'];
    control.push(this.initTim());
    this.data.tim.push({});
    console.log("timmm", this.data.tim)
  }

  deleteTim(index) {
    this.data.tim.splice(index, 1);
  }

  intializeApp1() {
    let self = this;
    this.selectActivityName = this.navParams.data.ActivityName ? this.navParams.data.ActivityName : '';
    self.data.action = "getactivitymasterdatafulllist";
    self.data.appsecret = this.appsecret;
    if (this.patientType == "independent" || this.userLoginData.usertype == "healthcareprovider") {
      self.data.userid = this.userLoginData.id;
      this.http.post(this.baseurl + "getdata.php", self.data).subscribe(data => {
        let result = JSON.parse(data["_body"]);
        console.log(result.data);
        if (result.status == 'success') {
          if (result.data !== null) {
            this.activityList = result.data;
            if (this.selectActivityName) {
              this.data.activity = this.activityList.find(a => a.name === this.selectActivityName).id;
            }
          } else {
            this.data = []
            this.activityList = [];
          }
        } else {
          this.toastCtrl.presentToast("There is a problem adding data, please try again");
        }
      }, err => {
        console.log(err);
      });
    }
    else {
      this.storage.get('currentHCP').then(hcpid => {
        self.data.userid = hcpid;
        self.data.appsecret = this.appsecret;
        this.http.post(this.baseurl + "getdata.php", self.data).subscribe(data => {
          this.loadingService.show();
          let result = JSON.parse(data["_body"]);
          console.log(result.data);
          if (result.status == 'success') {
            this.loadingService.hide();
            if (result.data !== null) {
              this.activityList = result.data;
              if (this.selectActivityName) {
                this.data.activity = this.activityList.find(a => a.name === this.selectActivityName).id;
              }
            } else {
              this.loadingService.hide();
              this.data = []
              this.activityList = [];
            }
          } else {
            this.loadingService.hide();
            this.toastCtrl.presentToast("There is a problem adding data, please try again");
          }
        }, err => {
          this.loadingService.hide();
          console.log(err);
        });
      })
    }

    this.presAddedBy = this.userLoginData.id;
    if (!_.isEmpty(this.navParams.data)) {
      // console.log('navParams-Medicinemaster',navParams.data);
      this.patientData = this.navParams.data.patientData;
      this.medicineData = this.navParams.data.MedicineData;
      this.AddedMedicine = this.navParams.data.AddedMedicine;
    }


    /*this.data.enablePrescriptionNotification = false;
    this.data.prescriptionaddress = '';
    this.data.areaexpertise = '';
    this.data.doctorid = '';*/
    this.data.enablePrescriptionNotification = this.data.enablePrescriptionNotification == undefined ? false : this.data.enablePrescriptionNotification;
    this.data.prescriptionaddress = this.data.prescriptionaddress == undefined ? '' : this.data.prescriptionaddress;
    this.data.areaexpertise = this.data.areaexpertise == undefined ? '' : this.data.areaexpertise;
    // this.data.doctorid = this.data.doctorid == undefined ? '' : this.data.doctorid;
    this.data.activity = this.data.activity == undefined ? '' : this.data.activity;
    console.log("activity", this.data.activity);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppMedicinePrescriptionIssuedPage');
    this.getHCPid();
  }

  addPatientActivityDetails(patientData, index) {
    if (this.userLoginData.usertype == 'patient') {
      var uid = this.userLoginData.id
    }
    var time = Array.prototype.map.call(this.data.tim, s => s.starttime).toString();
    this.data.starttime = time;
    if (index == undefined) index = 0;
    console.log(index);
    this.submitAttempt = true;
    let self = this;
    for (let key in self.AddedMedicine) {
      self.data[key] = self.AddedMedicine[key];
    }
    this.data.addedBy = this.presAddedBy;
    this.data.userid = patientData ? patientData.userid : uid;
    if (this.userLoginData.usertype == "healthcareprovider") {
      this.data.hcpid = this.userLoginData.id;
    }
    else if (this.patientType == "independent") {
      this.data.hcpid = 0
    }
    else {
      this.data.hcpid = this.hcpid;
    }
    console.log(this.data);
    self.data.action = "addpatientactivity";
    self.data.appsecret = this.appsecret;
    //self.data.starttime=self.item.startTime
    self.http.post(this.baseurl + "adddata.php", self.data).subscribe(data => {
      this.loadingService.show();
      let result = JSON.parse(data["_body"]);
      console.log(result);
      if (result.status == 'success') {
        this.loadingService.hide();

        self.toastCtrl.presentToast("Notification added successfully");
        this.viewCtrl.dismiss(true);
      } else {
        self.loadingService.hide();
        self.toastCtrl.presentToast("There is a problem adding data, please try again");
        this.viewCtrl.dismiss(false);
      }
    }, err => {
      this.loadingService.hide();
      console.log(err);
    });
  }
  getDate() {
    let date = new Date(this.date);
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    // return date.getDate();
  }
  addTime() {
    this.data.date.push({});
  }

  deleteTime(index) {
    this.data.date.splice(index, 1);
  }

  setInitialTime() {
    if (this.data.date === undefined) {
      this.data.date = [];
    }

    if (this.data.date.length === 0) {
      this.data.date.push({});
    }
  }

  async getHCPid() {
    await this.get('currentHCP').then(result => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      }
      else {
        this.hcpid = 0;
        this.getuserLoginData();
      }
    });
  }
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      else
        return null;
    } catch (reason) {
      return null;
    }
  }

  ngAfterViewInit(): void {
    this.setInitialTime();
  }
  onotherActvityChange(e) {
    console.log(e)
    // if (e == "other") {
    //   this.navCtrl.push("AppPatientsActivityMasterModalPage", { 'patientAction': 'add' });
    // }
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }
  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh('');
      }
      else {
        this.patientType = result;
        this.intializeApp1()
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');
    setTimeout(() => {
      console.log('Async operation has ended');
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }

  closeModal() {
    this.viewCtrl.dismiss(false);
  }
}
