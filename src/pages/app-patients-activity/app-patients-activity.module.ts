import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppPatientsActivityPage } from './app-patients-activity';

@NgModule({
  declarations: [
    AppPatientsActivityPage,
  ],
  imports: [
    IonicPageModule.forChild(AppPatientsActivityPage),
  ],
})
export class AppPatientsActivityPageModule {}
