import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  ModalController,
} from "ionic-angular";
import { LoadingService } from "../../services/loading-service";
import { environment } from "../../environment/environment";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Http } from "@angular/http";
import { Storage } from "@ionic/storage";
import { ToastService } from "../../services/toast-service";
import { MyApp } from "../../app/app.component";

@IonicPage()
@Component({
  selector: "page-app-event-viewall-notification",
  templateUrl: "app-event-viewall-notification.html",
})
export class AppEventViewallNotificationPage {
  SelectList: FormGroup;
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  data: any = {};
  MedicineData: any = [];
  SearchData: any = [];
  usertype: string;
  offset: number = 0;
  count: number = 0;
  terms: any;
  c = [];
  errmsg: boolean = false;
  hcpid: any;

  userLoginData: any;
  keyword: any;
  patientType: any;
  navdata: any;

  constructor(
    private loadingService: LoadingService,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public myApp: MyApp,
    public storage: Storage,
    public formBuilder: FormBuilder,
    public http: Http,
    private toastCtrl: ToastService,
  ) {
  }
  ionViewDidLoad() {
    this.getHCPid();
  }

  loadMasterEventData(infiniteScroll?) {
    this.loadingService.show();
    this.data.userid = this.userLoginData.id;
    this.data.keyword = this.keyword;
    this.data.offset = this.offset;
    this.data.action = "getpatienteventdata";
    this.data.appsecret = this.appsecret;
    this.data.hcpid = this.hcpid;
    this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);
        if (infiniteScroll) {
          infiniteScroll.complete();
        }
        console.log("result", result);
        if (result.status == "success") {
          this.loadingService.hide();
          if (result.data !== null) {
            for (var i in result.data) {
              this.c.push(result.data[i]);
            }
            console.log("this.c :::", this.c);
            this.MedicineData = this.c;
            this.SearchData = this.c;
            console.log("MedicenData", this.MedicineData);
            this.count = result.count;
            this.errmsg=false;
          } else {
            this.loadingService.hide();
            this.data = [];
            this.errmsg=true;
            this.toastCtrl.presentToast("There is a no data available.");
            this.data.keys = [];
            this.MedicineData = [];
            this.SearchData = [];
          }
        } else {
          this.loadingService.hide();
          this.errmsg=true;
        }
      },
      (err) => {
        this.errmsg=true;
        this.loadingService.hide();
        console.log(err);
      },
    );
  }

  deletePatientEvent(item, index, patientData) {
    let self = this;

    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete patients record?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
          // console.log('Cancel clicked');
        },
      }, {
        text: "Delete",
        handler: () => {
          let self = this;
          this.data.action = "deletepatientevent";
          this.data.id = item.id;
          this.data.appsecret = this.appsecret;
          this.http.post(this.baseurl + "deletedata.php", this.data).subscribe(
            (data) => {
              this.loadingService.show();
              let result = JSON.parse(data["_body"]);
              console.log(result.data);
              if (result.status == "success") {
                self.toastCtrl.presentToast("Wellbeing is deleted successfully");
                this.loadingService.hide();
                this.c = [];
                this.SearchData = [];
                this.MedicineData = [];
                this.loadMasterEventData();
              } else {
                this.loadingService.hide();
                self.toastCtrl.presentToast("There is an error deleting data!");
              }
            },
            (err) => {
              this.loadingService.show();
              console.log(err);
            },
          );
        },
      }],
    });
    alert.present();
  }

  loadMore(infiniteScroll) {
    if (this.MedicineData.length >= this.count || this.count == 0) {
      infiniteScroll.enable(false);
      console.log("aaaaaa");
    } else {
      this.offset = this.offset + 10;
      this.loadMasterEventData(infiniteScroll);
    }
  }
  async getHCPid() {
    await this.get("currentHCP").then((result) => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      } else {
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        console.log("this.userloigndata", this.userLoginData);
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
        }
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get("patientType").then((result) => {
      console.log("patienttypesdfdsfsdsdf", result);
      if (
        (!result || result == null || result == undefined) &&
        this.userLoginData == "patient"
      ) {
        this.doRefresh();
      } else {
        this.patientType = result;
        // this.appIntialize1();
        this.loadMasterEventData();
        console.log("sekeccdffvfdgdfgdfg", this.patientType);
      }
    });
  }
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      } else {
        return null;
      }
    } catch (reason) {
      return null;
    }
  }
  doRefresh() {
    this.loadingService.show();
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.loadingService.hide();
    }, 2000);
  }

  AddNotifications() {
    let modal = this.modalCtrl.create("AppPatientsEventPage");
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.MedicineData = [];
        this.SearchData = [];
        this.c = [];
        if (this.count > 10) {
          this.doRefresh();
        } else {
          this.loadMasterEventData();
        }
      }
    });
  }
  editEvent(event, index) {
    let modal = this.modalCtrl.create(
      "AppPatientEventModalPage",
      {
        "event": event,
        "patientData": this.userLoginData,
        "type": "edit",
        "index": index,
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.MedicineData = [];
        this.SearchData = [];
        this.c = [];
        if (this.count > 10) {
          this.doRefresh();
        } else {
          this.loadMasterEventData();
        }
      }
    });
  }

  setFilteredNameData() {
    // if (this.terms == '') {
    //   this.doRefresh();
    // }

    this.data.action = "getpatienteventdata";

    this.data.offset = 0;
    this.data.keyword = this.terms;
    this.data.appsecret = this.appsecret;
    this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);
        if (result.status == "success") {
          this.SearchData = result.data;
        } else {
          this.SearchData = [];
        }
      },
      (err) => {
        this.SearchData = [];
      },
    );
  }
}
