import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppEventViewallNotificationPage } from './app-event-viewall-notification';

@NgModule({
  declarations: [
    AppEventViewallNotificationPage,
  ],
  imports: [
    IonicPageModule.forChild(AppEventViewallNotificationPage),
  ],
})
export class AppEventViewallNotificationPageModule {}
