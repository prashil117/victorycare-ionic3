import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppTimelinePage } from './app-timeline';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AppTimelinePage,
  ],
  imports: [
    IonicPageModule.forChild(AppTimelinePage),
    PipesModule
  ],
})
export class AppTimelinePageModule {}
