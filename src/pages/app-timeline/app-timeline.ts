import { Component, ViewChild } from "@angular/core";
import {
  AlertController,
  Navbar,
  IonicPage,
  ModalController,
  NavController,
  NavParams,
  Item,
  Select,
} from "ionic-angular";
import * as _ from "lodash";
import { LoadingService } from "../../services/loading-service";
import { DatabaseService } from "../../services/database-service";
import { MyApp } from "../../app/app.component";
import { Subscription } from "rxjs/Subscription";
import { Storage } from "@ionic/storage";
import { SocialSharing } from '@ionic-native/social-sharing';
import { Screenshot } from '@ionic-native/screenshot/ngx'

declare const $: any;
declare const d3: any;
import {
  Validators,
  FormBuilder,
  FormArray,
  FormGroup,
  FormControl,
} from "@angular/forms";
import { Http } from "@angular/http";
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";

@IonicPage()
@Component({
  selector: "page-app-timeline",
  templateUrl: "app-timeline.html",
  providers: [DatabaseService, SocialSharing, Screenshot],
})
export class AppTimelinePage {
  shareImageNi: string;
  siteName: string;
  @ViewChild(Navbar)
  navBar: Navbar;
  SelectList: FormGroup;
  currentHCP: any[];
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  patientData: any = {};
  medicines: any[];
  WheelActivityData: any = [];
  WheelMedicineData: any = [];
  WheelEventData: any = [];
  SettingsData: any = [];
  activityData: any = [];
  data: any = [];
  ActivityArrar: any = [];
  EventArrar: any = [];
  MedicineArrar: any = [];
  myColor: any = [];
  EventColor: any = [];
  MedicineColor: any = [];
  name: any = [];
  userLogin: any;
  patientType: any;
  Eventname: any = [];
  column: string = "start";
  descending: boolean = false;
  order: number;
  medicineName: any = [];
  BorderColor: any = [];
  selectedDate: any;
  MedicineWheelData: any = [];
  ActivityWheelData: any = [];
  StatusWheelData: any = [];
  usertype: string;
  username: string;
  userid: string;
  usersessionid: string;
  ut: boolean;
  dropdown: any = [];
  notselected: boolean;
  selected1: any = [];
  result: any = [];
  bordercolors: Array<string> = [
    "#7e1b1b",
    "#611881",
    "#3b2079",
    "#004d44",
    "#1d491f",
    "#787b1e",
    "#966c03",
    "#996100",
    "#804000",
    "#5e7a87",
  ];
  colors: Array<string> = [
    "#D32F2F",
    "#7B1FA2",
    "#512DA8",
    "#00796B",
    "#388E3C",
    "#AFB42B",
    "#FBC02D",
    "#FFA000",
    "#F57C00",
    "#455A64",
  ];
  WheelData: any = [];
  date: any = new Date();
  Todaydate: Date;

  // Wheel declaretion start
  sleepColors: any = {
    "default": "#FFF",
    "sleep": "#6bb1d6",
    "sleepawake": "#afdb07",
    "sleepunawake": "#fad000",
    "sleepdistress": "#e26060",
    "sleepmultiple": "#FF69B4",
  };
  medicineColors: any = {
    "default": "#2b3249",
    "medicinelosec": "#95d1f0",
    "medicinezonegran": "#fadf59",
    "medicinemovicol": "#fd8686",
    "medicinekepra": "#cff14a",
    "medicinemultiple": "#FF69B4",
  };
  activityColors = {
    "default": "#FFF",
    "activitybpap": "#74a9c4",
    "activityoxygen": "#90b018",
    "activitytemperature": "#c8ac20",
    "activityurine": "#e69393",
    "activitymultiple": "#FF69B4",
  };
  numNodes: any = 96;
  defaultMinutes: any = 15;
  minuteTxt: any = [
    [15, 30, 45, 60],
    [30, 60],
  ];
  whichClockforTxt: any = 0;
  calMinute: any = this.numNodes / 12;
  arrClockSleep: any = new Array();
  arrClockMedicine: any = new Array();
  arrClockActivity: any = new Array();
  arrReturn: any = new Array();
  userLoginData: any;
  startHours: any;
  endHours: any;
  startMineuts: any;
  endMinites: any;
  status: any = [];
  sleepDescription: any = [];
  wheelEventColor: any = [];
  startTime: any = [];
  endTime: any = [];

  medicineStart: any = [];
  wheelMedicineColor: any = [];
  medicineStatus: any = [];
  medicineDescription: any = [];
  medicineStartHours: any;
  medicineEndMineuts: any;

  wheelActivityColor: any = [];
  activityStartTime: any = [];
  activityDescription: any = [];
  activityEndTime: any = [];
  activityStatus: any = [];
  activityStartHours: any;
  activityEndHours: any;
  activityStartMineuts: any;
  activityEndMinites: any;
  arrstartendtime: any = [];

  endmin: any = [];
  end: any = [];

  activityList: any = [];
  medicineList = [];
  wellbeaningList = [];
  arrDes: any = [];
  medicineDes: any = [];
  medicineColor: any = [];
  sleepTime: any = [];
  sleepDes: any = [];
  sleepColor: any = [];
  circle: any = [];
  startPoint: any = [];
  arrcolor: any = [];
  choices: any;
  userlogindata: any;
  patientMasterEventData: any = {};
  patientMasterMedicineData: any = {};
  patientMasterActivityData: any = {};
  notifyevent: any;
  notifyactivity: any;
  notifymedicine: any;
  hcpid: any;
  dependent: boolean = true;
  // SearchData: any = [];
  @ViewChild('mySelect') selectRef: Select;
  constructor(
    public socialShareCtrl: SocialSharing,
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private databaseService: DatabaseService,
    public myApp: MyApp,
    public storage: Storage,
    public formBuilder: FormBuilder,
    private loadingService: LoadingService,
    public http: Http,
    private toastCtrl: ToastService,

    private screenshot: Screenshot
  ) {
    this.shareImageNi = "https://lh3.googleusercontent.com/8c4SpecjfcnCdfq2PXTwS7vvjPmrOa-lgbcczv6RBRF-6fgYv7-qCrNEW5owqQRPx15FesYOt3oqJrQJTmk0XZoR4WGEc3UGGbZgt7GvPKOd_pW4_Cbd9yqiHY9mxoHAkr5m-coUo47lsSy484J4kINWBrR3YaXClG_KRLvYDMH5vjipCJsTlh8-1Eg5z-IMON0yW9dwL0C42FPDTJ5pQIE2ilZ7J4CYGSo14XDw87zdZ7GrkrLtuESlupo2rzAX80zaZ4ER-S-o8fhHwvLYNrrYrfrJMCBjduqziJxvuBnB5fMSkn8BUI9JuIcqq9Hp5J2aEwIrb1xQUpW8bbC0o2Y8hVUv2yECJ8F4jieEg5EQaz--sPxjogAEgybsaGS7QJK6yphIm59ulIQRUmcwOEf5oqMHwXUrYfaW4v-hia_9h4vuNWoUOksNstaJa22PVRbZGIZLdFdfPYE9ekVyFvUxqsZ9Ft6pFAXdkD3UX7GaKujky6QFNJDqkCef8Tu0vXQFPycxmIdm4spMgKy6NIc92_p6AQo_7AYrv7pI_vtUL4-Sh6HtQ09o6g0GbprKCnforqQyHWsAOdVQ5M6zjOrh6dNAuxFTl4MHIV8=w353-h588-no";
    this.siteName = "https://victory.care";
    this.SelectList = formBuilder.group({
      currentHCP: [""],
    });
  }
  openSelect() {
    this.selectRef.open();
  }
  appIntialize1() {
    this.loadingService.show();
    // setTimeout(() => {
    //   this.loadingService.hide
    //  }, 3000);
    var action = this.navParams.data.action;
    var patient = this.navParams.data.data;
    if (this.patientType == "independent") {
      this.dependent = false;
    }
    // if(action == true){
    if (this.userLoginData.usertype == "patient") {
      this.get("userLogin").then((res) => {
        setTimeout(() => {
          this.loadingService.hide();
        }, 2000);
        this.userlogindata = res;
        this.userlogindata.userid = this.userlogindata.id;
      });
      this.get("currentHCP").then((res) => {
        this.userlogindata.parentid = res;
        action = true;
        this.navParams.data.data = this.userlogindata;
        this.patientData = this.navParams.data.data;
        patient = this.navParams.data.data;
        this.storage.set("patientSession", action);
        this.storage.set("patientSessionData", patient);
      });
    } else {
      this.storage.set("patientSession", action);
      this.storage.set("patientSessionData", patient);
    }

    this.selectedDate = new Date().toISOString();
    this.WheelData = [];

    if (!_.isEmpty(this.navParams.data.data)) {
      this.WheelData = [];
      this.patientData = this.navParams.data.data;

      // this.GetAllSettingsData();
    }
    this.loadWheel();

    // let modal = this.modalCtrl.create("AppSelectWheelTimePage");
    // modal.present();
    // modal.onDidDismiss(data => {
    // });
    ////////// HCP DropDown Code Starts /////////////

    this.myApp.initializeApp();
    this.username = this.userLoginData.email;
    this.usertype = this.userLoginData.usertype;
    this.userid = this.userLoginData.id;
    // this.ut = (this.usertype == 'healthcareprovider' ? false : true);
    if (
      this.userLoginData.usertype == "nurse" ||
      this.userLoginData.usertype == "doctor" ||
      this.userLoginData.usertype == "healthcareprovider"
    ) {
      this.ut = false;
    } else {
      this.ut = true;
    }

    // alert(this.usertype);
    this.dropdown = this.getHCPData(this.userid, this.usertype)
    var result = null;
    this.get("currentHCP").then((result) => {
      if (result != null) {
        this.notselected = false;
        this.selected1 = result;
      } else {
        this.notselected = true;
        this.selected1 = this.dropdown;
      }
    });
    this.LoadData();
  }

  logout() {
    this.databaseService.logout();
  }
  ionViewDidLoad() {
    this.getHCPid();
    this.sortField("start");
    this.order = -1;
  }

  ionViewWillEnter() {
    this.sortField("start");
  }

  ionViewDidEnter() {
    this.sortField("start");
  }

  LoadData(selectedDate = "") {
    this.WheelData = [];
    this.getWheelActivity(selectedDate);
    this.getWheelEventData(selectedDate);
    this.getWheelMedicineData(selectedDate);
    this.GetAllSettingsData();
  }

  loadWheel() {
    var abc = this;
    setTimeout(function () {
      $(".pre-definedlist").show();
      abc.numNodes = (abc.defaultMinutes == 30) ? 48 : 96;
      abc.calMinute = abc.numNodes / 12;
      abc.updateClockHours();
      abc.GenerateCareGraphs();
    }, 1000);
  }

  getWheelActivity(date = "") {
    let self = this;
    // let Todaydate = this.getDate();
    // let Todaydate = new Date(this.selectedDate);
    // Todaydate = new Date();
    // console.log('current date', Todaydate);
    console.log(date);
    let Todaydate = (date != "") ? date : this.getDate();
    var webServiceData: any = {};
    webServiceData.action = "getactivitywheeldata";
    webServiceData.currentdate = Todaydate;
    webServiceData.userid = this.patientData.userid
      ? this.patientData.userid
      : this.userLoginData.id;
    webServiceData.appsecret = this.appsecret;
    if (this.patientType == "independent") {
      webServiceData.hcpid = 0;
    } else if (
      this.userLoginData.usertype == "nurse" ||
      this.userLoginData.usertype == "doctor" ||
      (this.userLoginData.usertype == "patient" &&
        this.patientType == "dependent")
    ) {
      webServiceData.hcpid = this.hcpid;
    } else {
      webServiceData.hcpid = this.userLoginData.id;
    }
    var activity = self.http.post(
      this.baseurl + "wheeldata.php",
      webServiceData,
    ).subscribe((data) => {
      let result = JSON.parse(data["_body"]);
      if (result.status == "success") {
        this.WheelActivityData = [];
        this.WheelActivityData = result.data;
      } else {
        this.WheelActivityData = [];

        // self.toastCtrl.presentToast("There is a No data Found");
      }
      this.ActivityArrar = [];
      this.ActivityArrar = self.WheelActivityData;
      // for(var c = 0; c < this.WheelActivityData.length; c++ ) {
      //   this.ActivityArrar.push(this.WheelActivityData[c].Activity);
      // }

      this.myColor = [];
      this.BorderColor = [];
      this.name = [];
      this.wheelActivityColor = [];
      this.activityStartTime = [];
      this.activityEndTime = [];
      this.activityStatus = [];
      this.activityDescription = [];

      for (var D = 0; D < this.ActivityArrar.length; D++) {
        this.myColor.push(this.ActivityArrar[D].color);
        this.name.push(this.ActivityArrar[D].name);
        this.BorderColor.push(this.toObject(this.myColor[D]));
        // if((<any>Object).keys(this.ActivityArrar[D]).length == 1) {
        //   this.myColor.push(this.ActivityArrar[D][0].color);
        //   this.name.push(this.ActivityArrar[D][0].name);
        //   this.BorderColor.push(this.toObject(this.myColor[D]));
        // }
      }
      for (var d = 0; d < this.WheelActivityData.length; d++) {
        this.WheelActivityData[d].color = this.myColor[d];
        this.WheelActivityData[d].name = this.name[d];
        this.WheelActivityData[d].bordercolor = this.BorderColor[d];
        this.WheelActivityData[d].source = "Activity";
        this.activityStartTime.push(this.WheelActivityData[d].start);
        this.activityEndTime.push(this.WheelActivityData[d].end);
        this.wheelActivityColor.push(this.WheelActivityData[d].color);
        this.activityStatus.push(this.WheelActivityData[d].name);
        this.activityDescription.push(this.WheelActivityData[d].description);

        this.endmin = [];
        this.end = [];
        for (let C = 0; C < this.activityStartTime.length; C++) {
          var End = this.activityEndTime[C].split(":");
          this.activityEndHours = End[0];
          this.end.push(this.activityEndHours);
          this.endmin.push(End[1]);
        }
      }

      this.WheelData = this.WheelData.concat(this.WheelActivityData);
      this.calltoupdateStatus("activity");
      // this.status = 'activityoxygen';
      activity.unsubscribe();
    });
  }

  getWheelEventData(date) {
    let self = this;
    let Todaydate = this.getDate();
    var webServiceData: any = {};
    webServiceData.eventaction = "geteventwheeldata";
    webServiceData.currentdate = Todaydate;
    webServiceData.userid = this.patientData.userid
      ? this.patientData.userid
      : this.userLoginData.id;
    webServiceData.appsecret = this.appsecret;
    if (this.patientType == "independent") {
      webServiceData.hcpid = 0;
    } else if (
      this.userLoginData.usertype == "nurse" ||
      this.userLoginData.usertype == "doctor" ||
      (this.userLoginData.usertype == "patient" &&
        this.patientType == "dependent")
    ) {
      webServiceData.hcpid = this.hcpid;
    } else {
      webServiceData.hcpid = this.userLoginData.id;
    }
    var event = self.http.post(this.baseurl + "wheeldata.php", webServiceData)
      .subscribe((data) => {
        let result = JSON.parse(data["_body"]);
        if (result.status == "success") {
          this.WheelEventData = [];
          this.WheelEventData = result.data;
        } else {
          this.WheelEventData = [];
          // self.toastCtrl.presentToast("There is a No data Found");
        }
        this.EventArrar = [];
        this.EventArrar = self.WheelEventData;

        this.EventColor = [];
        this.Eventname = [];
        this.startTime = [];
        this.endTime = [];
        this.wheelEventColor = [];
        this.status = [];
        this.sleepDescription = [];

        for (var D = 0; D < this.EventArrar.length; D++) {
          this.EventColor.push(this.EventArrar[D].eventcolor);
          this.Eventname.push(this.EventArrar[D].ename);
          this.BorderColor.push(this.toObject(this.myColor[D]));
          // if((<any>Object).keys(this.EventArrar[D]).length == 1) {
          //  this.EventColor.push(this.EventArrar[D][0].color);
          //  this.Eventname.push(this.EventArrar[D][0].Eventname);
          // }
        }

        for (var d = 0; d < this.WheelEventData.length; d++) {
          this.WheelEventData[d].color = this.EventColor[d];
          this.WheelEventData[d].Eventname = this.Eventname[d];
          this.WheelEventData[d].source = "Event";
          this.startTime.push(this.WheelEventData[d].start);
          this.endTime.push(this.WheelEventData[d].end);
          this.status.push(this.Eventname[d]);
          this.sleepDescription.push(this.WheelEventData[d].description);
          this.wheelEventColor.push(this.WheelEventData[d].color);
        }
        this.WheelData = this.WheelData.concat(this.WheelEventData);
        this.calltoupdateStatus("sleep");
        // this.status = 'sleepawake';
        event.unsubscribe();
      });
  }

  getWheelMedicineData(date) {
    let self = this;
    let Todaydate = this.getDate();
    var webServiceData: any = {};
    webServiceData.action = "getmedicinewheeldata";
    webServiceData.currentdate = Todaydate;
    webServiceData.userid = this.patientData.userid
      ? this.patientData.userid
      : this.userLoginData.id;
    webServiceData.appsecret = this.appsecret;
    if (this.patientType == "independent") {
      webServiceData.hcpid = 0;
    } else if (
      this.userLoginData.usertype == "nurse" ||
      this.userLoginData.usertype == "doctor" ||
      (this.userLoginData.usertype == "patient" &&
        this.patientType == "dependent")
    ) {
      webServiceData.hcpid = this.hcpid;
    } else {
      webServiceData.hcpid = this.userLoginData.id;
    }
    var medicine = self.http.post(
      this.baseurl + "wheeldata.php",
      webServiceData,
    ).subscribe((data) => {
      let result = JSON.parse(data["_body"]);
      if (result.status == "success") {
        this.WheelMedicineData = [];
        this.WheelMedicineData = result.data;
      } else {
        this.WheelMedicineData = [];
        // self.toastCtrl.presentToast("There is a No data Found");
      }
      // else{
      //   this.WheelData =  this.WheelData.concat(this.WheelMedicineData) ;
      //   this.toastCtrl.presentToast("There is a no data available.");
      // }
      this.MedicineArrar = [];
      this.MedicineArrar = self.WheelMedicineData;
      // for(var z = 0; z < this.WheelMedicineData.length; z++){
      //  this.MedicineArrar.push(this.WheelMedicineData[z].Medicine)
      // }
      this.MedicineColor = [];
      this.medicineName = [];
      this.medicineStart = [];
      this.wheelMedicineColor = [];
      this.medicineStatus = [];
      this.medicineDescription = [];

      for (var D = 0; D < this.MedicineArrar.length; D++) {
        this.MedicineColor.push(this.MedicineArrar[D].color);
        this.medicineName.push(this.MedicineArrar[D].medicinename);
      }
      for (var d = 0; d < this.WheelMedicineData.length; d++) {
        this.WheelMedicineData[d].color = this.MedicineColor[d];
        this.WheelMedicineData[d].medicineName = this.medicineName[d];
        this.WheelMedicineData[d].source = "Medicine";
        this.medicineStart.push(this.WheelMedicineData[d].start);
        this.wheelMedicineColor.push(this.WheelMedicineData[d].color);
        this.medicineStatus.push(this.WheelMedicineData[d].medicineName);
        this.medicineDescription.push(this.WheelMedicineData[d].description);
      }

      this.WheelData = this.WheelData.concat(this.WheelMedicineData);
      this.calltoupdateStatus("medicine");
      // this.status = 'medicinezonegran';
      medicine.unsubscribe();
    }, (err) => {
      console.log(err);
    });
  }

  notify(event) {
    let date = new Date(this.selectedDate);
    var selectedDate = event.year + "-" + event.month + "-" + event.day;

    if (selectedDate == undefined) {
      this.WheelData.selectedDate.getTime();
      date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    }
    this.updateClockHours();
    this.LoadData(selectedDate);
  }

  getTime() {
    let time = new Date(this.date);
    return time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
  }

  getFormatedTime() {
    var date = new Date(this.date);
    var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
    var am_pm = date.getHours() >= 12;
    var minutes = date.getMinutes() < 10
      ? "0" + date.getMinutes()
      : date.getMinutes();
    let time = hours + ":" + minutes + " ";
    return time;
  }

  sortField(fieldname) {
    // this.column = fieldname;
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }

  toObject(myColor) {
    var result = [];
    for (var i = 0; i < this.colors.length; i++) {
      result[this.colors[i]] = this.bordercolors[i];
    }
    return result[myColor];
  }

  addActivity() {
    let modal = this.modalCtrl.create(
      "AppAddActivityModelPage",
      {
        "patientData": this.patientData,
        "patientSettings": this.SettingsData,
        "date": this.getDate(),
        "activityData": this.notifyactivity
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      this.navParams.data.type = "";
      this.WheelData = [];
      this.LoadData();
      this.updateClockHours();
      this.GetAllSettingsData()
    });
    // this.WheelData = [];
  }

  addMedicien() {
    let modal = this.modalCtrl.create(
      "AppAddMedicineModelPage",
      {
        "patientData": this.patientData,
        "date": this.getDate(),
        "patientSettings": this.SettingsData,
        "medicineData": this.notifymedicine
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      this.navParams.data.type = "";
      this.WheelData = [];
      this.LoadData();
      this.updateClockHours();
      this.GetAllSettingsData()
    });
    // this.WheelData = [];
  }

  addStatus() {
    let modal = this.modalCtrl.create(
      "AppAddCurrentStatusModelPage",
      {
        "patientData": this.patientData,
        "date": this.getDate(),
        "patientSettings": this.SettingsData,
        "eventData": this.notifyevent
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      this.navParams.data.type = "";
      this.WheelData = [];
      this.LoadData();
      this.updateClockHours();
      this.GetAllSettingsData()
    });
  }

  GetAllSettingsData() {
    let self = this;
    self.ActivityWheelData = [];
    self.MedicineWheelData = [];
    self.StatusWheelData = [];
    this.patientMasterActivityData.action = "getactivitymasterforwheel";
    this.patientMasterActivityData.appsecret = this.appsecret;
    if (
      this.patientType == "independent" ||
      this.userLoginData.usertype == "healthcareprovider"
    ) {
      this.patientMasterActivityData.userid = this.userLoginData.id;
    } else {
      this.patientMasterActivityData.userid = this.hcpid;
    }
    this.http.post(this.baseurl + "getdata.php", this.patientMasterActivityData)
      .subscribe((data) => {
        let result = JSON.parse(data["_body"]);
        if (result.status == "success") {
          if (result.data !== null) {
            self.ActivityWheelData.push(result.data);
            self.activityList = result.data;
            var notify = this.navParams.data ? this.navParams.data.type : '';
            if (notify == "notification") {
              this.notifyactivity = this.navParams.data ? this.navParams.data.activitynot : '';
              if (this.notifyactivity) {
                this.addActivity();
                this.navParams.data.type = "";
              }
            }
          } else {
            self.ActivityWheelData = [];
            self.activityList = [];
          }
        }
      }, (err) => {
        console.log(err);
      });
    this.patientMasterMedicineData.action = "getmedicinemasterforwheel";
    this.patientMasterMedicineData.appsecret = this.appsecret;
    if (
      this.patientType == "independent" ||
      this.userLoginData.usertype == "healthcareprovider"
    ) {
      this.patientMasterMedicineData.userid = this.userLoginData.id;
    } else {
      this.patientMasterMedicineData.userid = this.hcpid;
    }

    this.http.post(this.baseurl + "getdata.php", this.patientMasterMedicineData)
      .subscribe((data) => {
        let result = JSON.parse(data["_body"]);

        if (result.status == "success") {
          if (result.data !== null) {
            self.MedicineWheelData.push(result.data);
            self.medicineList = result.data;
            var notify = this.navParams.data ? this.navParams.data.type : '';
            if (notify == "notification") {
              this.notifymedicine = this.navParams.data ? this.navParams.data.medicinenot : '';
              if (this.notifymedicine) {
                this.addMedicien();
                this.navParams.data.type = "";
              }
            }
          } else {
            self.MedicineWheelData = [];
            self.medicineList = [];
          }
        }
      }, (err) => {
        console.log(err);
      });
    this.patientMasterEventData.action = "geteventmasterforwheel";
    this.patientMasterEventData.appsecret = this.appsecret;
    if (
      this.patientType == "independent" ||
      this.userLoginData.usertype == "healthcareprovider"
    ) {
      this.patientMasterEventData.userid = this.userLoginData.id;
    } else {
      this.patientMasterEventData.userid = this.hcpid;
    }
    this.http.post(this.baseurl + "getdata.php", this.patientMasterEventData)
      .subscribe((data) => {
        let result = JSON.parse(data["_body"]);
        if (result.status == "success") {
          if (result.data !== null) {
            self.StatusWheelData.push(result.data);
            self.wellbeaningList = result.data;
            var notify = this.navParams.data ? this.navParams.data.type : '';
            if (notify == "notification") {
              this.notifyevent = this.navParams.data ? this.navParams.data.eventnot : '';
              if (this.notifyevent) {
                this.addStatus();
                this.navParams.data.type = "";
              }
            }
          } else {
            self.StatusWheelData = [];
            self.wellbeaningList = [];
          }
        }
      }, (err) => {
        console.log(err);
      });
    this.storage.get("user").then((patientData) => {
      ////////// Event Data Start //////
      this.patientMasterEventData.action = "getpatienteventdata";
      this.patientMasterEventData.userid = patientData
        ? patientData.id
        : this.userlogindata.id;
      this.patientMasterEventData.appsecret = this.appsecret;
      if (
        this.userLoginData.usertype == "nurse" ||
        this.userLoginData.usertype == "doctor" ||
        (this.userLoginData.usertype == "patient" &&
          this.patientType == "dependent")
      ) {
        this.patientMasterEventData.hcpid = this.hcpid;
      } else if (this.patientType == "dependent") {
        this.patientMasterEventData.hcpid = 0;
      } else {
        this.patientMasterEventData.hcpid = this.userLoginData.id;
      }
      this.http.post(this.baseurl + "getdata.php", this.patientMasterEventData)
        .subscribe((data) => {
          let result = JSON.parse(data["_body"]);
          if (result.status == "success") {
            if (result.data !== null) {
              this.wellbeaningList = (result.data == null ? [] : result.data);
              // this.MedicineData = result.data;
              // this.SearchData = result.data;
            } else {
              this.data = [];
              this.data.keys = [];
              this.wellbeaningList = [];
              // this.SearchData = [];
            }
          }
        }, (err) => {
          console.log(err);
        });
      ////////// Event Data End //////

      ////////// Medicine Data Start //////
      this.patientMasterMedicineData.action = "getpatientmedicinedata";
      this.patientMasterMedicineData.userid = patientData
        ? patientData.id
        : this.userlogindata.id;
      this.patientMasterMedicineData.appsecret = this.appsecret;
      if (
        this.userLoginData.usertype == "nurse" ||
        this.userLoginData.usertype == "doctor" ||
        (this.userLoginData.usertype == "patient" &&
          this.patientType == "dependent")
      ) {
        this.patientMasterMedicineData.hcpid = this.hcpid;
      } else if (this.patientType == "dependent") {
        this.patientMasterMedicineData.hcpid = 0;
      } else {
        this.patientMasterMedicineData.hcpid = this.userLoginData.id;
      }
      this.http.post(
        this.baseurl + "getdata.php",
        this.patientMasterMedicineData,
      ).subscribe((data) => {
        let result = JSON.parse(data["_body"]);
        if (result.status == "success") {
          if (result.data !== null) {
            this.medicineList = (result.data == null ? [] : result.data);

            // this.MedicineData = result.data;
            // this.SearchData = result.data;
          } else {
            this.data = [];
            this.data.keys = [];
            this.medicineList = [];
            // this.SearchData = [];
            this.toastCtrl.presentToast(
              "There is a no medicine data available.",
            );
          }
        } else {
          // this.loadingService.hide();

          // this.navCtrl.setRoot("AppDashboardPage");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      }, (err) => {
        console.log(err);
      });
      ////////// Medicine Data End //////

      ////////// Activity Data Start //////
      this.patientMasterActivityData.action = "getpatientactivitydata";
      this.patientMasterActivityData.userid = patientData
        ? patientData.id
        : this.userlogindata.id;
      this.patientMasterActivityData.appsecret = this.appsecret;
      if (
        this.userLoginData.usertype == "nurse" ||
        this.userLoginData.usertype == "doctor" ||
        (this.userLoginData.usertype == "patient" &&
          this.patientType == "dependent")
      ) {
        this.patientMasterActivityData.hcpid = this.hcpid;
      } else if (this.patientType == "dependent") {
        this.patientMasterActivityData.hcpid = 0;
      } else {
        this.patientMasterActivityData.hcpid = this.userLoginData.id;
      }
      this.http.post(
        this.baseurl + "getdata.php",
        this.patientMasterActivityData,
      ).subscribe((data) => {
        let result = JSON.parse(data["_body"]);
        if (result.status == "success") {
          if (result.data !== null) {
            this.activityList = (result.data == null ? [] : result.data);
            // this.MedicineData = result.data;
            // this.SearchData = result.data;
          } else {
            this.data = [];
            this.data.keys = [];
            this.activityList = [];
            // this.SearchData = [];
            this.toastCtrl.presentToast(
              "There is a no activity data available.",
            );
          }
        } else {
          // this.loadingService.hide();

          // this.navCtrl.setRoot("AppDashboardPage");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      }, (err) => {
        console.log(err);
      });
      ////////// Activity Data End //////
    });
    this.SettingsData = {
      patientActivityMasterData: self.ActivityWheelData,
      patientMedicineMasterData: self.MedicineWheelData,
      patientEventMasterData: self.StatusWheelData,
    };
  }

  getDate() {
    // console.log(this.selectedDate);
    let date = new Date(this.selectedDate);
    // return date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" +
      date.getDate();
  }

  editWheelActivity(ActivityData) {
    let modal = this.modalCtrl.create(
      "AppAddActivityModelPage",
      {
        "data": ActivityData,
        "patientSettings": this.SettingsData,
        "date": this.getDate(),
        "patient": this.patientData,
        "action": "edit",
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      this.WheelData = [];
      this.LoadData();
      this.updateClockHours();
    });
    // this.LoadData();
  }

  EditEvent(EventData) {
    let modal = this.modalCtrl.create(
      "AppAddCurrentStatusModelPage",
      {
        "data": EventData,
        "patientSettings": this.SettingsData,
        "date": this.getDate(),
        "patient": this.patientData,
        "action": "edit",
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      this.WheelData = [];
      this.LoadData();
      this.updateClockHours();
    });
  }

  EditMedicine(MedicineData) {
    let modal = this.modalCtrl.create(
      "AppAddMedicineModelPage",
      {
        "data": MedicineData,
        "patientSettings": this.SettingsData,
        "date": this.getDate(),
        "patient": this.patientData,
        "action": "edit",
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      this.WheelData = [];
      this.LoadData();
      this.updateClockHours();
    });
    // this.LoadData();
    //  this.WheelData = [];
  }

  deleteWheelActivityData(item) {
    console.log(item);
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete this record?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
          // console.log('Cancel clicked');
        },
      }, {
        text: "Delete",
        handler: () => {
          let self = this;
          var Todaydate = self.getDate();

          var webServiceData: any = {};
          webServiceData.action = "deleteactivitywheeldata";
          webServiceData.id = item.id;
          webServiceData.appsecret = this.appsecret;
          self.http.post(this.baseurl + "wheeldata.php", webServiceData)
            .subscribe((data) => {
              let result = JSON.parse(data["_body"]);
              console.log(result);
              if (result.status == "success") {
                self.toastCtrl.presentToast("Activity deleted successfully");
                self.LoadData(Todaydate);
                self.updateClockHours();
              } else {
                // self.toastCtrl.presentToast("There is a problem deleting data");
              }
            }, (err) => {
              console.log(err);
            });
        },
      }],
    });
    alert.present();
  }

  deleteWheelEventData(item) {
    console.log(item.id);
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete this record?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
        },
      }, {
        text: "Delete",
        handler: () => {
          let self = this;
          var Todaydate = self.getDate();
          var webServiceData: any = {};
          webServiceData.action = "deletestatuswheeldata";
          webServiceData.id = item.id;
          webServiceData.appsecret = this.appsecret;
          self.http.post(this.baseurl + "wheeldata.php", webServiceData)
            .subscribe((data) => {
              let result = JSON.parse(data["_body"]);
              console.log(result);
              if (result.status == "success") {
                self.toastCtrl.presentToast("Event deleted successfully");
                self.LoadData(Todaydate);
                self.updateClockHours();
              } else {
                // self.toastCtrl.presentToast("There is a problem deleting data");
              }
            }, (err) => {
              console.log(err);
            });
        },
      }],
    });
    alert.present();
  }

  deleteWheelMedicineData(item) {
    console.log(item.id);
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete this record?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
          },
        },
        {
          text: "Delete",
          handler: () => {
            let self = this;
            var Todaydate = self.getDate();
            var webServiceData: any = {};
            webServiceData.action = "deletemedicinewheeldata";
            webServiceData.id = item.id;
            webServiceData.appsecret = this.appsecret;
            self.http.post(this.baseurl + "wheeldata.php", webServiceData)
              .subscribe((data) => {
                let result = JSON.parse(data["_body"]);
                console.log(result);
                if (result.status == "success") {
                  self.toastCtrl.presentToast("Medicine deleted successfully");
                  self.LoadData(Todaydate);
                  self.updateClockHours();
                } else {
                  // self.toastCtrl.presentToast("There is a problem deleting data");
                }
              }, (err) => {
                console.log(err);
              });
          },
        },
      ],
    });
    alert.present();
  }

  // Wheel Segment start

  updateClockHours() {
    // var actualClockTxt = "";
    var clockStartPos = 6;
    var calMinutePos = 0;
    var abc = this;

    abc.whichClockforTxt = (abc.defaultMinutes == 15) ? 0 : 1;
    for (let i = 0; i < abc.numNodes; i++) {
      if (
        (abc.calMinute == 8 && i % 4 == 0) || (abc.calMinute == 4 && i % 2 == 0)
      ) {
        calMinutePos = 0;
      }
      let actualClockTxt = (calMinutePos == 0)
        ? (clockStartPos == 0 ? "24/0" : clockStartPos)
        : (clockStartPos - 1 < 0)
          ? "23." + abc.minuteTxt[abc.whichClockforTxt][calMinutePos - 1]
          : (clockStartPos - 1) + "." +
          abc.minuteTxt[abc.whichClockforTxt][calMinutePos - 1];
      calMinutePos++;
      abc.arrClockSleep[actualClockTxt] = abc.sleepColors["default"];
      abc.arrClockMedicine[actualClockTxt] = abc.medicineColors["default"];
      abc.arrClockActivity[actualClockTxt] = abc.activityColors["default"];

      abc.arrReturn[actualClockTxt] = new Array();
      abc.arrReturn[actualClockTxt]["sleep"] = abc
        .arrReturn[actualClockTxt]["medicine"] = abc
          .arrReturn[actualClockTxt]["activity"] = "";
      abc.arrReturn[actualClockTxt]["sleepText"] = abc
        .arrReturn[actualClockTxt]["medicineText"] = abc
          .arrReturn[actualClockTxt]["activityText"] = abc
            .arrReturn[actualClockTxt]["activityTextTime"] = abc
              .arrReturn[actualClockTxt]["activityTextDiscripation"] = abc
                .arrReturn[actualClockTxt]["sleepTextTime"] = abc
                  .arrReturn[actualClockTxt]["sleepTextDiscripation"] = abc
                    .arrReturn[actualClockTxt]["medicineTextTime"] = abc
                      .arrReturn[actualClockTxt]["medicineTextDiscripation"] =
        abc.arrReturn[actualClockTxt]["activityColor"] = abc
          .arrReturn[actualClockTxt]["medicineColor"] = abc
            .arrReturn[actualClockTxt]["sleepColor"] = "";
      abc.arrReturn[actualClockTxt]["activityStartPoint"] = abc
        .arrReturn[actualClockTxt]["activityEndPoint"] = "";
      clockStartPos = (clockStartPos >= 24)
        ? 0
        : ((abc.calMinute == 8 && i % 4 == 0) ||
          (abc.calMinute == 4 && i % 2 == 0))
          ? (clockStartPos + 1)
          : clockStartPos;
    }
  }

  resetData() {
    $('input[name="sleep_starttime"]').val("");
    $('input[name="sleep_endtime"]').val("");
    $('select[name="sleepstatus"]').val("default");

    $('input[name="medicine_time"]').val("");
    $('select[name="medicinestatus"]').val("default");

    $('input[name="activity_starttime"]').val("");
    $('input[name="activity_endtime"]').val("");
    $('select[name="activitystatus"]').val("default");
  }

  GenerateCareGraphs() {
    var abc = this;
    abc.drawActivity(250);
    abc.drawMedicine(220);
    abc.drawSleepCapsule(190);
    abc.drawClock(170);
  }

  drawSleepCapsule(radiusVal) {
    var Sleepradius = radiusVal;
    var data = { title: "", color: "", discripation: "", time: "" };
    var numNodes = 96;
    var abc = this;
    var Sleepnodes = this.createNodes(numNodes, Sleepradius);

    var tip = d3.tip()
      .attr("class", "d3-tip")
      .offset([-10, 0])
      .html(function (d) {
        var title = d.title.split(",");
        var time = d.time.split(",");
        var discription = d.discripation.split(",");
        var color = d.color.split(",");
        var html =
          '<div class="activites_popup"> <div class="" style="text-align: left;"><strong>Wellbeing Status</strong></div>';
        for (let i = 0; i < title.length; i++) {
          html +=
            '<div class="act_cont" style="padding-top:5px;"> <div class="activity" style="padding-top:10px; display:flex;"> <div class="point_dot" style="background: ' +
            color[i] +
            '; "></div> <span style="color: #81889f;display: flex;">' +
            time[i] +
            '</span><span style="padding-left:5px; color: #81889f;display: flex;">' +
            title[i] +
            '</span> </div> <div class="activity" style="padding-top:10px; display:flex;"><span padding-left:="" 3px;="" style="color: #81889f;display: flex;">' +
            discription[i] + "</span></div> </div>";
        }
        html += "</div>";
        return html;
        // return '<div class=""><div><div class="" style="text-align: left; padding: 0px 10px 10px 1px; "><strong>Current status</strong></div><div class="" style="display: flex; "><div class="point_dot" style="background: '+d.color+'; "></div><span style="color: #81889f;display: flex;">'+d.time+'</span><span style="padding-left:5px; color: #81889f;display: flex;">'+d.title+'</span></div><div class="" style="text-align: left; padding: 15px 10px 8px 0px; "><strong>Discription</strong></div><div class="" style="display: flex; "><span padding-left: 3px; style="color: #81889f;display: flex;">'+d.discripation+'</span></div></div></div>';
      });

    var createSleepSvg = function (radius, callback) {
      var svg = d3.select("#sleepcanvas").append("svg:svg")
        .attr("width", (radius * 2) + 50)
        .attr("height", (radius * 2) + 50);
      svg.call(tip);
      callback(svg);
    };

    var createCapsule = function (svg, nodes, elementRadius, numNodes) {
      var anglePerCapsule = 360 / abc.numNodes;
      var rotateAngle = 0;

      var element = svg.selectAll("circle").data(nodes).enter().append(
        "svg:rect",
      )
        .attr("x", function (d, i) {
          return d.x - 5;
        })
        .attr("y", function (d, i) {
          return d.y - 8;
        })
        .attr("rx", elementRadius)
        .attr("ry", elementRadius)
        .attr("width", 20)
        .attr("height", 10.1)
        .attr("fill", function (d, i) {
          return abc.arrClockSleep[d.clockActualTxt];
        })
        .attr("stroke", function (d, i) {
          return d.defaultCapsuleStroke;
        })
        .attr("stroke-width", 0.2)
        .attr("transform", function (d, i) {
          rotateAngle = anglePerCapsule * i;
          return "rotate(" + rotateAngle + "," + d.x + "," + d.y + ")";
        })
        .on("click", function (d, i) {
          if (abc.arrReturn[d.clockActualTxt]["sleepText"] != "") {
            data.title = abc.arrReturn[d.clockActualTxt]["sleep"];
            data.color = abc.arrReturn[d.clockActualTxt]["sleepColor"];
            data.time = abc.arrReturn[d.clockActualTxt]["sleepTextTime"];
            data.discripation =
              abc.arrReturn[d.clockActualTxt]["sleepTextDiscripation"];
            tip.show(data, i);
          }
        })
        .on("focusout", function (d, i) {
          if (abc.arrReturn[d.clockActualTxt]["sleepText"] != "") {
            data.title = abc.arrReturn[d.clockActualTxt]["sleep"];
            data.color = abc.arrReturn[d.clockActualTxt]["sleepColor"];
            data.time = abc.arrReturn[d.clockActualTxt]["sleepTextTime"];
            data.discripation =
              abc.arrReturn[d.clockActualTxt]["sleepTextDiscripation"];
            tip.hide(data, i);
          }
        });
    };

    createSleepSvg(Sleepradius, function (svg) {
      createCapsule(svg, Sleepnodes, 6, abc.numNodes);
    });
  }

  drawMedicine(radiusVal) {
    var Medicineradius = radiusVal;
    var data = { title: "", color: "", discripation: "", time: "" };
    var abc = this;
    var Medicinenodes = abc.createNodes(abc.numNodes, Medicineradius);

    var tip = d3.tip()
      .attr("class", "d3-tip")
      .offset([-10, 0])
      .html(function (d) {
        var title = d.title.split(",");
        var time = d.time.split(",");
        var discription = d.discripation.split(",");
        var color = d.color.split(",");
        var html =
          '<div class="activites_popup"> <div class="" style="text-align: left;"><strong>Medicines</strong></div>';
        for (let i = 0; i < title.length; i++) {
          html +=
            '<div class="act_cont" style="padding-top:5px;"> <div class="activity" style="padding-top:10px; display:flex;"> <div class="point_dot" style="background: ' +
            color[i] +
            '; "></div> <span style="color: #81889f;display: flex;">' +
            time[i] +
            '</span><span style="padding-left:5px; color: #81889f;display: flex;">' +
            title[i] +
            '</span> </div> <div class="activity" style="padding-top:10px; display:flex;"><span padding-left:="" 3px;="" style="color: #81889f;display: flex;">' +
            discription[i] + "</span></div> </div>";
        }
        html += "</div>";
        return html;
        // return '<div class=""><div><div class="" style="text-align: left; padding: 0px 10px 10px 1px; "><strong>Medicines</strong></div><div class="" style="display: flex; "><div class="point_dot" style="background: '+d.color+'; "></div><span style="color: #81889f;display: flex;">'+d.time+'</span><span style="padding-left:5px; color: #81889f;display: flex;">'+d.title+'</span></div><div class="" style="text-align: left; padding: 15px 10px 8px 0px; "><strong>Discription</strong></div><div class="" style="display: flex; "><span padding-left: 3px; style="color: #81889f;display: flex;">'+d.discripation+'</span></div></div></div>';

      });
    var createMedicineSvg = function (radius, callback) {
      //d3.selectAll('svg').remove();
      var svg = d3.select("#medicinecanvas").append("svg:svg")
        .attr("width", (radius * 2) + 50)
        .attr("height", (radius * 2) + 50);
      svg.call(tip);
      callback(svg);
    };

    var createMedicine = function (svg, nodes, elementRadius, numNodes) {
      var anglePerCapsule = 360 / abc.numNodes;
      var rotateAngle = 0;

      var element = svg.selectAll("circle").data(nodes).enter().append(
        "svg:rect",
      )
        .attr("x", function (d, i) {
          return d.x - 5;
        })
        .attr("y", function (d, i) {
          return d.y - 8;
        })
        .attr("rx", elementRadius)
        .attr("ry", elementRadius)
        .attr("width", 20)
        .attr("height", 10.1)
        .attr("fill", function (d, i) {
          return abc.arrClockMedicine[d.clockActualTxt];
        })
        .attr("stroke", "#2b3249")
        .attr("stroke-width", 0.2)
        .attr("transform", function (d, i) {
          rotateAngle = anglePerCapsule * i;
          return "rotate(" + rotateAngle + "," + d.x + "," + d.y + ")";
        })
        .on("click", function (d, i) {
          console.log("datsfsdfsda", abc)
          if (abc.arrReturn[d.clockActualTxt]["medicineText"] != "") {

            data.title = abc.arrReturn[d.clockActualTxt]["medicine"];
            data.color = abc.arrReturn[d.clockActualTxt]["medicineColor"];
            data.time = abc.arrReturn[d.clockActualTxt]["medicineTextTime"];
            data.discripation =
              abc.arrReturn[d.clockActualTxt]["medicineTextDiscripation"];
            tip.show(data, i);
          }
        })
        .on("focusout", function (d, i) {
          if (abc.arrReturn[d.clockActualTxt]["medicineText"] != "") {
            data.title = abc.arrReturn[d.clockActualTxt]["medicine"];
            data.color = abc.arrReturn[d.clockActualTxt]["medicineColor"];
            data.time = abc.arrReturn[d.clockActualTxt]["medicineTextTime"];
            data.discripation =
              abc.arrReturn[d.clockActualTxt]["medicineTextDiscripation"];
            tip.hide(data, i);
          }
        });

      element = svg.selectAll("circle").data(nodes).enter().append("svg:line")
        .attr("x1", function (d, i) {
          return d.x + 5;
        })
        .attr("y1", function (d, i) {
          return d.y - 8;
        })
        .attr("x2", function (d, i) {
          return d.x + 5;
        })
        .attr("y2", function (d, i) {
          return d.y + 2;
        })
        .attr("stroke", "#2b3249")
        .attr("stroke-width", 2)
        .attr("transform", function (d, i) {
          rotateAngle = anglePerCapsule * i;
          return "rotate(" + rotateAngle + "," + d.x + "," + d.y + ")";
        }).on("click", function (d, i) {
          console.log("abc",abc)
          if (abc.arrReturn[d.clockActualTxt]["medicineText"] != "") {
            data.title = abc.arrReturn[d.clockActualTxt]["medicine"];
            data.color = abc.arrReturn[d.clockActualTxt]["medicineColor"];
            data.time = abc.arrReturn[d.clockActualTxt]["medicineTextTime"];
            data.discripation =
              abc.arrReturn[d.clockActualTxt]["medicineTextDiscripation"];
            tip.show(data, i);
          }
        })
        .on("focusout", function (d, i) {
          if (abc.arrReturn[d.clockActualTxt]["medicineText"] != "") {
            data.title = abc.arrReturn[d.clockActualTxt]["medicine"];
            data.color = abc.arrReturn[d.clockActualTxt]["medicineColor"];
            data.time = abc.arrReturn[d.clockActualTxt]["medicineTextTime"];
            data.discripation =
              abc.arrReturn[d.clockActualTxt]["medicineTextDiscripation"];
            tip.hide(data, i);
          }
        });
    };

    createMedicineSvg(Medicineradius, function (svg) {
      createMedicine(svg, Medicinenodes, 6, 96);
    });
  }

  drawActivity(radiusVal) {
    var abc = this;
    var data = { title: "", color: "", discripation: "", time: "" };
    var Activityradius = radiusVal;
    var Activitynodes = abc.createNodes(abc.numNodes, Activityradius);

    var tip = d3.tip()
      .attr("class", "d3-tip")
      .offset([-10, 0])
      .html(function (d) {
        var title = d.title.split(",");
        var time = d.time.split(",");
        var discription = d.discripation.split(",");
        var color = d.color.split(",");

        var html =
          '<div class="activites_popup"> <div class="" style="text-align: left;"><strong>Activities</strong></div>';
        for (let i = 0; i < title.length; i++) {
          html +=
            '<div class="act_cont" style="padding-top:5px;"> <div class="activity" style="padding-top:10px; display:flex;"> <div class="point_dot" style="background: ' +
            color[i] +
            '; "></div> <span style="color: #81889f;display: flex;">' +
            time[i] +
            '</span><span style="padding-left:5px; color: #81889f;display: flex;">' +
            title[i] +
            '</span> </div> <div class="activity" style="padding-top:10px; display:flex;"><span padding-left:="" 3px;="" style="color: #81889f;display: flex;">' +
            discription[i] + "</span></div> </div>";
        }
        html += "</div>";
        return html;
        // return '<div class=""><div><div class="" style="text-align: left; padding: 0px 10px 10px 1px; "><strong>Activities</strong></div><div class="" style="display: flex; "><div class="point_dot" style="background: '+d.color+'; "></div><span style="color: #81889f;display: flex;">'+d.time+'</span><span style="padding-left:5px; color: #81889f;display: flex;">'+d.title+'</span></div><div class="" style="text-align: left; padding: 15px 10px 8px 0px; "><strong>Discription</strong></div><div class="" style="display: flex; "><span padding-left: 3px; style="color: #81889f;display: flex;">'+d.discripation+'</span></div></div></div>';
      });

    var createActivitySvg = function (radius, callback) {
      var svg = d3.select("#activitycanvas1").append("svg:svg")
        .attr("width", (radius * 2) + 50)
        .attr("height", (radius * 2) + 50);
      svg.call(tip);
      callback(svg);
    };

    var createActivity = function (svg, nodes, elementRadius, numNodes) {
      var anglePerCapsule = 360 / numNodes;
      var rotateAngle = 0;

      $.each(nodes, function (d, i) {
        var nextElement = d + 1;
        if (typeof nodes[nextElement] === "undefined") {
          nextElement = 0;
        }
        var element = svg.append("svg:line")
          .attr("x1", i.x - 5)
          .attr("y1", i.y - 8)
          .attr("x2", nodes[nextElement].x - 5)
          .attr("y2", nodes[nextElement].y - 8)
          // .attr('stroke', ((abc.arrReturn[nodes[d].clockActualTxt]['activity'] == abc.arrReturn[nodes[d].clockActualTxt]['activity']) || (abc.arrReturn[nodes[d].clockActualTxt]['activity'] != "" && abc.arrReturn[nodes[d].clockActualTxt]['activity'] != "")) ? abc.arrClockActivity[i.clockActualTxt] : abc.activityColors['default'])
          .attr(
            "stroke",
            (abc.arrReturn[nodes[nextElement].clockActualTxt]["activity"] == "")
              ? abc.activityColors["default"]
              : (abc.arrReturn[nodes[nextElement].clockActualTxt]["activity"]
                .split(",").length > 1)
                ? abc.arrClockActivity[i.clockActualTxt]
                : (abc.arrReturn[i.clockActualTxt]["activity"] != "")
                  ? abc.arrClockActivity[nodes[nextElement].clockActualTxt]
                  : abc.activityColors["default"],
          )
          .attr("stroke-width", 3)
          .on("click", function (d, i) {
            console.log("hello",abc)
            if (
              abc
                .arrReturn[nodes[nextElement].clockActualTxt]["activityText"] !=
              ""
            ) {
              data.title =
                abc.arrReturn[nodes[nextElement].clockActualTxt]["activity"];
              data.color =
                abc
                  .arrReturn[nodes[nextElement].clockActualTxt][
                "activitycolor"
                ];
              data.time =
                abc
                  .arrReturn[nodes[nextElement].clockActualTxt][
                "activityTextTime"
                ];
              data.discripation =
                abc
                  .arrReturn[nodes[nextElement].clockActualTxt][
                "activityTextDiscripation"
                ];
              tip.show(data, i);
            }
          })
          .on("focusout", function (d, i) {
            if (
              abc
                .arrReturn[nodes[nextElement].clockActualTxt]["activityText"] !=
              ""
            ) {
              data.title =
                abc.arrReturn[nodes[nextElement].clockActualTxt]["activity"];
              data.color =
                abc
                  .arrReturn[nodes[nextElement].clockActualTxt][
                "activitycolor"
                ];
              data.time =
                abc
                  .arrReturn[nodes[nextElement].clockActualTxt][
                "activityTextTime"
                ];
              data.discripation =
                abc
                  .arrReturn[nodes[nextElement].clockActualTxt][
                "activityTextDiscripation"
                ];
              tip.hide(data, i);
            }
          });

        // console.log('abc.arrReturn[i.clockActualTxt]',abc.arrReturn[i.clockActualTxt]);

        if (
          abc.arrReturn[i.clockActualTxt]["activity"] != "" &&
          abc.arrReturn[i.clockActualTxt]["activityText"] != ""
        ) {
          if (abc.arrReturn[i.clockActualTxt]["activityStartPoint"] != "") {
            var startPointStroke =
              (abc.arrReturn[i.clockActualTxt]["activityStartPoint"]);
            element = svg.append("svg:circle")
              .attr("r", 8)
              .attr("cx", i.x - 5)
              .attr("cy", i.y - 8)
              .attr(
                "fill",
                abc.arrReturn[i.clockActualTxt]["activityStartPoint"],
              )
              .attr("stroke", startPointStroke)
              .attr("stroke-width", 3)
              .on("click", function (i) {
                if (
                  abc
                    .arrReturn[nodes[nextElement].clockActualTxt][
                  "activityText"
                  ] != ""
                ) {
                  data.title =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt]["activity"];
                  data.color =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt][
                    "activitycolor"
                    ];
                  data.time =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt][
                    "activityTextTime"
                    ];
                  data.discripation =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt][
                    "activityTextDiscripation"
                    ];
                  tip.show(data, i);
                }
              })
              .on("focusout", function (d, i) {
                if (
                  abc
                    .arrReturn[nodes[nextElement].clockActualTxt][
                  "activityText"
                  ] != ""
                ) {
                  data.title =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt]["activity"];
                  data.color =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt][
                    "activitycolor"
                    ];
                  data.time =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt][
                    "activityTextTime"
                    ];
                  data.discripation =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt][
                    "activityTextDiscripation"
                    ];
                  tip.hide(data, i);
                }
              });
          }

          if (abc.arrReturn[i.clockActualTxt]["activityEndPoint"] != "") {
            var endPointStroke =
              (abc.arrReturn[i.clockActualTxt]["activityEndPoint"]);
            element = svg.append("svg:circle")
              .attr("r", 5)
              .attr("cx", i.x - 5)
              .attr("cy", i.y - 8)
              .attr("fill", abc.arrReturn[i.clockActualTxt]["activityEndPoint"])
              .attr("stroke", endPointStroke)
              .attr("stroke-width", 2.5)
              .on("click", function (d, i) {
                if (
                  abc
                    .arrReturn[nodes[nextElement].clockActualTxt][
                  "activityText"
                  ] != ""
                ) {
                  data.title =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt]["activity"];
                  data.color =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt][
                    "activitycolor"
                    ];
                  data.time =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt][
                    "activityTextTime"
                    ];
                  data.discripation =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt][
                    "activityTextDiscripation"
                    ];
                  tip.show(data, i);
                }
              })
              .on("focusout", function (d, i) {
                if (
                  abc
                    .arrReturn[nodes[nextElement].clockActualTxt][
                  "activityText"
                  ] != ""
                ) {
                  data.title =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt]["activity"];
                  data.color =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt][
                    "activitycolor"
                    ];
                  data.time =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt][
                    "activityTextTime"
                    ];
                  data.discripation =
                    abc
                      .arrReturn[nodes[nextElement].clockActualTxt][
                    "activityTextDiscripation"
                    ];
                  tip.hide(data, i);
                }
              });
          }
        }
      });
    };

    createActivitySvg(Activityradius, function (svg) {
      createActivity(svg, Activitynodes, 6, abc.numNodes);
    });
  }

  drawClock(radiusVal) {
    var radius = radiusVal;
    var abc = this;
    var nodes = abc.createNodes(this.numNodes, radius);

    var createSvg = function (radius, callback) {
      var svg = d3.select("#canvas").append("svg:svg")
        .attr("width", (radius * 2) + 50)
        .attr("height", (radius * 2) + 50);
      callback(svg);
    };
    var createElements = function (svg, nodes, elementRadius, numNodes) {
      var element = svg.selectAll("circle").data(nodes).enter().append(
        "svg:circle",
      )
        .attr("r", elementRadius)
        .attr("cx", function (d, i) {
          return d.x;
        })
        .attr("cy", function (d, i) {
          return d.y;
        })
        .attr("class", function (d, i) {
          if (
            (this.defaultMinutes == 30 && i % 2 == 0) ||
            (this.defaultMinutes == 15 && i % 4 == 0)
          ) {
            return "fillnone";
          } else {
            return "";
          }
        });
    };

    var CreateTextElements = function (svg, nodes, elementRadius, numNodes) {
      $.each(nodes, function (d, i) {
        if (i.clockTxt != "") {
          svg.append("text")
            .attr("x", i.xposition)
            .attr("y", i.yposition)
            .attr("fill", i.clockTxtColor)
            .attr("font-size", i.textsize)
            .text(i.clockTxt);
        }
      });
    };

    createSvg(radius, function (svg) {
      createElements(svg, nodes, 3, abc.numNodes);
      CreateTextElements(svg, nodes, 0, abc.numNodes);
    });
  }

  createNodes(numNodes, radius) {
    var nodes = [],
      width = (radius * 2) + 50,
      height = (radius * 2) + 50,
      angle,
      x,
      y,
      i;
    var clockTxt: any = "";
    var FillColor = "";
    var actualClockTxt = "";
    var clockStartPos = 6;
    var calMinutePos = 0;
    var txtXPosition = 0;
    var txtYPosition = 0;
    var DefaultTextSize = "";

    this.whichClockforTxt = (this.defaultMinutes == 15) ? 0 : 1;
    for (i = 0; i < this.numNodes; i++) {
      angle = (i / (this.numNodes / 2)) * Math.PI; // Calculate the angle at which the element will be placed.
      // For a semicircle, we would use (i / numNodes) * Math.PI.
      x = (radius * Math.cos(angle)) + (width / 2); // Calculate the x position of the element.
      y = (radius * Math.sin(angle)) + (width / 2); // Calculate the y position of the element.

      if (
        (this.calMinute == 8 && i % 4 == 0) ||
        (this.calMinute == 4 && i % 2 == 0)
      ) {
        calMinutePos = 0;
      }
      clockTxt = (calMinutePos == 0)
        ? (clockStartPos == 0 ? "24/0" : clockStartPos)
        : (clockStartPos - 1 < 0)
          ? "23." + this.minuteTxt[this.whichClockforTxt][calMinutePos - 1]
          : (clockStartPos - 1) + "." +
          this.minuteTxt[this.whichClockforTxt][calMinutePos - 1];
      calMinutePos++;
      FillColor = (clockStartPos % 2 == 0) ? "white" : "#aaa";
      txtXPosition = (clockTxt < 5)
        ? x - 2
        : ((clockTxt == "24/0")
          ? x - 10
          : ((clockTxt > 9 && clockTxt <= 23) ? x - 6 : x - 2));
      txtYPosition = (clockTxt < 5)
        ? y + 2
        : ((clockTxt == "24/0")
          ? y + 2
          : ((clockTxt > 9 && clockTxt <= 23) ? y + 4 : y + 5));
      DefaultTextSize = (clockTxt == "24/0") ? "8px" : "10px";
      if (this.defaultMinutes == 30) {
        DefaultTextSize = "12px";
        txtXPosition = (clockTxt < 5)
          ? x - 5
          : ((clockTxt == "24/0") ? x - 13 : (clockTxt > 9 && clockTxt <= 23)
            ? x - 8
            : x - 2);
        txtYPosition = y + 2;
      }
      actualClockTxt = clockTxt;
      if (
        (this.defaultMinutes == 30 && i % 2 != 0) ||
        (this.defaultMinutes == 15 && i % 4 != 0)
      ) {
        clockTxt = "";
      }
      nodes.push(
        {
          "id": i,
          "x": x,
          "y": y,
          "clockTxt": clockTxt,
          "clockActualTxt": actualClockTxt,
          "clockTxtColor": FillColor,
          "textsize": DefaultTextSize,
          "xposition": txtXPosition,
          "yposition": txtYPosition,
          "defaultCapsuleStroke": "black",
        },
      );
      clockStartPos = (clockStartPos >= 24)
        ? 0
        : ((this.calMinute == 8 && i % 4 == 0) ||
          (this.calMinute == 4 && i % 2 == 0))
          ? (clockStartPos + 1)
          : clockStartPos;
    }
    return nodes;
  }

  calltoupdateStatus(val) {
    var blcheck = true;
    var abc = this;
    if (val == "sleep") {
      if (blcheck) {
        $("#changedata-modal").delay(1000).fadeOut(450);
        setTimeout(function () {
          d3.select("#sleepcanvas > svg").remove();
          $("#changedata-modal").modal("hide").hide();
          abc.updateCapsuleData(val);
          abc.drawSleepCapsule(190);
        }, 1000);
      }
    } else if (val == "medicine") {
      if (blcheck) {
        $("#changedata-modal").delay(1000).fadeOut(450);
        setTimeout(function () {
          d3.select("#medicinecanvas > svg").remove();
          $("#changedata-modal").modal("hide").hide();
          abc.updateCapsuleData("medicine");
          abc.drawMedicine(220);
        }, 1000);
      }
    } else if (val == "activity") {
      if (blcheck) {
        $("#changedata-modal").delay(1000).fadeOut(450);
        setTimeout(function () {
          d3.select("#activitycanvas1 > svg").remove();
          $("#changedata-modal").modal("hide").hide();
          abc.updateCapsuleData("activity");
          abc.drawActivity(250);
        }, 1000);
      }
    } else {
      alert("Something went wrong. Please try again!");
      return false;
    }
  }

  updateCapsuleData(whichCircle) {
    var abc = this;
    if (whichCircle == "medicine") {
      for (let A = 0; A < abc.medicineStart.length; A++) {
        var status = this.medicineStatus[A];
        var updatedColor = this.wheelMedicineColor[A];
        var Start = abc.medicineStart[A].split(":");

        // this.startHours = Start[0];
        // this.startMineuts = Start[1];

        // this.startMineuts = (Math.round(Start[1]/15) * 15) % 60;
        // if(this.startMineuts == 0){
        //   this.startHours = ((((Start[1]/105) | 0) + Start[0]) % 24) + 1;
        //   if(this.startHours == 24){
        //     this.startHours = 0;
        //   }
        // }
        // else{
        //   this.startHours = ((((Start[1]/105) + .5) | 0) + Start[0]) % 24;
        // }

        if (Start[1] >= 53) {
          this.startMineuts = 45;
          this.startHours = ((((Start[1] / 105) | 0) + Start[0]) % 24);
        } else {
          this.startMineuts = (Math.round(Start[1] / 15) * 15) % 60;
          this.startHours = ((((Start[1] / 105) | 0) + Start[0]) % 24);
          if (this.startHours == 24) {
            this.startHours = 0;
          }
        }

        var medicineHour = abc.startHours;
        var medicineMinute = abc.startMineuts;

        var HourKey = (medicineHour == 0 && medicineMinute == 0)
          ? "24/0"
          : (medicineHour > 24)
            ? medicineHour - 24
            : medicineHour;
        var circleKey = (medicineMinute == 0)
          ? (HourKey == "24/0") ? HourKey : parseFloat(HourKey)
          : parseFloat(HourKey) + "." + medicineMinute;
        var arrExistingValues = [];
        this.medicineDes = [];
        this.medicineColor = [];

        if (status != "default") {
          if (this.arrReturn[circleKey][whichCircle] != "") {
            var listOfExistingValues = this.arrReturn[circleKey][whichCircle];
            arrExistingValues = listOfExistingValues.split(",");
            if (arrExistingValues.indexOf(status) != 0) {
              arrExistingValues.push(status);
            }

            var listOfMidicineDescr =
              this.arrReturn[circleKey]["medicineTextDiscripation"];
            this.medicineDes = listOfMidicineDescr.split(",");
            if (this.medicineDes.indexOf(this.medicineDescription[A]) != 0) {
              this.medicineDes.push(this.medicineDescription[A]);
            }

            var listOfMidicinecolor =
              this.arrReturn[circleKey]["medicineColor"];
            this.medicineColor = listOfMidicinecolor.split(",");
            if (this.medicineColor.indexOf(this.wheelMedicineColor[A]) != 0) {
              this.medicineColor.push(this.wheelMedicineColor[A]);
            }
          } else {
            arrExistingValues.push(status);
            this.medicineDes.push(this.medicineDescription[A]);
            this.medicineColor.push(this.wheelMedicineColor[A]);
          }
        }

        abc.arrReturn[circleKey]["medicineTextTime"] = "";
        abc.arrReturn[circleKey]["medicineTextDiscripation"] = "";
        abc.arrReturn[circleKey]["medicineColor"] = "";

        if (arrExistingValues.length == 1 || status == "default") {
          this.arrClockMedicine[circleKey] = updatedColor;
          abc.arrReturn[circleKey]["medicineTextTime"] = this.medicineStart[A];
          // abc.arrReturn[circleKey]['medicineTextDiscripation'] = this.medicineDescription[A];
        } else {
          this.arrClockMedicine[circleKey] =
            this.medicineColors[whichCircle + "multiple"];
          abc.arrReturn[circleKey]["medicineTextTime"] = this.medicineStart[A];
        }

        listOfExistingValues = arrExistingValues.join(",");
        var MedDes = this.medicineDes.join(",");
        var MedColor = this.medicineColor.join(",");
        this.arrReturn[circleKey][whichCircle] = listOfExistingValues;
        abc.arrReturn[circleKey]["medicineTextDiscripation"] = MedDes;
        abc.arrReturn[circleKey]["medicineColor"] = MedColor;

        var arrExistingLabels = [];
        $.each(arrExistingValues, function (key, item) {
          var textLabel = status; //(item == "medicinelosec") ? "Losec" : (item == "medicinekepra") ? "Kepra" : (item == "medicinezonegran") ? "Zonegran" : (item == "medicinemovicol") ? "Movicol Junior" : "";
          if (textLabel != "") {
            arrExistingLabels.push(textLabel);
          }
        });
        this.arrReturn[circleKey][whichCircle + "Text"] = arrExistingLabels
          .join(" & ");
      }
    } else if (whichCircle == "sleep") {
      for (let D = 0; D < abc.startTime.length; D++) {
        var status = this.status[D];
        var updatedColor = this.wheelEventColor[D];
        var Start = abc.startTime[D].split(":");
        var End = abc.endTime[D].split(":");

        // this.startHours = Start[0];
        // this.startMineuts = Start[1];
        // this.endHours = End[0];
        // this.endMinites = End[1];

        if (Start[1] >= 53) {
          this.startMineuts = 45;
          this.startHours = ((((Start[1] / 105) | 0) + Start[0]) % 24);
        } else {
          this.startMineuts = (Math.round(Start[1] / 15) * 15) % 60;
          this.startHours = ((((Start[1] / 105) | 0) + Start[0]) % 24);
          if (this.startHours == 24) {
            this.startHours = 0;
          }
        }

        if (End[1] >= 53) {
          this.endMinites = 45;
          this.endHours = ((((End[1] / 105) | 0) + End[0]) % 24);
        } else {
          this.endMinites = (Math.round(End[1] / 15) * 15) % 60;
          this.endHours = ((((End[1] / 105) | 0) + End[0]) % 24);
          if (this.endHours == 24) {
            this.endHours = 0;
          }
        }

        for (var i = this.startHours; i <= this.endHours; i++) {
          for (var m = 0; m < 60; m += parseFloat(this.defaultMinutes)) {
            var updateCircleStatus = true;
            if (i == this.startHours) {
              updateCircleStatus = false;
              if (m >= this.startMineuts) {
                updateCircleStatus = true;
              }
            }

            if (updateCircleStatus) {
              HourKey = (i == 0 && m == 0) ? "24/0" : (i > 24) ? i - 24 : i;
              var circleKey = (m == 0)
                ? (HourKey == "24/0") ? HourKey : parseFloat(HourKey)
                : parseFloat(HourKey) + "." + m;
              arrExistingValues = [];
              this.sleepTime = [];
              this.sleepDes = [];
              this.sleepColor = [];

              if (status != "default") {
                if (this.arrReturn[circleKey][whichCircle] != "") {
                  var listOfExistingValues =
                    this.arrReturn[circleKey][whichCircle];
                  arrExistingValues = listOfExistingValues.split(",");
                  if (arrExistingValues.indexOf(status) != 0) {
                    arrExistingValues.push(status);
                  }

                  var listOfSleepTime =
                    this.arrReturn[circleKey]["sleepTextTime"];
                  this.sleepTime = listOfSleepTime.split(",");
                  if (
                    this.sleepTime.indexOf(
                      this.startTime[D] + "-" + this.endTime[D],
                    ) != 0
                  ) {
                    this.sleepTime.push(
                      this.startTime[D] + "-" + this.endTime[D],
                    );
                  }

                  var listOfSleepTimeDescr =
                    this.arrReturn[circleKey]["sleepTextDiscripation"];
                  this.sleepDes = listOfSleepTimeDescr.split(",");
                  if (this.sleepDes.indexOf(this.sleepDescription[D]) != 0) {
                    this.sleepDes.push(this.sleepDescription[D]);
                  }

                  var listOfSleepTimecolor =
                    this.arrReturn[circleKey]["sleepColor"];
                  this.sleepColor = listOfSleepTimecolor.split(",");
                  if (this.sleepColor.indexOf(this.wheelEventColor[D]) != 0) {
                    this.sleepColor.push(this.wheelEventColor[D]);
                  }
                } else {
                  arrExistingValues.push(status);
                  this.sleepDes.push(this.sleepDescription[D]);
                  this.sleepTime.push(
                    this.startTime[D] + "-" + this.endTime[D],
                  );
                  this.sleepColor.push(this.wheelEventColor[D]);
                }
              }

              abc.arrReturn[circleKey]["sleepTextTime"] = "";
              abc.arrReturn[circleKey]["sleepTextDiscripation"] = "";
              abc.arrReturn[circleKey]["sleepColor"] = "";

              if (arrExistingValues.length == 1 || status == "default") {
                this.arrClockSleep[circleKey] = updatedColor;
                // abc.arrReturn[circleKey]['sleepTextTime'] = this.startTime[D]+'-'+this.endTime[D];
                // abc.arrReturn[circleKey]['sleepTextDiscripation'] = this.sleepDescription[D];
              } else {
                this.arrClockSleep[circleKey] = updatedColor; //(whichCircle == "sleep") ? this.sleepColors[whichCircle+"multiple"] : "#FFF";
              }

              listOfExistingValues = arrExistingValues.join(",");
              var Sleepstartendtime = this.sleepTime.join(",");
              var sleepDes = this.sleepDes.join(",");
              var sleepColor = this.sleepColor.join(",");

              this.arrReturn[circleKey][whichCircle] = listOfExistingValues;
              abc.arrReturn[circleKey]["sleepTextTime"] = Sleepstartendtime;
              abc.arrReturn[circleKey]["sleepTextDiscripation"] = sleepDes;
              abc.arrReturn[circleKey]["sleepColor"] = sleepColor;

              if (whichCircle == "sleep") {
                var arrExistingLabels = [];
                $.each(arrExistingValues, function (key, item) {
                  var textLabel = status; //(item == "sleepdistress") ? "Distress" : (item == "sleepunawake") ? "Uncomfortable" : (item == "sleep") ? "A Sleep" : (item == "sleepawake") ? "Awake" : "";
                  if (textLabel != "") {
                    arrExistingLabels.push(textLabel);
                  }
                });
                this.arrReturn[circleKey][whichCircle + "Text"] =
                  arrExistingLabels.join(" & ");
              }
            }

            if (i == abc.endHours && m == abc.endMinites) {
              break;
            }
          }
        }
      }
    } else {
      this.circle = [];
      this.startPoint = [];
      for (let C = 0; C < abc.activityStartTime.length; C++) {
        var status = this.activityStatus[C];
        var updatedColor = this.wheelActivityColor[C];
        var Start = abc.activityStartTime[C].split(":");
        var End = abc.activityEndTime[C].split(":");

        // minuteValues="0,15,30,45"

        // this.activityStartHours = Start[0];
        // this.activityStartMineuts = Start[1];
        // this.activityEndHours = End[0];
        // this.activityEndMinites = End[1];

        if (Start[1] >= 53) {
          this.activityStartMineuts = 45;
          this.activityStartHours = ((((Start[1] / 105) | 0) + Start[0]) % 24);
        } else {
          this.activityStartMineuts = (Math.round(Start[1] / 15) * 15) % 60;
          this.activityStartHours = ((((Start[1] / 105) | 0) + Start[0]) % 24);
          if (this.activityStartHours == 24) {
            this.activityStartHours = 0;
          }
        }

        if (End[1] >= 53) {
          this.activityEndMinites = 45;
          this.activityEndHours = ((((End[1] / 105) | 0) + End[0]) % 24);
        } else {
          this.activityEndMinites = (Math.round(End[1] / 15) * 15) % 60;
          this.activityEndHours = ((((End[1] / 105) | 0) + End[0]) % 24);
          if (this.activityEndHours == 24) {
            this.activityEndHours = 0;
          }
        }

        // this.activityEndMinites = (Math.round(End[1]/15) * 15) % 60;
        //   if(this.activityEndMinites == 0){
        //     this.activityEndHours = ((((End[1]/105) | 0) + End[0]) % 24) + 1;
        //     if(this.activityEndHours == 24){
        //       this.activityEndHours = 0;
        //     }
        //   }
        //   else{
        //     this.activityEndHours = ((((End[1]/105) + .5) | 0) + End[0]) % 24;
        //   }

        var counter = 0;

        for (var i = this.activityStartHours; i <= this.activityEndHours; i++) {
          for (var m = 0; m < 60; m += parseFloat(this.defaultMinutes)) {
            var updateCircleStatus = true;
            if (i == this.activityStartHours) {
              updateCircleStatus = false;
              if (m >= this.activityStartMineuts) {
                updateCircleStatus = true;
              }
            }

            if (updateCircleStatus) {
              HourKey = (i == 0 && m == 0) ? "24/0" : (i > 24) ? i - 24 : i;
              var circleKey = (m == 0)
                ? (HourKey == "24/0") ? HourKey : parseFloat(HourKey)
                : parseFloat(HourKey) + "." + m;
              arrExistingValues = [];
              this.arrDes = [];
              this.arrstartendtime = [];
              this.arrcolor = [];
              if (status != "default") {
                if (this.arrReturn[circleKey][whichCircle] != "") {
                  var listOfExistingValues =
                    this.arrReturn[circleKey][whichCircle];
                  arrExistingValues = listOfExistingValues.split(",");
                  if (arrExistingValues.indexOf(status) != 0) {
                    arrExistingValues.push(status);
                  }

                  var listOfExistingValuesact =
                    this.arrReturn[circleKey]["activityTextTime"];
                  this.arrstartendtime = listOfExistingValuesact.split(",");
                  if (
                    this.arrstartendtime.indexOf(
                      this.activityStartTime[C] + "-" + this.activityEndTime[C],
                    ) != 0
                  ) {
                    this.arrstartendtime.push(
                      this.activityStartTime[C] + "-" + this.activityEndTime[C],
                    );
                  }

                  var listOfExistingDescr =
                    this.arrReturn[circleKey]["activityTextDiscripation"];
                  this.arrDes = listOfExistingDescr.split(",");
                  if (this.arrDes.indexOf(this.activityDescription[C]) != 0) {
                    this.arrDes.push(this.activityDescription[C]);
                  }
                  var listOfExistingColor =
                    this.arrReturn[circleKey]["activitycolor"];
                  this.arrcolor = listOfExistingColor.split(",");
                  if (this.arrcolor.indexOf(this.activityStatus[C]) != 0) {
                    this.arrcolor.push(this.wheelActivityColor[C]);
                  }
                } else {
                  arrExistingValues.push(status);
                  this.arrstartendtime.push(
                    this.activityStartTime[C] + "-" + this.activityEndTime[C],
                  );
                  this.arrDes.push(this.activityDescription[C]);
                  this.arrcolor.push(this.wheelActivityColor[C]);
                }
              }

              this.arrReturn[circleKey]["activityStartPoint"] = "";
              this.arrReturn[circleKey]["activityEndPoint"] = "";
              this.arrReturn[circleKey]["activityColor"] = "";

              if (arrExistingValues.length == 1 || status == "default") {
                this.arrClockActivity[circleKey] = updatedColor;
              } else {
                this.arrClockActivity[circleKey] = updatedColor; //(whichCircle == "activity") ? this.activityColors[whichCircle+"multiple"] : "#FFF";
              }

              listOfExistingValues = arrExistingValues.join(",");
              var startendtime = this.arrstartendtime.join(",");
              var Des = this.arrDes.join(",");
              var Activtitycolor = this.arrcolor.join(",");

              this.arrReturn[circleKey][whichCircle] = listOfExistingValues;
              this.arrReturn[circleKey]["activityTextTime"] = startendtime;
              abc.arrReturn[circleKey]["activityTextDiscripation"] = Des;
              abc.arrReturn[circleKey]["activitycolor"] = Activtitycolor;

              if (whichCircle == "activity") {
                if (status != "default" && counter == 0) {
                  var start = this.arrReturn[circleKey]["activity"].split(",");
                  if (start.length !== 1) {
                    this.arrReturn[circleKey]["activityStartPoint"] = "#FF69B4";
                  } else {
                    this.arrReturn[circleKey]["activityStartPoint"] =
                      updatedColor;
                  }

                  var color = {
                    "color": this.arrReturn[circleKey]["activityStartPoint"],
                    "time": circleKey,
                    "type": "start",
                  };
                  this.circle.push(color);

                  for (let d = 0; d < this.end.length; d++) {
                    if (
                      this.activityStartHours == this.end[d] &&
                      this.activityStartMineuts == this.endmin[d]
                    ) {
                      this.arrReturn[circleKey]["activityStartPoint"] =
                        "#FF69B4";
                      this.startPoint.push(circleKey);
                    }
                  }
                }
              }
              var arrExistingLabels = [];
              $.each(arrExistingValues, function (key, item) {
                var textLabel = status;
                if (textLabel != "") {
                  arrExistingLabels.push(textLabel);
                }
              });
              this.arrReturn[circleKey][whichCircle + "Text"] =
                arrExistingLabels.join(" & ");
              counter++;
            }
            if (i == abc.activityEndHours && m == abc.activityEndMinites) {
              var end = this.arrReturn[circleKey]["activity"].split(",");
              if (end.length !== 1) {
                this.arrReturn[circleKey]["activityEndPoint"] = "#FF69B4";
              } else {
                this.arrReturn[circleKey]["activityEndPoint"] = updatedColor;
              }

              var color = {
                "color": this.arrReturn[circleKey]["activityEndPoint"],
                "time": circleKey,
                "type": "end",
              };
              this.circle.push(color);
              break;
            }
          }
        }
      }

      for (let p = 0; p < this.circle.length; p++) {
        if (this.circle[p].type == "start") {
          if ($.inArray(this.circle[p].time, this.startPoint) !== -1) {
            this.arrReturn[this.circle[p].time]["activityStartPoint"] =
              "#FF69B4";
          } else {
            this.arrReturn[this.circle[p].time]["activityStartPoint"] =
              this.circle[p].color;
          }
        } else {
          if ($.inArray(this.circle[p].time, this.startPoint) !== -1) {
            this.arrReturn[this.circle[p].time]["activityEndPoint"] = "#FF69B4";
          } else {
            this.arrReturn[this.circle[p].time]["activityEndPoint"] =
              this.circle[p].color;
          }
        }
      }
    }
    this.resetData();
  }
  ////////// HCP DropDown Code Starts /////////////
  getHCPData(userid, usertype) {
    var keys: any = [];
    var hcp1: any = [];
    let self = this;
    var webServiceData: any = {};
    webServiceData.action = "gethcpdata";
    webServiceData.id = userid;
    webServiceData.usertype = usertype;
    webServiceData.appsecret = this.appsecret;

    self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);
        if (result.status == "success") {
          // result.data;
          hcp1 = hcp1.push(result.data);
          // hcp1 = result.data;
          // return result1;
        } else {
        }
      },
      (err) => {
        console.log(err);
      },
    );
    return hcp1;
  }

  async getHCPid() {
    await this.get("currentHCP").then((result) => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      } else {
        this.getuserLoginData();
        this.hcpid = 0;
      }
    });
  }
  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
        }
        this.getpatientType();
      }
    });
  }
  async getpatientType() {

    await this.get("patientType").then((result) => {
      if (
        (!result || result == null || result == undefined) &&
        this.userLoginData == "patient"
      ) {
        this.doRefresh();
      } else {
        this.patientType = result;
        this.appIntialize1();
      }
    });
  }

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      return true;
    } catch (reason) {
      return false;
    }
  }

  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {

      return null;
    }
  }

  // remove a single key value:
  remove(key: string) {
    this.storage.remove(key);
  }

  doRefresh(event = "") {
    this.loadingService.show();
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.WheelData = [];
      this.loadingService.hide();
    }, 2000);
  }

  selectEmployee($event) {
    this.set("currentHCP", $event);
    this.doRefresh();
  }

  shareWhatsapp() {
    let message = "Daily Timeline Details";
    this.socialShareCtrl.shareViaWhatsApp(message, this.shareImageNi, this.siteName).then(res => {
    }).catch(error => {
    })

  }


  shareTwitter(img) {
    let message = "Daily Timeline Details";
    this.socialShareCtrl.shareViaTwitter(message, img, this.siteName).then(res => {
    }).catch(error => {
      console.log("failed : " + error);
    })
  }


  // shareFacebookHint() {
  //   let pasteData = "Testing";
  //   let message = "Daily Timeline Details";

  //   this.socialShareCtrl.shareViaFacebookWithPasteMessageHint(message, "", this.siteName, pasteData).then(res => {
  //     console.log("success facebook hint: " + res);
  //   }).catch(error => {
  //     console.log("failed facebook hint: " + error);
  //   })
  // }
  shareInsta() {
    let nameAndSite = "Victory Care" + this.siteName;

    this.socialShareCtrl.shareViaInstagram(nameAndSite, this.shareImageNi).then(res => {
    }).catch(error => {
    })
  }
  shareSms() {
    let nameAndSite = "Victory Care" + this.siteName;

    this.socialShareCtrl.shareViaSMS(nameAndSite, "").then(res => {

    }).catch(error => {
      console.log("failed SMS: " + error);
    })
  }

  shareEmail(img) {
    let nameAndSite = this.siteName;
    let subject = "Daily Timeline Details";

    this.socialShareCtrl.canShareViaEmail().then(res => {
      console.log("success can email  : " + res);
      this.socialShareCtrl.shareViaEmail(nameAndSite, subject, [], [], [], img).then(res => {
        console.log("success email : " + res);
      }).catch(error => {
        console.log("failed email: " + error);
      })
    }).catch(error => {
      console.log("failed can email: " + error);
    })
  }

  shareFacebook() {
    var img = "https://lh3.googleusercontent.com/8c4SpecjfcnCdfq2PXTwS7vvjPmrOa-lgbcczv6RBRF-6fgYv7-qCrNEW5owqQRPx15FesYOt3oqJrQJTmk0XZoR4WGEc3UGGbZgt7GvPKOd_pW4_Cbd9yqiHY9mxoHAkr5m-coUo47lsSy484J4kINWBrR3YaXClG_KRLvYDMH5vjipCJsTlh8-1Eg5z-IMON0yW9dwL0C42FPDTJ5pQIE2ilZ7J4CYGSo14XDw87zdZ7GrkrLtuESlupo2rzAX80zaZ4ER-S-o8fhHwvLYNrrYrfrJMCBjduqziJxvuBnB5fMSkn8BUI9JuIcqq9Hp5J2aEwIrb1xQUpW8bbC0o2Y8hVUv2yECJ8F4jieEg5EQaz--sPxjogAEgybsaGS7QJK6yphIm59ulIQRUmcwOEf5oqMHwXUrYfaW4v-hia_9h4vuNWoUOksNstaJa22PVRbZGIZLdFdfPYE9ekVyFvUxqsZ9Ft6pFAXdkD3UX7GaKujky6QFNJDqkCef8Tu0vXQFPycxmIdm4spMgKy6NIc92_p6AQo_7AYrv7pI_vtUL4-Sh6HtQ09o6g0GbprKCnforqQyHWsAOdVQ5M6zjOrh6dNAuxFTl4MHIV8=w353-h588-no";
    let pasteData = "Pasting About Nithyam Tutorials";
    let message = "Daily Timeline Details";

    this.socialShareCtrl.shareViaFacebook(message, img, this.siteName).then(res => {
      console.log("success facebook hint: " + res);
    }).catch(error => {
      console.log("failed facebook hint: " + error);
    })
  }
  shareFacebookHint(img) {
    let pasteData = "Pasting About Nithyam Tutorials";
    let message = "Nithyam Tutorials";

    this.socialShareCtrl.shareViaFacebookWithPasteMessageHint(message, "", this.siteName, pasteData).then(res => {
      console.log("success facebook hint: " + res);
    }).catch(error => {
      console.log("failed facebook hint: " + error);
    })
  }
  takeScreenShot(socailmediaType) {
    console.log("inside take screenshot");
    var message = "Daily Timeline Settings";
    var subject = "Victory Care Data";
    // var img = "https://lh3.googleusercontent.com/8c4SpecjfcnCdfq2PXTwS7vvjPmrOa-lgbcczv6RBRF-6fgYv7-qCrNEW5owqQRPx15FesYOt3oqJrQJTmk0XZoR4WGEc3UGGbZgt7GvPKOd_pW4_Cbd9yqiHY9mxoHAkr5m-coUo47lsSy484J4kINWBrR3YaXClG_KRLvYDMH5vjipCJsTlh8-1Eg5z-IMON0yW9dwL0C42FPDTJ5pQIE2ilZ7J4CYGSo14XDw87zdZ7GrkrLtuESlupo2rzAX80zaZ4ER-S-o8fhHwvLYNrrYrfrJMCBjduqziJxvuBnB5fMSkn8BUI9JuIcqq9Hp5J2aEwIrb1xQUpW8bbC0o2Y8hVUv2yECJ8F4jieEg5EQaz--sPxjogAEgybsaGS7QJK6yphIm59ulIQRUmcwOEf5oqMHwXUrYfaW4v-hia_9h4vuNWoUOksNstaJa22PVRbZGIZLdFdfPYE9ekVyFvUxqsZ9Ft6pFAXdkD3UX7GaKujky6QFNJDqkCef8Tu0vXQFPycxmIdm4spMgKy6NIc92_p6AQo_7AYrv7pI_vtUL4-Sh6HtQ09o6g0GbprKCnforqQyHWsAOdVQ5M6zjOrh6dNAuxFTl4MHIV8=w353-h588-no"
    // this.socialShareCtrl.share(message, subject, img, this.siteName);

    this.screenshot.URI(80).then(response => {
      this.socialShareCtrl.share(message, subject, response.URI, this.siteName);
      // console.log("response from take: " + response);
      // if (socailmediaType == "facebook") {
      //   this.shareFacebookHint(response.URI)
      // }
      // else if (socailmediaType == "twitter") {
      //   this.shareTwitter(response.URI);
      // }
      // else if (socailmediaType == "facebookhint") {
      //   this.shareFacebook();
      // }
      // else if (socailmediaType == "email") {
      //   this.shareEmail(response.URI);
      // }
    }, error => {
      console.log("response from take error : " + error);
    });
  }
  settings() {
    this.navCtrl.push("AppSettingsDailyWheelSettingsPage");
  }
  closeSelect(e) {

    this.takeScreenShot(e)
  }
}

