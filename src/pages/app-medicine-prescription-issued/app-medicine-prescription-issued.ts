import { Component, ViewChild, ElementRef } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController,
  ViewController,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl,
  FormArray,
} from "@angular/forms";
import * as _ from "lodash";
import { AppSharedService } from "../../services/app-shared-service";
import { Storage } from "@ionic/storage";
import { Http } from "@angular/http";
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";

/**
 * Generated class for the AppMedicinePrescriptionIssuedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-app-medicine-prescription-issued",
  templateUrl: "app-medicine-prescription-issued.html",
  providers: [DatabaseService],
})
export class AppMedicinePrescriptionIssuedPage {
  medicineStep2form: FormGroup;
  submitAttempt: boolean;
  data: any = {};
  patientData: any = {};
  medicineData: any = [];
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  AddedMedicine: any = [];
  doctorsList: any = [];
  medicineList: any = [];
  presAddedBy: string;
  date: any = new Date();
  index: any;
  doctorType: boolean;
  userLoginData: any;
  patientType: any;
  disable: boolean = false;
  doctorsFullName: string;
  doctorid: string;
  selectMedicine: string;
  DetailJson: any = [];
  medicineId: any;
  selectdoctorid: any;
  hcpid: any;
  i = 0;
  doctordrop: boolean = true;

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public appSharedService: AppSharedService,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private loadingService: LoadingService,
    private databaseService: DatabaseService,
    public storage: Storage,
    public http: Http,
    private toastCtrl: ToastService,
  ) {
    this.medicineStep2form = formBuilder.group({
      doctor: [""],
      medicine: ["", Validators.required],
      prestartdate: ["", Validators.required],
      preenddate: ["", Validators.required],
      enablePrescriptionNotification: [],
      // doctorid: [],
      areaexpertise: [],
      prescriptionaddress: [],
      tim: this.formBuilder.array([
        this.initTim(),
      ])
    });
    if (this.navParams.data.medicinedata1) {
      this.data.medicine = this.navParams.data.medicinedata1.id;
    }
  }

  initTim() {
    return this.formBuilder.group({
      dose: [''],
      time: ['']
    });
  }

  setInitialTim() {
    if (this.data.tim === undefined) {
      this.data.tim = [];
    }

    if (this.data.tim.length === 0) {
      this.data.tim.push({});
    }
  }

  addTim() {

    const control = <FormArray>this.medicineStep2form.controls['tim'];
    control.push(this.initTim());
    this.data.tim.push({});
    console.log("timmm", this.data.tim)
  }


  deleteTim(index) {
    this.data.tim.splice(index, 1);
  }


  intializeApp1() {
    this.doctorType = (this.userLoginData.usertype == "doctor" ? true : false);
    this.selectMedicine = this.navParams.data.NewAddedMedicineName
      ? this.navParams.data.NewAddedMedicineName
      : "";
    console.log("mediciene", this.selectMedicine);
    if (this.userLoginData.usertype == "doctor") {
      this.doctorsFullName = this.userLoginData.firstname;
      this.disable = true;
      this.doctorid = this.userLoginData.id;
      this.doctorsList = [
        {
          id: this.userLoginData.id,
          email: this.userLoginData.email,
          name: this.userLoginData.firstname,
        },
      ];
      this.data.doctor = this.userLoginData.id + "|" +
        this.userLoginData.firstname + "|" + this.userLoginData.email;
    }
    if (this.patientType == "independent") {
      this.doctordrop = false;
      this.data.doctor = this.userLoginData.id;
    }
    let self = this;
    // this.storage.get('user').then((val) => {
    //   console.log(val);
    //   this.patientData = val;
    // });
    if (
      this.userLoginData.usertype == "healthcareprovider" ||
      this.userLoginData.usertype == "nurse" ||
      this.userLoginData.usertype == "patient"
    ) {
      this.doctorid = this.userLoginData.id;
      var result1: any = [];
      var hcpWebServiceData: any = {};
      // hcpWebServiceData.action = "getdropdowndoctorsdata";
      // hcpWebServiceData.appsecret = this.appsecret;
      // self.http.post(this.baseurl + "getdropdowndata.php", hcpWebServiceData).subscribe(data => {
      //   this.loadingService.show();
      //   let result = JSON.parse(data["_body"]);
      //   if (result.status == 'success') {
      //     this.loadingService.hide();
      //     console.log(result.data);
      //     console.log(this.doctorid);
      //     this.doctorsList = result.data;
      //     var selectDoctorid = this.selectdoctorid;
      //     if (this.selectdoctorid) {
      //       console.log("insidedoctor", selectDoctorid)
      //       var index = this.doctorsList.findIndex(m => m.id === selectDoctorid);
      //       if (index != -1) {
      //         this.medicineStep2form.controls["doctor"].setValue(this.doctorsList[index].id);
      //         this.selectdoctorid = '';
      //       }
      //     }
      //   } else {
      //     this.loadingService.hide();
      //     this.toastCtrl.presentToast("Doctors dropdown is empty");
      //   }
      // }, err => {
      //   this.loadingService.hide();
      //   console.log(err);
      // });
    }
    // });
    this.data.action = "getmedicinemasterdatafulllist";
    if (
      this.patientType == "independent" ||
      this.userLoginData.usertype == "healthcareprovider"
    ) {
      console.log("data12345");
      this.data.userid = this.userLoginData.id;
      this.data.appsecret = this.appsecret;
      this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
        (data) => {
          this.loadingService.show();
          let result = JSON.parse(data["_body"]);
          console.log(result.data);
          if (result.status == "success") {
            this.loadingService.hide();
            if (result.data !== null) {
              this.medicineList = result.data;
              if (this.data.tim !== undefined) {
                for (let i = 0; i < this.data.tim.length; i++) {
                  const control = <FormArray>this.medicineStep2form.controls['tim'];
                  control.push(this.initTim());
                }
              }
              else {
                this.data.tim = [];
              }
              if (this.selectMedicine) {
                console.log("inside", this.selectMedicine);
                var index = this.medicineList.findIndex((m) =>
                  m.name === this.selectMedicine
                );
                if (index != -1) {
                  console.log("inside3", this.selectMedicine);
                  this.medicineStep2form.controls["medicine"].setValue(
                    this.medicineList[index].id,
                  );
                }
              }
            } else {
              this.loadingService.hide();
              this.data = [];
              this.medicineList = [];
            }
          } else {
            this.loadingService.hide();
            this.toastCtrl.presentToast(
              "There is a problem adding data, please try again",
            );
            // this.navCtrl.setRoot("AppDashboardPage");
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
          }
        },
        (err) => {
          this.loadingService.hide();
          console.log(err);
        },
      );
    } else {
      this.storage.get("currentHCP").then((hcpid) => {
        this.data.userid = hcpid;
        this.data.appsecret = this.appsecret;
        console.log("data12345678", hcpid);
        this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
          (data) => {
            this.loadingService.show();
            let result = JSON.parse(data["_body"]);
            console.log(result.data);
            if (result.status == "success") {
              this.loadingService.hide();
              if (result.data !== null) {
                this.medicineList = result.data;
                if (this.selectMedicine) {
                  console.log("inside", this.selectMedicine);
                  var index = this.medicineList.findIndex((m) =>
                    m.name === this.selectMedicine
                  );

                  if (index != -1) {
                    console.log("inside3", this.selectMedicine);
                    this.medicineStep2form.controls["medicine"].setValue(
                      this.medicineList[index].id,
                    );
                  }
                }
              } else {
                this.loadingService.hide();
                this.data = [];
                this.medicineList = [];
              }
            } else {
              this.loadingService.hide();
              this.toastCtrl.presentToast(
                "There is a problem adding data, please try again",
              );
              // this.navCtrl.setRoot("AppDashboardPage");
              // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
          },
          (err) => {
            this.loadingService.hide();
            console.log(err);
          },
        );
      });
    }

    this.presAddedBy = this.userLoginData.id + "|" +
      this.userLoginData.firstname + "|" + this.userLoginData.email;

    if (!_.isEmpty(this.navParams.data)) {
      // console.log('navParams-Medicinemaster',navParams.data);
      this.patientData = this.navParams.data.patientData;
      this.medicineData = this.navParams.data.MedicineData;
      this.AddedMedicine = this.navParams.data.AddedMedicine;
      console.log("this.patientData", this.patientData);
    }

    /*this.data.enablePrescriptionNotification = false;
    this.data.prescriptionaddress = '';
    this.data.areaexpertise = '';
    this.data.doctorid = '';*/
    if (this.data.enablePrescriptionNotification == "1") {
      this.data.enablePrescriptionNotification = true;
    } else {
      this.data.enablePrescriptionNotification = false;
    }
    this.data.prescriptionaddress = this.data.prescriptionaddress == undefined
      ? ""
      : this.data.prescriptionaddress;
    this.data.areaexpertise = this.data.areaexpertise == undefined
      ? ""
      : this.data.areaexpertise;
    // this.data.doctorid = this.data.doctorid == undefined ? '' : this.data.doctorid;
    this.data.medicine = this.data.medicine == undefined
      ? ""
      : this.data.medicine;
  }
  closeModal() {
    this.viewCtrl.dismiss(false);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AppMedicinePrescriptionIssuedPage");
    this.getHCPid();
  }

  addPatientMedicinePrescriptionDetails(patientData, index) {
    console.log("timmm123", this.data.tim)
    console.log(index);
    console.log(patientData);
    this.submitAttempt = true;
    console.log("dataadadsd", this.data.tim)

    var dose = Array.prototype.map.call(this.data.tim, s => s.dose).toString();
    var time = Array.prototype.map.call(this.data.tim, s => s.time).toString();
    // if (dose) {
    // }
    this.data.dose = dose;
    this.data.time = time;
    // this.data.hcpid = this.hcpid;
    console.log("dataaddose", this.data.dose)
    console.log("dataadtime", this.data.time)
    let self = this;
    if (!this.medicineStep2form.valid) {
      console.log("Medicine Master Step 2 Validation Fire!");
    } else {
      for (let key in self.AddedMedicine) {
        self.data[key] = self.AddedMedicine[key];
      }
      if (this.userLoginData.usertype == "patient") {
        var id = this.userLoginData.id;
        this.data.doctor = this.userLoginData.id;
        console.log("id", id);
      }
      this.data.addedBy = this.userLoginData.id;
      this.data.createDate = this.getDate();
      this.data.isDeleted = 0;
      self.data.patientid = patientData ? patientData.userid : id;
      self.data.action = "addpatientmedicine";
      self.data.appsecret = this.appsecret;
      if (this.data.enablePrescriptionNotification == true) {
        this.data.enablePrescriptionNotification = "1";
      } else {
        this.data.enablePrescriptionNotification = "0";
      }
      if (this.userLoginData.usertype == "healthcareprovider") {
        this.data.hcpid = this.userLoginData.id;
      } else if (this.patientType == "independent") {
        this.data.hcpid = 0;
      } else {
        this.data.hcpid = this.hcpid;
      }
      self.http.post(this.baseurl + "adddata.php", self.data).subscribe(
        (data) => {
          let result = JSON.parse(data["_body"]);
          console.log(result);
          if (result.status == "success") {
            self.loadingService.hide();
            self.toastCtrl.presentToast("notification data added successfully");
            this.viewCtrl.dismiss(true);
          } else {
            self.loadingService.hide();
            self.toastCtrl.presentToast(
              "There is a problem adding data, please try again",
            );
            this.viewCtrl.dismiss(false);
          }
        },
        (err) => {
          console.log(err);
        },
      );
    }
  }
  getDate() {
    let date = new Date(this.date);
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" +
      date.getDate();
    // return date.getDate();
  }

  onChangeMedicene(e) {
    console.log(e);
  }

  async getHCPid() {
    await this.get("currentHCP").then((result) => {
      if (result != null) {
        this.hcpid = result;
        console.log("if", this.hcpid);
        this.getuserLoginData();
      } else {
        this.hcpid = 0;
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get("patientType").then((result) => {
      console.log("patienttypesdfdsfsdsdf", result);
      if (
        (!result || result == null || result == undefined) &&
        this.userLoginData == "patient"
      ) {
        this.doRefresh();
      } else {
        this.patientType = result;
        this.intializeApp1();
        console.log("sekeccdffvfdgdfgdfg", this.patientType);
      }
    });
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      } else {
        return null;
      }
    } catch (reason) {
      return null;
    }
  }

  doRefresh() {
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }
  onChangeDoctor(e) {
    this.selectdoctorid = e;
  }
}
