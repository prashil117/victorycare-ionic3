import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppMedicinePrescriptionIssuedPage } from './app-medicine-prescription-issued';

@NgModule({
  declarations: [
    AppMedicinePrescriptionIssuedPage,
  ],
  imports: [
    IonicPageModule.forChild(AppMedicinePrescriptionIssuedPage),
  ],
})
export class AppMedicinePrescriptionIssuedPageModule {}
