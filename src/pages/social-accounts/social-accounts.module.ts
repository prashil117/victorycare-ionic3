import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SocialAccountsPage } from './social-accounts';

@NgModule({
  declarations: [
    SocialAccountsPage,
  ],
  imports: [
    IonicPageModule.forChild(SocialAccountsPage),
  ],
})
export class SocialAccountsPageModule {}
