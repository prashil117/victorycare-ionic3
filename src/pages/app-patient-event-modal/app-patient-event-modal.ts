import { Component, AfterViewInit } from '@angular/core';
import {
  AlertController, ModalController, IonicPage, NavController, NavParams, Platform,
  ViewController
} from 'ionic-angular';
//import { ModalController } from '@ionic/angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from 'rxjs/Observable';
import * as _ from "lodash";
import { AppSharedService } from "../../services/app-shared-service";
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'app-patient-event-modal',
  templateUrl: 'app-patient-event-modal.html',
  providers: [DatabaseService]
})
export class AppPatientEventModalPage implements AfterViewInit {

  // colors: Array<string> = ['#d435a2', '#a834bf', '#6011cf', '#0d0e81', '#0237f1', '#0d8bcd', '#16aca4', '#3c887e', '#157145', '#57a773', '#88aa3d', '#b7990d', '#fcbf55', '#ff8668', '#ff5c6a', '#c2454c', '#c2183f', '#d8226b', '#8f2d56', '#482971', '#000000', '#561f37', '#433835', '#797979', '#819595'];
  colors: Array<string> = ['#E26060', '#8E24AA', '#6BB1D6', '#00897B', '#AFDB07', '#C0CA33', '#FAD000', '#FB8C00'];
  data: any = {};
  Pagetitle: string;
  // colors:any = [];
  events: any = [];
  patientData: any = [];
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  eventType: any = {};
  userLoginData: any;
  patientType: any;
  itemIndex: any = {};
  Eventmaster: any;
  date: any = new Date();
  eventList: any = [];
  patientSession: boolean;
  disable: boolean = false;
  style: string;
  tim: any = [];
  presAddedBy: string;
  submitAttempt: boolean;
  onboard: any;
  hcpid: any;
  sel: boolean = true;
  tog: boolean = false;
  EditFormGroup: FormGroup;
  //

  constructor(public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private databaseService: DatabaseService, private loadingService: LoadingService, public modalCtrl: ModalController, public viewCtrl: ViewController, public appSharedService: AppSharedService, public storage: Storage, public http: Http, private toastCtrl: ToastService) {

    this.EditFormGroup = formBuilder.group({
      eventname: ["", Validators.required],
      recurringDaily: [],
      enableStandardWheel: [],
      prestartdate: ["", Validators.required],
      preenddate: ["", Validators.required],
      areaexpertise: [],
      tog: [],
      selecttime: [],
      prescriptionaddress: [],
      tim: this.formBuilder.array([
        this.initDiagnosis(),
      ]),
    });



    if (this.navParams.data.eventdata) {
      this.data = this.navParams.data.eventdata
      this.data.color = this.data ? this.data.color : this.navParams.data.eventdata;
    }
    else {
      this.data.color = '#E26060';
    }
    if (this.navParams.data.event) {
      if (this.navParams.data.event.selecttime) {
        console.log("data")
        this.sel = false;
        this.tog = true;
      }
      else {
        this.sel = true;
        this.tog = false;
      }
    }
  }

  initDiagnosis() {
    return this.formBuilder.group({
      dose: [''],
      time: ['']
    });
  }

  addDiagnosis() {
    const control = <FormArray>this.EditFormGroup.controls['tim'];
    control.push(this.initDiagnosis());
    this.tim.push({});
    console.log("abvc", this.tim)
  }

  setInitialDiagnosis() {
    if (this.tim === undefined) {
      this.tim = [];
    }

    if (this.tim.length === 0) {
      this.tim.push({});
    }
  }

  deleteDiagnosis(index) {
    this.tim.splice(index, 1);
    console.log("this.tim", this.tim)
  }

  intializeApp1() {
    this.presAddedBy = this.userLoginData.id;

    this.onboard = this.navParams.data.onboard ? this.navParams.data.onboard : false;
    //this.data = navParams.data.eventdata ? navParams.data.eventdata : '';
    //this.newEventName = navParams.data.EventName ? navParams.data.EventName : '';

    if (this.userLoginData.usertype == 'patient' && this.patientType == 'independent') {
      this.disable = false;
    }
    else if (this.userLoginData.usertype == 'doctor' || this.userLoginData.usertype == 'nurse' || this.userLoginData.usertype == 'healthcareprovider') {
      this.disable = false;
    }
    else {
      this.disable = true;
    }

    if (!_.isEmpty(this.navParams.data)) {
      if (this.navParams.data.type === 'edit') {
        this.storage.get('patientSession').then((val) => {
          if (this.userLoginData.usertype == 'patient') {

            this.data.event = this.navParams.data.event ? this.navParams.data.event.eventmasterid : this.navParams.data.eventdata.eventmasterid;
            this.data.eventid = this.navParams.data.event ? this.navParams.data.event.eventmasterid : this.navParams.data.eventdata.eventmasterid;
            this.data.eventname = this.navParams.data.event ? this.navParams.data.event.name : this.navParams.data.eventdata.name;
            this.data.addtowheel = this.navParams.data.event ? this.navParams.data.event.addtowheel : this.navParams.data.eventdata.name;;
            this.data.notification = this.navParams.data.event ? this.navParams.data.event.notification : this.navParams.data.eventdata.name;;
            if (this.data.notification == 1) {
              this.data.notification = true;
            }
            else {
              this.data.addtowheel = false;
            }
            if (this.data.addtowheel == 1) {
              this.data.addtowheel = true;
            }
            else {
              this.data.addtowheel = false;
            }
            if (this.data.starttime) {
              var search = this.data.starttime;
              var n = search.search(",");
            }
            
            if (n != -1) {
              // var dosearr = this.data.dose ? this.data.dose.split(',') : this.data.dose;
              var timearr: any = this.data.starttime ? this.data.starttime.split(',') : this.data.starttime;
              // var tmp = {}
              console.log("timearr", timearr)
              for (var i in timearr) {
                console.log("abcd", timearr[i])
                this.tim.push({ time: timearr[i] });
                console.log("abcdsdfsdf", this.tim)
              }

            }
            else {
              var timearr = this.data.starttime;
              this.tim.push({ time: timearr });
              console.log("this.tim", this.tim)
            }
            console.log("abcdefghijkl", this.tim);
            if (this.tim.length > 0) {
              for (let i = 0; i < this.tim.length; i++) {
                const control = <FormArray>this.EditFormGroup.controls['tim'];
                control.push(this.initDiagnosis());
                console.log("this.tim4", this.tim)
              }
            }
            else {
              console.log("this.tim5", this.tim)
              this.tim = [];
            }

            // this.data.eventname = navParams.data.event.event.split('|')[1];
            // var event = this.data.eventname;
            // console.log(event);
            if (this.patientType == "dependent") {
              this.patientSession = true;
              console.log("daatdadadasdsdfsdfsdf55555555555")
            }
            if (this.patientType == "independent") {
              if (this.navParams.data.onboard == true) {
                console.log("daatdadadasdsdfsdfsdfh77777777777")
                this.patientSession = false;
              }
              else if (!this.navParams.data.onboard) {
                this.patientSession = true;
                console.log("daatdadadasdsdfsdfsdf123456")
              }
            }
            else if (this.userLoginData.usertype == 'patient' && this.patientType == "dependent") {
              this.patientSession = true;
              console.log("terjwekfdnms,fkds")
            }
            else {
              this.patientSession = true;
            }
          }
          else {
            if (this.patientData) {
              console.log("patienrdata", this.patientData)
              console.log("terjwekfdnms,fkds")
              this.patientSession = true;
            }
            else {
              this.patientSession = false;
            }
            this.Pagetitle = 'Edit Wellbeing status';
            this.eventType = this.navParams.data.type;
            this.data = this.navParams.data.event;

            console.log("name", this.data.name)
            if (this.userLoginData.usertype == 'patient' && this.patientType == 'independent') {
              this.patientSession = true;
            }

            // this.patientData = navParams.data.patientData ? navParams.data.patientData : '';
            // this.Eventmaster = navParams.data.Eventmaster;
            // this.data.color = '#E26060';
          }
        });
        this.storage.get('user').then((val) => {
          this.patientData = val ? val : '';
        });
        let self = this;
        var hcpWebServiceData: any = {};
        if (this.patientType == "independent" || this.userLoginData.usertype == "healthcareprovider") {
          hcpWebServiceData.userid = this.userLoginData.id;
          hcpWebServiceData.action = "geteventmasterdatafulllist";
          hcpWebServiceData.appsecret = this.appsecret;
          self.http.post(this.baseurl + "getdata.php", hcpWebServiceData).subscribe(data => {
            this.loadingService.show();
            let result = JSON.parse(data["_body"]);
            if (result.status == 'success') {
              this.loadingService.hide();
              this.eventList = result.data;
              console.log("this", this.eventList)
              // if (this.newEventName != "") {
              //   this.data.event = this.eventList.find(e => e.name === this.newEventName).id;
              //   console.log("newevent", this.data.event);
              //   console.log("newevent", this.newEventName);
              // }
            } else {
              this.loadingService.hide();
              this.toastCtrl.presentToast("Event dropdown is empty");
            }
          }, err => {
            this.loadingService.hide();
            console.log(err);
          });
        }
        if (this.userLoginData.usertype == "doctor" || this.userLoginData.usertype == "nurse") {
          this.loadingService.show();
          this.storage.get('currentHCP').then(hcpid => {
            hcpWebServiceData.userid = hcpid;
            hcpWebServiceData.action = "geteventmasterdatafulllist";
            hcpWebServiceData.appsecret = this.appsecret;
            self.http.post(this.baseurl + "getdata.php", hcpWebServiceData).subscribe(data => {

              let result = JSON.parse(data["_body"]);
              if (result.status == 'success') {
                this.loadingService.hide();
                this.eventList = result.data;
                console.log("this", this.eventList)
                // if (this.newEventName != "") {
                //   this.data.event = this.eventList.find(e => e.name === this.newEventName).id;
                //   console.log("newevent", this.data.event);
                //   console.log("newevent", this.newEventName);
                // }
              } else {
                this.loadingService.hide();
                this.toastCtrl.presentToast("Event dropdown is empty");
              }
            }, err => {
              this.loadingService.hide();
              console.log(err);
            });
          });
        }
        this.Pagetitle = 'Edit Wellbeing Status';
        this.data = this.navParams.data.event ? this.navParams.data.event : this.navParams.data.eventdata;
        this.eventType = this.navParams.data.type;
        this.data.event = this.data.eventmasterid;
        if (this.data.addtowheel == "1") {
          this.data.addtowheel = true;
        }
        else {
          this.data.addtowheel = false;
        }
        if (this.data.notification === "1") {
          this.data.notification = true;
        }
        else {
          this.data.notification = false;
        }
        if (this.data.addtostandardwheel == "1") {
          this.data.addtostandardwheel = true;
        }
        else {
          this.data.addtostandardwheel = false;
        }
        if (this.data.recurringDaily === "1") {
          this.data.recurringDaily = true;
        }
        else {
          this.data.recurringDaily = false;
        }
        this.itemIndex = this.navParams.data.index;
        this.patientData = this.navParams.data.patientData ? this.navParams.data.patientData : '';
        this.Eventmaster = "";
        if (this.data.color) {
          this.data.color = this.data ? this.data.color : this.navParams.data.eventdata;
        }
        else {
          this.data.color = '#E26060';
        }

      } else {
        this.Pagetitle = 'Add Wellbeing status';
        this.eventType = this.navParams.data.type;
        console.log('else');
        this.events = this.navParams.data.events;
        this.patientData = this.navParams.data.patientData ? this.navParams.data.patientData : '';
        this.Eventmaster = this.navParams.data.Eventmaster;
        this.data.color = '#E26060';
      }
    }
    else {
      this.data.color = '#E26060';
    }
    // this.colors = this.appSharedService.colors;
  }
  // for choose color and set in dropdown
  prepareColorSelector() {
    setTimeout(() => {
      let buttonElements = document.querySelectorAll('div.alert-radio-group button');
      if (!buttonElements.length) {
        this.prepareColorSelector();
      } else {
        for (let index = 0; index < buttonElements.length; index++) {
          let buttonElement = buttonElements[index];
          let optionLabelElement = buttonElement.querySelector('.alert-radio-label');
          let color = optionLabelElement.innerHTML.trim();


          if (this.isHexColor(color)) {
            buttonElement.classList.add('colorselect', 'color_' + color.slice(1, 7));
            if (color == this.data.color) {
              buttonElement.classList.add('colorselected');
            }
          }
        }
      }
    }, 100);
  }

  // for choose color and set in dropdown
  isHexColor(color) {
    let hexColorRegEx = /^#(?:[0-9a-fA-F]{3}){1,2}$/;
    return hexColorRegEx.test(color);
  }

  // for choose color and set in dropdown
  selectColor(color) {
    let buttonElements = document.querySelectorAll('div.alert-radio-group button.colorselect');
    for (let index = 0; index < buttonElements.length; index++) {
      let buttonElement = buttonElements[index];
      buttonElement.classList.remove('colorselected');
      if (buttonElement.classList.contains('color_' + color.slice(1, 7))) {
        buttonElement.classList.add('colorselected');
      }
    }
  }

  // for choose color and set in dropdown
  setColor(color) {
    console.log('Selected Color is', color);

  }

  closeModal() {
    this.viewCtrl.dismiss(false);
  }

  saveEvent() {
    // console.log(this.events);
    // console.log(this.data);
    let self = this;
    if (this.data.addtowheel == true) {
      this.data.addtowheel = "1";
    }
    else {
      this.data.addtowheel = "0";
    }
    if (this.data.notification == true) {
      this.data.notification = "1";
    }
    else {
      this.data.notification = "0";
    }
    if (!_.isUndefined(this.data.name) && !_.isUndefined(this.data.color) && !_.isUndefined(this.data.description)) {
      self.data.createDate = this.getDate();
      self.data.hcpid = this.userLoginData.id;
      // self.events.push(self.data);
      console.log(self.data);
    } else {
      this.presentAlert('Please enter wellbeing name or choose color');
    }

  }

  getDate() {
    let date = new Date(this.date);
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    // return date.getDate();
  }

  onChange(e) {
    if (e.value) {
      this.sel = false;
    } else {
      this.sel = true;
    }
  }

  updateEvent(patientData, index) {
    if (this.data.addtowheel == true) {
      this.data.addtowheel = "1";
    }
    else {
      this.data.addtowheel = "0";
    }
    if (this.data.notification == true) {
      this.data.notification = "1";
    }
    else {
      this.data.notification = "0";
    }
    if (this.userLoginData.usertype == "healthcareprovider") {
      this.data.userid = this.userLoginData.id;
    }
    else {
      this.data.userid = this.hcpid;
    }
    let self = this;

    console.log("data", this.data)
    if (!_.isUndefined(this.data.name) && !_.isUndefined(this.data.description)) {
      self.data.action = "updateevent";
      self.data.appsecret = this.appsecret;
      console.log(self.data);
      if (!this.tog) {
        this.data.selecttime = null;
      }
      self.http.post(this.baseurl + "updatedata.php", self.data).subscribe(data => {

        console.log(data);
        let result = JSON.parse(data["_body"]);
        console.log(result.status);
        if (result.status == 'success') {
          self.toastCtrl.presentToast("Wellbeing is updated successfully");
          if (this.patientType == "independent")
            this.viewCtrl.dismiss(true);
          else
            self.navCtrl.setRoot("AppEventMasterListPage");
        } else {
          self.toastCtrl.presentToast("There is an error saving data!");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
        }
      }, err => {
        console.log(err);
      });
    } else {
      this.presentAlert('Please enter all required details');
    }
  }
  SaveEvent() {
    this.data.userid = this.userLoginData.id;
    let self = this;
    if (this.data.addtowheel == true) {
      this.data.addtowheel = "1";
    }
    else {
      this.data.addtowheel = "0";
    }
    if (this.data.notification == true) {
      this.data.notification = "1";
    }
    else {
      this.data.notification = "0";
    }
    if (!this.tog) {
      this.data.selecttime = null;
    }
    if (!_.isUndefined(this.data.name) && !_.isUndefined(this.data.description)) {
      self.data.action = "addevent";
      self.data.appsecret = this.appsecret;

      console.log(self.data);
      self.http.post(this.baseurl + "adddata.php", self.data).subscribe(data => {
        console.log(data);
        let result = JSON.parse(data["_body"]);
        console.log(result.status);
        if (result.status == 'success') {
          self.toastCtrl.presentToast("Wellbeing is save successfully");
          //this.modalCtrl.dismiss();
          this.viewCtrl.dismiss(true);
        } else {
          self.toastCtrl.presentToast("There is an error saving data!");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});

        }
      }, err => {
        console.log(err);
      });
    } else {
      this.presentAlert('Please enter all required details');
    }
  }


  updatePatientEvent(patientData, userid) {
    console.log("dsfdfssf", this.data.event);
    console.log("dsfdfssf", this.data);
    if (this.data.addtostandardwheel == true) {
      this.data.addtostandardwheel = "1";
    }
    else {
      this.data.addtostandardwheel = "0";
    }
    if (this.data.recurringDaily == true) {
      this.data.recurringDaily = "1";
    }
    else {
      this.data.recurringDaily = "0";
    }
    if (this.userLoginData.usertype == "patient") {
      var uid = this.userLoginData.id;
    }

    this.submitAttempt = true;
    let self = this;
    console.log("this.datasdsds", this.data.event);
    var time = Array.prototype.map.call(this.tim, s => s.time).toString();
    this.data.starttime = time;
    self.data.addedBy = this.presAddedBy;
    this.data.eventid = this.data ? this.data.event : this.data.eventid;
    this.data.eventmasterid = this.data ? this.data.event : this.data.eventid;
    self.data.uid = userid ? userid : uid;
    console.log("this.data", this.data);
    self.data.appsecret = this.appsecret;
    self.data.action = "updatepatientsevent";
    self.http.post(this.baseurl + "updatedata.php", self.data).subscribe(data => {
      console.log(data);
      let result = JSON.parse(data["_body"]);
      console.log(result.status);
      if (result.status == 'success') {
        self.toastCtrl.presentToast("Wellbeing is updated successfully");
        this.viewCtrl.dismiss(true);
      } else {
        self.toastCtrl.presentToast("There is an error saving data!");
        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
      }
    }, err => {
      console.log(err);
    });
  }

  onChangeAddnewevent(e) {
    console.log("event", e)
    // if (e === "other") {
    //   delete this.data.event;
    //   this.navCtrl.push("AppPatientsEventMasterModalPage", { 'patientaction': 'edit', 'eventdata': this.data });
    //   console.log("data", this.data)
    //   this.data.event = this.eventList.find(e => e.id === this.data.eventid).id;
    // }

  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  addTime() {
    console.log(this.data.date);
    // var newdate = this.data.date;
    this.data.date.push(this.data.date);
  }

  deleteTime(index) {
    this.data.date.splice(index, 1);
  }

  setInitialTime() {
    if (this.data.date === undefined) {
      this.data.date = [];
    }

    if (this.data.date.length === 0) {
      this.data.date.push({});
    }
  }
  ngAfterViewInit(): void {
    this.setInitialTime();
    this.getHCPid();
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }
  async getHCPid() {

    await this.get('currentHCP').then(result => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData()
      } else {
        this.getuserLoginData()

      }
    });
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        this.hcpid = this.hcpid ? this.hcpid : this.userLoginData.id
        this.getpatientType();
      }
    });
  }
  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh('');
      }
      else {
        this.patientType = result;
        this.intializeApp1();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }
}
