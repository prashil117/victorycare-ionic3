import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {AppPatientEventModalPage} from "./app-patient-event-modal";


@NgModule({
  declarations: [
      AppPatientEventModalPage
  ],
  imports: [
    IonicPageModule.forChild(AppPatientEventModalPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppPatientEventModalModule {}
