import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppSecurityAndAccessSettingsPage } from './app-security-and-access-settings';
import { IonicSelectableModule } from 'ionic-selectable';
@NgModule({
  declarations: [
    AppSecurityAndAccessSettingsPage,
  ],
  imports: [
    IonicSelectableModule,
    IonicPageModule.forChild(AppSecurityAndAccessSettingsPage),
  ],
})
export class AppSecurityAndAccessSettingsPageModule { }
