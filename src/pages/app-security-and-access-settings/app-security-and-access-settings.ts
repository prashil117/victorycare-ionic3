import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Http } from "@angular/http";
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
import { LoadingService } from "../../services/loading-service";
import { Storage } from "@ionic/storage";

@IonicPage()
@Component({
  selector: 'page-app-security-and-access-settings',
  templateUrl: 'app-security-and-access-settings.html',
})
export class AppSecurityAndAccessSettingsPage {

  IsonlyMe: any;
  Iseverybody: any;
  name1 = "Everybody";
  name2 = "Only me";
  users = [];
  userLoginData: any;
  selected_data: any;
  hcpid: any;
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  msg: any;

  constructor(public loadingService: LoadingService, private storage: Storage, public navCtrl: NavController, public navParams: NavParams, public http: Http, public toastctrl: ToastService, public loadingctrl: LoadingService, public alertctrl: AlertController) {
    // this.users = [
    //   { id: 1, name: 'Tokai' },
    //   { id: 2, name: 'Vladivostok' },
    //   { id: 3, name: 'Navlakhi' }
    // ];
  }

  ionViewDidLoad() {
    this.getHCPid();
  }
  iseverybody(value) {
    if (value) {
      this.IsonlyMe = 0;
      this.updateonlyme(this.IsonlyMe);
    }
  }
  isOnlyme(value) {
    if (value) {
      this.Iseverybody = 0;
      this.updateonlyme(this.IsonlyMe);
    }

  }

  updateonlyme(value) {
    var data = {
      action: "onlyme",
      userid: this.userLoginData.id,
      appsecret: this.appsecret,
      onlyme: value
    }
    console.log(value)
    this.userLoginData.only_me = value;
    this.storage.set('userLogin', this.userLoginData);
    this.http.post(this.baseurl + 'login.php', data).subscribe(res => {
      let result = JSON.parse(res['_body'])
      console.log("result", result)
    }, err =>
      console.log("error", err))
  }


  onSearch(value) {
    var data = {
      keyword: value.text,
      appsecret: this.appsecret,
      userid: this.userLoginData.id,
      action: 'getuserlist'
    }
    if (value.text) {
      this.http.post(this.baseurl + 'getuser.php', data).subscribe(res => {
        console.log("result", res)
        let result = JSON.parse(res['_body']);
        console.log("result", result)
        if (result.status == "success")
          this.users = result.data;
        else
          this.users = [];
      }, err => {
        console.log("error", err)
      })
    } else {
      this.msg = "No user found";
      this.users = [];
    }
  }
  alert(value) {
    if (value == "success")
      var val = 'Invitation sent successfully';
    else
      var val = "invitation already sent";
    let alert = this.alertctrl.create({
      message: val,
      buttons: ['ok']
    })
    alert.present();
  }

  onSubmit() {
    this.loadingService.show();
    console.log("this.data", this.selected_data)
    var data = {
      appsecret: this.appsecret,
      userid: this.userLoginData.id,
      action: 'addinvite',
      invited_id: this.selected_data.id,
      email: this.selected_data.email
    }
    this.http.post(this.baseurl + 'accesssecurity.php', data).subscribe(res => {
      this.loadingService.hide();
      let result = JSON.parse(res['_body']);
      if (result.status == "success") {
        this.alert('success');
      }
      else if (result.status == "exist") {
        this.alert('exist');
      }
      else {
        console.log("something went wrong");
      }
    }, err => {
      this.loadingService.hide();
      console.log("error", err);
    })
  }

  Intialize() {
    console.log("ta123d",this.userLoginData.only_me )
    if (this.userLoginData.only_me == 1 || this.userLoginData.only_me) {
      
      this.IsonlyMe = true;
      console.log("tad",this.userLoginData.only_me )
    }
    else {
      this.Iseverybody = true;
      console.log("tad123",this.userLoginData.only_me )
    }
  }

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log("set string in storage: " + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      } else {
        return null;
      }
    } catch (reason) {
      return null;
    }
  }
  async getHCPid() {
    await this.get("currentHCP").then((result) => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      } else {
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.Intialize();
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
        }
      }
    });
  }
}
