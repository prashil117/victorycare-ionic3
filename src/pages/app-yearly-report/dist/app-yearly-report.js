"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.AppYearlyReportPage = void 0;
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var chart_js_1 = require("chart.js");
var environment_1 = require("../../environment/environment");
var moment = require("moment");
var option = {
    point: {
        radius: 25,
        hoverRadius: 35,
        pointStyle: 'rectRounded'
    },
    cornerRadius: 30,
    legend: {
        display: false
    },
    scales: {
        xAxes: [{
                gridLines: {
                    display: false
                }
            }],
        yAxes: [{
                display: false,
                gridLines: {
                    display: false,
                    drawBorder: false
                }
            }]
    }
};
var dougnhutOption = {
    responsive: true,
    aspectRatio: 1,
    maintainAspectRatio: true,
    legend: {
        display: true,
        position: 'bottom',
        labels: {
            usePointStyle: true
        }
    }
};
var AppYearlyReportPage = /** @class */ (function () {
    function AppYearlyReportPage(events, toastService, loadingService, http, navCtrl, navParams, storage) {
        this.events = events;
        this.toastService = toastService;
        this.loadingService = loadingService;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.baseurl = environment_1.environment.apiUrl;
        this.appsecret = environment_1.environment.appsecret;
        this.TotalSleep = 0;
        this.TotalDistress = 0;
        this.TotalComfort = 0;
        this.TotalUncomfort = 0;
        this.startOfWeek = moment(moment().startOf('week').format('YYYY/MM/DD'));
        this.AsleepBarChartArray = [];
        this.AwakeBarChartArray = [];
        this.PainBarChartArray = [];
        this.UncomfortBarChartArray = [];
        this.AwakeArray = [];
        this.AsleepArray = [];
        this.PainArray = [];
        this.UncomfortArray = [];
        this.selectedDate = new Date();
        this.weeklyHour = 8760;
    }
    AppYearlyReportPage.prototype.ionViewDidLoad = function () {
        this.getuserLoginData();
        var weeknumber = moment().week();
        console.log("week", weeknumber);
    };
    AppYearlyReportPage.prototype.getDates1 = function () {
        var _this = this;
        this.events.subscribe('app:sendUser', function (logged) {
            _this.selectedDate = logged;
            _this.appIntialize();
        });
        this.events.publish('app:getUser');
    };
    AppYearlyReportPage.prototype.ionViewWillEnter = function () {
        this.getDates1();
    };
    AppYearlyReportPage.prototype.ionViewWillLeave = function () {
        this.events.unsubscribe('app:sendUser');
    };
    AppYearlyReportPage.prototype.SleepDoughnut = function (shour) {
        var data = [this.weeklyHour, shour];
        console.log("dta", data);
        this.chart = new chart_js_1.Chart(this.chartRef.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Total', 'Consume'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(83,155,192)',
                            'rgb(107,177,214)'
                        ]
                    }]
            },
            options: dougnhutOption
        });
    };
    AppYearlyReportPage.prototype.DistressDoughnut = function (Dhour) {
        var data = [this.weeklyHour, Dhour];
        this.chart = new chart_js_1.Chart(this.chartRef1.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Total', 'Consume'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(226,96,96)',
                            'rgb(171,87,87)'
                        ]
                    }]
            },
            options: dougnhutOption
        });
    };
    AppYearlyReportPage.prototype.UnomfortableDoughnut = function (Uhour) {
        var data = [this.weeklyHour, Uhour];
        this.chart = new chart_js_1.Chart(this.chartRef2.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Total', 'Consume'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(250,208,0)',
                            'rgb(201,169,13)'
                        ]
                    }]
            },
            options: dougnhutOption
        });
    };
    AppYearlyReportPage.prototype.ComfortableDoughnut = function (Chour) {
        var data = [this.weeklyHour, Chour];
        this.chart = new chart_js_1.Chart(this.chartRef3.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Total', 'Consume'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(175,219,7)',
                            'rgb(151,187,12)'
                        ]
                    }]
            },
            options: dougnhutOption
        });
    };
    AppYearlyReportPage.prototype.SleepChart = function () {
        this.barchart = new chart_js_1.Chart(this.barchartRef.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                datasets: [{
                        barPercentage: 0.5,
                        pointBorderColor: 'red',
                        data: this.AsleepBarChartArray,
                        backgroundColor: [
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                        ],
                        borderColor: [
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                        ],
                        borderWidth: 1
                    }, {
                        data: this.AsleepBarChartArray,
                        backgroundColor: [
                            'rgb(107,177,214)',
                        ],
                        borderColor: [
                            'rgb(107,177,214)',
                        ],
                        type: 'line',
                        fill: false
                    }
                ]
            },
            options: option
        });
    };
    AppYearlyReportPage.prototype.DistressChart = function () {
        this.barchart = new chart_js_1.Chart(this.barchartRef1.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                datasets: [{
                        barPercentage: 0.5,
                        data: this.PainBarChartArray,
                        backgroundColor: [
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                        ],
                        borderColor: [
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                        ],
                        borderWidth: 1
                    }, {
                        data: this.PainBarChartArray,
                        backgroundColor: [
                            'rgb(171,87,87)',
                        ],
                        borderColor: [
                            'rgb(171,87,87)',
                        ],
                        type: 'line',
                        fill: false
                    }]
            },
            options: option
        });
    };
    AppYearlyReportPage.prototype.ComfortableChart = function () {
        console.log("herfs", this.AwakeBarChartArray);
        this.barchart = new chart_js_1.Chart(this.barchartRef3.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                datasets: [{
                        barPercentage: 0.5,
                        data: this.AwakeBarChartArray,
                        backgroundColor: [
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                        ],
                        borderColor: [
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                        ],
                        borderWidth: 1
                    },
                    {
                        data: this.AwakeBarChartArray,
                        backgroundColor: [
                            'rgb(201,169,13)',
                        ],
                        borderColor: [
                            'rgb(201,169,13)',
                        ],
                        type: 'line',
                        fill: false
                    }]
            },
            options: option
        });
    };
    AppYearlyReportPage.prototype.UncomfortableChart = function () {
        this.barchart = new chart_js_1.Chart(this.barchartRef2.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                datasets: [{
                        barPercentage: 0.5,
                        data: this.UncomfortBarChartArray,
                        backgroundColor: [
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                        ],
                        borderColor: [
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                        ],
                        borderWidth: 1
                    },
                    {
                        data: this.UncomfortBarChartArray,
                        backgroundColor: [
                            'rgb(175,219,7)',
                        ],
                        borderColor: [
                            'rgb(175,219,7)',
                        ],
                        type: 'line',
                        fill: false
                    }
                ]
            },
            options: option
        });
    };
    AppYearlyReportPage.prototype.appIntialize = function () {
        var _this = this;
        var year = moment(this.selectedDate).format('YYYY');
        // this.getDates(startOfWeek, endOfWeek)
        this.loadingService.show();
        var data = {
            userid: this.userLoginData.id,
            appsecret: this.appsecret,
            currentYear: year,
            action: 'getyearlygraphdata'
        };
        this.http.post(this.baseurl + 'getgraphs.php', data).subscribe(function (data) {
            _this.UncomfortArray = [];
            _this.UncomfortBarChartArray = [];
            _this.AsleepArray = [];
            _this.AsleepBarChartArray = [];
            _this.PainArray = [];
            _this.PainBarChartArray = [];
            _this.AwakeArray = [];
            _this.AwakeBarChartArray = [];
            _this.TotalSleep = 0;
            // this.weeklyHour=0;
            _this.TotalDistress = 0;
            _this.TotalComfort = 0;
            _this.dateArray = [];
            _this.TotalUncomfort = 0;
            _this.SleepHours = "0:0";
            _this.DistressHours = "0:0";
            _this.ComfortHours = "0:0";
            _this.UncomfortHours = "0:0";
            _this.loadingService.hide();
            var data1 = JSON.parse(data['_body']);
            console.log("data", data1);
            var chartData = data1.graphSumData;
            if (data1.status == "success") {
                if (chartData && chartData.length > 0) {
                    _this.getDiffer(chartData);
                    console.log("chrser", chartData);
                    var Aindex = 0;
                    var Pindex = 0;
                    var Uindex = 0;
                    var Sindex = 0;
                    for (var i = 1; i <= 12; i++) {
                        if (_this.UncomfortArray[Uindex] !== undefined) {
                            console.log("dsf", _this.UncomfortArray[Uindex]);
                            if (i == parseInt(_this.UncomfortArray[Uindex].month_no)) {
                                var sec = (_this.UncomfortArray[Uindex].endtimeseconds - _this.UncomfortArray[Uindex].starttimeseconds) * 1000;
                                var tmpvalue = Math.round(moment.duration(sec).asHours());
                                _this.UncomfortBarChartArray.push(tmpvalue);
                                Uindex = Uindex + 1;
                            }
                            else {
                                _this.UncomfortBarChartArray.push(0);
                            }
                        }
                        else {
                            console.log("hello123456", _this.UncomfortArray);
                            _this.UncomfortBarChartArray.push(0);
                        }
                        if (_this.AsleepArray[Sindex] !== undefined) {
                            if (i == parseInt(_this.AsleepArray[Sindex].month_no)) {
                                var sec = (_this.AsleepArray[Sindex].endtimeseconds - _this.AsleepArray[Sindex].starttimeseconds) * 1000;
                                var tmpvalue = Math.round(moment.duration(sec).asHours());
                                _this.AsleepBarChartArray.push(tmpvalue);
                                Sindex = Sindex + 1;
                            }
                            else {
                                _this.AsleepBarChartArray.push(0);
                            }
                        }
                        else {
                            _this.AsleepBarChartArray.push(0);
                        }
                        if (_this.PainArray[Pindex] !== undefined) {
                            if (i == parseInt(_this.PainArray[Pindex].month_no)) {
                                var sec = (_this.PainArray[Pindex].endtimeseconds - _this.PainArray[Pindex].starttimeseconds) * 1000;
                                var tmpvalue = Math.round(moment.duration(sec).asHours());
                                _this.PainBarChartArray.push(tmpvalue);
                                Pindex = Pindex + 1;
                            }
                            else {
                                _this.PainBarChartArray.push(0);
                            }
                        }
                        else {
                            _this.PainBarChartArray.push(0);
                        }
                        if (_this.AwakeArray[Aindex] !== undefined) {
                            if (i == parseInt(_this.AwakeArray[Aindex].month_no)) {
                                var sec = (_this.AwakeArray[Aindex].endtimeseconds - _this.AwakeArray[Aindex].starttimeseconds) * 1000;
                                var tmpvalue = Math.round(moment.duration(sec).asHours());
                                _this.AwakeBarChartArray.push(tmpvalue);
                                Aindex = Aindex + 1;
                            }
                            else {
                                _this.AwakeBarChartArray.push(0);
                            }
                        }
                        else {
                            _this.AwakeBarChartArray.push(0);
                        }
                    }
                }
                else {
                    _this.AwakeBarChartArray = [0, 0, 0, 0];
                    _this.PainBarChartArray = [0, 0, 0, 0];
                    _this.AsleepBarChartArray = [0, 0, 0, 0];
                    _this.UncomfortBarChartArray = [0, 0, 0, 0];
                }
                console.log("hello123456", _this.UncomfortArray);
                _this.SleepChart();
                _this.DistressChart();
                _this.ComfortableChart();
                _this.UncomfortableChart();
                _this.DistressDoughnut(parseInt(_this.DistressHours.split(":")[0]));
                _this.SleepDoughnut(parseInt(_this.SleepHours.split(":")[0]));
                _this.ComfortableDoughnut(parseInt(_this.ComfortHours.split(":")[0]));
                _this.UnomfortableDoughnut(parseInt(_this.UncomfortHours.split(":")[0]));
            }
        }, function (err) {
            _this.loadingService.hide();
        });
    };
    AppYearlyReportPage.prototype.getDiffer = function (chartData) {
        var DistressArray = [];
        var SleepArray = [];
        var UncomfortArray = [];
        var ComfortArray = [];
        for (var i in chartData) {
            if (chartData[i].name == "Awake") {
                this.AwakeArray.push(chartData[i]);
                this.TotalComfort += parseInt(chartData[i].count_name);
                console.log("thisd", this.TotalComfort);
                console.log("thisd", parseInt(chartData[i].count_name));
                var startTime = moment(chartData[i].starttime).format("HH:mm");
                var endTime = moment(chartData[i].endtime).format("HH:mm");
                var diff = this.diff(startTime, endTime);
                ComfortArray.push(diff);
            }
            if (chartData[i].name == "Asleep") {
                this.AsleepArray.push(chartData[i]);
                this.TotalSleep += parseInt(chartData[i].count_name);
                var startTime = moment(chartData[i].starttime).format("HH:mm");
                var endTime = moment(chartData[i].endtime).format("HH:mm");
                var diff = this.diff(startTime, endTime);
                SleepArray.push(diff);
            }
            if (chartData[i].name == "Pain") {
                this.PainArray.push(chartData[i]);
                this.TotalDistress += parseInt(chartData[i].count_name);
                var startTime = moment(chartData[i].starttime).format("HH:mm");
                var endTime = moment(chartData[i].endtime).format("HH:mm");
                var diff = this.diff(startTime, endTime);
                DistressArray.push(diff);
            }
            if (chartData[i].name == "Uncomfortable") {
                this.UncomfortArray.push(chartData[i]);
                this.TotalUncomfort += parseInt(chartData[i].count_name);
                var startTime = moment(chartData[i].starttime).format("HH:mm");
                var endTime = moment(chartData[i].endtime).format("HH:mm");
                var diff = this.diff(startTime, endTime);
                UncomfortArray.push(diff);
            }
        }
        ;
        this.DistressHours = this.calulateTotalTime(DistressArray);
        console.log("this.sds", this.DistressHours);
        this.DistressDoughnut(parseInt(this.DistressHours.split(":")[0]));
        this.SleepHours = this.calulateTotalTime(SleepArray);
        this.SleepDoughnut(parseInt(this.SleepHours.split(":")[0]));
        this.ComfortHours = this.calulateTotalTime(ComfortArray);
        this.ComfortableDoughnut(parseInt(this.ComfortHours.split(":")[0]));
        this.UncomfortHours = this.calulateTotalTime(UncomfortArray);
        this.UnomfortableDoughnut(parseInt(this.UncomfortHours.split(":")[0]));
    };
    AppYearlyReportPage.prototype.diff = function (start, end) {
        start = start.split(":");
        end = end.split(":");
        var startDate = new Date(0, 0, 0, start[0], start[1], 0);
        var endDate = new Date(0, 0, 0, end[0], end[1], 0);
        var diff = endDate.getTime() - startDate.getTime();
        var hours = Math.floor(diff / 1000 / 60 / 60);
        diff -= hours * 1000 * 60 * 60;
        var minutes = Math.floor(diff / 1000 / 60);
        // If using time pickers with 24 hours format, add the below line get exact hours
        if (hours < 0)
            hours = hours + 24;
        return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
    };
    AppYearlyReportPage.prototype.hours = function (hh) {
        if (hh !== undefined)
            return hh.split(":")[0];
    };
    AppYearlyReportPage.prototype.minutes = function (mm) {
        if (mm !== undefined)
            return mm.split(":")[1];
    };
    AppYearlyReportPage.prototype.avg = function (hh) {
        if (hh !== undefined)
            return Math.ceil(hh.split(":")[0] / 12);
    };
    AppYearlyReportPage.prototype.calulateTotalTime = function (timeArray) {
        var sum = timeArray.reduce(function (acc, time) { return acc.add(moment.duration(time)); }, moment.duration());
        return [Math.floor(sum.asHours()), sum.minutes()].join(':');
    };
    AppYearlyReportPage.prototype.set = function (key, value) {
        return __awaiter(this, void 0, Promise, function () {
            var result, reason_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.set(key, value)];
                    case 1:
                        result = _a.sent();
                        // console.log("set string in storage: " + result);
                        return [2 /*return*/, true];
                    case 2:
                        reason_1 = _a.sent();
                        // console.log(reason);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppYearlyReportPage.prototype.get = function (key) {
        return __awaiter(this, void 0, Promise, function () {
            var result, reason_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.storage.get(key)];
                    case 1:
                        result = _a.sent();
                        if (result != null) {
                            return [2 /*return*/, result];
                        }
                        else {
                            return [2 /*return*/, null];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        reason_2 = _a.sent();
                        return [2 /*return*/, null];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppYearlyReportPage.prototype.getpatientType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("patientType").then(function (result) {
                            if (result == "independent") {
                                _this.dependent = false;
                            }
                            _this.appIntialize();
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppYearlyReportPage.prototype.getuserLoginData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("userLogin").then(function (result) {
                            if (result != null) {
                                _this.userLoginData = result;
                                _this.getpatientType();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        core_1.ViewChild('lineChart')
    ], AppYearlyReportPage.prototype, "chartRef");
    __decorate([
        core_1.ViewChild('barChart')
    ], AppYearlyReportPage.prototype, "barchartRef");
    __decorate([
        core_1.ViewChild('lineChart1')
    ], AppYearlyReportPage.prototype, "chartRef1");
    __decorate([
        core_1.ViewChild('barChart1')
    ], AppYearlyReportPage.prototype, "barchartRef1");
    __decorate([
        core_1.ViewChild('lineChart2')
    ], AppYearlyReportPage.prototype, "chartRef2");
    __decorate([
        core_1.ViewChild('barChart2')
    ], AppYearlyReportPage.prototype, "barchartRef2");
    __decorate([
        core_1.ViewChild('lineChart3')
    ], AppYearlyReportPage.prototype, "chartRef3");
    __decorate([
        core_1.ViewChild('barChart3')
    ], AppYearlyReportPage.prototype, "barchartRef3");
    AppYearlyReportPage = __decorate([
        ionic_angular_1.IonicPage(),
        core_1.Component({
            selector: 'page-app-yearly-report',
            templateUrl: 'app-yearly-report.html'
        })
    ], AppYearlyReportPage);
    return AppYearlyReportPage;
}());
exports.AppYearlyReportPage = AppYearlyReportPage;
