import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppYearlyReportPage } from './app-yearly-report';

@NgModule({
  declarations: [
    AppYearlyReportPage,
  ],
  imports: [
    IonicPageModule.forChild(AppYearlyReportPage),
  ],
})
export class AppYearlyReportPageModule {}
