import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppMedicineViewallNotificationPage } from './app-medicine-viewall-notification';

@NgModule({
  declarations: [
    AppMedicineViewallNotificationPage,
  ],
  imports: [
    IonicPageModule.forChild(AppMedicineViewallNotificationPage),
  ],
})
export class AppMedicineViewallNotificationPageModule {}
