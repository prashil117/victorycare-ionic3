import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppTipPage } from './app-tip';

@NgModule({
  declarations: [
    AppTipPage,
  ],
  imports: [
    IonicPageModule.forChild(AppTipPage),
  ],
})
export class AppTipPageModule {}
