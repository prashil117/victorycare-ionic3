import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppQuaterlyReportPage } from './app-quaterly-report';

@NgModule({
  declarations: [
    AppQuaterlyReportPage,
  ],
  imports: [
    IonicPageModule.forChild(AppQuaterlyReportPage),
  ],
})
export class AppQuaterlyReportPageModule {}
