import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppHowDoesWorkPage } from './app-how-does-work';

@NgModule({
  declarations: [
    AppHowDoesWorkPage,
  ],
  imports: [
    IonicPageModule.forChild(AppHowDoesWorkPage),
  ],
})
export class AppHowDoesWorkPageModule {}
