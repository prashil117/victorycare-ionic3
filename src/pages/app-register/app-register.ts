import { Component, ElementRef, ViewChild } from '@angular/core';
import { AlertController, IonicPage, ModalController, NavController, NavParams, Slides, ModalOptions, Platform } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { EmailValidator } from '../../app/directive/email-validator';
import { DatabaseService } from "../../services/database-service";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AppModalPage } from "../app-modal/app-modal";
import { ToastService } from "../../services/toast-service";
import { LoadingService } from "../../services/loading-service";
import { Http } from '@angular/http';
import { environment } from "../../environment/environment";
import { FCM } from '@ionic-native/fcm';
import { AppSettingsTermsAndConditionsPage } from "../app-settings-terms-and-conditions/app-settings-terms-and-conditions";
import { Storage } from "@ionic/storage";

@IonicPage()
@Component({
    selector: 'app-register',
    templateUrl: 'app-register.html',
    providers: [Camera, DatabaseService]

})
export class AppRegisterPage {
    termscondition: any = false;
    Registerform: FormGroup;
    errmsg: any;
    submitAttempt: boolean;
    data: any = {};
    usertypeMsg: string;
    nameMsg: string;
    firstnameMsg: string;
    lastnameMsg: string;
    baseurl = environment.apiUrl;
    appsecret = environment.appsecret;
    emailMsg: string;
    passwordMsg: string;
    confirmPasswordMsg: string;
    errorMessage: string;
    Registerdata: any = {};
    passwordType: string = 'password';
    passwordType1: string = 'password';
    passwordIcon: string = 'eye-off';
    passwordIcon1: string = 'eye-off';
    pushes: any = [];
    isEnabled: any = false;


    constructor(public fcm: FCM, public platform: Platform, public storage: Storage, public navCtrl: NavController, navParams: NavParams, public modalCtrl: ModalController, public formBuilder: FormBuilder, private alertCtrl: AlertController, private databaseService: DatabaseService, private toastCtrl: ToastService, private loadingService: LoadingService, public http: Http) {
        this.Registerform = formBuilder.group({
            // usertype: ['', Validators.required],
            // name: ['', Validators.required],
            firstname: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
            lastname: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
            email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required],
            termscondition: [''],
        }, { validator: this.matchingPasswords('password', 'confirmPassword') });
        this.initPushNotification();
        console.log('isCheck', this.termscondition);
    }
    initPushNotification() {
        this.platform.ready().then(() => {

            if (this.platform.is('corodova') || this.platform.is('ios')) {
                this.fcm.getToken().then(token => {
                    this.data.fcmtoken = token;
                    console.log("new token", token);
                    console.log(token);   
                })

                this.fcm.onTokenRefresh().subscribe(token => {
                    //register token
                    this.data.fcmtoken = token;
                    console.log("tokenrefresh", token);
                })
            }
        })
    }



    public OpenTermsCondition() {
        console.log("hello");
        let modal = this.modalCtrl.create(AppSettingsTermsAndConditionsPage, null, {
            // cssClass: "my-modal",
        });
        modal.present();
    }

    matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
        return (group: FormGroup): { [key: string]: any } => {
            let password = group.controls[passwordKey];
            let confirmPassword = group.controls[confirmPasswordKey];

            if (password.value !== confirmPassword.value) {
                return {
                    confirmPassword: true
                };
            }
        }
    }
    hideShowPassword(item) {
        if (item == "pass1") {
            this.passwordType1 = this.passwordType1 === 'text' ? 'password' : 'text';
            this.passwordIcon1 = this.passwordIcon1
                === 'eye-off' ? 'eye' : 'eye-off';
        }
        else {
            this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
            this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
        }

    }

    isInputValid() {
        this.usertypeMsg = "";
        this.nameMsg = "";
        this.firstnameMsg = "";
        this.lastnameMsg = "";
        this.emailMsg = "";
        this.passwordMsg = "";
        this.confirmPasswordMsg = "";

        if (this.data.usertype === undefined) {
            this.nameMsg = "Usertype cannot be empty";
            return false;
        }

        else if (this.data.firstname === undefined) {
            this.firstnameMsg = "Firstname cannot be empty";
            return false;
        }

        else if (this.data.firstname.length > 25 && this.data.firstname! == "") {
            this.firstnameMsg = "Firstname cannot be greater than 25 characters";
            return false;
        }

        else if (this.data.lastname === undefined) {
            this.lastnameMsg = "Lastname cannot be empty";
            return false;
        }

        else if (this.data.lastname.length > 25 && this.data.lastname! == "") {
            this.lastnameMsg = "Lastname cannot be greater than 25 characters";
            return false;
        }

        else if (!this.isEmailIDCorrect(this.data.email)) {
            this.emailMsg = "Please enter valid Email ID";
            return false;
        }

        else if (this.data.password === undefined) {
            this.passwordMsg = "Password cannot be empty";
            return false;
        }

        else if (this.data.confirmPassword === undefined) {
            this.confirmPasswordMsg = "Password cannot be empty";
        }

        else if (this.data.password !== this.data.confirmPassword) {
            this.confirmPasswordMsg = "Password does not match";
            return false;
        }
        else if (!this.termscondition) {
            this.errmsg = "please check the terms and condition"
            return false;
        }
        else

            return true;
    }

    isEmailIDCorrect(email) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }

    public backToLogin() {
        this.navCtrl.setRoot("AppLoginPage");
    }

    terms() {
        if (this.termscondition) {
            this.errmsg = ""
        }
        else {
            this.errmsg = "please check the terms and condition";
        }
    }

    createAccount() {
        let self = this;
        this.submitAttempt = true;

        if (!this.Registerform.valid) {
            console.log("Registration form Validation Fire!");
        }
        else {
            this.data.action = "insert";
            this.data.base_url = this.baseurl;
            this.data.appsecret = this.appsecret;
            this.data.firsttimelogin = 1;
            this.data.isterms = true
            this.data.usertype = 'patient';
            // if (this.data.usertype == "patient") {
            //     this.data.firsttimelogin = 1;
            // }
            // else {
            //     this.data.firsttimelogin = 0;
            // }
            if (this.isInputValid()) {
                this.loadingService.show();
                // this.data.id = this.af.database.ref().push().key;
                console.log("register data:::::>>>>>>", this.data);
                this.http.post(this.baseurl + "register.php", this.data).subscribe(data => {
                    this.loadingService.hide();
                    let result = JSON.parse(data["_body"]);
                    console.log(result.status1);
                    if (result.status1 == 'success') {
                        this.loadingService.hide();
                        let modal = self.modalCtrl.create(AppModalPage, {
                            'title': 'Confirm your email', 'description': 'Your account was successfully created but there\'s still few steps to do. Go to your mailbox and confirm your email and set up your profile!'
                        }, { cssClass: "my-register" });
                        modal.present();
                        // self.resetForm();
                        this.navCtrl.setRoot('AppWelcomeScreenPage')
                    } else if (result.status1 == 'exists') {
                        this.loadingService.hide();
                        let modal = self.modalCtrl.create(AppModalPage, { 'title': 'Email already exists', 'description': 'The email address you are trying to create is already exists, please try another email address.' }, { cssClass: "my-register" });
                        modal.present();
                        // self.resetForm();
                        
                    } else if (result.status1 == 'errorEmail') {
                        this.loadingService.hide();
                        let modal = self.modalCtrl.create(AppModalPage, { 'title': 'Email could not send', 'description': 'User registered but email couldnt be send, please contact administrator.', cssClass: "my-register" });
                        modal.present();
                    } else {
                        this.loadingService.hide();
                        // this.showToast("Something went wrong");
                        self.toastCtrl.presentToast("Something went wrong, Please check if email address you have enntered is same or not.");
                    }
                }, err => {
                    this.loadingService.hide();
                    console.log(err);
                });
                // }, function(error){
                //     console.log(error);
                //     if(error!==undefined && error.message!==undefined){
                //         self.errorMessage = error.message;
                //     }
                //     self.resetForm();
                // });
            }
        }
    }

    // New Implementation
    public goBack() {
        this.navCtrl.setRoot("AppWelcomeScreenPage");
    }

    public resetForm() {
        this.data.name = "";
        this.data.email = "";
        this.data.password = "";
        this.data.confirmPassword = "";
    }

    // public loginwithfackbook() {
    //     var self = this;
    //     // this.loadingService.show();
    //     this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then(function (res) {
    //         console.log('res', res);
    //         let displayName = res.user.displayName;
    //         let email = res.user.email;
    //         let id = this.af.database.ref().push().key;
    //         self.databaseService.CheckUserExist(email).subscribe(snapshot => {
    //             console.log('snapshot', snapshot);
    //             if (snapshot == '0') {
    //                 self.Registerdata = { 'id': id, 'email': email, 'name': displayName, 'loginwith': 'facebook' };
    //                 self.databaseService.registerUser(self.Registerdata, "1");
    //             }
    //             else {
    //                 self.navCtrl.setRoot("AppPatientsPage");
    //             }
    //         });
    //     }).catch(function (error) {
    //         console.log('error', error);
    //         // self.navCtrl.setRoot("AppLoginPage");
    //         // throw error;
    //     });
    // }

    // public loginwithgoogle() {
    //     var self = this;
    //     // this.loadingService.show();
    //     this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(function (res) {
    //         // console.log('res',res);
    //         let displayName = res.user.displayName;
    //         let email = res.user.email;
    //         let id = this.af.database.ref().push().key;
    //         self.databaseService.CheckUserExist(email).subscribe(snapshot => {
    //             console.log('snapshot', snapshot);
    //             if (snapshot == '0') {
    //                 self.Registerdata = { 'id': id, 'email': email, 'name': displayName, 'loginwith': 'facebook' };
    //                 self.databaseService.registerUser(self.Registerdata, "1");
    //             }
    //             else {
    //                 self.navCtrl.setRoot("AppPatientsPage");
    //             }
    //         });
    //     }).catch(function (error) {
    //         console.log('error', error);
    //         // self.navCtrl.setRoot("AppLoginPage");
    //         // throw error;
    //     });
    // }

}
