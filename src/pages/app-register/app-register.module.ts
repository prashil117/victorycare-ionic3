import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppRegisterPage} from "./app-register";


@NgModule({
  declarations: [
    AppRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(AppRegisterPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppRegisterModule {}
