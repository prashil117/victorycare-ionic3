import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyWheelSettingsActivityPage } from './daily-wheel-settings-activity';

@NgModule({
  declarations: [
    DailyWheelSettingsActivityPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyWheelSettingsActivityPage),
  ],
})
export class DailyWheelSettingsActivityPageModule {}
