import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Events, PopoverController } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { MyApp } from "../../app/app.component";
import { Chart } from 'chart.js';
import { Validators, FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
import { LoadingService } from '../../services/loading-service';
/**
 * Generated class for the AppDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-dashboard',
  templateUrl: 'app-dashboard.html',
  providers: [DatabaseService]
})
export class AppDashboardPage {
  Profileform: FormGroup;
  currentHCP: any[];
  @ViewChild('barChart') barChart;
  @ViewChild('barChart2') barChart2;
  @ViewChild('barChart3') barChart3;
  @ViewChild('barChart4') barChart4;

  bars: any;
  colorArray: any;
  bars2: any;
  colorArray2: any;
  bars3: any;
  colorArray3: any;
  bars4: any;

  colorArray4: any;
  dropdown: any = [];
  selected1: any = "";
  username: any = "";
  usertype: any = "";
  ut: any = [];
  notselected: boolean;
  userid: string;
  dependent: boolean = true;
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  hcpDropdownArray: any[];
  userLoginData: any;
  patientType: any;
  selected: any;


  constructor(private loadingService: LoadingService,public navCtrl: NavController, public navParams: NavParams, private databaseService: DatabaseService,  private menu: MenuController, public storage: Storage, public formBuilder: FormBuilder, public events: Events, public myApp: MyApp, public popoverController: PopoverController, public http: Http, private toastCtrl: ToastService) {
    this.storage.remove('patientSession');
    this.storage.remove('patientSessionData');
    this.storage.remove('user');
    this.Profileform = formBuilder.group({
      currentHCP: ['']
    });


    // this.get('currentHCP').then(result => {
    //   console.log('Username1:::::::: ' + result);
    //   console.log("result")
    //   if (result != null) {
    //     this.notselected = false;
    //     this.selected1 = result;
    //     console.log("resulrtttrtrtr")
    //   } else {
    //     this.notselected = true;
    //     console.log("dropdown    :::::::::", this.dropdown);
    //     this.selected1 = this.dropdown;
    //   }
    // });
  }

  intializeApp1() {
    if (this.userLoginData.usertype == "patient") {
      if (!this.patientType || this.patientType == undefined) {
        this.dependent = false;
      }
    }
    if (this.patientType == 'independent') {
      this.dependent = false;
      console.log("1234")
    }

    this.storage.get('patientSession').then((val) => {
      console.log(val);
      console.log("12345")
    });

    this.myApp.initializeApp();
    this.username = this.userLoginData.email;
    this.usertype = this.userLoginData.usertype;
    this.userid = this.userLoginData.id;
    this.ut = (this.usertype == 'healthcareprovider' ? false : true);
    console.log(this.userid);
    this.dropdown = this.getHCPData(this.userid, this.usertype);
    console.log(this.dropdown);
  }

  getHCPData(userid, usertype) {
    var hcp1: any = [];
    let self = this;
    var webServiceData: any = {};
    webServiceData.action = "gethcpdata";
    webServiceData.id = userid;
    webServiceData.usertype = usertype;
    webServiceData.appsecret = this.appsecret;
    
    self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
      this.loadingService.show();
      let result = JSON.parse(data["_body"]);
      console.log(result);
      if (result.status == 'success') {
        this.loadingService.hide();
        // result.datansol
        console.log("result ::::::::=>", result);
        hcp1 = hcp1.push(result.data);
        this.hcpDropdownArray = result.data;
        for (var i in this.hcpDropdownArray) {
          if (!this.selected || this.selected == undefined) {
            this.selected = this.hcpDropdownArray[0].id;
            this.set('currentHCP', this.selected)
          }
        }
      } else {
        this.loadingService.hide();
        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
      }
    }, err => {
      this.loadingService.hide();
      console.log(err);
    });
    return hcp1;
  }
  // set a key/value

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      return true;
    } catch (reason) {
      return false;
    }
  }
  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      return null;
    }
  }
  // remove a single key value:
  remove(key: string) {
    this.storage.remove(key);
  }

  selectEmployee($event) {
    this.set('currentHCP', $event);
    console.log("$seventtttttt", $event)
    this.doRefresh();
  }

  async getHCPid() {
    await this.get('currentHCP').then(result => {
      if (result != null) {
        this.selected = result;
        console.log("sekeccdffvfdgdfgdfg", this.selected)
        this.getuserLoginData()
      }
      else {
        this.getuserLoginData()
        console.log("else", this.selected)
      }
    });
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }
  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh();
      }
      else {
        this.patientType = result;
        this.intializeApp1();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }

  doRefresh() {
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }

  timeout() {
    setTimeout(() => {
      console.log('refresh');
      // event.target.complete();
    }, 2000);
  }

  ionViewDidLoad() {
    this.getHCPid();
    // this.getuserLoginData()
    // this.getpatientType()
    this.createBarChart();
    // this.storage.get('userLogin').then((val) => {
    //   console.log(val);
    // });
  }

  createBarChart() {
    this.bars = new Chart(this.barChart.nativeElement, {
      type: 'bar',
      data: {
        labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
        datasets: [{
          label: 'Viewers in millions',
          data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
          backgroundColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
          borderColor: 'rgb(255, 255, 255)',// array should have same number of elements as number of dataset
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
    this.bars2 = new Chart(this.barChart2.nativeElement, {
      type: 'line',
      data: {
        labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
        datasets: [{
          label: 'Viewers in millions',
          data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
          backgroundColor: 'rgb(225, 56, 56)', // array should have same number of elements as number of dataset
          borderColor: 'rgb(255, 255, 255)',// array should have same number of elements as number of dataset
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
    this.bars3 = new Chart(this.barChart3.nativeElement, {
      type: 'pie',
      data: {
        labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
        datasets: [{
          label: 'Viewers in millions',
          data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
          backgroundColor: ['rgb(225, 56, 56)', 'rgb(25, 1, 56)', 'rgb(78, 205, 196)', 'rgb(232, 198, 50)', 'rgb(255, 77, 20)', 'rgb(85, 98, 112)', 'rgb(224, 224, 224)', 'rgb(73, 88, 178)'], // array should have same number of elements as number of dataset
          borderColor: 'rgb(255, 255, 255)',// array should have same number of elements as number of dataset
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
    this.bars4 = new Chart(this.barChart4.nativeElement, {
      type: 'line',
      data: {
        labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
        datasets: [{
          label: 'Viewers in millions',
          data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
          backgroundColor: 'rgba(0, 0, 0, 0)', // array should have same number of elements as number of dataset
          borderColor: 'rgb(255, 255, 255)',// array should have same number of elements as number of dataset
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  logout() {

    this.storage.clear();
    this.databaseService.logout();
  }
}
