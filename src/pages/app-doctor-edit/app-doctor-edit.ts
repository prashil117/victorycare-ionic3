import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { AlertController, IonicPage, ModalController, NavController, NavParams } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import * as _ from "lodash";
import { Validators, FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { EmailValidator } from '../../app/directive/email-validator';
import { AppAddContactModalPage } from "../app-add-contact-modal/app-add-contact-modal";
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
// import { Routes, RouterModule } from '@angular/router';

/**
 * Generated class for the AppDoctorEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-doctor-edit',
  templateUrl: 'app-doctor-edit.html',
  providers: [Camera, DatabaseService]
})
export class AppDoctorEditPage implements AfterViewInit {
  newDoctors: any = "";
  doctorData: any = [];
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  doctorsData: any = [];
  usertype: any = [];
  topics = [];
  SearchData = [];
  Profileform: FormGroup;
  submitAttempt: boolean;
  userLoginData: any;
  patientType: any;
  data: any = {};
  // diagnosiss:any = {};
  upload: any = {};
  logo: any = {};
  action: any = "";
  image: any = "";
  enableDiv: boolean = false;
  selectType: any = "";
  enableDropdown: any = "";
  enableSelectDropdown: boolean;
  data1: any = {};
  data2: any = {};
  readonly: any = {};
  result: any = [];
  myDoctor: string;
  editable: boolean;
  date: any = new Date();
  hcp: any = [];
  hcpid: any;
  savebutton: boolean;
  confirmPasswordMsg: string;

  @ViewChild('imageUploader') imageUploader: ElementRef;
  @ViewChild('addPhotoCaption') addPhotoCaption: ElementRef;
  @ViewChild('removePhotoIcon') removePhotoIcon: ElementRef;
  @ViewChild('previewImage') previewImage: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams,  public storage: Storage, public formBuilder: FormBuilder, private nativeGeocoder: NativeGeocoder, public modalCtrl: ModalController, private alertCtrl: AlertController, private databaseService: DatabaseService,private loadingService: LoadingService, private camera: Camera, public http: Http, private toastCtrl: ToastService) {
    console.log('edit doctor', navParams.data.data);
    this.upload = "assets/images/upload1.svg";
    this.readonly = false;
    this.Profileform = formBuilder.group({
      selectType: [''],
      // email: ['',  Validators.compose([Validators.required, EmailValidator.isValid, Validators.pattern('^[a-z0-9_.+-]+@[a-z0-9-]+.[a-z0-9-.]+$')])],
      storedDoctors: ['',],
      firstname: ['', Validators.compose([Validators.maxLength(15), Validators.minLength(1), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastname: ['', Validators.compose([Validators.maxLength(15), Validators.minLength(1), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid, Validators.pattern('^[a-z0-9_.+-]+@[a-z0-9-]+.[a-z0-9-.]+$')])],
      birthDate: [''],
      mobile: ['', Validators.compose([Validators.maxLength(15), Validators.minLength(4), Validators.pattern('^[0-9]+$'), Validators.required])],
      password: ['', Validators.required],
      confirmpassword: [''],
      sex: [''],
      // languagePreferred: ['', Validators.required],
      address: [''],
      usertype: [''],
      monStart: [''],
      monEnd: [''],
      tueStart: [''],
      tueEnd: [''],
      wedStart: [''],
      wedEnd: [''],
      thuStart: [''],
      thuEnd: [''],
      friStart: [''],
      friEnd: [''],
      satStart: [''],
      satEnd: [''],
      sunStart: [''],
      sunEnd: [''],
      department: [''],
      yearofpassing: [''],
      designation: [''],
      description: [''],
      doctors: this.formBuilder.array([
        this.initDoctor(),
      ])
    });
    // });
  }

  appIntialize1() {
    this.data.address = (this.data.address == undefined ? "" : this.data.address);
    this.data.usertype = (this.data.usertype == undefined ? "doctor" : this.data.usertype);
    this.data.description = (this.data.description == undefined ? "" : this.data.description);
    console.log(this.navParams.data);
    this.doctorsData = this.navParams.data.doctorData;
    this.myDoctor = this.navParams.data.myDoctor;
    this.editable = this.navParams.data.editable;
    this.savebutton = (this.navParams.data.savebutton && this.navParams.data.savebutton == true) ? true : false;
    if (this.userLoginData.usertype == "healthcareprovider" || this.userLoginData.usertype == "doctor") {
      this.savebutton = true;
    }
    // this.enableDiv = (navParams.data.editable == false ? true : false);
    this.enableDiv = (this.userLoginData.usertype) == 'healthcareprovider' ? false : true;
    this.enableDropdown = (this.myDoctor != undefined ? true : false);
    this.enableSelectDropdown = (this.userLoginData.usertype == 'healthcareprovider' && this.navParams.data.editable != false) ? true : false;

    this.data1.newDoctors = this.getDoctors(this.doctorsData);
    console.log(this.result);
    console.log(this.data1.newDoctors);
    console.log(this.enableSelectDropdown);
    console.log("mydoctor", this.myDoctor);

    if (!_.isEmpty(this.navParams.data.data)) {
      // this.action = 'edit';
      // console.log('navParams',navParams.data);
      this.data = this.navParams.data.data;
      console.log(this.data);
      this.action = this.navParams.data.action;
      if (this.data.doctors !== undefined) {
        for (let i = 0; i < this.data.doctors.length; i++) {
          const control = <FormArray>this.Profileform.controls['doctors'];
          control.push(this.initDoctor());
        }
      }
      else {
        this.data.doctors = [];
      }
      // this.selectType();
      // console.log(this.selectType($event == 'new'));
      // }
    }

  }

  ionViewWillEnter() {
    // this.storageGet();
    // this.data.newDoctors = this.storageGet();
    // console.log(this.data.newDoctors);
  }
  storageGet() {
    var x0 = this.storage.get('doctorsData');
    var var1 = [];
    Promise.all([x0]).then((arrayOfResults) => {
      var1 = arrayOfResults[0];
    });
    return var1;
  }
  getDoctors(doctorsData) {
    console.log(doctorsData);
    var keys: any = [];
    var result1: any = [];
    var hcpWebServiceData: any = {};
    let self = this;
    hcpWebServiceData.action = "getdropdowndoctorsdata";
    hcpWebServiceData.appsecret = this.appsecret;
    hcpWebServiceData.getdropdowndoctorsdata = doctorsData;
    hcpWebServiceData.hcpid = this.hcpid;
    // self.data.id = this._cookieService.get('userid');
    self.http.post(this.baseurl + "getdropdowndata.php", hcpWebServiceData).subscribe(data => {
      let result = JSON.parse(data["_body"]);
      console.log(result);
      if (result.status == 'success') {
        console.log(result.data);
        // result.data;
        result1 = result1.push(result.data); //[result.data];
        console.log(result1);
        // return result1;
      } else {
        // this.toastCtrl.presentToast("Unable to fetch data");
      }
    }, err => {
      console.log(err);
    });
    return result1;
  }
  comparer(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        // console.log(other);
        return other.email == current.email //&& other.name == current.name
        // return other == current
      }).length == 0;
    }
  }
  checkState(email) {
    console.log(email);
    this.loadingService.show();
    var hcpWebServiceData: any = {};
    hcpWebServiceData.action = "getuserbyid";
    hcpWebServiceData.id = email;
    hcpWebServiceData.appsecret = this.appsecret;
    console.log(this.data);
    this.data1.selectType = 'exist';
    this.http.post(this.baseurl + "login.php", hcpWebServiceData).subscribe(snapshot => {
      console.log(snapshot);
      let result = JSON.parse(snapshot["_body"]);
      // var snapshot = snapshot.val();
      if (result.status == 'success') {
        this.data = result.data;
        this.data.name = result.data.firstname + ' ' + result.data.lastname;
        this.enableDropdown = false;
        this.data.storedDoctors = true;
        this.enableDiv = true;
        this.loadingService.hide();
      } else {
        // self.loadingService.hide();
        this.toastCtrl.presentToast("Unable to fetch data");
        // self.errorMessage = "Unable to fetch data";
      }
    });
  }
  addType($event) {
    console.log($event);
    if ($event == 'new') {
      //this.doRefresh($event);
      this.loadingService.show();
      // this.enableDropdown = "display:none !important;";
      // this.enableDiv = "display:block !important;";
      this.enableDropdown = false;
      this.enableDiv = false;
      this.selectType = 'addnew';
      //this.data1.selectType = false;
      this.loadingService.hide();
    } else if ($event == 'exist') {
      // this.doRefresh($event);
      this.loadingService.show();
      // this.enableDropdown = "display:block !important;";
      // this.enableDiv = "display:none !important;";
      this.enableDropdown = true;
      this.enableDiv = (this.userLoginData.usertype != 'healthcareprovider') ? true : false;
      this.selectType = 'addexist';
      //this.data.storedDoctors = false;
      console.log("this.data.storedoctors", this.data.storedDoctors)
      this.loadingService.hide();
    }
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }

  public selector() {
    for (var i = 0; i < this.newDoctors.length; i++) {
      this.newDoctors[i].checked = true;
    }
    console.log(this.newDoctors)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppDoctorEditPage');
    this.getHCPid();
  }

  initDoctor() {
    return this.formBuilder.group({
      email: [''],
      name: ['']
    });
  }
  onblur() {
    if (this.data1.selectType == "new") {
      if (this.data.confirmpassword === undefined) {
        this.confirmPasswordMsg = "Password cannot be empty";
        return
      }
      else if (this.data.password !== this.data.confirmpassword) {
        this.confirmPasswordMsg = "Password does not match";
        return
      }
      else {
        this.confirmPasswordMsg = "";
      }
    }
  }
  addDoctorDetails() {
    let self = this;
    this.submitAttempt = true;

    if ((!this.data.storedDoctors || this.data.storedDoctors == undefined) && this.selectType == "addexist") {
      console.log("this.data", this.data.storedDoctors);
      const alert = this.alertCtrl.create({
        title: 'Please select doctor',
        // subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
        buttons: ['OK']
      });
      alert.present();
    }
    else if (!this.Profileform.valid && this.data1.selectType == "new") {
      console.log("Profileform Validation Fire!");
    }
    else {
      console.log('this.data', this.data);
      this.Profileform.value.id = this.data.id;
      if (this.data.image != undefined) {
        this.data.image = this.data.image;
      }
      else {
        this.showImagePreview();
        this.data.image = "";
      }
      if (this.action === 'edit') {
        self.data.action = "updateuser";
        console.log(self.data);
        self.data.appsecret = this.appsecret;
        self.http.post(this.baseurl + "updateuser.php", self.data).subscribe(data => {
          let result = JSON.parse(data["_body"]);
          console.log(result.status);
          if (result.status == 'success') {
            self.toastCtrl.presentToast("Doctors profile is updated successfully");
            if (this.userLoginData.usertype == "patient") {
              self.navCtrl.setRoot("AppMyDoctorsPage", { 'data': self.data });
            }
            else {
              self.navCtrl.setRoot("AppDoctorsPage", { 'data': self.data });
            }
          } else {
            self.toastCtrl.presentToast("There is an error saving data!");
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
          }
        }, err => {
          console.log(err);
        });
        // this.updateDoctorDetails().then(function(){
        //       self.navCtrl.setRoot("AppDoctorsPage",{'data': self.data});
        //   });
      } else {
        console.log(this.data1.selectType);
        // return false;
        if (this.data1.selectType == 'exist') {
          this.loadingService.show();
          self.data.action = "adddoctortohcp";
          self.data.userid = this.data.id;

          self.data.hcpid = this.userLoginData.id;

          // console.log(this.data);return false;
          self.data.appsecret = this.appsecret;
          self.http.post(this.baseurl + "insertrelationdata.php", self.data).subscribe(data => {
            this.loadingService.show();
            let result = JSON.parse(data["_body"]);
            console.log(result);
            if (result.status == 'success') {
              self.loadingService.hide();
              self.toastCtrl.presentToast("Doctor added successfully");
              self.navCtrl.setRoot("AppDoctorsPage");
              // return result1;
            } else {
              self.loadingService.hide();
              self.toastCtrl.presentToast("There is a problem adding data, please try again");
              self.navCtrl.setRoot("AppMyDoctorsPage");
              // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
          }, err => {
            this.loadingService.hide();
            console.log(err);
          });
        } else {
          console.log(this.userLoginData.email);
          this.loadingService.show();
          var hcp: any = [];
          var doctors: any = [];
          // console.log(this.userTypeToBeAdded);
          // self.loadingService.show();
          self.data.action = "insert";
          self.data.doctorid = this.userLoginData.id;
          self.data.usertype = 'doctor';
          self.data.appsecret = this.appsecret;
          self.data.firsttimelogin = 0;
          self.data.base_url = this.baseurl;
          self.http.post(this.baseurl + "register.php", self.data).subscribe(data => {
            let result = JSON.parse(data["_body"]);
            console.log(result);
            if (result.status1 == 'success') {
              self.loadingService.hide();
              self.toastCtrl.presentToast("Doctor added successfully");
              self.navCtrl.setRoot("AppDoctorsPage");
              // return result1;
            } else if (result.status1 == 'exists') {
              self.loadingService.hide();
              self.toastCtrl.presentToast("Doctor exists please add from existing or contact administrator.");
              // self.navCtrl.setRoot("AppDoctorsPage");
            } else {
              self.loadingService.hide();
              // self.toastCtrl.presentToast("There is a problem adding data, please try again");
              self.navCtrl.setRoot("AppDoctorsPage");
              // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
            }
          }, err => {
            console.log(err);
          });
        }
      }
    }
  }

  ImageUpload() {
    let alert = this.alertCtrl.create({
      // title: 'Image Upload',
      message: 'Select Image source',
      buttons: [
        {
          text: 'Upload from Library',
          handler: () => {
            this.gallery(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.gallery(this.camera.PictureSourceType.CAMERA);
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  gallery(sourceType: PictureSourceType) {
    let self = this;
    this.camera.getPicture({
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      correctOrientation: true,
      allowEdit: true
    }).then((imageData) => {
      // imageData is a base64 encoded string
      self.showImagePreview();
      self.data.image = "data:image/jpeg;base64," + imageData;
      self.image = "data:image/jpeg;base64," + imageData;

    }, (err) => {
      console.log("Error " + err);
    });
  }

  showImagePreview() {
    let self = this;
    self.setVisibility(self.previewImage, "block");
    self.setVisibility(self.imageUploader, "none");
    self.setVisibility(self.addPhotoCaption, "none");
    self.setVisibility(self.removePhotoIcon, "block");
  }

  removeImage() {
    let self = this;
    this.data.image = undefined;
    self.setVisibility(self.previewImage, "none");
    self.setVisibility(self.imageUploader, "block");
    self.setVisibility(self.addPhotoCaption, "block");
    self.setVisibility(self.removePhotoIcon, "none");

  }

  setVisibility(element: ElementRef, state) {
    if (element !== undefined)
      element.nativeElement.style.display = state;
  }


  ngAfterViewInit(): void {
    // this.setInitialDiagnosis();
    if (this.data.contacts === undefined) {
      this.data.contacts = [];
    }

    if (!_.isEmpty(this.action)) {
      this.showImagePreview();

      if (this.action === "view") {
        this.readonly = true;
      }
    }
  }

  logout() {
    this.databaseService.logout();
  }
  // set a key/value
  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log('set string in storage: ' + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log('storageGET: ' + key + ': ' + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
        }
        
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh('');
      }
      else {
        this.patientType = result;
        this.appIntialize1();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }


  // remove a single key value:
  remove(key: string) {
    this.storage.remove(key);
  }
  async getHCPid() {
    await this.get('currentHCP').then(result => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      } else {
        this.getuserLoginData();
      }
    });
  }

  getDate() {
    let date = new Date(this.date);
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    // return date.getDate();
  }
}
