import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppDoctorEditPage } from './app-doctor-edit';

@NgModule({
  declarations: [
    AppDoctorEditPage,
  ],
  imports: [
    IonicPageModule.forChild(AppDoctorEditPage),
  ],
})
export class AppDoctorEditPageModule {}
