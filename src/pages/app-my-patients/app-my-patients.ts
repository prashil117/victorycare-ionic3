import { Component, AfterViewInit } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, Events, MenuController } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { Validators, FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { MyApp } from "../../app/app.component";
import { Http } from '@angular/http';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";

/**
 * Generated class for the AppMyPatientsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-my-patients',
  templateUrl: 'app-my-patients.html',
  providers: [DatabaseService]
})
export class AppMyPatientsPage implements AfterViewInit {
  SelectList: FormGroup;
  currentHCP: any[];
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  data: any = {};
  eventsData: any = [];
  patientData: any = {};
  SearchData: any = [];
  terms: string;
  doctorEmail: any = [];
  userLoginData: any;
  patientType: any;
  editOn: boolean = false;
  result: any = [];
  editable: boolean = false;
  actionLogo: any = "";
  usertype: string;
  username: string;
  userid: string;
  ut: boolean;
  dropdown: any = [];
  notselected: boolean;
  selected1: any = [];

  constructor(public navCtrl: NavController, navParams: NavParams, private menu: MenuController, public storage: Storage,  public events: Events, private alertCtrl: AlertController, private databaseService: DatabaseService,private loadingService: LoadingService, public formBuilder: FormBuilder, public myApp: MyApp, public http: Http, private toastCtrl: ToastService) {
    console.log('app patients');
    this.SelectList = formBuilder.group({
      currentHCP: ['']
    });
    this.doctorEmail = navParams.data.data;
    ////////// HCP DropDown Code ends /////////////
  }

  intializeApp1() {

    this.editable = (this.userLoginData.usertype == 'healthcareprovider' ? true : false);
    this.actionLogo = (this.userLoginData.usertype == 'healthcareprovider' ? "edit-icon.png" : "info-icon.png");
    console.log(this.actionLogo);

    ////////// HCP DropDown Code Starts /////////////

    this.myApp.initializeApp();
    this.username = this.userLoginData.email;
    this.usertype = this.userLoginData.usertype;
    this.userid = this.userLoginData.id;
    this.ut = (this.usertype == 'healthcareprovider' ? false : true);

    console.log(this.userid);
    // alert(this.usertype);
    this.dropdown = this.getHCPData(this.userid, this.usertype);
    console.log(this.dropdown);
    var result = null;
    this.get('currentHCP').then(result => {
      console.log('Username1: ' + result);
      if (result != null) {
        this.notselected = false;
        this.selected1 = result;
        console.log('Username: ' + this.selected1);
      } else {
        this.notselected = true;
        console.log(this.dropdown);
        this.selected1 = this.dropdown;
      }
    });
    this.ionViewWillEnter()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppMyPatientsPage');
    this.getuserLoginData();
  }

  logout() {
    this.databaseService.logout();
  }

  ionViewWillEnter() {
    this.loadingService.show();
    let self = this;
    this.usertype = this.userLoginData ? this.userLoginData.usertype : '';
    var webServiceData: any = {};
    this.editOn = true;
    if (this.usertype == 'healthcareprovider') {
      webServiceData.action = "getpatientsfromhcplogin";
      webServiceData.usertype = "patient";
      webServiceData.parentid = this.userLoginData.id;
      webServiceData.appsecret = this.appsecret;
      console.log(webServiceData);
      self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
        
        let result = JSON.parse(data["_body"]);
        console.log(result);
        if (result.status == 'success') {
          this.loadingService.hide();
          console.log(result.data);
          self.SearchData = result.data;
          // self.SearchData.firstname = (result.data.firstname == null ? 'noname' : result.data.firstname);
          self.data.values = result.data;
        } else {
          this.loadingService.hide();
          this.toastCtrl.presentToast("No data found");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      }, err => {
        this.loadingService.hide();
        console.log(err);
      });
    }
    else {
      webServiceData.action = "getpatientfromdoctornurse";
      self.storage.get('currentHCP').then((val) => {
        webServiceData.parentid = val;
        webServiceData.usertype = "patient";
        webServiceData.userid = this.userLoginData.id;
        webServiceData.appsecret = this.appsecret;
        console.log(webServiceData);
        self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
          let result = JSON.parse(data["_body"]);
          console.log(result);
          if (result.status == 'success') {
            this.loadingService.hide();
            console.log(result.data);
            self.SearchData = result.data;
            self.data.values = result.data;
          } else {
            this.loadingService.hide();
            this.toastCtrl.presentToast("No data found");
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
          }
        }, err => {
          this.loadingService.hide();
          console.log(err);
        });
      });
    }
  }

  comparer(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        // console.log(other);
        return other.email != current.email //&& other.name == current.name
        // return other == current
      }).length == 0;
    }
  }

  comparer2(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        // console.log(other);
        return other.email == current.email //&& other.name == current.name
        // return other == current
      }).length == 0;
    }
  }
  ngAfterViewInit(): void {

  }

  onSelectedPatient(item, action = "") {
    this.storage.set('user', item);
    console.log(item);
    // this.events.publish('user:created', item);

    this.navCtrl.setRoot("AppTimelinePage", { 'data': item, 'action': action });

  }

  onAddPatient(data, email, userData) {
    console.log(data);
    console.log(email);
    console.log(userData);
    this.navCtrl.push("AppPatientProfilePage", { 'patientData': data, 'email': email, 'userData': userData });
  }
  viewPatient(patient, editable) {
    console.log(editable);
    this.navCtrl.push("AppMyPatientProfilePage", { 'data': patient, 'action': 'edit', 'editable': editable });
  }

  RedirectToContacts(item) {
    this.navCtrl.push("AppPatientsContactsPage", { 'data': item });
  }

  RedirectToMedicineMasterList(item) {
    this.navCtrl.push("AppMedicineMasterListPage", { 'data': item });
  }

  RedirectToActivityMasterList(item) {
    this.navCtrl.push("AppActivityMasterListPage", { 'data': item });
  }

  RedirectToEventMasterList(item) {
    this.navCtrl.push("AppEventMasterListPage", { 'data': item });
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  setFilteredPatientsData() {
    this.SearchData = this.data.values.filter((Patient) => {
      return Patient.firstname.toLowerCase().indexOf(this.terms.toLowerCase()) > -1;
    });
  }

  deletePatientData(item) {
    var item1: any = [];
    item1 = [item];
    console.log(item);

    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Do you want to de-link this Patient?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            let self = this;
            var value: any = [];
            var keys: any = [];
            var result: any = [];
            var currentData = [{ id: self.userLoginData.id, email: self.userLoginData.email, name: self.userLoginData.firstname }];
            console.log('Delete Ok clicked');
            this.data.action = "unlinkhcpfromdoctor";
            this.data.parentid = this.userLoginData.id;
            this.data.userid = item.userid;
            this.data.appsecret = this.appsecret;
            console.log(this.data);
            this.http.post(this.baseurl + "removeuser.php", this.data).subscribe(data => {
              let result = JSON.parse(data["_body"]);
              console.log(result.status);
              if (result.status == 'success') {
                // self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
                self.toastCtrl.presentToast("Patient is unlinked successfully");
                this.navCtrl.setRoot(this.navCtrl.getActive().component);
                // self.logout();
              } else {
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
              }
            }, err => {
              console.log(err);
            });
          }
        }
      ]
    });
    alert.present();
  }
  ////////// HCP DropDown Code Starts /////////////

  getHCPData(userid, usertype) {

    var hcp1: any = [];
    let self = this;
    var webServiceData: any = {};
    webServiceData.action = "gethcpdata";
    webServiceData.id = userid;
    webServiceData.usertype = usertype;
    webServiceData.appsecret = this.appsecret;
    // self.data.id = this._cookieService.get('userid');
    self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
      let result = JSON.parse(data["_body"]);
      ;
      if (result.status == 'success') {
        // result.data;
        hcp1 = hcp1.push(result.data);
        // hcp1 = result.data;
        console.log(hcp1);
        // return result1;
      } else {
        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
      }
    }, err => {
      console.log(err);
    });
    return hcp1;
  }

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log('set string in storage: ' + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log('storageGET: ' + key + ': ' + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }
  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh();
      }
      else {
        this.patientType = result;
        this.intializeApp1();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }



  // remove a single key value:
  remove(key: string) {
    this.storage.remove(key);
  }

  doRefresh(event = '') {
    this.loadingService.show();
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.loadingService.hide();
    }, 2000);
  }
  selectEmployee($event) {
    this.doRefresh();
    this.set('currentHCP', $event);
  }
  ////////// HCP DropDown Code ends /////////////
}
