import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppMyPatientsPage } from './app-my-patients';

@NgModule({
  declarations: [
    AppMyPatientsPage,
  ],
  imports: [
    IonicPageModule.forChild(AppMyPatientsPage),
  ],
})
export class AppMyPatientsPageModule {}
