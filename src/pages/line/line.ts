import { Component, ViewChild } from "@angular/core";
import {
  AlertController,
  Navbar,
  IonicPage,
  ModalController,
  NavController,
  NavParams,
  Item,
} from "ionic-angular";
import * as _ from "lodash";
import { LoadingService } from "../../services/loading-service";
import { DatabaseService } from "../../services/database-service";
import { reduce } from "rxjs/operators";
import { ColdObservable } from "rxjs/testing/ColdObservable";

/**
 * Generated class for the LinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-line",
  templateUrl: "line.html",
  providers: [DatabaseService],
})
export class LinePage {
  @ViewChild(Navbar)
  navBar: Navbar;
  patientData: any = {};

  medicines: any[];
  WheelActivityData: any = [];
  SettingsData: any = [];
  date: any = new Date();
  activityData: any = [];
  data: any = [];
  ActivityArrar: any = [];
  myColor: any = [];
  name: any = [];
  BorderColor: any = [];

  bordercolors: Array<string> = [
    "#7e1b1b",
    "#611881",
    "#3b2079",
    "#004d44",
    "#1d491f",
    "#787b1e",
    "#966c03",
    "#996100",
    "#804000",
    "#5e7a87",
  ];
  colors: Array<string> = [
    "#D32F2F",
    "#7B1FA2",
    "#512DA8",
    "#00796B",
    "#388E3C",
    "#AFB42B",
    "#FBC02D",
    "#FFA000",
    "#F57C00",
    "#455A64",
  ];

  constructor(
    public navCtrl: NavController,
    private loadingService: LoadingService,
    public modalCtrl: ModalController,
    private databaseService: DatabaseService,
    public navParams: NavParams,
  ) {
    if (!_.isEmpty(navParams.data)) {
      this.patientData = navParams.data.data;
      this.loadWheelActivity();
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad LinePage");
  }

  loadWheelActivity() {
    let self = this;
    let Todaydate = this.getDate();
    console.log("Todaydate", Todaydate);
  }

  toObject(myColor) {
    var result = [];
    for (var i = 0; i < this.colors.length; i++) {
      result[this.colors[i]] = this.bordercolors[i];
    }
    return result[myColor];
  }

  addActivity() {
    console.log("add Activity");
    let modal = this.modalCtrl.create(
      "AppAddActivityModelPage",
      { "patientData": this.patientData, "patientSettings": this.SettingsData },
    );
    modal.present();
  }

  addMedicien() {
    console.log("add Medicens");
    let modal = this.modalCtrl.create(
      "AppAddMedicineModelPage",
      { "patientData": this.patientData, "patientSettings": this.SettingsData },
    );
    modal.present();
  }

  addStatus() {
    console.log("add curent status");
    let modal = this.modalCtrl.create(
      "AppAddCurrentStatusModelPage",
      { "patientData": this.patientData, "patientSettings": this.SettingsData },
    );
    modal.present();
  }


  getDate() {
    let date = new Date(this.date);
    return date.getDate() + "-" + (date.getMonth() + 1) + "-" +
      date.getFullYear();
  }
}
