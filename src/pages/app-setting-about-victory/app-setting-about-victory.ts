import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AppSettingAboutVictoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-setting-about-victory',
  templateUrl: 'app-setting-about-victory.html',
})
export class AppSettingAboutVictoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppSettingAboutVictoryPage');
  }


  openwindow() {
    console.log("inside url")
    // window.open('', '_system');
    window.open("https://victory.care/",'_system', 'location=yes');
  }

}
