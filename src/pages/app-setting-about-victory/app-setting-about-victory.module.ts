import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppSettingAboutVictoryPage } from './app-setting-about-victory';

@NgModule({
  declarations: [
    AppSettingAboutVictoryPage,
  ],
  imports: [
    IonicPageModule.forChild(AppSettingAboutVictoryPage),
  ],
})
export class AppSettingAboutVictoryPageModule {}
