import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { AppLoggedActivityTabPage} from "./app-logged-activity-tab";



@NgModule({
  declarations: [
      AppLoggedActivityTabPage
  ],
  imports: [
    IonicPageModule.forChild(AppLoggedActivityTabPage)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})

export class AppLoggedActivityTabModule {}
