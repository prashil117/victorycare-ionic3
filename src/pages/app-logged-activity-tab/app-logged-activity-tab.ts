import {Component, AfterViewInit} from '@angular/core';
import {
    AlertController, IonicPage, ModalController, NavController, NavParams, Platform,
    ViewController
} from 'ionic-angular';
import {DatabaseService} from "../../services/database-service";
import {LoadingService} from "../../services/loading-service";
import {Observable} from 'rxjs/Observable';
import * as _ from "lodash";
import {AppSharedService} from "../../services/app-shared-service";


@IonicPage()
@Component({
    selector: 'app-logged-activity-tab',
    templateUrl: 'app-logged-activity-tab.html',
    providers: [DatabaseService]
})
export class AppLoggedActivityTabPage {

    patientData: any = {};
    patientSettings: any = {};
    data: any = {};
    selectedDate: any = {};
    selectedTime: any = {};
    readOnly:any = true;

    constructor(public navCtrl: NavController, navParams: NavParams, private alertCtrl: AlertController, private databaseService: DatabaseService, private loadingService: LoadingService,public modalCtrl: ModalController, public appSharedService: AppSharedService) {

        let self = this;
        if(navParams.data !== undefined){
            this.selectedDate = navParams.data.selectedDate;
            this.selectedTime = navParams.data.selectedTime;
        }
        if(this.appSharedService.patientData !== undefined){
            this.patientData = this.appSharedService.patientData;
        }
    }


    setDummyValues(){
       let self = this;
        _.each(self.appSharedService.patientSettings.activityData, function(item, index){
            if(self.data[index] === undefined){
                self.data[index] = {'what':'','amount':''};
            }
        });
    }


    getSlot(interval){
        let selectedHour = this.selectedTime.split(':')[0];
        let selectedMinutes = this.selectedTime.split(':')[1];
        return selectedHour*(60/interval) + (selectedMinutes % interval === 0 ? (selectedMinutes/interval)+1 : (selectedMinutes / interval) - ((selectedMinutes/interval)%1)+1);
    }

    getDate(){
        var date = new Date(this.selectedDate);
        return date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
       // return (new Date(this.selectedDate)).toLocaleDateString().split('/').join('-');
    }

    makeEditable(){
        this.readOnly = false;
    }


}



