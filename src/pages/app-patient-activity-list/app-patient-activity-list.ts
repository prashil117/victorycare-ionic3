import { Component, AfterViewInit } from "@angular/core";
import {
  AlertController,
  IonicPage,
  ModalController,
  NavController,
  NavParams,
  Platform,
  ViewController,
  App,
  Events,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from "rxjs/Observable";
import * as _ from "lodash";
import { Storage } from "@ionic/storage";
import { AppPatientActivityModalPage } from "../app-patient-activity-modal/app-patient-activity-modal";
import { AppPatientSettingsPage } from "../app-patient-settings/app-patient-settings";
import { ToastService } from "../../services/toast-service";
import { Http } from "@angular/http";
import { environment } from "../../environment/environment";

@IonicPage()
@Component({
  selector: "app-patient-activity-list",
  templateUrl: "app-patient-activity-list.html",
  providers: [DatabaseService],
})
export class AppPatientActivityListPage {
  data: any = {};
  activities: any = [];
  patientData: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  count: number;
  ActivityData: any;
  isloadmore: boolean = false;
  offset: number = 0;
  userLoginData: any;

  constructor(
    public storage: Storage,
    private toastCtrl: ToastService,
    public http: Http,
    public navCtrl: NavController,
    navParams: NavParams,
    private app: App,
    public events: Events,
    private alertCtrl: AlertController,
    private databaseService: DatabaseService,
    private loadingService: LoadingService,
    public modalCtrl: ModalController,
  ) {
  }

  addActivityListItem() {
    let modal = this.modalCtrl.create(
      AppPatientActivityModalPage,
      {
        "type": "add",
        "activities": this.activities,
        "patientData": this.patientData,
      },
    );
    // let modal = this.modalCtrl.create(AppPatientsActivityMasterModalPage, { 'activities': this.activities, 'patientData': this.patientData });
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.tmparr = [];
        this.ActivityData = [];
        this.loadActivityListSettings();
      }
    });
  }
  doRefresh() {
    this.loadingService.show();
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.loadingService.hide();
    }, 100);
  }
  tmparr = [];
  loadActivityListSettings(infiniteScroll?) {
    this.data.action = "getactivitymasterdata";
    this.data.offset = 0;
    this.data.keyword = "";
    this.data.userid = this.userLoginData.id;
    this.data.appsecret = this.appsecret;
    console.log("this.data", this.data);
    this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
      (data) => {
        this.loadingService.show();
        let result = JSON.parse(data["_body"]);
        console.log("result", result);
        if (infiniteScroll) {
          infiniteScroll.complete();
        }
        if (result.status == "notmatchingkey") {
          this.doRefresh();
        }
        if (result.status == "success") {
          this.loadingService.hide();
          if (result.data0 !== null) {
            for (var i in result.data0) {
              this.tmparr.push(result.data0[i]);
            }
            this.ActivityData = this.tmparr;
            if (result.data0) {
              this.count = result.data0[0].count;
            } else {
              this.count = 0;
            }
            console.log("ActivityData", this.ActivityData);
          } else {
            this.loadingService.hide();
            this.data = [];
            this.data.keys = [];
            this.ActivityData = [];
          }
        } else {
          // this.loadingService.hide();
          this.loadingService.hide();
          // this.toastCtrl.presentToast("There is a no data available.");
          // this.navCtrl.setRoot("AppDashboardPage");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      },
      (err) => {
        this.loadingService.hide();
        console.log(err);
      },
    );
  }

  loadMore(infiniteScroll) {
    if (this.ActivityData.length >= this.count && this.count == 0) {
      infiniteScroll.enable(false);
    } else {
      this.offset = this.offset + 10;
      this.loadActivityListSettings(infiniteScroll);
    }
  }

  editActivity(activity, index) {
    let modal = this.modalCtrl.create(
      AppPatientActivityModalPage,
      {
        "activity": activity,
        "patientData": this.patientData,
        "type": "edit",
        "onboard": true,
        "index": index,
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.tmparr = [];
        this.ActivityData = [];
        this.loadActivityListSettings();
      }
    });
  }

  deleteActivity(item, index) {
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete this record?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
        {
          text: "Delete",
          handler: () => {
            let self = this;
            this.data.action = "deleteactivity";
            this.data.id = item.id;
            this.data.appsecret = this.appsecret;
            this.http.post(this.baseurl + "deletedata.php", this.data)
              .subscribe((data) => {
                this.loadingService.show();
                let result = JSON.parse(data["_body"]);
                if (result.status == "success") {
                  this.loadingService.hide();
                  self.toastCtrl.presentToast(
                    "activity is deleted successfully",
                  );
                  this.ActivityData = [];
                  this.tmparr = [];
                  this.loadActivityListSettings();
                } else {
                  this.loadingService.hide();
                  self.toastCtrl.presentToast(
                    "There is an error deleting data!",
                  );
                }
              }, (err) => {
                this.loadingService.hide();
                console.log(err);
              });
          },
        },
      ],
    });
    alert.present();
  }
  // tabNextButton() {
  //     this.app.getRootNav().setRoot("AppTimelinePage", { 'data': this.patientData });
  // }
  GotoHome() {
    this.loadingService.show();
    var data1 = {
      action: "setloginfirsttime",
      userid: this.userLoginData.id,
      appsecret: this.appsecret,
    }

    this.http.post(this.baseurl + "login.php", data1).subscribe((data) => {
      let result = JSON.parse(data["_body"]);
      if (result.status == "success") {
        this.loadingService.hide();
        this.app.getRootNav().setRoot("AppTimelinePage");
        console.log("result", result);
      } else {
        // this.showToast("Something went wrong");
        this.loadingService.hide();
        this.toastCtrl.presentToast("Something went wrong, Please try again.");
      }
    }, (err) => {
      this.loadingService.hide();
      console.log(err);
    });
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      return null;
    }
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.loadActivityListSettings();
      }
    });
  }
  ionViewDidLoad() {
    this.getuserLoginData();
  }
}
