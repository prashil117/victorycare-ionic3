import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppPatientActivityListPage} from "./app-patient-activity-list";



@NgModule({
  declarations: [
      AppPatientActivityListPage
  ],
  imports: [
    IonicPageModule.forChild(AppPatientActivityListPage)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})

export class AppPatientActivityListModule {}
