import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppPatientsEventMasterModalPage } from './app-patients-event-master-modal';

@NgModule({
  declarations: [
    AppPatientsEventMasterModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AppPatientsEventMasterModalPage),
  ],
})
export class AppPatientsEventMasterModalPageModule {}
