import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppPatientMedicineModalPage} from "./app-patient-medicine-modal";


@NgModule({
  declarations: [
      AppPatientMedicineModalPage
  ],
  imports: [
    IonicPageModule.forChild(AppPatientMedicineModalPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppPatientMedicineModalModule {}
