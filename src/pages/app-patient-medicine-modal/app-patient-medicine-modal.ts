import {Component, AfterViewInit} from '@angular/core';
import {
    AlertController, IonicPage, ModalController, NavController, NavParams, Platform,
    ViewController
} from 'ionic-angular';
import {DatabaseService} from "../../services/database-service";
import {LoadingService} from "../../services/loading-service";
import {Observable} from 'rxjs/Observable';
import * as _ from "lodash";
import {AppSharedService} from "../../services/app-shared-service";


@IonicPage()
@Component({
    selector: 'app-patient-medicine-modal',
    templateUrl: 'app-patient-medicine-modal.html',
    providers: [DatabaseService]
})
export class AppPatientMedicineModalPage {

    data: any = {};
    colors:any = [];
    medicines :any = [];
    patientData:any = [];
    eventType:any = {};
    itemIndex:any = {};

    constructor(public navCtrl: NavController, navParams: NavParams,  private alertCtrl: AlertController, private databaseService: DatabaseService, private loadingService: LoadingService,public modalCtrl: ModalController, public viewCtrl: ViewController, public appSharedService: AppSharedService) {
        if(!_.isEmpty(navParams.data)){

            if(navParams.data.type === 'edit'){
                this.data = navParams.data.medicine;
                this.eventType = navParams.data.type;
                this.itemIndex = navParams.data.index;
                this.patientData = navParams.data.patientData;

            }else{
                this.eventType = '';
                this.medicines = navParams.data.medicines;
                this.patientData = navParams.data.patientData;
            }
        }

        this.colors = this.appSharedService.colors;
    }

    closeModal(){
        this.viewCtrl.dismiss();
    }

    saveMedicine(){

        let self = this;
        if(!_.isUndefined(this.data.salt) && !_.isUndefined(this.data.color) && !_.isUndefined(this.data.dosage) && !_.isUndefined(this.data.doctor) && !_.isUndefined(this.data.date)){
            this.medicines.push(this.data);

        }else{
           this.presentAlert('Please enter all required details');
        }

    }

    presentAlert(message) {
        let alert = this.alertCtrl.create({
            subTitle: message,
            buttons: ['Dismiss']
        });
        alert.present();
    }


    updateMedicine(){

        let self = this;
        if(!_.isUndefined(this.data.salt) && !_.isUndefined(this.data.color) && !_.isUndefined(this.data.dosage) && !_.isUndefined(this.data.doctor) && !_.isUndefined(this.data.date)){
        }else{
            this.presentAlert('Please enter all required details');
        }

    }



}



