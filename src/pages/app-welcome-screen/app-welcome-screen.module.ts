import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppWelcomeScreenPage} from "./app-welcome-screen";


@NgModule({
  declarations: [
      AppWelcomeScreenPage,
  ],
  imports: [
    IonicPageModule.forChild(AppWelcomeScreenPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppWelcomeScreenModule {}
