import { Component, Input } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { AppSharedService } from "../../services/app-shared-service";
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
    selector: 'app-welcome-screen',
    templateUrl: 'app-welcome-screen.html',
    providers: [DatabaseService]
})
export class AppWelcomeScreenPage {

    constructor(public storage: Storage, public navCtrl: NavController, navParams: NavParams, private alertCtrl: AlertController, private databaseService: DatabaseService, private loadingService: LoadingService, public appSharedService: AppSharedService, public menu: MenuController) {
        // if (this.storage.get('remember')) {
        //     console.log("userfsdf")
        //     this.navCtrl.setRoot("ApptimelinePage");
        // }
        // else{
        //     console.log("userfsdfsdfsfsdf")
        // }
        this.storage.get('userLogin').then((val) => {
            if (val) {
                this.navCtrl.setRoot("AppTimelinePage");
            }
        });
    }

    public signUp() {
        this.navCtrl.setRoot("AppRegisterPage");
    }

    public signIn() {
        this.navCtrl.setRoot("AppLoginPage");
    }

    /*ionViewDidLoad() {
        this.menu.swipeEnable(false);
    }*/

    ionViewDidEnter() {
        this.menu.swipeEnable(false);
    }

    ionViewWillLeave() {
        this.menu.swipeEnable(true);
    }
}
