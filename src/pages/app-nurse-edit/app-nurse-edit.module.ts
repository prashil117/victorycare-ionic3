import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppNurseEditPage } from './app-nurse-edit';

@NgModule({
  declarations: [
    AppNurseEditPage,
  ],
  imports: [
    IonicPageModule.forChild(AppNurseEditPage),
  ],
})
export class AppNurseEditPageModule {}
