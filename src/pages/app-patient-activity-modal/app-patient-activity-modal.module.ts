import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppPatientActivityModalPage} from "./app-patient-activity-modal";


@NgModule({
  declarations: [
      AppPatientActivityModalPage
  ],
  imports: [
    IonicPageModule.forChild(AppPatientActivityModalPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppPatientActivityModalModule {}
