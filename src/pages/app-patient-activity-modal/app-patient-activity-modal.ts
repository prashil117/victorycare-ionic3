import { Component, AfterViewInit } from "@angular/core";
import {
  AlertController,
  IonicPage,
  ModalController,
  NavController,
  NavParams,
  Platform,
  ViewController,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from "rxjs/Observable";
import * as _ from "lodash";
import { AppSharedService } from "../../services/app-shared-service";
import { Storage } from "@ionic/storage";
import { Http } from "@angular/http";
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
import { Validators, FormGroup, FormBuilder, FormArray } from "@angular/forms";

@IonicPage()
@Component({
  selector: "app-patient-activity-modal",
  templateUrl: "app-patient-activity-modal.html",
  providers: [DatabaseService],
})
export class AppPatientActivityModalPage implements AfterViewInit {
  //    colors: Array<string> = ['#d435a2', '#a834bf', '#6011cf', '#0d0e81', '#0237f1', '#0d8bcd', '#16aca4', '#3c887e', '#157145', '#57a773', '#88aa3d', '#b7990d', '#fcbf55', '#ff8668', '#ff5c6a', '#c2454c', '#c2183f', '#d8226b', '#8f2d56', '#482971', '#000000', '#561f37', '#433835', '#797979', '#819595'];
  colors: Array<string> = [
    "#D32F2F",
    "#7B1FA2",
    "#512DA8",
    "#00796B",
    "#388E3C",
    "#AFB42B",
    "#FBC02D",
    "#FFA000",
    "#F57C00",
    "#455A64",
    "#FFFFFF",
  ];

  data: any = {};
  Pagetitle: string;
  // colors:any = [];
  activities: any = [];
  patientData: any = [];
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  eventType: any = {};
  itemIndex: any = {};
  Activitymaster: any;
  date: any = new Date();
  hcpid: any;
  hcpWebServiceData: any = {};
  activityList: any = [];
  patientSession: boolean;
  presAddedBy: string;
  disable: boolean = false;
  submitAttempt: boolean;
  newActivityData: any;
  newActivityName: string;
  type: string;
  hcpkey: any;
  userLoginData: any;
  patientType: any;
  rec: any;
  tim: any = [];
  sel: boolean = true;
  tog: boolean = false;
  EditFormGroup: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private databaseService: DatabaseService,
    private loadingService: LoadingService,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public appSharedService: AppSharedService,
    public storage: Storage,
    public http: Http,
    public formBuilder: FormBuilder,
    private toastCtrl: ToastService,
  ) {

    this.EditFormGroup = formBuilder.group({
      activity: ["", Validators.required],
      recurringDaily: [],
      enableStandardWheel: [],
      prestartdate: ["", Validators.required],
      preenddate: ["", Validators.required],
      tim: this.formBuilder.array([
        this.initDiagnosis(),
      ]),
    });
    console.log("this.", this.navParams.data)
    if (this.navParams.data.activity) {
      this.data.color = this.navParams.data.activity.color;
    } else {
      this.data.color = "#D32F2F";
    }
    if (this.navParams.data.activity) {
      if (this.navParams.data.activity.selecttime) {
        this.sel = false;
        this.tog = true;
      }
      else {
        this.sel = true;
        this.tog = false;
      }
    }
  }

  initDiagnosis() {
    return this.formBuilder.group({
      time: ['']
    });
  }

  addDiagnosis() {
    const control = <FormArray>this.EditFormGroup.controls['tim'];
    control.push(this.initDiagnosis());
    this.tim.push({});
    console.log("abvc", this.tim)
  }

  setInitialDiagnosis() {
    if (this.tim === undefined) {
      this.tim = [];
    }

    if (this.tim.length === 0) {
      this.tim.push({});
    }
  }

  deleteDiagnosis(index) {
    this.tim.splice(index, 1);
    console.log("this.tim", this.tim)
  }


  intializeApp1() {
    if (
      this.userLoginData.usertype == "patient" &&
      this.patientType == "independent"
    ) {
      this.disable = false;
    } else if (
      this.userLoginData.usertype == "doctor" ||
      this.userLoginData.usertype == "nurse" ||
      this.userLoginData.usertype == "healthcareprovider"
    ) {
      this.disable = false;
    } else if (
      this.userLoginData.usertype == "patient" &&
      this.patientType == "dependent"
    ) {
      this.disable = true;
    }
    this.presAddedBy = this.userLoginData.id;
    this.newActivityData = this.navParams.data.newActivityData
      ? this.navParams.data.newActivityData
      : "";
    this.newActivityName = this.navParams.data.ActivityName
      ? this.navParams.data.ActivityName
      : "";
    console.log("this.data111", this.newActivityData);
    console.log("this.data", this.newActivityName);
    if (!_.isEmpty(this.navParams.data)) {
      if (this.navParams.data.type === "edit") {
        this.type = "edit";
        this.storage.get("patientSession").then((val) => {
          if (this.userLoginData.usertype == "patient") {
            //this.disable = true;
            this.data.activity = this.navParams.data.activity
              ? this.navParams.data.activity.activitymasterid
              : "";
            console.log("this.dataadsds", this.data.activity);
          }
          if (val != undefined || this.userLoginData.usertype == "patient") {
            console.log("this.inside");
            if (this.patientType == "dependent") {
              this.patientSession = true;
            }
            if (this.patientType == "independent") {
              if (this.navParams.data.onboard == true) {
                console.log("this.inside1");
                this.patientSession = false;
              } else if (
                !this.navParams.data.onboard ||
                this.navParams.data.onboard == undefined
              ) {
                this.patientSession = true;
              } else {
                this.patientSession = true;
              }
            }
            if (this.data.starttime) {
              var search = this.data.starttime;
              var n = search.search(",");
            }
            if (n != -1) {
              // var dosearr = this.data.dose ? this.data.dose.split(',') : this.data.dose;
              var timearr: any = this.data.starttime ? this.data.starttime.split(',') : this.data.starttime;
              // var tmp = {}
              console.log("timearr", timearr)
              for (var i in timearr) {
                console.log("abcd", timearr[i])
                this.tim.push({ time: timearr[i] });
                console.log("abcdsdfsdf", this.tim)
              }

            }
            else {
              var timearr = this.data.starttime;
              this.tim.push({ time: timearr });
              console.log("this.tim", this.tim)
            }
            console.log("abcdefghijkl", this.tim);
            if (this.tim.length > 0) {
              for (let i = 0; i < this.tim.length; i++) {
                const control = <FormArray>this.EditFormGroup.controls['tim'];
                control.push(this.initDiagnosis());
                console.log("this.tim4", this.tim)
              }
            }
            else {
              console.log("this.tim5", this.tim)
              this.tim = [];
            }


            if (
              this.userLoginData.usertype == "healthcareprovider" ||
              this.userLoginData.usertype == "nurse" ||
              this.userLoginData.usertype == "doctor"
            ) {
              console.log("this.inside123");
              this.patientSession = true;
            }
          }
        });
        this.storage.get("user").then((val) => {
          this.patientData = val ? val : "";
        });
        let self = this;

        this.hcpWebServiceData.action = "getactivitymasterdatafulllist";
        if (
          this.patientType == "independent" ||
          this.userLoginData.usertype == "healthcareprovider"
        ) {
          self.hcpWebServiceData.userid = self.userLoginData.id;
          console.log("this.dastaauserid", self.hcpWebServiceData.userid);
          console.log("hcpwebseriideDta", self.hcpWebServiceData);
          self.hcpWebServiceData.appsecret = this.appsecret;
          self.http.post(this.baseurl + "getdata.php", this.hcpWebServiceData)
            .subscribe((data) => {
              this.loadingService.show();
              let result = JSON.parse(data["_body"]);
              if (result.status == "success") {
                this.loadingService.hide();
                this.activityList = result.data;
                console.log("this.activityList", this.activityList);
                if (this.newActivityName) {
                  console.log("name", this.newActivityName);
                  this.data.activity = this.activityList.find((a) =>
                    a.name === this.newActivityName
                  ).id;
                }
              } else {
                this.loadingService.hide();
                this.toastCtrl.presentToast("Activity dropdown is empty");
              }
            }, (err) => {
              this.loadingService.hide();
              console.log(err);
            });
        } else {
          this.storage.get("currentHCP").then((result) => {
            console.log(result);
            self.hcpWebServiceData.userid = result;
            self.hcpWebServiceData.appsecret = this.appsecret;
            console.log("this.hcpkey ::::", self.hcpWebServiceData.userfid);
            console.log("hcpwebseriideDta", self.hcpWebServiceData);
            self.http.post(this.baseurl + "getdata.php", this.hcpWebServiceData)
              .subscribe((data) => {
                this.loadingService.show();
                let result = JSON.parse(data["_body"]);
                if (result.status == "success") {
                  this.loadingService.hide();
                  this.activityList = result.data;
                  console.log("this.activityList", this.activityList);
                  if (this.newActivityName) {
                    console.log("name", this.newActivityName);
                    this.data.activity = this.activityList.find((a) =>
                      a.name === this.newActivityName
                    ).id;
                  }
                } else {
                  this.loadingService.hide();
                  this.toastCtrl.presentToast("Activity dropdown is empty");
                }
              }, (err) => {
                console.log(err);
                this.loadingService.hide();
              });
          });
        }
        //this.hcpWebServiceData.action = "getactivitymasterdatafulllist";

        this.Pagetitle = "Edit Activity";

        this.data = this.navParams.data.activity
          ? this.navParams.data.activity
          : this.newActivityData;
        this.data.activity = this.data.activitymasterid;
        console.log("this.datadfdfdfd", this.data);
        this.eventType = this.navParams.data.type;
        this.itemIndex = this.navParams.data.index;
        console.log("data:", this.data);
        if (this.navParams.data.activity.addtostandardwheel == "1") {
          this.data.addtostandardwheel = true;
        } else {
          this.data.addtostandardwheel = false;
        }
        if (this.navParams.data.activity.recurringDaily == "1") {
          this.data.recurringDaily = true;
        } else {
          this.data.recurringDaily = false;
        }
        if (this.navParams.data.activity.addtowheel == "1") {
          this.data.addtowheel = true;
        } else {
          this.data.addtowheel = false;
        }
        if (this.navParams.data.activity.notification == "1") {
          this.data.notification = true;
        } else {
          this.data.notification = false;
        }
        // this.patientData = navParams.data.patientData;
        this.Activitymaster = "";

        if (this.data.color) {
          this.data.color = this.data.color;
        } else {
          this.data.color = "#D32F2F";
        }
      } else {
        this.Pagetitle = "Add Activity";
        this.eventType = "";
        this.patientData = this.navParams.data.patientData
          ? this.navParams.data.patientData
          : "";
        this.data.color = "#E26060";
        this.type = "add";
      }
    } else {
      this.data.color = "#D32F2F";
    }

    // this.colors = this.appSharedService.colors;
  }

  getDates(startDate, endDate) {
    var dates = [],
      currentDate = startDate,
      addDays = function (days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
      };
    while (currentDate <= endDate) {
      dates.push(currentDate);
      currentDate = addDays.call(currentDate, 1);
    }
    return dates;
  }

  // for choose color and set in dropdown
  prepareColorSelector() {
    setTimeout(() => {
      let buttonElements = document.querySelectorAll(
        "div.alert-radio-group button",
      );
      if (!buttonElements.length) {
        this.prepareColorSelector();
      } else {
        for (let index = 0; index < buttonElements.length; index++) {
          let buttonElement = buttonElements[index];
          let optionLabelElement = buttonElement.querySelector(
            ".alert-radio-label",
          );
          let color = optionLabelElement.innerHTML.trim();

          if (this.isHexColor(color)) {
            buttonElement.classList.add(
              "colorselect",
              "color_" + color.slice(1, 7),
            );
            if (color == this.data.color) {
              buttonElement.classList.add("colorselected");
            }
          }
        }
      }
    }, 100);
  }

  // for choose color and set in dropdown
  isHexColor(color) {
    let hexColorRegEx = /^#(?:[0-9a-fA-F]{3}){1,2}$/;
    return hexColorRegEx.test(color);
  }

  // for choose color and set in dropdown
  selectColor(color) {
    let buttonElements = document.querySelectorAll(
      "div.alert-radio-group button.colorselect",
    );
    for (let index = 0; index < buttonElements.length; index++) {
      let buttonElement = buttonElements[index];
      buttonElement.classList.remove("colorselected");
      if (buttonElement.classList.contains("color_" + color.slice(1, 7))) {
        buttonElement.classList.add("colorselected");
      }
    }
  }

  // for choose color and set in dropdown
  setColor(color) {
    console.log("Selected Color is", color);
  }

  closeModal() {
    this.viewCtrl.dismiss(false);
  }

  saveActivity() {
    if (
      this.patientType == "independent" ||
      this.userLoginData.usertype == "healthcareprovider"
    ) {
      this.data.userid = this.userLoginData.id;
    } else {
      this.data.userid = this.hcpkey;
    }
    if (this.data.addtowheel == true) {
      this.data.addtowheel = "1";
    } else {
      this.data.addtowheel = "0";
    }
    if (this.data.notification == true) {
      this.data.notification = "1";
    } else {
      this.data.notification = "0";
    }
    if (!this.tog) {
      this.data.selecttime = null;
    }
    console.log(this.data);
    let self = this;
    if (
      !_.isUndefined(this.data.name) && !_.isUndefined(this.data.description)
    ) {
      self.data.action = "addactivity";
      self.data.appsecret = this.appsecret;
      console.log(self.data);
      self.http.post(this.baseurl + "adddata.php", self.data).subscribe(
        (data) => {
          console.log(data);
          let result = JSON.parse(data["_body"]);
          console.log(result.status);
          if (result.status == "success") {
            self.toastCtrl.presentToast("Activity is added successfully");
            this.viewCtrl.dismiss(true);
          } else {
            self.toastCtrl.presentToast("There is an error saving data!");
            this.viewCtrl.dismiss(false);
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
          }
        },
        (err) => {
          console.log(err);
        },
      );
    } else {
      this.presentAlert("Please enter all required details");
    }
  }

  getDate() {
    let date = new Date(this.date);
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" +
      date.getDate();
    // return date.getDate();
  }

  presentAlert(message) {
    let alert = this.alertCtrl.create({
      subTitle: message,
      buttons: ["Dismiss"],
    });
    alert.present();
  }

  updateActivity(patientData, index) {
    if (this.userLoginData.usertype == "healthcareprovider") {
      this.data.userid = this.userLoginData.id;
    } else {
      this.data.userid = this.hcpid;
    }
    console.log(this.data);
    console.log(patientData);
    console.log(index);
    let self = this;
    if (this.data.addtowheel == true) {
      this.data.addtowheel = "1";
    } else {
      this.data.addtowheel = "0";
    }
    if (this.data.notification == true) {
      this.data.notification = "1";
    } else {
      this.data.notification = "0";
    }
    if (!this.tog) {
      this.data.selecttime = null;
    }
    if (
      !_.isUndefined(this.data.name) && !_.isUndefined(this.data.description)
    ) {
      self.data.action = "updateactivity";
      console.log(self.data);
      self.data.appsecret = this.appsecret;
      self.http.post(this.baseurl + "updatedata.php", self.data).subscribe(
        (data) => {
          console.log(data);
          let result = JSON.parse(data["_body"]);
          console.log(result.status);
          if (result.status == "success") {
            self.toastCtrl.presentToast("Activity is updated successfully");
            if (this.patientType == "independent") {
              this.viewCtrl.dismiss(true);
            } else {
              this.viewCtrl.dismiss(true);
            }
          } else {
            self.toastCtrl.presentToast("There is an error saving data!");
            this.viewCtrl.dismiss(false);
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
          }
        },
        (err) => {
          console.log(err);
        },
      );
    } else {
      this.presentAlert("Please enter all required details");
    }
  }

  updatePatientsActivity(patientData, userid) {
    this.submitAttempt = true;
    let self = this;
    this.data.addedBy = this.presAddedBy;
    // if (this._cookieService.get('usertype') == "patient") {
    //   this.data.activitymasterid = this.data.activity;
    // }
    //this.data.userid = activityid;
    if (this.data.addtostandardwheel == true) {
      this.data.addtostandardwheel = "1";
    } else {
      this.data.addtostandardwheel = "0";
    }
    if (this.data.recurringDaily == true) {
      this.data.recurringDaily = "1";
    } else {
      this.data.recurringDaily = "0";
    }
    var time = Array.prototype.map.call(this.tim, s => s.time).toString();
    this.data.starttime = time;
    console.log("this.acticyit", this.data.activity);
    this.data.activitymasterid = this.data.activity;
    this.data.activityid = this.data.activity;
    if (this.patientType == "independent") {
      self.data.uid = this.userLoginData.id;
    } else {
      self.data.uid = userid;
    }
    console.log("this.data", this.data);
    self.data.action = "updatepatientsactivity";
    self.data.appsecret = this.appsecret;
    self.http.post(this.baseurl + "updatedata.php", self.data).subscribe(
      (data) => {
        console.log(data);
        let result = JSON.parse(data["_body"]);
        console.log(result.status);
        if (result.status == "success") {
          self.toastCtrl.presentToast("Medicine is updated successfully");
          this.viewCtrl.dismiss(true);
        } else {
          this.viewCtrl.dismiss(false);
          self.toastCtrl.presentToast("There is an error saving data!");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
        }
      },
      (err) => {
        console.log(err);
      },
    );
  }

  addTime() {
    console.log(this.data.date);
    // var newdate = this.data.date;
    this.data.date.push(this.data.date);
  }

  onChange(e) {
    if (e.value) {
      this.sel = false;
    } else {
      this.sel = true;
      this.data.selecttime = null;
    }
  }

  deleteTime(index) {
    this.data.date.splice(index, 1);
  }

  setInitialTime() {
    if (this.data.date === undefined) {
      this.data.date = [];
    }

    if (this.data.date.length === 0) {
      this.data.date.push({});
    }
  }

  ngAfterViewInit(): void {
    this.setInitialTime();
    this.getHCPid();
  }

  onChangeAddnewActivity(e) {
    console.log("event", e);
    // if (e === "other") {
    //   this.navCtrl.push("AppPatientsActivityMasterModalPage", { 'patientAction': 'edit', 'activitydata': this.data });
    //   this.data.activity = this.activityList.find(e => e.id === this.data.activitymasterid).id;
    //   console.log("this.data", this.data)
    // }
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }

  async getHCPid() {
    await this.get("currentHCP").then((result) => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      } else {
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.hcpid = this.hcpid ? this.hcpid : this.userLoginData.id;
        this.getpatientType();
      }
    });
  }
  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get("patientType").then((result) => {
      console.log("patienttypesdfdsfsdsdf", result);
      if (
        (!result || result == null || result == undefined) &&
        this.userLoginData == "patient"
      ) {
        this.doRefresh("");
      } else {
        this.patientType = result;
        this.intializeApp1();
        console.log("sekeccdffvfdgdfgdfg", this.patientType);
      }
    });
  }
  doRefresh(event) {
    console.log("Begin async operation");

    setTimeout(() => {
      console.log("Async operation has ended");
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }
}
