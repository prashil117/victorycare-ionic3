import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {AppLoggedMedicinesTabPage} from "./app-logged-medicines-tab";




@NgModule({
  declarations: [
      AppLoggedMedicinesTabPage
  ],
  imports: [
    IonicPageModule.forChild(AppLoggedMedicinesTabPage)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})

export class AppLoggedMedicinesTabModule {}
