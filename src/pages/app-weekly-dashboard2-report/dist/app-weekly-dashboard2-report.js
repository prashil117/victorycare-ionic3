"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppWeeklyDashboard2ReportPage = void 0;
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var chart_js_1 = require("chart.js");
var environment_1 = require("../../environment/environment");
var moment = require("moment");
var option = {
    legend: {
        display: false
    },
    scales: {
        xAxes: [{
                gridLines: {
                    display: false
                },
                categoryPercentage: 1.5
            }],
        yAxes: [{
                display: false,
                gridLines: {
                    display: false,
                    drawBorder: false
                }
            }]
    }
};
var dougnhutOption = {
    responsive: true,
    aspectRatio: 1,
    maintainAspectRatio: true,
    legend: {
        display: true,
        position: 'bottom',
        labels: {
            usePointStyle: true
        },
        centertext: 1
    }
};
var dougnhutOption1 = {
    responsive: true,
    aspectRatio: 1,
    maintainAspectRatio: true,
    legend: {
        display: false,
        position: 'bottom',
        labels: {
            usePointStyle: true
        }
    }
};
var AppWeeklyDashboard2ReportPage = /** @class */ (function () {
    function AppWeeklyDashboard2ReportPage(toastService, loadingService, http, navCtrl, navParams, storage) {
        this.toastService = toastService;
        this.loadingService = loadingService;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.baseurl = environment_1.environment.apiUrl;
        this.appsecret = environment_1.environment.appsecret;
        this.startOfWeek = moment(moment().startOf('week').format('YYYY/MM/DD'));
        this.AsleepBarChartArray = [];
        this.AwakeBarChartArray = [];
        this.PainBarChartArray = [];
        this.UncomfortBarChartArray = [];
        this.AwakeArray = [];
        this.AsleepArray = [];
        this.PainArray = [];
        this.UncomfortArray = [];
        this.weeklyHour = 168;
        this.data = {};
        this.tabs = [];
        this.data.tabs = [
            { page: "AppWeeklyDashboard2ReportPage", title: "Today" },
            { page: "AppDashboardPage", title: "Week" }
        ];
    }
    AppWeeklyDashboard2ReportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppWeeklyDashboard2ReportPage');
        this.SleepDoughnut(25);
        this.DistressDoughnut(30);
        this.UnomfortableDoughnut(50);
        this.ComfortableDoughnut(10);
        this.SleepDoughnut1(5);
        this.DistressDoughnut1(20);
        this.UnomfortableDoughnut1(40);
        this.ComfortableDoughnut1(50);
        this.SleepChart();
        this.DistressChart();
        this.ComfortableChart();
        this.UncomfortableChart();
    };
    AppWeeklyDashboard2ReportPage.prototype.SleepDoughnut = function (shour) {
        var data = [this.weeklyHour - shour, shour];
        console.log("dta", data);
        this.chart = new chart_js_1.Chart(this.chartRef.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Sleep'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(83,155,192)',
                            'rgb(107,177,214)'
                        ]
                    }]
            },
            options: dougnhutOption
        });
        this.chart1 = new chart_js_1.Chart(this.doughnut1.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Sleep'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(83,155,192)',
                            'rgb(107,177,214)'
                        ]
                    }]
            },
            options: dougnhutOption1
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.SleepDoughnut1 = function (shour) {
        var data = [this.weeklyHour - shour, shour];
        console.log("dta", data);
        this.chart = new chart_js_1.Chart(this.chartRef4.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Sleep'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(83,155,192)',
                            'rgb(107,177,214)'
                        ]
                    }]
            },
            options: dougnhutOption
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.DistressDoughnut = function (Dhour) {
        var data = [this.weeklyHour - Dhour, Dhour];
        this.chart = new chart_js_1.Chart(this.chartRef1.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Distress'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(226,96,96)',
                            'rgb(171,87,87)'
                        ]
                    }]
            },
            options: dougnhutOption
        });
        this.chart1 = new chart_js_1.Chart(this.doughnut2.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Distress'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(226,96,96)',
                            'rgb(171,87,87)'
                        ]
                    }]
            },
            options: dougnhutOption1
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.DistressDoughnut1 = function (Dhour) {
        var data = [this.weeklyHour - Dhour, Dhour];
        this.chart = new chart_js_1.Chart(this.chartRef5.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Distress'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(226,96,96)',
                            'rgb(171,87,87)'
                        ]
                    }]
            },
            options: dougnhutOption
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.UnomfortableDoughnut = function (Uhour) {
        var data = [this.weeklyHour - Uhour, Uhour];
        this.chart = new chart_js_1.Chart(this.chartRef2.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Awake'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(250,208,0)',
                            'rgb(201,169,13)'
                        ]
                    }]
            },
            options: dougnhutOption
        });
        this.chart1 = new chart_js_1.Chart(this.doughnut3.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Awake'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(250,208,0)',
                            'rgb(201,169,13)'
                        ]
                    }]
            },
            options: dougnhutOption1
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.UnomfortableDoughnut1 = function (Uhour) {
        var data = [this.weeklyHour - Uhour, Uhour];
        this.chart = new chart_js_1.Chart(this.chartRef6.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Awake'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(250,208,0)',
                            'rgb(201,169,13)'
                        ]
                    }]
            },
            options: dougnhutOption
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.ComfortableDoughnut = function (Chour) {
        var data = [this.weeklyHour - Chour, Chour];
        this.chart = new chart_js_1.Chart(this.chartRef3.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Daily'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(175,219,7)',
                            'rgb(151,187,12)'
                        ]
                    }]
            },
            options: dougnhutOption
        });
        this.chart1 = new chart_js_1.Chart(this.doughnut4.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Daily'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(175,219,7)',
                            'rgb(151,187,12)'
                        ]
                    }]
            },
            options: dougnhutOption1
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.ComfortableDoughnut1 = function (Chour) {
        var data = [this.weeklyHour - Chour, Chour];
        this.chart = new chart_js_1.Chart(this.chartRef7.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ['Daily'],
                datasets: [{
                        data: data,
                        borderColor: 'transparent',
                        backgroundColor: [
                            'rgb(175,219,7)',
                            'rgb(151,187,12)'
                        ]
                    }]
            },
            options: dougnhutOption
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.SleepChart = function () {
        this.AsleepBarChartArray = [9, 8, 7, 5, 8, 9, 6];
        this.barchart = new chart_js_1.Chart(this.barchartRef.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
                datasets: [{
                        // barThickness: 16,
                        // offsetGridLines: 0,
                        barPercentage: 0.5,
                        pointBorderColor: 'red',
                        data: this.AsleepBarChartArray,
                        backgroundColor: [
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                        ],
                        borderColor: [
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                            'rgb(107,177,214)',
                        ],
                        borderWidth: 1
                    }, {
                        data: this.AsleepBarChartArray,
                        backgroundColor: [
                            'rgb(107,177,214)',
                        ],
                        borderColor: [
                            'rgb(107,177,214)',
                        ],
                        type: 'line',
                        fill: false
                    }
                ]
            },
            options: option
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.DistressChart = function () {
        this.PainBarChartArray = [5, 6, 4, 8, 1, 3, 9];
        this.barchart = new chart_js_1.Chart(this.barchartRef1.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
                datasets: [{
                        barPercentage: 0.5,
                        data: this.PainBarChartArray,
                        backgroundColor: [
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                        ],
                        borderColor: [
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                            'rgb(171,87,87)',
                        ],
                        borderWidth: 1
                    }, {
                        data: this.PainBarChartArray,
                        backgroundColor: [
                            'rgb(171,87,87)',
                        ],
                        borderColor: [
                            'rgb(171,87,87)',
                        ],
                        type: 'line',
                        fill: false
                    }]
            },
            options: option
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.ComfortableChart = function () {
        this.AwakeBarChartArray = [10, 8, 7, 5, 8, 9, 6];
        this.barchart = new chart_js_1.Chart(this.barchartRef2.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
                datasets: [{
                        barPercentage: 0.5,
                        data: this.AwakeBarChartArray,
                        backgroundColor: [
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                        ],
                        borderColor: [
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                            'rgb(201,169,13)',
                        ],
                        borderWidth: 1
                    },
                    {
                        data: this.AwakeBarChartArray,
                        backgroundColor: [
                            'rgb(201,169,13)',
                        ],
                        borderColor: [
                            'rgb(201,169,13)',
                        ],
                        type: 'line',
                        fill: false
                    }]
            },
            options: option
        });
    };
    AppWeeklyDashboard2ReportPage.prototype.UncomfortableChart = function () {
        this.UncomfortBarChartArray = [2, 3, 4, 8, 5, 4, 2];
        this.barchart = new chart_js_1.Chart(this.barchartRef3.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
                datasets: [{
                        barPercentage: 0.5,
                        data: this.UncomfortBarChartArray,
                        backgroundColor: [
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                        ],
                        borderColor: [
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                            'rgb(175,219,7)',
                        ],
                        borderWidth: 1
                    },
                    {
                        data: this.UncomfortBarChartArray,
                        backgroundColor: [
                            'rgb(175,219,7)',
                        ],
                        borderColor: [
                            'rgb(175,219,7)',
                        ],
                        type: 'line',
                        fill: false
                    }
                ]
            },
            options: option
        });
    };
    __decorate([
        core_1.ViewChild("tabs")
    ], AppWeeklyDashboard2ReportPage.prototype, "TimelineTabs");
    __decorate([
        core_1.ViewChild('lineChart')
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef");
    __decorate([
        core_1.ViewChild('barChart')
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef");
    __decorate([
        core_1.ViewChild('lineChart1')
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef1");
    __decorate([
        core_1.ViewChild('barChart1')
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef1");
    __decorate([
        core_1.ViewChild('lineChart2')
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef2");
    __decorate([
        core_1.ViewChild('barChart2')
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef2");
    __decorate([
        core_1.ViewChild('lineChart3')
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef3");
    __decorate([
        core_1.ViewChild('barChart3')
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef3");
    __decorate([
        core_1.ViewChild('lineChart4')
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef4");
    __decorate([
        core_1.ViewChild('barChart4')
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef4");
    __decorate([
        core_1.ViewChild('lineChart5')
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef5");
    __decorate([
        core_1.ViewChild('barChart5')
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef5");
    __decorate([
        core_1.ViewChild('lineChart6')
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef6");
    __decorate([
        core_1.ViewChild('barChart6')
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef6");
    __decorate([
        core_1.ViewChild('lineChart7')
    ], AppWeeklyDashboard2ReportPage.prototype, "chartRef7");
    __decorate([
        core_1.ViewChild('barChart7')
    ], AppWeeklyDashboard2ReportPage.prototype, "barchartRef7");
    __decorate([
        core_1.ViewChild('doughnut1')
    ], AppWeeklyDashboard2ReportPage.prototype, "doughnut1");
    __decorate([
        core_1.ViewChild('doughnut2')
    ], AppWeeklyDashboard2ReportPage.prototype, "doughnut2");
    __decorate([
        core_1.ViewChild('doughnut3')
    ], AppWeeklyDashboard2ReportPage.prototype, "doughnut3");
    __decorate([
        core_1.ViewChild('doughnut4')
    ], AppWeeklyDashboard2ReportPage.prototype, "doughnut4");
    AppWeeklyDashboard2ReportPage = __decorate([
        ionic_angular_1.IonicPage(),
        core_1.Component({
            selector: 'page-app-weekly-dashboard2-report',
            templateUrl: 'app-weekly-dashboard2-report.html'
        })
    ], AppWeeklyDashboard2ReportPage);
    return AppWeeklyDashboard2ReportPage;
}());
exports.AppWeeklyDashboard2ReportPage = AppWeeklyDashboard2ReportPage;
