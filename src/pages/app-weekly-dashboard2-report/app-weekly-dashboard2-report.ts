import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs } from 'ionic-angular';
import { Chart } from 'chart.js';
import { Storage } from "@ionic/storage";
import { environment } from '../../environment/environment';
import { Http } from '@angular/http';
import { LoadingService } from '../../services/loading-service';
import { ToastService } from '../../services/toast-service';
import * as moment from 'moment';
const option = {
  legend: {
    display: false
  },
  scales: {
    xAxes: [{
      gridLines: {
        display: false
      },
      categoryPercentage: 1.5,
    }],
    yAxes: [{
      display: false,
      gridLines: {
        display: false,
        drawBorder: false,
      }
    }],
  },
}
const dougnhutOption: any = {
  responsive: true,
  aspectRatio: 1,
  maintainAspectRatio: true,
  legend: {
    display: true,
    position: 'bottom',
    labels: {
      usePointStyle: true,

    },
    centertext: 1
  }
}

const dougnhutOption1: any = {
  responsive: true,
  aspectRatio: 1,
  maintainAspectRatio: true,
  legend: {
    display: false,
    position: 'bottom',
    labels: {
      usePointStyle: true,

    }
  }
}


@IonicPage()
@Component({
  selector: 'page-app-weekly-dashboard2-report',
  templateUrl: 'app-weekly-dashboard2-report.html',
})
export class AppWeeklyDashboard2ReportPage {
  @ViewChild("tabs") TimelineTabs: Tabs;
  @ViewChild('lineChart') private chartRef: ElementRef;
  @ViewChild('barChart') private barchartRef: ElementRef;
  @ViewChild('lineChart1') private chartRef1: ElementRef;
  @ViewChild('barChart1') private barchartRef1: ElementRef;
  @ViewChild('lineChart2') private chartRef2: ElementRef;
  @ViewChild('barChart2') private barchartRef2: ElementRef;
  @ViewChild('lineChart3') private chartRef3: ElementRef;
  @ViewChild('barChart3') private barchartRef3: ElementRef;

  @ViewChild('lineChart4') private chartRef4: ElementRef;
  @ViewChild('barChart4') private barchartRef4: ElementRef;
  @ViewChild('lineChart5') private chartRef5: ElementRef;
  @ViewChild('barChart5') private barchartRef5: ElementRef;
  @ViewChild('lineChart6') private chartRef6: ElementRef;
  @ViewChild('barChart6') private barchartRef6: ElementRef;
  @ViewChild('lineChart7') private chartRef7: ElementRef;
  @ViewChild('barChart7') private barchartRef7: ElementRef;
  @ViewChild('doughnut1') private doughnut1: ElementRef;
  @ViewChild('doughnut2') private doughnut2: ElementRef;
  @ViewChild('doughnut3') private doughnut3: ElementRef;
  @ViewChild('doughnut4') private doughnut4: ElementRef;

  chart: any;
  chart1: any;
  barchart: any;
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  userLoginData: any;
  dependent: any;
  TotalSleep: any;
  TotalDistress: any;
  TotalComfort: any;
  dateArray: any;
  TotalUncomfort: any;
  SleepHours: any;
  DistressHours: any;
  ComfortHours: any;
  startOfWeek = moment(moment().startOf('week').format('YYYY/MM/DD'));
  UncomfortHours: any;
  AsleepBarChartArray: any = [];
  AwakeBarChartArray: any = [];
  PainBarChartArray: any = [];
  UncomfortBarChartArray: any = [];
  AwakeArray: any = [];
  AsleepArray: any = [];
  PainArray: any = [];
  UncomfortArray: any = [];
  weeklyHour = 168;
  data: any = {};
  tabs: any = [];

  constructor(private toastService: ToastService, private loadingService: LoadingService, public http: Http, public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
    this.data.tabs = [
      { page: "AppWeeklyDashboard2ReportPage", title: "Today" },
      { page: "AppDashboardPage", title: "Week" }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppWeeklyDashboard2ReportPage');
    this.SleepDoughnut(25);
    this.DistressDoughnut(30);
    this.UnomfortableDoughnut(50);
    this.ComfortableDoughnut(10);

    this.SleepDoughnut1(5);
    this.DistressDoughnut1(20);
    this.UnomfortableDoughnut1(40);
    this.ComfortableDoughnut1(50);

    this.SleepChart();
    this.DistressChart();
    this.ComfortableChart();
    this.UncomfortableChart();
  }

  SleepDoughnut(shour) {
    var data = [this.weeklyHour - shour, shour];
    console.log("dta", data);
    this.chart = new Chart(this.chartRef.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Sleep'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(83,155,192)',
            'rgb(107,177,214)'

          ],
        }]
      },
      options: dougnhutOption
    });
    this.chart1 = new Chart(this.doughnut1.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Sleep'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(83,155,192)',
            'rgb(107,177,214)'

          ],
        }]
      },
      options: dougnhutOption1
    });
  }
  SleepDoughnut1(shour) {
    var data = [this.weeklyHour - shour, shour];
    console.log("dta", data);
    this.chart = new Chart(this.chartRef4.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Sleep'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(83,155,192)',
            'rgb(107,177,214)'

          ],
        }]
      },
      options: dougnhutOption
    });
  }
  DistressDoughnut(Dhour) {
    var data = [this.weeklyHour - Dhour, Dhour]
    this.chart = new Chart(this.chartRef1.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Distress'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(226,96,96)',
            'rgb(171,87,87)'

          ],
        }]
      },
      options: dougnhutOption
    });
    this.chart1 = new Chart(this.doughnut2.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Distress'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(226,96,96)',
            'rgb(171,87,87)'

          ],
        }]
      },
      options: dougnhutOption1
    });
  }
  DistressDoughnut1(Dhour) {
    var data = [this.weeklyHour - Dhour, Dhour]
    this.chart = new Chart(this.chartRef5.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Distress'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(226,96,96)',
            'rgb(171,87,87)'

          ],
        }]
      },
      options: dougnhutOption
    });
  }
  UnomfortableDoughnut(Uhour) {
    var data = [this.weeklyHour - Uhour, Uhour]
    this.chart = new Chart(this.chartRef2.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Awake'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(250,208,0)',
            'rgb(201,169,13)'
          ],
        }]
      },
      options: dougnhutOption
    });
    this.chart1 = new Chart(this.doughnut3.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Awake'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(250,208,0)',
            'rgb(201,169,13)'
          ],
        }]
      },
      options: dougnhutOption1
    });
  }
  UnomfortableDoughnut1(Uhour) {
    var data = [this.weeklyHour - Uhour, Uhour]
    this.chart = new Chart(this.chartRef6.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Awake'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(250,208,0)',
            'rgb(201,169,13)'
          ],
        }]
      },
      options: dougnhutOption
    });
  }
  ComfortableDoughnut(Chour) {
    var data = [this.weeklyHour - Chour, Chour]
    this.chart = new Chart(this.chartRef3.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Daily'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(175,219,7)',
            'rgb(151,187,12)'


          ],
        }]
      },
      options: dougnhutOption
    });
    this.chart1 = new Chart(this.doughnut4.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Daily'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(175,219,7)',
            'rgb(151,187,12)'


          ],
        }]
      },
      options: dougnhutOption1
    });
  }
  ComfortableDoughnut1(Chour) {
    var data = [this.weeklyHour - Chour, Chour]
    this.chart = new Chart(this.chartRef7.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['Daily'],
        datasets: [{
          data: data,
          borderColor: 'transparent',
          backgroundColor: [
            'rgb(175,219,7)',
            'rgb(151,187,12)'
          ],
        }]
      },
      options: dougnhutOption
    });
  }

  SleepChart() {

    this.AsleepBarChartArray = [9, 8, 7, 5, 8, 9, 6];

    this.barchart = new Chart(this.barchartRef.nativeElement, {
      type: 'bar',
      data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
        datasets: [{
          // barThickness: 16,
          // offsetGridLines: 0,
          barPercentage: 0.5,
          pointBorderColor: 'red',

          data: this.AsleepBarChartArray,
          backgroundColor: [
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
          ],
          borderColor: [
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
            'rgb(107,177,214)',
          ],
          borderWidth: 1,

        }, {
          data: this.AsleepBarChartArray,
          backgroundColor: [
            'rgb(107,177,214)',
          ],
          borderColor: [
            'rgb(107,177,214)',
          ],
          type: 'line',
          fill: false
        }

        ]
      },
      options: option
    });
  }
  DistressChart() {
    this.PainBarChartArray = [5, 6, 4, 8, 1, 3, 9];
    this.barchart = new Chart(this.barchartRef1.nativeElement, {
      type: 'bar',
      data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
        datasets: [{
          barPercentage: 0.5,
          data: this.PainBarChartArray,
          backgroundColor: [
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
          ],
          borderColor: [
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
            'rgb(171,87,87)',
          ],
          borderWidth: 1
        }, {
          data: this.PainBarChartArray,
          backgroundColor: [
            'rgb(171,87,87)',
          ],
          borderColor: [
            'rgb(171,87,87)',
          ],
          type: 'line',
          fill: false
        }]
      },
      options: option
    });
  }
  ComfortableChart() {
    this.AwakeBarChartArray = [10, 8, 7, 5, 8, 9, 6];
    this.barchart = new Chart(this.barchartRef2.nativeElement, {
      type: 'bar',
      data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
        datasets: [{
          barPercentage: 0.5,
          data: this.AwakeBarChartArray,
          backgroundColor: [
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
          ],
          borderColor: [
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
            'rgb(201,169,13)',
          ],
          borderWidth: 1
        },
        {
          data: this.AwakeBarChartArray,
          backgroundColor: [
            'rgb(201,169,13)',
          ],
          borderColor: [
            'rgb(201,169,13)',
          ],
          type: 'line',
          fill: false
        }]
      },
      options: option
    });
  }
  UncomfortableChart() {
    this.UncomfortBarChartArray = [2, 3, 4, 8, 5, 4, 2];
    this.barchart = new Chart(this.barchartRef3.nativeElement, {
      type: 'bar',
      data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
        datasets: [{
          barPercentage: 0.5,
          data: this.UncomfortBarChartArray,
          backgroundColor: [
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',

          ],
          borderColor: [
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
            'rgb(175,219,7)',
          ],
          borderWidth: 1
        },
        {
          data: this.UncomfortBarChartArray,
          backgroundColor: [
            'rgb(175,219,7)',
          ],
          borderColor: [
            'rgb(175,219,7)',
          ],
          type: 'line',
          fill: false
        }
        ]
      },
      options: option
    });
  }

}
