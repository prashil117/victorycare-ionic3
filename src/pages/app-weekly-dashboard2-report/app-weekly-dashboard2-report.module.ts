import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppWeeklyDashboard2ReportPage } from './app-weekly-dashboard2-report';

@NgModule({
  declarations: [
    AppWeeklyDashboard2ReportPage,
  ],
  imports: [
    IonicPageModule.forChild(AppWeeklyDashboard2ReportPage),
  ],
})
export class AppWeeklyDashboard2ReportPageModule {}
