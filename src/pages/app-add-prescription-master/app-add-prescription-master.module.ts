import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppAddPrescriptionMasterPage } from './app-add-prescription-master';

@NgModule({
  declarations: [
    AppAddPrescriptionMasterPage,
  ],
  imports: [
    IonicPageModule.forChild(AppAddPrescriptionMasterPage),
  ],
})
export class AppAddPrescriptionMasterPageModule {}
