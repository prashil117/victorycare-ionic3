import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { AlertController, IonicPage, ModalController, NavController, NavParams } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import * as _ from "lodash";
import { Validators, FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { EmailValidator } from '../../app/directive/email-validator';
import { AppAddContactModalPage } from "../app-add-contact-modal/app-add-contact-modal";
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";

/**
 * Generated class for the AppHealthcareproviderEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-healthcareprovider-edit',
  templateUrl: 'app-healthcareprovider-edit.html',
  providers: [Camera, DatabaseService]
})
export class AppHealthcareproviderEditPage {
  Profileform: FormGroup;
  submitAttempt: boolean;
  data: any = {};
  // diagnosiss:any = {};
  upload: any = {};
  logo: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  action: any = "";
  image: any = "";
  data1: any = {};
  readonly: any = {};
  date: any = new Date();
  myRand: string;
  addexist: string;
  enableDiv: any = false;
  showSaveButton: any = false;
  usertype: any = [];
  hcpdata: any = [];
  doctorsData: any = [];
  hcp: any = [];
  hcpid: any;
  userLoginData: any;
  patientType: any;
  enableDropdown: boolean;
  result: any = [];
  data2: any = [];

  @ViewChild('imageUploader') imageUploader: ElementRef;
  @ViewChild('addPhotoCaption') addPhotoCaption: ElementRef;
  @ViewChild('removePhotoIcon') removePhotoIcon: ElementRef;
  @ViewChild('previewImage') previewImage: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public formBuilder: FormBuilder, private nativeGeocoder: NativeGeocoder, public modalCtrl: ModalController, private alertCtrl: AlertController, private databaseService: DatabaseService, private loadingService: LoadingService, private camera: Camera, public http: Http, private toastCtrl: ToastService) {
    this.upload = "assets/images/upload1.svg";
    this.readonly = false;
    this.Profileform = formBuilder.group({
      selectType: [''],
      doctorsEmail: [''],
      storedDoctors: [''],
      name: ['', Validators.compose([Validators.maxLength(15), Validators.minLength(3), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid, Validators.pattern('^[a-z0-9_.+-]+@[a-z0-9-]+.[a-z0-9-.]+$')])],//, Validators.compose([Validators.required, EmailValidator.isValid])
      birthDate: [''],
      mobile: ['', Validators.compose([Validators.maxLength(15), Validators.minLength(5), Validators.pattern('^[0-9]+$'), Validators.required])],
      address: [''],
      usertype: [''],
      description: ['']
    });
  }

  intializeApp1() {
    this.enableDiv = (this.navParams.data.editable == false ? true : false);
    this.showSaveButton = (this.navParams.data.showSaveButton == true ? true : false);
    this.data.usertype = (this.data.usertype === undefined ? "healthcareprovider" : this.data.usertype);
    console.log(this.enableDiv);
    console.log(this.showSaveButton);
    if (!_.isEmpty(this.navParams.data)) {
      console.log(this.navParams.data);
      this.data = this.navParams.data.data;
      console.log(this.data);
      // this.data.address = (this.data.address === undefined ? "" : this.data.address);
      // this.data.description = (this.data.description === undefined ? "" : this.data.description);

      // this.action = 'edit';
      // console.log('navParams',navParams.data);
      this.hcpdata = this.navParams.data.hcpdata;
      this.action = this.navParams.data.action;
      this.addexist = this.navParams.data.addexist;
      this.data1.newDoctors = this.getDoctors(this.hcpdata);
    }
    this.data1.newDoctors = (this.data1.newDoctors == undefined ? [] : this.data1.newDoctors);
    // this.enableDiv = false;
    if (this.data == undefined) {
      this.data = [];
      this.data.description = (this.data.description === undefined ? "" : this.data.description);
      this.data.address = (this.data.address === undefined ? "" : this.data.address);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppHealthcareproviderEditPage');
    this.getuserLoginData();
  }
  checkState(email) {
    console.log(email);
    this.loadingService.show();
    var hcpWebServiceData: any = {};
    hcpWebServiceData.action = "getuserbyid";
    hcpWebServiceData.id = email;
    hcpWebServiceData.appsecret = this.appsecret;
    console.log(this.data);
    this.http.post(this.baseurl + "login.php", hcpWebServiceData).subscribe(snapshot => {
      console.log(snapshot);
      let result = JSON.parse(snapshot["_body"]);
      // var snapshot = snapshot.val();
      if (result.status == 'success') {
        this.data = result.data;
        this.data.name = result.data.firstname + ' ' + result.data.lastname;
        this.enableDropdown = false;
        this.enableDiv = true;
        this.loadingService.hide();
      } else {
        // self.loadingService.hide();
        this.toastCtrl.presentToast("Unable to fetch data");
        // self.errorMessage = "Unable to fetch data";
      }
    });
  }

  getDoctors(doctorsData) {
    this.loadingService.show();
    console.log(doctorsData);
    var result1: any = [];
    var hcpWebServiceData: any = {};
    let self = this;
    hcpWebServiceData.action = "getdropdownhcpdata";
    hcpWebServiceData.getdropdownhcpdata = doctorsData;
    hcpWebServiceData.appsecret = this.appsecret
    // self.data.id = this._cookieService.get('userid');
    self.http.post(this.baseurl + "getdropdowndata.php", hcpWebServiceData).subscribe(data => {
      let result = JSON.parse(data["_body"]);
      console.log(result);
      if (result.status == 'success') {
        this.loadingService.hide();
        console.log(result.data);
        // result.data;
        result1 = result1.push(result.data); //[result.data];
        console.log(result1);
        // return result1;
      } else {
        this.loadingService.hide();
        // this.toastCtrl.presentToast("Unable to fetch data");
      }
    }, err => {
      this.loadingService.hide();
      console.log(err);
    });
    console.log(result1);
    return result1;
  }

  comparer(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        // console.log(other);
        return other.email == current.email //&& other.name == current.name
        // return other == current
      }).length == 0;
    }
  }

  addHealthcareproviderDetails() {
    let self = this;
    this.submitAttempt = true;

    if (!this.Profileform.valid) {
      console.log("Profileform Validation Fire!");
    }
    else {
      console.log('this.data', this.data);
      this.Profileform.value.id = this.data.id;
      if (this.data.image != undefined) {
        this.data.image = this.data.image;
      }
      else {
        this.showImagePreview();
        this.data.image = "";
      }
      if (this.action === 'edit') {
      } else {
        self.data.action = "adddoctortohcp";
        self.data.hcpid = self.data.id;
        self.data.userid = this.userLoginData.id;

        console.log(self.data);
        self.data.usertype = this.userLoginData.usertype;
        self.data.appsecret = this.appsecret
        self.http.post(this.baseurl + "insertrelationdata.php", self.data).subscribe(data => {
          this.loadingService.show();
          let result = JSON.parse(data["_body"]);
          console.log(result);
          if (result.status == 'success') {
            self.loadingService.hide();
            self.toastCtrl.presentToast("User added successfully");
            self.navCtrl.setRoot("AppDashboardPage");
            // return result1;
          } else {
            self.loadingService.hide();
            self.toastCtrl.presentToast("There is a problem adding data, please try again");
            self.navCtrl.setRoot("AppDashboardPage");
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
          }
        }, err => {
          this.loadingService.hide();
          console.log(err);
        });
      }
    }
  }
  random() {
    let rand = Math.floor(Math.random() * 2000000) + 1;
    return rand;
  }
  getDate() {
    let date = new Date(this.date);
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    // return date.getDate();
  }
  ImageUpload() {
    let alert = this.alertCtrl.create({
      // title: 'Image Upload',
      message: 'Select Image source',
      buttons: [
        {
          text: 'Upload from Library',
          handler: () => {
            this.gallery(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.gallery(this.camera.PictureSourceType.CAMERA);
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }


  gallery(sourceType: PictureSourceType) {
    let self = this;
    this.camera.getPicture({
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      correctOrientation: true,
      allowEdit: true
    }).then((imageData) => {
      // imageData is a base64 encoded string
      self.showImagePreview();
      self.data.image = "data:image/jpeg;base64," + imageData;
      self.image = "data:image/jpeg;base64," + imageData;

    }, (err) => {
      console.log("Error " + err);
    });
  }

  showImagePreview() {
    let self = this;
    self.setVisibility(self.previewImage, "block");
    self.setVisibility(self.imageUploader, "none");
    self.setVisibility(self.addPhotoCaption, "none");
    self.setVisibility(self.removePhotoIcon, "block");
  }

  removeImage() {
    let self = this;
    this.data.image = undefined;
    self.setVisibility(self.previewImage, "none");
    self.setVisibility(self.imageUploader, "block");
    self.setVisibility(self.addPhotoCaption, "block");
    self.setVisibility(self.removePhotoIcon, "none");

  }

  setVisibility(element: ElementRef, state) {
    if (element !== undefined)
      element.nativeElement.style.display = state;
  }


  logout() {
    this.databaseService.logout();
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }
  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh();
      }
      else {
        this.patientType = result;
        this.intializeApp1();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      return null;
    }
  }
  doRefresh() {
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }

}
