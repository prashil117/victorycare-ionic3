import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppHealthcareproviderEditPage } from './app-healthcareprovider-edit';

@NgModule({
  declarations: [
    AppHealthcareproviderEditPage,
  ],
  imports: [
    IonicPageModule.forChild(AppHealthcareproviderEditPage),
  ],
})
export class AppHealthcareproviderEditPageModule {}
