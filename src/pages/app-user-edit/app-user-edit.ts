import { AfterViewInit, Component, ElementRef, ViewChild } from "@angular/core";
import {
  AlertController,
  IonicPage,
  ModalController,
  NavController,
  NavParams,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Camera, CameraOptions, PictureSourceType } from "@ionic-native/camera";
import * as _ from "lodash";
import {
  Validators,
  FormBuilder,
  FormArray,
  FormGroup,
  FormControl,
} from "@angular/forms";
import { AppPatientsContactsPage } from "../app-patients-contacts/app-patients-contacts";
import { EmailValidator } from "../../app/directive/email-validator";
import { AppAddContactModalPage } from "../app-add-contact-modal/app-add-contact-modal";
import { Storage } from "@ionic/storage";
import { MyApp } from "../../app/app.component";
import { Http } from "@angular/http";
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
import { AppPatientContactsModalPage } from "../app-patient-contacts-modal/app-patient-contacts-modal";

// import {ImagePicker, ImagePickerOptions} from '@ionic-native/image-picker/ngx';
// import { File } from '@ionic-native/file/ngx';
/**
 * Generated class for the AppUserEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-app-user-edit",
  templateUrl: "app-user-edit.html",
  providers: [Camera, DatabaseService],
})
export class AppUserEditPage implements AfterViewInit {
  SelectList: FormGroup;
  currentHCP: any[];
  // username: any = [];
  Profileform: FormGroup;
  submitAttempt: boolean;
  data: any = {};
  diagnosiss: any = {};
  upload: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  logo: any = {};
  action: any = "";
  image: any = "";
  data1: any = {};
  readonly: any = {};
  deleteButtonDisplay: boolean;
  usertype: string;
  username: string;
  userid: string;
  ut: boolean;
  dropdown: any = [];
  notselected: boolean;
  selected1: any = [];
  result: any = [];
  dependent: boolean = true;
  isNextofkin: boolean = false;
  userLoginData: any;
  patientType: any;
  @ViewChild("imageUploader")
  imageUploader: ElementRef;
  @ViewChild("addPhotoCaption")
  addPhotoCaption: ElementRef;
  @ViewChild("removePhotoIcon")
  removePhotoIcon: ElementRef;
  @ViewChild("previewImage")
  previewImage: ElementRef;

  constructor(
    public navCtrl: NavController,
    navParams: NavParams,
    public storage: Storage,
    public formBuilder: FormBuilder,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private databaseService: DatabaseService,
    private loadingService: LoadingService,
    private camera: Camera,
    public myApp: MyApp,
    public http: Http,
    private toastCtrl: ToastService,
  ) {
    // this.storage.remove('patientSession');
    this.upload = "assets/images/upload1.svg";
    this.readonly = false;
    this.Profileform = formBuilder.group({
      usertype: [""],
      // name: ['', Validators.required],
      firstname: [
        "",
        Validators.compose(
          [
            Validators.maxLength(15),
            Validators.pattern("[a-zA-Z ]*"),
            Validators.required,
          ],
        ),
      ],
      lastname: [
        "",
        Validators.compose(
          [
            Validators.maxLength(15),
            Validators.pattern("[a-zA-Z ]*"),
            Validators.required,
          ],
        ),
      ],
      email: [
        "",
        Validators.compose(
          [
            Validators.required,
            EmailValidator.isValid,
            Validators.pattern("^[a-z0-9_.+-]+@[a-z0-9-]+.[a-z0-9-.]+$"),
          ],
        ),
      ],
      birthdate: ["", Validators.required],
      mobile: [
        "",
        Validators.compose(
          [
            Validators.maxLength(15),
            Validators.minLength(5),
            Validators.pattern("^[0-9]+$"),
            Validators.required,
          ],
        ),
      ],
      sex: ["", Validators.required],
      diagnosis: this.formBuilder.array([
        this.initDiagnosis(),
      ]),
      contacts: this.formBuilder.array([
        this.initContact(),
      ]),
      // address: ['']
      street: [""],
    });
    this.SelectList = formBuilder.group({
      currentHCP: [""],
    });

    ////////// HCP DropDown Code ends /////////////
  }

  appIntialize1() {
    this.loadingService.show();
    if (this.patientType == "independent") {
      this.dependent = false;
    }
    if (this.userLoginData.usertype == "patient") {
      this.isNextofkin = true;
    }

    if (this.data.contacts !== undefined) {
      console.log("result", result);
      for (let i = 0; i < this.data.contacts.length; i++) {
        const control = <FormArray>this.Profileform.controls["contacts"];
        control.push(this.initContact());
      }
    } else {
      this.data.contacts = [];
    }
    // this.data.address = (this.data.address == undefined ? "" : this.data.address);
    // this.username = (this._cookieService.get('username') ? this._cookieService.get('username') : '');
    // this.username = (this._cookieService.get('userid') ? this._cookieService.get('userid') : '');
    this.deleteButtonDisplay = this.userLoginData.usertype == "patient"
      ? true
      : false;
    this.data.action = "getuserbyid";
    this.data.userid = (this.userLoginData.id ? this.userLoginData.id : "");
    console.log(this.data.userid);
    this.data.appsecret = this.appsecret;
    this.http.post(this.baseurl + "getuser.php", this.data).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);
        console.log(result.data);
        // this.data.name = result.data.firstname+' '+result.data.lastname;
        console.log(this.data.name);
        this.data = result.data;
        var contactdata: any = {};
        contactdata.action = "getnextofkin";
        contactdata.userid = this.userLoginData.id;
        contactdata.addedby = this.userLoginData.id;
        console.log("contactdata", contactdata);
        contactdata.appsecret = this.appsecret;
        this.http.post(this.baseurl + "getdata.php", contactdata).subscribe(
          (result) => {
            this.loadingService.hide();
            console.log(result);
            let result1 = JSON.parse(result["_body"]);
            console.log("result", result1);
            this.data.contacts = result1.data;
            if (this.data.contacts !== undefined) {
              console.log("result", result);
              for (let i = 0; i < this.data.contacts.length; i++) {
                const control = <FormArray>this.Profileform
                  .controls["contacts"];
                control.push(this.initContact());
              }
            } else {
              this.loadingService.hide();
              this.data.contacts = [];
            }
            var diagnosisdata: any = {};
            diagnosisdata.action = "getdiagnosis";
            diagnosisdata.userid = this.userLoginData.id;
            diagnosisdata.addedby = this.userLoginData.id;
            diagnosisdata.appsecret = this.appsecret;
            this.http.post(this.baseurl + "getdata.php", diagnosisdata).subscribe(
              (result) => {
                this.loadingService.hide();
                console.log(result);
                let result1 = JSON.parse(result["_body"]);
                console.log("result", result1);
                this.data.diagnosis = result1.data;
                if (this.data.diagnosis !== undefined) {
                  for (let i = 0; i < this.data.diagnosis.length; i++) {
                    const control = <FormArray>this.Profileform.controls['diagnosis'];
                    control.push(this.initDiagnosis());
                  }
                }
                else {
                  this.data.diagnosis = [];
                }
              }),
              (err) => {
                this.loadingService.hide();
                this.toastCtrl.presentToast("something went wrong !");
              };
          }), (err) => {
            this.loadingService.hide();
            this.toastCtrl.presentToast("something went wrong !");
          };
      },
      (err) => {
        this.loadingService.hide();
        console.log(err);
      },
    );
    ////////// HCP DropDown Code Starts /////////////

    this.myApp.initializeApp();
    this.username = this.userLoginData.email;
    this.usertype = this.userLoginData.usertype;
    this.userid = this.userLoginData.id;
    this.ut = (this.usertype == "healthcareprovider" ? false : true);

    console.log(this.userid);
    // alert(this.usertype);
    this.dropdown = this.getHCPData(this.userid, this.usertype);
    console.log("dropdownnnnnnnnnn", this.dropdown);
    var result = null;
    this.get("currentHCP").then((result) => {
      console.log("Username1: " + result);
      if (result != null) {
        this.notselected = false;
        this.selected1 = result;
        console.log("Username: " + this.selected1);
      } else {
        this.notselected = true;
        console.log(this.dropdown);
        this.selected1 = this.dropdown;
      }
    });

    var contactdata: any = {};
  }

  initDiagnosis() {
    return this.formBuilder.group({
      name: ['']
    });
  }

  setInitialDiagnosis() {
    if (this.data.diagnosis === undefined) {
      this.data.diagnosis = [];
    }

    if (this.data.diagnosis.length === 0) {
      this.data.diagnosis.push({});
    }
  }

  addDiagnosis() {
    console.log('diagnosis', this.data.diagnosis);
    const control = <FormArray>this.Profileform.controls['diagnosis'];
    control.push(this.initDiagnosis());
    this.data.diagnosis.push({ name: '' });

  }

  deleteDiagnosis(index) {
    this.data.diagnosis.splice(index, 1);
  }

  logout() {
    this.databaseService.logout();
  }

  addUserDetails() {
    let self = this;
    this.submitAttempt = true;
    this.action = "edit";

    if (!this.Profileform.valid) {
      console.log("Profileform Validation Fire!");
    } else {
      // console.log('this.data',this.data);
      this.Profileform.value.id = this.data.id;
      if (this.data.image != undefined) {
        this.data.image = this.data.image;
        this.storage.get('userLogin').then(val => {
          val.image = this.data.image;
          self.storage.set("userLogin", val);
          this.doRefresh();
        })
      } else {
        this.showImagePreview();
        this.data.image = "";
        this.storage.get('userLogin').then(val => {
          val.image = this.data.image;
          console.log("val.image", val.image)
          self.storage.set("userLogin", val);
          console.log("val.image1222121", val)
          this.doRefresh();
        })
      }
      this.loadingService.show();
      self.data.action = "updateuser";
      self.data.appsecret = this.appsecret;
      self.http.post(this.baseurl + "updateuser.php", self.data).subscribe(
        (data) => {
          this.adddiagnosis();
          let result = JSON.parse(data["_body"]);
          console.log(result.status);
          if (result.status == "success") {
            this.loadingService.hide();
            self.toastCtrl.presentToast("Your profile is updated successfully");
            // self.navCtrl.setRoot("AppUserEditPage", { 'data': self.data });
          } else {
            self.toastCtrl.presentToast("There is an error saving data!");
            // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
          }
        },
        (err) => {
          console.log(err);
        },
      );

      // self.updateUserDetails().then(function(){
      //     self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
      // });
    }
  }

  addnextofkin() {
    var data1: any = {};
    var self = this;
    data1.action = "addnextofkin";
    data1.data = self.data.contacts;
    data1.userid = this.userLoginData.id;
    data1.addedby = this.userLoginData.id;
    data1.appsecret = this.appsecret;
    console.log("datasdfsfsd", data1);
    self.http.post(this.baseurl + "adddata.php", data1).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);
        console.log(result.status);
        if (result.status == "success") {
          this.getNextofkin();
        } else {
          // self.toastCtrl.presentToast("There is an error saving data!");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
        }
      },
      (err) => {
        this.loadingService.hide();
        console.log(err);
      },
    );
  }

  adddiagnosis() {
    var data1: any = {};
    var self = this;
    data1.action = "adddiagnosis";
    data1.data = self.data.diagnosis;
    data1.userid = this.userLoginData.id;
    data1.addedby = this.userLoginData.id;
    data1.appsecret = this.appsecret;
    console.log("datasdfsfsd", data1);
    self.http.post(this.baseurl + "adddata.php", data1).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);
        console.log(result.status);
        if (result.status == "success") {
        } else {
        }
      },
      (err) => {
        this.loadingService.hide();
        console.log(err);
      },
    );
  }

  deleteUserDetails(id, name) {
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete your profile?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
        {
          text: "Delete",
          handler: () => {
            var value: any = [];
            var keys: any = [];
            var result: any = [];
            let self = this;
            var userTypeToBeAdded: string;
            // var currentData = [{email:email, name:name}];
            console.log("Delete Ok clicked", id);
            self.data.action = "deleteuser";
            self.data.id = id;
            self.data.appsecret = this.appsecret;
            self.http.post(this.baseurl + "deleteuser.php", self.data)
              .subscribe((data) => {
                let result = JSON.parse(data["_body"]);
                console.log(result.status);
                if (result.status == "success") {
                  // self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
                  self.toastCtrl.presentToast(
                    "Your profile is deleted successfully",
                  );
                  self.logout();
                } else {
                  // self.toastCtrl.presentToast("There is an error deleting data!");
                  // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
                }
              }, (err) => {
                console.log(err);
              });
          },
        },
      ],
    });
    alert.present();
  }

  comparer(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        // console.log(other);
        return other.email == current.email; //&& other.name == current.name
        // return other == current
      }).length == 0;
    };
  }

  ImageUpload() {
    let alert = this.alertCtrl.create({
      // title: 'Image Upload',
      message: "Select Image source",
      buttons: [
        {
          text: "Upload from Library",
          handler: () => {
            this.gallery(this.camera.PictureSourceType.PHOTOLIBRARY);
          },
        },
        {
          text: "Use Camera",
          handler: () => {
            this.gallery(this.camera.PictureSourceType.CAMERA);
          },
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
          },
        },
      ],
    });
    alert.present();
  }

  gallery(sourceType: PictureSourceType) {
    let self = this;
    this.camera.getPicture({
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      correctOrientation: true,
      allowEdit: true
    }).then((imageData) => {
      // imageData is a base64 encoded string
      self.showImagePreview();
      self.data.image = "data:image/jpeg;base64," + imageData;
      self.image = "data:image/jpeg;base64," + imageData;
    }, (err) => {
      console.log("Error " + err);
    });
  }

  showImagePreview() {
    let self = this;
    self.setVisibility(self.previewImage, "block");
    self.setVisibility(self.imageUploader, "none");
    self.setVisibility(self.addPhotoCaption, "none");
    self.setVisibility(self.removePhotoIcon, "block");
  }

  removeImage() {
    let self = this;
    this.data.image = undefined;
    self.setVisibility(self.previewImage, "none");
    self.setVisibility(self.imageUploader, "block");
    self.setVisibility(self.addPhotoCaption, "block");
    self.setVisibility(self.removePhotoIcon, "none");
  }

  setVisibility(element: ElementRef, state) {
    if (element !== undefined) {
      element.nativeElement.style.display = state;
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AppUserEditPage");
    this.getuserLoginData();
  }

  ////////// HCP DropDown Code Starts /////////////
  getHCPData(userid, usertype) {
    console.log(userid);
    console.log(usertype);
    var keys: any = [];
    var hcp1: any = [];
    let self = this;
    var webServiceData: any = {};
    webServiceData.action = "gethcpdata";
    webServiceData.id = userid;
    webServiceData.usertype = usertype;
    self.data.id = this.userLoginData.id;
    webServiceData.appsecret = this.appsecret;
    self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);
        console.log(result);
        if (result.status == "success") {
          console.log(result.data);
          // result.data;
          hcp1 = hcp1.push(result.data);
          // hcp1 = result.data;
          console.log("hcp12345", hcp1);
          // return result1;
        } else {
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      },
      (err) => {
        console.log(err);
      },
    );
    return hcp1;
  }

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log("set string in storage: " + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log("storageGET: " + key + ": " + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }

  // remove a single key value:
  remove(key: string) {
    this.storage.remove(key);
  }

  selectEmployee($event) {
    this.set("currentHCP", $event);
    this.doRefresh();
  }

  addContact() {
    var data: any = {};
    console.log(this.data.contacts);
    this.data.contacts =
      (this.data.contacts == undefined ? [] : this.data.contacts);
    let modal = this.modalCtrl.create(
      AppPatientContactsModalPage,
      { "contactsData": data, type: "add", msg: "no", profile: this.data },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      this.getNextofkin();
    });
    // this.data.contacts.push({});
  }

  doRefresh() {
    this.loadingService.show();
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.loadingService.hide();
    }, 2000);
  }

  deleteContact(index) {
    this.data.contacts.splice(index, 1);
    this.addnextofkin();
  }

  ngAfterViewInit(): void {
    this.setInitialDiagnosis();
    if (this.data.contacts === undefined) {
      this.data.contacts = [];
    }

    if (!_.isEmpty(this.action)) {
      this.showImagePreview();

      if (this.action === "view") {
        this.readonly = true;
      }
    }
  }

  initContact() {
    return this.formBuilder.group({
      name: [""],
      phone: [""],
    });
  }

  async getpatientType() {
    await this.get("patientType").then((result) => {
      if (result == "independent") {
        this.dependent = false;
      }
      this.appIntialize1();
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        console.log("this.userlogin", this.userLoginData);
        if (this.userLoginData.usertype == "patient") {
          this.isNextofkin = true;
        }
        this.getpatientType();
      }
    });
  }

  getNextofkin() {
    var contactdata: any = {};
    contactdata.action = "getnextofkin";
    contactdata.userid = this.userLoginData.id;
    contactdata.addedby = this.userLoginData.id;
    console.log("contactdata", contactdata);
    contactdata.appsecret = this.appsecret;
    this.http.post(this.baseurl + "getdata.php", contactdata).subscribe(
      (result) => {
        console.log(result);
        let result1 = JSON.parse(result["_body"]);
        console.log("result", result1);
        this.data.contacts = result1.data;
        if (this.data.contacts !== undefined) {
          console.log("result", result);
          for (let i = 0; i < this.data.contacts.length; i++) {
            const control = <FormArray>this.Profileform
              .controls["contacts"];
            control.push(this.initContact());
          }
        } else {
          this.data.contacts = [];
        }
      },
    ), (err) => {
      this.toastCtrl.presentToast("something went wrong !");
    };
  }

  Remove() {
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete your picture?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
        {
          text: "Delete",
          handler: () => {
            this.removeImage()
          }
        },
      ],
    });
    alert.present();
  }

}
