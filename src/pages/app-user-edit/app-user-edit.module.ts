import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppUserEditPage } from './app-user-edit';

@NgModule({
  declarations: [
    AppUserEditPage,
  ],
  imports: [
    IonicPageModule.forChild(AppUserEditPage),
  ],
})
export class AppUserEditPageModule {}
