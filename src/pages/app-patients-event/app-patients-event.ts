import { AfterViewInit, Component, ViewChild, ElementRef } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController,
  ViewController,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl,
  FormArray,
} from "@angular/forms";
import * as _ from "lodash";
import { AppSharedService } from "../../services/app-shared-service";
import { Storage } from "@ionic/storage";
import { Http } from "@angular/http";
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";

/**
 * Generated class for the AppPatientsEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-app-patients-event",
  templateUrl: "app-patients-event.html",
  providers: [DatabaseService],
})
export class AppPatientsEventPage implements AfterViewInit {
  medicineStep2form: FormGroup;
  submitAttempt: boolean;
  data: any = {};
  patientData: any = {};
  medicineData: any = [];
  AddedMedicine: any = [];
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  doctorsList: any = [];
  eventList: any = [];
  newEventName: string;
  presAddedBy: string;
  date: any = new Date();
  index: any;
  userLoginData: any;
  patientType: any;
  hcpid: any;

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public appSharedService: AppSharedService,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private loadingService: LoadingService,
    private databaseService: DatabaseService,
    public storage: Storage,
    public http: Http,
    private toastCtrl: ToastService,
  ) {
    this.medicineStep2form = formBuilder.group({
      event: ["", Validators.required],
      prestartdate: ["", Validators.required],
      preenddate: ["", Validators.required],
      recurringDaily: [],
      addToStdWheel: [],
      enableDuringDay: [],
      tim: this.formBuilder.array([
        this.initTim(),
      ])
    });
    this.data.tim=[];
    if (this.navParams.data.eventdata1) {
      this.data.event = this.navParams.data.eventdata1.id;
    }
  }

  initTim() {
    return this.formBuilder.group({
      // dose: [''],
      starttime: ['']
    });
  }

  setInitialTim() {
    if (this.data.tim === undefined) {
      this.data.tim = [];
    }

    if (this.data.tim.length === 0) {
      this.data.tim.push({});
    }
  }

  addTim() {

    const control = <FormArray>this.medicineStep2form.controls['tim'];
    control.push(this.initTim());
    this.data.tim.push({});
    console.log("timmm", this.data.tim)
  }

  deleteTim(index) {
    this.data.tim.splice(index, 1);
  }

  intializeApp1() {
    let self = this;
    this.newEventName = this.navParams.data.EventName
      ? this.navParams.data.EventName
      : "";
    this.data.action = "geteventmasterdatafulllist";
    if (
      this.patientType == "independent" ||
      this.userLoginData.usertype == "healthcareprovider"
    ) {
      this.data.userid = this.userLoginData.id;
      this.data.appsecret = this.appsecret;
      this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
        (data) => {
          this.loadingService.show();
          let result = JSON.parse(data["_body"]);
          console.log(result.data);
          if (result.status == "success") {
            this.loadingService.hide();
            if (result.data !== null) {
              this.eventList = result.data;
              this.data.tim=[];
              // if (this.data.tim !== undefined) {
              //   for (let i = 0; i < this.data.tim.length; i++) {
              //     const control = <FormArray>this.medicineStep2form.controls['tim'];
              //     control.push(this.initTim());
              //   }
              // }
              // else {
              //   this.data.tim = [];
              // }
              if (this.newEventName) {
                this.data.event = this.eventList.find((e) =>
                  e.name === this.newEventName
                ).id;
              }
            } else {
              this.loadingService.hide();
              this.data = [];
              this.eventList = [];
            }
          } else {
            this.loadingService.hide();
            this.toastCtrl.presentToast(
              "There is a problem adding data, please try again",
            );
          }
        },
        (err) => {
          this.loadingService.hide();
          console.log(err);
        },
      );
    } else {
      this.storage.get("currentHCP").then((hcpid) => {
        this.data.userid = hcpid;
        this.data.appsecret = this.appsecret;
        this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
          (data) => {
            this.loadingService.show();
            let result = JSON.parse(data["_body"]);
            console.log(result.data);
            if (result.status == "success") {
              this.loadingService.hide();
              if (result.data !== null) {
                this.eventList = result.data;
                if (this.newEventName) {
                  this.data.event = this.eventList.find((e) =>
                    e.name === this.newEventName
                  ).id;
                }
              } else {
                this.loadingService.hide();
                this.data = [];
                this.eventList = [];
              }
            } else {
              this.loadingService.hide();
              this.toastCtrl.presentToast(
                "There is a problem adding data, please try again",
              );
            }
          },
          (err) => {
            this.loadingService.hide();
            console.log(err);
          },
        );
      });
    }
    this.presAddedBy = this.userLoginData.id;

    if (!_.isEmpty(this.navParams.data)) {
      // console.log('navParams-Medicinemaster',navParams.data);
      this.patientData = this.navParams.data.patientData;
      this.medicineData = this.navParams.data.MedicineData;
      this.AddedMedicine = this.navParams.data.AddedMedicine;
    }

    /*this.data.enablePrescriptionNotification = false;
    this.data.prescriptionaddress = '';
    this.data.areaexpertise = '';
    this.data.doctorid = '';*/
    this.data.enablePrescriptionNotification =
      this.data.enablePrescriptionNotification == undefined
        ? false
        : this.data.enablePrescriptionNotification;
    this.data.prescriptionaddress = this.data.prescriptionaddress == undefined
      ? ""
      : this.data.prescriptionaddress;
    this.data.areaexpertise = this.data.areaexpertise == undefined
      ? ""
      : this.data.areaexpertise;
    // this.data.doctorid = this.data.doctorid == undefined ? '' : this.data.doctorid;
    this.data.event = this.data.event == undefined ? "" : this.data.event;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AppMedicinePrescriptionIssuedPage");
    this.getHCPid();
  }
  closeModal() {
    this.viewCtrl.dismiss(false);
  }

  addPatientEventDetails(patientData, index) {
    
    var dose = Array.prototype.map.call(this.data.tim, s => s.dose).toString();
    var time = Array.prototype.map.call(this.data.tim, s => s.starttime).toString();
    // if (dose) {
    // }
    this.data.dose = dose;
    this.data.startTime = time;
    // this.data.hcpid = this.hcpid;
    console.log("dataaddose", this.data.dose)
    console.log("dataadtime", this.data.time)
    if (this.userLoginData.usertype == "patient") {
      var uid = this.userLoginData.id;
    }

    if (index == undefined) index = 0;
    console.log(index);
    this.submitAttempt = true;

    // if (!this.medicineStep2form.valid)
    // {
    //   console.log("Medicine Master Step 2 Validation Fire!");
    // }
    //
    // {
    let self = this;
    for (let key in self.AddedMedicine) {
      self.data[key] = self.AddedMedicine[key];
    }
    this.data.addedBy = this.presAddedBy;
    this.data.userid = this.userLoginData.id;
    if (this.data.addtowheel == true) {
      this.data.addtowheel == "1";
    } else {
      this.data.addtowheel == "0";
    }
    if (this.data.notification == true) {
      this.data.notification == "1";
    } else {
      this.data.notification == "0";
    }
    if (this.userLoginData.usertype == "healthcareprovider") {
      this.data.hcpid = this.userLoginData.id;
    } else if (this.patientType == "independent") {
      this.data.hcpid = 0;
    } else {
      this.data.hcpid = this.hcpid;
    }
    console.log(self.data);
    self.data.action = "addpatientevent";
    self.data.appsecret = this.appsecret;
    self.http.post(this.baseurl + "adddata.php", self.data).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);
        console.log(result);
        if (result.status == "success") {
          self.loadingService.hide();
          self.toastCtrl.presentToast("notification added successfully");
          self.viewCtrl.dismiss(true);
        } else {
          self.loadingService.hide();
          self.toastCtrl.presentToast(
            "There is a problem adding data, please try again",
          );
          self.viewCtrl.dismiss(false);
        }
      },
      (err) => {
        console.log(err);
      },
    );
  }
  getDate() {
    let date = new Date(this.date);
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" +
      date.getDate();
    // return date.getDate();
  }
  addTime() {
    this.data.date.push({});
  }

  deleteTime(index) {
    this.data.date.splice(index, 1);
  }

  setInitialTime() {
    if (this.data.date === undefined) {
      this.data.date = [];
    }

    if (this.data.date.length === 0) {
      this.data.date.push({});
    }
  }

  async getHCPid() {
    await this.get("currentHCP").then((result) => {
      if (result != null) {
        this.hcpid = result;
        console.log("if", this.hcpid);
        this.getuserLoginData();
      } else {
        this.hcpid = 0;
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get("patientType").then((result) => {
      console.log("patienttypesdfdsfsdsdf", result);
      if (
        (!result || result == null || result == undefined) &&
        this.userLoginData == "patient"
      ) {
        this.doRefresh("");
      } else {
        this.patientType = result;
        this.intializeApp1();
        console.log("sekeccdffvfdgdfgdfg", this.patientType);
      }
    });
  }

  doRefresh(event) {
    console.log("Begin async operation");
    setTimeout(() => {
      console.log("Async operation has ended");
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      // event.target.complete();
    }, 2000);
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      } else {
        return null;
      }
    } catch (reason) {
      return null;
    }
  }

  ngAfterViewInit(): void {
    this.setInitialTime();
  }
  oneventChange(e) {
    console.log("this.data.event", this.data.event);
    // if(e=="other")
    // {
    //   this.navCtrl.push('AppPatientsEventMasterModalPage',{ 'patientaction': 'add' });
    // }
  }
}
