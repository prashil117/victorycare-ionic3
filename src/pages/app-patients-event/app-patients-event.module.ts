import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppPatientsEventPage } from './app-patients-event';

@NgModule({
  declarations: [
    AppPatientsEventPage,
  ],
  imports: [
    IonicPageModule.forChild(AppPatientsEventPage),
  ],
})
export class AppPatientsEventPageModule {}
