import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppEventMasterListPage } from './app-event-master-list';
import { PipesModule } from '../../pipes/pipes.module';//<--- here

@NgModule({
  declarations: [
    AppEventMasterListPage,
  ],
  imports: [
    IonicPageModule.forChild(AppEventMasterListPage),
    PipesModule
  ],
})
export class AppEventMasterListPageModule {}
