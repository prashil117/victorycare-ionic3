import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-app-settings',
  templateUrl: 'app-settings.html',
})
export class AppSettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppSettingsPage');
  }

  getLocation(value) {
    var target = value == "profile" ? "AppUserEditPage" :
      value == "notification" ? "AppNotificationSettingsPage" :
        value == "privacy" ? "AppPrivacyPage" :
          value == "how" ? "AppHowDoesWorkPage" :
              value == "wheel" ? "AppSettingsDailyWheelSettingsPage" :
                value == "security" ? "AppSecurityAndAccessSettingsPage" :
                  value == "about" ? "AppSettingAboutVictoryPage" : "AppSettingsTermsAndConditionsPage";
    this.navCtrl.push(target);
  }

}
