import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppPrivacyPage } from './app-privacy';

@NgModule({
  declarations: [
    AppPrivacyPage,
  ],
  imports: [
    IonicPageModule.forChild(AppPrivacyPage),
  ],
})
export class AppPrivacyPageModule {}
