import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Thumbnail, } from 'ionic-angular';
import { ToastService } from "../../services/toast-service";
import { LoadingService } from "../../services/loading-service";
import { Http } from '@angular/http';
import { environment } from "../../environment/environment";
import { Storage } from "@ionic/storage";

@IonicPage()
@Component({
  selector: 'page-app-privacy',
  templateUrl: 'app-privacy.html',
})
export class AppPrivacyPage {

  data: any = {};
  oldata: any;
  appsecret = environment.appsecret;
  baseurl = environment.apiUrl;
  userLoginData: any;


  constructor(private storage: Storage, public toastctrl: ToastService, public http: Http, public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppPrivacyPage');
    this.getuserLoginData();
  }

  getPrivacy() {
    var data1 = {
      userid: this.userLoginData.id,
      appsecret: this.appsecret,
      action: "getprivacy"
    }
    this.http.post(this.baseurl + 'privacy.php', data1).subscribe(res => {
      let result = JSON.parse(res['_body']);
      console.log("result", res);
      if (result.status == "success") {
        if (result.data) {
          this.data = result.data;
          this.oldata = result.data
        }
      }
      else {
        this.data.isNotification = 1;
        this.data.isCopy = 1;
        this.data.isReport = 1;
        this.data.isStatistical = 1;
        this.data.language = "English";
      }
    }, err => {
      console.log("error", err)
    })
  }
  addupdatePrivacy() {
    var data1 = {
      userid: this.userLoginData.id,
      appsecret: this.appsecret,
      action: !this.oldata ? "addprivacy" : 'updateprivacy',
      isnotification: this.data.isNotification,
      iscopy: this.data.isCopy,
      isstatistical: this.data.isStatistical,
      isreport: this.data.isReport,
      language: this.data.language
    }

    console.log("data", data1)
    this.http.post(this.baseurl + 'privacy.php', data1).subscribe(res => {
      let result = JSON.parse(res['_body']);
      this.toastctrl.presentToast(result.status + "fully updated");
      this.getPrivacy();
    })
  }

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }


  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.getPrivacy();
      }
    });
  }

}
