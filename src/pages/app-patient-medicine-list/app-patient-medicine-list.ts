import { Component, AfterViewInit } from "@angular/core";
import {
  AlertController,
  IonicPage,
  ModalController,
  NavController,
  NavParams,
  Platform,
  ViewController,
  App,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from "rxjs/Observable";
import * as _ from "lodash";
import { AppPatientMedicineModalPage } from "../app-patient-medicine-modal/app-patient-medicine-modal";
import { AppAddMedicineMasterPage } from "../app-add-medicine-master/app-add-medicine-master";
import { AppMedicineMasterListModalPage } from "../app-medicine-master-list-modal/app-medicine-master-list-modal";
import { Http } from "@angular/http";
import { Storage } from "@ionic/storage";
import { environment } from "../../environment/environment";
import { ToastService } from "../../services/toast-service";

// import {AppAddContactModalPage} from "../app-add-contact-modal/app-add-contact-modal";

@IonicPage()
@Component({
  selector: "app-patient-medicine-list",
  templateUrl: "app-patient-medicine-list.html",
  providers: [DatabaseService],
})
export class AppPatientMedicineListPage {
  data: any = {};
  medicines: any = [];
  patientData: any = {};
  MedicineData: any;
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  count: number;
  isloadmore: boolean = false;
  offset: number = 0;
  userLoginData: any;

  constructor(
    public storage: Storage,
    private toastCtrl: ToastService,
    public http: Http,
    public navCtrl: NavController,
    navParams: NavParams,
    private app: App,
    private alertCtrl: AlertController,
    private databaseService: DatabaseService,
    private loadingService: LoadingService,
    public modalCtrl: ModalController,
  ) {
  }

  addMedicineListItem() {
    // console.log('patientDatalist',this.patientData);
    let modal = this.modalCtrl.create(
      AppMedicineMasterListModalPage,
      {
        "type": "add",
        "MedicineData": this.medicines,
        "patientData": this.patientData,
        "onboard": true,
      },
      { cssClass: "on-addmedicine" },
    );
    // let modal = this.modalCtrl.create(AppMedicineMasterListModalPage, { 'MedicineData': this.medicines, 'patientData': this.patientData });
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.MedicineData = [];
        this.tmparr = [];
        this.loadMedicineListSettings();
      }
    });
  }
  doRefresh() {
    this.loadingService.show();
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.loadingService.hide();
    }, 100);
  }
  tmparr = [];
  loadMedicineListSettings(infiniteScroll?) {
    this.data.action = "getmedicinemasterdata";
    this.data.userid = this.userLoginData.id;
    this.data.offset = 0;
    this.data.keyword = "";
    this.data.appsecret = this.appsecret;
    console.log("this.data", this.data);
    this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
      (data) => {
        this.loadingService.show();
        let result = JSON.parse(data["_body"]);
        console.log("result", result);
        if (infiniteScroll) {
          infiniteScroll.complete();
        }
        if (result.status == "notmatchingkey") {
          this.doRefresh();
        }
        if (result.status == "success") {
          this.loadingService.hide();
          if (result.data0 !== null) {
            for (var i in result.data0) {
              this.tmparr.push(result.data0[i]);
            }
            this.MedicineData = this.tmparr;
            if (result.data0) {
              this.count = result.data0[0].count;
            } else {
              this.count = 0;
            }
            console.log("MedicenData", this.MedicineData);
          } else {
            this.loadingService.hide();
            this.data = [];
            this.data.keys = [];
            this.MedicineData = [];
          }
        } else {
          this.loadingService.hide();
          // this.loadingService.hide();
          // this.toastCtrl.presentToast("There is a no data available.");
          // this.navCtrl.setRoot("AppDashboardPage");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      },
      (err) => {
        console.log(err);
        this.loadingService.hide();
      },
    );
  }
  loadMore(infiniteScroll) {
    if (this.MedicineData.length >= this.count || this.count == 0) {
      infiniteScroll.enable(false);
    } else {
      this.offset = this.offset + 10;
      this.loadMedicineListSettings(infiniteScroll);
    }
  }

  MedicineUpdate(item, index) {
    let modal = this.modalCtrl.create(
      AppMedicineMasterListModalPage,
      {
        "MedicineData": item,
        "patientData": this.patientData,
        "index": index,
        "type": "edit",
        "onboard": true,
      },
      { cssClass: "on-addmedicine " },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.MedicineData = [];
        this.tmparr = [];
        this.loadMedicineListSettings();
      }
    });
  }

  tabNextButton() {
    this.navCtrl.parent.select(2);
  }

  deleteMedicineData(item, index) {
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete this record?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
        },
      }, {
        text: "Delete",
        handler: () => {
          let self = this;
          this.data.action = "deletemedicine";
          this.data.id = item.id;
          this.data.appsecret = this.appsecret;
          this.http.post(this.baseurl + "deletedata.php", this.data).subscribe(
            (data) => {
              let result = JSON.parse(data["_body"]);
              if (result.status == "success") {
                self.toastCtrl.presentToast("Medicine is deleted successfully");
                this.MedicineData = [];
                this.tmparr = [];
                this.loadMedicineListSettings();
              } else {
                self.toastCtrl.presentToast("There is an error deleting data!");
              }
            },
            (err) => {
              console.log(err);
            },
          );
        },
      }],
    });
    alert.present();
  }

  GotoHome() {
    this.loadingService.show();
    this.data.action = "setloginfirsttime";
    this.data.userid = this.userLoginData.id;
    this.data.appsecret = this.appsecret;
    console.log("data", this.data);

    this.http.post(this.baseurl + "login.php", this.data).subscribe((data) => {
      this.loadingService.show();
      let result = JSON.parse(data["_body"]);
      if (result.status == "success") {
        this.loadingService.hide();
        this.app.getRootNav().setRoot("AppTimelinePage");
        console.log("result", result);
      } else {
        this.loadingService.hide();
        // this.showToast("Something went wrong");
        // this.toastCtrl.presentToast("Something went wrong, Please try again.");
      }
    }, (err) => {
      this.loadingService.hide();
      console.log(err);
    });
  }
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      return null;
    }
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.loadMedicineListSettings();
      }
    });
  }
  ionViewDidLoad() {
    this.getuserLoginData();
  }
}
