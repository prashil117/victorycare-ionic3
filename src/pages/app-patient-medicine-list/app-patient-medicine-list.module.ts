import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppPatientMedicineListPage} from "./app-patient-medicine-list";



@NgModule({
  declarations: [
      AppPatientMedicineListPage
  ],
  imports: [
    IonicPageModule.forChild(AppPatientMedicineListPage)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})

export class AppPatientMedicineListModule {}
