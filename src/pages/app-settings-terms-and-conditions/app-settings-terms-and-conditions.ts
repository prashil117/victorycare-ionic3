import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController,NavParams } from 'ionic-angular';

/**
 * Generated class for the AppSettingsTermsAndConditionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-settings-terms-and-conditions',
  templateUrl: 'app-settings-terms-and-conditions.html',
})
export class AppSettingsTermsAndConditionsPage {

  constructor(public navCtrl: NavController,public viewCtrl: ViewController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppSettingsTermsAndConditionsPage');
  }

  closeModal() {
    this.viewCtrl.dismiss();
    // this.loadWheelActivity();
  }

}
