import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppSettingsTermsAndConditionsPage } from './app-settings-terms-and-conditions';

@NgModule({
  declarations: [
    AppSettingsTermsAndConditionsPage,
  ],
  imports: [
    IonicPageModule.forChild(AppSettingsTermsAndConditionsPage),
  ],
})
export class AppSettingsTermsAndConditionsPageModule {}
