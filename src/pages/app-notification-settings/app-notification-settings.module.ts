import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppNotificationSettingsPage } from './app-notification-settings';

@NgModule({
  declarations: [
    AppNotificationSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(AppNotificationSettingsPage),
  ],
})
export class AppNotificationSettingsPageModule {}
