import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { environment } from '../../environment/environment';
import { Http } from '@angular/http';
import { LoadingService } from '../../services/loading-service';
import { ToastService } from '../../services/toast-service';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';

@IonicPage()
@Component({
  selector: 'page-app-notification-settings',
  templateUrl: 'app-notification-settings.html',
})
export class AppNotificationSettingsPage {
  userLoginData: any;
  dependent: any;
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  eventtime: any;
  activitytime: any;
  medicinetime: any;
  patientType: any;
  isNotification: any;
  isActivity: any;
  isStatistical: any;
  isAccept: any;
  timeOption: any = [
    { key: '+0 minutes', value: 'on time' },
    { key: '+15 minutes', value: 'Default (15 minutes)' },
    { key: '+30 minutes', value: '30 minutes' },
    { key: '+45 minutes', value: '45 minutes' },
    { key: '+1 hour', value: '1 hour' }
  ]
  constructor(private openNativeSettings: OpenNativeSettings,private toastServie: ToastService, private loadingService: LoadingService, public http: Http, public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
  }


  ionViewDidLoad() {
  }

  appIntialize() {
    var data = {
      userid: this.userLoginData.id,
      appsecret: this.appsecret,
      action: 'getNotificationSettings',
    }
    this.http.post(this.baseurl + 'notificationsettings.php', data).subscribe(data => {
      let result = JSON.parse(data['_body']);
      console.log("result", result)
      this.eventtime = result.data.event_time;
      this.activitytime = result.data.activity_time;
      this.medicinetime = result.data.medicine_time;
      this.isNotification = result.data.isNotification ? result.data.isNotification : 0
      this.isStatistical = result.data.isStatistical ? result.data.isStatistical : 0
      this.isActivity = result.data.isActivity ? result.data.isActivity : 0
      this.isAccept = result.data.isAccept ? result.data.isAccept : 0
    }), err => {
      console.log("error", err)
    }
  }

  updateNotificationTime() {
    this.loadingService.show();
    var time = [
      { eventTime: this.eventtime },
      { activityTime: this.activitytime },
      { medicineTime: this.medicinetime },
    ]
    var data = {
      userid: this.userLoginData.id,
      appsecret: this.appsecret,
      activitytime: this.activitytime,
      medicinetime: this.medicinetime,
      eventtime: this.eventtime,
      isNotification: this.isNotification,
      isActivity: this.isActivity,
      isStatistical: this.isStatistical,
      isAccept: this.isAccept,
      action: 'timesettings',
    }
    this.openNativeSettings.open("application_details").then(val => {
      console.log('success',val)
    });

    this.http.post(this.baseurl + 'notificationsettings.php', data).subscribe(data => {
      this.loadingService.hide();
      let result = JSON.parse(data['_body']);
      if (result.status == "success") {
        this.toastServie.presentToast('Data saved successfully');
      } 
    }), err => {
      this.loadingService.hide();
      console.log("error : ", err)
    }
  }
  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log("set string in storage: " + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      } else {
        return null;
      }
    } catch (reason) {
      return null;
    }
  }

  async getpatientType() {
    await this.get("patientType").then((result) => {
      if (result == "independent") {
        this.dependent = false;
      }
      this.appIntialize();
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }

}
