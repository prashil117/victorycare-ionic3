import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppModalPage} from "./app-modal";


@NgModule({
  declarations: [
      AppModalPage
  ],
  imports: [
    IonicPageModule.forChild(AppModalPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppModalModule {}
