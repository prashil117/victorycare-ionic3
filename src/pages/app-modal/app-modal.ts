import {Component, AfterViewInit, NgModule} from '@angular/core';
import {
    AlertController, IonicPage, ModalController, NavController, NavParams, Platform,
    ViewController
} from 'ionic-angular';
import {LoadingService} from "../../services/loading-service";
import {Observable} from 'rxjs/Observable';
import * as _ from "lodash";
import {AppSharedService} from "../../services/app-shared-service";
import {ImagePicker} from '@ionic-native/image-picker/ngx';
import { Camera } from '@ionic-native/camera'

@IonicPage()
@Component({
    selector: 'app-modal',
    templateUrl: 'app-modal.html',
    providers: []
})

@NgModule({
    
//     entryComponents: [
     
//     ],
//     // imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
    providers: [
//     //   StatusBar,
//     //   SplashScreen,
      
//       { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
//       ImagePicker,
      Camera,
//       File
    ]
  })
export class AppModalPage {

    title:string;
    description:string;

    constructor(public navCtrl: NavController, navParams: NavParams, private alertCtrl: AlertController, private loadingService: LoadingService,public modalCtrl: ModalController, public viewCtrl: ViewController, public appSharedService: AppSharedService) {

        if(!_.isEmpty(navParams.data)){
            this.title = navParams.data.title;
            this.description = navParams.data.description;
        }
    }

    closeModal(){
        this.viewCtrl.dismiss();
    }
}



