import { AfterViewInit, Component, ElementRef, ViewChild } from "@angular/core";
import { AlertController, IonicPage, ModalController, NavController, NavParams, LoadingController } from "ionic-angular";
import { ImageViewerController } from 'ionic-img-viewer';
import { LoadingService } from "../../services/loading-service";
import { Camera, CameraOptions, PictureSourceType, MediaType } from "@ionic-native/camera";
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import * as _ from "lodash";
import {
  FormBuilder
} from "@angular/forms";
import { Storage } from "@ionic/storage";
import { MyApp } from "../../app/app.component";
import { Http } from "@angular/http";
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
import { File } from '@ionic-native/file/ngx';

@IonicPage()
@Component({
  selector: 'page-app-library',
  templateUrl: 'app-library.html',
  providers: [Camera, FileTransfer, File]
})
export class AppLibraryPage {
  data: any = {};
  dependent: boolean = true;
  @ViewChild("imageUploader")
  imageUploader: ElementRef;

  @ViewChild("addPhotoCaption")
  addPhotoCaption: ElementRef;
  @ViewChild("removePhotoIcon")
  removePhotoIcon: ElementRef;
  items: any;
  @ViewChild("previewImage")
  previewImage: ElementRef;
  image: any;
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  videoFileUpload: FileTransferObject;
  vedio: any;
  upload: any;
  url: any;
  images: any;
  userLoginData: any;
  constructor(public navCtrl: NavController,
    navParams: NavParams,
    public storage: Storage,
    public formBuilder: FormBuilder,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private loadingService: LoadingService,
    private camera: Camera,
    public imageViewerCtrl: ImageViewerController,
    public myApp: MyApp,
    public http: Http,
    private transfer: FileTransfer, private file: File,
    private toastCtrl: ToastService,) {
    this.upload = "assets/images/upload1.svg";
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LibraryPage');
    this.getuserLoginData()
  }




  ImageUpload() {
    let alert = this.alertCtrl.create({
      // title: 'Image Upload',
      message: "Select Image source",
      buttons: [
        {
          text: "Upload from Library",
          handler: () => {
            this.gallery(this.camera.PictureSourceType.PHOTOLIBRARY);
          },
        },
        {
          text: "Use Camera",
          handler: () => {
            this.gallery(this.camera.PictureSourceType.CAMERA);
          },
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
          },
        },
      ],
    });
    alert.present();



  }

  ConfirmImage() {
    let alert = this.alertCtrl.create({
      message: "Are you sure You want to upload picture ?",
      buttons: [
        {
          text: "Yes",
          handler: () => {
            this.uploadFile();
          }
        },
        {
          text: "No",
          role: "cancel",
          handler: () => {
          },
        },
      ],
    });
    alert.present();
  }

  presentImage(myImage) {
    const imageViewer = this.imageViewerCtrl.create(myImage);
    imageViewer.present();

    // setTimeout(() => imageViewer.dismiss(), 1000);
    // imageViewer.onDidDismiss(() => alert('Viewer dismissed'));
  }

  gallery(sourceType: PictureSourceType) {
    let self = this;
    {
      const options: CameraOptions = {
        quality: 50,
        destinationType: this.camera.DestinationType.FILE_URI,
        sourceType: sourceType,
        correctOrientation:true,
        // encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.ALLMEDIA,
        allowEdit: true
      }
      this.camera.getPicture(options).then((imageData) => {

        this.image = imageData;
        this.ConfirmImage()
      }, (err) => {
        console.log(err);
        // this.presentToast(err);
        this.toastCtrl.presentToast(err);
      });
    }
  }

  uploadFile() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();
    var name = this.image.split("/").pop();
    var Params: any = {};
    Params.userid = this.userLoginData.id;
    Params.appsecret = this.appsecret;
    Params.action = "addlibrary";
    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: name,
      chunkedMode: false,
      mimeType: 'multipart/form-data',
      headers: {},
      httpMethod: 'POST',
      params: Params
    }

    fileTransfer.upload(this.image, this.baseurl + 'library.php', options)
      .then((data) => {
        console.log("data",data)
        // console.log(JSON.stringify(data) + " Uploaded Successfully");
        console.log("image", this.image);
        loader.dismiss();
        this.getlibrary();
      }, (err) => {
        console.log(err);
        loader.dismiss();
      });
  }


  setVisibility(element: ElementRef, state) {
    if (element !== undefined) {
      element.nativeElement.style.display = state;
    }
  }


  addlibrary() {
    var data1: any = {};
    var self = this;
    data1.action = "addlibrary";
    data1.userid = this.userLoginData.id;
    data1.addedby = this.userLoginData.id;
    data1.appsecret = this.appsecret;
    data1.image = this.image;

    console.log("datasdfsfsd", data1);
    self.http.post(this.baseurl + "library.php", data1).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);
        console.log(result.status);
        if (result.status == "success") {
          alert("photo uploaded");
          this.getlibrary();
        } else {
          // self.toastCtrl.presentToast("There is an error saving data!");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
        }
      },
      (err) => {
        this.loadingService.hide();
        console.log(err);
      },
    );
  }

  getlibrary() {
    this.loadingService.show();
    var data1: any = {};
    var self = this;
    data1.action = "getlibrary";
    data1.userid = this.userLoginData.id;
    data1.appsecret = this.appsecret;
    console.log("datasdfsfsd", data1);
    self.http.post(this.baseurl + "library.php", data1).subscribe(
      (data) => {
        this.loadingService.hide();
        let result = JSON.parse(data["_body"]);
        if (result.data) {
          this.images = result.data;
          console.log(result);
        }
        else {
          console.log("error");
        }
      },
      (err) => {
        this.loadingService.hide();
        console.log("error", err);
        console.log(err);
      },
    );

  }

  ConfirmDelete(id) {
    let alert = this.alertCtrl.create({
      message: "Are you sure You want to delete picture ?",
      buttons: [
        {
          text: "Yes",
          handler: () => {
            this.deletelibrary(id);
          }
        },
        {
          text: "No",
          role: "cancel",
          handler: () => {
          },
        },
      ],
    });
    alert.present();
  }


  deletelibrary(id) {
    this.loadingService.show();
    var data1: any = {};
    var self = this;
    data1.action = "deleteimage";
    data1.userid = this.userLoginData.id;
    data1.addedby = this.userLoginData.id;
    data1.appsecret = this.appsecret;
    data1.id = id;
    data1.image = this.image;
    console.log("datasdfsfsd", data1);
    self.http.post(this.baseurl + "library.php", data1).subscribe(
      (data) => {
        this.loadingService.hide();
        let result = JSON.parse(data["_body"]);
        console.log(result.status);
        if (result.status == "success") {
          this.toastCtrl.presentToast("picture Deleted!")
          this.getlibrary();
        } else {
          // self.toastCtrl.presentToast("There is an error saving data!");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error saving data!'});
        }
      },
      (err) => {
        this.loadingService.hide();
        console.log(err);
      },
    );
  }

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log("set string in storage: " + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  // to get a key/value pair
  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log("storageGET: " + key + ": " + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }


  async getpatientType() {
    await this.get("patientType").then((result) => {
      if (result == "independent") {
        this.dependent = false;
      }
      this.getlibrary();
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.getpatientType();
      }
    });
  }
  pressed() {
    console.log("pressed")
  }
  active() {
    console.log("active")
  }
  release() {
    console.log("released")
  }

}
