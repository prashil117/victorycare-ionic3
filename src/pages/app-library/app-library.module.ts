import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppLibraryPage } from './app-library';

@NgModule({
  declarations: [
    AppLibraryPage,
  ],
  imports: [
    IonicPageModule.forChild(AppLibraryPage),
  ],
})
export class AppLibraryPageModule {}
