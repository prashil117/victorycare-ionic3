import {Component, AfterViewInit} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {DatabaseService} from "../../services/database-service";
import {LoadingService} from "../../services/loading-service";
import {Observable} from 'rxjs/Observable';
import * as _ from "lodash";


@IonicPage()
@Component({
    selector: 'app-logged-activity-home',
    templateUrl: 'app-logged-activity-home.html',
    providers: [DatabaseService]
})
export class AppLoggedActivityHome {

    data: any = {};
    patientData:any = {};
    patientSettings:any = {};
    tabs : any = [];


    constructor(public navCtrl: NavController, 
                navParams: NavParams, 
                private alertCtrl: AlertController,  
                private loadingService: LoadingService) 
                {
                    if(!_.isEmpty(navParams.data)){
                        this.patientData = navParams.data.patientData;
                        this.patientSettings = navParams.data.patientSettings;

                        let params = {
                            'selectedDate': navParams.data.selectedDate,
                            'selectedTime': navParams.data.selectedTime
                        };

                        this.data.tabs = [{ page: "AppLoggedGeneralTabPage", title: "General Details" , params: params},
                            { page: "AppLoggedActivityTabPage", title: "Activity Details",  params: params},
                            { page: "AppLoggedMedicinesTabPage", title: "Medicine Details", params: params}
                        ];
                    }
                }

    calculateAge(birthDate){
        var birthday = new Date(birthDate);
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    backToPatientHome(){
        this.navCtrl.setRoot("AppPatientHomePage",{'data': this.patientData});
    }
}
