import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppLoggedActivityHome} from "./app-logged-activity-home";


@NgModule({
  declarations: [
      AppLoggedActivityHome
  ],
  imports: [
    IonicPageModule.forChild(AppLoggedActivityHome)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})

export class AppLoggedActivityHomeModule {}
