import { Component, AfterViewInit } from "@angular/core";
import {
  AlertController,
  IonicPage,
  ModalController,
  NavController,
  NavParams,
  Platform,
  ViewController,
  Tabs,
  App,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from "rxjs/Observable";
import * as _ from "lodash";
import { AppPatientEventModalPage } from "../app-patient-event-modal/app-patient-event-modal";
import { AppPatientMedicineListPage } from "../app-patient-medicine-list/app-patient-medicine-list";
import { Http } from "@angular/http";
import { Storage } from "@ionic/storage";
import { environment } from "../../environment/environment";
import { ToastService } from "../../services/toast-service";

// import {Tabs} from "ionic-angular";

@IonicPage()
@Component({
  selector: "app-patient-event-settings",
  templateUrl: "app-patient-event-settings.html",
  providers: [DatabaseService],
})
export class AppPatientEventSettingsPage {
  data: any = {};
  events: any = [];
  patientData: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  count: number;
  EventData: any;
  isloadmore: boolean = false;
  offset: number = 0;
  userLoginData: any;

  constructor(
    public storage: Storage,
    private toastCtrl: ToastService,
    public http: Http,
    public navCtrl: NavController,
    private app: App,
    navParams: NavParams,
    private alertCtrl: AlertController,
    private databaseService: DatabaseService,
    private loadingService: LoadingService,
    public modalCtrl: ModalController,
  ) {
    // if (!_.isEmpty(navParams.data)) {
    //     this.patientData = navParams.data;
    //     this.loadEventSettings();
    // }
  }

  addEventSettingItem() {
    let modal = this.modalCtrl.create(
      AppPatientEventModalPage,
      { "type": "add", "events": this.events, "patientData": this.patientData },
      { cssClass: "on-board " },
    );
    // let modal = this.modalCtrl.create(AppPatientsEventMasterModalPage, { 'events': this.events, 'patientData': this.patientData });
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.EventData = [];
        this.tmparr = [];
        this.loadEventSettings();
      }
    });
  }
  doRefresh() {
    this.loadingService.show();
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.loadingService.hide();
    }, 100);
  }
  tmparr = [];
  loadEventSettings(infiniteScroll?) {
    this.data.action = "geteventmasterdata";
    this.data.userid = this.userLoginData.id;
    this.data.offset = 0;
    this.data.keyword = "";
    this.data.appsecret = this.appsecret;
    console.log("this.data", this.data);
    this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
      (data) => {
        this.loadingService.show();
        let result = JSON.parse(data["_body"]);
        console.log("result", result);
        if (infiniteScroll) {
          infiniteScroll.complete();
        }
        if (result.status == "notmatchingkey") {
          this.doRefresh();
        }
        if (result.status == "success") {
          this.loadingService.hide();
          if (result.data0 !== null) {
            for (var i in result.data0) {
              this.tmparr.push(result.data0[i]);
            }
            this.EventData = this.tmparr;
            if (result.data0) {
              this.count = result.data0[0].count;
            }
            else
            {
              this.count=0;
            }
            console.log("MedicenData", this.EventData);
          } else {
            this.loadingService.hide();
            this.data = [];
            this.data.keys = [];
            this.EventData = [];
          }
        } else {
          this.loadingService.hide();
          // this.loadingService.hide();
          // this.toastCtrl.presentToast("There is a no data available.");
          // this.navCtrl.setRoot("AppDashboardPage");
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      },
      (err) => {
        this.loadingService.hide();
        console.log(err);
      },
    );
  }

  loadMore(infiniteScroll) {
    if (this.EventData.length >= this.count && this.count == 0) {
      infiniteScroll.enable(false);
    } else {
      this.offset = this.offset + 10;
      this.loadEventSettings(infiniteScroll);
    }
  }

  editEvent(event, index) {
    let modal = this.modalCtrl.create(
      AppPatientEventModalPage,
      {
        "event": event,
        "patientData": this.patientData,
        "type": "edit",
        "index": index,
        "onboard": true,
      },
      {
        cssClass: "on-board ",
      },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.EventData = [];
        this.tmparr = [];
        this.loadEventSettings();
      }
    });
  }

  tabNextButton() {
    this.navCtrl.parent.select(1);
  }

  deleteEventItem(item, index) {
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete this record?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
          console.log("Cancel clicked");
        },
      }, {
        text: "Delete",
        handler: () => {
          let self = this;
          // self.databaseService.deleteEventItemData(this.patientData, index);
          // setTimeout(function () { self.loadEventSettings(); }, 1000);
          this.data.action = "deleteevent";
          this.data.id = item.id;
          this.data.appsecret = this.appsecret;
          this.http.post(this.baseurl + "deletedata.php", this.data).subscribe(
            (data) => {
              this.loadingService.show();
              let result = JSON.parse(data["_body"]);
              if (result.status == "success") {
                this.loadingService.hide();
                self.toastCtrl.presentToast("Medicine is deleted successfully");
                this.EventData = [];
                this.tmparr = [];
                this.loadEventSettings();
              } else {
                this.loadingService.hide();
                self.toastCtrl.presentToast("There is an error deleting data!");
              }
            },
            (err) => {
              this.loadingService.hide();
              console.log(err);
            },
          );
        },
      }],
    });
    alert.present();
  }

  GotoHome() {
    this.loadingService.show();
    this.data.action = "setloginfirsttime";
    this.data.userid = this.userLoginData.id;
    console.log("data", this.data);
    this.data.appsecret = this.appsecret;
    this.http.post(this.baseurl + "login.php", this.data).subscribe((data) => {
      let result = JSON.parse(data["_body"]);
      if (result.status == "success") {
        this.loadingService.hide();
        this.app.getRootNav().setRoot("AppTimelinePage");
        console.log("result", result);
      } else {
        // this.showToast("Something went wrong");
        // this.toastCtrl.presentToast("Something went wrong, Please try again.");
      }
    }, (err) => {
      console.log(err);
    });
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      return null;
    }
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        this.loadEventSettings();
      }
    });
  }
  ionViewDidLoad() {
    this.getuserLoginData();
  }
}
