import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import {AppPatientEventSettingsPage} from "./app-patient-event-settings";



@NgModule({
  declarations: [
      AppPatientEventSettingsPage
  ],
  imports: [
    IonicPageModule.forChild(AppPatientEventSettingsPage)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})

export class AppPatientEventSettingsModule {}
