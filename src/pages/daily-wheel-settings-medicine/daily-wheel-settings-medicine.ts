import { Component, ViewChild } from "@angular/core";
import {
  AlertController,
  IonicPage,
  NavController,
  NavParams,
  Navbar,
  ModalController,
} from "ionic-angular";
import { DatabaseService } from "../../services/database-service";
import { MyApp } from "../../app/app.component";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Storage } from "@ionic/storage";
import * as _ from "lodash";
import { Http } from "@angular/http";
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";
import { LoadingService } from "../../services/loading-service";
import { AppMedicineViewallNotificationPage } from "../app-medicine-viewall-notification/app-medicine-viewall-notification";
import { AppAddMedicineMasterPage } from "../app-add-medicine-master/app-add-medicine-master";
import { AppMedicinePrescriptionIssuedPage } from "../app-medicine-prescription-issued/app-medicine-prescription-issued";
import { IfObservable } from "rxjs/observable/IfObservable";

/**
 * Generated class for the DailyWheelSettingsMedicinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-daily-wheel-settings-medicine',
  templateUrl: 'daily-wheel-settings-medicine.html',
  providers: [DatabaseService]
})
export class DailyWheelSettingsMedicinePage {

  SelectList: FormGroup;
  currentHCP: any[];
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  data: any = {};
  arrMedicineData: any;
  MedicineData: any = [];
  SearchData: any = [];
  patientData: any = {};
  descending: boolean = false;
  order: number;
  terms: string = "";
  column: string = "medicinename";
  patientSession: boolean;
  patientPreSession: boolean;
  patientSessionData: any = [];
  usertype: string;
  offset: number = 0;
  count: number = 0;
  username: string;
  userid: string;
  ut: boolean;
  dropdown: any = [];
  notselected: boolean;
  selected1: any = [];
  c = [];
  isloadmore: boolean = false;
  result: any = [];
  Ishide: boolean = true;
  IsIndependent: boolean = false;
  IssamePage: boolean = false;
  @ViewChild(Navbar)
  navBar: Navbar;
  masterDataVisbleTo: boolean = true;
  hcpid: any;
  onboard: boolean = false;
  userLoginData: any;
  patientType: any;
  hcpkey: any;
  errmsg: boolean = false;
  dependent: boolean = true;

  constructor(
    private loadingService: LoadingService,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private databaseService: DatabaseService,
    public myApp: MyApp,
    public storage: Storage,
    public formBuilder: FormBuilder,
    public http: Http,
    private toastCtrl: ToastService,
  ) {
    if (!_.isEmpty(navParams.data)) {
      this.patientData = navParams.data.data;
    }

    this.SelectList = formBuilder.group({
      currentHCP: [""],
    });
  }

  appIntialize1() {
    if (this.patientType == "independent") {
      this.dependent = false;
    }
    if (this.userLoginData.usertype == "patient") {
      this.patientPreSession = true;
      this.masterDataVisbleTo = false;
    } else {
      this.masterDataVisbleTo = true;
    }
    this.storage.get("patientSession").then((val) => {
      if (val != undefined || this.userLoginData.usertype == "patient") {
        if (this.patientType == "independent") {
          this.IsIndependent = true;
        } else {
          this.IsIndependent = false;
        }
        this.patientSession = true;
      }
    });

    this.storage.get("user").then((val) => {
      this.patientData = val;
    });

    this.myApp.initializeApp();
    this.username = this.userLoginData.email;
    this.usertype = this.userLoginData.usertype;
    this.userid = this.userLoginData.id;
    this.ut = (this.usertype == "healthcareprovider" ? false : true);
    this.dropdown = this.getHCPData(this.userid, this.usertype);

    this.get("currentHCP").then((result) => {
      if (result != null) {
        this.notselected = false;
        this.selected1 = result;
      } else {
        this.notselected = true;
        this.selected1 = this.dropdown;
      }
    });
    this.loadMasterMedicineData();
  }

  ViewMedicineMaster() {
    this.patientSession = false;
    this.IssamePage = false;
    this.c = [];
    this.onboard = true;
    this.SearchData = [];
    this.MedicineData = [];
    this.loadMasterMedicineData("");

    //this.navCtrl.setRoot(AppMedicineMasterListPage);
  }

  ionViewDidLoad() {
    this.order = -1;
    this.getHCPid();
  }

  AddMedicine() {
    console.log("helloooooo");
    let modal = this.modalCtrl.create(
      AppAddMedicineMasterPage,
      {
        "type": "add",
        "MedicineData": this.MedicineData,
        "patientData": this.patientData,
      },
      { cssClass: "on-addmedicine" },
    );
    // let modal = this.modalCtrl.create(AppMedicineMasterListModalPage, { 'type': 'add', 'MedicineData': this.medicines, 'patientData': this.patientData, 'onboard': true }, { cssClass: 'on-board ' });
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.c = [];
        this.SearchData = [];
        this.MedicineData = [];

        this.loadMasterMedicineData();

      }
    });
  }

  AddPrescription() {
    console.log("medicenedata", this.MedicineData);
    console.log("patientData", this.patientData);
    this.navCtrl.push(
      "AppMedicinePrescriptionIssuedPage",
      { "MedicineData": this.MedicineData, "patientData": this.patientData },
    );
  }

  loadMasterMedicineData(infiniteScroll?) {
    this.loadingService.show();
    this.storage.get("patientSessionData").then((data) => {
      this.patientSessionData = data;
    });
    this.storage.get("patientSession").then((val) => {
      this.storage.get("user").then((patientData) => {
        if (
          val == true && val != undefined &&
          (this.userLoginData.usertype == "patient" && this.IssamePage)
        ) {
          if (this.patientType == "independent") {
            this.IsIndependent = true;
          } else {
            this.IsIndependent = false;
            this.Ishide = false;
          }
          if (this.userLoginData.usertype == "healthcareprovider") {
            this.IsIndependent = true;
          }
          if (this.userLoginData.usertype == "patient") {
            this.patientData = {
              id: this.userLoginData.id,
              email: this.userLoginData.email,
              name: this.userLoginData.firstname,
            };
          }
          patientData = this.patientData;
          this.data.hcpid = this.hcpid;
          this.data.action = "getpatientmedicinedata";
          if (this.patientType == "independent") {
            this.data.userid = this.userLoginData.id;
            this.data.hcpid = 0;
          } else {
            this.data.userid = patientData.id;
          }
          //this.data.parentid = patientData.id;
          this.data.offset = this.offset;
          this.data.keyword = "";
          this.data.appsecret = this.appsecret;
          console.log("this.data", this.data);
          if (
            this.userLoginData.usertype == "doctor" ||
            this.userLoginData.usertype == "nurse"
          ) {
            this.data.hcpid = this.hcpid;
            this.IsIndependent = true;
          }
          if (this.userLoginData.usertype == "heatlhcareprovider") {
            this.data.hcpid = this.userLoginData.id;
          }
          this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
            (data) => {
              let result = JSON.parse(data["_body"]);
              if (infiniteScroll) {
                infiniteScroll.complete();
              }
              console.log("result", result);
              if (result.status == "success") {
                this.loadingService.hide();
                if (result.data !== null) {
                  for (var i in result.data) {
                    this.c.push(result.data[i]);
                  }
                  console.log("this.c :::", this.c);
                  this.MedicineData = this.c;
                  this.SearchData = this.c;
                  console.log("MedicenData", this.MedicineData);
                  this.count = result.count;
                } else {
                  this.errmsg = true;
                  this.loadingService.hide();
                  this.data = [];
                  this.toastCtrl.presentToast("There is a no data available.");
                  this.data.keys = [];
                  this.MedicineData = [];
                  this.SearchData = [];
                }
              } else {
                this.errmsg = true;
                this.loadingService.hide();
              }
            },
            (err) => {
              this.errmsg = true;
              this.loadingService.hide();
              console.log(err);
            },
          );
        } else {
          this.data.action = "getmedicinemasterdata";
          //this.data.offset=this.offset;
          this.data.offset = this.offset;
          this.data.keyword = "";
          this.data.appsecret = this.appsecret;
          if (
            this.patientType == "independent" ||
            this.userLoginData.usertype == "healthcareprovider"
          ) {
            this.data.userid = this.userLoginData.id;
            this.data.hcpid = 0;
            console.log("this.userid", this.data.userid);
          } else {
            this.data.userid = this.hcpid;
          }
          //this.data.userid = this._cookieService.get('userid');
          console.log("this.data.keyword", this.data);
          this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
            (data) => {
              this.loadingService.hide();

              let result = JSON.parse(data["_body"]);
              if (result.status == "success") {
                if (result.data0 == null) {
                  this.errmsg = true;
                } else {
                  this.errmsg = false;
                }
                this.loadingService.hide();
                for (var i in result.data0) {
                  for (var j in result.data1) {
                    if (result.data0[i].id == result.data1[j].id) {
                      result.data0[i].numofdata = result.data1[j].numofdata;
                    }
                  }
                  this.c.push(result.data0[i]);
                }
                console.log("this.c :::", this.c);
                this.MedicineData = this.c;
                this.SearchData = this.c;
                if (result.data0) {
                  this.count = result.data0[0].count;
                } else {
                  this.count = 0;
                }
                console.log("this.c :::", this.count);
                console.log("MedicenData", this.count);
                if (infiniteScroll) {
                  infiniteScroll.complete();
                }
              } else {
                this.c = [];
                this.SearchData = [];
                this.MedicineData = [];
                this.errmsg = true;
                this.loadingService.hide();
                this.toastCtrl.presentToast(
                  "There is a problem adding data, please try again",
                );
              }
            },
            (err) => {
              this.errmsg = true;
              this.loadingService.hide();
              console.log(err);
            },
          );
        }
      });
    });
  }

  loadMore(infiniteScroll) {
    if (this.patientData || !this.patientData) {
      if (this.MedicineData.length >= this.count || this.count == 0 || this.count <= 10) {
        infiniteScroll.enable(false);
        console.log("aaaaaa");
      } else {

        this.offset = this.offset + 10;
        this.loadMasterMedicineData(infiniteScroll);
      }
    }
  }

  MedicineUpdate(item, index) {
    console.log("item", item);
    console.log(index);
    console.log(this.patientData);
    let modal = this.modalCtrl.create(
      "AppMedicineMasterListModalPage",
      {
        "MedicineData": item,
        "patientData": this.patientData,
        "index": index,
        "type": "edit",
        onboard: this.onboard,
      },
      { cssClass: "on-addmedicine" },
    );
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.c = [];
        this.SearchData = [];
        this.MedicineData = [];
        this.loadMasterMedicineData();
      }
    });
  }

  // ionViewDidEnter() {
  //   this.navBar.backButtonClick = (e: UIEvent) => {
  //     this.navCtrl.setRoot("AppPatientsPage");
  //   };
  // }

  sortField(fieldname) {
    this.column = fieldname;
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }

  setFilteredMedicineNameData() {
    // if (this.terms == '') {
    //   this.doRefresh();
    // }
    if (!this.patientSession) {
      this.data.action = "getmedicinemasterdata";
      console.log("aaaaaa");
    } else {
      this.data.action = "getpatientmedicinedata";
      console.log("ppppp");
    }

    this.data.offset = 0;
    this.data.keyword = this.terms;
    this.data.appsecret = this.appsecret;
    this.http.post(this.baseurl + "getdata.php", this.data).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);
        if (result.status == "success") {
          this.SearchData = result.data0;
          console.log("ppppp", result);
        } else {
          this.SearchData = [];
          this.isloadmore = true;
        }
      },
      (err) => {
        this.SearchData = [];
      },
    );
  }

  setFilteredMedicineData() {
    this.SearchData = this.MedicineData.filter((Medicine) => {
      return Medicine.name.toLowerCase().indexOf(this.terms.toLowerCase()) > -1;
    });
  }

  deletePatientMedicineData(item, index, patientData) {
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete Patients record?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
        },
      }, {
        text: "Delete",
        handler: () => {
          let self = this;
          this.data.action = "deletepatientmedicine";
          this.data.id = item.id;
          this.data.appsecret = this.appsecret;
          this.http.post(this.baseurl + "deletedata.php", this.data).subscribe(
            (data) => {
              this.loadingService.show();
              let result = JSON.parse(data["_body"]);

              if (result.status == "success") {
                this.loadingService.hide();
                self.toastCtrl.presentToast("Medicine is deleted successfully");
                this.SearchData = [];
                this.MedicineData = [];
                this.loadMasterMedicineData();
              } else {
                this.loadingService.hide();
                self.toastCtrl.presentToast("There is an error deleting data!");
              }
            },
            (err) => {
              this.loadingService.hide();
              console.log(err);
            },
          );
        },
      }],
    });
    alert.present();
  }

  deleteMedicineData(item, index, patientData) {
    // item = [item];
    let alert = this.alertCtrl.create({
      title: "Confirm Delete",
      message: "Do you want to delete this record?",
      buttons: [{
        text: "Cancel",
        role: "cancel",
        handler: () => {
        },
      }, {
        text: "Delete",
        handler: () => {
          let self = this;
          this.data.action = "deletemedicine";
          this.data.id = item.id;
          this.data.appsecret = this.appsecret;
          this.http.post(this.baseurl + "deletedata.php", this.data).subscribe(
            (data) => {
              this.loadingService.show();
              let result = JSON.parse(data["_body"]);

              if (result.status == "success") {
                this.loadingService.hide();
                self.toastCtrl.presentToast("Medicine is deleted successfully");
                this.SearchData = [];
                this.MedicineData = [];
                this.loadMasterMedicineData();
              } else {
                this.loadingService.hide();
                self.toastCtrl.presentToast("There is an error deleting data!");
              }
            },
            (err) => {
              console.log(err);
              this.loadingService.hide();
            },
          );
        },
      }],
    });
    alert.present();
  }

  comparer(otherArray) {
    return function (current, key) {
      return otherArray.filter(function (other, k) {
        return other.id == current.id; // && other.name == current.name
        // return other == current
      }).length == 0;
    };
  }

  doRefresh() {
    this.loadingService.show();
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.loadingService.hide();
    }, 2000);
  }

  logout() {
    this.databaseService.logout();
  }

  onMedicineNotification(item) {
    let modal = this.modalCtrl.create(
      AppMedicinePrescriptionIssuedPage,
      { medicinedata1: item },
      { cssClass: "on-addmedicine" },
    );
    modal.onDidDismiss((data) => {
      if (data) {
        this.doRefresh();
      }
    });
    modal.present();
  }

  async getHCPid() {
    await this.get("currentHCP").then((result) => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      } else {
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get("userLogin").then((result) => {
      if (result != null) {
        this.userLoginData = result;
        console.log("this.userloigndata", this.userLoginData);
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
        }
        this.getpatientType();
      }
    });
  }

  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get("patientType").then((result) => {
      console.log("patienttypesdfdsfsdsdf", result);
      if (
        (!result || result == null || result == undefined) &&
        this.userLoginData == "patient"
      ) {
        this.doRefresh();
      } else {
        this.patientType = result;
        // this.appIntialize1();
        this.ViewMedicineMaster();
        console.log("sekeccdffvfdgdfgdfg", this.patientType);
      }
    });
  }

  getHCPData(userid, usertype) {
    var hcp1: any = [];
    let self = this;
    var webServiceData: any = {};
    webServiceData.action = "gethcpdata";
    webServiceData.id = userid;
    webServiceData.usertype = usertype;
    webServiceData.appsecret = this.appsecret;
    // self.data.id = this._cookieService.get('userid');
    self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(
      (data) => {
        let result = JSON.parse(data["_body"]);

        if (result.status == "success") {
          // result.data;
          hcp1 = hcp1.push(result.data);
          // hcp1 = result.data;
          console.log("hcp12345", hcp1);
          console.log(hcp1);
          // return result1;
        } else {
          // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
        }
      },
      (err) => {
        console.log(err);
      },
    );
    return hcp1;
  }

  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log("set string in storage: " + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      if (result != null) {
        return result;
      } else {
        return null;
      }
    } catch (reason) {
      return null;
    }
  }

  remove(key: string) {
    this.storage.remove(key);
  }

  selectEmployee($event) {
    this.set("currentHCP", $event);
    this.doRefresh();
  }

  updateAddtoWheel(item, e) {
    this.loadingService.show();
    console.log("eventwheel", e.checked);
    console.log("eventitem", item);
    this.data.appsecret = this.appsecret;
    this.data.action = "updatestandardwheelmedicine";
    this.data.standardwheel = e.checked;
    this.data.id = item.id;
    this.http.post(this.baseurl + "updatedata.php", this.data).subscribe(
      (data) => {
        this.loadingService.hide();
        this.toastCtrl.presentToast("Wheel data updated");
      },
      (err) => {
        console.log("err", err);
        this.loadingService.hide();
      },
    );
  }

  onViewAllNotifications() {
    console.log("helloooooo0000000099090909");
    this.navCtrl.push(AppMedicineViewallNotificationPage);
  }
}
