import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyWheelSettingsMedicinePage } from './daily-wheel-settings-medicine';

@NgModule({
  declarations: [
    DailyWheelSettingsMedicinePage,
  ],
  imports: [
    IonicPageModule.forChild(DailyWheelSettingsMedicinePage),
  ],
})
export class DailyWheelSettingsMedicinePageModule {}
