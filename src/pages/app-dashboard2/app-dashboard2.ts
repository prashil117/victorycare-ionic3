import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, Navbar, NavController, NavParams, Tabs } from 'ionic-angular';

/**
 * Generated class for the AppDashboard2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-dashboard2',
  templateUrl: 'app-dashboard2.html',
})
export class AppDashboard2Page {
  @ViewChild("tabs") TimelineTabs: Tabs;
  @ViewChild(Navbar) navBar: Navbar;
  data1: any = {};
  patientData: any = {};
  tabs1: any = [];
  prepopulated: any;
  eventData: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
    
    this.tabs1 = [
      { page: "AppWeeklyDashboard2ReportPage", title: "Today" },
      { page: "AppWeeklyDashboard2ReportPage", title: "Week" }
    ];
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppDashboard2Page');
    this.navCtrl.popToRoot();
  }


}
