import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppDashboard2Page } from './app-dashboard2';

@NgModule({
  declarations: [
    AppDashboard2Page,
  ],
  imports: [
    IonicPageModule.forChild(AppDashboard2Page),
  ],
})
export class AppDashboard2PageModule {}
