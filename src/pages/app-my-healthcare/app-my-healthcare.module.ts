import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppMyHealthcarePage } from './app-my-healthcare';

@NgModule({
  declarations: [
    AppMyHealthcarePage,
  ],
  imports: [
    IonicPageModule.forChild(AppMyHealthcarePage),
  ],
})
export class AppMyHealthcarePageModule {}
