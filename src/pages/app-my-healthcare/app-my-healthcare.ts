import { Component, AfterViewInit } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { DatabaseService } from "../../services/database-service";
import { LoadingService } from "../../services/loading-service";
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { Validators, FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { MyApp } from "../../app/app.component";
import { Http } from '@angular/http';
import { ToastService } from "../../services/toast-service";
import { environment } from "../../environment/environment";

/**
 * Generated class for the AppMyHealthcarePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-my-healthcare',
  templateUrl: 'app-my-healthcare.html',
  providers: [DatabaseService]
})
export class AppMyHealthcarePage {
  SelectList: FormGroup;
  currentHCP: any[];
  data: any = {};
  baseurl = environment.apiUrl;
  appsecret = environment.appsecret;
  eventsData: any = [];
  patientData: any = {};
  userLoginData: any;
  patientType: any;
  SearchData: any = [];
  terms: string;
  healthcareProviders: any = [];
  result: any = [];
  hcpid: any;
  usertype: string;
  username: string;
  userid: string;
  ut: boolean;
  dropdown: any = [];
  notselected: boolean;
  selected1: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, public storage: Storage, private alertCtrl: AlertController, private databaseService: DatabaseService,  private loadingService: LoadingService, public formBuilder: FormBuilder, public myApp: MyApp, public http: Http, private toastCtrl: ToastService) {
    ////////// HCP DropDown Code Starts /////////////
    this.SelectList = formBuilder.group({
      currentHCP: ['']
    });
  }

  appIntialize1() {
    this.myApp.initializeApp();
    this.username = this.userLoginData.email;
    this.usertype = this.userLoginData.usertype;
    this.userid = this.userLoginData.id;
    this.ut = (this.usertype == 'healthcareprovider' ? false : true);

    console.log(this.userid);
    // alert(this.usertype);
    this.dropdown = this.getHCPData(this.userid, this.usertype);
    console.log(this.dropdown);
    var result = null;
    this.get('currentHCP').then(result => {
      console.log('Username1: ' + result);
      if (result != null) {
        this.notselected = false;
        this.selected1 = result;
        console.log('Username: ' + this.selected1);
      } else {
        this.notselected = true;
        console.log(this.dropdown);
        this.selected1 = this.dropdown;
      }
    });
    this.gethcpdatafordoctor();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppMyHealthcarePage');
    this.getHCPid();
  }

  logout() {
    this.databaseService.logout();
  }
  gethcpdatafordoctor() {
    this.loadingService.show();
    this.data.action = "gethcpdatafordoctor";
    this.data.id = this.userLoginData.id;
    this.data.appsecret = this.appsecret;
    this.http.post(this.baseurl + "getuser.php", this.data).subscribe(data => {
      
      let result = JSON.parse(data["_body"]);
      console.log(result.data);
      if (result.status == 'success') {
        this.loadingService.hide();
        if (result.data !== null) {
          this.data.values = result.data
          this.SearchData = result.data;
        } else {
          this.loadingService.hide();
          this.data = []
          this.data.keys = [];
          this.data.values = [];
          this.SearchData = [];
        }
      } else {
        this.loadingService.hide();
        this.toastCtrl.presentToast("There is a problem adding data, please try again");
        // this.navCtrl.setRoot("AppDashboardPage");
        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
      }
    }, err => {
      console.log(err);
      this.loadingService.hide();
    });
  }
  setFilteredPatientsData() {
    this.SearchData = this.data.values.filter((Patient) => {
      console.log(Patient);
      return Patient.firstname.toLowerCase().indexOf(this.terms.toLowerCase()) > -1;
    });
  }

  editHealthcareProvider(email, editable) {
    this.loadingService.show();
    console.log(email);
    console.log(editable);
    this.data.action = "getuserbyid";
    this.data.userid = email;
    this.data.appsecret = this.appsecret;
    console.log(this.data.userid);
    this.http.post(this.baseurl + "getuser.php", this.data).subscribe(data => {
      let result = JSON.parse(data["_body"]);
      if (result.status == 'exists') {
        this.loadingService.hide();
        this.data = result.data;
        this.data.name = result.data.firstname + ' ' + result.data.lastname;
        this.navCtrl.push("AppHealthcareproviderEditPage", { 'data': this.data, 'action': 'edit', 'editable': editable });
      } else {
        this.loadingService.hide();
        this.toastCtrl.presentToast("Unable to fetch data");
        // self.errorMessage = "Unable to fetch data";
      }
    }, err => {
      this.loadingService.hide();
      console.log(err);
    });
  }
  deleteHealthcareProvidersData(item) {
    var item1: any = [];
    item1 = [item];
    console.log(item1[0]);
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Do you want to leave from this Healthcare Provider???',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            let self = this;
            var value: any = [];
            var keys: any = [];
            var result: any = [];
            var currentData = [{ id: self.userLoginData.id, email: self.userLoginData.email, name: self.userLoginData.firstname }];
            console.log('Delete Ok clicked');
            this.data.action = "unlinkhcpfromdoctor";
            this.data.userid = this.userLoginData.id;
            this.data.parentid = item1[0].parentid;
            this.data.appsecret = this.appsecret;
            console.log(this.data);
            this.http.post(this.baseurl + "removeuser.php", this.data).subscribe(data => {
              this.loadingService.show();
              let result = JSON.parse(data["_body"]);
              console.log(result.status);
              if (result.status == 'success') {
                this.loadingService.hide();
                // self.navCtrl.setRoot("AppUserEditPage",{'data': self.data});
                self.toastCtrl.presentToast("Healthcare Provider is unlinked successfully");
                this.navCtrl.setRoot(this.navCtrl.getActive().component);
                // self.logout();
              } else {
                this.loadingService.hide();
                self.toastCtrl.presentToast("There is an error unlinking HCP!");
                // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
              }
            }, err => {
              this.loadingService.hide();
              console.log(err);
            });
          }
        }
      ]
    });
    alert.present();
  }
  comparer(otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        return other.email == current.email // && other.name == current.name
        // return other == current
      }).length == 0;
    }
  }
  onAddHealthcareprovider(data, addexist, showSaveButton) {
    console.log(data);
    this.navCtrl.push("AppHealthcareproviderEditPage", { 'hcpdata': data, 'addexist': addexist, 'showSaveButton': showSaveButton, 'editable': false });
  }
  ////////// HCP DropDown Code Starts /////////////
  getHCPData(userid, usertype) {
    console.log(userid);
    console.log(usertype);
    var keys: any = [];
    var hcp1: any = [];
    let self = this;
    var webServiceData: any = {};
    webServiceData.action = "gethcpdata";
    webServiceData.id = userid;
    webServiceData.appsecret = this.appsecret;
    webServiceData.usertype = usertype;
    self.data.id = this.userLoginData.id;
    self.http.post(this.baseurl + "getuser.php", webServiceData).subscribe(data => {
      this.loadingService.show();
      let result = JSON.parse(data["_body"]);
      console.log(result);
      if (result.status == 'success') {
        this.loadingService.hide();
        console.log(result.data);
        // result.data;
        hcp1 = hcp1.push(result.data);
        // hcp1 = result.data;
        console.log(hcp1);
        // return result1;
      } else {
        this.loadingService.hide();
        // let modal = self.modalCtrl.create(AppModalPage, {'error':'there is an error deleting data!'});
      }
    }, err => {
      this.loadingService.hide();
      console.log(err);
    });
    return hcp1;

  }
  async set(key: string, value: any): Promise<any> {
    try {
      const result = await this.storage.set(key, value);
      console.log('set string in storage: ' + result);
      return true;
    } catch (reason) {
      console.log(reason);
      return false;
    }
  }

  async get(key: string): Promise<any> {
    try {
      const result = await this.storage.get(key);
      console.log('storageGET: ' + key + ': ' + result);
      if (result != null) {
        return result;
      }
      return null;
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }

  async getHCPid() {
    await this.get('currentHCP').then(result => {
      if (result != null) {
        this.hcpid = result;
        this.getuserLoginData();
      } else {
        this.getuserLoginData();
      }
    });
  }

  async getuserLoginData() {
    await this.get('userLogin').then(result => {
      if (result != null) {
        this.userLoginData = result;
        if (this.userLoginData.usertype == "healthcareprovider") {
          this.hcpid = this.userLoginData.id;
        }
        this.getpatientType();
      }
    });
  }


  async getpatientType() {
    console.log("this.userdata", this.userLoginData);
    await this.get('patientType').then(result => {
      console.log("patienttypesdfdsfsdsdf", result)
      if ((!result || result == null || result == undefined) && this.userLoginData == "patient") {
        this.doRefresh();
      }
      else {
        this.patientType = result;
        this.appIntialize1();
        console.log("sekeccdffvfdgdfgdfg", this.patientType)
      }
    });
  }

  remove(key: string) {
    this.storage.remove(key);
  }

  doRefresh(event = '') {
    this.loadingService.show();
    setTimeout(() => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.loadingService.hide();
    }, 2000);
  }

  selectEmployee($event) {
    this.doRefresh();
    this.set('currentHCP', $event);
  }

  ////////// HCP DropDown Code ends /////////////
}
