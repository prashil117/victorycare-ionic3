import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, ModalController, NavController, NavParams } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AlertController } from 'ionic-angular';

import { FCM } from '@ionic-native/fcm';
import { SettingsProvider } from "../services/SettingsProvider";
import { AppMedicineMasterListPage } from '../pages/app-medicine-master-list/app-medicine-master-list';
import { AppPatientsContactsPage } from '../pages/app-patients-contacts/app-patients-contacts';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { LoadingService } from '../services/loading-service';
import { AppLogoutPage } from '../pages/app-logout/app-logout';
import { AppTimelinePage } from '../pages/app-timeline/app-timeline';
import { AppSettingsPage } from '../pages/app-settings/app-settings';
import { JsonPipe } from '@angular/common';
declare var document: any;
@Component({
  templateUrl: 'app.html',
  providers: [SettingsProvider],

})

export class MyApp {
  @ViewChild(Nav) nav: NavController;

  // pages: Array<{title: string, component: any, icon:any, flag:any}>;
  pages: any = [];
  rootPage = "AppWelcomeScreenPage";
  params: any;
  leftMenuTitle: string;
  selectedTheme: String;
  UserProfileImage: any = "";
  UserName: any = "";
  userTypeCookie: any = "";
  userNameCookie: any = "";
  menuCreator: boolean = true;
  image: string;
  private wide: boolean = false;
  i: any = 0;
  // const webserviceUrl = ""

  constructor(
    public alertCtrl: AlertController,
    public platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public menu: MenuController,
    public events: Events,
    public storage: Storage,
    public modalCtrl: ModalController,
    private localNotifications: LocalNotifications,
    // public navctrl: NavController,
    private settings: SettingsProvider,
    public loadingservice: LoadingService,
    private fcm: FCM,
  ) {


    this.settings.getActiveTheme().subscribe(val => this.selectedTheme = val);
    // this.events.publish('patinetData', '');
    this.splashScreen.hide();
    events.subscribe('user:created', (user, time) => {
      // console.log(user);
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.UserProfileImage = user.image;
      this.UserName = user.name;
      if (this.pages != undefined) {
        this.pages[0].title = this.UserName;
        this.pages[0].component = 'AppPatientProfilePage';
        this.pages[0].icon = this.UserProfileImage;
        this.pages[0].flag = "image";
      }
    });


    this.initializeApp();
    //this.initPushNotification();
  }
  // async presentAlert(data) {
  //   console.log("dataaaaaa", data)
  //   const alert = await this.alertCtrl.create({
  //     cssClass: 'my-custom-class',
  //     title: data.aps.alert.title,
  //     message: data.aps.alert.body,
  //     buttons: ['OK']
  //   });

  //   await alert.present();
  // }

  setWidth() {

    this.wide = true;
    this.menu.open();
  };
  // initPushNotification() {
  //   this.platform.ready().then(() => {
  //     this.splashScreen.hide();
  //     if (this.platform.is('corodova') || this.platform.is('ios')) {
  //       this.fcm.subscribeToTopic('marketing');

  //       this.fcm.getToken().then(token => {
  //         //register token
  //         console.log("new token", token);
  //         console.log(token);
  //       })

  //       this.fcm.onNotification().subscribe(data => {
  //         if (data.wasTapped) {
  //           console.log("recieved in foregorund", data)
  //         } else {
  //           this.presentAlert(data)
  //           console.log("get notification", data)
  //         };
  //       })

  //       this.fcm.onTokenRefresh().subscribe(token => {
  //         //register token
  //         console.log("tokenrefresh", token);
  //       })

  //       this.fcm.unsubscribeFromTopic('marketing');
  //     }
  //   })
  // }

  initializeApp() {
    this.platform.ready().then(() => {
      this.splashScreen.hide();

      /*this.localNotifications.schedule({
        id: 1,
        title: 'Attention',
        text: 'Simons Notification',
        data: { mydata: 'My hidden message this is' },
        // trigger: {at: new Date(new Date().getTime() + 10 * 2000)},
      });*/
      /*this.push.hasPermission()
        .then((res: any) => {

          if (res.isEnabled) {
            console.log('We have permission to send push notifications');
          } else {
            console.log('We do not have permission to send push notifications');
          }

        });*/
      // this.firebase.getToken().then(token => console.log(token)).catch(err=> console.log(err));
      // this.firebase.onNotificationOpen().subscribe(data=>{
      //   console.log(data);
      //   console.log(data.name);
      // }, err=> console.log(err));
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // this.menu.enable(true);
      // used for an example of ngFor and navigation
      // alert(this.UserProfileImage);
      this.pages = [
        // { title: 'DASHBOARD', component: "AppDashboardPage", icon: 'dashboard',flag: ""},
        // { title: this.userNameCookie, component: "AppUserEditPage", icon: '',flag: ""},
        // { title: 'MEDICINES', component: "AppMedicineMasterListPage", icon: 'medicines',flag: ""},
        // { title: 'ACTIVITY', component: "AppActivityMasterListPage", icon: 'library',flag: ""},
        // { title: 'EVENTS', component: "AppEventMasterListPage", icon: 'event',flag: ""},
        // { title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts',flag: ""}
      ];
      // alert('this.userNameCookie');

      this.storage.get('userLogin').then((val) => {
        if (val) {
          if (val.image) {
            this.image = val.image
          }
          else {
            this.image = "";
          }
        }
        // console.log(val);
        if (val != null) {
          // this.menuCreator = true;
          // this.storage.remove('allDoctorsData');
          // if(this.i == 0){
          if (val.usertype == 'superadmin' && this.menuCreator == true) {
            this.pages = [{ title: 'DASHBOARD', component: "AppDashboardPage", icon: 'dashboard', flag: "" },
            { title: 'MY PROFILE', component: "AppUserEditPage", icon: 'settings', flag: "" },
            { title: 'MEDICINES', component: "AppMedicineMasterListPage", icon: 'medicines', flag: "" },
            { title: 'ACTIVITIES', component: "AppActivityMasterListPage", icon: 'library', flag: "" },
            { title: 'WELLBEING', component: "AppEventMasterListPage", icon: 'event', flag: "" },
            // { title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts',flag: ""},
            { title: 'PATIENTS', component: "AppPatientsPage", icon: 'patient', flag: "" }, { title: 'HEALTHCARE', component: "AppHealthcareproviderPage", icon: 'hospital', flag: "" }, { title: 'DOCTORS', component: "AppDoctorsPage", icon: 'doctor', flag: "" }, { title: 'NURSES', component: "AppNursesPage", icon: 'nurse', flag: "" }];
            // this.pages = this.pages.concat({ title: 'PATIENTS', component: "AppPatientsPage", icon: 'patient',flag: ""});
            // this.pages = this.pages.concat({ title: 'HEALTHCARE', component: "AppHealthcareproviderPage", icon: 'hospital',flag: ""});
            // this.pages = this.pages.concat({ title: 'DOCTORS', component: "AppDoctorsPage", icon: 'doctor',flag: ""});
            // this.pages = this.pages.concat({ title: 'NURSES', component: "AppNursesPage", icon: 'nurse',flag: ""});
            this.storage.get('patientSession').then((val) => {
              if (val != undefined)
                this.pages = this.pages.concat({ title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts', flag: "" });
            });
          }
          else if (val.usertype == 'healthcareprovider') {
            this.pages = [{ title: 'DASHBOARD', component: "AppDashboardPage", icon: 'dashboard', flag: "" },
            { title: 'MY PROFILE', component: "AppUserEditPage", icon: 'settings', flag: "" },
            { title: 'MEDICINES', component: "AppMedicineMasterListPage", icon: 'medicines', flag: "" },
            { title: 'ACTIVITIES', component: "AppActivityMasterListPage", icon: 'library', flag: "" },
            { title: 'WELLBEING', component: "AppEventMasterListPage", icon: 'event', flag: "" },
            // { title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts',flag: ""},
            { title: 'MY PATIENTS', component: "AppMyPatientsPage", icon: 'patients', flag: "" },
            { title: 'DOCTORS', component: "AppDoctorsPage", icon: 'doctor', flag: "" }, { title: 'NURSES', component: "AppNursesPage", icon: 'nurse', flag: "" }];
            // this.pages = this.pages.concat({ title: 'MY PATIENTS', component: "AppMyPatientsPage", icon: 'patient',flag: ""});
            // this.pages = this.pages.concat({ title: 'DOCTORS', component: "AppDoctorsPage", icon: 'doctor',flag: ""});
            // this.pages = this.pages.concat({ title: 'NURSES', component: "AppNursesPage", icon: 'nurse',flag: ""});
            this.storage.get('patientSession').then((val) => {
              if (val != undefined)
                this.pages = this.pages.concat({ title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts', flag: "" });
            });
          }
          else if (val.usertype == 'doctor') {
            this.pages = [{ title: 'DASHBOARD', component: "AppDashboardPage", icon: 'dashboard', flag: "" },
            { title: 'MY PROFILE', component: "AppUserEditPage", icon: 'settings', flag: "" },
            { title: 'MEDICINES', component: "AppMedicineMasterListPage", icon: 'medicines', flag: "" },
            { title: 'ACTIVITIES', component: "AppActivityMasterListPage", icon: 'library', flag: "" },
            { title: 'WELLBEING', component: "AppEventMasterListPage", icon: 'event', flag: "" },
            // { title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts',flag: ""},
            { title: 'MY PATIENTS', component: "AppMyPatientsPage", icon: 'patients', flag: "" }, { title: 'MY HEALTHCARE', component: "AppMyHealthcarePage", icon: 'hospital', flag: "" }, { title: 'MY NURSES', component: "AppMyNursesPage", icon: 'nurse', flag: "" }];
            // this.pages = this.pages.push(pages1);
            // this.pages = this.pages.concat({ title: 'MY HEALTHCARE', component: "AppMyHealthcarePage", icon: 'hospital',flag: ""});
            // this.pages = this.pages.concat({ title: 'MY NURSES', component: "AppMyNursesPage", icon: 'nurse',flag: ""});
            this.storage.get('patientSession').then((val) => {
              if (val != undefined)
                this.pages = this.pages.concat({ title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts', flag: "" });
            });
          }
          else if (val.usertype == 'nurse') {
            this.pages = [{ title: 'DASHBOARD', component: "AppDashboardPage", icon: 'dashboard', flag: "" },
            { title: 'MY PROFILE', component: "AppUserEditPage", icon: 'settings', flag: "" },
            { title: 'MEDICINES', component: "AppMedicineMasterListPage", icon: 'medicines', flag: "" },
            { title: 'ACTIVITIES', component: "AppActivityMasterListPage", icon: 'library', flag: "" },
            { title: 'WELLBEING', component: "AppEventMasterListPage", icon: 'event', flag: "" },
            // { title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts',flag: ""},
            { title: 'MY PATIENTS', component: "AppMyPatientsPage", icon: 'patients', flag: "" }, { title: 'MY HEALTHCARE', component: "AppMyHealthcarePage", icon: 'hospital', flag: "" }, { title: 'MY DOCTORS', component: "AppMyDoctorsPage", icon: 'doctor', flag: "" }];

            // this.pages = this.pages.concat({ title: 'MY PATIENTS', component: "AppMyPatientsPage", icon: 'patient',flag: ""});
            // this.pages = this.pages.concat({ title: 'MY HEALTHCARE', component: "AppMyHealthcarePage", icon: 'hospital',flag: ""});
            // this.pages = this.pages.concat({ title: 'MY DOCTORS', component: "AppMyDoctorsPage", icon: 'doctor',flag: ""});
            this.storage.get('patientSession').then((val) => {
              if (val != undefined)
                this.pages = this.pages.concat({ title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts', flag: "" });
            });
          }
          else if (val.usertype == 'patient') {

            this.pages = [
              { title: 'WELLBEING DASHBOARD', component: "AppReportPage", icon: 'dashboard', flag: "" },
              { title: 'TIMELINE', component: "AppTimelinePage", icon: 'timeline', flag: "" },
              { title: 'MEDICINES', component: "AppMedicineMasterListPage", icon: 'medicines', flag: "" },
              { title: 'ACTIVITIES', component: "AppActivityMasterListPage", icon: 'library', flag: "" },
              { title: 'WELLBEING', component: "AppEventMasterListPage", icon: 'event', flag: "" },
              { title: 'LIBRARY', component: "AppLibraryPage", icon: 'event', flag: "" },
              { title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts', flag: "" }]
            // this.pages = this.pages.concat({ title: 'MY DOCTORS', component: "AppMyDoctorsPage", icon: 'patient',flag: ""});
            // this.pages = this.pages.concat({ title: 'MY HEALTHCARE', component: "AppMyHealthcarePage", icon: 'hospital',flag: ""});
            // this.pages = this.pages.concat({ title: 'MY NURSES', component: "AppMyNursesPage", icon: 'nurse',flag: ""});

            if (val.only_me == 1 || val.email == "chintan.accupoint@gmail.com" || val.email == "Mads.thrane@victory.care" || val.email == "nishil.jain@victory.care" || val.email == "rahulparmar56@gmail.com") {
              console.log("val", val.only_me)
              this.pages.unshift({ title: 'MAIN DASHBOARD', component: "AppWeeklyDashboard2ReportPage", icon: 'dashboard', flag: "" })
            }
            this.storage.get('patientType').then((val) => {
              if (val == 'dependent') {
                this.pages = this.pages.concat({ title: 'MY DOCTORS', component: "AppMyDoctorsPage", icon: 'doctor', flag: "" });
                this.pages = this.pages.concat({ title: 'MY HEALTHCARE', component: "AppMyHealthcarePage", icon: 'hospital', flag: "" });
                this.pages = this.pages.concat({ title: 'MY NURSES', component: "AppMyNursesPage", icon: 'nurse', flag: "" });
              }
              this.pages = this.pages.concat({ title: 'SETTINGS', component: "AppSettingsPage", icon: 'settings', flag: "" });
            });
            //this.pages = this.pages.concat({ title: 'LOGOUT', component: "AppModalPage", icon: 'logout', flag: "" });
          }


          // }
        }
      });
    });
  }
  patientLogin(action) {
    // console.log(action);
    if (action == true) {
      return true;
    } else {
      return false;
    }
  }
  openPage(page) {
    this.initializeApp();
    this.storage.forEach((value, key, index) => {
      // console.log("This is the value", value);
      // console.log("from the key", key);
      // console.log("Index is", index);
    })
    this.storage.get('user').then((val) => {
      // console.log(val);
      // alert(val.image);
      if (val != null) {
        this.UserProfileImage = val.image != null ? val.image : '';
        this.UserName = val.name ? val.name : '';
      } else {
        this.UserProfileImage = '';
        this.UserName = '';
      }
      // console.log('this.pages',this.pages);
      if (page.component) {
        for (let i = 0; i < this.pages.length; i++) {
          /*console.log('page.title',page);
          console.log('title',this.pages[i].icon);*/
          if (page.title == this.pages[i].title) {
            /*console.log('page.title selected',page.title);
            console.log('title selected',this.pages[i].title);*/
            document.getElementById('active_page_' + this.pages[i].title).style = "background:#84D4E1 !important;";
            if (this.pages[i].flag != "image") {
              if (this.pages[i].icon != "") {
                document.getElementById('active_image_' + this.pages[i].title).src = "assets/images/menu-icons/" + this.pages[i].icon + "-white.png";
              }
            }
          }
          else {
            /*console.log('page.title unselected',page.title);
            console.log('title unselected',this.pages[i].title);*/
            document.getElementById('active_page_' + this.pages[i].title).style = "background:#474f69 !important;";
            if (this.pages[i].flag != "image") {
              if (this.pages[i].icon != "") {
                document.getElementById('active_image_' + this.pages[i].title).src = "assets/images/menu-icons/" + this.pages[i].icon + ".png";
              }
            }
          }
        }
        if (page.component == 'AppPatientProfilePage') {
          this.nav.setRoot(page.component, { 'data': val, 'action': 'edit' });
        }
        else {
          this.nav.setRoot(page.component, { 'data': val });
        }

      } else {
        this.presentConfirm();
      }

    });


  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      message: 'Are you sure you want to log out?',
      buttons: [
        {
          text: 'Yes',
          role: 'yes',
          handler: () => {
            this.nav.setRoot('AppLoginPage');
          }
        },
        {
          text: 'No',
          handler: () => {

          }
        }
      ]
    });
    alert.present();
  }

  profile() {
    this.nav.setRoot("AppUserEditPage");
  }
  logout() {
    // console.log("looged out");
    let modal = this.modalCtrl.create(AppLogoutPage, null, { cssClass: "my-logout" }
    );
    modal.onDidDismiss(data => {
      if (data) {
        this.loadingservice.show();
        this.storage.remove('userLogin');
        this.storage.remove('currentHCP');
        // this.navCtrl.setRoot(this.navCtrl.getActive().component);
        // event.target.complete();
        this.nav.setRoot("AppLoginPage");
        this.loadingservice.hide();
      }
      else {
        this.loadingservice.hide();
      }
      // console.log(data);
    });
    modal.present();
  }
}
