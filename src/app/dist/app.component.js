"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.MyApp = void 0;
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var SettingsProvider_1 = require("../services/SettingsProvider");
var app_logout_1 = require("../pages/app-logout/app-logout");
var MyApp = /** @class */ (function () {
    // const webserviceUrl = ""
    function MyApp(alertCtrl, platform, splashScreen, statusBar, menu, events, storage, modalCtrl, localNotifications, 
    // public navctrl: NavController,
    settings, loadingservice, fcm) {
        var _this = this;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.menu = menu;
        this.events = events;
        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.localNotifications = localNotifications;
        this.settings = settings;
        this.loadingservice = loadingservice;
        this.fcm = fcm;
        // pages: Array<{title: string, component: any, icon:any, flag:any}>;
        this.pages = [];
        this.rootPage = "AppWelcomeScreenPage";
        this.UserProfileImage = "";
        this.UserName = "";
        this.userTypeCookie = "";
        this.userNameCookie = "";
        this.menuCreator = true;
        this.wide = false;
        this.i = 0;
        this.settings.getActiveTheme().subscribe(function (val) { return _this.selectedTheme = val; });
        // this.events.publish('patinetData', '');
        this.splashScreen.hide();
        events.subscribe('user:created', function (user, time) {
            // console.log(user);
            // user and time are the same arguments passed in `events.publish(user, time)`
            _this.UserProfileImage = user.image;
            _this.UserName = user.name;
            if (_this.pages != undefined) {
                _this.pages[0].title = _this.UserName;
                _this.pages[0].component = 'AppPatientProfilePage';
                _this.pages[0].icon = _this.UserProfileImage;
                _this.pages[0].flag = "image";
            }
        });
        this.initializeApp();
        //this.initPushNotification();
    }
    // async presentAlert(data) {
    //   console.log("dataaaaaa", data)
    //   const alert = await this.alertCtrl.create({
    //     cssClass: 'my-custom-class',
    //     title: data.aps.alert.title,
    //     message: data.aps.alert.body,
    //     buttons: ['OK']
    //   });
    //   await alert.present();
    // }
    MyApp.prototype.setWidth = function () {
        this.wide = true;
        this.menu.open();
    };
    ;
    // initPushNotification() {
    //   this.platform.ready().then(() => {
    //     this.splashScreen.hide();
    //     if (this.platform.is('corodova') || this.platform.is('ios')) {
    //       this.fcm.subscribeToTopic('marketing');
    //       this.fcm.getToken().then(token => {
    //         //register token
    //         console.log("new token", token);
    //         console.log(token);
    //       })
    //       this.fcm.onNotification().subscribe(data => {
    //         if (data.wasTapped) {
    //           console.log("recieved in foregorund", data)
    //         } else {
    //           this.presentAlert(data)
    //           console.log("get notification", data)
    //         };
    //       })
    //       this.fcm.onTokenRefresh().subscribe(token => {
    //         //register token
    //         console.log("tokenrefresh", token);
    //       })
    //       this.fcm.unsubscribeFromTopic('marketing');
    //     }
    //   })
    // }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.splashScreen.hide();
            /*this.localNotifications.schedule({
              id: 1,
              title: 'Attention',
              text: 'Simons Notification',
              data: { mydata: 'My hidden message this is' },
              // trigger: {at: new Date(new Date().getTime() + 10 * 2000)},
            });*/
            /*this.push.hasPermission()
              .then((res: any) => {
      
                if (res.isEnabled) {
                  console.log('We have permission to send push notifications');
                } else {
                  console.log('We do not have permission to send push notifications');
                }
      
              });*/
            // this.firebase.getToken().then(token => console.log(token)).catch(err=> console.log(err));
            // this.firebase.onNotificationOpen().subscribe(data=>{
            //   console.log(data);
            //   console.log(data.name);
            // }, err=> console.log(err));
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            // this.menu.enable(true);
            // used for an example of ngFor and navigation
            // alert(this.UserProfileImage);
            _this.pages = [
            // { title: 'DASHBOARD', component: "AppDashboardPage", icon: 'dashboard',flag: ""},
            // { title: this.userNameCookie, component: "AppUserEditPage", icon: '',flag: ""},
            // { title: 'MEDICINES', component: "AppMedicineMasterListPage", icon: 'medicines',flag: ""},
            // { title: 'ACTIVITY', component: "AppActivityMasterListPage", icon: 'library',flag: ""},
            // { title: 'EVENTS', component: "AppEventMasterListPage", icon: 'event',flag: ""},
            // { title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts',flag: ""}
            ];
            // alert('this.userNameCookie');
            _this.storage.get('userLogin').then(function (val) {
                if (val) {
                    if (val.image) {
                        _this.image = val.image;
                    }
                    else {
                        _this.image = "";
                    }
                }
                // console.log(val);
                if (val != null) {
                    // this.menuCreator = true;
                    // this.storage.remove('allDoctorsData');
                    // if(this.i == 0){
                    if (val.usertype == 'superadmin' && _this.menuCreator == true) {
                        _this.pages = [{ title: 'DASHBOARD', component: "AppDashboardPage", icon: 'dashboard', flag: "" },
                            { title: 'MY PROFILE', component: "AppUserEditPage", icon: 'settings', flag: "" },
                            { title: 'MEDICINES', component: "AppMedicineMasterListPage", icon: 'medicines', flag: "" },
                            { title: 'ACTIVITIES', component: "AppActivityMasterListPage", icon: 'library', flag: "" },
                            { title: 'WELLBEING', component: "AppEventMasterListPage", icon: 'event', flag: "" },
                            // { title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts',flag: ""},
                            { title: 'PATIENTS', component: "AppPatientsPage", icon: 'patient', flag: "" }, { title: 'HEALTHCARE', component: "AppHealthcareproviderPage", icon: 'hospital', flag: "" }, { title: 'DOCTORS', component: "AppDoctorsPage", icon: 'doctor', flag: "" }, { title: 'NURSES', component: "AppNursesPage", icon: 'nurse', flag: "" }];
                        // this.pages = this.pages.concat({ title: 'PATIENTS', component: "AppPatientsPage", icon: 'patient',flag: ""});
                        // this.pages = this.pages.concat({ title: 'HEALTHCARE', component: "AppHealthcareproviderPage", icon: 'hospital',flag: ""});
                        // this.pages = this.pages.concat({ title: 'DOCTORS', component: "AppDoctorsPage", icon: 'doctor',flag: ""});
                        // this.pages = this.pages.concat({ title: 'NURSES', component: "AppNursesPage", icon: 'nurse',flag: ""});
                        _this.storage.get('patientSession').then(function (val) {
                            if (val != undefined)
                                _this.pages = _this.pages.concat({ title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts', flag: "" });
                        });
                    }
                    else if (val.usertype == 'healthcareprovider') {
                        _this.pages = [{ title: 'DASHBOARD', component: "AppDashboardPage", icon: 'dashboard', flag: "" },
                            { title: 'MY PROFILE', component: "AppUserEditPage", icon: 'settings', flag: "" },
                            { title: 'MEDICINES', component: "AppMedicineMasterListPage", icon: 'medicines', flag: "" },
                            { title: 'ACTIVITIES', component: "AppActivityMasterListPage", icon: 'library', flag: "" },
                            { title: 'WELLBEING', component: "AppEventMasterListPage", icon: 'event', flag: "" },
                            // { title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts',flag: ""},
                            { title: 'MY PATIENTS', component: "AppMyPatientsPage", icon: 'patients', flag: "" },
                            { title: 'DOCTORS', component: "AppDoctorsPage", icon: 'doctor', flag: "" }, { title: 'NURSES', component: "AppNursesPage", icon: 'nurse', flag: "" }];
                        // this.pages = this.pages.concat({ title: 'MY PATIENTS', component: "AppMyPatientsPage", icon: 'patient',flag: ""});
                        // this.pages = this.pages.concat({ title: 'DOCTORS', component: "AppDoctorsPage", icon: 'doctor',flag: ""});
                        // this.pages = this.pages.concat({ title: 'NURSES', component: "AppNursesPage", icon: 'nurse',flag: ""});
                        _this.storage.get('patientSession').then(function (val) {
                            if (val != undefined)
                                _this.pages = _this.pages.concat({ title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts', flag: "" });
                        });
                    }
                    else if (val.usertype == 'doctor') {
                        _this.pages = [{ title: 'DASHBOARD', component: "AppDashboardPage", icon: 'dashboard', flag: "" },
                            { title: 'MY PROFILE', component: "AppUserEditPage", icon: 'settings', flag: "" },
                            { title: 'MEDICINES', component: "AppMedicineMasterListPage", icon: 'medicines', flag: "" },
                            { title: 'ACTIVITIES', component: "AppActivityMasterListPage", icon: 'library', flag: "" },
                            { title: 'WELLBEING', component: "AppEventMasterListPage", icon: 'event', flag: "" },
                            // { title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts',flag: ""},
                            { title: 'MY PATIENTS', component: "AppMyPatientsPage", icon: 'patients', flag: "" }, { title: 'MY HEALTHCARE', component: "AppMyHealthcarePage", icon: 'hospital', flag: "" }, { title: 'MY NURSES', component: "AppMyNursesPage", icon: 'nurse', flag: "" }];
                        // this.pages = this.pages.push(pages1);
                        // this.pages = this.pages.concat({ title: 'MY HEALTHCARE', component: "AppMyHealthcarePage", icon: 'hospital',flag: ""});
                        // this.pages = this.pages.concat({ title: 'MY NURSES', component: "AppMyNursesPage", icon: 'nurse',flag: ""});
                        _this.storage.get('patientSession').then(function (val) {
                            if (val != undefined)
                                _this.pages = _this.pages.concat({ title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts', flag: "" });
                        });
                    }
                    else if (val.usertype == 'nurse') {
                        _this.pages = [{ title: 'DASHBOARD', component: "AppDashboardPage", icon: 'dashboard', flag: "" },
                            { title: 'MY PROFILE', component: "AppUserEditPage", icon: 'settings', flag: "" },
                            { title: 'MEDICINES', component: "AppMedicineMasterListPage", icon: 'medicines', flag: "" },
                            { title: 'ACTIVITIES', component: "AppActivityMasterListPage", icon: 'library', flag: "" },
                            { title: 'WELLBEING', component: "AppEventMasterListPage", icon: 'event', flag: "" },
                            // { title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts',flag: ""},
                            { title: 'MY PATIENTS', component: "AppMyPatientsPage", icon: 'patients', flag: "" }, { title: 'MY HEALTHCARE', component: "AppMyHealthcarePage", icon: 'hospital', flag: "" }, { title: 'MY DOCTORS', component: "AppMyDoctorsPage", icon: 'doctor', flag: "" }];
                        // this.pages = this.pages.concat({ title: 'MY PATIENTS', component: "AppMyPatientsPage", icon: 'patient',flag: ""});
                        // this.pages = this.pages.concat({ title: 'MY HEALTHCARE', component: "AppMyHealthcarePage", icon: 'hospital',flag: ""});
                        // this.pages = this.pages.concat({ title: 'MY DOCTORS', component: "AppMyDoctorsPage", icon: 'doctor',flag: ""});
                        _this.storage.get('patientSession').then(function (val) {
                            if (val != undefined)
                                _this.pages = _this.pages.concat({ title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts', flag: "" });
                        });
                    }
                    else if (val.usertype == 'patient') {
                        _this.pages = [
                            { title: 'WELLBEING DASHBOARD', component: "AppReportPage", icon: 'dashboard', flag: "" },
                            { title: 'TIMELINE', component: "AppTimelinePage", icon: 'timeline', flag: "" },
                            { title: 'MEDICINES', component: "AppMedicineMasterListPage", icon: 'medicines', flag: "" },
                            { title: 'ACTIVITIES', component: "AppActivityMasterListPage", icon: 'library', flag: "" },
                            { title: 'WELLBEING', component: "AppEventMasterListPage", icon: 'event', flag: "" },
                            { title: 'LIBRARY', component: "AppLibraryPage", icon: 'event', flag: "" },
                            { title: 'CONTACTS', component: "AppPatientsContactsPage", icon: 'contacts', flag: "" }
                        ];
                        // this.pages = this.pages.concat({ title: 'MY DOCTORS', component: "AppMyDoctorsPage", icon: 'patient',flag: ""});
                        // this.pages = this.pages.concat({ title: 'MY HEALTHCARE', component: "AppMyHealthcarePage", icon: 'hospital',flag: ""});
                        // this.pages = this.pages.concat({ title: 'MY NURSES', component: "AppMyNursesPage", icon: 'nurse',flag: ""});
                        if (val.only_me == 1 || val.email == "chintan.accupoint@gmail.com" || val.email == "Mads.thrane@victory.care" || val.email == "nishil.jain@victory.care" || val.email == "rahulparmar56@gmail.com") {
                            console.log("val", val.only_me);
                            _this.pages.unshift({ title: 'MAIN DASHBOARD', component: "AppWeeklyDashboard2ReportPage", icon: 'dashboard', flag: "" });
                        }
                        _this.storage.get('patientType').then(function (val) {
                            if (val == 'dependent') {
                                _this.pages = _this.pages.concat({ title: 'MY DOCTORS', component: "AppMyDoctorsPage", icon: 'doctor', flag: "" });
                                _this.pages = _this.pages.concat({ title: 'MY HEALTHCARE', component: "AppMyHealthcarePage", icon: 'hospital', flag: "" });
                                _this.pages = _this.pages.concat({ title: 'MY NURSES', component: "AppMyNursesPage", icon: 'nurse', flag: "" });
                            }
                            _this.pages = _this.pages.concat({ title: 'SETTINGS', component: "AppSettingsPage", icon: 'settings', flag: "" });
                        });
                        //this.pages = this.pages.concat({ title: 'LOGOUT', component: "AppModalPage", icon: 'logout', flag: "" });
                    }
                    // }
                }
            });
        });
    };
    MyApp.prototype.patientLogin = function (action) {
        // console.log(action);
        if (action == true) {
            return true;
        }
        else {
            return false;
        }
    };
    MyApp.prototype.openPage = function (page) {
        var _this = this;
        this.initializeApp();
        this.storage.forEach(function (value, key, index) {
            // console.log("This is the value", value);
            // console.log("from the key", key);
            // console.log("Index is", index);
        });
        this.storage.get('user').then(function (val) {
            // console.log(val);
            // alert(val.image);
            if (val != null) {
                _this.UserProfileImage = val.image != null ? val.image : '';
                _this.UserName = val.name ? val.name : '';
            }
            else {
                _this.UserProfileImage = '';
                _this.UserName = '';
            }
            // console.log('this.pages',this.pages);
            if (page.component) {
                for (var i = 0; i < _this.pages.length; i++) {
                    /*console.log('page.title',page);
                    console.log('title',this.pages[i].icon);*/
                    if (page.title == _this.pages[i].title) {
                        /*console.log('page.title selected',page.title);
                        console.log('title selected',this.pages[i].title);*/
                        document.getElementById('active_page_' + _this.pages[i].title).style = "background:#84D4E1 !important;";
                        if (_this.pages[i].flag != "image") {
                            if (_this.pages[i].icon != "") {
                                document.getElementById('active_image_' + _this.pages[i].title).src = "assets/images/menu-icons/" + _this.pages[i].icon + "-white.png";
                            }
                        }
                    }
                    else {
                        /*console.log('page.title unselected',page.title);
                        console.log('title unselected',this.pages[i].title);*/
                        document.getElementById('active_page_' + _this.pages[i].title).style = "background:#474f69 !important;";
                        if (_this.pages[i].flag != "image") {
                            if (_this.pages[i].icon != "") {
                                document.getElementById('active_image_' + _this.pages[i].title).src = "assets/images/menu-icons/" + _this.pages[i].icon + ".png";
                            }
                        }
                    }
                }
                if (page.component == 'AppPatientProfilePage') {
                    _this.nav.setRoot(page.component, { 'data': val, 'action': 'edit' });
                }
                else {
                    _this.nav.setRoot(page.component, { 'data': val });
                }
            }
            else {
                _this.presentConfirm();
            }
        });
    };
    MyApp.prototype.presentConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: 'Are you sure you want to log out?',
            buttons: [
                {
                    text: 'Yes',
                    role: 'yes',
                    handler: function () {
                        _this.nav.setRoot('AppLoginPage');
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    MyApp.prototype.profile = function () {
        this.nav.setRoot("AppUserEditPage");
    };
    MyApp.prototype.logout = function () {
        var _this = this;
        // console.log("looged out");
        var modal = this.modalCtrl.create(app_logout_1.AppLogoutPage, null, { cssClass: "my-logout" });
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.loadingservice.show();
                _this.storage.remove('userLogin');
                _this.storage.remove('currentHCP');
                // this.navCtrl.setRoot(this.navCtrl.getActive().component);
                // event.target.complete();
                _this.nav.setRoot("AppLoginPage");
                _this.loadingservice.hide();
            }
            else {
                _this.loadingservice.hide();
            }
            // console.log(data);
        });
        modal.present();
    };
    __decorate([
        core_1.ViewChild(ionic_angular_1.Nav)
    ], MyApp.prototype, "nav");
    MyApp = __decorate([
        core_1.Component({
            templateUrl: 'app.html',
            providers: [SettingsProvider_1.SettingsProvider]
        })
    ], MyApp);
    return MyApp;
}());
exports.MyApp = MyApp;
