import { NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler, ModalController, NavController } from 'ionic-angular';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
// import { RouterModule } from '@angular/router';
import { ToastService } from '../services/toast-service';
import { LoadingService } from '../services/loading-service';
import { Camera } from '@ionic-native/camera';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { AppPatientEventModalPage } from "../pages/app-patient-event-modal/app-patient-event-modal";
import { AppPatientEventModalModule } from "../pages/app-patient-event-modal/app-patient-event-modal.module";
import { DatabaseService } from "../services/database-service";
import { AppPatientMedicineModalModule } from "../pages/app-patient-medicine-modal/app-patient-medicine-modal.module";
import { AppPatientMedicineModalPage } from "../pages/app-patient-medicine-modal/app-patient-medicine-modal";
import { AppPatientContactsModalPageModule } from "../pages/app-patient-contacts-modal/app-patient-contacts-modal.module";
import { AppPatientContactsModalPage } from "../pages/app-patient-contacts-modal/app-patient-contacts-modal";
import { AppPatientActivityModalModule } from "../pages/app-patient-activity-modal/app-patient-activity-modal.module";
import { AppPatientActivityModalPage } from "../pages/app-patient-activity-modal/app-patient-activity-modal";
import { AppPatientWheelEventModalModule } from "../pages/app-patient-wheel-event-modal/app-patient-wheel-event-modal.module";
import { AppPatientWheelEventModalPage } from "../pages/app-patient-wheel-event-modal/app-patient-wheel-event-modal";
import { AppSharedService } from "../services/app-shared-service";
import { AppPasswordRecoveryModalPage } from "../pages/app-password-recovery-modal/app-password-recovery-modal";
import { AppPasswordRecoveryModalModule } from "../pages/app-password-recovery-modal/app-password-recovery-modal.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppModalModule } from "../pages/app-modal/app-modal.module";
import { AppAddContactModalPage } from "../pages/app-add-contact-modal/app-add-contact-modal";
import { AppAddContactModalModule } from "../pages/app-add-contact-modal/app-add-contact-modal.module";
import { AppPatientsContactsPage } from '../pages/app-patients-contacts/app-patients-contacts';
import { AppPatientsContactsPageModule } from '../pages/app-patients-contacts/app-patients-contacts.module';
import { AppMedicineMasterListPage } from '../pages/app-medicine-master-list/app-medicine-master-list';
import { AppMedicineMasterListPageModule } from '../pages/app-medicine-master-list/app-medicine-master-list.module';
import { AppMedicineMasterListModalPage } from '../pages/app-medicine-master-list-modal/app-medicine-master-list-modal';
import { AppMedicineMasterListModalPageModule } from '../pages/app-medicine-master-list-modal/app-medicine-master-list-modal.module';
import { AppAddMedicineMasterPage } from "../pages/app-add-medicine-master/app-add-medicine-master";
import { AppAddMedicineMasterPageModule } from "../pages/app-add-medicine-master/app-add-medicine-master.module";
import { AppActivityMasterListPage } from "../pages/app-activity-master-list/app-activity-master-list";
import { AppActivityMasterListPageModule } from "../pages/app-activity-master-list/app-activity-master-list.module";
import { AppEventMasterListPage } from "../pages/app-event-master-list/app-event-master-list";
import { AppEventMasterListPageModule } from "../pages/app-event-master-list/app-event-master-list.module";
import { AppPatientHomePage } from "../pages/app-patient-home/app-patient-home";
import { AppPatientHomeModule } from "../pages/app-patient-home/app-patient-home.module";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { IonicStorageModule } from '@ionic/storage';
import { GooglePlus } from '@ionic-native/google-plus';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { LinePageModule } from '../pages/line/line.module';
import { AppAddActivityModelPage } from '../pages/app-add-activity-model/app-add-activity-model';
import { AppAddActivityModelPageModule } from "../pages/app-add-activity-model/app-add-activity-model.module";
import { AppAddMedicineModelPage } from '../pages/app-add-medicine-model/app-add-medicine-model';
import { AppAddMedicineModelPageModule } from "../pages/app-add-medicine-model/app-add-medicine-model.module";
import { AppAddCurrentStatusModelPage } from '../pages/app-add-current-status-model/app-add-current-status-model';
import { AppAddCurrentStatusModelPageModule } from "../pages/app-add-current-status-model/app-add-current-status-model.module";
import { RlTagInputModule } from 'angular2-tag-input';
import { AppLogoutPage } from '../pages/app-logout/app-logout';
import { AppLogoutPageModule } from '../pages/app-logout/app-logout.module';
import { Geolocation } from '@ionic-native/geolocation';
import { AppMedicineViewallNotificationPage } from '../pages/app-medicine-viewall-notification/app-medicine-viewall-notification';
import { AppActivityViewallNotificationPage } from '../pages/app-activity-viewall-notification/app-activity-viewall-notification';
import { AppEventViewallNotificationPage } from '../pages/app-event-viewall-notification/app-event-viewall-notification';
import { AppMedicineViewallNotificationPageModule } from '../pages/app-medicine-viewall-notification/app-medicine-viewall-notification.module';
import { AppActivityViewallNotificationPageModule } from '../pages/app-activity-viewall-notification/app-activity-viewall-notification.module';
import { AppEventViewallNotificationPageModule } from '../pages/app-event-viewall-notification/app-event-viewall-notification.module';
import { AppPatientsActivityPage } from '../pages/app-patients-activity/app-patients-activity';
import { AppPatientsActivityPageModule } from '../pages/app-patients-activity/app-patients-activity.module';
import { AppMedicinePrescriptionIssuedPageModule } from '../pages/app-medicine-prescription-issued/app-medicine-prescription-issued.module';
import { AppMedicinePrescriptionIssuedPage } from '../pages/app-medicine-prescription-issued/app-medicine-prescription-issued';
import { DailyWheelSettingsActivityPage } from '../pages/daily-wheel-settings-activity/daily-wheel-settings-activity'
import { DailyWheelSettingsWellbeingPage } from '../pages/daily-wheel-settings-wellbeing/daily-wheel-settings-wellbeing'
import { DailyWheelSettingsMedicinePage } from '../pages/daily-wheel-settings-medicine/daily-wheel-settings-medicine'
import { AppSettingsDailyWheelSettingsPage } from '../pages/app-settings-daily-wheel-settings/app-settings-daily-wheel-settings'
import { FCM } from '@ionic-native/fcm';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { Storage } from "@ionic/storage";
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file/ngx';
import { AppSettingsDailyWheelSettingsPageModule } from '../pages/app-settings-daily-wheel-settings/app-settings-daily-wheel-settings.module';
import { DailyWheelSettingsActivityPageModule } from '../pages/daily-wheel-settings-activity/daily-wheel-settings-activity.module';
import { DailyWheelSettingsMedicinePageModule } from '../pages/daily-wheel-settings-medicine/daily-wheel-settings-medicine.module';
import { DailyWheelSettingsWellbeingPageModule } from '../pages/daily-wheel-settings-wellbeing/daily-wheel-settings-wellbeing.module';
import { SocialAccountsPage } from '../pages/social-accounts/social-accounts';
import { SocialAccountsPageModule } from '../pages/social-accounts/social-accounts.module';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Screenshot } from '@ionic-native/screenshot/ngx';
import { IonicSelectableModule } from 'ionic-selectable';
import { AppSecurityAndAccessSettingsPage } from "../pages/app-security-and-access-settings/app-security-and-access-settings";
import { AppSecurityAndAccessSettingsPageModule } from "../pages/app-security-and-access-settings/app-security-and-access-settings.module";
import { AppSettingsTermsAndConditionsPage } from '../pages/app-settings-terms-and-conditions/app-settings-terms-and-conditions';
import { AppSettingsTermsAndConditionsPageModule } from '../pages/app-settings-terms-and-conditions/app-settings-terms-and-conditions.module';
import { AppReportPage } from '../pages/app-report/app-report';
import { AppReportPageModule } from '../pages/app-report/app-report.module';
import { AppWeeklyReportPage } from '../pages/app-weekly-report/app-weekly-report';
import { AppWeeklyReportPageModule } from '../pages/app-weekly-report/app-weekly-report.module';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';


@NgModule({
    declarations: [MyApp,],
    providers: [
        StatusBar, SocialSharing, FCM, Screenshot, SplashScreen, Geolocation, LocalNotifications, CookieService, Camera, GooglePlus,
        ToastService, LoadingService, GoogleAnalytics, ModalController,OpenNativeSettings, AppSharedService, FileTransfer, File,DatabaseService,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
    ],
    imports: [
        BrowserModule,
        HttpModule,
        IonicImageViewerModule,
        ReactiveFormsModule,
        IonicSelectableModule,
        NgxDatatableModule,
        IonicStorageModule.forRoot(),
        IonicModule.forRoot(MyApp, {
            backButtonText: '',
            scrollAssist: false,
            autoFocusAssist: false,
        }), AppPatientEventModalModule, AppWeeklyReportPageModule, AppReportPageModule, AppSettingsTermsAndConditionsPageModule, AppSecurityAndAccessSettingsPageModule, SocialAccountsPageModule, AppSettingsDailyWheelSettingsPageModule, DailyWheelSettingsActivityPageModule, DailyWheelSettingsMedicinePageModule, DailyWheelSettingsWellbeingPageModule, AppMedicineMasterListModalPageModule, AppMedicinePrescriptionIssuedPageModule, AppActivityMasterListPageModule, AppPatientsActivityPageModule, AppMedicineViewallNotificationPageModule, AppActivityViewallNotificationPageModule, AppEventViewallNotificationPageModule, AppLogoutPageModule, AppAddCurrentStatusModelPageModule, AppAddMedicineModelPageModule, AppAddActivityModelPageModule, AppPatientContactsModalPageModule, LinePageModule, AppPatientMedicineModalModule, AppPatientActivityModalModule, AppPatientWheelEventModalModule, AppPasswordRecoveryModalModule, AppModalModule, AppAddContactModalModule, AppPatientsContactsPageModule, AppMedicineMasterListPageModule, AppMedicineMasterListModalPageModule, AppAddMedicineMasterPageModule, AppActivityMasterListPageModule, AppPatientHomeModule, RlTagInputModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [MyApp, AppSettingsTermsAndConditionsPage, AppReportPage, AppWeeklyReportPage, AppLogoutPage, AppSecurityAndAccessSettingsPage, SocialAccountsPage, AppSettingsDailyWheelSettingsPage, DailyWheelSettingsActivityPage, DailyWheelSettingsWellbeingPage, DailyWheelSettingsMedicinePage, AppMedicineViewallNotificationPage, AppActivityViewallNotificationPage, AppMedicineMasterListPage, AppMedicinePrescriptionIssuedPage, AppPatientsActivityPage, AppEventViewallNotificationPage, AppPatientEventModalPage, AppAddCurrentStatusModelPage, AppAddMedicineModelPage, AppAddActivityModelPage, AppPatientMedicineModalPage, AppPatientContactsModalPage, AppPatientActivityModalPage, AppPatientWheelEventModalPage, AppPasswordRecoveryModalPage, AppAddContactModalPage, AppPatientsContactsPage, AppMedicineMasterListPage, AppMedicineMasterListModalPage, AppAddMedicineMasterPage, AppActivityMasterListPage, AppPatientHomePage],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppModule {

}
