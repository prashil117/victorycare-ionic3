import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { LoadingService } from "./loading-service";
import { ToastService } from "./toast-service";
import {
  AlertController,
  NavController,
  ViewController,
  ModalController,
  Events,
} from "ionic-angular";
import { AppSharedService } from "./app-shared-service";
import { CookieService } from "angular2-cookie/services/cookies.service";
import { Storage } from "@ionic/storage";
import { AppLogoutPage } from "../pages/app-logout/app-logout";
import moment from "moment";

// import { HttpClient } from '@angular/common/http';
@Injectable()
export class DatabaseService {
  public loggedInUser: any;
  public authState: any;
  public currentUser: any = {};
  public patientData: any;
  myRand: any;
  date: any;

  constructor(
    public loadingservice: LoadingService,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    private loadingService: LoadingService,
    private toastCtrl: ToastService,
    private alertCtrl: AlertController,
    public appSharedService: AppSharedService,
    private _cookieService: CookieService,
    public storage: Storage,
    public viewCtrl: ViewController,
    public events: Events,
  ) {
    let self = this;
    this.currentUser = this.appSharedService.currentUser;
    this.currentUser = {};
    this.currentUser.email = "kunal@kunal.com";
    this.storage.get("user").then((val) => {
    });
  }

  logout() {
    let modal = this.modalCtrl.create(
      AppLogoutPage,
      null,
      { cssClass: "my-logout" },
    );
    modal.onDidDismiss((data) => {
      this.loadingservice.show();
      if (data) {
        this.storage.remove("userLogin");
        this.storage.remove("currentHCP");
        this.storage.get("remember").then((val) => {
          if (val != undefined || val != null) {
            this.storage.remove("userLogin");
            this.storage.remove("currentHCP");
            this.navCtrl.setRoot("AppLoginPage");
          } else {
            this.storage.clear();
            this.navCtrl.setRoot("AppLoginPage");
          }
        });
        this.loadingservice.hide();
      }
      console.log(data);
    });
    modal.present();
  }

  setDate(date) {
    this.events.publish('set:changed', date);
  }

  getDate() {
    this.events.subscribe('user:created', (date) => {
      console.log('Welcome hello at', date);
    });
  }
}
